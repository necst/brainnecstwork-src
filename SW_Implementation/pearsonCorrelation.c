/*
 * Copyright 2016 Enrico Reggiani, Eleonora D'Arnese, Marco Gucciardi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/  
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <sys/time.h>
#include <omp.h>
#include <assert.h>

# define float_type float

void initValues(float_type * mat, int rows, int cols)
{
	int i, j;
	for (i = 0; i < rows; i++)
		for (j = 0; j < cols; j++)

			mat[i * 50 + j] = i + j;
}

void outputValues(float_type * mat, int vals)
{
	int i, j;
	for (i = 0; i < vals; i++) {
		for (j = 0; j < vals; j++)
			printf("%f\t", mat[i * vals + j]);

		printf("\n");
	}
}

unsigned long getTime()
{
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return tv.tv_sec * 1000 * 1000 + tv.tv_usec;
}

void *pearsonCorrelation_serial(float_type * pixel, float_type * dep,
				float_type * devs, float_type * corr, int P,
				int K)
{
	int c1, c2;
	int s;
	float_type devtot;

	for (c1 = 0; c1 < P; c1++) {
		for (c2 = 0; c2 < P; c2++) {
			if (c1 > c2) {
				float_type tmp = 0;

				for (s = 0; s < K; s++) {

					tmp =
					    tmp + dep[c1 * K + s] * dep[c2 * K +
									s];
				}
				devtot = devs[c1] * devs[c2] * K;
				*(corr + c1 * P + c2) = tmp / devtot;
				corr[c2 * P + c1] = corr[c1 * P + c2];
			}
			if (c1 == c2) {
				*(corr + c1 * P + c2) = 1;
			}
		}
	}

	return NULL;
}

void *pearsonCorrelation_OpenMP(float_type * pixel, float_type * dep,
				float_type * devs, float_type * corr, int P,
				int K)
{

	int c1, c2;

#pragma omp parallel for firstprivate(corr, dep, devs)
	for (c1 = 0; c1 < P; c1++) {
		for (c2 = 0; c2 < P; c2++) {
			if (c1 == c2) {
				corr[c1 * P + c2] = 1;
			}
			if (c1 > c2) {
				float_type tmp = 0;

				int s;
				for (s = 0; s < K; s++) {

					tmp =
					    tmp + dep[c1 * K + s] * dep[c2 * K +
									s];
				}
				float_type devtot;
				devtot = devs[c1] * devs[c2] * K;
				corr[c1 * P + c2] = tmp / devtot;
				corr[c2 * P + c1] = corr[c1 * P + c2];
			}
		}
	}

	return NULL;
}

int main()
{

	int P = 24000;
	int K = 50;

	unsigned long t0, t1, tCorrSerial, tCorrOpenMp;
	float_type *media = (float_type *) malloc(sizeof(float_type) * P);
	float_type *pixel = (float_type *) malloc(sizeof(float_type) * P * K);
	float_type *dep = (float_type *) malloc(sizeof(float_type) * P * K);
	float_type *devs = (float_type *) malloc(sizeof(float_type) * P);;

	float_type *corr = (float_type *) malloc(sizeof(float_type) * P * P);

	int i, j, k, r, a, b, s, c1, c2;

	assert(pixel != NULL);
	assert(dep != NULL);
	assert(corr != NULL);

	initValues(pixel, P, K);

	printf("Starting computation... \n");
	for (i = 0; i < P; i++) {
		media[i] = 0;

		for (j = 0; j < K; j++) {
			media[i] = media[i] + *(pixel + K * i + j);
		}
		media[i] = media[i] / K;
	}

	for (k = 0; k < P; k++) {
		for (r = 0; r < K; r++) {
			dep[k * K + r] = *(pixel + k * K + r) - media[k];
		}
	}

	for (a = 0; a < P; a++) {
		devs[a] = 0;
		for (b = 0; b < K; b++) {
			devs[a] = devs[a] + dep[a * K + b] * dep[a * K + b];
		}
		devs[a] = devs[a] / (K - 1.0);
		devs[a] = sqrt(devs[a]);
	}

	t0 = getTime();
	pearsonCorrelation_serial(pixel, dep, devs, corr, P, K);
	t1 = getTime();
	tCorrSerial = (t1 - t0) / 1000000;

	t0 = getTime();
	pearsonCorrelation_OpenMP(pixel, dep, devs, corr, P, K);
	t1 = getTime();
	tCorrOpenMp = (t1 - t0) / 1000000;

	printf("Execution Time Serial:\t%ld seconds\n", tCorrSerial);
	printf("Execution Time OPENMP:\t%ld seconds\n", tCorrOpenMp);


	free(media);
	free(devs);
	free(pixel);
	free(dep);
	free(corr);

	return 0;
}
