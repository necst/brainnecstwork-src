// (c) Copyright 1995-2016 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.

// IP VLNV: xilinx.com:hls:pcc_stream:1.0
// IP Revision: 1606231801

// The following must be inserted into your Verilog file for this
// core to be instantiated. Change the instance name and port connections
// (in parentheses) to your own signal names.

//----------- Begin Cut here for INSTANTIATION Template ---// INST_TAG
design_1_pcc_stream_0_0 your_instance_name (
  .ap_clk(ap_clk),                                                  // input wire ap_clk
  .ap_rst_n(ap_rst_n),                                              // input wire ap_rst_n
  .DMA_NumberOfCorrelation_TVALID(DMA_NumberOfCorrelation_TVALID),  // input wire DMA_NumberOfCorrelation_TVALID
  .DMA_NumberOfCorrelation_TREADY(DMA_NumberOfCorrelation_TREADY),  // output wire DMA_NumberOfCorrelation_TREADY
  .DMA_NumberOfCorrelation_TDATA(DMA_NumberOfCorrelation_TDATA),    // input wire [31 : 0] DMA_NumberOfCorrelation_TDATA
  .DMA_NumberOfCorrelation_TKEEP(DMA_NumberOfCorrelation_TKEEP),    // input wire [3 : 0] DMA_NumberOfCorrelation_TKEEP
  .DMA_NumberOfCorrelation_TSTRB(DMA_NumberOfCorrelation_TSTRB),    // input wire [3 : 0] DMA_NumberOfCorrelation_TSTRB
  .DMA_NumberOfCorrelation_TUSER(DMA_NumberOfCorrelation_TUSER),    // input wire [3 : 0] DMA_NumberOfCorrelation_TUSER
  .DMA_NumberOfCorrelation_TLAST(DMA_NumberOfCorrelation_TLAST),    // input wire [0 : 0] DMA_NumberOfCorrelation_TLAST
  .DMA_NumberOfCorrelation_TID(DMA_NumberOfCorrelation_TID),        // input wire [4 : 0] DMA_NumberOfCorrelation_TID
  .DMA_NumberOfCorrelation_TDEST(DMA_NumberOfCorrelation_TDEST),    // input wire [4 : 0] DMA_NumberOfCorrelation_TDEST
  .DMA_dep1_TVALID(DMA_dep1_TVALID),                                // input wire DMA_dep1_TVALID
  .DMA_dep1_TREADY(DMA_dep1_TREADY),                                // output wire DMA_dep1_TREADY
  .DMA_dep1_TDATA(DMA_dep1_TDATA),                                  // input wire [31 : 0] DMA_dep1_TDATA
  .DMA_dep1_TKEEP(DMA_dep1_TKEEP),                                  // input wire [3 : 0] DMA_dep1_TKEEP
  .DMA_dep1_TSTRB(DMA_dep1_TSTRB),                                  // input wire [3 : 0] DMA_dep1_TSTRB
  .DMA_dep1_TUSER(DMA_dep1_TUSER),                                  // input wire [3 : 0] DMA_dep1_TUSER
  .DMA_dep1_TLAST(DMA_dep1_TLAST),                                  // input wire [0 : 0] DMA_dep1_TLAST
  .DMA_dep1_TID(DMA_dep1_TID),                                      // input wire [4 : 0] DMA_dep1_TID
  .DMA_dep1_TDEST(DMA_dep1_TDEST),                                  // input wire [4 : 0] DMA_dep1_TDEST
  .DMA_dep2_TVALID(DMA_dep2_TVALID),                                // input wire DMA_dep2_TVALID
  .DMA_dep2_TREADY(DMA_dep2_TREADY),                                // output wire DMA_dep2_TREADY
  .DMA_dep2_TDATA(DMA_dep2_TDATA),                                  // input wire [31 : 0] DMA_dep2_TDATA
  .DMA_dep2_TKEEP(DMA_dep2_TKEEP),                                  // input wire [3 : 0] DMA_dep2_TKEEP
  .DMA_dep2_TSTRB(DMA_dep2_TSTRB),                                  // input wire [3 : 0] DMA_dep2_TSTRB
  .DMA_dep2_TUSER(DMA_dep2_TUSER),                                  // input wire [3 : 0] DMA_dep2_TUSER
  .DMA_dep2_TLAST(DMA_dep2_TLAST),                                  // input wire [0 : 0] DMA_dep2_TLAST
  .DMA_dep2_TID(DMA_dep2_TID),                                      // input wire [4 : 0] DMA_dep2_TID
  .DMA_dep2_TDEST(DMA_dep2_TDEST),                                  // input wire [4 : 0] DMA_dep2_TDEST
  .DMA_pcc_TVALID(DMA_pcc_TVALID),                                  // output wire DMA_pcc_TVALID
  .DMA_pcc_TREADY(DMA_pcc_TREADY),                                  // input wire DMA_pcc_TREADY
  .DMA_pcc_TDATA(DMA_pcc_TDATA),                                    // output wire [31 : 0] DMA_pcc_TDATA
  .DMA_pcc_TKEEP(DMA_pcc_TKEEP),                                    // output wire [3 : 0] DMA_pcc_TKEEP
  .DMA_pcc_TSTRB(DMA_pcc_TSTRB),                                    // output wire [3 : 0] DMA_pcc_TSTRB
  .DMA_pcc_TUSER(DMA_pcc_TUSER),                                    // output wire [3 : 0] DMA_pcc_TUSER
  .DMA_pcc_TLAST(DMA_pcc_TLAST),                                    // output wire [0 : 0] DMA_pcc_TLAST
  .DMA_pcc_TID(DMA_pcc_TID),                                        // output wire [4 : 0] DMA_pcc_TID
  .DMA_pcc_TDEST(DMA_pcc_TDEST)                                    // output wire [4 : 0] DMA_pcc_TDEST
);
// INST_TAG_END ------ End INSTANTIATION Template ---------

// You must compile the wrapper file design_1_pcc_stream_0_0.v when simulating
// the core, design_1_pcc_stream_0_0. When compiling the wrapper file, be sure to
// reference the Verilog simulation library.

