//Copyright 1986-2015 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2015.4 (lin64) Build 1412921 Wed Nov 18 09:44:32 MST 2015
//Date        : Wed Jun 29 11:21:32 2016
//Host        : localhost.localdomain running 64-bit CentOS release 6.6 (Final)
//Command     : generate_target design_1.bd
//Design      : design_1
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CORE_GENERATION_INFO = "design_1,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=design_1,x_ipVersion=1.00.a,x_ipLanguage=VERILOG,numBlks=115,numReposBlks=69,numNonXlnxBlks=0,numHierBlks=46,maxHierDepth=1,da_axi4_cnt=32,da_board_cnt=1,da_mb_cnt=1,da_mig7_cnt=1,synth_mode=Global}" *) (* HW_HANDOFF = "design_1.hwdef" *) 
module design_1
   (ddr3_sdram_addr,
    ddr3_sdram_ba,
    ddr3_sdram_cas_n,
    ddr3_sdram_ck_n,
    ddr3_sdram_ck_p,
    ddr3_sdram_cke,
    ddr3_sdram_cs_n,
    ddr3_sdram_dm,
    ddr3_sdram_dq,
    ddr3_sdram_dqs_n,
    ddr3_sdram_dqs_p,
    ddr3_sdram_odt,
    ddr3_sdram_ras_n,
    ddr3_sdram_reset_n,
    ddr3_sdram_we_n,
    reset,
    sys_diff_clock_clk_n,
    sys_diff_clock_clk_p);
  output [13:0]ddr3_sdram_addr;
  output [2:0]ddr3_sdram_ba;
  output ddr3_sdram_cas_n;
  output [0:0]ddr3_sdram_ck_n;
  output [0:0]ddr3_sdram_ck_p;
  output [0:0]ddr3_sdram_cke;
  output [0:0]ddr3_sdram_cs_n;
  output [7:0]ddr3_sdram_dm;
  inout [63:0]ddr3_sdram_dq;
  inout [7:0]ddr3_sdram_dqs_n;
  inout [7:0]ddr3_sdram_dqs_p;
  output [0:0]ddr3_sdram_odt;
  output ddr3_sdram_ras_n;
  output ddr3_sdram_reset_n;
  output ddr3_sdram_we_n;
  input reset;
  input sys_diff_clock_clk_n;
  input sys_diff_clock_clk_p;

  wire [31:0]axi_dma_0_M_AXIS_MM2S_TDATA;
  wire [4:0]axi_dma_0_M_AXIS_MM2S_TDEST;
  wire [4:0]axi_dma_0_M_AXIS_MM2S_TID;
  wire [3:0]axi_dma_0_M_AXIS_MM2S_TKEEP;
  wire axi_dma_0_M_AXIS_MM2S_TLAST;
  wire [0:0]axi_dma_0_M_AXIS_MM2S_TREADY;
  wire [3:0]axi_dma_0_M_AXIS_MM2S_TUSER;
  wire axi_dma_0_M_AXIS_MM2S_TVALID;
  wire [31:0]axi_dma_0_M_AXI_MM2S_ARADDR;
  wire [1:0]axi_dma_0_M_AXI_MM2S_ARBURST;
  wire [3:0]axi_dma_0_M_AXI_MM2S_ARCACHE;
  wire [7:0]axi_dma_0_M_AXI_MM2S_ARLEN;
  wire [2:0]axi_dma_0_M_AXI_MM2S_ARPROT;
  wire axi_dma_0_M_AXI_MM2S_ARREADY;
  wire [2:0]axi_dma_0_M_AXI_MM2S_ARSIZE;
  wire axi_dma_0_M_AXI_MM2S_ARVALID;
  wire [31:0]axi_dma_0_M_AXI_MM2S_RDATA;
  wire axi_dma_0_M_AXI_MM2S_RLAST;
  wire axi_dma_0_M_AXI_MM2S_RREADY;
  wire [1:0]axi_dma_0_M_AXI_MM2S_RRESP;
  wire axi_dma_0_M_AXI_MM2S_RVALID;
  wire [31:0]axi_dma_0_M_AXI_S2MM_AWADDR;
  wire [1:0]axi_dma_0_M_AXI_S2MM_AWBURST;
  wire [3:0]axi_dma_0_M_AXI_S2MM_AWCACHE;
  wire [7:0]axi_dma_0_M_AXI_S2MM_AWLEN;
  wire [2:0]axi_dma_0_M_AXI_S2MM_AWPROT;
  wire axi_dma_0_M_AXI_S2MM_AWREADY;
  wire [2:0]axi_dma_0_M_AXI_S2MM_AWSIZE;
  wire axi_dma_0_M_AXI_S2MM_AWVALID;
  wire axi_dma_0_M_AXI_S2MM_BREADY;
  wire [1:0]axi_dma_0_M_AXI_S2MM_BRESP;
  wire axi_dma_0_M_AXI_S2MM_BVALID;
  wire [31:0]axi_dma_0_M_AXI_S2MM_WDATA;
  wire axi_dma_0_M_AXI_S2MM_WLAST;
  wire axi_dma_0_M_AXI_S2MM_WREADY;
  wire [3:0]axi_dma_0_M_AXI_S2MM_WSTRB;
  wire axi_dma_0_M_AXI_S2MM_WVALID;
  wire [31:0]axi_dma_0_M_AXI_SG_ARADDR;
  wire [1:0]axi_dma_0_M_AXI_SG_ARBURST;
  wire [3:0]axi_dma_0_M_AXI_SG_ARCACHE;
  wire [7:0]axi_dma_0_M_AXI_SG_ARLEN;
  wire [2:0]axi_dma_0_M_AXI_SG_ARPROT;
  wire axi_dma_0_M_AXI_SG_ARREADY;
  wire [2:0]axi_dma_0_M_AXI_SG_ARSIZE;
  wire axi_dma_0_M_AXI_SG_ARVALID;
  wire [31:0]axi_dma_0_M_AXI_SG_AWADDR;
  wire [1:0]axi_dma_0_M_AXI_SG_AWBURST;
  wire [3:0]axi_dma_0_M_AXI_SG_AWCACHE;
  wire [7:0]axi_dma_0_M_AXI_SG_AWLEN;
  wire [2:0]axi_dma_0_M_AXI_SG_AWPROT;
  wire axi_dma_0_M_AXI_SG_AWREADY;
  wire [2:0]axi_dma_0_M_AXI_SG_AWSIZE;
  wire axi_dma_0_M_AXI_SG_AWVALID;
  wire axi_dma_0_M_AXI_SG_BREADY;
  wire [1:0]axi_dma_0_M_AXI_SG_BRESP;
  wire axi_dma_0_M_AXI_SG_BVALID;
  wire [31:0]axi_dma_0_M_AXI_SG_RDATA;
  wire axi_dma_0_M_AXI_SG_RLAST;
  wire axi_dma_0_M_AXI_SG_RREADY;
  wire [1:0]axi_dma_0_M_AXI_SG_RRESP;
  wire axi_dma_0_M_AXI_SG_RVALID;
  wire [31:0]axi_dma_0_M_AXI_SG_WDATA;
  wire axi_dma_0_M_AXI_SG_WLAST;
  wire axi_dma_0_M_AXI_SG_WREADY;
  wire [3:0]axi_dma_0_M_AXI_SG_WSTRB;
  wire axi_dma_0_M_AXI_SG_WVALID;
  wire axi_dma_0_mm2s_introut;
  wire axi_dma_0_s2mm_introut;
  wire [31:0]axi_dma_1_M_AXIS_MM2S_TDATA;
  wire [4:0]axi_dma_1_M_AXIS_MM2S_TDEST;
  wire [4:0]axi_dma_1_M_AXIS_MM2S_TID;
  wire [3:0]axi_dma_1_M_AXIS_MM2S_TKEEP;
  wire axi_dma_1_M_AXIS_MM2S_TLAST;
  wire [0:0]axi_dma_1_M_AXIS_MM2S_TREADY;
  wire [3:0]axi_dma_1_M_AXIS_MM2S_TUSER;
  wire axi_dma_1_M_AXIS_MM2S_TVALID;
  wire [31:0]axi_dma_1_M_AXI_MM2S_ARADDR;
  wire [1:0]axi_dma_1_M_AXI_MM2S_ARBURST;
  wire [3:0]axi_dma_1_M_AXI_MM2S_ARCACHE;
  wire [7:0]axi_dma_1_M_AXI_MM2S_ARLEN;
  wire [2:0]axi_dma_1_M_AXI_MM2S_ARPROT;
  wire axi_dma_1_M_AXI_MM2S_ARREADY;
  wire [2:0]axi_dma_1_M_AXI_MM2S_ARSIZE;
  wire axi_dma_1_M_AXI_MM2S_ARVALID;
  wire [31:0]axi_dma_1_M_AXI_MM2S_RDATA;
  wire axi_dma_1_M_AXI_MM2S_RLAST;
  wire axi_dma_1_M_AXI_MM2S_RREADY;
  wire [1:0]axi_dma_1_M_AXI_MM2S_RRESP;
  wire axi_dma_1_M_AXI_MM2S_RVALID;
  wire [31:0]axi_dma_1_M_AXI_S2MM_AWADDR;
  wire [1:0]axi_dma_1_M_AXI_S2MM_AWBURST;
  wire [3:0]axi_dma_1_M_AXI_S2MM_AWCACHE;
  wire [7:0]axi_dma_1_M_AXI_S2MM_AWLEN;
  wire [2:0]axi_dma_1_M_AXI_S2MM_AWPROT;
  wire axi_dma_1_M_AXI_S2MM_AWREADY;
  wire [2:0]axi_dma_1_M_AXI_S2MM_AWSIZE;
  wire axi_dma_1_M_AXI_S2MM_AWVALID;
  wire axi_dma_1_M_AXI_S2MM_BREADY;
  wire [1:0]axi_dma_1_M_AXI_S2MM_BRESP;
  wire axi_dma_1_M_AXI_S2MM_BVALID;
  wire [31:0]axi_dma_1_M_AXI_S2MM_WDATA;
  wire axi_dma_1_M_AXI_S2MM_WLAST;
  wire axi_dma_1_M_AXI_S2MM_WREADY;
  wire [3:0]axi_dma_1_M_AXI_S2MM_WSTRB;
  wire axi_dma_1_M_AXI_S2MM_WVALID;
  wire [31:0]axi_dma_1_M_AXI_SG_ARADDR;
  wire [1:0]axi_dma_1_M_AXI_SG_ARBURST;
  wire [3:0]axi_dma_1_M_AXI_SG_ARCACHE;
  wire [7:0]axi_dma_1_M_AXI_SG_ARLEN;
  wire [2:0]axi_dma_1_M_AXI_SG_ARPROT;
  wire axi_dma_1_M_AXI_SG_ARREADY;
  wire [2:0]axi_dma_1_M_AXI_SG_ARSIZE;
  wire axi_dma_1_M_AXI_SG_ARVALID;
  wire [31:0]axi_dma_1_M_AXI_SG_AWADDR;
  wire [1:0]axi_dma_1_M_AXI_SG_AWBURST;
  wire [3:0]axi_dma_1_M_AXI_SG_AWCACHE;
  wire [7:0]axi_dma_1_M_AXI_SG_AWLEN;
  wire [2:0]axi_dma_1_M_AXI_SG_AWPROT;
  wire axi_dma_1_M_AXI_SG_AWREADY;
  wire [2:0]axi_dma_1_M_AXI_SG_AWSIZE;
  wire axi_dma_1_M_AXI_SG_AWVALID;
  wire axi_dma_1_M_AXI_SG_BREADY;
  wire [1:0]axi_dma_1_M_AXI_SG_BRESP;
  wire axi_dma_1_M_AXI_SG_BVALID;
  wire [31:0]axi_dma_1_M_AXI_SG_RDATA;
  wire axi_dma_1_M_AXI_SG_RLAST;
  wire axi_dma_1_M_AXI_SG_RREADY;
  wire [1:0]axi_dma_1_M_AXI_SG_RRESP;
  wire axi_dma_1_M_AXI_SG_RVALID;
  wire [31:0]axi_dma_1_M_AXI_SG_WDATA;
  wire axi_dma_1_M_AXI_SG_WLAST;
  wire axi_dma_1_M_AXI_SG_WREADY;
  wire [3:0]axi_dma_1_M_AXI_SG_WSTRB;
  wire axi_dma_1_M_AXI_SG_WVALID;
  wire axi_dma_1_mm2s_introut;
  wire axi_dma_1_s2mm_introut;
  wire [31:0]axi_dma_2_M_AXIS_MM2S_TDATA;
  wire [4:0]axi_dma_2_M_AXIS_MM2S_TDEST;
  wire [4:0]axi_dma_2_M_AXIS_MM2S_TID;
  wire [3:0]axi_dma_2_M_AXIS_MM2S_TKEEP;
  wire axi_dma_2_M_AXIS_MM2S_TLAST;
  wire [0:0]axi_dma_2_M_AXIS_MM2S_TREADY;
  wire [3:0]axi_dma_2_M_AXIS_MM2S_TUSER;
  wire axi_dma_2_M_AXIS_MM2S_TVALID;
  wire [31:0]axi_dma_2_M_AXI_MM2S_ARADDR;
  wire [1:0]axi_dma_2_M_AXI_MM2S_ARBURST;
  wire [3:0]axi_dma_2_M_AXI_MM2S_ARCACHE;
  wire [7:0]axi_dma_2_M_AXI_MM2S_ARLEN;
  wire [2:0]axi_dma_2_M_AXI_MM2S_ARPROT;
  wire axi_dma_2_M_AXI_MM2S_ARREADY;
  wire [2:0]axi_dma_2_M_AXI_MM2S_ARSIZE;
  wire axi_dma_2_M_AXI_MM2S_ARVALID;
  wire [31:0]axi_dma_2_M_AXI_MM2S_RDATA;
  wire axi_dma_2_M_AXI_MM2S_RLAST;
  wire axi_dma_2_M_AXI_MM2S_RREADY;
  wire [1:0]axi_dma_2_M_AXI_MM2S_RRESP;
  wire axi_dma_2_M_AXI_MM2S_RVALID;
  wire [31:0]axi_dma_2_M_AXI_S2MM_AWADDR;
  wire [1:0]axi_dma_2_M_AXI_S2MM_AWBURST;
  wire [3:0]axi_dma_2_M_AXI_S2MM_AWCACHE;
  wire [7:0]axi_dma_2_M_AXI_S2MM_AWLEN;
  wire [2:0]axi_dma_2_M_AXI_S2MM_AWPROT;
  wire axi_dma_2_M_AXI_S2MM_AWREADY;
  wire [2:0]axi_dma_2_M_AXI_S2MM_AWSIZE;
  wire axi_dma_2_M_AXI_S2MM_AWVALID;
  wire axi_dma_2_M_AXI_S2MM_BREADY;
  wire [1:0]axi_dma_2_M_AXI_S2MM_BRESP;
  wire axi_dma_2_M_AXI_S2MM_BVALID;
  wire [31:0]axi_dma_2_M_AXI_S2MM_WDATA;
  wire axi_dma_2_M_AXI_S2MM_WLAST;
  wire axi_dma_2_M_AXI_S2MM_WREADY;
  wire [3:0]axi_dma_2_M_AXI_S2MM_WSTRB;
  wire axi_dma_2_M_AXI_S2MM_WVALID;
  wire [31:0]axi_dma_2_M_AXI_SG_ARADDR;
  wire [1:0]axi_dma_2_M_AXI_SG_ARBURST;
  wire [3:0]axi_dma_2_M_AXI_SG_ARCACHE;
  wire [7:0]axi_dma_2_M_AXI_SG_ARLEN;
  wire [2:0]axi_dma_2_M_AXI_SG_ARPROT;
  wire axi_dma_2_M_AXI_SG_ARREADY;
  wire [2:0]axi_dma_2_M_AXI_SG_ARSIZE;
  wire axi_dma_2_M_AXI_SG_ARVALID;
  wire [31:0]axi_dma_2_M_AXI_SG_AWADDR;
  wire [1:0]axi_dma_2_M_AXI_SG_AWBURST;
  wire [3:0]axi_dma_2_M_AXI_SG_AWCACHE;
  wire [7:0]axi_dma_2_M_AXI_SG_AWLEN;
  wire [2:0]axi_dma_2_M_AXI_SG_AWPROT;
  wire axi_dma_2_M_AXI_SG_AWREADY;
  wire [2:0]axi_dma_2_M_AXI_SG_AWSIZE;
  wire axi_dma_2_M_AXI_SG_AWVALID;
  wire axi_dma_2_M_AXI_SG_BREADY;
  wire [1:0]axi_dma_2_M_AXI_SG_BRESP;
  wire axi_dma_2_M_AXI_SG_BVALID;
  wire [31:0]axi_dma_2_M_AXI_SG_RDATA;
  wire axi_dma_2_M_AXI_SG_RLAST;
  wire axi_dma_2_M_AXI_SG_RREADY;
  wire [1:0]axi_dma_2_M_AXI_SG_RRESP;
  wire axi_dma_2_M_AXI_SG_RVALID;
  wire [31:0]axi_dma_2_M_AXI_SG_WDATA;
  wire axi_dma_2_M_AXI_SG_WLAST;
  wire axi_dma_2_M_AXI_SG_WREADY;
  wire [3:0]axi_dma_2_M_AXI_SG_WSTRB;
  wire axi_dma_2_M_AXI_SG_WVALID;
  wire axi_dma_2_mm2s_introut;
  wire axi_dma_2_s2mm_introut;
  wire [31:0]axi_dma_3_M_AXIS_MM2S_TDATA;
  wire [4:0]axi_dma_3_M_AXIS_MM2S_TDEST;
  wire [4:0]axi_dma_3_M_AXIS_MM2S_TID;
  wire [3:0]axi_dma_3_M_AXIS_MM2S_TKEEP;
  wire axi_dma_3_M_AXIS_MM2S_TLAST;
  wire [0:0]axi_dma_3_M_AXIS_MM2S_TREADY;
  wire [3:0]axi_dma_3_M_AXIS_MM2S_TUSER;
  wire axi_dma_3_M_AXIS_MM2S_TVALID;
  wire [31:0]axi_dma_3_M_AXI_MM2S_ARADDR;
  wire [1:0]axi_dma_3_M_AXI_MM2S_ARBURST;
  wire [3:0]axi_dma_3_M_AXI_MM2S_ARCACHE;
  wire [7:0]axi_dma_3_M_AXI_MM2S_ARLEN;
  wire [2:0]axi_dma_3_M_AXI_MM2S_ARPROT;
  wire axi_dma_3_M_AXI_MM2S_ARREADY;
  wire [2:0]axi_dma_3_M_AXI_MM2S_ARSIZE;
  wire axi_dma_3_M_AXI_MM2S_ARVALID;
  wire [31:0]axi_dma_3_M_AXI_MM2S_RDATA;
  wire axi_dma_3_M_AXI_MM2S_RLAST;
  wire axi_dma_3_M_AXI_MM2S_RREADY;
  wire [1:0]axi_dma_3_M_AXI_MM2S_RRESP;
  wire axi_dma_3_M_AXI_MM2S_RVALID;
  wire [31:0]axi_dma_3_M_AXI_S2MM_AWADDR;
  wire [1:0]axi_dma_3_M_AXI_S2MM_AWBURST;
  wire [3:0]axi_dma_3_M_AXI_S2MM_AWCACHE;
  wire [7:0]axi_dma_3_M_AXI_S2MM_AWLEN;
  wire [2:0]axi_dma_3_M_AXI_S2MM_AWPROT;
  wire axi_dma_3_M_AXI_S2MM_AWREADY;
  wire [2:0]axi_dma_3_M_AXI_S2MM_AWSIZE;
  wire axi_dma_3_M_AXI_S2MM_AWVALID;
  wire axi_dma_3_M_AXI_S2MM_BREADY;
  wire [1:0]axi_dma_3_M_AXI_S2MM_BRESP;
  wire axi_dma_3_M_AXI_S2MM_BVALID;
  wire [31:0]axi_dma_3_M_AXI_S2MM_WDATA;
  wire axi_dma_3_M_AXI_S2MM_WLAST;
  wire axi_dma_3_M_AXI_S2MM_WREADY;
  wire [3:0]axi_dma_3_M_AXI_S2MM_WSTRB;
  wire axi_dma_3_M_AXI_S2MM_WVALID;
  wire [31:0]axi_dma_3_M_AXI_SG_ARADDR;
  wire [1:0]axi_dma_3_M_AXI_SG_ARBURST;
  wire [3:0]axi_dma_3_M_AXI_SG_ARCACHE;
  wire [7:0]axi_dma_3_M_AXI_SG_ARLEN;
  wire [2:0]axi_dma_3_M_AXI_SG_ARPROT;
  wire axi_dma_3_M_AXI_SG_ARREADY;
  wire [2:0]axi_dma_3_M_AXI_SG_ARSIZE;
  wire axi_dma_3_M_AXI_SG_ARVALID;
  wire [31:0]axi_dma_3_M_AXI_SG_AWADDR;
  wire [1:0]axi_dma_3_M_AXI_SG_AWBURST;
  wire [3:0]axi_dma_3_M_AXI_SG_AWCACHE;
  wire [7:0]axi_dma_3_M_AXI_SG_AWLEN;
  wire [2:0]axi_dma_3_M_AXI_SG_AWPROT;
  wire axi_dma_3_M_AXI_SG_AWREADY;
  wire [2:0]axi_dma_3_M_AXI_SG_AWSIZE;
  wire axi_dma_3_M_AXI_SG_AWVALID;
  wire axi_dma_3_M_AXI_SG_BREADY;
  wire [1:0]axi_dma_3_M_AXI_SG_BRESP;
  wire axi_dma_3_M_AXI_SG_BVALID;
  wire [31:0]axi_dma_3_M_AXI_SG_RDATA;
  wire axi_dma_3_M_AXI_SG_RLAST;
  wire axi_dma_3_M_AXI_SG_RREADY;
  wire [1:0]axi_dma_3_M_AXI_SG_RRESP;
  wire axi_dma_3_M_AXI_SG_RVALID;
  wire [31:0]axi_dma_3_M_AXI_SG_WDATA;
  wire axi_dma_3_M_AXI_SG_WLAST;
  wire axi_dma_3_M_AXI_SG_WREADY;
  wire [3:0]axi_dma_3_M_AXI_SG_WSTRB;
  wire axi_dma_3_M_AXI_SG_WVALID;
  wire axi_dma_3_mm2s_introut;
  wire axi_dma_3_s2mm_introut;
  wire [0:1]axi_intc_0_interrupt_ACK;
  wire [31:0]axi_intc_0_interrupt_ADDRESS;
  wire axi_intc_0_interrupt_INTERRUPT;
  wire [31:0]axi_mem_intercon_M00_AXI_ARADDR;
  wire [1:0]axi_mem_intercon_M00_AXI_ARBURST;
  wire [3:0]axi_mem_intercon_M00_AXI_ARCACHE;
  wire [3:0]axi_mem_intercon_M00_AXI_ARID;
  wire [7:0]axi_mem_intercon_M00_AXI_ARLEN;
  wire [0:0]axi_mem_intercon_M00_AXI_ARLOCK;
  wire [2:0]axi_mem_intercon_M00_AXI_ARPROT;
  wire [3:0]axi_mem_intercon_M00_AXI_ARQOS;
  wire axi_mem_intercon_M00_AXI_ARREADY;
  wire [2:0]axi_mem_intercon_M00_AXI_ARSIZE;
  wire [0:0]axi_mem_intercon_M00_AXI_ARVALID;
  wire [31:0]axi_mem_intercon_M00_AXI_AWADDR;
  wire [1:0]axi_mem_intercon_M00_AXI_AWBURST;
  wire [3:0]axi_mem_intercon_M00_AXI_AWCACHE;
  wire [3:0]axi_mem_intercon_M00_AXI_AWID;
  wire [7:0]axi_mem_intercon_M00_AXI_AWLEN;
  wire [0:0]axi_mem_intercon_M00_AXI_AWLOCK;
  wire [2:0]axi_mem_intercon_M00_AXI_AWPROT;
  wire [3:0]axi_mem_intercon_M00_AXI_AWQOS;
  wire axi_mem_intercon_M00_AXI_AWREADY;
  wire [2:0]axi_mem_intercon_M00_AXI_AWSIZE;
  wire [0:0]axi_mem_intercon_M00_AXI_AWVALID;
  wire [3:0]axi_mem_intercon_M00_AXI_BID;
  wire [0:0]axi_mem_intercon_M00_AXI_BREADY;
  wire [1:0]axi_mem_intercon_M00_AXI_BRESP;
  wire axi_mem_intercon_M00_AXI_BVALID;
  wire [511:0]axi_mem_intercon_M00_AXI_RDATA;
  wire [3:0]axi_mem_intercon_M00_AXI_RID;
  wire axi_mem_intercon_M00_AXI_RLAST;
  wire [0:0]axi_mem_intercon_M00_AXI_RREADY;
  wire [1:0]axi_mem_intercon_M00_AXI_RRESP;
  wire axi_mem_intercon_M00_AXI_RVALID;
  wire [511:0]axi_mem_intercon_M00_AXI_WDATA;
  wire [0:0]axi_mem_intercon_M00_AXI_WLAST;
  wire axi_mem_intercon_M00_AXI_WREADY;
  wire [63:0]axi_mem_intercon_M00_AXI_WSTRB;
  wire [0:0]axi_mem_intercon_M00_AXI_WVALID;
  wire [31:0]axis_data_fifo_0_M_AXIS_TDATA;
  wire [4:0]axis_data_fifo_0_M_AXIS_TDEST;
  wire [4:0]axis_data_fifo_0_M_AXIS_TID;
  wire [3:0]axis_data_fifo_0_M_AXIS_TKEEP;
  wire axis_data_fifo_0_M_AXIS_TLAST;
  wire axis_data_fifo_0_M_AXIS_TREADY;
  wire [3:0]axis_data_fifo_0_M_AXIS_TUSER;
  wire axis_data_fifo_0_M_AXIS_TVALID;
  wire [31:0]axis_data_fifo_1_M_AXIS_TDATA;
  wire [4:0]axis_data_fifo_1_M_AXIS_TDEST;
  wire [4:0]axis_data_fifo_1_M_AXIS_TID;
  wire [3:0]axis_data_fifo_1_M_AXIS_TKEEP;
  wire axis_data_fifo_1_M_AXIS_TLAST;
  wire axis_data_fifo_1_M_AXIS_TREADY;
  wire [3:0]axis_data_fifo_1_M_AXIS_TUSER;
  wire axis_data_fifo_1_M_AXIS_TVALID;
  wire [31:0]axis_data_fifo_2_M_AXIS_TDATA;
  wire [4:0]axis_data_fifo_2_M_AXIS_TDEST;
  wire [4:0]axis_data_fifo_2_M_AXIS_TID;
  wire [3:0]axis_data_fifo_2_M_AXIS_TKEEP;
  wire axis_data_fifo_2_M_AXIS_TLAST;
  wire axis_data_fifo_2_M_AXIS_TREADY;
  wire [3:0]axis_data_fifo_2_M_AXIS_TUSER;
  wire axis_data_fifo_2_M_AXIS_TVALID;
  wire [31:0]axis_data_fifo_3_M_AXIS_TDATA;
  wire [4:0]axis_data_fifo_3_M_AXIS_TDEST;
  wire [4:0]axis_data_fifo_3_M_AXIS_TID;
  wire [3:0]axis_data_fifo_3_M_AXIS_TKEEP;
  wire axis_data_fifo_3_M_AXIS_TLAST;
  wire axis_data_fifo_3_M_AXIS_TREADY;
  wire [3:0]axis_data_fifo_3_M_AXIS_TUSER;
  wire axis_data_fifo_3_M_AXIS_TVALID;
  wire [31:0]axis_interconnect_0_M00_AXIS_TDATA;
  wire [4:0]axis_interconnect_0_M00_AXIS_TDEST;
  wire [4:0]axis_interconnect_0_M00_AXIS_TID;
  wire [3:0]axis_interconnect_0_M00_AXIS_TKEEP;
  wire [0:0]axis_interconnect_0_M00_AXIS_TLAST;
  wire axis_interconnect_0_M00_AXIS_TREADY;
  wire [3:0]axis_interconnect_0_M00_AXIS_TSTRB;
  wire [3:0]axis_interconnect_0_M00_AXIS_TUSER;
  wire axis_interconnect_0_M00_AXIS_TVALID;
  wire [31:0]axis_interconnect_0_M01_AXIS_TDATA;
  wire [4:0]axis_interconnect_0_M01_AXIS_TDEST;
  wire [4:0]axis_interconnect_0_M01_AXIS_TID;
  wire [3:0]axis_interconnect_0_M01_AXIS_TKEEP;
  wire [0:0]axis_interconnect_0_M01_AXIS_TLAST;
  wire axis_interconnect_0_M01_AXIS_TREADY;
  wire [3:0]axis_interconnect_0_M01_AXIS_TSTRB;
  wire [3:0]axis_interconnect_0_M01_AXIS_TUSER;
  wire axis_interconnect_0_M01_AXIS_TVALID;
  wire [31:0]axis_interconnect_0_M02_AXIS_TDATA;
  wire [4:0]axis_interconnect_0_M02_AXIS_TDEST;
  wire [4:0]axis_interconnect_0_M02_AXIS_TID;
  wire [3:0]axis_interconnect_0_M02_AXIS_TKEEP;
  wire [0:0]axis_interconnect_0_M02_AXIS_TLAST;
  wire axis_interconnect_0_M02_AXIS_TREADY;
  wire [3:0]axis_interconnect_0_M02_AXIS_TSTRB;
  wire [3:0]axis_interconnect_0_M02_AXIS_TUSER;
  wire axis_interconnect_0_M02_AXIS_TVALID;
  wire [31:0]axis_interconnect_1_M00_AXIS_TDATA;
  wire [4:0]axis_interconnect_1_M00_AXIS_TDEST;
  wire [4:0]axis_interconnect_1_M00_AXIS_TID;
  wire [3:0]axis_interconnect_1_M00_AXIS_TKEEP;
  wire [0:0]axis_interconnect_1_M00_AXIS_TLAST;
  wire axis_interconnect_1_M00_AXIS_TREADY;
  wire [3:0]axis_interconnect_1_M00_AXIS_TSTRB;
  wire [3:0]axis_interconnect_1_M00_AXIS_TUSER;
  wire axis_interconnect_1_M00_AXIS_TVALID;
  wire [31:0]axis_interconnect_1_M01_AXIS_TDATA;
  wire [4:0]axis_interconnect_1_M01_AXIS_TDEST;
  wire [4:0]axis_interconnect_1_M01_AXIS_TID;
  wire [3:0]axis_interconnect_1_M01_AXIS_TKEEP;
  wire [0:0]axis_interconnect_1_M01_AXIS_TLAST;
  wire axis_interconnect_1_M01_AXIS_TREADY;
  wire [3:0]axis_interconnect_1_M01_AXIS_TSTRB;
  wire [3:0]axis_interconnect_1_M01_AXIS_TUSER;
  wire axis_interconnect_1_M01_AXIS_TVALID;
  wire [31:0]axis_interconnect_1_M02_AXIS_TDATA;
  wire [4:0]axis_interconnect_1_M02_AXIS_TDEST;
  wire [4:0]axis_interconnect_1_M02_AXIS_TID;
  wire [3:0]axis_interconnect_1_M02_AXIS_TKEEP;
  wire [0:0]axis_interconnect_1_M02_AXIS_TLAST;
  wire axis_interconnect_1_M02_AXIS_TREADY;
  wire [3:0]axis_interconnect_1_M02_AXIS_TSTRB;
  wire [3:0]axis_interconnect_1_M02_AXIS_TUSER;
  wire axis_interconnect_1_M02_AXIS_TVALID;
  wire [31:0]axis_interconnect_2_M00_AXIS_TDATA;
  wire [4:0]axis_interconnect_2_M00_AXIS_TDEST;
  wire [4:0]axis_interconnect_2_M00_AXIS_TID;
  wire [3:0]axis_interconnect_2_M00_AXIS_TKEEP;
  wire [0:0]axis_interconnect_2_M00_AXIS_TLAST;
  wire axis_interconnect_2_M00_AXIS_TREADY;
  wire [3:0]axis_interconnect_2_M00_AXIS_TSTRB;
  wire [3:0]axis_interconnect_2_M00_AXIS_TUSER;
  wire axis_interconnect_2_M00_AXIS_TVALID;
  wire [31:0]axis_interconnect_2_M01_AXIS_TDATA;
  wire [4:0]axis_interconnect_2_M01_AXIS_TDEST;
  wire [4:0]axis_interconnect_2_M01_AXIS_TID;
  wire [3:0]axis_interconnect_2_M01_AXIS_TKEEP;
  wire [0:0]axis_interconnect_2_M01_AXIS_TLAST;
  wire axis_interconnect_2_M01_AXIS_TREADY;
  wire [3:0]axis_interconnect_2_M01_AXIS_TSTRB;
  wire [3:0]axis_interconnect_2_M01_AXIS_TUSER;
  wire axis_interconnect_2_M01_AXIS_TVALID;
  wire [31:0]axis_interconnect_2_M02_AXIS_TDATA;
  wire [4:0]axis_interconnect_2_M02_AXIS_TDEST;
  wire [4:0]axis_interconnect_2_M02_AXIS_TID;
  wire [3:0]axis_interconnect_2_M02_AXIS_TKEEP;
  wire [0:0]axis_interconnect_2_M02_AXIS_TLAST;
  wire axis_interconnect_2_M02_AXIS_TREADY;
  wire [3:0]axis_interconnect_2_M02_AXIS_TSTRB;
  wire [3:0]axis_interconnect_2_M02_AXIS_TUSER;
  wire axis_interconnect_2_M02_AXIS_TVALID;
  wire [31:0]axis_interconnect_3_M00_AXIS_TDATA;
  wire [4:0]axis_interconnect_3_M00_AXIS_TDEST;
  wire [4:0]axis_interconnect_3_M00_AXIS_TID;
  wire [3:0]axis_interconnect_3_M00_AXIS_TKEEP;
  wire [0:0]axis_interconnect_3_M00_AXIS_TLAST;
  wire axis_interconnect_3_M00_AXIS_TREADY;
  wire [3:0]axis_interconnect_3_M00_AXIS_TSTRB;
  wire [3:0]axis_interconnect_3_M00_AXIS_TUSER;
  wire axis_interconnect_3_M00_AXIS_TVALID;
  wire [31:0]axis_interconnect_3_M01_AXIS_TDATA;
  wire [4:0]axis_interconnect_3_M01_AXIS_TDEST;
  wire [4:0]axis_interconnect_3_M01_AXIS_TID;
  wire [3:0]axis_interconnect_3_M01_AXIS_TKEEP;
  wire [0:0]axis_interconnect_3_M01_AXIS_TLAST;
  wire axis_interconnect_3_M01_AXIS_TREADY;
  wire [3:0]axis_interconnect_3_M01_AXIS_TSTRB;
  wire [3:0]axis_interconnect_3_M01_AXIS_TUSER;
  wire axis_interconnect_3_M01_AXIS_TVALID;
  wire [31:0]axis_interconnect_3_M02_AXIS_TDATA;
  wire [4:0]axis_interconnect_3_M02_AXIS_TDEST;
  wire [4:0]axis_interconnect_3_M02_AXIS_TID;
  wire [3:0]axis_interconnect_3_M02_AXIS_TKEEP;
  wire [0:0]axis_interconnect_3_M02_AXIS_TLAST;
  wire axis_interconnect_3_M02_AXIS_TREADY;
  wire [3:0]axis_interconnect_3_M02_AXIS_TSTRB;
  wire [3:0]axis_interconnect_3_M02_AXIS_TUSER;
  wire axis_interconnect_3_M02_AXIS_TVALID;
  wire mdm_1_debug_sys_rst;
  wire microblaze_0_Clk;
  wire [31:0]microblaze_0_M_AXI_DC_ARADDR;
  wire [1:0]microblaze_0_M_AXI_DC_ARBURST;
  wire [3:0]microblaze_0_M_AXI_DC_ARCACHE;
  wire [7:0]microblaze_0_M_AXI_DC_ARLEN;
  wire microblaze_0_M_AXI_DC_ARLOCK;
  wire [2:0]microblaze_0_M_AXI_DC_ARPROT;
  wire [3:0]microblaze_0_M_AXI_DC_ARQOS;
  wire microblaze_0_M_AXI_DC_ARREADY;
  wire [2:0]microblaze_0_M_AXI_DC_ARSIZE;
  wire microblaze_0_M_AXI_DC_ARVALID;
  wire [31:0]microblaze_0_M_AXI_DC_AWADDR;
  wire [1:0]microblaze_0_M_AXI_DC_AWBURST;
  wire [3:0]microblaze_0_M_AXI_DC_AWCACHE;
  wire [7:0]microblaze_0_M_AXI_DC_AWLEN;
  wire microblaze_0_M_AXI_DC_AWLOCK;
  wire [2:0]microblaze_0_M_AXI_DC_AWPROT;
  wire [3:0]microblaze_0_M_AXI_DC_AWQOS;
  wire microblaze_0_M_AXI_DC_AWREADY;
  wire [2:0]microblaze_0_M_AXI_DC_AWSIZE;
  wire microblaze_0_M_AXI_DC_AWVALID;
  wire microblaze_0_M_AXI_DC_BREADY;
  wire [1:0]microblaze_0_M_AXI_DC_BRESP;
  wire microblaze_0_M_AXI_DC_BVALID;
  wire [31:0]microblaze_0_M_AXI_DC_RDATA;
  wire microblaze_0_M_AXI_DC_RLAST;
  wire microblaze_0_M_AXI_DC_RREADY;
  wire [1:0]microblaze_0_M_AXI_DC_RRESP;
  wire microblaze_0_M_AXI_DC_RVALID;
  wire [31:0]microblaze_0_M_AXI_DC_WDATA;
  wire microblaze_0_M_AXI_DC_WLAST;
  wire microblaze_0_M_AXI_DC_WREADY;
  wire [3:0]microblaze_0_M_AXI_DC_WSTRB;
  wire microblaze_0_M_AXI_DC_WVALID;
  wire [31:0]microblaze_0_M_AXI_DP_ARADDR;
  wire [2:0]microblaze_0_M_AXI_DP_ARPROT;
  wire microblaze_0_M_AXI_DP_ARREADY;
  wire microblaze_0_M_AXI_DP_ARVALID;
  wire [31:0]microblaze_0_M_AXI_DP_AWADDR;
  wire [2:0]microblaze_0_M_AXI_DP_AWPROT;
  wire microblaze_0_M_AXI_DP_AWREADY;
  wire microblaze_0_M_AXI_DP_AWVALID;
  wire microblaze_0_M_AXI_DP_BREADY;
  wire [1:0]microblaze_0_M_AXI_DP_BRESP;
  wire microblaze_0_M_AXI_DP_BVALID;
  wire [31:0]microblaze_0_M_AXI_DP_RDATA;
  wire microblaze_0_M_AXI_DP_RREADY;
  wire [1:0]microblaze_0_M_AXI_DP_RRESP;
  wire microblaze_0_M_AXI_DP_RVALID;
  wire [31:0]microblaze_0_M_AXI_DP_WDATA;
  wire microblaze_0_M_AXI_DP_WREADY;
  wire [3:0]microblaze_0_M_AXI_DP_WSTRB;
  wire microblaze_0_M_AXI_DP_WVALID;
  wire [31:0]microblaze_0_M_AXI_IC_ARADDR;
  wire [1:0]microblaze_0_M_AXI_IC_ARBURST;
  wire [3:0]microblaze_0_M_AXI_IC_ARCACHE;
  wire [7:0]microblaze_0_M_AXI_IC_ARLEN;
  wire microblaze_0_M_AXI_IC_ARLOCK;
  wire [2:0]microblaze_0_M_AXI_IC_ARPROT;
  wire [3:0]microblaze_0_M_AXI_IC_ARQOS;
  wire microblaze_0_M_AXI_IC_ARREADY;
  wire [2:0]microblaze_0_M_AXI_IC_ARSIZE;
  wire microblaze_0_M_AXI_IC_ARVALID;
  wire [31:0]microblaze_0_M_AXI_IC_RDATA;
  wire microblaze_0_M_AXI_IC_RLAST;
  wire microblaze_0_M_AXI_IC_RREADY;
  wire [1:0]microblaze_0_M_AXI_IC_RRESP;
  wire microblaze_0_M_AXI_IC_RVALID;
  wire [31:0]microblaze_0_axi_periph_M00_AXI_ARADDR;
  wire microblaze_0_axi_periph_M00_AXI_ARREADY;
  wire [0:0]microblaze_0_axi_periph_M00_AXI_ARVALID;
  wire [31:0]microblaze_0_axi_periph_M00_AXI_AWADDR;
  wire microblaze_0_axi_periph_M00_AXI_AWREADY;
  wire [0:0]microblaze_0_axi_periph_M00_AXI_AWVALID;
  wire [0:0]microblaze_0_axi_periph_M00_AXI_BREADY;
  wire [1:0]microblaze_0_axi_periph_M00_AXI_BRESP;
  wire microblaze_0_axi_periph_M00_AXI_BVALID;
  wire [31:0]microblaze_0_axi_periph_M00_AXI_RDATA;
  wire [0:0]microblaze_0_axi_periph_M00_AXI_RREADY;
  wire [1:0]microblaze_0_axi_periph_M00_AXI_RRESP;
  wire microblaze_0_axi_periph_M00_AXI_RVALID;
  wire [31:0]microblaze_0_axi_periph_M00_AXI_WDATA;
  wire microblaze_0_axi_periph_M00_AXI_WREADY;
  wire [3:0]microblaze_0_axi_periph_M00_AXI_WSTRB;
  wire [0:0]microblaze_0_axi_periph_M00_AXI_WVALID;
  wire [31:0]microblaze_0_axi_periph_M01_AXI_ARADDR;
  wire microblaze_0_axi_periph_M01_AXI_ARREADY;
  wire microblaze_0_axi_periph_M01_AXI_ARVALID;
  wire [31:0]microblaze_0_axi_periph_M01_AXI_AWADDR;
  wire microblaze_0_axi_periph_M01_AXI_AWREADY;
  wire microblaze_0_axi_periph_M01_AXI_AWVALID;
  wire microblaze_0_axi_periph_M01_AXI_BREADY;
  wire [1:0]microblaze_0_axi_periph_M01_AXI_BRESP;
  wire microblaze_0_axi_periph_M01_AXI_BVALID;
  wire [31:0]microblaze_0_axi_periph_M01_AXI_RDATA;
  wire microblaze_0_axi_periph_M01_AXI_RREADY;
  wire [1:0]microblaze_0_axi_periph_M01_AXI_RRESP;
  wire microblaze_0_axi_periph_M01_AXI_RVALID;
  wire [31:0]microblaze_0_axi_periph_M01_AXI_WDATA;
  wire microblaze_0_axi_periph_M01_AXI_WREADY;
  wire [3:0]microblaze_0_axi_periph_M01_AXI_WSTRB;
  wire microblaze_0_axi_periph_M01_AXI_WVALID;
  wire [31:0]microblaze_0_axi_periph_M02_AXI_ARADDR;
  wire microblaze_0_axi_periph_M02_AXI_ARREADY;
  wire microblaze_0_axi_periph_M02_AXI_ARVALID;
  wire [31:0]microblaze_0_axi_periph_M02_AXI_AWADDR;
  wire microblaze_0_axi_periph_M02_AXI_AWREADY;
  wire microblaze_0_axi_periph_M02_AXI_AWVALID;
  wire microblaze_0_axi_periph_M02_AXI_BREADY;
  wire [1:0]microblaze_0_axi_periph_M02_AXI_BRESP;
  wire microblaze_0_axi_periph_M02_AXI_BVALID;
  wire [31:0]microblaze_0_axi_periph_M02_AXI_RDATA;
  wire microblaze_0_axi_periph_M02_AXI_RREADY;
  wire [1:0]microblaze_0_axi_periph_M02_AXI_RRESP;
  wire microblaze_0_axi_periph_M02_AXI_RVALID;
  wire [31:0]microblaze_0_axi_periph_M02_AXI_WDATA;
  wire microblaze_0_axi_periph_M02_AXI_WREADY;
  wire [3:0]microblaze_0_axi_periph_M02_AXI_WSTRB;
  wire microblaze_0_axi_periph_M02_AXI_WVALID;
  wire [31:0]microblaze_0_axi_periph_M03_AXI_ARADDR;
  wire microblaze_0_axi_periph_M03_AXI_ARREADY;
  wire microblaze_0_axi_periph_M03_AXI_ARVALID;
  wire [31:0]microblaze_0_axi_periph_M03_AXI_AWADDR;
  wire microblaze_0_axi_periph_M03_AXI_AWREADY;
  wire microblaze_0_axi_periph_M03_AXI_AWVALID;
  wire microblaze_0_axi_periph_M03_AXI_BREADY;
  wire [1:0]microblaze_0_axi_periph_M03_AXI_BRESP;
  wire microblaze_0_axi_periph_M03_AXI_BVALID;
  wire [31:0]microblaze_0_axi_periph_M03_AXI_RDATA;
  wire microblaze_0_axi_periph_M03_AXI_RREADY;
  wire [1:0]microblaze_0_axi_periph_M03_AXI_RRESP;
  wire microblaze_0_axi_periph_M03_AXI_RVALID;
  wire [31:0]microblaze_0_axi_periph_M03_AXI_WDATA;
  wire microblaze_0_axi_periph_M03_AXI_WREADY;
  wire microblaze_0_axi_periph_M03_AXI_WVALID;
  wire [31:0]microblaze_0_axi_periph_M04_AXI_ARADDR;
  wire microblaze_0_axi_periph_M04_AXI_ARREADY;
  wire microblaze_0_axi_periph_M04_AXI_ARVALID;
  wire [31:0]microblaze_0_axi_periph_M04_AXI_AWADDR;
  wire microblaze_0_axi_periph_M04_AXI_AWREADY;
  wire microblaze_0_axi_periph_M04_AXI_AWVALID;
  wire microblaze_0_axi_periph_M04_AXI_BREADY;
  wire [1:0]microblaze_0_axi_periph_M04_AXI_BRESP;
  wire microblaze_0_axi_periph_M04_AXI_BVALID;
  wire [31:0]microblaze_0_axi_periph_M04_AXI_RDATA;
  wire microblaze_0_axi_periph_M04_AXI_RREADY;
  wire [1:0]microblaze_0_axi_periph_M04_AXI_RRESP;
  wire microblaze_0_axi_periph_M04_AXI_RVALID;
  wire [31:0]microblaze_0_axi_periph_M04_AXI_WDATA;
  wire microblaze_0_axi_periph_M04_AXI_WREADY;
  wire microblaze_0_axi_periph_M04_AXI_WVALID;
  wire [31:0]microblaze_0_axi_periph_M05_AXI_ARADDR;
  wire microblaze_0_axi_periph_M05_AXI_ARREADY;
  wire microblaze_0_axi_periph_M05_AXI_ARVALID;
  wire [31:0]microblaze_0_axi_periph_M05_AXI_AWADDR;
  wire microblaze_0_axi_periph_M05_AXI_AWREADY;
  wire microblaze_0_axi_periph_M05_AXI_AWVALID;
  wire microblaze_0_axi_periph_M05_AXI_BREADY;
  wire [1:0]microblaze_0_axi_periph_M05_AXI_BRESP;
  wire microblaze_0_axi_periph_M05_AXI_BVALID;
  wire [31:0]microblaze_0_axi_periph_M05_AXI_RDATA;
  wire microblaze_0_axi_periph_M05_AXI_RREADY;
  wire [1:0]microblaze_0_axi_periph_M05_AXI_RRESP;
  wire microblaze_0_axi_periph_M05_AXI_RVALID;
  wire [31:0]microblaze_0_axi_periph_M05_AXI_WDATA;
  wire microblaze_0_axi_periph_M05_AXI_WREADY;
  wire microblaze_0_axi_periph_M05_AXI_WVALID;
  wire [31:0]microblaze_0_axi_periph_M06_AXI_ARADDR;
  wire microblaze_0_axi_periph_M06_AXI_ARREADY;
  wire microblaze_0_axi_periph_M06_AXI_ARVALID;
  wire [31:0]microblaze_0_axi_periph_M06_AXI_AWADDR;
  wire microblaze_0_axi_periph_M06_AXI_AWREADY;
  wire microblaze_0_axi_periph_M06_AXI_AWVALID;
  wire microblaze_0_axi_periph_M06_AXI_BREADY;
  wire [1:0]microblaze_0_axi_periph_M06_AXI_BRESP;
  wire microblaze_0_axi_periph_M06_AXI_BVALID;
  wire [31:0]microblaze_0_axi_periph_M06_AXI_RDATA;
  wire microblaze_0_axi_periph_M06_AXI_RREADY;
  wire [1:0]microblaze_0_axi_periph_M06_AXI_RRESP;
  wire microblaze_0_axi_periph_M06_AXI_RVALID;
  wire [31:0]microblaze_0_axi_periph_M06_AXI_WDATA;
  wire microblaze_0_axi_periph_M06_AXI_WREADY;
  wire microblaze_0_axi_periph_M06_AXI_WVALID;
  wire microblaze_0_debug_CAPTURE;
  wire microblaze_0_debug_CLK;
  wire [0:7]microblaze_0_debug_REG_EN;
  wire microblaze_0_debug_RST;
  wire microblaze_0_debug_SHIFT;
  wire microblaze_0_debug_TDI;
  wire microblaze_0_debug_TDO;
  wire microblaze_0_debug_UPDATE;
  wire [0:31]microblaze_0_dlmb_1_ABUS;
  wire microblaze_0_dlmb_1_ADDRSTROBE;
  wire [0:3]microblaze_0_dlmb_1_BE;
  wire microblaze_0_dlmb_1_CE;
  wire [0:31]microblaze_0_dlmb_1_READDBUS;
  wire microblaze_0_dlmb_1_READSTROBE;
  wire microblaze_0_dlmb_1_READY;
  wire microblaze_0_dlmb_1_UE;
  wire microblaze_0_dlmb_1_WAIT;
  wire [0:31]microblaze_0_dlmb_1_WRITEDBUS;
  wire microblaze_0_dlmb_1_WRITESTROBE;
  wire [0:31]microblaze_0_ilmb_1_ABUS;
  wire microblaze_0_ilmb_1_ADDRSTROBE;
  wire microblaze_0_ilmb_1_CE;
  wire [0:31]microblaze_0_ilmb_1_READDBUS;
  wire microblaze_0_ilmb_1_READSTROBE;
  wire microblaze_0_ilmb_1_READY;
  wire microblaze_0_ilmb_1_UE;
  wire microblaze_0_ilmb_1_WAIT;
  wire [13:0]mig_7series_0_DDR3_ADDR;
  wire [2:0]mig_7series_0_DDR3_BA;
  wire mig_7series_0_DDR3_CAS_N;
  wire [0:0]mig_7series_0_DDR3_CKE;
  wire [0:0]mig_7series_0_DDR3_CK_N;
  wire [0:0]mig_7series_0_DDR3_CK_P;
  wire [0:0]mig_7series_0_DDR3_CS_N;
  wire [7:0]mig_7series_0_DDR3_DM;
  wire [63:0]mig_7series_0_DDR3_DQ;
  wire [7:0]mig_7series_0_DDR3_DQS_N;
  wire [7:0]mig_7series_0_DDR3_DQS_P;
  wire [0:0]mig_7series_0_DDR3_ODT;
  wire mig_7series_0_DDR3_RAS_N;
  wire mig_7series_0_DDR3_RESET_N;
  wire mig_7series_0_DDR3_WE_N;
  wire mig_7series_0_mmcm_locked;
  wire mig_7series_0_ui_clk_sync_rst;
  wire [31:0]pcc_stream_0_DMA_pcc_TDATA;
  wire [4:0]pcc_stream_0_DMA_pcc_TDEST;
  wire [4:0]pcc_stream_0_DMA_pcc_TID;
  wire [3:0]pcc_stream_0_DMA_pcc_TKEEP;
  wire [0:0]pcc_stream_0_DMA_pcc_TLAST;
  wire pcc_stream_0_DMA_pcc_TREADY;
  wire [3:0]pcc_stream_0_DMA_pcc_TSTRB;
  wire [3:0]pcc_stream_0_DMA_pcc_TUSER;
  wire pcc_stream_0_DMA_pcc_TVALID;
  wire [31:0]pcc_stream_1_DMA_pcc_TDATA;
  wire [4:0]pcc_stream_1_DMA_pcc_TDEST;
  wire [4:0]pcc_stream_1_DMA_pcc_TID;
  wire [3:0]pcc_stream_1_DMA_pcc_TKEEP;
  wire [0:0]pcc_stream_1_DMA_pcc_TLAST;
  wire pcc_stream_1_DMA_pcc_TREADY;
  wire [3:0]pcc_stream_1_DMA_pcc_TSTRB;
  wire [3:0]pcc_stream_1_DMA_pcc_TUSER;
  wire pcc_stream_1_DMA_pcc_TVALID;
  wire [31:0]pcc_stream_2_DMA_pcc_TDATA;
  wire [4:0]pcc_stream_2_DMA_pcc_TDEST;
  wire [4:0]pcc_stream_2_DMA_pcc_TID;
  wire [3:0]pcc_stream_2_DMA_pcc_TKEEP;
  wire [0:0]pcc_stream_2_DMA_pcc_TLAST;
  wire pcc_stream_2_DMA_pcc_TREADY;
  wire [3:0]pcc_stream_2_DMA_pcc_TSTRB;
  wire [3:0]pcc_stream_2_DMA_pcc_TUSER;
  wire pcc_stream_2_DMA_pcc_TVALID;
  wire [31:0]pcc_stream_3_DMA_pcc_TDATA;
  wire [4:0]pcc_stream_3_DMA_pcc_TDEST;
  wire [4:0]pcc_stream_3_DMA_pcc_TID;
  wire [3:0]pcc_stream_3_DMA_pcc_TKEEP;
  wire [0:0]pcc_stream_3_DMA_pcc_TLAST;
  wire pcc_stream_3_DMA_pcc_TREADY;
  wire [3:0]pcc_stream_3_DMA_pcc_TSTRB;
  wire [3:0]pcc_stream_3_DMA_pcc_TUSER;
  wire pcc_stream_3_DMA_pcc_TVALID;
  wire reset_1;
  wire [0:0]rst_mig_7series_0_100M_bus_struct_reset;
  wire [0:0]rst_mig_7series_0_100M_interconnect_aresetn;
  wire rst_mig_7series_0_100M_mb_reset;
  wire [0:0]rst_mig_7series_0_100M_peripheral_aresetn;
  wire sys_diff_clock_1_CLK_N;
  wire sys_diff_clock_1_CLK_P;
  wire [7:0]xlconcat_0_dout;

  assign ddr3_sdram_addr[13:0] = mig_7series_0_DDR3_ADDR;
  assign ddr3_sdram_ba[2:0] = mig_7series_0_DDR3_BA;
  assign ddr3_sdram_cas_n = mig_7series_0_DDR3_CAS_N;
  assign ddr3_sdram_ck_n[0] = mig_7series_0_DDR3_CK_N;
  assign ddr3_sdram_ck_p[0] = mig_7series_0_DDR3_CK_P;
  assign ddr3_sdram_cke[0] = mig_7series_0_DDR3_CKE;
  assign ddr3_sdram_cs_n[0] = mig_7series_0_DDR3_CS_N;
  assign ddr3_sdram_dm[7:0] = mig_7series_0_DDR3_DM;
  assign ddr3_sdram_odt[0] = mig_7series_0_DDR3_ODT;
  assign ddr3_sdram_ras_n = mig_7series_0_DDR3_RAS_N;
  assign ddr3_sdram_reset_n = mig_7series_0_DDR3_RESET_N;
  assign ddr3_sdram_we_n = mig_7series_0_DDR3_WE_N;
  assign reset_1 = reset;
  assign sys_diff_clock_1_CLK_N = sys_diff_clock_clk_n;
  assign sys_diff_clock_1_CLK_P = sys_diff_clock_clk_p;
  design_1_axi_dma_0_0 axi_dma_0
       (.axi_resetn(rst_mig_7series_0_100M_peripheral_aresetn),
        .m_axi_mm2s_aclk(microblaze_0_Clk),
        .m_axi_mm2s_araddr(axi_dma_0_M_AXI_MM2S_ARADDR),
        .m_axi_mm2s_arburst(axi_dma_0_M_AXI_MM2S_ARBURST),
        .m_axi_mm2s_arcache(axi_dma_0_M_AXI_MM2S_ARCACHE),
        .m_axi_mm2s_arlen(axi_dma_0_M_AXI_MM2S_ARLEN),
        .m_axi_mm2s_arprot(axi_dma_0_M_AXI_MM2S_ARPROT),
        .m_axi_mm2s_arready(axi_dma_0_M_AXI_MM2S_ARREADY),
        .m_axi_mm2s_arsize(axi_dma_0_M_AXI_MM2S_ARSIZE),
        .m_axi_mm2s_arvalid(axi_dma_0_M_AXI_MM2S_ARVALID),
        .m_axi_mm2s_rdata(axi_dma_0_M_AXI_MM2S_RDATA),
        .m_axi_mm2s_rlast(axi_dma_0_M_AXI_MM2S_RLAST),
        .m_axi_mm2s_rready(axi_dma_0_M_AXI_MM2S_RREADY),
        .m_axi_mm2s_rresp(axi_dma_0_M_AXI_MM2S_RRESP),
        .m_axi_mm2s_rvalid(axi_dma_0_M_AXI_MM2S_RVALID),
        .m_axi_s2mm_aclk(microblaze_0_Clk),
        .m_axi_s2mm_awaddr(axi_dma_0_M_AXI_S2MM_AWADDR),
        .m_axi_s2mm_awburst(axi_dma_0_M_AXI_S2MM_AWBURST),
        .m_axi_s2mm_awcache(axi_dma_0_M_AXI_S2MM_AWCACHE),
        .m_axi_s2mm_awlen(axi_dma_0_M_AXI_S2MM_AWLEN),
        .m_axi_s2mm_awprot(axi_dma_0_M_AXI_S2MM_AWPROT),
        .m_axi_s2mm_awready(axi_dma_0_M_AXI_S2MM_AWREADY),
        .m_axi_s2mm_awsize(axi_dma_0_M_AXI_S2MM_AWSIZE),
        .m_axi_s2mm_awvalid(axi_dma_0_M_AXI_S2MM_AWVALID),
        .m_axi_s2mm_bready(axi_dma_0_M_AXI_S2MM_BREADY),
        .m_axi_s2mm_bresp(axi_dma_0_M_AXI_S2MM_BRESP),
        .m_axi_s2mm_bvalid(axi_dma_0_M_AXI_S2MM_BVALID),
        .m_axi_s2mm_wdata(axi_dma_0_M_AXI_S2MM_WDATA),
        .m_axi_s2mm_wlast(axi_dma_0_M_AXI_S2MM_WLAST),
        .m_axi_s2mm_wready(axi_dma_0_M_AXI_S2MM_WREADY),
        .m_axi_s2mm_wstrb(axi_dma_0_M_AXI_S2MM_WSTRB),
        .m_axi_s2mm_wvalid(axi_dma_0_M_AXI_S2MM_WVALID),
        .m_axi_sg_aclk(microblaze_0_Clk),
        .m_axi_sg_araddr(axi_dma_0_M_AXI_SG_ARADDR),
        .m_axi_sg_arburst(axi_dma_0_M_AXI_SG_ARBURST),
        .m_axi_sg_arcache(axi_dma_0_M_AXI_SG_ARCACHE),
        .m_axi_sg_arlen(axi_dma_0_M_AXI_SG_ARLEN),
        .m_axi_sg_arprot(axi_dma_0_M_AXI_SG_ARPROT),
        .m_axi_sg_arready(axi_dma_0_M_AXI_SG_ARREADY),
        .m_axi_sg_arsize(axi_dma_0_M_AXI_SG_ARSIZE),
        .m_axi_sg_arvalid(axi_dma_0_M_AXI_SG_ARVALID),
        .m_axi_sg_awaddr(axi_dma_0_M_AXI_SG_AWADDR),
        .m_axi_sg_awburst(axi_dma_0_M_AXI_SG_AWBURST),
        .m_axi_sg_awcache(axi_dma_0_M_AXI_SG_AWCACHE),
        .m_axi_sg_awlen(axi_dma_0_M_AXI_SG_AWLEN),
        .m_axi_sg_awprot(axi_dma_0_M_AXI_SG_AWPROT),
        .m_axi_sg_awready(axi_dma_0_M_AXI_SG_AWREADY),
        .m_axi_sg_awsize(axi_dma_0_M_AXI_SG_AWSIZE),
        .m_axi_sg_awvalid(axi_dma_0_M_AXI_SG_AWVALID),
        .m_axi_sg_bready(axi_dma_0_M_AXI_SG_BREADY),
        .m_axi_sg_bresp(axi_dma_0_M_AXI_SG_BRESP),
        .m_axi_sg_bvalid(axi_dma_0_M_AXI_SG_BVALID),
        .m_axi_sg_rdata(axi_dma_0_M_AXI_SG_RDATA),
        .m_axi_sg_rlast(axi_dma_0_M_AXI_SG_RLAST),
        .m_axi_sg_rready(axi_dma_0_M_AXI_SG_RREADY),
        .m_axi_sg_rresp(axi_dma_0_M_AXI_SG_RRESP),
        .m_axi_sg_rvalid(axi_dma_0_M_AXI_SG_RVALID),
        .m_axi_sg_wdata(axi_dma_0_M_AXI_SG_WDATA),
        .m_axi_sg_wlast(axi_dma_0_M_AXI_SG_WLAST),
        .m_axi_sg_wready(axi_dma_0_M_AXI_SG_WREADY),
        .m_axi_sg_wstrb(axi_dma_0_M_AXI_SG_WSTRB),
        .m_axi_sg_wvalid(axi_dma_0_M_AXI_SG_WVALID),
        .m_axis_mm2s_tdata(axi_dma_0_M_AXIS_MM2S_TDATA),
        .m_axis_mm2s_tdest(axi_dma_0_M_AXIS_MM2S_TDEST),
        .m_axis_mm2s_tid(axi_dma_0_M_AXIS_MM2S_TID),
        .m_axis_mm2s_tkeep(axi_dma_0_M_AXIS_MM2S_TKEEP),
        .m_axis_mm2s_tlast(axi_dma_0_M_AXIS_MM2S_TLAST),
        .m_axis_mm2s_tready(axi_dma_0_M_AXIS_MM2S_TREADY),
        .m_axis_mm2s_tuser(axi_dma_0_M_AXIS_MM2S_TUSER),
        .m_axis_mm2s_tvalid(axi_dma_0_M_AXIS_MM2S_TVALID),
        .mm2s_introut(axi_dma_0_mm2s_introut),
        .s2mm_introut(axi_dma_0_s2mm_introut),
        .s_axi_lite_aclk(microblaze_0_Clk),
        .s_axi_lite_araddr(microblaze_0_axi_periph_M03_AXI_ARADDR[9:0]),
        .s_axi_lite_arready(microblaze_0_axi_periph_M03_AXI_ARREADY),
        .s_axi_lite_arvalid(microblaze_0_axi_periph_M03_AXI_ARVALID),
        .s_axi_lite_awaddr(microblaze_0_axi_periph_M03_AXI_AWADDR[9:0]),
        .s_axi_lite_awready(microblaze_0_axi_periph_M03_AXI_AWREADY),
        .s_axi_lite_awvalid(microblaze_0_axi_periph_M03_AXI_AWVALID),
        .s_axi_lite_bready(microblaze_0_axi_periph_M03_AXI_BREADY),
        .s_axi_lite_bresp(microblaze_0_axi_periph_M03_AXI_BRESP),
        .s_axi_lite_bvalid(microblaze_0_axi_periph_M03_AXI_BVALID),
        .s_axi_lite_rdata(microblaze_0_axi_periph_M03_AXI_RDATA),
        .s_axi_lite_rready(microblaze_0_axi_periph_M03_AXI_RREADY),
        .s_axi_lite_rresp(microblaze_0_axi_periph_M03_AXI_RRESP),
        .s_axi_lite_rvalid(microblaze_0_axi_periph_M03_AXI_RVALID),
        .s_axi_lite_wdata(microblaze_0_axi_periph_M03_AXI_WDATA),
        .s_axi_lite_wready(microblaze_0_axi_periph_M03_AXI_WREADY),
        .s_axi_lite_wvalid(microblaze_0_axi_periph_M03_AXI_WVALID),
        .s_axis_s2mm_tdata(axis_data_fifo_0_M_AXIS_TDATA),
        .s_axis_s2mm_tdest(axis_data_fifo_0_M_AXIS_TDEST),
        .s_axis_s2mm_tid(axis_data_fifo_0_M_AXIS_TID),
        .s_axis_s2mm_tkeep(axis_data_fifo_0_M_AXIS_TKEEP),
        .s_axis_s2mm_tlast(axis_data_fifo_0_M_AXIS_TLAST),
        .s_axis_s2mm_tready(axis_data_fifo_0_M_AXIS_TREADY),
        .s_axis_s2mm_tuser(axis_data_fifo_0_M_AXIS_TUSER),
        .s_axis_s2mm_tvalid(axis_data_fifo_0_M_AXIS_TVALID));
  design_1_axi_dma_0_1 axi_dma_1
       (.axi_resetn(rst_mig_7series_0_100M_peripheral_aresetn),
        .m_axi_mm2s_aclk(microblaze_0_Clk),
        .m_axi_mm2s_araddr(axi_dma_1_M_AXI_MM2S_ARADDR),
        .m_axi_mm2s_arburst(axi_dma_1_M_AXI_MM2S_ARBURST),
        .m_axi_mm2s_arcache(axi_dma_1_M_AXI_MM2S_ARCACHE),
        .m_axi_mm2s_arlen(axi_dma_1_M_AXI_MM2S_ARLEN),
        .m_axi_mm2s_arprot(axi_dma_1_M_AXI_MM2S_ARPROT),
        .m_axi_mm2s_arready(axi_dma_1_M_AXI_MM2S_ARREADY),
        .m_axi_mm2s_arsize(axi_dma_1_M_AXI_MM2S_ARSIZE),
        .m_axi_mm2s_arvalid(axi_dma_1_M_AXI_MM2S_ARVALID),
        .m_axi_mm2s_rdata(axi_dma_1_M_AXI_MM2S_RDATA),
        .m_axi_mm2s_rlast(axi_dma_1_M_AXI_MM2S_RLAST),
        .m_axi_mm2s_rready(axi_dma_1_M_AXI_MM2S_RREADY),
        .m_axi_mm2s_rresp(axi_dma_1_M_AXI_MM2S_RRESP),
        .m_axi_mm2s_rvalid(axi_dma_1_M_AXI_MM2S_RVALID),
        .m_axi_s2mm_aclk(microblaze_0_Clk),
        .m_axi_s2mm_awaddr(axi_dma_1_M_AXI_S2MM_AWADDR),
        .m_axi_s2mm_awburst(axi_dma_1_M_AXI_S2MM_AWBURST),
        .m_axi_s2mm_awcache(axi_dma_1_M_AXI_S2MM_AWCACHE),
        .m_axi_s2mm_awlen(axi_dma_1_M_AXI_S2MM_AWLEN),
        .m_axi_s2mm_awprot(axi_dma_1_M_AXI_S2MM_AWPROT),
        .m_axi_s2mm_awready(axi_dma_1_M_AXI_S2MM_AWREADY),
        .m_axi_s2mm_awsize(axi_dma_1_M_AXI_S2MM_AWSIZE),
        .m_axi_s2mm_awvalid(axi_dma_1_M_AXI_S2MM_AWVALID),
        .m_axi_s2mm_bready(axi_dma_1_M_AXI_S2MM_BREADY),
        .m_axi_s2mm_bresp(axi_dma_1_M_AXI_S2MM_BRESP),
        .m_axi_s2mm_bvalid(axi_dma_1_M_AXI_S2MM_BVALID),
        .m_axi_s2mm_wdata(axi_dma_1_M_AXI_S2MM_WDATA),
        .m_axi_s2mm_wlast(axi_dma_1_M_AXI_S2MM_WLAST),
        .m_axi_s2mm_wready(axi_dma_1_M_AXI_S2MM_WREADY),
        .m_axi_s2mm_wstrb(axi_dma_1_M_AXI_S2MM_WSTRB),
        .m_axi_s2mm_wvalid(axi_dma_1_M_AXI_S2MM_WVALID),
        .m_axi_sg_aclk(microblaze_0_Clk),
        .m_axi_sg_araddr(axi_dma_1_M_AXI_SG_ARADDR),
        .m_axi_sg_arburst(axi_dma_1_M_AXI_SG_ARBURST),
        .m_axi_sg_arcache(axi_dma_1_M_AXI_SG_ARCACHE),
        .m_axi_sg_arlen(axi_dma_1_M_AXI_SG_ARLEN),
        .m_axi_sg_arprot(axi_dma_1_M_AXI_SG_ARPROT),
        .m_axi_sg_arready(axi_dma_1_M_AXI_SG_ARREADY),
        .m_axi_sg_arsize(axi_dma_1_M_AXI_SG_ARSIZE),
        .m_axi_sg_arvalid(axi_dma_1_M_AXI_SG_ARVALID),
        .m_axi_sg_awaddr(axi_dma_1_M_AXI_SG_AWADDR),
        .m_axi_sg_awburst(axi_dma_1_M_AXI_SG_AWBURST),
        .m_axi_sg_awcache(axi_dma_1_M_AXI_SG_AWCACHE),
        .m_axi_sg_awlen(axi_dma_1_M_AXI_SG_AWLEN),
        .m_axi_sg_awprot(axi_dma_1_M_AXI_SG_AWPROT),
        .m_axi_sg_awready(axi_dma_1_M_AXI_SG_AWREADY),
        .m_axi_sg_awsize(axi_dma_1_M_AXI_SG_AWSIZE),
        .m_axi_sg_awvalid(axi_dma_1_M_AXI_SG_AWVALID),
        .m_axi_sg_bready(axi_dma_1_M_AXI_SG_BREADY),
        .m_axi_sg_bresp(axi_dma_1_M_AXI_SG_BRESP),
        .m_axi_sg_bvalid(axi_dma_1_M_AXI_SG_BVALID),
        .m_axi_sg_rdata(axi_dma_1_M_AXI_SG_RDATA),
        .m_axi_sg_rlast(axi_dma_1_M_AXI_SG_RLAST),
        .m_axi_sg_rready(axi_dma_1_M_AXI_SG_RREADY),
        .m_axi_sg_rresp(axi_dma_1_M_AXI_SG_RRESP),
        .m_axi_sg_rvalid(axi_dma_1_M_AXI_SG_RVALID),
        .m_axi_sg_wdata(axi_dma_1_M_AXI_SG_WDATA),
        .m_axi_sg_wlast(axi_dma_1_M_AXI_SG_WLAST),
        .m_axi_sg_wready(axi_dma_1_M_AXI_SG_WREADY),
        .m_axi_sg_wstrb(axi_dma_1_M_AXI_SG_WSTRB),
        .m_axi_sg_wvalid(axi_dma_1_M_AXI_SG_WVALID),
        .m_axis_mm2s_tdata(axi_dma_1_M_AXIS_MM2S_TDATA),
        .m_axis_mm2s_tdest(axi_dma_1_M_AXIS_MM2S_TDEST),
        .m_axis_mm2s_tid(axi_dma_1_M_AXIS_MM2S_TID),
        .m_axis_mm2s_tkeep(axi_dma_1_M_AXIS_MM2S_TKEEP),
        .m_axis_mm2s_tlast(axi_dma_1_M_AXIS_MM2S_TLAST),
        .m_axis_mm2s_tready(axi_dma_1_M_AXIS_MM2S_TREADY),
        .m_axis_mm2s_tuser(axi_dma_1_M_AXIS_MM2S_TUSER),
        .m_axis_mm2s_tvalid(axi_dma_1_M_AXIS_MM2S_TVALID),
        .mm2s_introut(axi_dma_1_mm2s_introut),
        .s2mm_introut(axi_dma_1_s2mm_introut),
        .s_axi_lite_aclk(microblaze_0_Clk),
        .s_axi_lite_araddr(microblaze_0_axi_periph_M04_AXI_ARADDR[9:0]),
        .s_axi_lite_arready(microblaze_0_axi_periph_M04_AXI_ARREADY),
        .s_axi_lite_arvalid(microblaze_0_axi_periph_M04_AXI_ARVALID),
        .s_axi_lite_awaddr(microblaze_0_axi_periph_M04_AXI_AWADDR[9:0]),
        .s_axi_lite_awready(microblaze_0_axi_periph_M04_AXI_AWREADY),
        .s_axi_lite_awvalid(microblaze_0_axi_periph_M04_AXI_AWVALID),
        .s_axi_lite_bready(microblaze_0_axi_periph_M04_AXI_BREADY),
        .s_axi_lite_bresp(microblaze_0_axi_periph_M04_AXI_BRESP),
        .s_axi_lite_bvalid(microblaze_0_axi_periph_M04_AXI_BVALID),
        .s_axi_lite_rdata(microblaze_0_axi_periph_M04_AXI_RDATA),
        .s_axi_lite_rready(microblaze_0_axi_periph_M04_AXI_RREADY),
        .s_axi_lite_rresp(microblaze_0_axi_periph_M04_AXI_RRESP),
        .s_axi_lite_rvalid(microblaze_0_axi_periph_M04_AXI_RVALID),
        .s_axi_lite_wdata(microblaze_0_axi_periph_M04_AXI_WDATA),
        .s_axi_lite_wready(microblaze_0_axi_periph_M04_AXI_WREADY),
        .s_axi_lite_wvalid(microblaze_0_axi_periph_M04_AXI_WVALID),
        .s_axis_s2mm_tdata(axis_data_fifo_1_M_AXIS_TDATA),
        .s_axis_s2mm_tdest(axis_data_fifo_1_M_AXIS_TDEST),
        .s_axis_s2mm_tid(axis_data_fifo_1_M_AXIS_TID),
        .s_axis_s2mm_tkeep(axis_data_fifo_1_M_AXIS_TKEEP),
        .s_axis_s2mm_tlast(axis_data_fifo_1_M_AXIS_TLAST),
        .s_axis_s2mm_tready(axis_data_fifo_1_M_AXIS_TREADY),
        .s_axis_s2mm_tuser(axis_data_fifo_1_M_AXIS_TUSER),
        .s_axis_s2mm_tvalid(axis_data_fifo_1_M_AXIS_TVALID));
  design_1_axi_dma_0_2 axi_dma_2
       (.axi_resetn(rst_mig_7series_0_100M_peripheral_aresetn),
        .m_axi_mm2s_aclk(microblaze_0_Clk),
        .m_axi_mm2s_araddr(axi_dma_2_M_AXI_MM2S_ARADDR),
        .m_axi_mm2s_arburst(axi_dma_2_M_AXI_MM2S_ARBURST),
        .m_axi_mm2s_arcache(axi_dma_2_M_AXI_MM2S_ARCACHE),
        .m_axi_mm2s_arlen(axi_dma_2_M_AXI_MM2S_ARLEN),
        .m_axi_mm2s_arprot(axi_dma_2_M_AXI_MM2S_ARPROT),
        .m_axi_mm2s_arready(axi_dma_2_M_AXI_MM2S_ARREADY),
        .m_axi_mm2s_arsize(axi_dma_2_M_AXI_MM2S_ARSIZE),
        .m_axi_mm2s_arvalid(axi_dma_2_M_AXI_MM2S_ARVALID),
        .m_axi_mm2s_rdata(axi_dma_2_M_AXI_MM2S_RDATA),
        .m_axi_mm2s_rlast(axi_dma_2_M_AXI_MM2S_RLAST),
        .m_axi_mm2s_rready(axi_dma_2_M_AXI_MM2S_RREADY),
        .m_axi_mm2s_rresp(axi_dma_2_M_AXI_MM2S_RRESP),
        .m_axi_mm2s_rvalid(axi_dma_2_M_AXI_MM2S_RVALID),
        .m_axi_s2mm_aclk(microblaze_0_Clk),
        .m_axi_s2mm_awaddr(axi_dma_2_M_AXI_S2MM_AWADDR),
        .m_axi_s2mm_awburst(axi_dma_2_M_AXI_S2MM_AWBURST),
        .m_axi_s2mm_awcache(axi_dma_2_M_AXI_S2MM_AWCACHE),
        .m_axi_s2mm_awlen(axi_dma_2_M_AXI_S2MM_AWLEN),
        .m_axi_s2mm_awprot(axi_dma_2_M_AXI_S2MM_AWPROT),
        .m_axi_s2mm_awready(axi_dma_2_M_AXI_S2MM_AWREADY),
        .m_axi_s2mm_awsize(axi_dma_2_M_AXI_S2MM_AWSIZE),
        .m_axi_s2mm_awvalid(axi_dma_2_M_AXI_S2MM_AWVALID),
        .m_axi_s2mm_bready(axi_dma_2_M_AXI_S2MM_BREADY),
        .m_axi_s2mm_bresp(axi_dma_2_M_AXI_S2MM_BRESP),
        .m_axi_s2mm_bvalid(axi_dma_2_M_AXI_S2MM_BVALID),
        .m_axi_s2mm_wdata(axi_dma_2_M_AXI_S2MM_WDATA),
        .m_axi_s2mm_wlast(axi_dma_2_M_AXI_S2MM_WLAST),
        .m_axi_s2mm_wready(axi_dma_2_M_AXI_S2MM_WREADY),
        .m_axi_s2mm_wstrb(axi_dma_2_M_AXI_S2MM_WSTRB),
        .m_axi_s2mm_wvalid(axi_dma_2_M_AXI_S2MM_WVALID),
        .m_axi_sg_aclk(microblaze_0_Clk),
        .m_axi_sg_araddr(axi_dma_2_M_AXI_SG_ARADDR),
        .m_axi_sg_arburst(axi_dma_2_M_AXI_SG_ARBURST),
        .m_axi_sg_arcache(axi_dma_2_M_AXI_SG_ARCACHE),
        .m_axi_sg_arlen(axi_dma_2_M_AXI_SG_ARLEN),
        .m_axi_sg_arprot(axi_dma_2_M_AXI_SG_ARPROT),
        .m_axi_sg_arready(axi_dma_2_M_AXI_SG_ARREADY),
        .m_axi_sg_arsize(axi_dma_2_M_AXI_SG_ARSIZE),
        .m_axi_sg_arvalid(axi_dma_2_M_AXI_SG_ARVALID),
        .m_axi_sg_awaddr(axi_dma_2_M_AXI_SG_AWADDR),
        .m_axi_sg_awburst(axi_dma_2_M_AXI_SG_AWBURST),
        .m_axi_sg_awcache(axi_dma_2_M_AXI_SG_AWCACHE),
        .m_axi_sg_awlen(axi_dma_2_M_AXI_SG_AWLEN),
        .m_axi_sg_awprot(axi_dma_2_M_AXI_SG_AWPROT),
        .m_axi_sg_awready(axi_dma_2_M_AXI_SG_AWREADY),
        .m_axi_sg_awsize(axi_dma_2_M_AXI_SG_AWSIZE),
        .m_axi_sg_awvalid(axi_dma_2_M_AXI_SG_AWVALID),
        .m_axi_sg_bready(axi_dma_2_M_AXI_SG_BREADY),
        .m_axi_sg_bresp(axi_dma_2_M_AXI_SG_BRESP),
        .m_axi_sg_bvalid(axi_dma_2_M_AXI_SG_BVALID),
        .m_axi_sg_rdata(axi_dma_2_M_AXI_SG_RDATA),
        .m_axi_sg_rlast(axi_dma_2_M_AXI_SG_RLAST),
        .m_axi_sg_rready(axi_dma_2_M_AXI_SG_RREADY),
        .m_axi_sg_rresp(axi_dma_2_M_AXI_SG_RRESP),
        .m_axi_sg_rvalid(axi_dma_2_M_AXI_SG_RVALID),
        .m_axi_sg_wdata(axi_dma_2_M_AXI_SG_WDATA),
        .m_axi_sg_wlast(axi_dma_2_M_AXI_SG_WLAST),
        .m_axi_sg_wready(axi_dma_2_M_AXI_SG_WREADY),
        .m_axi_sg_wstrb(axi_dma_2_M_AXI_SG_WSTRB),
        .m_axi_sg_wvalid(axi_dma_2_M_AXI_SG_WVALID),
        .m_axis_mm2s_tdata(axi_dma_2_M_AXIS_MM2S_TDATA),
        .m_axis_mm2s_tdest(axi_dma_2_M_AXIS_MM2S_TDEST),
        .m_axis_mm2s_tid(axi_dma_2_M_AXIS_MM2S_TID),
        .m_axis_mm2s_tkeep(axi_dma_2_M_AXIS_MM2S_TKEEP),
        .m_axis_mm2s_tlast(axi_dma_2_M_AXIS_MM2S_TLAST),
        .m_axis_mm2s_tready(axi_dma_2_M_AXIS_MM2S_TREADY),
        .m_axis_mm2s_tuser(axi_dma_2_M_AXIS_MM2S_TUSER),
        .m_axis_mm2s_tvalid(axi_dma_2_M_AXIS_MM2S_TVALID),
        .mm2s_introut(axi_dma_2_mm2s_introut),
        .s2mm_introut(axi_dma_2_s2mm_introut),
        .s_axi_lite_aclk(microblaze_0_Clk),
        .s_axi_lite_araddr(microblaze_0_axi_periph_M05_AXI_ARADDR[9:0]),
        .s_axi_lite_arready(microblaze_0_axi_periph_M05_AXI_ARREADY),
        .s_axi_lite_arvalid(microblaze_0_axi_periph_M05_AXI_ARVALID),
        .s_axi_lite_awaddr(microblaze_0_axi_periph_M05_AXI_AWADDR[9:0]),
        .s_axi_lite_awready(microblaze_0_axi_periph_M05_AXI_AWREADY),
        .s_axi_lite_awvalid(microblaze_0_axi_periph_M05_AXI_AWVALID),
        .s_axi_lite_bready(microblaze_0_axi_periph_M05_AXI_BREADY),
        .s_axi_lite_bresp(microblaze_0_axi_periph_M05_AXI_BRESP),
        .s_axi_lite_bvalid(microblaze_0_axi_periph_M05_AXI_BVALID),
        .s_axi_lite_rdata(microblaze_0_axi_periph_M05_AXI_RDATA),
        .s_axi_lite_rready(microblaze_0_axi_periph_M05_AXI_RREADY),
        .s_axi_lite_rresp(microblaze_0_axi_periph_M05_AXI_RRESP),
        .s_axi_lite_rvalid(microblaze_0_axi_periph_M05_AXI_RVALID),
        .s_axi_lite_wdata(microblaze_0_axi_periph_M05_AXI_WDATA),
        .s_axi_lite_wready(microblaze_0_axi_periph_M05_AXI_WREADY),
        .s_axi_lite_wvalid(microblaze_0_axi_periph_M05_AXI_WVALID),
        .s_axis_s2mm_tdata(axis_data_fifo_2_M_AXIS_TDATA),
        .s_axis_s2mm_tdest(axis_data_fifo_2_M_AXIS_TDEST),
        .s_axis_s2mm_tid(axis_data_fifo_2_M_AXIS_TID),
        .s_axis_s2mm_tkeep(axis_data_fifo_2_M_AXIS_TKEEP),
        .s_axis_s2mm_tlast(axis_data_fifo_2_M_AXIS_TLAST),
        .s_axis_s2mm_tready(axis_data_fifo_2_M_AXIS_TREADY),
        .s_axis_s2mm_tuser(axis_data_fifo_2_M_AXIS_TUSER),
        .s_axis_s2mm_tvalid(axis_data_fifo_2_M_AXIS_TVALID));
  design_1_axi_dma_0_3 axi_dma_3
       (.axi_resetn(rst_mig_7series_0_100M_peripheral_aresetn),
        .m_axi_mm2s_aclk(microblaze_0_Clk),
        .m_axi_mm2s_araddr(axi_dma_3_M_AXI_MM2S_ARADDR),
        .m_axi_mm2s_arburst(axi_dma_3_M_AXI_MM2S_ARBURST),
        .m_axi_mm2s_arcache(axi_dma_3_M_AXI_MM2S_ARCACHE),
        .m_axi_mm2s_arlen(axi_dma_3_M_AXI_MM2S_ARLEN),
        .m_axi_mm2s_arprot(axi_dma_3_M_AXI_MM2S_ARPROT),
        .m_axi_mm2s_arready(axi_dma_3_M_AXI_MM2S_ARREADY),
        .m_axi_mm2s_arsize(axi_dma_3_M_AXI_MM2S_ARSIZE),
        .m_axi_mm2s_arvalid(axi_dma_3_M_AXI_MM2S_ARVALID),
        .m_axi_mm2s_rdata(axi_dma_3_M_AXI_MM2S_RDATA),
        .m_axi_mm2s_rlast(axi_dma_3_M_AXI_MM2S_RLAST),
        .m_axi_mm2s_rready(axi_dma_3_M_AXI_MM2S_RREADY),
        .m_axi_mm2s_rresp(axi_dma_3_M_AXI_MM2S_RRESP),
        .m_axi_mm2s_rvalid(axi_dma_3_M_AXI_MM2S_RVALID),
        .m_axi_s2mm_aclk(microblaze_0_Clk),
        .m_axi_s2mm_awaddr(axi_dma_3_M_AXI_S2MM_AWADDR),
        .m_axi_s2mm_awburst(axi_dma_3_M_AXI_S2MM_AWBURST),
        .m_axi_s2mm_awcache(axi_dma_3_M_AXI_S2MM_AWCACHE),
        .m_axi_s2mm_awlen(axi_dma_3_M_AXI_S2MM_AWLEN),
        .m_axi_s2mm_awprot(axi_dma_3_M_AXI_S2MM_AWPROT),
        .m_axi_s2mm_awready(axi_dma_3_M_AXI_S2MM_AWREADY),
        .m_axi_s2mm_awsize(axi_dma_3_M_AXI_S2MM_AWSIZE),
        .m_axi_s2mm_awvalid(axi_dma_3_M_AXI_S2MM_AWVALID),
        .m_axi_s2mm_bready(axi_dma_3_M_AXI_S2MM_BREADY),
        .m_axi_s2mm_bresp(axi_dma_3_M_AXI_S2MM_BRESP),
        .m_axi_s2mm_bvalid(axi_dma_3_M_AXI_S2MM_BVALID),
        .m_axi_s2mm_wdata(axi_dma_3_M_AXI_S2MM_WDATA),
        .m_axi_s2mm_wlast(axi_dma_3_M_AXI_S2MM_WLAST),
        .m_axi_s2mm_wready(axi_dma_3_M_AXI_S2MM_WREADY),
        .m_axi_s2mm_wstrb(axi_dma_3_M_AXI_S2MM_WSTRB),
        .m_axi_s2mm_wvalid(axi_dma_3_M_AXI_S2MM_WVALID),
        .m_axi_sg_aclk(microblaze_0_Clk),
        .m_axi_sg_araddr(axi_dma_3_M_AXI_SG_ARADDR),
        .m_axi_sg_arburst(axi_dma_3_M_AXI_SG_ARBURST),
        .m_axi_sg_arcache(axi_dma_3_M_AXI_SG_ARCACHE),
        .m_axi_sg_arlen(axi_dma_3_M_AXI_SG_ARLEN),
        .m_axi_sg_arprot(axi_dma_3_M_AXI_SG_ARPROT),
        .m_axi_sg_arready(axi_dma_3_M_AXI_SG_ARREADY),
        .m_axi_sg_arsize(axi_dma_3_M_AXI_SG_ARSIZE),
        .m_axi_sg_arvalid(axi_dma_3_M_AXI_SG_ARVALID),
        .m_axi_sg_awaddr(axi_dma_3_M_AXI_SG_AWADDR),
        .m_axi_sg_awburst(axi_dma_3_M_AXI_SG_AWBURST),
        .m_axi_sg_awcache(axi_dma_3_M_AXI_SG_AWCACHE),
        .m_axi_sg_awlen(axi_dma_3_M_AXI_SG_AWLEN),
        .m_axi_sg_awprot(axi_dma_3_M_AXI_SG_AWPROT),
        .m_axi_sg_awready(axi_dma_3_M_AXI_SG_AWREADY),
        .m_axi_sg_awsize(axi_dma_3_M_AXI_SG_AWSIZE),
        .m_axi_sg_awvalid(axi_dma_3_M_AXI_SG_AWVALID),
        .m_axi_sg_bready(axi_dma_3_M_AXI_SG_BREADY),
        .m_axi_sg_bresp(axi_dma_3_M_AXI_SG_BRESP),
        .m_axi_sg_bvalid(axi_dma_3_M_AXI_SG_BVALID),
        .m_axi_sg_rdata(axi_dma_3_M_AXI_SG_RDATA),
        .m_axi_sg_rlast(axi_dma_3_M_AXI_SG_RLAST),
        .m_axi_sg_rready(axi_dma_3_M_AXI_SG_RREADY),
        .m_axi_sg_rresp(axi_dma_3_M_AXI_SG_RRESP),
        .m_axi_sg_rvalid(axi_dma_3_M_AXI_SG_RVALID),
        .m_axi_sg_wdata(axi_dma_3_M_AXI_SG_WDATA),
        .m_axi_sg_wlast(axi_dma_3_M_AXI_SG_WLAST),
        .m_axi_sg_wready(axi_dma_3_M_AXI_SG_WREADY),
        .m_axi_sg_wstrb(axi_dma_3_M_AXI_SG_WSTRB),
        .m_axi_sg_wvalid(axi_dma_3_M_AXI_SG_WVALID),
        .m_axis_mm2s_tdata(axi_dma_3_M_AXIS_MM2S_TDATA),
        .m_axis_mm2s_tdest(axi_dma_3_M_AXIS_MM2S_TDEST),
        .m_axis_mm2s_tid(axi_dma_3_M_AXIS_MM2S_TID),
        .m_axis_mm2s_tkeep(axi_dma_3_M_AXIS_MM2S_TKEEP),
        .m_axis_mm2s_tlast(axi_dma_3_M_AXIS_MM2S_TLAST),
        .m_axis_mm2s_tready(axi_dma_3_M_AXIS_MM2S_TREADY),
        .m_axis_mm2s_tuser(axi_dma_3_M_AXIS_MM2S_TUSER),
        .m_axis_mm2s_tvalid(axi_dma_3_M_AXIS_MM2S_TVALID),
        .mm2s_introut(axi_dma_3_mm2s_introut),
        .s2mm_introut(axi_dma_3_s2mm_introut),
        .s_axi_lite_aclk(microblaze_0_Clk),
        .s_axi_lite_araddr(microblaze_0_axi_periph_M06_AXI_ARADDR[9:0]),
        .s_axi_lite_arready(microblaze_0_axi_periph_M06_AXI_ARREADY),
        .s_axi_lite_arvalid(microblaze_0_axi_periph_M06_AXI_ARVALID),
        .s_axi_lite_awaddr(microblaze_0_axi_periph_M06_AXI_AWADDR[9:0]),
        .s_axi_lite_awready(microblaze_0_axi_periph_M06_AXI_AWREADY),
        .s_axi_lite_awvalid(microblaze_0_axi_periph_M06_AXI_AWVALID),
        .s_axi_lite_bready(microblaze_0_axi_periph_M06_AXI_BREADY),
        .s_axi_lite_bresp(microblaze_0_axi_periph_M06_AXI_BRESP),
        .s_axi_lite_bvalid(microblaze_0_axi_periph_M06_AXI_BVALID),
        .s_axi_lite_rdata(microblaze_0_axi_periph_M06_AXI_RDATA),
        .s_axi_lite_rready(microblaze_0_axi_periph_M06_AXI_RREADY),
        .s_axi_lite_rresp(microblaze_0_axi_periph_M06_AXI_RRESP),
        .s_axi_lite_rvalid(microblaze_0_axi_periph_M06_AXI_RVALID),
        .s_axi_lite_wdata(microblaze_0_axi_periph_M06_AXI_WDATA),
        .s_axi_lite_wready(microblaze_0_axi_periph_M06_AXI_WREADY),
        .s_axi_lite_wvalid(microblaze_0_axi_periph_M06_AXI_WVALID),
        .s_axis_s2mm_tdata(axis_data_fifo_3_M_AXIS_TDATA),
        .s_axis_s2mm_tdest(axis_data_fifo_3_M_AXIS_TDEST),
        .s_axis_s2mm_tid(axis_data_fifo_3_M_AXIS_TID),
        .s_axis_s2mm_tkeep(axis_data_fifo_3_M_AXIS_TKEEP),
        .s_axis_s2mm_tlast(axis_data_fifo_3_M_AXIS_TLAST),
        .s_axis_s2mm_tready(axis_data_fifo_3_M_AXIS_TREADY),
        .s_axis_s2mm_tuser(axis_data_fifo_3_M_AXIS_TUSER),
        .s_axis_s2mm_tvalid(axis_data_fifo_3_M_AXIS_TVALID));
  design_1_axi_intc_0_0 axi_intc_0
       (.interrupt_address(axi_intc_0_interrupt_ADDRESS),
        .intr(xlconcat_0_dout),
        .irq(axi_intc_0_interrupt_INTERRUPT),
        .processor_ack({axi_intc_0_interrupt_ACK[0],axi_intc_0_interrupt_ACK[1]}),
        .processor_clk(microblaze_0_Clk),
        .processor_rst(rst_mig_7series_0_100M_mb_reset),
        .s_axi_aclk(microblaze_0_Clk),
        .s_axi_araddr(microblaze_0_axi_periph_M02_AXI_ARADDR[8:0]),
        .s_axi_aresetn(rst_mig_7series_0_100M_peripheral_aresetn),
        .s_axi_arready(microblaze_0_axi_periph_M02_AXI_ARREADY),
        .s_axi_arvalid(microblaze_0_axi_periph_M02_AXI_ARVALID),
        .s_axi_awaddr(microblaze_0_axi_periph_M02_AXI_AWADDR[8:0]),
        .s_axi_awready(microblaze_0_axi_periph_M02_AXI_AWREADY),
        .s_axi_awvalid(microblaze_0_axi_periph_M02_AXI_AWVALID),
        .s_axi_bready(microblaze_0_axi_periph_M02_AXI_BREADY),
        .s_axi_bresp(microblaze_0_axi_periph_M02_AXI_BRESP),
        .s_axi_bvalid(microblaze_0_axi_periph_M02_AXI_BVALID),
        .s_axi_rdata(microblaze_0_axi_periph_M02_AXI_RDATA),
        .s_axi_rready(microblaze_0_axi_periph_M02_AXI_RREADY),
        .s_axi_rresp(microblaze_0_axi_periph_M02_AXI_RRESP),
        .s_axi_rvalid(microblaze_0_axi_periph_M02_AXI_RVALID),
        .s_axi_wdata(microblaze_0_axi_periph_M02_AXI_WDATA),
        .s_axi_wready(microblaze_0_axi_periph_M02_AXI_WREADY),
        .s_axi_wstrb(microblaze_0_axi_periph_M02_AXI_WSTRB),
        .s_axi_wvalid(microblaze_0_axi_periph_M02_AXI_WVALID));
  design_1_axi_mem_intercon_0 axi_mem_intercon
       (.ACLK(microblaze_0_Clk),
        .ARESETN(rst_mig_7series_0_100M_interconnect_aresetn),
        .M00_ACLK(microblaze_0_Clk),
        .M00_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .M00_AXI_araddr(axi_mem_intercon_M00_AXI_ARADDR),
        .M00_AXI_arburst(axi_mem_intercon_M00_AXI_ARBURST),
        .M00_AXI_arcache(axi_mem_intercon_M00_AXI_ARCACHE),
        .M00_AXI_arid(axi_mem_intercon_M00_AXI_ARID),
        .M00_AXI_arlen(axi_mem_intercon_M00_AXI_ARLEN),
        .M00_AXI_arlock(axi_mem_intercon_M00_AXI_ARLOCK),
        .M00_AXI_arprot(axi_mem_intercon_M00_AXI_ARPROT),
        .M00_AXI_arqos(axi_mem_intercon_M00_AXI_ARQOS),
        .M00_AXI_arready(axi_mem_intercon_M00_AXI_ARREADY),
        .M00_AXI_arsize(axi_mem_intercon_M00_AXI_ARSIZE),
        .M00_AXI_arvalid(axi_mem_intercon_M00_AXI_ARVALID),
        .M00_AXI_awaddr(axi_mem_intercon_M00_AXI_AWADDR),
        .M00_AXI_awburst(axi_mem_intercon_M00_AXI_AWBURST),
        .M00_AXI_awcache(axi_mem_intercon_M00_AXI_AWCACHE),
        .M00_AXI_awid(axi_mem_intercon_M00_AXI_AWID),
        .M00_AXI_awlen(axi_mem_intercon_M00_AXI_AWLEN),
        .M00_AXI_awlock(axi_mem_intercon_M00_AXI_AWLOCK),
        .M00_AXI_awprot(axi_mem_intercon_M00_AXI_AWPROT),
        .M00_AXI_awqos(axi_mem_intercon_M00_AXI_AWQOS),
        .M00_AXI_awready(axi_mem_intercon_M00_AXI_AWREADY),
        .M00_AXI_awsize(axi_mem_intercon_M00_AXI_AWSIZE),
        .M00_AXI_awvalid(axi_mem_intercon_M00_AXI_AWVALID),
        .M00_AXI_bid(axi_mem_intercon_M00_AXI_BID),
        .M00_AXI_bready(axi_mem_intercon_M00_AXI_BREADY),
        .M00_AXI_bresp(axi_mem_intercon_M00_AXI_BRESP),
        .M00_AXI_bvalid(axi_mem_intercon_M00_AXI_BVALID),
        .M00_AXI_rdata(axi_mem_intercon_M00_AXI_RDATA),
        .M00_AXI_rid(axi_mem_intercon_M00_AXI_RID),
        .M00_AXI_rlast(axi_mem_intercon_M00_AXI_RLAST),
        .M00_AXI_rready(axi_mem_intercon_M00_AXI_RREADY),
        .M00_AXI_rresp(axi_mem_intercon_M00_AXI_RRESP),
        .M00_AXI_rvalid(axi_mem_intercon_M00_AXI_RVALID),
        .M00_AXI_wdata(axi_mem_intercon_M00_AXI_WDATA),
        .M00_AXI_wlast(axi_mem_intercon_M00_AXI_WLAST),
        .M00_AXI_wready(axi_mem_intercon_M00_AXI_WREADY),
        .M00_AXI_wstrb(axi_mem_intercon_M00_AXI_WSTRB),
        .M00_AXI_wvalid(axi_mem_intercon_M00_AXI_WVALID),
        .S00_ACLK(microblaze_0_Clk),
        .S00_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .S00_AXI_araddr(microblaze_0_M_AXI_DC_ARADDR),
        .S00_AXI_arburst(microblaze_0_M_AXI_DC_ARBURST),
        .S00_AXI_arcache(microblaze_0_M_AXI_DC_ARCACHE),
        .S00_AXI_arlen(microblaze_0_M_AXI_DC_ARLEN),
        .S00_AXI_arlock(microblaze_0_M_AXI_DC_ARLOCK),
        .S00_AXI_arprot(microblaze_0_M_AXI_DC_ARPROT),
        .S00_AXI_arqos(microblaze_0_M_AXI_DC_ARQOS),
        .S00_AXI_arready(microblaze_0_M_AXI_DC_ARREADY),
        .S00_AXI_arsize(microblaze_0_M_AXI_DC_ARSIZE),
        .S00_AXI_arvalid(microblaze_0_M_AXI_DC_ARVALID),
        .S00_AXI_awaddr(microblaze_0_M_AXI_DC_AWADDR),
        .S00_AXI_awburst(microblaze_0_M_AXI_DC_AWBURST),
        .S00_AXI_awcache(microblaze_0_M_AXI_DC_AWCACHE),
        .S00_AXI_awlen(microblaze_0_M_AXI_DC_AWLEN),
        .S00_AXI_awlock(microblaze_0_M_AXI_DC_AWLOCK),
        .S00_AXI_awprot(microblaze_0_M_AXI_DC_AWPROT),
        .S00_AXI_awqos(microblaze_0_M_AXI_DC_AWQOS),
        .S00_AXI_awready(microblaze_0_M_AXI_DC_AWREADY),
        .S00_AXI_awsize(microblaze_0_M_AXI_DC_AWSIZE),
        .S00_AXI_awvalid(microblaze_0_M_AXI_DC_AWVALID),
        .S00_AXI_bready(microblaze_0_M_AXI_DC_BREADY),
        .S00_AXI_bresp(microblaze_0_M_AXI_DC_BRESP),
        .S00_AXI_bvalid(microblaze_0_M_AXI_DC_BVALID),
        .S00_AXI_rdata(microblaze_0_M_AXI_DC_RDATA),
        .S00_AXI_rlast(microblaze_0_M_AXI_DC_RLAST),
        .S00_AXI_rready(microblaze_0_M_AXI_DC_RREADY),
        .S00_AXI_rresp(microblaze_0_M_AXI_DC_RRESP),
        .S00_AXI_rvalid(microblaze_0_M_AXI_DC_RVALID),
        .S00_AXI_wdata(microblaze_0_M_AXI_DC_WDATA),
        .S00_AXI_wlast(microblaze_0_M_AXI_DC_WLAST),
        .S00_AXI_wready(microblaze_0_M_AXI_DC_WREADY),
        .S00_AXI_wstrb(microblaze_0_M_AXI_DC_WSTRB),
        .S00_AXI_wvalid(microblaze_0_M_AXI_DC_WVALID),
        .S01_ACLK(microblaze_0_Clk),
        .S01_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .S01_AXI_araddr(microblaze_0_M_AXI_IC_ARADDR),
        .S01_AXI_arburst(microblaze_0_M_AXI_IC_ARBURST),
        .S01_AXI_arcache(microblaze_0_M_AXI_IC_ARCACHE),
        .S01_AXI_arlen(microblaze_0_M_AXI_IC_ARLEN),
        .S01_AXI_arlock(microblaze_0_M_AXI_IC_ARLOCK),
        .S01_AXI_arprot(microblaze_0_M_AXI_IC_ARPROT),
        .S01_AXI_arqos(microblaze_0_M_AXI_IC_ARQOS),
        .S01_AXI_arready(microblaze_0_M_AXI_IC_ARREADY),
        .S01_AXI_arsize(microblaze_0_M_AXI_IC_ARSIZE),
        .S01_AXI_arvalid(microblaze_0_M_AXI_IC_ARVALID),
        .S01_AXI_rdata(microblaze_0_M_AXI_IC_RDATA),
        .S01_AXI_rlast(microblaze_0_M_AXI_IC_RLAST),
        .S01_AXI_rready(microblaze_0_M_AXI_IC_RREADY),
        .S01_AXI_rresp(microblaze_0_M_AXI_IC_RRESP),
        .S01_AXI_rvalid(microblaze_0_M_AXI_IC_RVALID),
        .S02_ACLK(microblaze_0_Clk),
        .S02_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .S02_AXI_araddr(axi_dma_0_M_AXI_SG_ARADDR),
        .S02_AXI_arburst(axi_dma_0_M_AXI_SG_ARBURST),
        .S02_AXI_arcache(axi_dma_0_M_AXI_SG_ARCACHE),
        .S02_AXI_arlen(axi_dma_0_M_AXI_SG_ARLEN),
        .S02_AXI_arprot(axi_dma_0_M_AXI_SG_ARPROT),
        .S02_AXI_arready(axi_dma_0_M_AXI_SG_ARREADY),
        .S02_AXI_arsize(axi_dma_0_M_AXI_SG_ARSIZE),
        .S02_AXI_arvalid(axi_dma_0_M_AXI_SG_ARVALID),
        .S02_AXI_awaddr(axi_dma_0_M_AXI_SG_AWADDR),
        .S02_AXI_awburst(axi_dma_0_M_AXI_SG_AWBURST),
        .S02_AXI_awcache(axi_dma_0_M_AXI_SG_AWCACHE),
        .S02_AXI_awlen(axi_dma_0_M_AXI_SG_AWLEN),
        .S02_AXI_awprot(axi_dma_0_M_AXI_SG_AWPROT),
        .S02_AXI_awready(axi_dma_0_M_AXI_SG_AWREADY),
        .S02_AXI_awsize(axi_dma_0_M_AXI_SG_AWSIZE),
        .S02_AXI_awvalid(axi_dma_0_M_AXI_SG_AWVALID),
        .S02_AXI_bready(axi_dma_0_M_AXI_SG_BREADY),
        .S02_AXI_bresp(axi_dma_0_M_AXI_SG_BRESP),
        .S02_AXI_bvalid(axi_dma_0_M_AXI_SG_BVALID),
        .S02_AXI_rdata(axi_dma_0_M_AXI_SG_RDATA),
        .S02_AXI_rlast(axi_dma_0_M_AXI_SG_RLAST),
        .S02_AXI_rready(axi_dma_0_M_AXI_SG_RREADY),
        .S02_AXI_rresp(axi_dma_0_M_AXI_SG_RRESP),
        .S02_AXI_rvalid(axi_dma_0_M_AXI_SG_RVALID),
        .S02_AXI_wdata(axi_dma_0_M_AXI_SG_WDATA),
        .S02_AXI_wlast(axi_dma_0_M_AXI_SG_WLAST),
        .S02_AXI_wready(axi_dma_0_M_AXI_SG_WREADY),
        .S02_AXI_wstrb(axi_dma_0_M_AXI_SG_WSTRB),
        .S02_AXI_wvalid(axi_dma_0_M_AXI_SG_WVALID),
        .S03_ACLK(microblaze_0_Clk),
        .S03_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .S03_AXI_araddr(axi_dma_0_M_AXI_MM2S_ARADDR),
        .S03_AXI_arburst(axi_dma_0_M_AXI_MM2S_ARBURST),
        .S03_AXI_arcache(axi_dma_0_M_AXI_MM2S_ARCACHE),
        .S03_AXI_arlen(axi_dma_0_M_AXI_MM2S_ARLEN),
        .S03_AXI_arprot(axi_dma_0_M_AXI_MM2S_ARPROT),
        .S03_AXI_arready(axi_dma_0_M_AXI_MM2S_ARREADY),
        .S03_AXI_arsize(axi_dma_0_M_AXI_MM2S_ARSIZE),
        .S03_AXI_arvalid(axi_dma_0_M_AXI_MM2S_ARVALID),
        .S03_AXI_rdata(axi_dma_0_M_AXI_MM2S_RDATA),
        .S03_AXI_rlast(axi_dma_0_M_AXI_MM2S_RLAST),
        .S03_AXI_rready(axi_dma_0_M_AXI_MM2S_RREADY),
        .S03_AXI_rresp(axi_dma_0_M_AXI_MM2S_RRESP),
        .S03_AXI_rvalid(axi_dma_0_M_AXI_MM2S_RVALID),
        .S04_ACLK(microblaze_0_Clk),
        .S04_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .S04_AXI_awaddr(axi_dma_0_M_AXI_S2MM_AWADDR),
        .S04_AXI_awburst(axi_dma_0_M_AXI_S2MM_AWBURST),
        .S04_AXI_awcache(axi_dma_0_M_AXI_S2MM_AWCACHE),
        .S04_AXI_awlen(axi_dma_0_M_AXI_S2MM_AWLEN),
        .S04_AXI_awprot(axi_dma_0_M_AXI_S2MM_AWPROT),
        .S04_AXI_awready(axi_dma_0_M_AXI_S2MM_AWREADY),
        .S04_AXI_awsize(axi_dma_0_M_AXI_S2MM_AWSIZE),
        .S04_AXI_awvalid(axi_dma_0_M_AXI_S2MM_AWVALID),
        .S04_AXI_bready(axi_dma_0_M_AXI_S2MM_BREADY),
        .S04_AXI_bresp(axi_dma_0_M_AXI_S2MM_BRESP),
        .S04_AXI_bvalid(axi_dma_0_M_AXI_S2MM_BVALID),
        .S04_AXI_wdata(axi_dma_0_M_AXI_S2MM_WDATA),
        .S04_AXI_wlast(axi_dma_0_M_AXI_S2MM_WLAST),
        .S04_AXI_wready(axi_dma_0_M_AXI_S2MM_WREADY),
        .S04_AXI_wstrb(axi_dma_0_M_AXI_S2MM_WSTRB),
        .S04_AXI_wvalid(axi_dma_0_M_AXI_S2MM_WVALID),
        .S05_ACLK(microblaze_0_Clk),
        .S05_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .S05_AXI_araddr(axi_dma_1_M_AXI_SG_ARADDR),
        .S05_AXI_arburst(axi_dma_1_M_AXI_SG_ARBURST),
        .S05_AXI_arcache(axi_dma_1_M_AXI_SG_ARCACHE),
        .S05_AXI_arlen(axi_dma_1_M_AXI_SG_ARLEN),
        .S05_AXI_arprot(axi_dma_1_M_AXI_SG_ARPROT),
        .S05_AXI_arready(axi_dma_1_M_AXI_SG_ARREADY),
        .S05_AXI_arsize(axi_dma_1_M_AXI_SG_ARSIZE),
        .S05_AXI_arvalid(axi_dma_1_M_AXI_SG_ARVALID),
        .S05_AXI_awaddr(axi_dma_1_M_AXI_SG_AWADDR),
        .S05_AXI_awburst(axi_dma_1_M_AXI_SG_AWBURST),
        .S05_AXI_awcache(axi_dma_1_M_AXI_SG_AWCACHE),
        .S05_AXI_awlen(axi_dma_1_M_AXI_SG_AWLEN),
        .S05_AXI_awprot(axi_dma_1_M_AXI_SG_AWPROT),
        .S05_AXI_awready(axi_dma_1_M_AXI_SG_AWREADY),
        .S05_AXI_awsize(axi_dma_1_M_AXI_SG_AWSIZE),
        .S05_AXI_awvalid(axi_dma_1_M_AXI_SG_AWVALID),
        .S05_AXI_bready(axi_dma_1_M_AXI_SG_BREADY),
        .S05_AXI_bresp(axi_dma_1_M_AXI_SG_BRESP),
        .S05_AXI_bvalid(axi_dma_1_M_AXI_SG_BVALID),
        .S05_AXI_rdata(axi_dma_1_M_AXI_SG_RDATA),
        .S05_AXI_rlast(axi_dma_1_M_AXI_SG_RLAST),
        .S05_AXI_rready(axi_dma_1_M_AXI_SG_RREADY),
        .S05_AXI_rresp(axi_dma_1_M_AXI_SG_RRESP),
        .S05_AXI_rvalid(axi_dma_1_M_AXI_SG_RVALID),
        .S05_AXI_wdata(axi_dma_1_M_AXI_SG_WDATA),
        .S05_AXI_wlast(axi_dma_1_M_AXI_SG_WLAST),
        .S05_AXI_wready(axi_dma_1_M_AXI_SG_WREADY),
        .S05_AXI_wstrb(axi_dma_1_M_AXI_SG_WSTRB),
        .S05_AXI_wvalid(axi_dma_1_M_AXI_SG_WVALID),
        .S06_ACLK(microblaze_0_Clk),
        .S06_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .S06_AXI_araddr(axi_dma_1_M_AXI_MM2S_ARADDR),
        .S06_AXI_arburst(axi_dma_1_M_AXI_MM2S_ARBURST),
        .S06_AXI_arcache(axi_dma_1_M_AXI_MM2S_ARCACHE),
        .S06_AXI_arlen(axi_dma_1_M_AXI_MM2S_ARLEN),
        .S06_AXI_arprot(axi_dma_1_M_AXI_MM2S_ARPROT),
        .S06_AXI_arready(axi_dma_1_M_AXI_MM2S_ARREADY),
        .S06_AXI_arsize(axi_dma_1_M_AXI_MM2S_ARSIZE),
        .S06_AXI_arvalid(axi_dma_1_M_AXI_MM2S_ARVALID),
        .S06_AXI_rdata(axi_dma_1_M_AXI_MM2S_RDATA),
        .S06_AXI_rlast(axi_dma_1_M_AXI_MM2S_RLAST),
        .S06_AXI_rready(axi_dma_1_M_AXI_MM2S_RREADY),
        .S06_AXI_rresp(axi_dma_1_M_AXI_MM2S_RRESP),
        .S06_AXI_rvalid(axi_dma_1_M_AXI_MM2S_RVALID),
        .S07_ACLK(microblaze_0_Clk),
        .S07_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .S07_AXI_awaddr(axi_dma_1_M_AXI_S2MM_AWADDR),
        .S07_AXI_awburst(axi_dma_1_M_AXI_S2MM_AWBURST),
        .S07_AXI_awcache(axi_dma_1_M_AXI_S2MM_AWCACHE),
        .S07_AXI_awlen(axi_dma_1_M_AXI_S2MM_AWLEN),
        .S07_AXI_awprot(axi_dma_1_M_AXI_S2MM_AWPROT),
        .S07_AXI_awready(axi_dma_1_M_AXI_S2MM_AWREADY),
        .S07_AXI_awsize(axi_dma_1_M_AXI_S2MM_AWSIZE),
        .S07_AXI_awvalid(axi_dma_1_M_AXI_S2MM_AWVALID),
        .S07_AXI_bready(axi_dma_1_M_AXI_S2MM_BREADY),
        .S07_AXI_bresp(axi_dma_1_M_AXI_S2MM_BRESP),
        .S07_AXI_bvalid(axi_dma_1_M_AXI_S2MM_BVALID),
        .S07_AXI_wdata(axi_dma_1_M_AXI_S2MM_WDATA),
        .S07_AXI_wlast(axi_dma_1_M_AXI_S2MM_WLAST),
        .S07_AXI_wready(axi_dma_1_M_AXI_S2MM_WREADY),
        .S07_AXI_wstrb(axi_dma_1_M_AXI_S2MM_WSTRB),
        .S07_AXI_wvalid(axi_dma_1_M_AXI_S2MM_WVALID),
        .S08_ACLK(microblaze_0_Clk),
        .S08_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .S08_AXI_araddr(axi_dma_2_M_AXI_SG_ARADDR),
        .S08_AXI_arburst(axi_dma_2_M_AXI_SG_ARBURST),
        .S08_AXI_arcache(axi_dma_2_M_AXI_SG_ARCACHE),
        .S08_AXI_arlen(axi_dma_2_M_AXI_SG_ARLEN),
        .S08_AXI_arprot(axi_dma_2_M_AXI_SG_ARPROT),
        .S08_AXI_arready(axi_dma_2_M_AXI_SG_ARREADY),
        .S08_AXI_arsize(axi_dma_2_M_AXI_SG_ARSIZE),
        .S08_AXI_arvalid(axi_dma_2_M_AXI_SG_ARVALID),
        .S08_AXI_awaddr(axi_dma_2_M_AXI_SG_AWADDR),
        .S08_AXI_awburst(axi_dma_2_M_AXI_SG_AWBURST),
        .S08_AXI_awcache(axi_dma_2_M_AXI_SG_AWCACHE),
        .S08_AXI_awlen(axi_dma_2_M_AXI_SG_AWLEN),
        .S08_AXI_awprot(axi_dma_2_M_AXI_SG_AWPROT),
        .S08_AXI_awready(axi_dma_2_M_AXI_SG_AWREADY),
        .S08_AXI_awsize(axi_dma_2_M_AXI_SG_AWSIZE),
        .S08_AXI_awvalid(axi_dma_2_M_AXI_SG_AWVALID),
        .S08_AXI_bready(axi_dma_2_M_AXI_SG_BREADY),
        .S08_AXI_bresp(axi_dma_2_M_AXI_SG_BRESP),
        .S08_AXI_bvalid(axi_dma_2_M_AXI_SG_BVALID),
        .S08_AXI_rdata(axi_dma_2_M_AXI_SG_RDATA),
        .S08_AXI_rlast(axi_dma_2_M_AXI_SG_RLAST),
        .S08_AXI_rready(axi_dma_2_M_AXI_SG_RREADY),
        .S08_AXI_rresp(axi_dma_2_M_AXI_SG_RRESP),
        .S08_AXI_rvalid(axi_dma_2_M_AXI_SG_RVALID),
        .S08_AXI_wdata(axi_dma_2_M_AXI_SG_WDATA),
        .S08_AXI_wlast(axi_dma_2_M_AXI_SG_WLAST),
        .S08_AXI_wready(axi_dma_2_M_AXI_SG_WREADY),
        .S08_AXI_wstrb(axi_dma_2_M_AXI_SG_WSTRB),
        .S08_AXI_wvalid(axi_dma_2_M_AXI_SG_WVALID),
        .S09_ACLK(microblaze_0_Clk),
        .S09_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .S09_AXI_araddr(axi_dma_2_M_AXI_MM2S_ARADDR),
        .S09_AXI_arburst(axi_dma_2_M_AXI_MM2S_ARBURST),
        .S09_AXI_arcache(axi_dma_2_M_AXI_MM2S_ARCACHE),
        .S09_AXI_arlen(axi_dma_2_M_AXI_MM2S_ARLEN),
        .S09_AXI_arprot(axi_dma_2_M_AXI_MM2S_ARPROT),
        .S09_AXI_arready(axi_dma_2_M_AXI_MM2S_ARREADY),
        .S09_AXI_arsize(axi_dma_2_M_AXI_MM2S_ARSIZE),
        .S09_AXI_arvalid(axi_dma_2_M_AXI_MM2S_ARVALID),
        .S09_AXI_rdata(axi_dma_2_M_AXI_MM2S_RDATA),
        .S09_AXI_rlast(axi_dma_2_M_AXI_MM2S_RLAST),
        .S09_AXI_rready(axi_dma_2_M_AXI_MM2S_RREADY),
        .S09_AXI_rresp(axi_dma_2_M_AXI_MM2S_RRESP),
        .S09_AXI_rvalid(axi_dma_2_M_AXI_MM2S_RVALID),
        .S10_ACLK(microblaze_0_Clk),
        .S10_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .S10_AXI_awaddr(axi_dma_2_M_AXI_S2MM_AWADDR),
        .S10_AXI_awburst(axi_dma_2_M_AXI_S2MM_AWBURST),
        .S10_AXI_awcache(axi_dma_2_M_AXI_S2MM_AWCACHE),
        .S10_AXI_awlen(axi_dma_2_M_AXI_S2MM_AWLEN),
        .S10_AXI_awprot(axi_dma_2_M_AXI_S2MM_AWPROT),
        .S10_AXI_awready(axi_dma_2_M_AXI_S2MM_AWREADY),
        .S10_AXI_awsize(axi_dma_2_M_AXI_S2MM_AWSIZE),
        .S10_AXI_awvalid(axi_dma_2_M_AXI_S2MM_AWVALID),
        .S10_AXI_bready(axi_dma_2_M_AXI_S2MM_BREADY),
        .S10_AXI_bresp(axi_dma_2_M_AXI_S2MM_BRESP),
        .S10_AXI_bvalid(axi_dma_2_M_AXI_S2MM_BVALID),
        .S10_AXI_wdata(axi_dma_2_M_AXI_S2MM_WDATA),
        .S10_AXI_wlast(axi_dma_2_M_AXI_S2MM_WLAST),
        .S10_AXI_wready(axi_dma_2_M_AXI_S2MM_WREADY),
        .S10_AXI_wstrb(axi_dma_2_M_AXI_S2MM_WSTRB),
        .S10_AXI_wvalid(axi_dma_2_M_AXI_S2MM_WVALID),
        .S11_ACLK(microblaze_0_Clk),
        .S11_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .S11_AXI_araddr(axi_dma_3_M_AXI_SG_ARADDR),
        .S11_AXI_arburst(axi_dma_3_M_AXI_SG_ARBURST),
        .S11_AXI_arcache(axi_dma_3_M_AXI_SG_ARCACHE),
        .S11_AXI_arlen(axi_dma_3_M_AXI_SG_ARLEN),
        .S11_AXI_arprot(axi_dma_3_M_AXI_SG_ARPROT),
        .S11_AXI_arready(axi_dma_3_M_AXI_SG_ARREADY),
        .S11_AXI_arsize(axi_dma_3_M_AXI_SG_ARSIZE),
        .S11_AXI_arvalid(axi_dma_3_M_AXI_SG_ARVALID),
        .S11_AXI_awaddr(axi_dma_3_M_AXI_SG_AWADDR),
        .S11_AXI_awburst(axi_dma_3_M_AXI_SG_AWBURST),
        .S11_AXI_awcache(axi_dma_3_M_AXI_SG_AWCACHE),
        .S11_AXI_awlen(axi_dma_3_M_AXI_SG_AWLEN),
        .S11_AXI_awprot(axi_dma_3_M_AXI_SG_AWPROT),
        .S11_AXI_awready(axi_dma_3_M_AXI_SG_AWREADY),
        .S11_AXI_awsize(axi_dma_3_M_AXI_SG_AWSIZE),
        .S11_AXI_awvalid(axi_dma_3_M_AXI_SG_AWVALID),
        .S11_AXI_bready(axi_dma_3_M_AXI_SG_BREADY),
        .S11_AXI_bresp(axi_dma_3_M_AXI_SG_BRESP),
        .S11_AXI_bvalid(axi_dma_3_M_AXI_SG_BVALID),
        .S11_AXI_rdata(axi_dma_3_M_AXI_SG_RDATA),
        .S11_AXI_rlast(axi_dma_3_M_AXI_SG_RLAST),
        .S11_AXI_rready(axi_dma_3_M_AXI_SG_RREADY),
        .S11_AXI_rresp(axi_dma_3_M_AXI_SG_RRESP),
        .S11_AXI_rvalid(axi_dma_3_M_AXI_SG_RVALID),
        .S11_AXI_wdata(axi_dma_3_M_AXI_SG_WDATA),
        .S11_AXI_wlast(axi_dma_3_M_AXI_SG_WLAST),
        .S11_AXI_wready(axi_dma_3_M_AXI_SG_WREADY),
        .S11_AXI_wstrb(axi_dma_3_M_AXI_SG_WSTRB),
        .S11_AXI_wvalid(axi_dma_3_M_AXI_SG_WVALID),
        .S12_ACLK(microblaze_0_Clk),
        .S12_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .S12_AXI_araddr(axi_dma_3_M_AXI_MM2S_ARADDR),
        .S12_AXI_arburst(axi_dma_3_M_AXI_MM2S_ARBURST),
        .S12_AXI_arcache(axi_dma_3_M_AXI_MM2S_ARCACHE),
        .S12_AXI_arlen(axi_dma_3_M_AXI_MM2S_ARLEN),
        .S12_AXI_arprot(axi_dma_3_M_AXI_MM2S_ARPROT),
        .S12_AXI_arready(axi_dma_3_M_AXI_MM2S_ARREADY),
        .S12_AXI_arsize(axi_dma_3_M_AXI_MM2S_ARSIZE),
        .S12_AXI_arvalid(axi_dma_3_M_AXI_MM2S_ARVALID),
        .S12_AXI_rdata(axi_dma_3_M_AXI_MM2S_RDATA),
        .S12_AXI_rlast(axi_dma_3_M_AXI_MM2S_RLAST),
        .S12_AXI_rready(axi_dma_3_M_AXI_MM2S_RREADY),
        .S12_AXI_rresp(axi_dma_3_M_AXI_MM2S_RRESP),
        .S12_AXI_rvalid(axi_dma_3_M_AXI_MM2S_RVALID),
        .S13_ACLK(microblaze_0_Clk),
        .S13_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .S13_AXI_awaddr(axi_dma_3_M_AXI_S2MM_AWADDR),
        .S13_AXI_awburst(axi_dma_3_M_AXI_S2MM_AWBURST),
        .S13_AXI_awcache(axi_dma_3_M_AXI_S2MM_AWCACHE),
        .S13_AXI_awlen(axi_dma_3_M_AXI_S2MM_AWLEN),
        .S13_AXI_awprot(axi_dma_3_M_AXI_S2MM_AWPROT),
        .S13_AXI_awready(axi_dma_3_M_AXI_S2MM_AWREADY),
        .S13_AXI_awsize(axi_dma_3_M_AXI_S2MM_AWSIZE),
        .S13_AXI_awvalid(axi_dma_3_M_AXI_S2MM_AWVALID),
        .S13_AXI_bready(axi_dma_3_M_AXI_S2MM_BREADY),
        .S13_AXI_bresp(axi_dma_3_M_AXI_S2MM_BRESP),
        .S13_AXI_bvalid(axi_dma_3_M_AXI_S2MM_BVALID),
        .S13_AXI_wdata(axi_dma_3_M_AXI_S2MM_WDATA),
        .S13_AXI_wlast(axi_dma_3_M_AXI_S2MM_WLAST),
        .S13_AXI_wready(axi_dma_3_M_AXI_S2MM_WREADY),
        .S13_AXI_wstrb(axi_dma_3_M_AXI_S2MM_WSTRB),
        .S13_AXI_wvalid(axi_dma_3_M_AXI_S2MM_WVALID));
  design_1_axi_timer_0_0 axi_timer_0
       (.capturetrig0(1'b0),
        .capturetrig1(1'b0),
        .freeze(1'b0),
        .s_axi_aclk(microblaze_0_Clk),
        .s_axi_araddr(microblaze_0_axi_periph_M01_AXI_ARADDR[4:0]),
        .s_axi_aresetn(rst_mig_7series_0_100M_peripheral_aresetn),
        .s_axi_arready(microblaze_0_axi_periph_M01_AXI_ARREADY),
        .s_axi_arvalid(microblaze_0_axi_periph_M01_AXI_ARVALID),
        .s_axi_awaddr(microblaze_0_axi_periph_M01_AXI_AWADDR[4:0]),
        .s_axi_awready(microblaze_0_axi_periph_M01_AXI_AWREADY),
        .s_axi_awvalid(microblaze_0_axi_periph_M01_AXI_AWVALID),
        .s_axi_bready(microblaze_0_axi_periph_M01_AXI_BREADY),
        .s_axi_bresp(microblaze_0_axi_periph_M01_AXI_BRESP),
        .s_axi_bvalid(microblaze_0_axi_periph_M01_AXI_BVALID),
        .s_axi_rdata(microblaze_0_axi_periph_M01_AXI_RDATA),
        .s_axi_rready(microblaze_0_axi_periph_M01_AXI_RREADY),
        .s_axi_rresp(microblaze_0_axi_periph_M01_AXI_RRESP),
        .s_axi_rvalid(microblaze_0_axi_periph_M01_AXI_RVALID),
        .s_axi_wdata(microblaze_0_axi_periph_M01_AXI_WDATA),
        .s_axi_wready(microblaze_0_axi_periph_M01_AXI_WREADY),
        .s_axi_wstrb(microblaze_0_axi_periph_M01_AXI_WSTRB),
        .s_axi_wvalid(microblaze_0_axi_periph_M01_AXI_WVALID));
  design_1_axis_data_fifo_0_0 axis_data_fifo_0
       (.m_axis_tdata(axis_data_fifo_0_M_AXIS_TDATA),
        .m_axis_tdest(axis_data_fifo_0_M_AXIS_TDEST),
        .m_axis_tid(axis_data_fifo_0_M_AXIS_TID),
        .m_axis_tkeep(axis_data_fifo_0_M_AXIS_TKEEP),
        .m_axis_tlast(axis_data_fifo_0_M_AXIS_TLAST),
        .m_axis_tready(axis_data_fifo_0_M_AXIS_TREADY),
        .m_axis_tuser(axis_data_fifo_0_M_AXIS_TUSER),
        .m_axis_tvalid(axis_data_fifo_0_M_AXIS_TVALID),
        .s_axis_aclk(microblaze_0_Clk),
        .s_axis_aresetn(rst_mig_7series_0_100M_peripheral_aresetn),
        .s_axis_tdata(pcc_stream_0_DMA_pcc_TDATA),
        .s_axis_tdest(pcc_stream_0_DMA_pcc_TDEST),
        .s_axis_tid(pcc_stream_0_DMA_pcc_TID),
        .s_axis_tkeep(pcc_stream_0_DMA_pcc_TKEEP),
        .s_axis_tlast(pcc_stream_0_DMA_pcc_TLAST),
        .s_axis_tready(pcc_stream_0_DMA_pcc_TREADY),
        .s_axis_tstrb(pcc_stream_0_DMA_pcc_TSTRB),
        .s_axis_tuser(pcc_stream_0_DMA_pcc_TUSER),
        .s_axis_tvalid(pcc_stream_0_DMA_pcc_TVALID));
  design_1_axis_data_fifo_0_1 axis_data_fifo_1
       (.m_axis_tdata(axis_data_fifo_1_M_AXIS_TDATA),
        .m_axis_tdest(axis_data_fifo_1_M_AXIS_TDEST),
        .m_axis_tid(axis_data_fifo_1_M_AXIS_TID),
        .m_axis_tkeep(axis_data_fifo_1_M_AXIS_TKEEP),
        .m_axis_tlast(axis_data_fifo_1_M_AXIS_TLAST),
        .m_axis_tready(axis_data_fifo_1_M_AXIS_TREADY),
        .m_axis_tuser(axis_data_fifo_1_M_AXIS_TUSER),
        .m_axis_tvalid(axis_data_fifo_1_M_AXIS_TVALID),
        .s_axis_aclk(microblaze_0_Clk),
        .s_axis_aresetn(rst_mig_7series_0_100M_peripheral_aresetn),
        .s_axis_tdata(pcc_stream_1_DMA_pcc_TDATA),
        .s_axis_tdest(pcc_stream_1_DMA_pcc_TDEST),
        .s_axis_tid(pcc_stream_1_DMA_pcc_TID),
        .s_axis_tkeep(pcc_stream_1_DMA_pcc_TKEEP),
        .s_axis_tlast(pcc_stream_1_DMA_pcc_TLAST),
        .s_axis_tready(pcc_stream_1_DMA_pcc_TREADY),
        .s_axis_tstrb(pcc_stream_1_DMA_pcc_TSTRB),
        .s_axis_tuser(pcc_stream_1_DMA_pcc_TUSER),
        .s_axis_tvalid(pcc_stream_1_DMA_pcc_TVALID));
  design_1_axis_data_fifo_0_2 axis_data_fifo_2
       (.m_axis_tdata(axis_data_fifo_2_M_AXIS_TDATA),
        .m_axis_tdest(axis_data_fifo_2_M_AXIS_TDEST),
        .m_axis_tid(axis_data_fifo_2_M_AXIS_TID),
        .m_axis_tkeep(axis_data_fifo_2_M_AXIS_TKEEP),
        .m_axis_tlast(axis_data_fifo_2_M_AXIS_TLAST),
        .m_axis_tready(axis_data_fifo_2_M_AXIS_TREADY),
        .m_axis_tuser(axis_data_fifo_2_M_AXIS_TUSER),
        .m_axis_tvalid(axis_data_fifo_2_M_AXIS_TVALID),
        .s_axis_aclk(microblaze_0_Clk),
        .s_axis_aresetn(rst_mig_7series_0_100M_peripheral_aresetn),
        .s_axis_tdata(pcc_stream_2_DMA_pcc_TDATA),
        .s_axis_tdest(pcc_stream_2_DMA_pcc_TDEST),
        .s_axis_tid(pcc_stream_2_DMA_pcc_TID),
        .s_axis_tkeep(pcc_stream_2_DMA_pcc_TKEEP),
        .s_axis_tlast(pcc_stream_2_DMA_pcc_TLAST),
        .s_axis_tready(pcc_stream_2_DMA_pcc_TREADY),
        .s_axis_tstrb(pcc_stream_2_DMA_pcc_TSTRB),
        .s_axis_tuser(pcc_stream_2_DMA_pcc_TUSER),
        .s_axis_tvalid(pcc_stream_2_DMA_pcc_TVALID));
  design_1_axis_data_fifo_0_3 axis_data_fifo_3
       (.m_axis_tdata(axis_data_fifo_3_M_AXIS_TDATA),
        .m_axis_tdest(axis_data_fifo_3_M_AXIS_TDEST),
        .m_axis_tid(axis_data_fifo_3_M_AXIS_TID),
        .m_axis_tkeep(axis_data_fifo_3_M_AXIS_TKEEP),
        .m_axis_tlast(axis_data_fifo_3_M_AXIS_TLAST),
        .m_axis_tready(axis_data_fifo_3_M_AXIS_TREADY),
        .m_axis_tuser(axis_data_fifo_3_M_AXIS_TUSER),
        .m_axis_tvalid(axis_data_fifo_3_M_AXIS_TVALID),
        .s_axis_aclk(microblaze_0_Clk),
        .s_axis_aresetn(rst_mig_7series_0_100M_peripheral_aresetn),
        .s_axis_tdata(pcc_stream_3_DMA_pcc_TDATA),
        .s_axis_tdest(pcc_stream_3_DMA_pcc_TDEST),
        .s_axis_tid(pcc_stream_3_DMA_pcc_TID),
        .s_axis_tkeep(pcc_stream_3_DMA_pcc_TKEEP),
        .s_axis_tlast(pcc_stream_3_DMA_pcc_TLAST),
        .s_axis_tready(pcc_stream_3_DMA_pcc_TREADY),
        .s_axis_tstrb(pcc_stream_3_DMA_pcc_TSTRB),
        .s_axis_tuser(pcc_stream_3_DMA_pcc_TUSER),
        .s_axis_tvalid(pcc_stream_3_DMA_pcc_TVALID));
  design_1_axis_interconnect_0_0 axis_interconnect_0
       (.ACLK(microblaze_0_Clk),
        .ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .M00_AXIS_ACLK(microblaze_0_Clk),
        .M00_AXIS_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .M00_AXIS_tdata(axis_interconnect_0_M00_AXIS_TDATA),
        .M00_AXIS_tdest(axis_interconnect_0_M00_AXIS_TDEST),
        .M00_AXIS_tid(axis_interconnect_0_M00_AXIS_TID),
        .M00_AXIS_tkeep(axis_interconnect_0_M00_AXIS_TKEEP),
        .M00_AXIS_tlast(axis_interconnect_0_M00_AXIS_TLAST),
        .M00_AXIS_tready(axis_interconnect_0_M00_AXIS_TREADY),
        .M00_AXIS_tstrb(axis_interconnect_0_M00_AXIS_TSTRB),
        .M00_AXIS_tuser(axis_interconnect_0_M00_AXIS_TUSER),
        .M00_AXIS_tvalid(axis_interconnect_0_M00_AXIS_TVALID),
        .M01_AXIS_ACLK(microblaze_0_Clk),
        .M01_AXIS_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .M01_AXIS_tdata(axis_interconnect_0_M01_AXIS_TDATA),
        .M01_AXIS_tdest(axis_interconnect_0_M01_AXIS_TDEST),
        .M01_AXIS_tid(axis_interconnect_0_M01_AXIS_TID),
        .M01_AXIS_tkeep(axis_interconnect_0_M01_AXIS_TKEEP),
        .M01_AXIS_tlast(axis_interconnect_0_M01_AXIS_TLAST),
        .M01_AXIS_tready(axis_interconnect_0_M01_AXIS_TREADY),
        .M01_AXIS_tstrb(axis_interconnect_0_M01_AXIS_TSTRB),
        .M01_AXIS_tuser(axis_interconnect_0_M01_AXIS_TUSER),
        .M01_AXIS_tvalid(axis_interconnect_0_M01_AXIS_TVALID),
        .M02_AXIS_ACLK(microblaze_0_Clk),
        .M02_AXIS_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .M02_AXIS_tdata(axis_interconnect_0_M02_AXIS_TDATA),
        .M02_AXIS_tdest(axis_interconnect_0_M02_AXIS_TDEST),
        .M02_AXIS_tid(axis_interconnect_0_M02_AXIS_TID),
        .M02_AXIS_tkeep(axis_interconnect_0_M02_AXIS_TKEEP),
        .M02_AXIS_tlast(axis_interconnect_0_M02_AXIS_TLAST),
        .M02_AXIS_tready(axis_interconnect_0_M02_AXIS_TREADY),
        .M02_AXIS_tstrb(axis_interconnect_0_M02_AXIS_TSTRB),
        .M02_AXIS_tuser(axis_interconnect_0_M02_AXIS_TUSER),
        .M02_AXIS_tvalid(axis_interconnect_0_M02_AXIS_TVALID),
        .S00_AXIS_ACLK(microblaze_0_Clk),
        .S00_AXIS_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .S00_AXIS_tdata(axi_dma_0_M_AXIS_MM2S_TDATA),
        .S00_AXIS_tdest(axi_dma_0_M_AXIS_MM2S_TDEST),
        .S00_AXIS_tid(axi_dma_0_M_AXIS_MM2S_TID),
        .S00_AXIS_tkeep(axi_dma_0_M_AXIS_MM2S_TKEEP),
        .S00_AXIS_tlast(axi_dma_0_M_AXIS_MM2S_TLAST),
        .S00_AXIS_tready(axi_dma_0_M_AXIS_MM2S_TREADY),
        .S00_AXIS_tuser(axi_dma_0_M_AXIS_MM2S_TUSER),
        .S00_AXIS_tvalid(axi_dma_0_M_AXIS_MM2S_TVALID));
  design_1_axis_interconnect_0_1 axis_interconnect_1
       (.ACLK(microblaze_0_Clk),
        .ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .M00_AXIS_ACLK(microblaze_0_Clk),
        .M00_AXIS_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .M00_AXIS_tdata(axis_interconnect_1_M00_AXIS_TDATA),
        .M00_AXIS_tdest(axis_interconnect_1_M00_AXIS_TDEST),
        .M00_AXIS_tid(axis_interconnect_1_M00_AXIS_TID),
        .M00_AXIS_tkeep(axis_interconnect_1_M00_AXIS_TKEEP),
        .M00_AXIS_tlast(axis_interconnect_1_M00_AXIS_TLAST),
        .M00_AXIS_tready(axis_interconnect_1_M00_AXIS_TREADY),
        .M00_AXIS_tstrb(axis_interconnect_1_M00_AXIS_TSTRB),
        .M00_AXIS_tuser(axis_interconnect_1_M00_AXIS_TUSER),
        .M00_AXIS_tvalid(axis_interconnect_1_M00_AXIS_TVALID),
        .M01_AXIS_ACLK(microblaze_0_Clk),
        .M01_AXIS_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .M01_AXIS_tdata(axis_interconnect_1_M01_AXIS_TDATA),
        .M01_AXIS_tdest(axis_interconnect_1_M01_AXIS_TDEST),
        .M01_AXIS_tid(axis_interconnect_1_M01_AXIS_TID),
        .M01_AXIS_tkeep(axis_interconnect_1_M01_AXIS_TKEEP),
        .M01_AXIS_tlast(axis_interconnect_1_M01_AXIS_TLAST),
        .M01_AXIS_tready(axis_interconnect_1_M01_AXIS_TREADY),
        .M01_AXIS_tstrb(axis_interconnect_1_M01_AXIS_TSTRB),
        .M01_AXIS_tuser(axis_interconnect_1_M01_AXIS_TUSER),
        .M01_AXIS_tvalid(axis_interconnect_1_M01_AXIS_TVALID),
        .M02_AXIS_ACLK(microblaze_0_Clk),
        .M02_AXIS_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .M02_AXIS_tdata(axis_interconnect_1_M02_AXIS_TDATA),
        .M02_AXIS_tdest(axis_interconnect_1_M02_AXIS_TDEST),
        .M02_AXIS_tid(axis_interconnect_1_M02_AXIS_TID),
        .M02_AXIS_tkeep(axis_interconnect_1_M02_AXIS_TKEEP),
        .M02_AXIS_tlast(axis_interconnect_1_M02_AXIS_TLAST),
        .M02_AXIS_tready(axis_interconnect_1_M02_AXIS_TREADY),
        .M02_AXIS_tstrb(axis_interconnect_1_M02_AXIS_TSTRB),
        .M02_AXIS_tuser(axis_interconnect_1_M02_AXIS_TUSER),
        .M02_AXIS_tvalid(axis_interconnect_1_M02_AXIS_TVALID),
        .S00_AXIS_ACLK(microblaze_0_Clk),
        .S00_AXIS_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .S00_AXIS_tdata(axi_dma_1_M_AXIS_MM2S_TDATA),
        .S00_AXIS_tdest(axi_dma_1_M_AXIS_MM2S_TDEST),
        .S00_AXIS_tid(axi_dma_1_M_AXIS_MM2S_TID),
        .S00_AXIS_tkeep(axi_dma_1_M_AXIS_MM2S_TKEEP),
        .S00_AXIS_tlast(axi_dma_1_M_AXIS_MM2S_TLAST),
        .S00_AXIS_tready(axi_dma_1_M_AXIS_MM2S_TREADY),
        .S00_AXIS_tuser(axi_dma_1_M_AXIS_MM2S_TUSER),
        .S00_AXIS_tvalid(axi_dma_1_M_AXIS_MM2S_TVALID));
  design_1_axis_interconnect_0_2 axis_interconnect_2
       (.ACLK(microblaze_0_Clk),
        .ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .M00_AXIS_ACLK(microblaze_0_Clk),
        .M00_AXIS_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .M00_AXIS_tdata(axis_interconnect_2_M00_AXIS_TDATA),
        .M00_AXIS_tdest(axis_interconnect_2_M00_AXIS_TDEST),
        .M00_AXIS_tid(axis_interconnect_2_M00_AXIS_TID),
        .M00_AXIS_tkeep(axis_interconnect_2_M00_AXIS_TKEEP),
        .M00_AXIS_tlast(axis_interconnect_2_M00_AXIS_TLAST),
        .M00_AXIS_tready(axis_interconnect_2_M00_AXIS_TREADY),
        .M00_AXIS_tstrb(axis_interconnect_2_M00_AXIS_TSTRB),
        .M00_AXIS_tuser(axis_interconnect_2_M00_AXIS_TUSER),
        .M00_AXIS_tvalid(axis_interconnect_2_M00_AXIS_TVALID),
        .M01_AXIS_ACLK(microblaze_0_Clk),
        .M01_AXIS_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .M01_AXIS_tdata(axis_interconnect_2_M01_AXIS_TDATA),
        .M01_AXIS_tdest(axis_interconnect_2_M01_AXIS_TDEST),
        .M01_AXIS_tid(axis_interconnect_2_M01_AXIS_TID),
        .M01_AXIS_tkeep(axis_interconnect_2_M01_AXIS_TKEEP),
        .M01_AXIS_tlast(axis_interconnect_2_M01_AXIS_TLAST),
        .M01_AXIS_tready(axis_interconnect_2_M01_AXIS_TREADY),
        .M01_AXIS_tstrb(axis_interconnect_2_M01_AXIS_TSTRB),
        .M01_AXIS_tuser(axis_interconnect_2_M01_AXIS_TUSER),
        .M01_AXIS_tvalid(axis_interconnect_2_M01_AXIS_TVALID),
        .M02_AXIS_ACLK(microblaze_0_Clk),
        .M02_AXIS_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .M02_AXIS_tdata(axis_interconnect_2_M02_AXIS_TDATA),
        .M02_AXIS_tdest(axis_interconnect_2_M02_AXIS_TDEST),
        .M02_AXIS_tid(axis_interconnect_2_M02_AXIS_TID),
        .M02_AXIS_tkeep(axis_interconnect_2_M02_AXIS_TKEEP),
        .M02_AXIS_tlast(axis_interconnect_2_M02_AXIS_TLAST),
        .M02_AXIS_tready(axis_interconnect_2_M02_AXIS_TREADY),
        .M02_AXIS_tstrb(axis_interconnect_2_M02_AXIS_TSTRB),
        .M02_AXIS_tuser(axis_interconnect_2_M02_AXIS_TUSER),
        .M02_AXIS_tvalid(axis_interconnect_2_M02_AXIS_TVALID),
        .S00_AXIS_ACLK(microblaze_0_Clk),
        .S00_AXIS_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .S00_AXIS_tdata(axi_dma_2_M_AXIS_MM2S_TDATA),
        .S00_AXIS_tdest(axi_dma_2_M_AXIS_MM2S_TDEST),
        .S00_AXIS_tid(axi_dma_2_M_AXIS_MM2S_TID),
        .S00_AXIS_tkeep(axi_dma_2_M_AXIS_MM2S_TKEEP),
        .S00_AXIS_tlast(axi_dma_2_M_AXIS_MM2S_TLAST),
        .S00_AXIS_tready(axi_dma_2_M_AXIS_MM2S_TREADY),
        .S00_AXIS_tuser(axi_dma_2_M_AXIS_MM2S_TUSER),
        .S00_AXIS_tvalid(axi_dma_2_M_AXIS_MM2S_TVALID));
  design_1_axis_interconnect_0_3 axis_interconnect_3
       (.ACLK(microblaze_0_Clk),
        .ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .M00_AXIS_ACLK(microblaze_0_Clk),
        .M00_AXIS_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .M00_AXIS_tdata(axis_interconnect_3_M00_AXIS_TDATA),
        .M00_AXIS_tdest(axis_interconnect_3_M00_AXIS_TDEST),
        .M00_AXIS_tid(axis_interconnect_3_M00_AXIS_TID),
        .M00_AXIS_tkeep(axis_interconnect_3_M00_AXIS_TKEEP),
        .M00_AXIS_tlast(axis_interconnect_3_M00_AXIS_TLAST),
        .M00_AXIS_tready(axis_interconnect_3_M00_AXIS_TREADY),
        .M00_AXIS_tstrb(axis_interconnect_3_M00_AXIS_TSTRB),
        .M00_AXIS_tuser(axis_interconnect_3_M00_AXIS_TUSER),
        .M00_AXIS_tvalid(axis_interconnect_3_M00_AXIS_TVALID),
        .M01_AXIS_ACLK(microblaze_0_Clk),
        .M01_AXIS_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .M01_AXIS_tdata(axis_interconnect_3_M01_AXIS_TDATA),
        .M01_AXIS_tdest(axis_interconnect_3_M01_AXIS_TDEST),
        .M01_AXIS_tid(axis_interconnect_3_M01_AXIS_TID),
        .M01_AXIS_tkeep(axis_interconnect_3_M01_AXIS_TKEEP),
        .M01_AXIS_tlast(axis_interconnect_3_M01_AXIS_TLAST),
        .M01_AXIS_tready(axis_interconnect_3_M01_AXIS_TREADY),
        .M01_AXIS_tstrb(axis_interconnect_3_M01_AXIS_TSTRB),
        .M01_AXIS_tuser(axis_interconnect_3_M01_AXIS_TUSER),
        .M01_AXIS_tvalid(axis_interconnect_3_M01_AXIS_TVALID),
        .M02_AXIS_ACLK(microblaze_0_Clk),
        .M02_AXIS_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .M02_AXIS_tdata(axis_interconnect_3_M02_AXIS_TDATA),
        .M02_AXIS_tdest(axis_interconnect_3_M02_AXIS_TDEST),
        .M02_AXIS_tid(axis_interconnect_3_M02_AXIS_TID),
        .M02_AXIS_tkeep(axis_interconnect_3_M02_AXIS_TKEEP),
        .M02_AXIS_tlast(axis_interconnect_3_M02_AXIS_TLAST),
        .M02_AXIS_tready(axis_interconnect_3_M02_AXIS_TREADY),
        .M02_AXIS_tstrb(axis_interconnect_3_M02_AXIS_TSTRB),
        .M02_AXIS_tuser(axis_interconnect_3_M02_AXIS_TUSER),
        .M02_AXIS_tvalid(axis_interconnect_3_M02_AXIS_TVALID),
        .S00_AXIS_ACLK(microblaze_0_Clk),
        .S00_AXIS_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .S00_AXIS_tdata(axi_dma_3_M_AXIS_MM2S_TDATA),
        .S00_AXIS_tdest(axi_dma_3_M_AXIS_MM2S_TDEST),
        .S00_AXIS_tid(axi_dma_3_M_AXIS_MM2S_TID),
        .S00_AXIS_tkeep(axi_dma_3_M_AXIS_MM2S_TKEEP),
        .S00_AXIS_tlast(axi_dma_3_M_AXIS_MM2S_TLAST),
        .S00_AXIS_tready(axi_dma_3_M_AXIS_MM2S_TREADY),
        .S00_AXIS_tuser(axi_dma_3_M_AXIS_MM2S_TUSER),
        .S00_AXIS_tvalid(axi_dma_3_M_AXIS_MM2S_TVALID));
  design_1_mdm_1_0 mdm_1
       (.Dbg_Capture_0(microblaze_0_debug_CAPTURE),
        .Dbg_Clk_0(microblaze_0_debug_CLK),
        .Dbg_Reg_En_0(microblaze_0_debug_REG_EN),
        .Dbg_Rst_0(microblaze_0_debug_RST),
        .Dbg_Shift_0(microblaze_0_debug_SHIFT),
        .Dbg_TDI_0(microblaze_0_debug_TDI),
        .Dbg_TDO_0(microblaze_0_debug_TDO),
        .Dbg_Update_0(microblaze_0_debug_UPDATE),
        .Debug_SYS_Rst(mdm_1_debug_sys_rst),
        .S_AXI_ACLK(microblaze_0_Clk),
        .S_AXI_ARADDR(microblaze_0_axi_periph_M00_AXI_ARADDR),
        .S_AXI_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .S_AXI_ARREADY(microblaze_0_axi_periph_M00_AXI_ARREADY),
        .S_AXI_ARVALID(microblaze_0_axi_periph_M00_AXI_ARVALID),
        .S_AXI_AWADDR(microblaze_0_axi_periph_M00_AXI_AWADDR),
        .S_AXI_AWREADY(microblaze_0_axi_periph_M00_AXI_AWREADY),
        .S_AXI_AWVALID(microblaze_0_axi_periph_M00_AXI_AWVALID),
        .S_AXI_BREADY(microblaze_0_axi_periph_M00_AXI_BREADY),
        .S_AXI_BRESP(microblaze_0_axi_periph_M00_AXI_BRESP),
        .S_AXI_BVALID(microblaze_0_axi_periph_M00_AXI_BVALID),
        .S_AXI_RDATA(microblaze_0_axi_periph_M00_AXI_RDATA),
        .S_AXI_RREADY(microblaze_0_axi_periph_M00_AXI_RREADY),
        .S_AXI_RRESP(microblaze_0_axi_periph_M00_AXI_RRESP),
        .S_AXI_RVALID(microblaze_0_axi_periph_M00_AXI_RVALID),
        .S_AXI_WDATA(microblaze_0_axi_periph_M00_AXI_WDATA),
        .S_AXI_WREADY(microblaze_0_axi_periph_M00_AXI_WREADY),
        .S_AXI_WSTRB(microblaze_0_axi_periph_M00_AXI_WSTRB),
        .S_AXI_WVALID(microblaze_0_axi_periph_M00_AXI_WVALID));
  (* BMM_INFO_PROCESSOR = "microblaze-le > design_1 microblaze_0_local_memory/dlmb_bram_if_cntlr" *) 
  (* KEEP_HIERARCHY = "yes" *) 
  design_1_microblaze_0_0 microblaze_0
       (.Byte_Enable(microblaze_0_dlmb_1_BE),
        .Clk(microblaze_0_Clk),
        .DCE(microblaze_0_dlmb_1_CE),
        .DReady(microblaze_0_dlmb_1_READY),
        .DUE(microblaze_0_dlmb_1_UE),
        .DWait(microblaze_0_dlmb_1_WAIT),
        .D_AS(microblaze_0_dlmb_1_ADDRSTROBE),
        .Data_Addr(microblaze_0_dlmb_1_ABUS),
        .Data_Read(microblaze_0_dlmb_1_READDBUS),
        .Data_Write(microblaze_0_dlmb_1_WRITEDBUS),
        .Dbg_Capture(microblaze_0_debug_CAPTURE),
        .Dbg_Clk(microblaze_0_debug_CLK),
        .Dbg_Reg_En(microblaze_0_debug_REG_EN),
        .Dbg_Shift(microblaze_0_debug_SHIFT),
        .Dbg_TDI(microblaze_0_debug_TDI),
        .Dbg_TDO(microblaze_0_debug_TDO),
        .Dbg_Update(microblaze_0_debug_UPDATE),
        .Debug_Rst(microblaze_0_debug_RST),
        .ICE(microblaze_0_ilmb_1_CE),
        .IFetch(microblaze_0_ilmb_1_READSTROBE),
        .IReady(microblaze_0_ilmb_1_READY),
        .IUE(microblaze_0_ilmb_1_UE),
        .IWAIT(microblaze_0_ilmb_1_WAIT),
        .I_AS(microblaze_0_ilmb_1_ADDRSTROBE),
        .Instr(microblaze_0_ilmb_1_READDBUS),
        .Instr_Addr(microblaze_0_ilmb_1_ABUS),
        .Interrupt(axi_intc_0_interrupt_INTERRUPT),
        .Interrupt_Ack(axi_intc_0_interrupt_ACK),
        .Interrupt_Address({axi_intc_0_interrupt_ADDRESS[31],axi_intc_0_interrupt_ADDRESS[30],axi_intc_0_interrupt_ADDRESS[29],axi_intc_0_interrupt_ADDRESS[28],axi_intc_0_interrupt_ADDRESS[27],axi_intc_0_interrupt_ADDRESS[26],axi_intc_0_interrupt_ADDRESS[25],axi_intc_0_interrupt_ADDRESS[24],axi_intc_0_interrupt_ADDRESS[23],axi_intc_0_interrupt_ADDRESS[22],axi_intc_0_interrupt_ADDRESS[21],axi_intc_0_interrupt_ADDRESS[20],axi_intc_0_interrupt_ADDRESS[19],axi_intc_0_interrupt_ADDRESS[18],axi_intc_0_interrupt_ADDRESS[17],axi_intc_0_interrupt_ADDRESS[16],axi_intc_0_interrupt_ADDRESS[15],axi_intc_0_interrupt_ADDRESS[14],axi_intc_0_interrupt_ADDRESS[13],axi_intc_0_interrupt_ADDRESS[12],axi_intc_0_interrupt_ADDRESS[11],axi_intc_0_interrupt_ADDRESS[10],axi_intc_0_interrupt_ADDRESS[9],axi_intc_0_interrupt_ADDRESS[8],axi_intc_0_interrupt_ADDRESS[7],axi_intc_0_interrupt_ADDRESS[6],axi_intc_0_interrupt_ADDRESS[5],axi_intc_0_interrupt_ADDRESS[4],axi_intc_0_interrupt_ADDRESS[3],axi_intc_0_interrupt_ADDRESS[2],axi_intc_0_interrupt_ADDRESS[1],axi_intc_0_interrupt_ADDRESS[0]}),
        .M_AXI_DC_ARADDR(microblaze_0_M_AXI_DC_ARADDR),
        .M_AXI_DC_ARBURST(microblaze_0_M_AXI_DC_ARBURST),
        .M_AXI_DC_ARCACHE(microblaze_0_M_AXI_DC_ARCACHE),
        .M_AXI_DC_ARLEN(microblaze_0_M_AXI_DC_ARLEN),
        .M_AXI_DC_ARLOCK(microblaze_0_M_AXI_DC_ARLOCK),
        .M_AXI_DC_ARPROT(microblaze_0_M_AXI_DC_ARPROT),
        .M_AXI_DC_ARQOS(microblaze_0_M_AXI_DC_ARQOS),
        .M_AXI_DC_ARREADY(microblaze_0_M_AXI_DC_ARREADY),
        .M_AXI_DC_ARSIZE(microblaze_0_M_AXI_DC_ARSIZE),
        .M_AXI_DC_ARVALID(microblaze_0_M_AXI_DC_ARVALID),
        .M_AXI_DC_AWADDR(microblaze_0_M_AXI_DC_AWADDR),
        .M_AXI_DC_AWBURST(microblaze_0_M_AXI_DC_AWBURST),
        .M_AXI_DC_AWCACHE(microblaze_0_M_AXI_DC_AWCACHE),
        .M_AXI_DC_AWLEN(microblaze_0_M_AXI_DC_AWLEN),
        .M_AXI_DC_AWLOCK(microblaze_0_M_AXI_DC_AWLOCK),
        .M_AXI_DC_AWPROT(microblaze_0_M_AXI_DC_AWPROT),
        .M_AXI_DC_AWQOS(microblaze_0_M_AXI_DC_AWQOS),
        .M_AXI_DC_AWREADY(microblaze_0_M_AXI_DC_AWREADY),
        .M_AXI_DC_AWSIZE(microblaze_0_M_AXI_DC_AWSIZE),
        .M_AXI_DC_AWVALID(microblaze_0_M_AXI_DC_AWVALID),
        .M_AXI_DC_BID(1'b0),
        .M_AXI_DC_BREADY(microblaze_0_M_AXI_DC_BREADY),
        .M_AXI_DC_BRESP(microblaze_0_M_AXI_DC_BRESP),
        .M_AXI_DC_BVALID(microblaze_0_M_AXI_DC_BVALID),
        .M_AXI_DC_RDATA(microblaze_0_M_AXI_DC_RDATA),
        .M_AXI_DC_RID(1'b0),
        .M_AXI_DC_RLAST(microblaze_0_M_AXI_DC_RLAST),
        .M_AXI_DC_RREADY(microblaze_0_M_AXI_DC_RREADY),
        .M_AXI_DC_RRESP(microblaze_0_M_AXI_DC_RRESP),
        .M_AXI_DC_RVALID(microblaze_0_M_AXI_DC_RVALID),
        .M_AXI_DC_WDATA(microblaze_0_M_AXI_DC_WDATA),
        .M_AXI_DC_WLAST(microblaze_0_M_AXI_DC_WLAST),
        .M_AXI_DC_WREADY(microblaze_0_M_AXI_DC_WREADY),
        .M_AXI_DC_WSTRB(microblaze_0_M_AXI_DC_WSTRB),
        .M_AXI_DC_WVALID(microblaze_0_M_AXI_DC_WVALID),
        .M_AXI_DP_ARADDR(microblaze_0_M_AXI_DP_ARADDR),
        .M_AXI_DP_ARPROT(microblaze_0_M_AXI_DP_ARPROT),
        .M_AXI_DP_ARREADY(microblaze_0_M_AXI_DP_ARREADY),
        .M_AXI_DP_ARVALID(microblaze_0_M_AXI_DP_ARVALID),
        .M_AXI_DP_AWADDR(microblaze_0_M_AXI_DP_AWADDR),
        .M_AXI_DP_AWPROT(microblaze_0_M_AXI_DP_AWPROT),
        .M_AXI_DP_AWREADY(microblaze_0_M_AXI_DP_AWREADY),
        .M_AXI_DP_AWVALID(microblaze_0_M_AXI_DP_AWVALID),
        .M_AXI_DP_BREADY(microblaze_0_M_AXI_DP_BREADY),
        .M_AXI_DP_BRESP(microblaze_0_M_AXI_DP_BRESP),
        .M_AXI_DP_BVALID(microblaze_0_M_AXI_DP_BVALID),
        .M_AXI_DP_RDATA(microblaze_0_M_AXI_DP_RDATA),
        .M_AXI_DP_RREADY(microblaze_0_M_AXI_DP_RREADY),
        .M_AXI_DP_RRESP(microblaze_0_M_AXI_DP_RRESP),
        .M_AXI_DP_RVALID(microblaze_0_M_AXI_DP_RVALID),
        .M_AXI_DP_WDATA(microblaze_0_M_AXI_DP_WDATA),
        .M_AXI_DP_WREADY(microblaze_0_M_AXI_DP_WREADY),
        .M_AXI_DP_WSTRB(microblaze_0_M_AXI_DP_WSTRB),
        .M_AXI_DP_WVALID(microblaze_0_M_AXI_DP_WVALID),
        .M_AXI_IC_ARADDR(microblaze_0_M_AXI_IC_ARADDR),
        .M_AXI_IC_ARBURST(microblaze_0_M_AXI_IC_ARBURST),
        .M_AXI_IC_ARCACHE(microblaze_0_M_AXI_IC_ARCACHE),
        .M_AXI_IC_ARLEN(microblaze_0_M_AXI_IC_ARLEN),
        .M_AXI_IC_ARLOCK(microblaze_0_M_AXI_IC_ARLOCK),
        .M_AXI_IC_ARPROT(microblaze_0_M_AXI_IC_ARPROT),
        .M_AXI_IC_ARQOS(microblaze_0_M_AXI_IC_ARQOS),
        .M_AXI_IC_ARREADY(microblaze_0_M_AXI_IC_ARREADY),
        .M_AXI_IC_ARSIZE(microblaze_0_M_AXI_IC_ARSIZE),
        .M_AXI_IC_ARVALID(microblaze_0_M_AXI_IC_ARVALID),
        .M_AXI_IC_AWREADY(1'b0),
        .M_AXI_IC_BID(1'b0),
        .M_AXI_IC_BRESP({1'b0,1'b0}),
        .M_AXI_IC_BVALID(1'b0),
        .M_AXI_IC_RDATA(microblaze_0_M_AXI_IC_RDATA),
        .M_AXI_IC_RID(1'b0),
        .M_AXI_IC_RLAST(microblaze_0_M_AXI_IC_RLAST),
        .M_AXI_IC_RREADY(microblaze_0_M_AXI_IC_RREADY),
        .M_AXI_IC_RRESP(microblaze_0_M_AXI_IC_RRESP),
        .M_AXI_IC_RVALID(microblaze_0_M_AXI_IC_RVALID),
        .M_AXI_IC_WREADY(1'b0),
        .Read_Strobe(microblaze_0_dlmb_1_READSTROBE),
        .Reset(rst_mig_7series_0_100M_mb_reset),
        .Write_Strobe(microblaze_0_dlmb_1_WRITESTROBE));
  design_1_microblaze_0_axi_periph_0 microblaze_0_axi_periph
       (.ACLK(microblaze_0_Clk),
        .ARESETN(rst_mig_7series_0_100M_interconnect_aresetn),
        .M00_ACLK(microblaze_0_Clk),
        .M00_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .M00_AXI_araddr(microblaze_0_axi_periph_M00_AXI_ARADDR),
        .M00_AXI_arready(microblaze_0_axi_periph_M00_AXI_ARREADY),
        .M00_AXI_arvalid(microblaze_0_axi_periph_M00_AXI_ARVALID),
        .M00_AXI_awaddr(microblaze_0_axi_periph_M00_AXI_AWADDR),
        .M00_AXI_awready(microblaze_0_axi_periph_M00_AXI_AWREADY),
        .M00_AXI_awvalid(microblaze_0_axi_periph_M00_AXI_AWVALID),
        .M00_AXI_bready(microblaze_0_axi_periph_M00_AXI_BREADY),
        .M00_AXI_bresp(microblaze_0_axi_periph_M00_AXI_BRESP),
        .M00_AXI_bvalid(microblaze_0_axi_periph_M00_AXI_BVALID),
        .M00_AXI_rdata(microblaze_0_axi_periph_M00_AXI_RDATA),
        .M00_AXI_rready(microblaze_0_axi_periph_M00_AXI_RREADY),
        .M00_AXI_rresp(microblaze_0_axi_periph_M00_AXI_RRESP),
        .M00_AXI_rvalid(microblaze_0_axi_periph_M00_AXI_RVALID),
        .M00_AXI_wdata(microblaze_0_axi_periph_M00_AXI_WDATA),
        .M00_AXI_wready(microblaze_0_axi_periph_M00_AXI_WREADY),
        .M00_AXI_wstrb(microblaze_0_axi_periph_M00_AXI_WSTRB),
        .M00_AXI_wvalid(microblaze_0_axi_periph_M00_AXI_WVALID),
        .M01_ACLK(microblaze_0_Clk),
        .M01_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .M01_AXI_araddr(microblaze_0_axi_periph_M01_AXI_ARADDR),
        .M01_AXI_arready(microblaze_0_axi_periph_M01_AXI_ARREADY),
        .M01_AXI_arvalid(microblaze_0_axi_periph_M01_AXI_ARVALID),
        .M01_AXI_awaddr(microblaze_0_axi_periph_M01_AXI_AWADDR),
        .M01_AXI_awready(microblaze_0_axi_periph_M01_AXI_AWREADY),
        .M01_AXI_awvalid(microblaze_0_axi_periph_M01_AXI_AWVALID),
        .M01_AXI_bready(microblaze_0_axi_periph_M01_AXI_BREADY),
        .M01_AXI_bresp(microblaze_0_axi_periph_M01_AXI_BRESP),
        .M01_AXI_bvalid(microblaze_0_axi_periph_M01_AXI_BVALID),
        .M01_AXI_rdata(microblaze_0_axi_periph_M01_AXI_RDATA),
        .M01_AXI_rready(microblaze_0_axi_periph_M01_AXI_RREADY),
        .M01_AXI_rresp(microblaze_0_axi_periph_M01_AXI_RRESP),
        .M01_AXI_rvalid(microblaze_0_axi_periph_M01_AXI_RVALID),
        .M01_AXI_wdata(microblaze_0_axi_periph_M01_AXI_WDATA),
        .M01_AXI_wready(microblaze_0_axi_periph_M01_AXI_WREADY),
        .M01_AXI_wstrb(microblaze_0_axi_periph_M01_AXI_WSTRB),
        .M01_AXI_wvalid(microblaze_0_axi_periph_M01_AXI_WVALID),
        .M02_ACLK(microblaze_0_Clk),
        .M02_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .M02_AXI_araddr(microblaze_0_axi_periph_M02_AXI_ARADDR),
        .M02_AXI_arready(microblaze_0_axi_periph_M02_AXI_ARREADY),
        .M02_AXI_arvalid(microblaze_0_axi_periph_M02_AXI_ARVALID),
        .M02_AXI_awaddr(microblaze_0_axi_periph_M02_AXI_AWADDR),
        .M02_AXI_awready(microblaze_0_axi_periph_M02_AXI_AWREADY),
        .M02_AXI_awvalid(microblaze_0_axi_periph_M02_AXI_AWVALID),
        .M02_AXI_bready(microblaze_0_axi_periph_M02_AXI_BREADY),
        .M02_AXI_bresp(microblaze_0_axi_periph_M02_AXI_BRESP),
        .M02_AXI_bvalid(microblaze_0_axi_periph_M02_AXI_BVALID),
        .M02_AXI_rdata(microblaze_0_axi_periph_M02_AXI_RDATA),
        .M02_AXI_rready(microblaze_0_axi_periph_M02_AXI_RREADY),
        .M02_AXI_rresp(microblaze_0_axi_periph_M02_AXI_RRESP),
        .M02_AXI_rvalid(microblaze_0_axi_periph_M02_AXI_RVALID),
        .M02_AXI_wdata(microblaze_0_axi_periph_M02_AXI_WDATA),
        .M02_AXI_wready(microblaze_0_axi_periph_M02_AXI_WREADY),
        .M02_AXI_wstrb(microblaze_0_axi_periph_M02_AXI_WSTRB),
        .M02_AXI_wvalid(microblaze_0_axi_periph_M02_AXI_WVALID),
        .M03_ACLK(microblaze_0_Clk),
        .M03_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .M03_AXI_araddr(microblaze_0_axi_periph_M03_AXI_ARADDR),
        .M03_AXI_arready(microblaze_0_axi_periph_M03_AXI_ARREADY),
        .M03_AXI_arvalid(microblaze_0_axi_periph_M03_AXI_ARVALID),
        .M03_AXI_awaddr(microblaze_0_axi_periph_M03_AXI_AWADDR),
        .M03_AXI_awready(microblaze_0_axi_periph_M03_AXI_AWREADY),
        .M03_AXI_awvalid(microblaze_0_axi_periph_M03_AXI_AWVALID),
        .M03_AXI_bready(microblaze_0_axi_periph_M03_AXI_BREADY),
        .M03_AXI_bresp(microblaze_0_axi_periph_M03_AXI_BRESP),
        .M03_AXI_bvalid(microblaze_0_axi_periph_M03_AXI_BVALID),
        .M03_AXI_rdata(microblaze_0_axi_periph_M03_AXI_RDATA),
        .M03_AXI_rready(microblaze_0_axi_periph_M03_AXI_RREADY),
        .M03_AXI_rresp(microblaze_0_axi_periph_M03_AXI_RRESP),
        .M03_AXI_rvalid(microblaze_0_axi_periph_M03_AXI_RVALID),
        .M03_AXI_wdata(microblaze_0_axi_periph_M03_AXI_WDATA),
        .M03_AXI_wready(microblaze_0_axi_periph_M03_AXI_WREADY),
        .M03_AXI_wvalid(microblaze_0_axi_periph_M03_AXI_WVALID),
        .M04_ACLK(microblaze_0_Clk),
        .M04_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .M04_AXI_araddr(microblaze_0_axi_periph_M04_AXI_ARADDR),
        .M04_AXI_arready(microblaze_0_axi_periph_M04_AXI_ARREADY),
        .M04_AXI_arvalid(microblaze_0_axi_periph_M04_AXI_ARVALID),
        .M04_AXI_awaddr(microblaze_0_axi_periph_M04_AXI_AWADDR),
        .M04_AXI_awready(microblaze_0_axi_periph_M04_AXI_AWREADY),
        .M04_AXI_awvalid(microblaze_0_axi_periph_M04_AXI_AWVALID),
        .M04_AXI_bready(microblaze_0_axi_periph_M04_AXI_BREADY),
        .M04_AXI_bresp(microblaze_0_axi_periph_M04_AXI_BRESP),
        .M04_AXI_bvalid(microblaze_0_axi_periph_M04_AXI_BVALID),
        .M04_AXI_rdata(microblaze_0_axi_periph_M04_AXI_RDATA),
        .M04_AXI_rready(microblaze_0_axi_periph_M04_AXI_RREADY),
        .M04_AXI_rresp(microblaze_0_axi_periph_M04_AXI_RRESP),
        .M04_AXI_rvalid(microblaze_0_axi_periph_M04_AXI_RVALID),
        .M04_AXI_wdata(microblaze_0_axi_periph_M04_AXI_WDATA),
        .M04_AXI_wready(microblaze_0_axi_periph_M04_AXI_WREADY),
        .M04_AXI_wvalid(microblaze_0_axi_periph_M04_AXI_WVALID),
        .M05_ACLK(microblaze_0_Clk),
        .M05_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .M05_AXI_araddr(microblaze_0_axi_periph_M05_AXI_ARADDR),
        .M05_AXI_arready(microblaze_0_axi_periph_M05_AXI_ARREADY),
        .M05_AXI_arvalid(microblaze_0_axi_periph_M05_AXI_ARVALID),
        .M05_AXI_awaddr(microblaze_0_axi_periph_M05_AXI_AWADDR),
        .M05_AXI_awready(microblaze_0_axi_periph_M05_AXI_AWREADY),
        .M05_AXI_awvalid(microblaze_0_axi_periph_M05_AXI_AWVALID),
        .M05_AXI_bready(microblaze_0_axi_periph_M05_AXI_BREADY),
        .M05_AXI_bresp(microblaze_0_axi_periph_M05_AXI_BRESP),
        .M05_AXI_bvalid(microblaze_0_axi_periph_M05_AXI_BVALID),
        .M05_AXI_rdata(microblaze_0_axi_periph_M05_AXI_RDATA),
        .M05_AXI_rready(microblaze_0_axi_periph_M05_AXI_RREADY),
        .M05_AXI_rresp(microblaze_0_axi_periph_M05_AXI_RRESP),
        .M05_AXI_rvalid(microblaze_0_axi_periph_M05_AXI_RVALID),
        .M05_AXI_wdata(microblaze_0_axi_periph_M05_AXI_WDATA),
        .M05_AXI_wready(microblaze_0_axi_periph_M05_AXI_WREADY),
        .M05_AXI_wvalid(microblaze_0_axi_periph_M05_AXI_WVALID),
        .M06_ACLK(microblaze_0_Clk),
        .M06_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .M06_AXI_araddr(microblaze_0_axi_periph_M06_AXI_ARADDR),
        .M06_AXI_arready(microblaze_0_axi_periph_M06_AXI_ARREADY),
        .M06_AXI_arvalid(microblaze_0_axi_periph_M06_AXI_ARVALID),
        .M06_AXI_awaddr(microblaze_0_axi_periph_M06_AXI_AWADDR),
        .M06_AXI_awready(microblaze_0_axi_periph_M06_AXI_AWREADY),
        .M06_AXI_awvalid(microblaze_0_axi_periph_M06_AXI_AWVALID),
        .M06_AXI_bready(microblaze_0_axi_periph_M06_AXI_BREADY),
        .M06_AXI_bresp(microblaze_0_axi_periph_M06_AXI_BRESP),
        .M06_AXI_bvalid(microblaze_0_axi_periph_M06_AXI_BVALID),
        .M06_AXI_rdata(microblaze_0_axi_periph_M06_AXI_RDATA),
        .M06_AXI_rready(microblaze_0_axi_periph_M06_AXI_RREADY),
        .M06_AXI_rresp(microblaze_0_axi_periph_M06_AXI_RRESP),
        .M06_AXI_rvalid(microblaze_0_axi_periph_M06_AXI_RVALID),
        .M06_AXI_wdata(microblaze_0_axi_periph_M06_AXI_WDATA),
        .M06_AXI_wready(microblaze_0_axi_periph_M06_AXI_WREADY),
        .M06_AXI_wvalid(microblaze_0_axi_periph_M06_AXI_WVALID),
        .S00_ACLK(microblaze_0_Clk),
        .S00_ARESETN(rst_mig_7series_0_100M_peripheral_aresetn),
        .S00_AXI_araddr(microblaze_0_M_AXI_DP_ARADDR),
        .S00_AXI_arprot(microblaze_0_M_AXI_DP_ARPROT),
        .S00_AXI_arready(microblaze_0_M_AXI_DP_ARREADY),
        .S00_AXI_arvalid(microblaze_0_M_AXI_DP_ARVALID),
        .S00_AXI_awaddr(microblaze_0_M_AXI_DP_AWADDR),
        .S00_AXI_awprot(microblaze_0_M_AXI_DP_AWPROT),
        .S00_AXI_awready(microblaze_0_M_AXI_DP_AWREADY),
        .S00_AXI_awvalid(microblaze_0_M_AXI_DP_AWVALID),
        .S00_AXI_bready(microblaze_0_M_AXI_DP_BREADY),
        .S00_AXI_bresp(microblaze_0_M_AXI_DP_BRESP),
        .S00_AXI_bvalid(microblaze_0_M_AXI_DP_BVALID),
        .S00_AXI_rdata(microblaze_0_M_AXI_DP_RDATA),
        .S00_AXI_rready(microblaze_0_M_AXI_DP_RREADY),
        .S00_AXI_rresp(microblaze_0_M_AXI_DP_RRESP),
        .S00_AXI_rvalid(microblaze_0_M_AXI_DP_RVALID),
        .S00_AXI_wdata(microblaze_0_M_AXI_DP_WDATA),
        .S00_AXI_wready(microblaze_0_M_AXI_DP_WREADY),
        .S00_AXI_wstrb(microblaze_0_M_AXI_DP_WSTRB),
        .S00_AXI_wvalid(microblaze_0_M_AXI_DP_WVALID));
  microblaze_0_local_memory_imp_1K0VQXK microblaze_0_local_memory
       (.DLMB_abus(microblaze_0_dlmb_1_ABUS),
        .DLMB_addrstrobe(microblaze_0_dlmb_1_ADDRSTROBE),
        .DLMB_be(microblaze_0_dlmb_1_BE),
        .DLMB_ce(microblaze_0_dlmb_1_CE),
        .DLMB_readdbus(microblaze_0_dlmb_1_READDBUS),
        .DLMB_readstrobe(microblaze_0_dlmb_1_READSTROBE),
        .DLMB_ready(microblaze_0_dlmb_1_READY),
        .DLMB_ue(microblaze_0_dlmb_1_UE),
        .DLMB_wait(microblaze_0_dlmb_1_WAIT),
        .DLMB_writedbus(microblaze_0_dlmb_1_WRITEDBUS),
        .DLMB_writestrobe(microblaze_0_dlmb_1_WRITESTROBE),
        .ILMB_abus(microblaze_0_ilmb_1_ABUS),
        .ILMB_addrstrobe(microblaze_0_ilmb_1_ADDRSTROBE),
        .ILMB_ce(microblaze_0_ilmb_1_CE),
        .ILMB_readdbus(microblaze_0_ilmb_1_READDBUS),
        .ILMB_readstrobe(microblaze_0_ilmb_1_READSTROBE),
        .ILMB_ready(microblaze_0_ilmb_1_READY),
        .ILMB_ue(microblaze_0_ilmb_1_UE),
        .ILMB_wait(microblaze_0_ilmb_1_WAIT),
        .LMB_Clk(microblaze_0_Clk),
        .SYS_Rst(rst_mig_7series_0_100M_bus_struct_reset));
  design_1_mig_7series_0_0 mig_7series_0
       (.aresetn(rst_mig_7series_0_100M_peripheral_aresetn),
        .ddr3_addr(mig_7series_0_DDR3_ADDR),
        .ddr3_ba(mig_7series_0_DDR3_BA),
        .ddr3_cas_n(mig_7series_0_DDR3_CAS_N),
        .ddr3_ck_n(mig_7series_0_DDR3_CK_N),
        .ddr3_ck_p(mig_7series_0_DDR3_CK_P),
        .ddr3_cke(mig_7series_0_DDR3_CKE),
        .ddr3_cs_n(mig_7series_0_DDR3_CS_N),
        .ddr3_dm(mig_7series_0_DDR3_DM),
        .ddr3_dq(ddr3_sdram_dq[63:0]),
        .ddr3_dqs_n(ddr3_sdram_dqs_n[7:0]),
        .ddr3_dqs_p(ddr3_sdram_dqs_p[7:0]),
        .ddr3_odt(mig_7series_0_DDR3_ODT),
        .ddr3_ras_n(mig_7series_0_DDR3_RAS_N),
        .ddr3_reset_n(mig_7series_0_DDR3_RESET_N),
        .ddr3_we_n(mig_7series_0_DDR3_WE_N),
        .mmcm_locked(mig_7series_0_mmcm_locked),
        .s_axi_araddr(axi_mem_intercon_M00_AXI_ARADDR),
        .s_axi_arburst(axi_mem_intercon_M00_AXI_ARBURST),
        .s_axi_arcache(axi_mem_intercon_M00_AXI_ARCACHE),
        .s_axi_arid(axi_mem_intercon_M00_AXI_ARID),
        .s_axi_arlen(axi_mem_intercon_M00_AXI_ARLEN),
        .s_axi_arlock(axi_mem_intercon_M00_AXI_ARLOCK),
        .s_axi_arprot(axi_mem_intercon_M00_AXI_ARPROT),
        .s_axi_arqos(axi_mem_intercon_M00_AXI_ARQOS),
        .s_axi_arready(axi_mem_intercon_M00_AXI_ARREADY),
        .s_axi_arsize(axi_mem_intercon_M00_AXI_ARSIZE),
        .s_axi_arvalid(axi_mem_intercon_M00_AXI_ARVALID),
        .s_axi_awaddr(axi_mem_intercon_M00_AXI_AWADDR),
        .s_axi_awburst(axi_mem_intercon_M00_AXI_AWBURST),
        .s_axi_awcache(axi_mem_intercon_M00_AXI_AWCACHE),
        .s_axi_awid(axi_mem_intercon_M00_AXI_AWID),
        .s_axi_awlen(axi_mem_intercon_M00_AXI_AWLEN),
        .s_axi_awlock(axi_mem_intercon_M00_AXI_AWLOCK),
        .s_axi_awprot(axi_mem_intercon_M00_AXI_AWPROT),
        .s_axi_awqos(axi_mem_intercon_M00_AXI_AWQOS),
        .s_axi_awready(axi_mem_intercon_M00_AXI_AWREADY),
        .s_axi_awsize(axi_mem_intercon_M00_AXI_AWSIZE),
        .s_axi_awvalid(axi_mem_intercon_M00_AXI_AWVALID),
        .s_axi_bid(axi_mem_intercon_M00_AXI_BID),
        .s_axi_bready(axi_mem_intercon_M00_AXI_BREADY),
        .s_axi_bresp(axi_mem_intercon_M00_AXI_BRESP),
        .s_axi_bvalid(axi_mem_intercon_M00_AXI_BVALID),
        .s_axi_rdata(axi_mem_intercon_M00_AXI_RDATA),
        .s_axi_rid(axi_mem_intercon_M00_AXI_RID),
        .s_axi_rlast(axi_mem_intercon_M00_AXI_RLAST),
        .s_axi_rready(axi_mem_intercon_M00_AXI_RREADY),
        .s_axi_rresp(axi_mem_intercon_M00_AXI_RRESP),
        .s_axi_rvalid(axi_mem_intercon_M00_AXI_RVALID),
        .s_axi_wdata(axi_mem_intercon_M00_AXI_WDATA),
        .s_axi_wlast(axi_mem_intercon_M00_AXI_WLAST),
        .s_axi_wready(axi_mem_intercon_M00_AXI_WREADY),
        .s_axi_wstrb(axi_mem_intercon_M00_AXI_WSTRB),
        .s_axi_wvalid(axi_mem_intercon_M00_AXI_WVALID),
        .sys_clk_n(sys_diff_clock_1_CLK_N),
        .sys_clk_p(sys_diff_clock_1_CLK_P),
        .sys_rst(reset_1),
        .ui_clk(microblaze_0_Clk),
        .ui_clk_sync_rst(mig_7series_0_ui_clk_sync_rst));
  design_1_pcc_stream_0_0 pcc_stream_0
       (.DMA_NumberOfCorrelation_TDATA(axis_interconnect_0_M00_AXIS_TDATA),
        .DMA_NumberOfCorrelation_TDEST(axis_interconnect_0_M00_AXIS_TDEST),
        .DMA_NumberOfCorrelation_TID(axis_interconnect_0_M00_AXIS_TID),
        .DMA_NumberOfCorrelation_TKEEP(axis_interconnect_0_M00_AXIS_TKEEP),
        .DMA_NumberOfCorrelation_TLAST(axis_interconnect_0_M00_AXIS_TLAST),
        .DMA_NumberOfCorrelation_TREADY(axis_interconnect_0_M00_AXIS_TREADY),
        .DMA_NumberOfCorrelation_TSTRB(axis_interconnect_0_M00_AXIS_TSTRB),
        .DMA_NumberOfCorrelation_TUSER(axis_interconnect_0_M00_AXIS_TUSER),
        .DMA_NumberOfCorrelation_TVALID(axis_interconnect_0_M00_AXIS_TVALID),
        .DMA_dep1_TDATA(axis_interconnect_0_M01_AXIS_TDATA),
        .DMA_dep1_TDEST(axis_interconnect_0_M01_AXIS_TDEST),
        .DMA_dep1_TID(axis_interconnect_0_M01_AXIS_TID),
        .DMA_dep1_TKEEP(axis_interconnect_0_M01_AXIS_TKEEP),
        .DMA_dep1_TLAST(axis_interconnect_0_M01_AXIS_TLAST),
        .DMA_dep1_TREADY(axis_interconnect_0_M01_AXIS_TREADY),
        .DMA_dep1_TSTRB(axis_interconnect_0_M01_AXIS_TSTRB),
        .DMA_dep1_TUSER(axis_interconnect_0_M01_AXIS_TUSER),
        .DMA_dep1_TVALID(axis_interconnect_0_M01_AXIS_TVALID),
        .DMA_dep2_TDATA(axis_interconnect_0_M02_AXIS_TDATA),
        .DMA_dep2_TDEST(axis_interconnect_0_M02_AXIS_TDEST),
        .DMA_dep2_TID(axis_interconnect_0_M02_AXIS_TID),
        .DMA_dep2_TKEEP(axis_interconnect_0_M02_AXIS_TKEEP),
        .DMA_dep2_TLAST(axis_interconnect_0_M02_AXIS_TLAST),
        .DMA_dep2_TREADY(axis_interconnect_0_M02_AXIS_TREADY),
        .DMA_dep2_TSTRB(axis_interconnect_0_M02_AXIS_TSTRB),
        .DMA_dep2_TUSER(axis_interconnect_0_M02_AXIS_TUSER),
        .DMA_dep2_TVALID(axis_interconnect_0_M02_AXIS_TVALID),
        .DMA_pcc_TDATA(pcc_stream_0_DMA_pcc_TDATA),
        .DMA_pcc_TDEST(pcc_stream_0_DMA_pcc_TDEST),
        .DMA_pcc_TID(pcc_stream_0_DMA_pcc_TID),
        .DMA_pcc_TKEEP(pcc_stream_0_DMA_pcc_TKEEP),
        .DMA_pcc_TLAST(pcc_stream_0_DMA_pcc_TLAST),
        .DMA_pcc_TREADY(pcc_stream_0_DMA_pcc_TREADY),
        .DMA_pcc_TSTRB(pcc_stream_0_DMA_pcc_TSTRB),
        .DMA_pcc_TUSER(pcc_stream_0_DMA_pcc_TUSER),
        .DMA_pcc_TVALID(pcc_stream_0_DMA_pcc_TVALID),
        .ap_clk(microblaze_0_Clk),
        .ap_rst_n(rst_mig_7series_0_100M_peripheral_aresetn));
  design_1_pcc_stream_0_1 pcc_stream_1
       (.DMA_NumberOfCorrelation_TDATA(axis_interconnect_1_M00_AXIS_TDATA),
        .DMA_NumberOfCorrelation_TDEST(axis_interconnect_1_M00_AXIS_TDEST),
        .DMA_NumberOfCorrelation_TID(axis_interconnect_1_M00_AXIS_TID),
        .DMA_NumberOfCorrelation_TKEEP(axis_interconnect_1_M00_AXIS_TKEEP),
        .DMA_NumberOfCorrelation_TLAST(axis_interconnect_1_M00_AXIS_TLAST),
        .DMA_NumberOfCorrelation_TREADY(axis_interconnect_1_M00_AXIS_TREADY),
        .DMA_NumberOfCorrelation_TSTRB(axis_interconnect_1_M00_AXIS_TSTRB),
        .DMA_NumberOfCorrelation_TUSER(axis_interconnect_1_M00_AXIS_TUSER),
        .DMA_NumberOfCorrelation_TVALID(axis_interconnect_1_M00_AXIS_TVALID),
        .DMA_dep1_TDATA(axis_interconnect_1_M01_AXIS_TDATA),
        .DMA_dep1_TDEST(axis_interconnect_1_M01_AXIS_TDEST),
        .DMA_dep1_TID(axis_interconnect_1_M01_AXIS_TID),
        .DMA_dep1_TKEEP(axis_interconnect_1_M01_AXIS_TKEEP),
        .DMA_dep1_TLAST(axis_interconnect_1_M01_AXIS_TLAST),
        .DMA_dep1_TREADY(axis_interconnect_1_M01_AXIS_TREADY),
        .DMA_dep1_TSTRB(axis_interconnect_1_M01_AXIS_TSTRB),
        .DMA_dep1_TUSER(axis_interconnect_1_M01_AXIS_TUSER),
        .DMA_dep1_TVALID(axis_interconnect_1_M01_AXIS_TVALID),
        .DMA_dep2_TDATA(axis_interconnect_1_M02_AXIS_TDATA),
        .DMA_dep2_TDEST(axis_interconnect_1_M02_AXIS_TDEST),
        .DMA_dep2_TID(axis_interconnect_1_M02_AXIS_TID),
        .DMA_dep2_TKEEP(axis_interconnect_1_M02_AXIS_TKEEP),
        .DMA_dep2_TLAST(axis_interconnect_1_M02_AXIS_TLAST),
        .DMA_dep2_TREADY(axis_interconnect_1_M02_AXIS_TREADY),
        .DMA_dep2_TSTRB(axis_interconnect_1_M02_AXIS_TSTRB),
        .DMA_dep2_TUSER(axis_interconnect_1_M02_AXIS_TUSER),
        .DMA_dep2_TVALID(axis_interconnect_1_M02_AXIS_TVALID),
        .DMA_pcc_TDATA(pcc_stream_1_DMA_pcc_TDATA),
        .DMA_pcc_TDEST(pcc_stream_1_DMA_pcc_TDEST),
        .DMA_pcc_TID(pcc_stream_1_DMA_pcc_TID),
        .DMA_pcc_TKEEP(pcc_stream_1_DMA_pcc_TKEEP),
        .DMA_pcc_TLAST(pcc_stream_1_DMA_pcc_TLAST),
        .DMA_pcc_TREADY(pcc_stream_1_DMA_pcc_TREADY),
        .DMA_pcc_TSTRB(pcc_stream_1_DMA_pcc_TSTRB),
        .DMA_pcc_TUSER(pcc_stream_1_DMA_pcc_TUSER),
        .DMA_pcc_TVALID(pcc_stream_1_DMA_pcc_TVALID),
        .ap_clk(microblaze_0_Clk),
        .ap_rst_n(rst_mig_7series_0_100M_peripheral_aresetn));
  design_1_pcc_stream_0_2 pcc_stream_2
       (.DMA_NumberOfCorrelation_TDATA(axis_interconnect_2_M00_AXIS_TDATA),
        .DMA_NumberOfCorrelation_TDEST(axis_interconnect_2_M00_AXIS_TDEST),
        .DMA_NumberOfCorrelation_TID(axis_interconnect_2_M00_AXIS_TID),
        .DMA_NumberOfCorrelation_TKEEP(axis_interconnect_2_M00_AXIS_TKEEP),
        .DMA_NumberOfCorrelation_TLAST(axis_interconnect_2_M00_AXIS_TLAST),
        .DMA_NumberOfCorrelation_TREADY(axis_interconnect_2_M00_AXIS_TREADY),
        .DMA_NumberOfCorrelation_TSTRB(axis_interconnect_2_M00_AXIS_TSTRB),
        .DMA_NumberOfCorrelation_TUSER(axis_interconnect_2_M00_AXIS_TUSER),
        .DMA_NumberOfCorrelation_TVALID(axis_interconnect_2_M00_AXIS_TVALID),
        .DMA_dep1_TDATA(axis_interconnect_2_M01_AXIS_TDATA),
        .DMA_dep1_TDEST(axis_interconnect_2_M01_AXIS_TDEST),
        .DMA_dep1_TID(axis_interconnect_2_M01_AXIS_TID),
        .DMA_dep1_TKEEP(axis_interconnect_2_M01_AXIS_TKEEP),
        .DMA_dep1_TLAST(axis_interconnect_2_M01_AXIS_TLAST),
        .DMA_dep1_TREADY(axis_interconnect_2_M01_AXIS_TREADY),
        .DMA_dep1_TSTRB(axis_interconnect_2_M01_AXIS_TSTRB),
        .DMA_dep1_TUSER(axis_interconnect_2_M01_AXIS_TUSER),
        .DMA_dep1_TVALID(axis_interconnect_2_M01_AXIS_TVALID),
        .DMA_dep2_TDATA(axis_interconnect_2_M02_AXIS_TDATA),
        .DMA_dep2_TDEST(axis_interconnect_2_M02_AXIS_TDEST),
        .DMA_dep2_TID(axis_interconnect_2_M02_AXIS_TID),
        .DMA_dep2_TKEEP(axis_interconnect_2_M02_AXIS_TKEEP),
        .DMA_dep2_TLAST(axis_interconnect_2_M02_AXIS_TLAST),
        .DMA_dep2_TREADY(axis_interconnect_2_M02_AXIS_TREADY),
        .DMA_dep2_TSTRB(axis_interconnect_2_M02_AXIS_TSTRB),
        .DMA_dep2_TUSER(axis_interconnect_2_M02_AXIS_TUSER),
        .DMA_dep2_TVALID(axis_interconnect_2_M02_AXIS_TVALID),
        .DMA_pcc_TDATA(pcc_stream_2_DMA_pcc_TDATA),
        .DMA_pcc_TDEST(pcc_stream_2_DMA_pcc_TDEST),
        .DMA_pcc_TID(pcc_stream_2_DMA_pcc_TID),
        .DMA_pcc_TKEEP(pcc_stream_2_DMA_pcc_TKEEP),
        .DMA_pcc_TLAST(pcc_stream_2_DMA_pcc_TLAST),
        .DMA_pcc_TREADY(pcc_stream_2_DMA_pcc_TREADY),
        .DMA_pcc_TSTRB(pcc_stream_2_DMA_pcc_TSTRB),
        .DMA_pcc_TUSER(pcc_stream_2_DMA_pcc_TUSER),
        .DMA_pcc_TVALID(pcc_stream_2_DMA_pcc_TVALID),
        .ap_clk(microblaze_0_Clk),
        .ap_rst_n(rst_mig_7series_0_100M_peripheral_aresetn));
  design_1_pcc_stream_1_0 pcc_stream_3
       (.DMA_NumberOfCorrelation_TDATA(axis_interconnect_3_M00_AXIS_TDATA),
        .DMA_NumberOfCorrelation_TDEST(axis_interconnect_3_M00_AXIS_TDEST),
        .DMA_NumberOfCorrelation_TID(axis_interconnect_3_M00_AXIS_TID),
        .DMA_NumberOfCorrelation_TKEEP(axis_interconnect_3_M00_AXIS_TKEEP),
        .DMA_NumberOfCorrelation_TLAST(axis_interconnect_3_M00_AXIS_TLAST),
        .DMA_NumberOfCorrelation_TREADY(axis_interconnect_3_M00_AXIS_TREADY),
        .DMA_NumberOfCorrelation_TSTRB(axis_interconnect_3_M00_AXIS_TSTRB),
        .DMA_NumberOfCorrelation_TUSER(axis_interconnect_3_M00_AXIS_TUSER),
        .DMA_NumberOfCorrelation_TVALID(axis_interconnect_3_M00_AXIS_TVALID),
        .DMA_dep1_TDATA(axis_interconnect_3_M01_AXIS_TDATA),
        .DMA_dep1_TDEST(axis_interconnect_3_M01_AXIS_TDEST),
        .DMA_dep1_TID(axis_interconnect_3_M01_AXIS_TID),
        .DMA_dep1_TKEEP(axis_interconnect_3_M01_AXIS_TKEEP),
        .DMA_dep1_TLAST(axis_interconnect_3_M01_AXIS_TLAST),
        .DMA_dep1_TREADY(axis_interconnect_3_M01_AXIS_TREADY),
        .DMA_dep1_TSTRB(axis_interconnect_3_M01_AXIS_TSTRB),
        .DMA_dep1_TUSER(axis_interconnect_3_M01_AXIS_TUSER),
        .DMA_dep1_TVALID(axis_interconnect_3_M01_AXIS_TVALID),
        .DMA_dep2_TDATA(axis_interconnect_3_M02_AXIS_TDATA),
        .DMA_dep2_TDEST(axis_interconnect_3_M02_AXIS_TDEST),
        .DMA_dep2_TID(axis_interconnect_3_M02_AXIS_TID),
        .DMA_dep2_TKEEP(axis_interconnect_3_M02_AXIS_TKEEP),
        .DMA_dep2_TLAST(axis_interconnect_3_M02_AXIS_TLAST),
        .DMA_dep2_TREADY(axis_interconnect_3_M02_AXIS_TREADY),
        .DMA_dep2_TSTRB(axis_interconnect_3_M02_AXIS_TSTRB),
        .DMA_dep2_TUSER(axis_interconnect_3_M02_AXIS_TUSER),
        .DMA_dep2_TVALID(axis_interconnect_3_M02_AXIS_TVALID),
        .DMA_pcc_TDATA(pcc_stream_3_DMA_pcc_TDATA),
        .DMA_pcc_TDEST(pcc_stream_3_DMA_pcc_TDEST),
        .DMA_pcc_TID(pcc_stream_3_DMA_pcc_TID),
        .DMA_pcc_TKEEP(pcc_stream_3_DMA_pcc_TKEEP),
        .DMA_pcc_TLAST(pcc_stream_3_DMA_pcc_TLAST),
        .DMA_pcc_TREADY(pcc_stream_3_DMA_pcc_TREADY),
        .DMA_pcc_TSTRB(pcc_stream_3_DMA_pcc_TSTRB),
        .DMA_pcc_TUSER(pcc_stream_3_DMA_pcc_TUSER),
        .DMA_pcc_TVALID(pcc_stream_3_DMA_pcc_TVALID),
        .ap_clk(microblaze_0_Clk),
        .ap_rst_n(rst_mig_7series_0_100M_peripheral_aresetn));
  design_1_rst_mig_7series_0_100M_0 rst_mig_7series_0_100M
       (.aux_reset_in(1'b1),
        .bus_struct_reset(rst_mig_7series_0_100M_bus_struct_reset),
        .dcm_locked(mig_7series_0_mmcm_locked),
        .ext_reset_in(mig_7series_0_ui_clk_sync_rst),
        .interconnect_aresetn(rst_mig_7series_0_100M_interconnect_aresetn),
        .mb_debug_sys_rst(mdm_1_debug_sys_rst),
        .mb_reset(rst_mig_7series_0_100M_mb_reset),
        .peripheral_aresetn(rst_mig_7series_0_100M_peripheral_aresetn),
        .slowest_sync_clk(microblaze_0_Clk));
  design_1_xlconcat_0_0 xlconcat_0
       (.In0(axi_dma_0_mm2s_introut),
        .In1(axi_dma_0_s2mm_introut),
        .In2(axi_dma_1_mm2s_introut),
        .In3(axi_dma_1_s2mm_introut),
        .In4(axi_dma_2_mm2s_introut),
        .In5(axi_dma_2_s2mm_introut),
        .In6(axi_dma_3_mm2s_introut),
        .In7(axi_dma_3_s2mm_introut),
        .dout(xlconcat_0_dout));
endmodule

module design_1_axi_mem_intercon_0
   (ACLK,
    ARESETN,
    M00_ACLK,
    M00_ARESETN,
    M00_AXI_araddr,
    M00_AXI_arburst,
    M00_AXI_arcache,
    M00_AXI_arid,
    M00_AXI_arlen,
    M00_AXI_arlock,
    M00_AXI_arprot,
    M00_AXI_arqos,
    M00_AXI_arready,
    M00_AXI_arsize,
    M00_AXI_arvalid,
    M00_AXI_awaddr,
    M00_AXI_awburst,
    M00_AXI_awcache,
    M00_AXI_awid,
    M00_AXI_awlen,
    M00_AXI_awlock,
    M00_AXI_awprot,
    M00_AXI_awqos,
    M00_AXI_awready,
    M00_AXI_awsize,
    M00_AXI_awvalid,
    M00_AXI_bid,
    M00_AXI_bready,
    M00_AXI_bresp,
    M00_AXI_bvalid,
    M00_AXI_rdata,
    M00_AXI_rid,
    M00_AXI_rlast,
    M00_AXI_rready,
    M00_AXI_rresp,
    M00_AXI_rvalid,
    M00_AXI_wdata,
    M00_AXI_wlast,
    M00_AXI_wready,
    M00_AXI_wstrb,
    M00_AXI_wvalid,
    S00_ACLK,
    S00_ARESETN,
    S00_AXI_araddr,
    S00_AXI_arburst,
    S00_AXI_arcache,
    S00_AXI_arlen,
    S00_AXI_arlock,
    S00_AXI_arprot,
    S00_AXI_arqos,
    S00_AXI_arready,
    S00_AXI_arsize,
    S00_AXI_arvalid,
    S00_AXI_awaddr,
    S00_AXI_awburst,
    S00_AXI_awcache,
    S00_AXI_awlen,
    S00_AXI_awlock,
    S00_AXI_awprot,
    S00_AXI_awqos,
    S00_AXI_awready,
    S00_AXI_awsize,
    S00_AXI_awvalid,
    S00_AXI_bready,
    S00_AXI_bresp,
    S00_AXI_bvalid,
    S00_AXI_rdata,
    S00_AXI_rlast,
    S00_AXI_rready,
    S00_AXI_rresp,
    S00_AXI_rvalid,
    S00_AXI_wdata,
    S00_AXI_wlast,
    S00_AXI_wready,
    S00_AXI_wstrb,
    S00_AXI_wvalid,
    S01_ACLK,
    S01_ARESETN,
    S01_AXI_araddr,
    S01_AXI_arburst,
    S01_AXI_arcache,
    S01_AXI_arlen,
    S01_AXI_arlock,
    S01_AXI_arprot,
    S01_AXI_arqos,
    S01_AXI_arready,
    S01_AXI_arsize,
    S01_AXI_arvalid,
    S01_AXI_rdata,
    S01_AXI_rlast,
    S01_AXI_rready,
    S01_AXI_rresp,
    S01_AXI_rvalid,
    S02_ACLK,
    S02_ARESETN,
    S02_AXI_araddr,
    S02_AXI_arburst,
    S02_AXI_arcache,
    S02_AXI_arlen,
    S02_AXI_arprot,
    S02_AXI_arready,
    S02_AXI_arsize,
    S02_AXI_arvalid,
    S02_AXI_awaddr,
    S02_AXI_awburst,
    S02_AXI_awcache,
    S02_AXI_awlen,
    S02_AXI_awprot,
    S02_AXI_awready,
    S02_AXI_awsize,
    S02_AXI_awvalid,
    S02_AXI_bready,
    S02_AXI_bresp,
    S02_AXI_bvalid,
    S02_AXI_rdata,
    S02_AXI_rlast,
    S02_AXI_rready,
    S02_AXI_rresp,
    S02_AXI_rvalid,
    S02_AXI_wdata,
    S02_AXI_wlast,
    S02_AXI_wready,
    S02_AXI_wstrb,
    S02_AXI_wvalid,
    S03_ACLK,
    S03_ARESETN,
    S03_AXI_araddr,
    S03_AXI_arburst,
    S03_AXI_arcache,
    S03_AXI_arlen,
    S03_AXI_arprot,
    S03_AXI_arready,
    S03_AXI_arsize,
    S03_AXI_arvalid,
    S03_AXI_rdata,
    S03_AXI_rlast,
    S03_AXI_rready,
    S03_AXI_rresp,
    S03_AXI_rvalid,
    S04_ACLK,
    S04_ARESETN,
    S04_AXI_awaddr,
    S04_AXI_awburst,
    S04_AXI_awcache,
    S04_AXI_awlen,
    S04_AXI_awprot,
    S04_AXI_awready,
    S04_AXI_awsize,
    S04_AXI_awvalid,
    S04_AXI_bready,
    S04_AXI_bresp,
    S04_AXI_bvalid,
    S04_AXI_wdata,
    S04_AXI_wlast,
    S04_AXI_wready,
    S04_AXI_wstrb,
    S04_AXI_wvalid,
    S05_ACLK,
    S05_ARESETN,
    S05_AXI_araddr,
    S05_AXI_arburst,
    S05_AXI_arcache,
    S05_AXI_arlen,
    S05_AXI_arprot,
    S05_AXI_arready,
    S05_AXI_arsize,
    S05_AXI_arvalid,
    S05_AXI_awaddr,
    S05_AXI_awburst,
    S05_AXI_awcache,
    S05_AXI_awlen,
    S05_AXI_awprot,
    S05_AXI_awready,
    S05_AXI_awsize,
    S05_AXI_awvalid,
    S05_AXI_bready,
    S05_AXI_bresp,
    S05_AXI_bvalid,
    S05_AXI_rdata,
    S05_AXI_rlast,
    S05_AXI_rready,
    S05_AXI_rresp,
    S05_AXI_rvalid,
    S05_AXI_wdata,
    S05_AXI_wlast,
    S05_AXI_wready,
    S05_AXI_wstrb,
    S05_AXI_wvalid,
    S06_ACLK,
    S06_ARESETN,
    S06_AXI_araddr,
    S06_AXI_arburst,
    S06_AXI_arcache,
    S06_AXI_arlen,
    S06_AXI_arprot,
    S06_AXI_arready,
    S06_AXI_arsize,
    S06_AXI_arvalid,
    S06_AXI_rdata,
    S06_AXI_rlast,
    S06_AXI_rready,
    S06_AXI_rresp,
    S06_AXI_rvalid,
    S07_ACLK,
    S07_ARESETN,
    S07_AXI_awaddr,
    S07_AXI_awburst,
    S07_AXI_awcache,
    S07_AXI_awlen,
    S07_AXI_awprot,
    S07_AXI_awready,
    S07_AXI_awsize,
    S07_AXI_awvalid,
    S07_AXI_bready,
    S07_AXI_bresp,
    S07_AXI_bvalid,
    S07_AXI_wdata,
    S07_AXI_wlast,
    S07_AXI_wready,
    S07_AXI_wstrb,
    S07_AXI_wvalid,
    S08_ACLK,
    S08_ARESETN,
    S08_AXI_araddr,
    S08_AXI_arburst,
    S08_AXI_arcache,
    S08_AXI_arlen,
    S08_AXI_arprot,
    S08_AXI_arready,
    S08_AXI_arsize,
    S08_AXI_arvalid,
    S08_AXI_awaddr,
    S08_AXI_awburst,
    S08_AXI_awcache,
    S08_AXI_awlen,
    S08_AXI_awprot,
    S08_AXI_awready,
    S08_AXI_awsize,
    S08_AXI_awvalid,
    S08_AXI_bready,
    S08_AXI_bresp,
    S08_AXI_bvalid,
    S08_AXI_rdata,
    S08_AXI_rlast,
    S08_AXI_rready,
    S08_AXI_rresp,
    S08_AXI_rvalid,
    S08_AXI_wdata,
    S08_AXI_wlast,
    S08_AXI_wready,
    S08_AXI_wstrb,
    S08_AXI_wvalid,
    S09_ACLK,
    S09_ARESETN,
    S09_AXI_araddr,
    S09_AXI_arburst,
    S09_AXI_arcache,
    S09_AXI_arlen,
    S09_AXI_arprot,
    S09_AXI_arready,
    S09_AXI_arsize,
    S09_AXI_arvalid,
    S09_AXI_rdata,
    S09_AXI_rlast,
    S09_AXI_rready,
    S09_AXI_rresp,
    S09_AXI_rvalid,
    S10_ACLK,
    S10_ARESETN,
    S10_AXI_awaddr,
    S10_AXI_awburst,
    S10_AXI_awcache,
    S10_AXI_awlen,
    S10_AXI_awprot,
    S10_AXI_awready,
    S10_AXI_awsize,
    S10_AXI_awvalid,
    S10_AXI_bready,
    S10_AXI_bresp,
    S10_AXI_bvalid,
    S10_AXI_wdata,
    S10_AXI_wlast,
    S10_AXI_wready,
    S10_AXI_wstrb,
    S10_AXI_wvalid,
    S11_ACLK,
    S11_ARESETN,
    S11_AXI_araddr,
    S11_AXI_arburst,
    S11_AXI_arcache,
    S11_AXI_arlen,
    S11_AXI_arprot,
    S11_AXI_arready,
    S11_AXI_arsize,
    S11_AXI_arvalid,
    S11_AXI_awaddr,
    S11_AXI_awburst,
    S11_AXI_awcache,
    S11_AXI_awlen,
    S11_AXI_awprot,
    S11_AXI_awready,
    S11_AXI_awsize,
    S11_AXI_awvalid,
    S11_AXI_bready,
    S11_AXI_bresp,
    S11_AXI_bvalid,
    S11_AXI_rdata,
    S11_AXI_rlast,
    S11_AXI_rready,
    S11_AXI_rresp,
    S11_AXI_rvalid,
    S11_AXI_wdata,
    S11_AXI_wlast,
    S11_AXI_wready,
    S11_AXI_wstrb,
    S11_AXI_wvalid,
    S12_ACLK,
    S12_ARESETN,
    S12_AXI_araddr,
    S12_AXI_arburst,
    S12_AXI_arcache,
    S12_AXI_arlen,
    S12_AXI_arprot,
    S12_AXI_arready,
    S12_AXI_arsize,
    S12_AXI_arvalid,
    S12_AXI_rdata,
    S12_AXI_rlast,
    S12_AXI_rready,
    S12_AXI_rresp,
    S12_AXI_rvalid,
    S13_ACLK,
    S13_ARESETN,
    S13_AXI_awaddr,
    S13_AXI_awburst,
    S13_AXI_awcache,
    S13_AXI_awlen,
    S13_AXI_awprot,
    S13_AXI_awready,
    S13_AXI_awsize,
    S13_AXI_awvalid,
    S13_AXI_bready,
    S13_AXI_bresp,
    S13_AXI_bvalid,
    S13_AXI_wdata,
    S13_AXI_wlast,
    S13_AXI_wready,
    S13_AXI_wstrb,
    S13_AXI_wvalid);
  input ACLK;
  input [0:0]ARESETN;
  input M00_ACLK;
  input [0:0]M00_ARESETN;
  output [31:0]M00_AXI_araddr;
  output [1:0]M00_AXI_arburst;
  output [3:0]M00_AXI_arcache;
  output [3:0]M00_AXI_arid;
  output [7:0]M00_AXI_arlen;
  output [0:0]M00_AXI_arlock;
  output [2:0]M00_AXI_arprot;
  output [3:0]M00_AXI_arqos;
  input [0:0]M00_AXI_arready;
  output [2:0]M00_AXI_arsize;
  output [0:0]M00_AXI_arvalid;
  output [31:0]M00_AXI_awaddr;
  output [1:0]M00_AXI_awburst;
  output [3:0]M00_AXI_awcache;
  output [3:0]M00_AXI_awid;
  output [7:0]M00_AXI_awlen;
  output [0:0]M00_AXI_awlock;
  output [2:0]M00_AXI_awprot;
  output [3:0]M00_AXI_awqos;
  input [0:0]M00_AXI_awready;
  output [2:0]M00_AXI_awsize;
  output [0:0]M00_AXI_awvalid;
  input [3:0]M00_AXI_bid;
  output [0:0]M00_AXI_bready;
  input [1:0]M00_AXI_bresp;
  input [0:0]M00_AXI_bvalid;
  input [511:0]M00_AXI_rdata;
  input [3:0]M00_AXI_rid;
  input [0:0]M00_AXI_rlast;
  output [0:0]M00_AXI_rready;
  input [1:0]M00_AXI_rresp;
  input [0:0]M00_AXI_rvalid;
  output [511:0]M00_AXI_wdata;
  output [0:0]M00_AXI_wlast;
  input [0:0]M00_AXI_wready;
  output [63:0]M00_AXI_wstrb;
  output [0:0]M00_AXI_wvalid;
  input S00_ACLK;
  input [0:0]S00_ARESETN;
  input [31:0]S00_AXI_araddr;
  input [1:0]S00_AXI_arburst;
  input [3:0]S00_AXI_arcache;
  input [7:0]S00_AXI_arlen;
  input [0:0]S00_AXI_arlock;
  input [2:0]S00_AXI_arprot;
  input [3:0]S00_AXI_arqos;
  output S00_AXI_arready;
  input [2:0]S00_AXI_arsize;
  input S00_AXI_arvalid;
  input [31:0]S00_AXI_awaddr;
  input [1:0]S00_AXI_awburst;
  input [3:0]S00_AXI_awcache;
  input [7:0]S00_AXI_awlen;
  input [0:0]S00_AXI_awlock;
  input [2:0]S00_AXI_awprot;
  input [3:0]S00_AXI_awqos;
  output S00_AXI_awready;
  input [2:0]S00_AXI_awsize;
  input S00_AXI_awvalid;
  input S00_AXI_bready;
  output [1:0]S00_AXI_bresp;
  output S00_AXI_bvalid;
  output [31:0]S00_AXI_rdata;
  output S00_AXI_rlast;
  input S00_AXI_rready;
  output [1:0]S00_AXI_rresp;
  output S00_AXI_rvalid;
  input [31:0]S00_AXI_wdata;
  input S00_AXI_wlast;
  output S00_AXI_wready;
  input [3:0]S00_AXI_wstrb;
  input S00_AXI_wvalid;
  input S01_ACLK;
  input [0:0]S01_ARESETN;
  input [31:0]S01_AXI_araddr;
  input [1:0]S01_AXI_arburst;
  input [3:0]S01_AXI_arcache;
  input [7:0]S01_AXI_arlen;
  input [0:0]S01_AXI_arlock;
  input [2:0]S01_AXI_arprot;
  input [3:0]S01_AXI_arqos;
  output S01_AXI_arready;
  input [2:0]S01_AXI_arsize;
  input S01_AXI_arvalid;
  output [31:0]S01_AXI_rdata;
  output S01_AXI_rlast;
  input S01_AXI_rready;
  output [1:0]S01_AXI_rresp;
  output S01_AXI_rvalid;
  input S02_ACLK;
  input [0:0]S02_ARESETN;
  input [31:0]S02_AXI_araddr;
  input [1:0]S02_AXI_arburst;
  input [3:0]S02_AXI_arcache;
  input [7:0]S02_AXI_arlen;
  input [2:0]S02_AXI_arprot;
  output S02_AXI_arready;
  input [2:0]S02_AXI_arsize;
  input S02_AXI_arvalid;
  input [31:0]S02_AXI_awaddr;
  input [1:0]S02_AXI_awburst;
  input [3:0]S02_AXI_awcache;
  input [7:0]S02_AXI_awlen;
  input [2:0]S02_AXI_awprot;
  output S02_AXI_awready;
  input [2:0]S02_AXI_awsize;
  input S02_AXI_awvalid;
  input S02_AXI_bready;
  output [1:0]S02_AXI_bresp;
  output S02_AXI_bvalid;
  output [31:0]S02_AXI_rdata;
  output S02_AXI_rlast;
  input S02_AXI_rready;
  output [1:0]S02_AXI_rresp;
  output S02_AXI_rvalid;
  input [31:0]S02_AXI_wdata;
  input S02_AXI_wlast;
  output S02_AXI_wready;
  input [3:0]S02_AXI_wstrb;
  input S02_AXI_wvalid;
  input S03_ACLK;
  input [0:0]S03_ARESETN;
  input [31:0]S03_AXI_araddr;
  input [1:0]S03_AXI_arburst;
  input [3:0]S03_AXI_arcache;
  input [7:0]S03_AXI_arlen;
  input [2:0]S03_AXI_arprot;
  output S03_AXI_arready;
  input [2:0]S03_AXI_arsize;
  input S03_AXI_arvalid;
  output [31:0]S03_AXI_rdata;
  output S03_AXI_rlast;
  input S03_AXI_rready;
  output [1:0]S03_AXI_rresp;
  output S03_AXI_rvalid;
  input S04_ACLK;
  input [0:0]S04_ARESETN;
  input [31:0]S04_AXI_awaddr;
  input [1:0]S04_AXI_awburst;
  input [3:0]S04_AXI_awcache;
  input [7:0]S04_AXI_awlen;
  input [2:0]S04_AXI_awprot;
  output S04_AXI_awready;
  input [2:0]S04_AXI_awsize;
  input S04_AXI_awvalid;
  input S04_AXI_bready;
  output [1:0]S04_AXI_bresp;
  output S04_AXI_bvalid;
  input [31:0]S04_AXI_wdata;
  input S04_AXI_wlast;
  output S04_AXI_wready;
  input [3:0]S04_AXI_wstrb;
  input S04_AXI_wvalid;
  input S05_ACLK;
  input [0:0]S05_ARESETN;
  input [31:0]S05_AXI_araddr;
  input [1:0]S05_AXI_arburst;
  input [3:0]S05_AXI_arcache;
  input [7:0]S05_AXI_arlen;
  input [2:0]S05_AXI_arprot;
  output S05_AXI_arready;
  input [2:0]S05_AXI_arsize;
  input S05_AXI_arvalid;
  input [31:0]S05_AXI_awaddr;
  input [1:0]S05_AXI_awburst;
  input [3:0]S05_AXI_awcache;
  input [7:0]S05_AXI_awlen;
  input [2:0]S05_AXI_awprot;
  output S05_AXI_awready;
  input [2:0]S05_AXI_awsize;
  input S05_AXI_awvalid;
  input S05_AXI_bready;
  output [1:0]S05_AXI_bresp;
  output S05_AXI_bvalid;
  output [31:0]S05_AXI_rdata;
  output S05_AXI_rlast;
  input S05_AXI_rready;
  output [1:0]S05_AXI_rresp;
  output S05_AXI_rvalid;
  input [31:0]S05_AXI_wdata;
  input S05_AXI_wlast;
  output S05_AXI_wready;
  input [3:0]S05_AXI_wstrb;
  input S05_AXI_wvalid;
  input S06_ACLK;
  input [0:0]S06_ARESETN;
  input [31:0]S06_AXI_araddr;
  input [1:0]S06_AXI_arburst;
  input [3:0]S06_AXI_arcache;
  input [7:0]S06_AXI_arlen;
  input [2:0]S06_AXI_arprot;
  output S06_AXI_arready;
  input [2:0]S06_AXI_arsize;
  input S06_AXI_arvalid;
  output [31:0]S06_AXI_rdata;
  output S06_AXI_rlast;
  input S06_AXI_rready;
  output [1:0]S06_AXI_rresp;
  output S06_AXI_rvalid;
  input S07_ACLK;
  input [0:0]S07_ARESETN;
  input [31:0]S07_AXI_awaddr;
  input [1:0]S07_AXI_awburst;
  input [3:0]S07_AXI_awcache;
  input [7:0]S07_AXI_awlen;
  input [2:0]S07_AXI_awprot;
  output S07_AXI_awready;
  input [2:0]S07_AXI_awsize;
  input S07_AXI_awvalid;
  input S07_AXI_bready;
  output [1:0]S07_AXI_bresp;
  output S07_AXI_bvalid;
  input [31:0]S07_AXI_wdata;
  input S07_AXI_wlast;
  output S07_AXI_wready;
  input [3:0]S07_AXI_wstrb;
  input S07_AXI_wvalid;
  input S08_ACLK;
  input [0:0]S08_ARESETN;
  input [31:0]S08_AXI_araddr;
  input [1:0]S08_AXI_arburst;
  input [3:0]S08_AXI_arcache;
  input [7:0]S08_AXI_arlen;
  input [2:0]S08_AXI_arprot;
  output S08_AXI_arready;
  input [2:0]S08_AXI_arsize;
  input S08_AXI_arvalid;
  input [31:0]S08_AXI_awaddr;
  input [1:0]S08_AXI_awburst;
  input [3:0]S08_AXI_awcache;
  input [7:0]S08_AXI_awlen;
  input [2:0]S08_AXI_awprot;
  output S08_AXI_awready;
  input [2:0]S08_AXI_awsize;
  input S08_AXI_awvalid;
  input S08_AXI_bready;
  output [1:0]S08_AXI_bresp;
  output S08_AXI_bvalid;
  output [31:0]S08_AXI_rdata;
  output S08_AXI_rlast;
  input S08_AXI_rready;
  output [1:0]S08_AXI_rresp;
  output S08_AXI_rvalid;
  input [31:0]S08_AXI_wdata;
  input S08_AXI_wlast;
  output S08_AXI_wready;
  input [3:0]S08_AXI_wstrb;
  input S08_AXI_wvalid;
  input S09_ACLK;
  input [0:0]S09_ARESETN;
  input [31:0]S09_AXI_araddr;
  input [1:0]S09_AXI_arburst;
  input [3:0]S09_AXI_arcache;
  input [7:0]S09_AXI_arlen;
  input [2:0]S09_AXI_arprot;
  output S09_AXI_arready;
  input [2:0]S09_AXI_arsize;
  input S09_AXI_arvalid;
  output [31:0]S09_AXI_rdata;
  output S09_AXI_rlast;
  input S09_AXI_rready;
  output [1:0]S09_AXI_rresp;
  output S09_AXI_rvalid;
  input S10_ACLK;
  input [0:0]S10_ARESETN;
  input [31:0]S10_AXI_awaddr;
  input [1:0]S10_AXI_awburst;
  input [3:0]S10_AXI_awcache;
  input [7:0]S10_AXI_awlen;
  input [2:0]S10_AXI_awprot;
  output S10_AXI_awready;
  input [2:0]S10_AXI_awsize;
  input S10_AXI_awvalid;
  input S10_AXI_bready;
  output [1:0]S10_AXI_bresp;
  output S10_AXI_bvalid;
  input [31:0]S10_AXI_wdata;
  input S10_AXI_wlast;
  output S10_AXI_wready;
  input [3:0]S10_AXI_wstrb;
  input S10_AXI_wvalid;
  input S11_ACLK;
  input [0:0]S11_ARESETN;
  input [31:0]S11_AXI_araddr;
  input [1:0]S11_AXI_arburst;
  input [3:0]S11_AXI_arcache;
  input [7:0]S11_AXI_arlen;
  input [2:0]S11_AXI_arprot;
  output S11_AXI_arready;
  input [2:0]S11_AXI_arsize;
  input S11_AXI_arvalid;
  input [31:0]S11_AXI_awaddr;
  input [1:0]S11_AXI_awburst;
  input [3:0]S11_AXI_awcache;
  input [7:0]S11_AXI_awlen;
  input [2:0]S11_AXI_awprot;
  output S11_AXI_awready;
  input [2:0]S11_AXI_awsize;
  input S11_AXI_awvalid;
  input S11_AXI_bready;
  output [1:0]S11_AXI_bresp;
  output S11_AXI_bvalid;
  output [31:0]S11_AXI_rdata;
  output S11_AXI_rlast;
  input S11_AXI_rready;
  output [1:0]S11_AXI_rresp;
  output S11_AXI_rvalid;
  input [31:0]S11_AXI_wdata;
  input S11_AXI_wlast;
  output S11_AXI_wready;
  input [3:0]S11_AXI_wstrb;
  input S11_AXI_wvalid;
  input S12_ACLK;
  input [0:0]S12_ARESETN;
  input [31:0]S12_AXI_araddr;
  input [1:0]S12_AXI_arburst;
  input [3:0]S12_AXI_arcache;
  input [7:0]S12_AXI_arlen;
  input [2:0]S12_AXI_arprot;
  output S12_AXI_arready;
  input [2:0]S12_AXI_arsize;
  input S12_AXI_arvalid;
  output [31:0]S12_AXI_rdata;
  output S12_AXI_rlast;
  input S12_AXI_rready;
  output [1:0]S12_AXI_rresp;
  output S12_AXI_rvalid;
  input S13_ACLK;
  input [0:0]S13_ARESETN;
  input [31:0]S13_AXI_awaddr;
  input [1:0]S13_AXI_awburst;
  input [3:0]S13_AXI_awcache;
  input [7:0]S13_AXI_awlen;
  input [2:0]S13_AXI_awprot;
  output S13_AXI_awready;
  input [2:0]S13_AXI_awsize;
  input S13_AXI_awvalid;
  input S13_AXI_bready;
  output [1:0]S13_AXI_bresp;
  output S13_AXI_bvalid;
  input [31:0]S13_AXI_wdata;
  input S13_AXI_wlast;
  output S13_AXI_wready;
  input [3:0]S13_AXI_wstrb;
  input S13_AXI_wvalid;

  wire M00_ACLK_1;
  wire [0:0]M00_ARESETN_1;
  wire S00_ACLK_1;
  wire [0:0]S00_ARESETN_1;
  wire S01_ACLK_1;
  wire [0:0]S01_ARESETN_1;
  wire S02_ACLK_1;
  wire [0:0]S02_ARESETN_1;
  wire S03_ACLK_1;
  wire [0:0]S03_ARESETN_1;
  wire S04_ACLK_1;
  wire [0:0]S04_ARESETN_1;
  wire S05_ACLK_1;
  wire [0:0]S05_ARESETN_1;
  wire S06_ACLK_1;
  wire [0:0]S06_ARESETN_1;
  wire S07_ACLK_1;
  wire [0:0]S07_ARESETN_1;
  wire S08_ACLK_1;
  wire [0:0]S08_ARESETN_1;
  wire S09_ACLK_1;
  wire [0:0]S09_ARESETN_1;
  wire S10_ACLK_1;
  wire [0:0]S10_ARESETN_1;
  wire S11_ACLK_1;
  wire [0:0]S11_ARESETN_1;
  wire S12_ACLK_1;
  wire [0:0]S12_ARESETN_1;
  wire S13_ACLK_1;
  wire [0:0]S13_ARESETN_1;
  wire axi_mem_intercon_ACLK_net;
  wire [0:0]axi_mem_intercon_ARESETN_net;
  wire [31:0]axi_mem_intercon_to_s00_couplers_ARADDR;
  wire [1:0]axi_mem_intercon_to_s00_couplers_ARBURST;
  wire [3:0]axi_mem_intercon_to_s00_couplers_ARCACHE;
  wire [7:0]axi_mem_intercon_to_s00_couplers_ARLEN;
  wire [0:0]axi_mem_intercon_to_s00_couplers_ARLOCK;
  wire [2:0]axi_mem_intercon_to_s00_couplers_ARPROT;
  wire [3:0]axi_mem_intercon_to_s00_couplers_ARQOS;
  wire axi_mem_intercon_to_s00_couplers_ARREADY;
  wire [2:0]axi_mem_intercon_to_s00_couplers_ARSIZE;
  wire axi_mem_intercon_to_s00_couplers_ARVALID;
  wire [31:0]axi_mem_intercon_to_s00_couplers_AWADDR;
  wire [1:0]axi_mem_intercon_to_s00_couplers_AWBURST;
  wire [3:0]axi_mem_intercon_to_s00_couplers_AWCACHE;
  wire [7:0]axi_mem_intercon_to_s00_couplers_AWLEN;
  wire [0:0]axi_mem_intercon_to_s00_couplers_AWLOCK;
  wire [2:0]axi_mem_intercon_to_s00_couplers_AWPROT;
  wire [3:0]axi_mem_intercon_to_s00_couplers_AWQOS;
  wire axi_mem_intercon_to_s00_couplers_AWREADY;
  wire [2:0]axi_mem_intercon_to_s00_couplers_AWSIZE;
  wire axi_mem_intercon_to_s00_couplers_AWVALID;
  wire axi_mem_intercon_to_s00_couplers_BREADY;
  wire [1:0]axi_mem_intercon_to_s00_couplers_BRESP;
  wire axi_mem_intercon_to_s00_couplers_BVALID;
  wire [31:0]axi_mem_intercon_to_s00_couplers_RDATA;
  wire axi_mem_intercon_to_s00_couplers_RLAST;
  wire axi_mem_intercon_to_s00_couplers_RREADY;
  wire [1:0]axi_mem_intercon_to_s00_couplers_RRESP;
  wire axi_mem_intercon_to_s00_couplers_RVALID;
  wire [31:0]axi_mem_intercon_to_s00_couplers_WDATA;
  wire axi_mem_intercon_to_s00_couplers_WLAST;
  wire axi_mem_intercon_to_s00_couplers_WREADY;
  wire [3:0]axi_mem_intercon_to_s00_couplers_WSTRB;
  wire axi_mem_intercon_to_s00_couplers_WVALID;
  wire [31:0]axi_mem_intercon_to_s01_couplers_ARADDR;
  wire [1:0]axi_mem_intercon_to_s01_couplers_ARBURST;
  wire [3:0]axi_mem_intercon_to_s01_couplers_ARCACHE;
  wire [7:0]axi_mem_intercon_to_s01_couplers_ARLEN;
  wire [0:0]axi_mem_intercon_to_s01_couplers_ARLOCK;
  wire [2:0]axi_mem_intercon_to_s01_couplers_ARPROT;
  wire [3:0]axi_mem_intercon_to_s01_couplers_ARQOS;
  wire axi_mem_intercon_to_s01_couplers_ARREADY;
  wire [2:0]axi_mem_intercon_to_s01_couplers_ARSIZE;
  wire axi_mem_intercon_to_s01_couplers_ARVALID;
  wire [31:0]axi_mem_intercon_to_s01_couplers_RDATA;
  wire axi_mem_intercon_to_s01_couplers_RLAST;
  wire axi_mem_intercon_to_s01_couplers_RREADY;
  wire [1:0]axi_mem_intercon_to_s01_couplers_RRESP;
  wire axi_mem_intercon_to_s01_couplers_RVALID;
  wire [31:0]axi_mem_intercon_to_s02_couplers_ARADDR;
  wire [1:0]axi_mem_intercon_to_s02_couplers_ARBURST;
  wire [3:0]axi_mem_intercon_to_s02_couplers_ARCACHE;
  wire [7:0]axi_mem_intercon_to_s02_couplers_ARLEN;
  wire [2:0]axi_mem_intercon_to_s02_couplers_ARPROT;
  wire axi_mem_intercon_to_s02_couplers_ARREADY;
  wire [2:0]axi_mem_intercon_to_s02_couplers_ARSIZE;
  wire axi_mem_intercon_to_s02_couplers_ARVALID;
  wire [31:0]axi_mem_intercon_to_s02_couplers_AWADDR;
  wire [1:0]axi_mem_intercon_to_s02_couplers_AWBURST;
  wire [3:0]axi_mem_intercon_to_s02_couplers_AWCACHE;
  wire [7:0]axi_mem_intercon_to_s02_couplers_AWLEN;
  wire [2:0]axi_mem_intercon_to_s02_couplers_AWPROT;
  wire axi_mem_intercon_to_s02_couplers_AWREADY;
  wire [2:0]axi_mem_intercon_to_s02_couplers_AWSIZE;
  wire axi_mem_intercon_to_s02_couplers_AWVALID;
  wire axi_mem_intercon_to_s02_couplers_BREADY;
  wire [1:0]axi_mem_intercon_to_s02_couplers_BRESP;
  wire axi_mem_intercon_to_s02_couplers_BVALID;
  wire [31:0]axi_mem_intercon_to_s02_couplers_RDATA;
  wire axi_mem_intercon_to_s02_couplers_RLAST;
  wire axi_mem_intercon_to_s02_couplers_RREADY;
  wire [1:0]axi_mem_intercon_to_s02_couplers_RRESP;
  wire axi_mem_intercon_to_s02_couplers_RVALID;
  wire [31:0]axi_mem_intercon_to_s02_couplers_WDATA;
  wire axi_mem_intercon_to_s02_couplers_WLAST;
  wire axi_mem_intercon_to_s02_couplers_WREADY;
  wire [3:0]axi_mem_intercon_to_s02_couplers_WSTRB;
  wire axi_mem_intercon_to_s02_couplers_WVALID;
  wire [31:0]axi_mem_intercon_to_s03_couplers_ARADDR;
  wire [1:0]axi_mem_intercon_to_s03_couplers_ARBURST;
  wire [3:0]axi_mem_intercon_to_s03_couplers_ARCACHE;
  wire [7:0]axi_mem_intercon_to_s03_couplers_ARLEN;
  wire [2:0]axi_mem_intercon_to_s03_couplers_ARPROT;
  wire axi_mem_intercon_to_s03_couplers_ARREADY;
  wire [2:0]axi_mem_intercon_to_s03_couplers_ARSIZE;
  wire axi_mem_intercon_to_s03_couplers_ARVALID;
  wire [31:0]axi_mem_intercon_to_s03_couplers_RDATA;
  wire axi_mem_intercon_to_s03_couplers_RLAST;
  wire axi_mem_intercon_to_s03_couplers_RREADY;
  wire [1:0]axi_mem_intercon_to_s03_couplers_RRESP;
  wire axi_mem_intercon_to_s03_couplers_RVALID;
  wire [31:0]axi_mem_intercon_to_s04_couplers_AWADDR;
  wire [1:0]axi_mem_intercon_to_s04_couplers_AWBURST;
  wire [3:0]axi_mem_intercon_to_s04_couplers_AWCACHE;
  wire [7:0]axi_mem_intercon_to_s04_couplers_AWLEN;
  wire [2:0]axi_mem_intercon_to_s04_couplers_AWPROT;
  wire axi_mem_intercon_to_s04_couplers_AWREADY;
  wire [2:0]axi_mem_intercon_to_s04_couplers_AWSIZE;
  wire axi_mem_intercon_to_s04_couplers_AWVALID;
  wire axi_mem_intercon_to_s04_couplers_BREADY;
  wire [1:0]axi_mem_intercon_to_s04_couplers_BRESP;
  wire axi_mem_intercon_to_s04_couplers_BVALID;
  wire [31:0]axi_mem_intercon_to_s04_couplers_WDATA;
  wire axi_mem_intercon_to_s04_couplers_WLAST;
  wire axi_mem_intercon_to_s04_couplers_WREADY;
  wire [3:0]axi_mem_intercon_to_s04_couplers_WSTRB;
  wire axi_mem_intercon_to_s04_couplers_WVALID;
  wire [31:0]axi_mem_intercon_to_s05_couplers_ARADDR;
  wire [1:0]axi_mem_intercon_to_s05_couplers_ARBURST;
  wire [3:0]axi_mem_intercon_to_s05_couplers_ARCACHE;
  wire [7:0]axi_mem_intercon_to_s05_couplers_ARLEN;
  wire [2:0]axi_mem_intercon_to_s05_couplers_ARPROT;
  wire axi_mem_intercon_to_s05_couplers_ARREADY;
  wire [2:0]axi_mem_intercon_to_s05_couplers_ARSIZE;
  wire axi_mem_intercon_to_s05_couplers_ARVALID;
  wire [31:0]axi_mem_intercon_to_s05_couplers_AWADDR;
  wire [1:0]axi_mem_intercon_to_s05_couplers_AWBURST;
  wire [3:0]axi_mem_intercon_to_s05_couplers_AWCACHE;
  wire [7:0]axi_mem_intercon_to_s05_couplers_AWLEN;
  wire [2:0]axi_mem_intercon_to_s05_couplers_AWPROT;
  wire axi_mem_intercon_to_s05_couplers_AWREADY;
  wire [2:0]axi_mem_intercon_to_s05_couplers_AWSIZE;
  wire axi_mem_intercon_to_s05_couplers_AWVALID;
  wire axi_mem_intercon_to_s05_couplers_BREADY;
  wire [1:0]axi_mem_intercon_to_s05_couplers_BRESP;
  wire axi_mem_intercon_to_s05_couplers_BVALID;
  wire [31:0]axi_mem_intercon_to_s05_couplers_RDATA;
  wire axi_mem_intercon_to_s05_couplers_RLAST;
  wire axi_mem_intercon_to_s05_couplers_RREADY;
  wire [1:0]axi_mem_intercon_to_s05_couplers_RRESP;
  wire axi_mem_intercon_to_s05_couplers_RVALID;
  wire [31:0]axi_mem_intercon_to_s05_couplers_WDATA;
  wire axi_mem_intercon_to_s05_couplers_WLAST;
  wire axi_mem_intercon_to_s05_couplers_WREADY;
  wire [3:0]axi_mem_intercon_to_s05_couplers_WSTRB;
  wire axi_mem_intercon_to_s05_couplers_WVALID;
  wire [31:0]axi_mem_intercon_to_s06_couplers_ARADDR;
  wire [1:0]axi_mem_intercon_to_s06_couplers_ARBURST;
  wire [3:0]axi_mem_intercon_to_s06_couplers_ARCACHE;
  wire [7:0]axi_mem_intercon_to_s06_couplers_ARLEN;
  wire [2:0]axi_mem_intercon_to_s06_couplers_ARPROT;
  wire axi_mem_intercon_to_s06_couplers_ARREADY;
  wire [2:0]axi_mem_intercon_to_s06_couplers_ARSIZE;
  wire axi_mem_intercon_to_s06_couplers_ARVALID;
  wire [31:0]axi_mem_intercon_to_s06_couplers_RDATA;
  wire axi_mem_intercon_to_s06_couplers_RLAST;
  wire axi_mem_intercon_to_s06_couplers_RREADY;
  wire [1:0]axi_mem_intercon_to_s06_couplers_RRESP;
  wire axi_mem_intercon_to_s06_couplers_RVALID;
  wire [31:0]axi_mem_intercon_to_s07_couplers_AWADDR;
  wire [1:0]axi_mem_intercon_to_s07_couplers_AWBURST;
  wire [3:0]axi_mem_intercon_to_s07_couplers_AWCACHE;
  wire [7:0]axi_mem_intercon_to_s07_couplers_AWLEN;
  wire [2:0]axi_mem_intercon_to_s07_couplers_AWPROT;
  wire axi_mem_intercon_to_s07_couplers_AWREADY;
  wire [2:0]axi_mem_intercon_to_s07_couplers_AWSIZE;
  wire axi_mem_intercon_to_s07_couplers_AWVALID;
  wire axi_mem_intercon_to_s07_couplers_BREADY;
  wire [1:0]axi_mem_intercon_to_s07_couplers_BRESP;
  wire axi_mem_intercon_to_s07_couplers_BVALID;
  wire [31:0]axi_mem_intercon_to_s07_couplers_WDATA;
  wire axi_mem_intercon_to_s07_couplers_WLAST;
  wire axi_mem_intercon_to_s07_couplers_WREADY;
  wire [3:0]axi_mem_intercon_to_s07_couplers_WSTRB;
  wire axi_mem_intercon_to_s07_couplers_WVALID;
  wire [31:0]axi_mem_intercon_to_s08_couplers_ARADDR;
  wire [1:0]axi_mem_intercon_to_s08_couplers_ARBURST;
  wire [3:0]axi_mem_intercon_to_s08_couplers_ARCACHE;
  wire [7:0]axi_mem_intercon_to_s08_couplers_ARLEN;
  wire [2:0]axi_mem_intercon_to_s08_couplers_ARPROT;
  wire axi_mem_intercon_to_s08_couplers_ARREADY;
  wire [2:0]axi_mem_intercon_to_s08_couplers_ARSIZE;
  wire axi_mem_intercon_to_s08_couplers_ARVALID;
  wire [31:0]axi_mem_intercon_to_s08_couplers_AWADDR;
  wire [1:0]axi_mem_intercon_to_s08_couplers_AWBURST;
  wire [3:0]axi_mem_intercon_to_s08_couplers_AWCACHE;
  wire [7:0]axi_mem_intercon_to_s08_couplers_AWLEN;
  wire [2:0]axi_mem_intercon_to_s08_couplers_AWPROT;
  wire axi_mem_intercon_to_s08_couplers_AWREADY;
  wire [2:0]axi_mem_intercon_to_s08_couplers_AWSIZE;
  wire axi_mem_intercon_to_s08_couplers_AWVALID;
  wire axi_mem_intercon_to_s08_couplers_BREADY;
  wire [1:0]axi_mem_intercon_to_s08_couplers_BRESP;
  wire axi_mem_intercon_to_s08_couplers_BVALID;
  wire [31:0]axi_mem_intercon_to_s08_couplers_RDATA;
  wire axi_mem_intercon_to_s08_couplers_RLAST;
  wire axi_mem_intercon_to_s08_couplers_RREADY;
  wire [1:0]axi_mem_intercon_to_s08_couplers_RRESP;
  wire axi_mem_intercon_to_s08_couplers_RVALID;
  wire [31:0]axi_mem_intercon_to_s08_couplers_WDATA;
  wire axi_mem_intercon_to_s08_couplers_WLAST;
  wire axi_mem_intercon_to_s08_couplers_WREADY;
  wire [3:0]axi_mem_intercon_to_s08_couplers_WSTRB;
  wire axi_mem_intercon_to_s08_couplers_WVALID;
  wire [31:0]axi_mem_intercon_to_s09_couplers_ARADDR;
  wire [1:0]axi_mem_intercon_to_s09_couplers_ARBURST;
  wire [3:0]axi_mem_intercon_to_s09_couplers_ARCACHE;
  wire [7:0]axi_mem_intercon_to_s09_couplers_ARLEN;
  wire [2:0]axi_mem_intercon_to_s09_couplers_ARPROT;
  wire axi_mem_intercon_to_s09_couplers_ARREADY;
  wire [2:0]axi_mem_intercon_to_s09_couplers_ARSIZE;
  wire axi_mem_intercon_to_s09_couplers_ARVALID;
  wire [31:0]axi_mem_intercon_to_s09_couplers_RDATA;
  wire axi_mem_intercon_to_s09_couplers_RLAST;
  wire axi_mem_intercon_to_s09_couplers_RREADY;
  wire [1:0]axi_mem_intercon_to_s09_couplers_RRESP;
  wire axi_mem_intercon_to_s09_couplers_RVALID;
  wire [31:0]axi_mem_intercon_to_s10_couplers_AWADDR;
  wire [1:0]axi_mem_intercon_to_s10_couplers_AWBURST;
  wire [3:0]axi_mem_intercon_to_s10_couplers_AWCACHE;
  wire [7:0]axi_mem_intercon_to_s10_couplers_AWLEN;
  wire [2:0]axi_mem_intercon_to_s10_couplers_AWPROT;
  wire axi_mem_intercon_to_s10_couplers_AWREADY;
  wire [2:0]axi_mem_intercon_to_s10_couplers_AWSIZE;
  wire axi_mem_intercon_to_s10_couplers_AWVALID;
  wire axi_mem_intercon_to_s10_couplers_BREADY;
  wire [1:0]axi_mem_intercon_to_s10_couplers_BRESP;
  wire axi_mem_intercon_to_s10_couplers_BVALID;
  wire [31:0]axi_mem_intercon_to_s10_couplers_WDATA;
  wire axi_mem_intercon_to_s10_couplers_WLAST;
  wire axi_mem_intercon_to_s10_couplers_WREADY;
  wire [3:0]axi_mem_intercon_to_s10_couplers_WSTRB;
  wire axi_mem_intercon_to_s10_couplers_WVALID;
  wire [31:0]axi_mem_intercon_to_s11_couplers_ARADDR;
  wire [1:0]axi_mem_intercon_to_s11_couplers_ARBURST;
  wire [3:0]axi_mem_intercon_to_s11_couplers_ARCACHE;
  wire [7:0]axi_mem_intercon_to_s11_couplers_ARLEN;
  wire [2:0]axi_mem_intercon_to_s11_couplers_ARPROT;
  wire axi_mem_intercon_to_s11_couplers_ARREADY;
  wire [2:0]axi_mem_intercon_to_s11_couplers_ARSIZE;
  wire axi_mem_intercon_to_s11_couplers_ARVALID;
  wire [31:0]axi_mem_intercon_to_s11_couplers_AWADDR;
  wire [1:0]axi_mem_intercon_to_s11_couplers_AWBURST;
  wire [3:0]axi_mem_intercon_to_s11_couplers_AWCACHE;
  wire [7:0]axi_mem_intercon_to_s11_couplers_AWLEN;
  wire [2:0]axi_mem_intercon_to_s11_couplers_AWPROT;
  wire axi_mem_intercon_to_s11_couplers_AWREADY;
  wire [2:0]axi_mem_intercon_to_s11_couplers_AWSIZE;
  wire axi_mem_intercon_to_s11_couplers_AWVALID;
  wire axi_mem_intercon_to_s11_couplers_BREADY;
  wire [1:0]axi_mem_intercon_to_s11_couplers_BRESP;
  wire axi_mem_intercon_to_s11_couplers_BVALID;
  wire [31:0]axi_mem_intercon_to_s11_couplers_RDATA;
  wire axi_mem_intercon_to_s11_couplers_RLAST;
  wire axi_mem_intercon_to_s11_couplers_RREADY;
  wire [1:0]axi_mem_intercon_to_s11_couplers_RRESP;
  wire axi_mem_intercon_to_s11_couplers_RVALID;
  wire [31:0]axi_mem_intercon_to_s11_couplers_WDATA;
  wire axi_mem_intercon_to_s11_couplers_WLAST;
  wire axi_mem_intercon_to_s11_couplers_WREADY;
  wire [3:0]axi_mem_intercon_to_s11_couplers_WSTRB;
  wire axi_mem_intercon_to_s11_couplers_WVALID;
  wire [31:0]axi_mem_intercon_to_s12_couplers_ARADDR;
  wire [1:0]axi_mem_intercon_to_s12_couplers_ARBURST;
  wire [3:0]axi_mem_intercon_to_s12_couplers_ARCACHE;
  wire [7:0]axi_mem_intercon_to_s12_couplers_ARLEN;
  wire [2:0]axi_mem_intercon_to_s12_couplers_ARPROT;
  wire axi_mem_intercon_to_s12_couplers_ARREADY;
  wire [2:0]axi_mem_intercon_to_s12_couplers_ARSIZE;
  wire axi_mem_intercon_to_s12_couplers_ARVALID;
  wire [31:0]axi_mem_intercon_to_s12_couplers_RDATA;
  wire axi_mem_intercon_to_s12_couplers_RLAST;
  wire axi_mem_intercon_to_s12_couplers_RREADY;
  wire [1:0]axi_mem_intercon_to_s12_couplers_RRESP;
  wire axi_mem_intercon_to_s12_couplers_RVALID;
  wire [31:0]axi_mem_intercon_to_s13_couplers_AWADDR;
  wire [1:0]axi_mem_intercon_to_s13_couplers_AWBURST;
  wire [3:0]axi_mem_intercon_to_s13_couplers_AWCACHE;
  wire [7:0]axi_mem_intercon_to_s13_couplers_AWLEN;
  wire [2:0]axi_mem_intercon_to_s13_couplers_AWPROT;
  wire axi_mem_intercon_to_s13_couplers_AWREADY;
  wire [2:0]axi_mem_intercon_to_s13_couplers_AWSIZE;
  wire axi_mem_intercon_to_s13_couplers_AWVALID;
  wire axi_mem_intercon_to_s13_couplers_BREADY;
  wire [1:0]axi_mem_intercon_to_s13_couplers_BRESP;
  wire axi_mem_intercon_to_s13_couplers_BVALID;
  wire [31:0]axi_mem_intercon_to_s13_couplers_WDATA;
  wire axi_mem_intercon_to_s13_couplers_WLAST;
  wire axi_mem_intercon_to_s13_couplers_WREADY;
  wire [3:0]axi_mem_intercon_to_s13_couplers_WSTRB;
  wire axi_mem_intercon_to_s13_couplers_WVALID;
  wire [31:0]m00_couplers_to_axi_mem_intercon_ARADDR;
  wire [1:0]m00_couplers_to_axi_mem_intercon_ARBURST;
  wire [3:0]m00_couplers_to_axi_mem_intercon_ARCACHE;
  wire [3:0]m00_couplers_to_axi_mem_intercon_ARID;
  wire [7:0]m00_couplers_to_axi_mem_intercon_ARLEN;
  wire [0:0]m00_couplers_to_axi_mem_intercon_ARLOCK;
  wire [2:0]m00_couplers_to_axi_mem_intercon_ARPROT;
  wire [3:0]m00_couplers_to_axi_mem_intercon_ARQOS;
  wire [0:0]m00_couplers_to_axi_mem_intercon_ARREADY;
  wire [2:0]m00_couplers_to_axi_mem_intercon_ARSIZE;
  wire [0:0]m00_couplers_to_axi_mem_intercon_ARVALID;
  wire [31:0]m00_couplers_to_axi_mem_intercon_AWADDR;
  wire [1:0]m00_couplers_to_axi_mem_intercon_AWBURST;
  wire [3:0]m00_couplers_to_axi_mem_intercon_AWCACHE;
  wire [3:0]m00_couplers_to_axi_mem_intercon_AWID;
  wire [7:0]m00_couplers_to_axi_mem_intercon_AWLEN;
  wire [0:0]m00_couplers_to_axi_mem_intercon_AWLOCK;
  wire [2:0]m00_couplers_to_axi_mem_intercon_AWPROT;
  wire [3:0]m00_couplers_to_axi_mem_intercon_AWQOS;
  wire [0:0]m00_couplers_to_axi_mem_intercon_AWREADY;
  wire [2:0]m00_couplers_to_axi_mem_intercon_AWSIZE;
  wire [0:0]m00_couplers_to_axi_mem_intercon_AWVALID;
  wire [3:0]m00_couplers_to_axi_mem_intercon_BID;
  wire [0:0]m00_couplers_to_axi_mem_intercon_BREADY;
  wire [1:0]m00_couplers_to_axi_mem_intercon_BRESP;
  wire [0:0]m00_couplers_to_axi_mem_intercon_BVALID;
  wire [511:0]m00_couplers_to_axi_mem_intercon_RDATA;
  wire [3:0]m00_couplers_to_axi_mem_intercon_RID;
  wire [0:0]m00_couplers_to_axi_mem_intercon_RLAST;
  wire [0:0]m00_couplers_to_axi_mem_intercon_RREADY;
  wire [1:0]m00_couplers_to_axi_mem_intercon_RRESP;
  wire [0:0]m00_couplers_to_axi_mem_intercon_RVALID;
  wire [511:0]m00_couplers_to_axi_mem_intercon_WDATA;
  wire [0:0]m00_couplers_to_axi_mem_intercon_WLAST;
  wire [0:0]m00_couplers_to_axi_mem_intercon_WREADY;
  wire [63:0]m00_couplers_to_axi_mem_intercon_WSTRB;
  wire [0:0]m00_couplers_to_axi_mem_intercon_WVALID;
  wire [31:0]s00_couplers_to_xbar_ARADDR;
  wire [1:0]s00_couplers_to_xbar_ARBURST;
  wire [3:0]s00_couplers_to_xbar_ARCACHE;
  wire [7:0]s00_couplers_to_xbar_ARLEN;
  wire [0:0]s00_couplers_to_xbar_ARLOCK;
  wire [2:0]s00_couplers_to_xbar_ARPROT;
  wire [3:0]s00_couplers_to_xbar_ARQOS;
  wire [0:0]s00_couplers_to_xbar_ARREADY;
  wire [2:0]s00_couplers_to_xbar_ARSIZE;
  wire s00_couplers_to_xbar_ARVALID;
  wire [31:0]s00_couplers_to_xbar_AWADDR;
  wire [1:0]s00_couplers_to_xbar_AWBURST;
  wire [3:0]s00_couplers_to_xbar_AWCACHE;
  wire [7:0]s00_couplers_to_xbar_AWLEN;
  wire [0:0]s00_couplers_to_xbar_AWLOCK;
  wire [2:0]s00_couplers_to_xbar_AWPROT;
  wire [3:0]s00_couplers_to_xbar_AWQOS;
  wire [0:0]s00_couplers_to_xbar_AWREADY;
  wire [2:0]s00_couplers_to_xbar_AWSIZE;
  wire s00_couplers_to_xbar_AWVALID;
  wire s00_couplers_to_xbar_BREADY;
  wire [1:0]s00_couplers_to_xbar_BRESP;
  wire [0:0]s00_couplers_to_xbar_BVALID;
  wire [511:0]s00_couplers_to_xbar_RDATA;
  wire [0:0]s00_couplers_to_xbar_RLAST;
  wire s00_couplers_to_xbar_RREADY;
  wire [1:0]s00_couplers_to_xbar_RRESP;
  wire [0:0]s00_couplers_to_xbar_RVALID;
  wire [511:0]s00_couplers_to_xbar_WDATA;
  wire s00_couplers_to_xbar_WLAST;
  wire [0:0]s00_couplers_to_xbar_WREADY;
  wire [63:0]s00_couplers_to_xbar_WSTRB;
  wire s00_couplers_to_xbar_WVALID;
  wire [31:0]s01_couplers_to_xbar_ARADDR;
  wire [1:0]s01_couplers_to_xbar_ARBURST;
  wire [3:0]s01_couplers_to_xbar_ARCACHE;
  wire [7:0]s01_couplers_to_xbar_ARLEN;
  wire [0:0]s01_couplers_to_xbar_ARLOCK;
  wire [2:0]s01_couplers_to_xbar_ARPROT;
  wire [3:0]s01_couplers_to_xbar_ARQOS;
  wire [1:1]s01_couplers_to_xbar_ARREADY;
  wire [2:0]s01_couplers_to_xbar_ARSIZE;
  wire s01_couplers_to_xbar_ARVALID;
  wire [1023:512]s01_couplers_to_xbar_RDATA;
  wire [1:1]s01_couplers_to_xbar_RLAST;
  wire s01_couplers_to_xbar_RREADY;
  wire [3:2]s01_couplers_to_xbar_RRESP;
  wire [1:1]s01_couplers_to_xbar_RVALID;
  wire [31:0]s02_couplers_to_xbar_ARADDR;
  wire [1:0]s02_couplers_to_xbar_ARBURST;
  wire [3:0]s02_couplers_to_xbar_ARCACHE;
  wire [7:0]s02_couplers_to_xbar_ARLEN;
  wire [0:0]s02_couplers_to_xbar_ARLOCK;
  wire [2:0]s02_couplers_to_xbar_ARPROT;
  wire [3:0]s02_couplers_to_xbar_ARQOS;
  wire [2:2]s02_couplers_to_xbar_ARREADY;
  wire [2:0]s02_couplers_to_xbar_ARSIZE;
  wire s02_couplers_to_xbar_ARVALID;
  wire [31:0]s02_couplers_to_xbar_AWADDR;
  wire [1:0]s02_couplers_to_xbar_AWBURST;
  wire [3:0]s02_couplers_to_xbar_AWCACHE;
  wire [7:0]s02_couplers_to_xbar_AWLEN;
  wire [0:0]s02_couplers_to_xbar_AWLOCK;
  wire [2:0]s02_couplers_to_xbar_AWPROT;
  wire [3:0]s02_couplers_to_xbar_AWQOS;
  wire [2:2]s02_couplers_to_xbar_AWREADY;
  wire [2:0]s02_couplers_to_xbar_AWSIZE;
  wire s02_couplers_to_xbar_AWVALID;
  wire s02_couplers_to_xbar_BREADY;
  wire [5:4]s02_couplers_to_xbar_BRESP;
  wire [2:2]s02_couplers_to_xbar_BVALID;
  wire [1535:1024]s02_couplers_to_xbar_RDATA;
  wire [2:2]s02_couplers_to_xbar_RLAST;
  wire s02_couplers_to_xbar_RREADY;
  wire [5:4]s02_couplers_to_xbar_RRESP;
  wire [2:2]s02_couplers_to_xbar_RVALID;
  wire [511:0]s02_couplers_to_xbar_WDATA;
  wire s02_couplers_to_xbar_WLAST;
  wire [2:2]s02_couplers_to_xbar_WREADY;
  wire [63:0]s02_couplers_to_xbar_WSTRB;
  wire s02_couplers_to_xbar_WVALID;
  wire [31:0]s03_couplers_to_xbar_ARADDR;
  wire [1:0]s03_couplers_to_xbar_ARBURST;
  wire [3:0]s03_couplers_to_xbar_ARCACHE;
  wire [7:0]s03_couplers_to_xbar_ARLEN;
  wire [0:0]s03_couplers_to_xbar_ARLOCK;
  wire [2:0]s03_couplers_to_xbar_ARPROT;
  wire [3:0]s03_couplers_to_xbar_ARQOS;
  wire [3:3]s03_couplers_to_xbar_ARREADY;
  wire [2:0]s03_couplers_to_xbar_ARSIZE;
  wire s03_couplers_to_xbar_ARVALID;
  wire [2047:1536]s03_couplers_to_xbar_RDATA;
  wire [3:3]s03_couplers_to_xbar_RLAST;
  wire s03_couplers_to_xbar_RREADY;
  wire [7:6]s03_couplers_to_xbar_RRESP;
  wire [3:3]s03_couplers_to_xbar_RVALID;
  wire [31:0]s04_couplers_to_xbar_AWADDR;
  wire [1:0]s04_couplers_to_xbar_AWBURST;
  wire [3:0]s04_couplers_to_xbar_AWCACHE;
  wire [7:0]s04_couplers_to_xbar_AWLEN;
  wire [0:0]s04_couplers_to_xbar_AWLOCK;
  wire [2:0]s04_couplers_to_xbar_AWPROT;
  wire [3:0]s04_couplers_to_xbar_AWQOS;
  wire [4:4]s04_couplers_to_xbar_AWREADY;
  wire [2:0]s04_couplers_to_xbar_AWSIZE;
  wire s04_couplers_to_xbar_AWVALID;
  wire s04_couplers_to_xbar_BREADY;
  wire [9:8]s04_couplers_to_xbar_BRESP;
  wire [4:4]s04_couplers_to_xbar_BVALID;
  wire [511:0]s04_couplers_to_xbar_WDATA;
  wire s04_couplers_to_xbar_WLAST;
  wire [4:4]s04_couplers_to_xbar_WREADY;
  wire [63:0]s04_couplers_to_xbar_WSTRB;
  wire s04_couplers_to_xbar_WVALID;
  wire [31:0]s05_couplers_to_xbar_ARADDR;
  wire [1:0]s05_couplers_to_xbar_ARBURST;
  wire [3:0]s05_couplers_to_xbar_ARCACHE;
  wire [7:0]s05_couplers_to_xbar_ARLEN;
  wire [0:0]s05_couplers_to_xbar_ARLOCK;
  wire [2:0]s05_couplers_to_xbar_ARPROT;
  wire [3:0]s05_couplers_to_xbar_ARQOS;
  wire [5:5]s05_couplers_to_xbar_ARREADY;
  wire [2:0]s05_couplers_to_xbar_ARSIZE;
  wire s05_couplers_to_xbar_ARVALID;
  wire [31:0]s05_couplers_to_xbar_AWADDR;
  wire [1:0]s05_couplers_to_xbar_AWBURST;
  wire [3:0]s05_couplers_to_xbar_AWCACHE;
  wire [7:0]s05_couplers_to_xbar_AWLEN;
  wire [0:0]s05_couplers_to_xbar_AWLOCK;
  wire [2:0]s05_couplers_to_xbar_AWPROT;
  wire [3:0]s05_couplers_to_xbar_AWQOS;
  wire [5:5]s05_couplers_to_xbar_AWREADY;
  wire [2:0]s05_couplers_to_xbar_AWSIZE;
  wire s05_couplers_to_xbar_AWVALID;
  wire s05_couplers_to_xbar_BREADY;
  wire [11:10]s05_couplers_to_xbar_BRESP;
  wire [5:5]s05_couplers_to_xbar_BVALID;
  wire [3071:2560]s05_couplers_to_xbar_RDATA;
  wire [5:5]s05_couplers_to_xbar_RLAST;
  wire s05_couplers_to_xbar_RREADY;
  wire [11:10]s05_couplers_to_xbar_RRESP;
  wire [5:5]s05_couplers_to_xbar_RVALID;
  wire [511:0]s05_couplers_to_xbar_WDATA;
  wire s05_couplers_to_xbar_WLAST;
  wire [5:5]s05_couplers_to_xbar_WREADY;
  wire [63:0]s05_couplers_to_xbar_WSTRB;
  wire s05_couplers_to_xbar_WVALID;
  wire [31:0]s06_couplers_to_xbar_ARADDR;
  wire [1:0]s06_couplers_to_xbar_ARBURST;
  wire [3:0]s06_couplers_to_xbar_ARCACHE;
  wire [7:0]s06_couplers_to_xbar_ARLEN;
  wire [0:0]s06_couplers_to_xbar_ARLOCK;
  wire [2:0]s06_couplers_to_xbar_ARPROT;
  wire [3:0]s06_couplers_to_xbar_ARQOS;
  wire [6:6]s06_couplers_to_xbar_ARREADY;
  wire [2:0]s06_couplers_to_xbar_ARSIZE;
  wire s06_couplers_to_xbar_ARVALID;
  wire [3583:3072]s06_couplers_to_xbar_RDATA;
  wire [6:6]s06_couplers_to_xbar_RLAST;
  wire s06_couplers_to_xbar_RREADY;
  wire [13:12]s06_couplers_to_xbar_RRESP;
  wire [6:6]s06_couplers_to_xbar_RVALID;
  wire [31:0]s07_couplers_to_xbar_AWADDR;
  wire [1:0]s07_couplers_to_xbar_AWBURST;
  wire [3:0]s07_couplers_to_xbar_AWCACHE;
  wire [7:0]s07_couplers_to_xbar_AWLEN;
  wire [0:0]s07_couplers_to_xbar_AWLOCK;
  wire [2:0]s07_couplers_to_xbar_AWPROT;
  wire [3:0]s07_couplers_to_xbar_AWQOS;
  wire [7:7]s07_couplers_to_xbar_AWREADY;
  wire [2:0]s07_couplers_to_xbar_AWSIZE;
  wire s07_couplers_to_xbar_AWVALID;
  wire s07_couplers_to_xbar_BREADY;
  wire [15:14]s07_couplers_to_xbar_BRESP;
  wire [7:7]s07_couplers_to_xbar_BVALID;
  wire [511:0]s07_couplers_to_xbar_WDATA;
  wire s07_couplers_to_xbar_WLAST;
  wire [7:7]s07_couplers_to_xbar_WREADY;
  wire [63:0]s07_couplers_to_xbar_WSTRB;
  wire s07_couplers_to_xbar_WVALID;
  wire [31:0]s08_couplers_to_xbar_ARADDR;
  wire [1:0]s08_couplers_to_xbar_ARBURST;
  wire [3:0]s08_couplers_to_xbar_ARCACHE;
  wire [7:0]s08_couplers_to_xbar_ARLEN;
  wire [0:0]s08_couplers_to_xbar_ARLOCK;
  wire [2:0]s08_couplers_to_xbar_ARPROT;
  wire [3:0]s08_couplers_to_xbar_ARQOS;
  wire [8:8]s08_couplers_to_xbar_ARREADY;
  wire [2:0]s08_couplers_to_xbar_ARSIZE;
  wire s08_couplers_to_xbar_ARVALID;
  wire [31:0]s08_couplers_to_xbar_AWADDR;
  wire [1:0]s08_couplers_to_xbar_AWBURST;
  wire [3:0]s08_couplers_to_xbar_AWCACHE;
  wire [7:0]s08_couplers_to_xbar_AWLEN;
  wire [0:0]s08_couplers_to_xbar_AWLOCK;
  wire [2:0]s08_couplers_to_xbar_AWPROT;
  wire [3:0]s08_couplers_to_xbar_AWQOS;
  wire [8:8]s08_couplers_to_xbar_AWREADY;
  wire [2:0]s08_couplers_to_xbar_AWSIZE;
  wire s08_couplers_to_xbar_AWVALID;
  wire s08_couplers_to_xbar_BREADY;
  wire [17:16]s08_couplers_to_xbar_BRESP;
  wire [8:8]s08_couplers_to_xbar_BVALID;
  wire [4607:4096]s08_couplers_to_xbar_RDATA;
  wire [8:8]s08_couplers_to_xbar_RLAST;
  wire s08_couplers_to_xbar_RREADY;
  wire [17:16]s08_couplers_to_xbar_RRESP;
  wire [8:8]s08_couplers_to_xbar_RVALID;
  wire [511:0]s08_couplers_to_xbar_WDATA;
  wire s08_couplers_to_xbar_WLAST;
  wire [8:8]s08_couplers_to_xbar_WREADY;
  wire [63:0]s08_couplers_to_xbar_WSTRB;
  wire s08_couplers_to_xbar_WVALID;
  wire [31:0]s09_couplers_to_xbar_ARADDR;
  wire [1:0]s09_couplers_to_xbar_ARBURST;
  wire [3:0]s09_couplers_to_xbar_ARCACHE;
  wire [7:0]s09_couplers_to_xbar_ARLEN;
  wire [0:0]s09_couplers_to_xbar_ARLOCK;
  wire [2:0]s09_couplers_to_xbar_ARPROT;
  wire [3:0]s09_couplers_to_xbar_ARQOS;
  wire [9:9]s09_couplers_to_xbar_ARREADY;
  wire [2:0]s09_couplers_to_xbar_ARSIZE;
  wire s09_couplers_to_xbar_ARVALID;
  wire [5119:4608]s09_couplers_to_xbar_RDATA;
  wire [9:9]s09_couplers_to_xbar_RLAST;
  wire s09_couplers_to_xbar_RREADY;
  wire [19:18]s09_couplers_to_xbar_RRESP;
  wire [9:9]s09_couplers_to_xbar_RVALID;
  wire [31:0]s10_couplers_to_xbar_AWADDR;
  wire [1:0]s10_couplers_to_xbar_AWBURST;
  wire [3:0]s10_couplers_to_xbar_AWCACHE;
  wire [7:0]s10_couplers_to_xbar_AWLEN;
  wire [0:0]s10_couplers_to_xbar_AWLOCK;
  wire [2:0]s10_couplers_to_xbar_AWPROT;
  wire [3:0]s10_couplers_to_xbar_AWQOS;
  wire [10:10]s10_couplers_to_xbar_AWREADY;
  wire [2:0]s10_couplers_to_xbar_AWSIZE;
  wire s10_couplers_to_xbar_AWVALID;
  wire s10_couplers_to_xbar_BREADY;
  wire [21:20]s10_couplers_to_xbar_BRESP;
  wire [10:10]s10_couplers_to_xbar_BVALID;
  wire [511:0]s10_couplers_to_xbar_WDATA;
  wire s10_couplers_to_xbar_WLAST;
  wire [10:10]s10_couplers_to_xbar_WREADY;
  wire [63:0]s10_couplers_to_xbar_WSTRB;
  wire s10_couplers_to_xbar_WVALID;
  wire [31:0]s11_couplers_to_xbar_ARADDR;
  wire [1:0]s11_couplers_to_xbar_ARBURST;
  wire [3:0]s11_couplers_to_xbar_ARCACHE;
  wire [7:0]s11_couplers_to_xbar_ARLEN;
  wire [0:0]s11_couplers_to_xbar_ARLOCK;
  wire [2:0]s11_couplers_to_xbar_ARPROT;
  wire [3:0]s11_couplers_to_xbar_ARQOS;
  wire [11:11]s11_couplers_to_xbar_ARREADY;
  wire [2:0]s11_couplers_to_xbar_ARSIZE;
  wire s11_couplers_to_xbar_ARVALID;
  wire [31:0]s11_couplers_to_xbar_AWADDR;
  wire [1:0]s11_couplers_to_xbar_AWBURST;
  wire [3:0]s11_couplers_to_xbar_AWCACHE;
  wire [7:0]s11_couplers_to_xbar_AWLEN;
  wire [0:0]s11_couplers_to_xbar_AWLOCK;
  wire [2:0]s11_couplers_to_xbar_AWPROT;
  wire [3:0]s11_couplers_to_xbar_AWQOS;
  wire [11:11]s11_couplers_to_xbar_AWREADY;
  wire [2:0]s11_couplers_to_xbar_AWSIZE;
  wire s11_couplers_to_xbar_AWVALID;
  wire s11_couplers_to_xbar_BREADY;
  wire [23:22]s11_couplers_to_xbar_BRESP;
  wire [11:11]s11_couplers_to_xbar_BVALID;
  wire [6143:5632]s11_couplers_to_xbar_RDATA;
  wire [11:11]s11_couplers_to_xbar_RLAST;
  wire s11_couplers_to_xbar_RREADY;
  wire [23:22]s11_couplers_to_xbar_RRESP;
  wire [11:11]s11_couplers_to_xbar_RVALID;
  wire [511:0]s11_couplers_to_xbar_WDATA;
  wire s11_couplers_to_xbar_WLAST;
  wire [11:11]s11_couplers_to_xbar_WREADY;
  wire [63:0]s11_couplers_to_xbar_WSTRB;
  wire s11_couplers_to_xbar_WVALID;
  wire [31:0]s12_couplers_to_xbar_ARADDR;
  wire [1:0]s12_couplers_to_xbar_ARBURST;
  wire [3:0]s12_couplers_to_xbar_ARCACHE;
  wire [7:0]s12_couplers_to_xbar_ARLEN;
  wire [0:0]s12_couplers_to_xbar_ARLOCK;
  wire [2:0]s12_couplers_to_xbar_ARPROT;
  wire [3:0]s12_couplers_to_xbar_ARQOS;
  wire [12:12]s12_couplers_to_xbar_ARREADY;
  wire [2:0]s12_couplers_to_xbar_ARSIZE;
  wire s12_couplers_to_xbar_ARVALID;
  wire [6655:6144]s12_couplers_to_xbar_RDATA;
  wire [12:12]s12_couplers_to_xbar_RLAST;
  wire s12_couplers_to_xbar_RREADY;
  wire [25:24]s12_couplers_to_xbar_RRESP;
  wire [12:12]s12_couplers_to_xbar_RVALID;
  wire [31:0]s13_couplers_to_xbar_AWADDR;
  wire [1:0]s13_couplers_to_xbar_AWBURST;
  wire [3:0]s13_couplers_to_xbar_AWCACHE;
  wire [7:0]s13_couplers_to_xbar_AWLEN;
  wire [0:0]s13_couplers_to_xbar_AWLOCK;
  wire [2:0]s13_couplers_to_xbar_AWPROT;
  wire [3:0]s13_couplers_to_xbar_AWQOS;
  wire [13:13]s13_couplers_to_xbar_AWREADY;
  wire [2:0]s13_couplers_to_xbar_AWSIZE;
  wire s13_couplers_to_xbar_AWVALID;
  wire s13_couplers_to_xbar_BREADY;
  wire [27:26]s13_couplers_to_xbar_BRESP;
  wire [13:13]s13_couplers_to_xbar_BVALID;
  wire [511:0]s13_couplers_to_xbar_WDATA;
  wire s13_couplers_to_xbar_WLAST;
  wire [13:13]s13_couplers_to_xbar_WREADY;
  wire [63:0]s13_couplers_to_xbar_WSTRB;
  wire s13_couplers_to_xbar_WVALID;
  wire [31:0]xbar_to_m00_couplers_ARADDR;
  wire [1:0]xbar_to_m00_couplers_ARBURST;
  wire [3:0]xbar_to_m00_couplers_ARCACHE;
  wire [3:0]xbar_to_m00_couplers_ARID;
  wire [7:0]xbar_to_m00_couplers_ARLEN;
  wire [0:0]xbar_to_m00_couplers_ARLOCK;
  wire [2:0]xbar_to_m00_couplers_ARPROT;
  wire [3:0]xbar_to_m00_couplers_ARQOS;
  wire [0:0]xbar_to_m00_couplers_ARREADY;
  wire [2:0]xbar_to_m00_couplers_ARSIZE;
  wire [0:0]xbar_to_m00_couplers_ARVALID;
  wire [31:0]xbar_to_m00_couplers_AWADDR;
  wire [1:0]xbar_to_m00_couplers_AWBURST;
  wire [3:0]xbar_to_m00_couplers_AWCACHE;
  wire [3:0]xbar_to_m00_couplers_AWID;
  wire [7:0]xbar_to_m00_couplers_AWLEN;
  wire [0:0]xbar_to_m00_couplers_AWLOCK;
  wire [2:0]xbar_to_m00_couplers_AWPROT;
  wire [3:0]xbar_to_m00_couplers_AWQOS;
  wire [0:0]xbar_to_m00_couplers_AWREADY;
  wire [2:0]xbar_to_m00_couplers_AWSIZE;
  wire [0:0]xbar_to_m00_couplers_AWVALID;
  wire [3:0]xbar_to_m00_couplers_BID;
  wire [0:0]xbar_to_m00_couplers_BREADY;
  wire [1:0]xbar_to_m00_couplers_BRESP;
  wire [0:0]xbar_to_m00_couplers_BVALID;
  wire [511:0]xbar_to_m00_couplers_RDATA;
  wire [3:0]xbar_to_m00_couplers_RID;
  wire [0:0]xbar_to_m00_couplers_RLAST;
  wire [0:0]xbar_to_m00_couplers_RREADY;
  wire [1:0]xbar_to_m00_couplers_RRESP;
  wire [0:0]xbar_to_m00_couplers_RVALID;
  wire [511:0]xbar_to_m00_couplers_WDATA;
  wire [0:0]xbar_to_m00_couplers_WLAST;
  wire [0:0]xbar_to_m00_couplers_WREADY;
  wire [63:0]xbar_to_m00_couplers_WSTRB;
  wire [0:0]xbar_to_m00_couplers_WVALID;
  wire [13:0]NLW_xbar_s_axi_arready_UNCONNECTED;
  wire [13:0]NLW_xbar_s_axi_awready_UNCONNECTED;
  wire [27:0]NLW_xbar_s_axi_bresp_UNCONNECTED;
  wire [13:0]NLW_xbar_s_axi_bvalid_UNCONNECTED;
  wire [7167:0]NLW_xbar_s_axi_rdata_UNCONNECTED;
  wire [13:0]NLW_xbar_s_axi_rlast_UNCONNECTED;
  wire [27:0]NLW_xbar_s_axi_rresp_UNCONNECTED;
  wire [13:0]NLW_xbar_s_axi_rvalid_UNCONNECTED;
  wire [13:0]NLW_xbar_s_axi_wready_UNCONNECTED;

  assign M00_ACLK_1 = M00_ACLK;
  assign M00_ARESETN_1 = M00_ARESETN[0];
  assign M00_AXI_araddr[31:0] = m00_couplers_to_axi_mem_intercon_ARADDR;
  assign M00_AXI_arburst[1:0] = m00_couplers_to_axi_mem_intercon_ARBURST;
  assign M00_AXI_arcache[3:0] = m00_couplers_to_axi_mem_intercon_ARCACHE;
  assign M00_AXI_arid[3:0] = m00_couplers_to_axi_mem_intercon_ARID;
  assign M00_AXI_arlen[7:0] = m00_couplers_to_axi_mem_intercon_ARLEN;
  assign M00_AXI_arlock[0] = m00_couplers_to_axi_mem_intercon_ARLOCK;
  assign M00_AXI_arprot[2:0] = m00_couplers_to_axi_mem_intercon_ARPROT;
  assign M00_AXI_arqos[3:0] = m00_couplers_to_axi_mem_intercon_ARQOS;
  assign M00_AXI_arsize[2:0] = m00_couplers_to_axi_mem_intercon_ARSIZE;
  assign M00_AXI_arvalid[0] = m00_couplers_to_axi_mem_intercon_ARVALID;
  assign M00_AXI_awaddr[31:0] = m00_couplers_to_axi_mem_intercon_AWADDR;
  assign M00_AXI_awburst[1:0] = m00_couplers_to_axi_mem_intercon_AWBURST;
  assign M00_AXI_awcache[3:0] = m00_couplers_to_axi_mem_intercon_AWCACHE;
  assign M00_AXI_awid[3:0] = m00_couplers_to_axi_mem_intercon_AWID;
  assign M00_AXI_awlen[7:0] = m00_couplers_to_axi_mem_intercon_AWLEN;
  assign M00_AXI_awlock[0] = m00_couplers_to_axi_mem_intercon_AWLOCK;
  assign M00_AXI_awprot[2:0] = m00_couplers_to_axi_mem_intercon_AWPROT;
  assign M00_AXI_awqos[3:0] = m00_couplers_to_axi_mem_intercon_AWQOS;
  assign M00_AXI_awsize[2:0] = m00_couplers_to_axi_mem_intercon_AWSIZE;
  assign M00_AXI_awvalid[0] = m00_couplers_to_axi_mem_intercon_AWVALID;
  assign M00_AXI_bready[0] = m00_couplers_to_axi_mem_intercon_BREADY;
  assign M00_AXI_rready[0] = m00_couplers_to_axi_mem_intercon_RREADY;
  assign M00_AXI_wdata[511:0] = m00_couplers_to_axi_mem_intercon_WDATA;
  assign M00_AXI_wlast[0] = m00_couplers_to_axi_mem_intercon_WLAST;
  assign M00_AXI_wstrb[63:0] = m00_couplers_to_axi_mem_intercon_WSTRB;
  assign M00_AXI_wvalid[0] = m00_couplers_to_axi_mem_intercon_WVALID;
  assign S00_ACLK_1 = S00_ACLK;
  assign S00_ARESETN_1 = S00_ARESETN[0];
  assign S00_AXI_arready = axi_mem_intercon_to_s00_couplers_ARREADY;
  assign S00_AXI_awready = axi_mem_intercon_to_s00_couplers_AWREADY;
  assign S00_AXI_bresp[1:0] = axi_mem_intercon_to_s00_couplers_BRESP;
  assign S00_AXI_bvalid = axi_mem_intercon_to_s00_couplers_BVALID;
  assign S00_AXI_rdata[31:0] = axi_mem_intercon_to_s00_couplers_RDATA;
  assign S00_AXI_rlast = axi_mem_intercon_to_s00_couplers_RLAST;
  assign S00_AXI_rresp[1:0] = axi_mem_intercon_to_s00_couplers_RRESP;
  assign S00_AXI_rvalid = axi_mem_intercon_to_s00_couplers_RVALID;
  assign S00_AXI_wready = axi_mem_intercon_to_s00_couplers_WREADY;
  assign S01_ACLK_1 = S01_ACLK;
  assign S01_ARESETN_1 = S01_ARESETN[0];
  assign S01_AXI_arready = axi_mem_intercon_to_s01_couplers_ARREADY;
  assign S01_AXI_rdata[31:0] = axi_mem_intercon_to_s01_couplers_RDATA;
  assign S01_AXI_rlast = axi_mem_intercon_to_s01_couplers_RLAST;
  assign S01_AXI_rresp[1:0] = axi_mem_intercon_to_s01_couplers_RRESP;
  assign S01_AXI_rvalid = axi_mem_intercon_to_s01_couplers_RVALID;
  assign S02_ACLK_1 = S02_ACLK;
  assign S02_ARESETN_1 = S02_ARESETN[0];
  assign S02_AXI_arready = axi_mem_intercon_to_s02_couplers_ARREADY;
  assign S02_AXI_awready = axi_mem_intercon_to_s02_couplers_AWREADY;
  assign S02_AXI_bresp[1:0] = axi_mem_intercon_to_s02_couplers_BRESP;
  assign S02_AXI_bvalid = axi_mem_intercon_to_s02_couplers_BVALID;
  assign S02_AXI_rdata[31:0] = axi_mem_intercon_to_s02_couplers_RDATA;
  assign S02_AXI_rlast = axi_mem_intercon_to_s02_couplers_RLAST;
  assign S02_AXI_rresp[1:0] = axi_mem_intercon_to_s02_couplers_RRESP;
  assign S02_AXI_rvalid = axi_mem_intercon_to_s02_couplers_RVALID;
  assign S02_AXI_wready = axi_mem_intercon_to_s02_couplers_WREADY;
  assign S03_ACLK_1 = S03_ACLK;
  assign S03_ARESETN_1 = S03_ARESETN[0];
  assign S03_AXI_arready = axi_mem_intercon_to_s03_couplers_ARREADY;
  assign S03_AXI_rdata[31:0] = axi_mem_intercon_to_s03_couplers_RDATA;
  assign S03_AXI_rlast = axi_mem_intercon_to_s03_couplers_RLAST;
  assign S03_AXI_rresp[1:0] = axi_mem_intercon_to_s03_couplers_RRESP;
  assign S03_AXI_rvalid = axi_mem_intercon_to_s03_couplers_RVALID;
  assign S04_ACLK_1 = S04_ACLK;
  assign S04_ARESETN_1 = S04_ARESETN[0];
  assign S04_AXI_awready = axi_mem_intercon_to_s04_couplers_AWREADY;
  assign S04_AXI_bresp[1:0] = axi_mem_intercon_to_s04_couplers_BRESP;
  assign S04_AXI_bvalid = axi_mem_intercon_to_s04_couplers_BVALID;
  assign S04_AXI_wready = axi_mem_intercon_to_s04_couplers_WREADY;
  assign S05_ACLK_1 = S05_ACLK;
  assign S05_ARESETN_1 = S05_ARESETN[0];
  assign S05_AXI_arready = axi_mem_intercon_to_s05_couplers_ARREADY;
  assign S05_AXI_awready = axi_mem_intercon_to_s05_couplers_AWREADY;
  assign S05_AXI_bresp[1:0] = axi_mem_intercon_to_s05_couplers_BRESP;
  assign S05_AXI_bvalid = axi_mem_intercon_to_s05_couplers_BVALID;
  assign S05_AXI_rdata[31:0] = axi_mem_intercon_to_s05_couplers_RDATA;
  assign S05_AXI_rlast = axi_mem_intercon_to_s05_couplers_RLAST;
  assign S05_AXI_rresp[1:0] = axi_mem_intercon_to_s05_couplers_RRESP;
  assign S05_AXI_rvalid = axi_mem_intercon_to_s05_couplers_RVALID;
  assign S05_AXI_wready = axi_mem_intercon_to_s05_couplers_WREADY;
  assign S06_ACLK_1 = S06_ACLK;
  assign S06_ARESETN_1 = S06_ARESETN[0];
  assign S06_AXI_arready = axi_mem_intercon_to_s06_couplers_ARREADY;
  assign S06_AXI_rdata[31:0] = axi_mem_intercon_to_s06_couplers_RDATA;
  assign S06_AXI_rlast = axi_mem_intercon_to_s06_couplers_RLAST;
  assign S06_AXI_rresp[1:0] = axi_mem_intercon_to_s06_couplers_RRESP;
  assign S06_AXI_rvalid = axi_mem_intercon_to_s06_couplers_RVALID;
  assign S07_ACLK_1 = S07_ACLK;
  assign S07_ARESETN_1 = S07_ARESETN[0];
  assign S07_AXI_awready = axi_mem_intercon_to_s07_couplers_AWREADY;
  assign S07_AXI_bresp[1:0] = axi_mem_intercon_to_s07_couplers_BRESP;
  assign S07_AXI_bvalid = axi_mem_intercon_to_s07_couplers_BVALID;
  assign S07_AXI_wready = axi_mem_intercon_to_s07_couplers_WREADY;
  assign S08_ACLK_1 = S08_ACLK;
  assign S08_ARESETN_1 = S08_ARESETN[0];
  assign S08_AXI_arready = axi_mem_intercon_to_s08_couplers_ARREADY;
  assign S08_AXI_awready = axi_mem_intercon_to_s08_couplers_AWREADY;
  assign S08_AXI_bresp[1:0] = axi_mem_intercon_to_s08_couplers_BRESP;
  assign S08_AXI_bvalid = axi_mem_intercon_to_s08_couplers_BVALID;
  assign S08_AXI_rdata[31:0] = axi_mem_intercon_to_s08_couplers_RDATA;
  assign S08_AXI_rlast = axi_mem_intercon_to_s08_couplers_RLAST;
  assign S08_AXI_rresp[1:0] = axi_mem_intercon_to_s08_couplers_RRESP;
  assign S08_AXI_rvalid = axi_mem_intercon_to_s08_couplers_RVALID;
  assign S08_AXI_wready = axi_mem_intercon_to_s08_couplers_WREADY;
  assign S09_ACLK_1 = S09_ACLK;
  assign S09_ARESETN_1 = S09_ARESETN[0];
  assign S09_AXI_arready = axi_mem_intercon_to_s09_couplers_ARREADY;
  assign S09_AXI_rdata[31:0] = axi_mem_intercon_to_s09_couplers_RDATA;
  assign S09_AXI_rlast = axi_mem_intercon_to_s09_couplers_RLAST;
  assign S09_AXI_rresp[1:0] = axi_mem_intercon_to_s09_couplers_RRESP;
  assign S09_AXI_rvalid = axi_mem_intercon_to_s09_couplers_RVALID;
  assign S10_ACLK_1 = S10_ACLK;
  assign S10_ARESETN_1 = S10_ARESETN[0];
  assign S10_AXI_awready = axi_mem_intercon_to_s10_couplers_AWREADY;
  assign S10_AXI_bresp[1:0] = axi_mem_intercon_to_s10_couplers_BRESP;
  assign S10_AXI_bvalid = axi_mem_intercon_to_s10_couplers_BVALID;
  assign S10_AXI_wready = axi_mem_intercon_to_s10_couplers_WREADY;
  assign S11_ACLK_1 = S11_ACLK;
  assign S11_ARESETN_1 = S11_ARESETN[0];
  assign S11_AXI_arready = axi_mem_intercon_to_s11_couplers_ARREADY;
  assign S11_AXI_awready = axi_mem_intercon_to_s11_couplers_AWREADY;
  assign S11_AXI_bresp[1:0] = axi_mem_intercon_to_s11_couplers_BRESP;
  assign S11_AXI_bvalid = axi_mem_intercon_to_s11_couplers_BVALID;
  assign S11_AXI_rdata[31:0] = axi_mem_intercon_to_s11_couplers_RDATA;
  assign S11_AXI_rlast = axi_mem_intercon_to_s11_couplers_RLAST;
  assign S11_AXI_rresp[1:0] = axi_mem_intercon_to_s11_couplers_RRESP;
  assign S11_AXI_rvalid = axi_mem_intercon_to_s11_couplers_RVALID;
  assign S11_AXI_wready = axi_mem_intercon_to_s11_couplers_WREADY;
  assign S12_ACLK_1 = S12_ACLK;
  assign S12_ARESETN_1 = S12_ARESETN[0];
  assign S12_AXI_arready = axi_mem_intercon_to_s12_couplers_ARREADY;
  assign S12_AXI_rdata[31:0] = axi_mem_intercon_to_s12_couplers_RDATA;
  assign S12_AXI_rlast = axi_mem_intercon_to_s12_couplers_RLAST;
  assign S12_AXI_rresp[1:0] = axi_mem_intercon_to_s12_couplers_RRESP;
  assign S12_AXI_rvalid = axi_mem_intercon_to_s12_couplers_RVALID;
  assign S13_ACLK_1 = S13_ACLK;
  assign S13_ARESETN_1 = S13_ARESETN[0];
  assign S13_AXI_awready = axi_mem_intercon_to_s13_couplers_AWREADY;
  assign S13_AXI_bresp[1:0] = axi_mem_intercon_to_s13_couplers_BRESP;
  assign S13_AXI_bvalid = axi_mem_intercon_to_s13_couplers_BVALID;
  assign S13_AXI_wready = axi_mem_intercon_to_s13_couplers_WREADY;
  assign axi_mem_intercon_ACLK_net = ACLK;
  assign axi_mem_intercon_ARESETN_net = ARESETN[0];
  assign axi_mem_intercon_to_s00_couplers_ARADDR = S00_AXI_araddr[31:0];
  assign axi_mem_intercon_to_s00_couplers_ARBURST = S00_AXI_arburst[1:0];
  assign axi_mem_intercon_to_s00_couplers_ARCACHE = S00_AXI_arcache[3:0];
  assign axi_mem_intercon_to_s00_couplers_ARLEN = S00_AXI_arlen[7:0];
  assign axi_mem_intercon_to_s00_couplers_ARLOCK = S00_AXI_arlock[0];
  assign axi_mem_intercon_to_s00_couplers_ARPROT = S00_AXI_arprot[2:0];
  assign axi_mem_intercon_to_s00_couplers_ARQOS = S00_AXI_arqos[3:0];
  assign axi_mem_intercon_to_s00_couplers_ARSIZE = S00_AXI_arsize[2:0];
  assign axi_mem_intercon_to_s00_couplers_ARVALID = S00_AXI_arvalid;
  assign axi_mem_intercon_to_s00_couplers_AWADDR = S00_AXI_awaddr[31:0];
  assign axi_mem_intercon_to_s00_couplers_AWBURST = S00_AXI_awburst[1:0];
  assign axi_mem_intercon_to_s00_couplers_AWCACHE = S00_AXI_awcache[3:0];
  assign axi_mem_intercon_to_s00_couplers_AWLEN = S00_AXI_awlen[7:0];
  assign axi_mem_intercon_to_s00_couplers_AWLOCK = S00_AXI_awlock[0];
  assign axi_mem_intercon_to_s00_couplers_AWPROT = S00_AXI_awprot[2:0];
  assign axi_mem_intercon_to_s00_couplers_AWQOS = S00_AXI_awqos[3:0];
  assign axi_mem_intercon_to_s00_couplers_AWSIZE = S00_AXI_awsize[2:0];
  assign axi_mem_intercon_to_s00_couplers_AWVALID = S00_AXI_awvalid;
  assign axi_mem_intercon_to_s00_couplers_BREADY = S00_AXI_bready;
  assign axi_mem_intercon_to_s00_couplers_RREADY = S00_AXI_rready;
  assign axi_mem_intercon_to_s00_couplers_WDATA = S00_AXI_wdata[31:0];
  assign axi_mem_intercon_to_s00_couplers_WLAST = S00_AXI_wlast;
  assign axi_mem_intercon_to_s00_couplers_WSTRB = S00_AXI_wstrb[3:0];
  assign axi_mem_intercon_to_s00_couplers_WVALID = S00_AXI_wvalid;
  assign axi_mem_intercon_to_s01_couplers_ARADDR = S01_AXI_araddr[31:0];
  assign axi_mem_intercon_to_s01_couplers_ARBURST = S01_AXI_arburst[1:0];
  assign axi_mem_intercon_to_s01_couplers_ARCACHE = S01_AXI_arcache[3:0];
  assign axi_mem_intercon_to_s01_couplers_ARLEN = S01_AXI_arlen[7:0];
  assign axi_mem_intercon_to_s01_couplers_ARLOCK = S01_AXI_arlock[0];
  assign axi_mem_intercon_to_s01_couplers_ARPROT = S01_AXI_arprot[2:0];
  assign axi_mem_intercon_to_s01_couplers_ARQOS = S01_AXI_arqos[3:0];
  assign axi_mem_intercon_to_s01_couplers_ARSIZE = S01_AXI_arsize[2:0];
  assign axi_mem_intercon_to_s01_couplers_ARVALID = S01_AXI_arvalid;
  assign axi_mem_intercon_to_s01_couplers_RREADY = S01_AXI_rready;
  assign axi_mem_intercon_to_s02_couplers_ARADDR = S02_AXI_araddr[31:0];
  assign axi_mem_intercon_to_s02_couplers_ARBURST = S02_AXI_arburst[1:0];
  assign axi_mem_intercon_to_s02_couplers_ARCACHE = S02_AXI_arcache[3:0];
  assign axi_mem_intercon_to_s02_couplers_ARLEN = S02_AXI_arlen[7:0];
  assign axi_mem_intercon_to_s02_couplers_ARPROT = S02_AXI_arprot[2:0];
  assign axi_mem_intercon_to_s02_couplers_ARSIZE = S02_AXI_arsize[2:0];
  assign axi_mem_intercon_to_s02_couplers_ARVALID = S02_AXI_arvalid;
  assign axi_mem_intercon_to_s02_couplers_AWADDR = S02_AXI_awaddr[31:0];
  assign axi_mem_intercon_to_s02_couplers_AWBURST = S02_AXI_awburst[1:0];
  assign axi_mem_intercon_to_s02_couplers_AWCACHE = S02_AXI_awcache[3:0];
  assign axi_mem_intercon_to_s02_couplers_AWLEN = S02_AXI_awlen[7:0];
  assign axi_mem_intercon_to_s02_couplers_AWPROT = S02_AXI_awprot[2:0];
  assign axi_mem_intercon_to_s02_couplers_AWSIZE = S02_AXI_awsize[2:0];
  assign axi_mem_intercon_to_s02_couplers_AWVALID = S02_AXI_awvalid;
  assign axi_mem_intercon_to_s02_couplers_BREADY = S02_AXI_bready;
  assign axi_mem_intercon_to_s02_couplers_RREADY = S02_AXI_rready;
  assign axi_mem_intercon_to_s02_couplers_WDATA = S02_AXI_wdata[31:0];
  assign axi_mem_intercon_to_s02_couplers_WLAST = S02_AXI_wlast;
  assign axi_mem_intercon_to_s02_couplers_WSTRB = S02_AXI_wstrb[3:0];
  assign axi_mem_intercon_to_s02_couplers_WVALID = S02_AXI_wvalid;
  assign axi_mem_intercon_to_s03_couplers_ARADDR = S03_AXI_araddr[31:0];
  assign axi_mem_intercon_to_s03_couplers_ARBURST = S03_AXI_arburst[1:0];
  assign axi_mem_intercon_to_s03_couplers_ARCACHE = S03_AXI_arcache[3:0];
  assign axi_mem_intercon_to_s03_couplers_ARLEN = S03_AXI_arlen[7:0];
  assign axi_mem_intercon_to_s03_couplers_ARPROT = S03_AXI_arprot[2:0];
  assign axi_mem_intercon_to_s03_couplers_ARSIZE = S03_AXI_arsize[2:0];
  assign axi_mem_intercon_to_s03_couplers_ARVALID = S03_AXI_arvalid;
  assign axi_mem_intercon_to_s03_couplers_RREADY = S03_AXI_rready;
  assign axi_mem_intercon_to_s04_couplers_AWADDR = S04_AXI_awaddr[31:0];
  assign axi_mem_intercon_to_s04_couplers_AWBURST = S04_AXI_awburst[1:0];
  assign axi_mem_intercon_to_s04_couplers_AWCACHE = S04_AXI_awcache[3:0];
  assign axi_mem_intercon_to_s04_couplers_AWLEN = S04_AXI_awlen[7:0];
  assign axi_mem_intercon_to_s04_couplers_AWPROT = S04_AXI_awprot[2:0];
  assign axi_mem_intercon_to_s04_couplers_AWSIZE = S04_AXI_awsize[2:0];
  assign axi_mem_intercon_to_s04_couplers_AWVALID = S04_AXI_awvalid;
  assign axi_mem_intercon_to_s04_couplers_BREADY = S04_AXI_bready;
  assign axi_mem_intercon_to_s04_couplers_WDATA = S04_AXI_wdata[31:0];
  assign axi_mem_intercon_to_s04_couplers_WLAST = S04_AXI_wlast;
  assign axi_mem_intercon_to_s04_couplers_WSTRB = S04_AXI_wstrb[3:0];
  assign axi_mem_intercon_to_s04_couplers_WVALID = S04_AXI_wvalid;
  assign axi_mem_intercon_to_s05_couplers_ARADDR = S05_AXI_araddr[31:0];
  assign axi_mem_intercon_to_s05_couplers_ARBURST = S05_AXI_arburst[1:0];
  assign axi_mem_intercon_to_s05_couplers_ARCACHE = S05_AXI_arcache[3:0];
  assign axi_mem_intercon_to_s05_couplers_ARLEN = S05_AXI_arlen[7:0];
  assign axi_mem_intercon_to_s05_couplers_ARPROT = S05_AXI_arprot[2:0];
  assign axi_mem_intercon_to_s05_couplers_ARSIZE = S05_AXI_arsize[2:0];
  assign axi_mem_intercon_to_s05_couplers_ARVALID = S05_AXI_arvalid;
  assign axi_mem_intercon_to_s05_couplers_AWADDR = S05_AXI_awaddr[31:0];
  assign axi_mem_intercon_to_s05_couplers_AWBURST = S05_AXI_awburst[1:0];
  assign axi_mem_intercon_to_s05_couplers_AWCACHE = S05_AXI_awcache[3:0];
  assign axi_mem_intercon_to_s05_couplers_AWLEN = S05_AXI_awlen[7:0];
  assign axi_mem_intercon_to_s05_couplers_AWPROT = S05_AXI_awprot[2:0];
  assign axi_mem_intercon_to_s05_couplers_AWSIZE = S05_AXI_awsize[2:0];
  assign axi_mem_intercon_to_s05_couplers_AWVALID = S05_AXI_awvalid;
  assign axi_mem_intercon_to_s05_couplers_BREADY = S05_AXI_bready;
  assign axi_mem_intercon_to_s05_couplers_RREADY = S05_AXI_rready;
  assign axi_mem_intercon_to_s05_couplers_WDATA = S05_AXI_wdata[31:0];
  assign axi_mem_intercon_to_s05_couplers_WLAST = S05_AXI_wlast;
  assign axi_mem_intercon_to_s05_couplers_WSTRB = S05_AXI_wstrb[3:0];
  assign axi_mem_intercon_to_s05_couplers_WVALID = S05_AXI_wvalid;
  assign axi_mem_intercon_to_s06_couplers_ARADDR = S06_AXI_araddr[31:0];
  assign axi_mem_intercon_to_s06_couplers_ARBURST = S06_AXI_arburst[1:0];
  assign axi_mem_intercon_to_s06_couplers_ARCACHE = S06_AXI_arcache[3:0];
  assign axi_mem_intercon_to_s06_couplers_ARLEN = S06_AXI_arlen[7:0];
  assign axi_mem_intercon_to_s06_couplers_ARPROT = S06_AXI_arprot[2:0];
  assign axi_mem_intercon_to_s06_couplers_ARSIZE = S06_AXI_arsize[2:0];
  assign axi_mem_intercon_to_s06_couplers_ARVALID = S06_AXI_arvalid;
  assign axi_mem_intercon_to_s06_couplers_RREADY = S06_AXI_rready;
  assign axi_mem_intercon_to_s07_couplers_AWADDR = S07_AXI_awaddr[31:0];
  assign axi_mem_intercon_to_s07_couplers_AWBURST = S07_AXI_awburst[1:0];
  assign axi_mem_intercon_to_s07_couplers_AWCACHE = S07_AXI_awcache[3:0];
  assign axi_mem_intercon_to_s07_couplers_AWLEN = S07_AXI_awlen[7:0];
  assign axi_mem_intercon_to_s07_couplers_AWPROT = S07_AXI_awprot[2:0];
  assign axi_mem_intercon_to_s07_couplers_AWSIZE = S07_AXI_awsize[2:0];
  assign axi_mem_intercon_to_s07_couplers_AWVALID = S07_AXI_awvalid;
  assign axi_mem_intercon_to_s07_couplers_BREADY = S07_AXI_bready;
  assign axi_mem_intercon_to_s07_couplers_WDATA = S07_AXI_wdata[31:0];
  assign axi_mem_intercon_to_s07_couplers_WLAST = S07_AXI_wlast;
  assign axi_mem_intercon_to_s07_couplers_WSTRB = S07_AXI_wstrb[3:0];
  assign axi_mem_intercon_to_s07_couplers_WVALID = S07_AXI_wvalid;
  assign axi_mem_intercon_to_s08_couplers_ARADDR = S08_AXI_araddr[31:0];
  assign axi_mem_intercon_to_s08_couplers_ARBURST = S08_AXI_arburst[1:0];
  assign axi_mem_intercon_to_s08_couplers_ARCACHE = S08_AXI_arcache[3:0];
  assign axi_mem_intercon_to_s08_couplers_ARLEN = S08_AXI_arlen[7:0];
  assign axi_mem_intercon_to_s08_couplers_ARPROT = S08_AXI_arprot[2:0];
  assign axi_mem_intercon_to_s08_couplers_ARSIZE = S08_AXI_arsize[2:0];
  assign axi_mem_intercon_to_s08_couplers_ARVALID = S08_AXI_arvalid;
  assign axi_mem_intercon_to_s08_couplers_AWADDR = S08_AXI_awaddr[31:0];
  assign axi_mem_intercon_to_s08_couplers_AWBURST = S08_AXI_awburst[1:0];
  assign axi_mem_intercon_to_s08_couplers_AWCACHE = S08_AXI_awcache[3:0];
  assign axi_mem_intercon_to_s08_couplers_AWLEN = S08_AXI_awlen[7:0];
  assign axi_mem_intercon_to_s08_couplers_AWPROT = S08_AXI_awprot[2:0];
  assign axi_mem_intercon_to_s08_couplers_AWSIZE = S08_AXI_awsize[2:0];
  assign axi_mem_intercon_to_s08_couplers_AWVALID = S08_AXI_awvalid;
  assign axi_mem_intercon_to_s08_couplers_BREADY = S08_AXI_bready;
  assign axi_mem_intercon_to_s08_couplers_RREADY = S08_AXI_rready;
  assign axi_mem_intercon_to_s08_couplers_WDATA = S08_AXI_wdata[31:0];
  assign axi_mem_intercon_to_s08_couplers_WLAST = S08_AXI_wlast;
  assign axi_mem_intercon_to_s08_couplers_WSTRB = S08_AXI_wstrb[3:0];
  assign axi_mem_intercon_to_s08_couplers_WVALID = S08_AXI_wvalid;
  assign axi_mem_intercon_to_s09_couplers_ARADDR = S09_AXI_araddr[31:0];
  assign axi_mem_intercon_to_s09_couplers_ARBURST = S09_AXI_arburst[1:0];
  assign axi_mem_intercon_to_s09_couplers_ARCACHE = S09_AXI_arcache[3:0];
  assign axi_mem_intercon_to_s09_couplers_ARLEN = S09_AXI_arlen[7:0];
  assign axi_mem_intercon_to_s09_couplers_ARPROT = S09_AXI_arprot[2:0];
  assign axi_mem_intercon_to_s09_couplers_ARSIZE = S09_AXI_arsize[2:0];
  assign axi_mem_intercon_to_s09_couplers_ARVALID = S09_AXI_arvalid;
  assign axi_mem_intercon_to_s09_couplers_RREADY = S09_AXI_rready;
  assign axi_mem_intercon_to_s10_couplers_AWADDR = S10_AXI_awaddr[31:0];
  assign axi_mem_intercon_to_s10_couplers_AWBURST = S10_AXI_awburst[1:0];
  assign axi_mem_intercon_to_s10_couplers_AWCACHE = S10_AXI_awcache[3:0];
  assign axi_mem_intercon_to_s10_couplers_AWLEN = S10_AXI_awlen[7:0];
  assign axi_mem_intercon_to_s10_couplers_AWPROT = S10_AXI_awprot[2:0];
  assign axi_mem_intercon_to_s10_couplers_AWSIZE = S10_AXI_awsize[2:0];
  assign axi_mem_intercon_to_s10_couplers_AWVALID = S10_AXI_awvalid;
  assign axi_mem_intercon_to_s10_couplers_BREADY = S10_AXI_bready;
  assign axi_mem_intercon_to_s10_couplers_WDATA = S10_AXI_wdata[31:0];
  assign axi_mem_intercon_to_s10_couplers_WLAST = S10_AXI_wlast;
  assign axi_mem_intercon_to_s10_couplers_WSTRB = S10_AXI_wstrb[3:0];
  assign axi_mem_intercon_to_s10_couplers_WVALID = S10_AXI_wvalid;
  assign axi_mem_intercon_to_s11_couplers_ARADDR = S11_AXI_araddr[31:0];
  assign axi_mem_intercon_to_s11_couplers_ARBURST = S11_AXI_arburst[1:0];
  assign axi_mem_intercon_to_s11_couplers_ARCACHE = S11_AXI_arcache[3:0];
  assign axi_mem_intercon_to_s11_couplers_ARLEN = S11_AXI_arlen[7:0];
  assign axi_mem_intercon_to_s11_couplers_ARPROT = S11_AXI_arprot[2:0];
  assign axi_mem_intercon_to_s11_couplers_ARSIZE = S11_AXI_arsize[2:0];
  assign axi_mem_intercon_to_s11_couplers_ARVALID = S11_AXI_arvalid;
  assign axi_mem_intercon_to_s11_couplers_AWADDR = S11_AXI_awaddr[31:0];
  assign axi_mem_intercon_to_s11_couplers_AWBURST = S11_AXI_awburst[1:0];
  assign axi_mem_intercon_to_s11_couplers_AWCACHE = S11_AXI_awcache[3:0];
  assign axi_mem_intercon_to_s11_couplers_AWLEN = S11_AXI_awlen[7:0];
  assign axi_mem_intercon_to_s11_couplers_AWPROT = S11_AXI_awprot[2:0];
  assign axi_mem_intercon_to_s11_couplers_AWSIZE = S11_AXI_awsize[2:0];
  assign axi_mem_intercon_to_s11_couplers_AWVALID = S11_AXI_awvalid;
  assign axi_mem_intercon_to_s11_couplers_BREADY = S11_AXI_bready;
  assign axi_mem_intercon_to_s11_couplers_RREADY = S11_AXI_rready;
  assign axi_mem_intercon_to_s11_couplers_WDATA = S11_AXI_wdata[31:0];
  assign axi_mem_intercon_to_s11_couplers_WLAST = S11_AXI_wlast;
  assign axi_mem_intercon_to_s11_couplers_WSTRB = S11_AXI_wstrb[3:0];
  assign axi_mem_intercon_to_s11_couplers_WVALID = S11_AXI_wvalid;
  assign axi_mem_intercon_to_s12_couplers_ARADDR = S12_AXI_araddr[31:0];
  assign axi_mem_intercon_to_s12_couplers_ARBURST = S12_AXI_arburst[1:0];
  assign axi_mem_intercon_to_s12_couplers_ARCACHE = S12_AXI_arcache[3:0];
  assign axi_mem_intercon_to_s12_couplers_ARLEN = S12_AXI_arlen[7:0];
  assign axi_mem_intercon_to_s12_couplers_ARPROT = S12_AXI_arprot[2:0];
  assign axi_mem_intercon_to_s12_couplers_ARSIZE = S12_AXI_arsize[2:0];
  assign axi_mem_intercon_to_s12_couplers_ARVALID = S12_AXI_arvalid;
  assign axi_mem_intercon_to_s12_couplers_RREADY = S12_AXI_rready;
  assign axi_mem_intercon_to_s13_couplers_AWADDR = S13_AXI_awaddr[31:0];
  assign axi_mem_intercon_to_s13_couplers_AWBURST = S13_AXI_awburst[1:0];
  assign axi_mem_intercon_to_s13_couplers_AWCACHE = S13_AXI_awcache[3:0];
  assign axi_mem_intercon_to_s13_couplers_AWLEN = S13_AXI_awlen[7:0];
  assign axi_mem_intercon_to_s13_couplers_AWPROT = S13_AXI_awprot[2:0];
  assign axi_mem_intercon_to_s13_couplers_AWSIZE = S13_AXI_awsize[2:0];
  assign axi_mem_intercon_to_s13_couplers_AWVALID = S13_AXI_awvalid;
  assign axi_mem_intercon_to_s13_couplers_BREADY = S13_AXI_bready;
  assign axi_mem_intercon_to_s13_couplers_WDATA = S13_AXI_wdata[31:0];
  assign axi_mem_intercon_to_s13_couplers_WLAST = S13_AXI_wlast;
  assign axi_mem_intercon_to_s13_couplers_WSTRB = S13_AXI_wstrb[3:0];
  assign axi_mem_intercon_to_s13_couplers_WVALID = S13_AXI_wvalid;
  assign m00_couplers_to_axi_mem_intercon_ARREADY = M00_AXI_arready[0];
  assign m00_couplers_to_axi_mem_intercon_AWREADY = M00_AXI_awready[0];
  assign m00_couplers_to_axi_mem_intercon_BID = M00_AXI_bid[3:0];
  assign m00_couplers_to_axi_mem_intercon_BRESP = M00_AXI_bresp[1:0];
  assign m00_couplers_to_axi_mem_intercon_BVALID = M00_AXI_bvalid[0];
  assign m00_couplers_to_axi_mem_intercon_RDATA = M00_AXI_rdata[511:0];
  assign m00_couplers_to_axi_mem_intercon_RID = M00_AXI_rid[3:0];
  assign m00_couplers_to_axi_mem_intercon_RLAST = M00_AXI_rlast[0];
  assign m00_couplers_to_axi_mem_intercon_RRESP = M00_AXI_rresp[1:0];
  assign m00_couplers_to_axi_mem_intercon_RVALID = M00_AXI_rvalid[0];
  assign m00_couplers_to_axi_mem_intercon_WREADY = M00_AXI_wready[0];
  m00_couplers_imp_1R706YB m00_couplers
       (.M_ACLK(M00_ACLK_1),
        .M_ARESETN(M00_ARESETN_1),
        .M_AXI_araddr(m00_couplers_to_axi_mem_intercon_ARADDR),
        .M_AXI_arburst(m00_couplers_to_axi_mem_intercon_ARBURST),
        .M_AXI_arcache(m00_couplers_to_axi_mem_intercon_ARCACHE),
        .M_AXI_arid(m00_couplers_to_axi_mem_intercon_ARID),
        .M_AXI_arlen(m00_couplers_to_axi_mem_intercon_ARLEN),
        .M_AXI_arlock(m00_couplers_to_axi_mem_intercon_ARLOCK),
        .M_AXI_arprot(m00_couplers_to_axi_mem_intercon_ARPROT),
        .M_AXI_arqos(m00_couplers_to_axi_mem_intercon_ARQOS),
        .M_AXI_arready(m00_couplers_to_axi_mem_intercon_ARREADY),
        .M_AXI_arsize(m00_couplers_to_axi_mem_intercon_ARSIZE),
        .M_AXI_arvalid(m00_couplers_to_axi_mem_intercon_ARVALID),
        .M_AXI_awaddr(m00_couplers_to_axi_mem_intercon_AWADDR),
        .M_AXI_awburst(m00_couplers_to_axi_mem_intercon_AWBURST),
        .M_AXI_awcache(m00_couplers_to_axi_mem_intercon_AWCACHE),
        .M_AXI_awid(m00_couplers_to_axi_mem_intercon_AWID),
        .M_AXI_awlen(m00_couplers_to_axi_mem_intercon_AWLEN),
        .M_AXI_awlock(m00_couplers_to_axi_mem_intercon_AWLOCK),
        .M_AXI_awprot(m00_couplers_to_axi_mem_intercon_AWPROT),
        .M_AXI_awqos(m00_couplers_to_axi_mem_intercon_AWQOS),
        .M_AXI_awready(m00_couplers_to_axi_mem_intercon_AWREADY),
        .M_AXI_awsize(m00_couplers_to_axi_mem_intercon_AWSIZE),
        .M_AXI_awvalid(m00_couplers_to_axi_mem_intercon_AWVALID),
        .M_AXI_bid(m00_couplers_to_axi_mem_intercon_BID),
        .M_AXI_bready(m00_couplers_to_axi_mem_intercon_BREADY),
        .M_AXI_bresp(m00_couplers_to_axi_mem_intercon_BRESP),
        .M_AXI_bvalid(m00_couplers_to_axi_mem_intercon_BVALID),
        .M_AXI_rdata(m00_couplers_to_axi_mem_intercon_RDATA),
        .M_AXI_rid(m00_couplers_to_axi_mem_intercon_RID),
        .M_AXI_rlast(m00_couplers_to_axi_mem_intercon_RLAST),
        .M_AXI_rready(m00_couplers_to_axi_mem_intercon_RREADY),
        .M_AXI_rresp(m00_couplers_to_axi_mem_intercon_RRESP),
        .M_AXI_rvalid(m00_couplers_to_axi_mem_intercon_RVALID),
        .M_AXI_wdata(m00_couplers_to_axi_mem_intercon_WDATA),
        .M_AXI_wlast(m00_couplers_to_axi_mem_intercon_WLAST),
        .M_AXI_wready(m00_couplers_to_axi_mem_intercon_WREADY),
        .M_AXI_wstrb(m00_couplers_to_axi_mem_intercon_WSTRB),
        .M_AXI_wvalid(m00_couplers_to_axi_mem_intercon_WVALID),
        .S_ACLK(axi_mem_intercon_ACLK_net),
        .S_ARESETN(axi_mem_intercon_ARESETN_net),
        .S_AXI_araddr(xbar_to_m00_couplers_ARADDR),
        .S_AXI_arburst(xbar_to_m00_couplers_ARBURST),
        .S_AXI_arcache(xbar_to_m00_couplers_ARCACHE),
        .S_AXI_arid(xbar_to_m00_couplers_ARID),
        .S_AXI_arlen(xbar_to_m00_couplers_ARLEN),
        .S_AXI_arlock(xbar_to_m00_couplers_ARLOCK),
        .S_AXI_arprot(xbar_to_m00_couplers_ARPROT),
        .S_AXI_arqos(xbar_to_m00_couplers_ARQOS),
        .S_AXI_arready(xbar_to_m00_couplers_ARREADY),
        .S_AXI_arsize(xbar_to_m00_couplers_ARSIZE),
        .S_AXI_arvalid(xbar_to_m00_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m00_couplers_AWADDR),
        .S_AXI_awburst(xbar_to_m00_couplers_AWBURST),
        .S_AXI_awcache(xbar_to_m00_couplers_AWCACHE),
        .S_AXI_awid(xbar_to_m00_couplers_AWID),
        .S_AXI_awlen(xbar_to_m00_couplers_AWLEN),
        .S_AXI_awlock(xbar_to_m00_couplers_AWLOCK),
        .S_AXI_awprot(xbar_to_m00_couplers_AWPROT),
        .S_AXI_awqos(xbar_to_m00_couplers_AWQOS),
        .S_AXI_awready(xbar_to_m00_couplers_AWREADY),
        .S_AXI_awsize(xbar_to_m00_couplers_AWSIZE),
        .S_AXI_awvalid(xbar_to_m00_couplers_AWVALID),
        .S_AXI_bid(xbar_to_m00_couplers_BID),
        .S_AXI_bready(xbar_to_m00_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m00_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m00_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m00_couplers_RDATA),
        .S_AXI_rid(xbar_to_m00_couplers_RID),
        .S_AXI_rlast(xbar_to_m00_couplers_RLAST),
        .S_AXI_rready(xbar_to_m00_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m00_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m00_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m00_couplers_WDATA),
        .S_AXI_wlast(xbar_to_m00_couplers_WLAST),
        .S_AXI_wready(xbar_to_m00_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m00_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m00_couplers_WVALID));
  s00_couplers_imp_7HNO1D s00_couplers
       (.M_ACLK(axi_mem_intercon_ACLK_net),
        .M_ARESETN(axi_mem_intercon_ARESETN_net),
        .M_AXI_araddr(s00_couplers_to_xbar_ARADDR),
        .M_AXI_arburst(s00_couplers_to_xbar_ARBURST),
        .M_AXI_arcache(s00_couplers_to_xbar_ARCACHE),
        .M_AXI_arlen(s00_couplers_to_xbar_ARLEN),
        .M_AXI_arlock(s00_couplers_to_xbar_ARLOCK),
        .M_AXI_arprot(s00_couplers_to_xbar_ARPROT),
        .M_AXI_arqos(s00_couplers_to_xbar_ARQOS),
        .M_AXI_arready(s00_couplers_to_xbar_ARREADY),
        .M_AXI_arsize(s00_couplers_to_xbar_ARSIZE),
        .M_AXI_arvalid(s00_couplers_to_xbar_ARVALID),
        .M_AXI_awaddr(s00_couplers_to_xbar_AWADDR),
        .M_AXI_awburst(s00_couplers_to_xbar_AWBURST),
        .M_AXI_awcache(s00_couplers_to_xbar_AWCACHE),
        .M_AXI_awlen(s00_couplers_to_xbar_AWLEN),
        .M_AXI_awlock(s00_couplers_to_xbar_AWLOCK),
        .M_AXI_awprot(s00_couplers_to_xbar_AWPROT),
        .M_AXI_awqos(s00_couplers_to_xbar_AWQOS),
        .M_AXI_awready(s00_couplers_to_xbar_AWREADY),
        .M_AXI_awsize(s00_couplers_to_xbar_AWSIZE),
        .M_AXI_awvalid(s00_couplers_to_xbar_AWVALID),
        .M_AXI_bready(s00_couplers_to_xbar_BREADY),
        .M_AXI_bresp(s00_couplers_to_xbar_BRESP),
        .M_AXI_bvalid(s00_couplers_to_xbar_BVALID),
        .M_AXI_rdata(s00_couplers_to_xbar_RDATA),
        .M_AXI_rlast(s00_couplers_to_xbar_RLAST),
        .M_AXI_rready(s00_couplers_to_xbar_RREADY),
        .M_AXI_rresp(s00_couplers_to_xbar_RRESP),
        .M_AXI_rvalid(s00_couplers_to_xbar_RVALID),
        .M_AXI_wdata(s00_couplers_to_xbar_WDATA),
        .M_AXI_wlast(s00_couplers_to_xbar_WLAST),
        .M_AXI_wready(s00_couplers_to_xbar_WREADY),
        .M_AXI_wstrb(s00_couplers_to_xbar_WSTRB),
        .M_AXI_wvalid(s00_couplers_to_xbar_WVALID),
        .S_ACLK(S00_ACLK_1),
        .S_ARESETN(S00_ARESETN_1),
        .S_AXI_araddr(axi_mem_intercon_to_s00_couplers_ARADDR),
        .S_AXI_arburst(axi_mem_intercon_to_s00_couplers_ARBURST),
        .S_AXI_arcache(axi_mem_intercon_to_s00_couplers_ARCACHE),
        .S_AXI_arlen(axi_mem_intercon_to_s00_couplers_ARLEN),
        .S_AXI_arlock(axi_mem_intercon_to_s00_couplers_ARLOCK),
        .S_AXI_arprot(axi_mem_intercon_to_s00_couplers_ARPROT),
        .S_AXI_arqos(axi_mem_intercon_to_s00_couplers_ARQOS),
        .S_AXI_arready(axi_mem_intercon_to_s00_couplers_ARREADY),
        .S_AXI_arsize(axi_mem_intercon_to_s00_couplers_ARSIZE),
        .S_AXI_arvalid(axi_mem_intercon_to_s00_couplers_ARVALID),
        .S_AXI_awaddr(axi_mem_intercon_to_s00_couplers_AWADDR),
        .S_AXI_awburst(axi_mem_intercon_to_s00_couplers_AWBURST),
        .S_AXI_awcache(axi_mem_intercon_to_s00_couplers_AWCACHE),
        .S_AXI_awlen(axi_mem_intercon_to_s00_couplers_AWLEN),
        .S_AXI_awlock(axi_mem_intercon_to_s00_couplers_AWLOCK),
        .S_AXI_awprot(axi_mem_intercon_to_s00_couplers_AWPROT),
        .S_AXI_awqos(axi_mem_intercon_to_s00_couplers_AWQOS),
        .S_AXI_awready(axi_mem_intercon_to_s00_couplers_AWREADY),
        .S_AXI_awsize(axi_mem_intercon_to_s00_couplers_AWSIZE),
        .S_AXI_awvalid(axi_mem_intercon_to_s00_couplers_AWVALID),
        .S_AXI_bready(axi_mem_intercon_to_s00_couplers_BREADY),
        .S_AXI_bresp(axi_mem_intercon_to_s00_couplers_BRESP),
        .S_AXI_bvalid(axi_mem_intercon_to_s00_couplers_BVALID),
        .S_AXI_rdata(axi_mem_intercon_to_s00_couplers_RDATA),
        .S_AXI_rlast(axi_mem_intercon_to_s00_couplers_RLAST),
        .S_AXI_rready(axi_mem_intercon_to_s00_couplers_RREADY),
        .S_AXI_rresp(axi_mem_intercon_to_s00_couplers_RRESP),
        .S_AXI_rvalid(axi_mem_intercon_to_s00_couplers_RVALID),
        .S_AXI_wdata(axi_mem_intercon_to_s00_couplers_WDATA),
        .S_AXI_wlast(axi_mem_intercon_to_s00_couplers_WLAST),
        .S_AXI_wready(axi_mem_intercon_to_s00_couplers_WREADY),
        .S_AXI_wstrb(axi_mem_intercon_to_s00_couplers_WSTRB),
        .S_AXI_wvalid(axi_mem_intercon_to_s00_couplers_WVALID));
  s01_couplers_imp_1W60HW0 s01_couplers
       (.M_ACLK(axi_mem_intercon_ACLK_net),
        .M_ARESETN(axi_mem_intercon_ARESETN_net),
        .M_AXI_araddr(s01_couplers_to_xbar_ARADDR),
        .M_AXI_arburst(s01_couplers_to_xbar_ARBURST),
        .M_AXI_arcache(s01_couplers_to_xbar_ARCACHE),
        .M_AXI_arlen(s01_couplers_to_xbar_ARLEN),
        .M_AXI_arlock(s01_couplers_to_xbar_ARLOCK),
        .M_AXI_arprot(s01_couplers_to_xbar_ARPROT),
        .M_AXI_arqos(s01_couplers_to_xbar_ARQOS),
        .M_AXI_arready(s01_couplers_to_xbar_ARREADY),
        .M_AXI_arsize(s01_couplers_to_xbar_ARSIZE),
        .M_AXI_arvalid(s01_couplers_to_xbar_ARVALID),
        .M_AXI_rdata(s01_couplers_to_xbar_RDATA),
        .M_AXI_rlast(s01_couplers_to_xbar_RLAST),
        .M_AXI_rready(s01_couplers_to_xbar_RREADY),
        .M_AXI_rresp(s01_couplers_to_xbar_RRESP),
        .M_AXI_rvalid(s01_couplers_to_xbar_RVALID),
        .S_ACLK(S01_ACLK_1),
        .S_ARESETN(S01_ARESETN_1),
        .S_AXI_araddr(axi_mem_intercon_to_s01_couplers_ARADDR),
        .S_AXI_arburst(axi_mem_intercon_to_s01_couplers_ARBURST),
        .S_AXI_arcache(axi_mem_intercon_to_s01_couplers_ARCACHE),
        .S_AXI_arlen(axi_mem_intercon_to_s01_couplers_ARLEN),
        .S_AXI_arlock(axi_mem_intercon_to_s01_couplers_ARLOCK),
        .S_AXI_arprot(axi_mem_intercon_to_s01_couplers_ARPROT),
        .S_AXI_arqos(axi_mem_intercon_to_s01_couplers_ARQOS),
        .S_AXI_arready(axi_mem_intercon_to_s01_couplers_ARREADY),
        .S_AXI_arsize(axi_mem_intercon_to_s01_couplers_ARSIZE),
        .S_AXI_arvalid(axi_mem_intercon_to_s01_couplers_ARVALID),
        .S_AXI_rdata(axi_mem_intercon_to_s01_couplers_RDATA),
        .S_AXI_rlast(axi_mem_intercon_to_s01_couplers_RLAST),
        .S_AXI_rready(axi_mem_intercon_to_s01_couplers_RREADY),
        .S_AXI_rresp(axi_mem_intercon_to_s01_couplers_RRESP),
        .S_AXI_rvalid(axi_mem_intercon_to_s01_couplers_RVALID));
  s02_couplers_imp_8NCF02 s02_couplers
       (.M_ACLK(axi_mem_intercon_ACLK_net),
        .M_ARESETN(axi_mem_intercon_ARESETN_net),
        .M_AXI_araddr(s02_couplers_to_xbar_ARADDR),
        .M_AXI_arburst(s02_couplers_to_xbar_ARBURST),
        .M_AXI_arcache(s02_couplers_to_xbar_ARCACHE),
        .M_AXI_arlen(s02_couplers_to_xbar_ARLEN),
        .M_AXI_arlock(s02_couplers_to_xbar_ARLOCK),
        .M_AXI_arprot(s02_couplers_to_xbar_ARPROT),
        .M_AXI_arqos(s02_couplers_to_xbar_ARQOS),
        .M_AXI_arready(s02_couplers_to_xbar_ARREADY),
        .M_AXI_arsize(s02_couplers_to_xbar_ARSIZE),
        .M_AXI_arvalid(s02_couplers_to_xbar_ARVALID),
        .M_AXI_awaddr(s02_couplers_to_xbar_AWADDR),
        .M_AXI_awburst(s02_couplers_to_xbar_AWBURST),
        .M_AXI_awcache(s02_couplers_to_xbar_AWCACHE),
        .M_AXI_awlen(s02_couplers_to_xbar_AWLEN),
        .M_AXI_awlock(s02_couplers_to_xbar_AWLOCK),
        .M_AXI_awprot(s02_couplers_to_xbar_AWPROT),
        .M_AXI_awqos(s02_couplers_to_xbar_AWQOS),
        .M_AXI_awready(s02_couplers_to_xbar_AWREADY),
        .M_AXI_awsize(s02_couplers_to_xbar_AWSIZE),
        .M_AXI_awvalid(s02_couplers_to_xbar_AWVALID),
        .M_AXI_bready(s02_couplers_to_xbar_BREADY),
        .M_AXI_bresp(s02_couplers_to_xbar_BRESP),
        .M_AXI_bvalid(s02_couplers_to_xbar_BVALID),
        .M_AXI_rdata(s02_couplers_to_xbar_RDATA),
        .M_AXI_rlast(s02_couplers_to_xbar_RLAST),
        .M_AXI_rready(s02_couplers_to_xbar_RREADY),
        .M_AXI_rresp(s02_couplers_to_xbar_RRESP),
        .M_AXI_rvalid(s02_couplers_to_xbar_RVALID),
        .M_AXI_wdata(s02_couplers_to_xbar_WDATA),
        .M_AXI_wlast(s02_couplers_to_xbar_WLAST),
        .M_AXI_wready(s02_couplers_to_xbar_WREADY),
        .M_AXI_wstrb(s02_couplers_to_xbar_WSTRB),
        .M_AXI_wvalid(s02_couplers_to_xbar_WVALID),
        .S_ACLK(S02_ACLK_1),
        .S_ARESETN(S02_ARESETN_1),
        .S_AXI_araddr(axi_mem_intercon_to_s02_couplers_ARADDR),
        .S_AXI_arburst(axi_mem_intercon_to_s02_couplers_ARBURST),
        .S_AXI_arcache(axi_mem_intercon_to_s02_couplers_ARCACHE),
        .S_AXI_arlen(axi_mem_intercon_to_s02_couplers_ARLEN),
        .S_AXI_arprot(axi_mem_intercon_to_s02_couplers_ARPROT),
        .S_AXI_arready(axi_mem_intercon_to_s02_couplers_ARREADY),
        .S_AXI_arsize(axi_mem_intercon_to_s02_couplers_ARSIZE),
        .S_AXI_arvalid(axi_mem_intercon_to_s02_couplers_ARVALID),
        .S_AXI_awaddr(axi_mem_intercon_to_s02_couplers_AWADDR),
        .S_AXI_awburst(axi_mem_intercon_to_s02_couplers_AWBURST),
        .S_AXI_awcache(axi_mem_intercon_to_s02_couplers_AWCACHE),
        .S_AXI_awlen(axi_mem_intercon_to_s02_couplers_AWLEN),
        .S_AXI_awprot(axi_mem_intercon_to_s02_couplers_AWPROT),
        .S_AXI_awready(axi_mem_intercon_to_s02_couplers_AWREADY),
        .S_AXI_awsize(axi_mem_intercon_to_s02_couplers_AWSIZE),
        .S_AXI_awvalid(axi_mem_intercon_to_s02_couplers_AWVALID),
        .S_AXI_bready(axi_mem_intercon_to_s02_couplers_BREADY),
        .S_AXI_bresp(axi_mem_intercon_to_s02_couplers_BRESP),
        .S_AXI_bvalid(axi_mem_intercon_to_s02_couplers_BVALID),
        .S_AXI_rdata(axi_mem_intercon_to_s02_couplers_RDATA),
        .S_AXI_rlast(axi_mem_intercon_to_s02_couplers_RLAST),
        .S_AXI_rready(axi_mem_intercon_to_s02_couplers_RREADY),
        .S_AXI_rresp(axi_mem_intercon_to_s02_couplers_RRESP),
        .S_AXI_rvalid(axi_mem_intercon_to_s02_couplers_RVALID),
        .S_AXI_wdata(axi_mem_intercon_to_s02_couplers_WDATA),
        .S_AXI_wlast(axi_mem_intercon_to_s02_couplers_WLAST),
        .S_AXI_wready(axi_mem_intercon_to_s02_couplers_WREADY),
        .S_AXI_wstrb(axi_mem_intercon_to_s02_couplers_WSTRB),
        .S_AXI_wvalid(axi_mem_intercon_to_s02_couplers_WVALID));
  s03_couplers_imp_1UQ1PUB s03_couplers
       (.M_ACLK(axi_mem_intercon_ACLK_net),
        .M_ARESETN(axi_mem_intercon_ARESETN_net),
        .M_AXI_araddr(s03_couplers_to_xbar_ARADDR),
        .M_AXI_arburst(s03_couplers_to_xbar_ARBURST),
        .M_AXI_arcache(s03_couplers_to_xbar_ARCACHE),
        .M_AXI_arlen(s03_couplers_to_xbar_ARLEN),
        .M_AXI_arlock(s03_couplers_to_xbar_ARLOCK),
        .M_AXI_arprot(s03_couplers_to_xbar_ARPROT),
        .M_AXI_arqos(s03_couplers_to_xbar_ARQOS),
        .M_AXI_arready(s03_couplers_to_xbar_ARREADY),
        .M_AXI_arsize(s03_couplers_to_xbar_ARSIZE),
        .M_AXI_arvalid(s03_couplers_to_xbar_ARVALID),
        .M_AXI_rdata(s03_couplers_to_xbar_RDATA),
        .M_AXI_rlast(s03_couplers_to_xbar_RLAST),
        .M_AXI_rready(s03_couplers_to_xbar_RREADY),
        .M_AXI_rresp(s03_couplers_to_xbar_RRESP),
        .M_AXI_rvalid(s03_couplers_to_xbar_RVALID),
        .S_ACLK(S03_ACLK_1),
        .S_ARESETN(S03_ARESETN_1),
        .S_AXI_araddr(axi_mem_intercon_to_s03_couplers_ARADDR),
        .S_AXI_arburst(axi_mem_intercon_to_s03_couplers_ARBURST),
        .S_AXI_arcache(axi_mem_intercon_to_s03_couplers_ARCACHE),
        .S_AXI_arlen(axi_mem_intercon_to_s03_couplers_ARLEN),
        .S_AXI_arprot(axi_mem_intercon_to_s03_couplers_ARPROT),
        .S_AXI_arready(axi_mem_intercon_to_s03_couplers_ARREADY),
        .S_AXI_arsize(axi_mem_intercon_to_s03_couplers_ARSIZE),
        .S_AXI_arvalid(axi_mem_intercon_to_s03_couplers_ARVALID),
        .S_AXI_rdata(axi_mem_intercon_to_s03_couplers_RDATA),
        .S_AXI_rlast(axi_mem_intercon_to_s03_couplers_RLAST),
        .S_AXI_rready(axi_mem_intercon_to_s03_couplers_RREADY),
        .S_AXI_rresp(axi_mem_intercon_to_s03_couplers_RRESP),
        .S_AXI_rvalid(axi_mem_intercon_to_s03_couplers_RVALID));
  s04_couplers_imp_4T8GAF s04_couplers
       (.M_ACLK(axi_mem_intercon_ACLK_net),
        .M_ARESETN(axi_mem_intercon_ARESETN_net),
        .M_AXI_awaddr(s04_couplers_to_xbar_AWADDR),
        .M_AXI_awburst(s04_couplers_to_xbar_AWBURST),
        .M_AXI_awcache(s04_couplers_to_xbar_AWCACHE),
        .M_AXI_awlen(s04_couplers_to_xbar_AWLEN),
        .M_AXI_awlock(s04_couplers_to_xbar_AWLOCK),
        .M_AXI_awprot(s04_couplers_to_xbar_AWPROT),
        .M_AXI_awqos(s04_couplers_to_xbar_AWQOS),
        .M_AXI_awready(s04_couplers_to_xbar_AWREADY),
        .M_AXI_awsize(s04_couplers_to_xbar_AWSIZE),
        .M_AXI_awvalid(s04_couplers_to_xbar_AWVALID),
        .M_AXI_bready(s04_couplers_to_xbar_BREADY),
        .M_AXI_bresp(s04_couplers_to_xbar_BRESP),
        .M_AXI_bvalid(s04_couplers_to_xbar_BVALID),
        .M_AXI_wdata(s04_couplers_to_xbar_WDATA),
        .M_AXI_wlast(s04_couplers_to_xbar_WLAST),
        .M_AXI_wready(s04_couplers_to_xbar_WREADY),
        .M_AXI_wstrb(s04_couplers_to_xbar_WSTRB),
        .M_AXI_wvalid(s04_couplers_to_xbar_WVALID),
        .S_ACLK(S04_ACLK_1),
        .S_ARESETN(S04_ARESETN_1),
        .S_AXI_awaddr(axi_mem_intercon_to_s04_couplers_AWADDR),
        .S_AXI_awburst(axi_mem_intercon_to_s04_couplers_AWBURST),
        .S_AXI_awcache(axi_mem_intercon_to_s04_couplers_AWCACHE),
        .S_AXI_awlen(axi_mem_intercon_to_s04_couplers_AWLEN),
        .S_AXI_awprot(axi_mem_intercon_to_s04_couplers_AWPROT),
        .S_AXI_awready(axi_mem_intercon_to_s04_couplers_AWREADY),
        .S_AXI_awsize(axi_mem_intercon_to_s04_couplers_AWSIZE),
        .S_AXI_awvalid(axi_mem_intercon_to_s04_couplers_AWVALID),
        .S_AXI_bready(axi_mem_intercon_to_s04_couplers_BREADY),
        .S_AXI_bresp(axi_mem_intercon_to_s04_couplers_BRESP),
        .S_AXI_bvalid(axi_mem_intercon_to_s04_couplers_BVALID),
        .S_AXI_wdata(axi_mem_intercon_to_s04_couplers_WDATA),
        .S_AXI_wlast(axi_mem_intercon_to_s04_couplers_WLAST),
        .S_AXI_wready(axi_mem_intercon_to_s04_couplers_WREADY),
        .S_AXI_wstrb(axi_mem_intercon_to_s04_couplers_WSTRB),
        .S_AXI_wvalid(axi_mem_intercon_to_s04_couplers_WVALID));
  s05_couplers_imp_1YHCGIE s05_couplers
       (.M_ACLK(axi_mem_intercon_ACLK_net),
        .M_ARESETN(axi_mem_intercon_ARESETN_net),
        .M_AXI_araddr(s05_couplers_to_xbar_ARADDR),
        .M_AXI_arburst(s05_couplers_to_xbar_ARBURST),
        .M_AXI_arcache(s05_couplers_to_xbar_ARCACHE),
        .M_AXI_arlen(s05_couplers_to_xbar_ARLEN),
        .M_AXI_arlock(s05_couplers_to_xbar_ARLOCK),
        .M_AXI_arprot(s05_couplers_to_xbar_ARPROT),
        .M_AXI_arqos(s05_couplers_to_xbar_ARQOS),
        .M_AXI_arready(s05_couplers_to_xbar_ARREADY),
        .M_AXI_arsize(s05_couplers_to_xbar_ARSIZE),
        .M_AXI_arvalid(s05_couplers_to_xbar_ARVALID),
        .M_AXI_awaddr(s05_couplers_to_xbar_AWADDR),
        .M_AXI_awburst(s05_couplers_to_xbar_AWBURST),
        .M_AXI_awcache(s05_couplers_to_xbar_AWCACHE),
        .M_AXI_awlen(s05_couplers_to_xbar_AWLEN),
        .M_AXI_awlock(s05_couplers_to_xbar_AWLOCK),
        .M_AXI_awprot(s05_couplers_to_xbar_AWPROT),
        .M_AXI_awqos(s05_couplers_to_xbar_AWQOS),
        .M_AXI_awready(s05_couplers_to_xbar_AWREADY),
        .M_AXI_awsize(s05_couplers_to_xbar_AWSIZE),
        .M_AXI_awvalid(s05_couplers_to_xbar_AWVALID),
        .M_AXI_bready(s05_couplers_to_xbar_BREADY),
        .M_AXI_bresp(s05_couplers_to_xbar_BRESP),
        .M_AXI_bvalid(s05_couplers_to_xbar_BVALID),
        .M_AXI_rdata(s05_couplers_to_xbar_RDATA),
        .M_AXI_rlast(s05_couplers_to_xbar_RLAST),
        .M_AXI_rready(s05_couplers_to_xbar_RREADY),
        .M_AXI_rresp(s05_couplers_to_xbar_RRESP),
        .M_AXI_rvalid(s05_couplers_to_xbar_RVALID),
        .M_AXI_wdata(s05_couplers_to_xbar_WDATA),
        .M_AXI_wlast(s05_couplers_to_xbar_WLAST),
        .M_AXI_wready(s05_couplers_to_xbar_WREADY),
        .M_AXI_wstrb(s05_couplers_to_xbar_WSTRB),
        .M_AXI_wvalid(s05_couplers_to_xbar_WVALID),
        .S_ACLK(S05_ACLK_1),
        .S_ARESETN(S05_ARESETN_1),
        .S_AXI_araddr(axi_mem_intercon_to_s05_couplers_ARADDR),
        .S_AXI_arburst(axi_mem_intercon_to_s05_couplers_ARBURST),
        .S_AXI_arcache(axi_mem_intercon_to_s05_couplers_ARCACHE),
        .S_AXI_arlen(axi_mem_intercon_to_s05_couplers_ARLEN),
        .S_AXI_arprot(axi_mem_intercon_to_s05_couplers_ARPROT),
        .S_AXI_arready(axi_mem_intercon_to_s05_couplers_ARREADY),
        .S_AXI_arsize(axi_mem_intercon_to_s05_couplers_ARSIZE),
        .S_AXI_arvalid(axi_mem_intercon_to_s05_couplers_ARVALID),
        .S_AXI_awaddr(axi_mem_intercon_to_s05_couplers_AWADDR),
        .S_AXI_awburst(axi_mem_intercon_to_s05_couplers_AWBURST),
        .S_AXI_awcache(axi_mem_intercon_to_s05_couplers_AWCACHE),
        .S_AXI_awlen(axi_mem_intercon_to_s05_couplers_AWLEN),
        .S_AXI_awprot(axi_mem_intercon_to_s05_couplers_AWPROT),
        .S_AXI_awready(axi_mem_intercon_to_s05_couplers_AWREADY),
        .S_AXI_awsize(axi_mem_intercon_to_s05_couplers_AWSIZE),
        .S_AXI_awvalid(axi_mem_intercon_to_s05_couplers_AWVALID),
        .S_AXI_bready(axi_mem_intercon_to_s05_couplers_BREADY),
        .S_AXI_bresp(axi_mem_intercon_to_s05_couplers_BRESP),
        .S_AXI_bvalid(axi_mem_intercon_to_s05_couplers_BVALID),
        .S_AXI_rdata(axi_mem_intercon_to_s05_couplers_RDATA),
        .S_AXI_rlast(axi_mem_intercon_to_s05_couplers_RLAST),
        .S_AXI_rready(axi_mem_intercon_to_s05_couplers_RREADY),
        .S_AXI_rresp(axi_mem_intercon_to_s05_couplers_RRESP),
        .S_AXI_rvalid(axi_mem_intercon_to_s05_couplers_RVALID),
        .S_AXI_wdata(axi_mem_intercon_to_s05_couplers_WDATA),
        .S_AXI_wlast(axi_mem_intercon_to_s05_couplers_WLAST),
        .S_AXI_wready(axi_mem_intercon_to_s05_couplers_WREADY),
        .S_AXI_wstrb(axi_mem_intercon_to_s05_couplers_WSTRB),
        .S_AXI_wvalid(axi_mem_intercon_to_s05_couplers_WVALID));
  s06_couplers_imp_5OWWZ8 s06_couplers
       (.M_ACLK(axi_mem_intercon_ACLK_net),
        .M_ARESETN(axi_mem_intercon_ARESETN_net),
        .M_AXI_araddr(s06_couplers_to_xbar_ARADDR),
        .M_AXI_arburst(s06_couplers_to_xbar_ARBURST),
        .M_AXI_arcache(s06_couplers_to_xbar_ARCACHE),
        .M_AXI_arlen(s06_couplers_to_xbar_ARLEN),
        .M_AXI_arlock(s06_couplers_to_xbar_ARLOCK),
        .M_AXI_arprot(s06_couplers_to_xbar_ARPROT),
        .M_AXI_arqos(s06_couplers_to_xbar_ARQOS),
        .M_AXI_arready(s06_couplers_to_xbar_ARREADY),
        .M_AXI_arsize(s06_couplers_to_xbar_ARSIZE),
        .M_AXI_arvalid(s06_couplers_to_xbar_ARVALID),
        .M_AXI_rdata(s06_couplers_to_xbar_RDATA),
        .M_AXI_rlast(s06_couplers_to_xbar_RLAST),
        .M_AXI_rready(s06_couplers_to_xbar_RREADY),
        .M_AXI_rresp(s06_couplers_to_xbar_RRESP),
        .M_AXI_rvalid(s06_couplers_to_xbar_RVALID),
        .S_ACLK(S06_ACLK_1),
        .S_ARESETN(S06_ARESETN_1),
        .S_AXI_araddr(axi_mem_intercon_to_s06_couplers_ARADDR),
        .S_AXI_arburst(axi_mem_intercon_to_s06_couplers_ARBURST),
        .S_AXI_arcache(axi_mem_intercon_to_s06_couplers_ARCACHE),
        .S_AXI_arlen(axi_mem_intercon_to_s06_couplers_ARLEN),
        .S_AXI_arprot(axi_mem_intercon_to_s06_couplers_ARPROT),
        .S_AXI_arready(axi_mem_intercon_to_s06_couplers_ARREADY),
        .S_AXI_arsize(axi_mem_intercon_to_s06_couplers_ARSIZE),
        .S_AXI_arvalid(axi_mem_intercon_to_s06_couplers_ARVALID),
        .S_AXI_rdata(axi_mem_intercon_to_s06_couplers_RDATA),
        .S_AXI_rlast(axi_mem_intercon_to_s06_couplers_RLAST),
        .S_AXI_rready(axi_mem_intercon_to_s06_couplers_RREADY),
        .S_AXI_rresp(axi_mem_intercon_to_s06_couplers_RRESP),
        .S_AXI_rvalid(axi_mem_intercon_to_s06_couplers_RVALID));
  s07_couplers_imp_1XVBQ51 s07_couplers
       (.M_ACLK(axi_mem_intercon_ACLK_net),
        .M_ARESETN(axi_mem_intercon_ARESETN_net),
        .M_AXI_awaddr(s07_couplers_to_xbar_AWADDR),
        .M_AXI_awburst(s07_couplers_to_xbar_AWBURST),
        .M_AXI_awcache(s07_couplers_to_xbar_AWCACHE),
        .M_AXI_awlen(s07_couplers_to_xbar_AWLEN),
        .M_AXI_awlock(s07_couplers_to_xbar_AWLOCK),
        .M_AXI_awprot(s07_couplers_to_xbar_AWPROT),
        .M_AXI_awqos(s07_couplers_to_xbar_AWQOS),
        .M_AXI_awready(s07_couplers_to_xbar_AWREADY),
        .M_AXI_awsize(s07_couplers_to_xbar_AWSIZE),
        .M_AXI_awvalid(s07_couplers_to_xbar_AWVALID),
        .M_AXI_bready(s07_couplers_to_xbar_BREADY),
        .M_AXI_bresp(s07_couplers_to_xbar_BRESP),
        .M_AXI_bvalid(s07_couplers_to_xbar_BVALID),
        .M_AXI_wdata(s07_couplers_to_xbar_WDATA),
        .M_AXI_wlast(s07_couplers_to_xbar_WLAST),
        .M_AXI_wready(s07_couplers_to_xbar_WREADY),
        .M_AXI_wstrb(s07_couplers_to_xbar_WSTRB),
        .M_AXI_wvalid(s07_couplers_to_xbar_WVALID),
        .S_ACLK(S07_ACLK_1),
        .S_ARESETN(S07_ARESETN_1),
        .S_AXI_awaddr(axi_mem_intercon_to_s07_couplers_AWADDR),
        .S_AXI_awburst(axi_mem_intercon_to_s07_couplers_AWBURST),
        .S_AXI_awcache(axi_mem_intercon_to_s07_couplers_AWCACHE),
        .S_AXI_awlen(axi_mem_intercon_to_s07_couplers_AWLEN),
        .S_AXI_awprot(axi_mem_intercon_to_s07_couplers_AWPROT),
        .S_AXI_awready(axi_mem_intercon_to_s07_couplers_AWREADY),
        .S_AXI_awsize(axi_mem_intercon_to_s07_couplers_AWSIZE),
        .S_AXI_awvalid(axi_mem_intercon_to_s07_couplers_AWVALID),
        .S_AXI_bready(axi_mem_intercon_to_s07_couplers_BREADY),
        .S_AXI_bresp(axi_mem_intercon_to_s07_couplers_BRESP),
        .S_AXI_bvalid(axi_mem_intercon_to_s07_couplers_BVALID),
        .S_AXI_wdata(axi_mem_intercon_to_s07_couplers_WDATA),
        .S_AXI_wlast(axi_mem_intercon_to_s07_couplers_WLAST),
        .S_AXI_wready(axi_mem_intercon_to_s07_couplers_WREADY),
        .S_AXI_wstrb(axi_mem_intercon_to_s07_couplers_WSTRB),
        .S_AXI_wvalid(axi_mem_intercon_to_s07_couplers_WVALID));
  s08_couplers_imp_3SO22L s08_couplers
       (.M_ACLK(axi_mem_intercon_ACLK_net),
        .M_ARESETN(axi_mem_intercon_ARESETN_net),
        .M_AXI_araddr(s08_couplers_to_xbar_ARADDR),
        .M_AXI_arburst(s08_couplers_to_xbar_ARBURST),
        .M_AXI_arcache(s08_couplers_to_xbar_ARCACHE),
        .M_AXI_arlen(s08_couplers_to_xbar_ARLEN),
        .M_AXI_arlock(s08_couplers_to_xbar_ARLOCK),
        .M_AXI_arprot(s08_couplers_to_xbar_ARPROT),
        .M_AXI_arqos(s08_couplers_to_xbar_ARQOS),
        .M_AXI_arready(s08_couplers_to_xbar_ARREADY),
        .M_AXI_arsize(s08_couplers_to_xbar_ARSIZE),
        .M_AXI_arvalid(s08_couplers_to_xbar_ARVALID),
        .M_AXI_awaddr(s08_couplers_to_xbar_AWADDR),
        .M_AXI_awburst(s08_couplers_to_xbar_AWBURST),
        .M_AXI_awcache(s08_couplers_to_xbar_AWCACHE),
        .M_AXI_awlen(s08_couplers_to_xbar_AWLEN),
        .M_AXI_awlock(s08_couplers_to_xbar_AWLOCK),
        .M_AXI_awprot(s08_couplers_to_xbar_AWPROT),
        .M_AXI_awqos(s08_couplers_to_xbar_AWQOS),
        .M_AXI_awready(s08_couplers_to_xbar_AWREADY),
        .M_AXI_awsize(s08_couplers_to_xbar_AWSIZE),
        .M_AXI_awvalid(s08_couplers_to_xbar_AWVALID),
        .M_AXI_bready(s08_couplers_to_xbar_BREADY),
        .M_AXI_bresp(s08_couplers_to_xbar_BRESP),
        .M_AXI_bvalid(s08_couplers_to_xbar_BVALID),
        .M_AXI_rdata(s08_couplers_to_xbar_RDATA),
        .M_AXI_rlast(s08_couplers_to_xbar_RLAST),
        .M_AXI_rready(s08_couplers_to_xbar_RREADY),
        .M_AXI_rresp(s08_couplers_to_xbar_RRESP),
        .M_AXI_rvalid(s08_couplers_to_xbar_RVALID),
        .M_AXI_wdata(s08_couplers_to_xbar_WDATA),
        .M_AXI_wlast(s08_couplers_to_xbar_WLAST),
        .M_AXI_wready(s08_couplers_to_xbar_WREADY),
        .M_AXI_wstrb(s08_couplers_to_xbar_WSTRB),
        .M_AXI_wvalid(s08_couplers_to_xbar_WVALID),
        .S_ACLK(S08_ACLK_1),
        .S_ARESETN(S08_ARESETN_1),
        .S_AXI_araddr(axi_mem_intercon_to_s08_couplers_ARADDR),
        .S_AXI_arburst(axi_mem_intercon_to_s08_couplers_ARBURST),
        .S_AXI_arcache(axi_mem_intercon_to_s08_couplers_ARCACHE),
        .S_AXI_arlen(axi_mem_intercon_to_s08_couplers_ARLEN),
        .S_AXI_arprot(axi_mem_intercon_to_s08_couplers_ARPROT),
        .S_AXI_arready(axi_mem_intercon_to_s08_couplers_ARREADY),
        .S_AXI_arsize(axi_mem_intercon_to_s08_couplers_ARSIZE),
        .S_AXI_arvalid(axi_mem_intercon_to_s08_couplers_ARVALID),
        .S_AXI_awaddr(axi_mem_intercon_to_s08_couplers_AWADDR),
        .S_AXI_awburst(axi_mem_intercon_to_s08_couplers_AWBURST),
        .S_AXI_awcache(axi_mem_intercon_to_s08_couplers_AWCACHE),
        .S_AXI_awlen(axi_mem_intercon_to_s08_couplers_AWLEN),
        .S_AXI_awprot(axi_mem_intercon_to_s08_couplers_AWPROT),
        .S_AXI_awready(axi_mem_intercon_to_s08_couplers_AWREADY),
        .S_AXI_awsize(axi_mem_intercon_to_s08_couplers_AWSIZE),
        .S_AXI_awvalid(axi_mem_intercon_to_s08_couplers_AWVALID),
        .S_AXI_bready(axi_mem_intercon_to_s08_couplers_BREADY),
        .S_AXI_bresp(axi_mem_intercon_to_s08_couplers_BRESP),
        .S_AXI_bvalid(axi_mem_intercon_to_s08_couplers_BVALID),
        .S_AXI_rdata(axi_mem_intercon_to_s08_couplers_RDATA),
        .S_AXI_rlast(axi_mem_intercon_to_s08_couplers_RLAST),
        .S_AXI_rready(axi_mem_intercon_to_s08_couplers_RREADY),
        .S_AXI_rresp(axi_mem_intercon_to_s08_couplers_RRESP),
        .S_AXI_rvalid(axi_mem_intercon_to_s08_couplers_RVALID),
        .S_AXI_wdata(axi_mem_intercon_to_s08_couplers_WDATA),
        .S_AXI_wlast(axi_mem_intercon_to_s08_couplers_WLAST),
        .S_AXI_wready(axi_mem_intercon_to_s08_couplers_WREADY),
        .S_AXI_wstrb(axi_mem_intercon_to_s08_couplers_WSTRB),
        .S_AXI_wvalid(axi_mem_intercon_to_s08_couplers_WVALID));
  s09_couplers_imp_1QZADNG s09_couplers
       (.M_ACLK(axi_mem_intercon_ACLK_net),
        .M_ARESETN(axi_mem_intercon_ARESETN_net),
        .M_AXI_araddr(s09_couplers_to_xbar_ARADDR),
        .M_AXI_arburst(s09_couplers_to_xbar_ARBURST),
        .M_AXI_arcache(s09_couplers_to_xbar_ARCACHE),
        .M_AXI_arlen(s09_couplers_to_xbar_ARLEN),
        .M_AXI_arlock(s09_couplers_to_xbar_ARLOCK),
        .M_AXI_arprot(s09_couplers_to_xbar_ARPROT),
        .M_AXI_arqos(s09_couplers_to_xbar_ARQOS),
        .M_AXI_arready(s09_couplers_to_xbar_ARREADY),
        .M_AXI_arsize(s09_couplers_to_xbar_ARSIZE),
        .M_AXI_arvalid(s09_couplers_to_xbar_ARVALID),
        .M_AXI_rdata(s09_couplers_to_xbar_RDATA),
        .M_AXI_rlast(s09_couplers_to_xbar_RLAST),
        .M_AXI_rready(s09_couplers_to_xbar_RREADY),
        .M_AXI_rresp(s09_couplers_to_xbar_RRESP),
        .M_AXI_rvalid(s09_couplers_to_xbar_RVALID),
        .S_ACLK(S09_ACLK_1),
        .S_ARESETN(S09_ARESETN_1),
        .S_AXI_araddr(axi_mem_intercon_to_s09_couplers_ARADDR),
        .S_AXI_arburst(axi_mem_intercon_to_s09_couplers_ARBURST),
        .S_AXI_arcache(axi_mem_intercon_to_s09_couplers_ARCACHE),
        .S_AXI_arlen(axi_mem_intercon_to_s09_couplers_ARLEN),
        .S_AXI_arprot(axi_mem_intercon_to_s09_couplers_ARPROT),
        .S_AXI_arready(axi_mem_intercon_to_s09_couplers_ARREADY),
        .S_AXI_arsize(axi_mem_intercon_to_s09_couplers_ARSIZE),
        .S_AXI_arvalid(axi_mem_intercon_to_s09_couplers_ARVALID),
        .S_AXI_rdata(axi_mem_intercon_to_s09_couplers_RDATA),
        .S_AXI_rlast(axi_mem_intercon_to_s09_couplers_RLAST),
        .S_AXI_rready(axi_mem_intercon_to_s09_couplers_RREADY),
        .S_AXI_rresp(axi_mem_intercon_to_s09_couplers_RRESP),
        .S_AXI_rvalid(axi_mem_intercon_to_s09_couplers_RVALID));
  s10_couplers_imp_1KWU8FD s10_couplers
       (.M_ACLK(axi_mem_intercon_ACLK_net),
        .M_ARESETN(axi_mem_intercon_ARESETN_net),
        .M_AXI_awaddr(s10_couplers_to_xbar_AWADDR),
        .M_AXI_awburst(s10_couplers_to_xbar_AWBURST),
        .M_AXI_awcache(s10_couplers_to_xbar_AWCACHE),
        .M_AXI_awlen(s10_couplers_to_xbar_AWLEN),
        .M_AXI_awlock(s10_couplers_to_xbar_AWLOCK),
        .M_AXI_awprot(s10_couplers_to_xbar_AWPROT),
        .M_AXI_awqos(s10_couplers_to_xbar_AWQOS),
        .M_AXI_awready(s10_couplers_to_xbar_AWREADY),
        .M_AXI_awsize(s10_couplers_to_xbar_AWSIZE),
        .M_AXI_awvalid(s10_couplers_to_xbar_AWVALID),
        .M_AXI_bready(s10_couplers_to_xbar_BREADY),
        .M_AXI_bresp(s10_couplers_to_xbar_BRESP),
        .M_AXI_bvalid(s10_couplers_to_xbar_BVALID),
        .M_AXI_wdata(s10_couplers_to_xbar_WDATA),
        .M_AXI_wlast(s10_couplers_to_xbar_WLAST),
        .M_AXI_wready(s10_couplers_to_xbar_WREADY),
        .M_AXI_wstrb(s10_couplers_to_xbar_WSTRB),
        .M_AXI_wvalid(s10_couplers_to_xbar_WVALID),
        .S_ACLK(S10_ACLK_1),
        .S_ARESETN(S10_ARESETN_1),
        .S_AXI_awaddr(axi_mem_intercon_to_s10_couplers_AWADDR),
        .S_AXI_awburst(axi_mem_intercon_to_s10_couplers_AWBURST),
        .S_AXI_awcache(axi_mem_intercon_to_s10_couplers_AWCACHE),
        .S_AXI_awlen(axi_mem_intercon_to_s10_couplers_AWLEN),
        .S_AXI_awprot(axi_mem_intercon_to_s10_couplers_AWPROT),
        .S_AXI_awready(axi_mem_intercon_to_s10_couplers_AWREADY),
        .S_AXI_awsize(axi_mem_intercon_to_s10_couplers_AWSIZE),
        .S_AXI_awvalid(axi_mem_intercon_to_s10_couplers_AWVALID),
        .S_AXI_bready(axi_mem_intercon_to_s10_couplers_BREADY),
        .S_AXI_bresp(axi_mem_intercon_to_s10_couplers_BRESP),
        .S_AXI_bvalid(axi_mem_intercon_to_s10_couplers_BVALID),
        .S_AXI_wdata(axi_mem_intercon_to_s10_couplers_WDATA),
        .S_AXI_wlast(axi_mem_intercon_to_s10_couplers_WLAST),
        .S_AXI_wready(axi_mem_intercon_to_s10_couplers_WREADY),
        .S_AXI_wstrb(axi_mem_intercon_to_s10_couplers_WSTRB),
        .S_AXI_wvalid(axi_mem_intercon_to_s10_couplers_WVALID));
  s11_couplers_imp_9IPFIG s11_couplers
       (.M_ACLK(axi_mem_intercon_ACLK_net),
        .M_ARESETN(axi_mem_intercon_ARESETN_net),
        .M_AXI_araddr(s11_couplers_to_xbar_ARADDR),
        .M_AXI_arburst(s11_couplers_to_xbar_ARBURST),
        .M_AXI_arcache(s11_couplers_to_xbar_ARCACHE),
        .M_AXI_arlen(s11_couplers_to_xbar_ARLEN),
        .M_AXI_arlock(s11_couplers_to_xbar_ARLOCK),
        .M_AXI_arprot(s11_couplers_to_xbar_ARPROT),
        .M_AXI_arqos(s11_couplers_to_xbar_ARQOS),
        .M_AXI_arready(s11_couplers_to_xbar_ARREADY),
        .M_AXI_arsize(s11_couplers_to_xbar_ARSIZE),
        .M_AXI_arvalid(s11_couplers_to_xbar_ARVALID),
        .M_AXI_awaddr(s11_couplers_to_xbar_AWADDR),
        .M_AXI_awburst(s11_couplers_to_xbar_AWBURST),
        .M_AXI_awcache(s11_couplers_to_xbar_AWCACHE),
        .M_AXI_awlen(s11_couplers_to_xbar_AWLEN),
        .M_AXI_awlock(s11_couplers_to_xbar_AWLOCK),
        .M_AXI_awprot(s11_couplers_to_xbar_AWPROT),
        .M_AXI_awqos(s11_couplers_to_xbar_AWQOS),
        .M_AXI_awready(s11_couplers_to_xbar_AWREADY),
        .M_AXI_awsize(s11_couplers_to_xbar_AWSIZE),
        .M_AXI_awvalid(s11_couplers_to_xbar_AWVALID),
        .M_AXI_bready(s11_couplers_to_xbar_BREADY),
        .M_AXI_bresp(s11_couplers_to_xbar_BRESP),
        .M_AXI_bvalid(s11_couplers_to_xbar_BVALID),
        .M_AXI_rdata(s11_couplers_to_xbar_RDATA),
        .M_AXI_rlast(s11_couplers_to_xbar_RLAST),
        .M_AXI_rready(s11_couplers_to_xbar_RREADY),
        .M_AXI_rresp(s11_couplers_to_xbar_RRESP),
        .M_AXI_rvalid(s11_couplers_to_xbar_RVALID),
        .M_AXI_wdata(s11_couplers_to_xbar_WDATA),
        .M_AXI_wlast(s11_couplers_to_xbar_WLAST),
        .M_AXI_wready(s11_couplers_to_xbar_WREADY),
        .M_AXI_wstrb(s11_couplers_to_xbar_WSTRB),
        .M_AXI_wvalid(s11_couplers_to_xbar_WVALID),
        .S_ACLK(S11_ACLK_1),
        .S_ARESETN(S11_ARESETN_1),
        .S_AXI_araddr(axi_mem_intercon_to_s11_couplers_ARADDR),
        .S_AXI_arburst(axi_mem_intercon_to_s11_couplers_ARBURST),
        .S_AXI_arcache(axi_mem_intercon_to_s11_couplers_ARCACHE),
        .S_AXI_arlen(axi_mem_intercon_to_s11_couplers_ARLEN),
        .S_AXI_arprot(axi_mem_intercon_to_s11_couplers_ARPROT),
        .S_AXI_arready(axi_mem_intercon_to_s11_couplers_ARREADY),
        .S_AXI_arsize(axi_mem_intercon_to_s11_couplers_ARSIZE),
        .S_AXI_arvalid(axi_mem_intercon_to_s11_couplers_ARVALID),
        .S_AXI_awaddr(axi_mem_intercon_to_s11_couplers_AWADDR),
        .S_AXI_awburst(axi_mem_intercon_to_s11_couplers_AWBURST),
        .S_AXI_awcache(axi_mem_intercon_to_s11_couplers_AWCACHE),
        .S_AXI_awlen(axi_mem_intercon_to_s11_couplers_AWLEN),
        .S_AXI_awprot(axi_mem_intercon_to_s11_couplers_AWPROT),
        .S_AXI_awready(axi_mem_intercon_to_s11_couplers_AWREADY),
        .S_AXI_awsize(axi_mem_intercon_to_s11_couplers_AWSIZE),
        .S_AXI_awvalid(axi_mem_intercon_to_s11_couplers_AWVALID),
        .S_AXI_bready(axi_mem_intercon_to_s11_couplers_BREADY),
        .S_AXI_bresp(axi_mem_intercon_to_s11_couplers_BRESP),
        .S_AXI_bvalid(axi_mem_intercon_to_s11_couplers_BVALID),
        .S_AXI_rdata(axi_mem_intercon_to_s11_couplers_RDATA),
        .S_AXI_rlast(axi_mem_intercon_to_s11_couplers_RLAST),
        .S_AXI_rready(axi_mem_intercon_to_s11_couplers_RREADY),
        .S_AXI_rresp(axi_mem_intercon_to_s11_couplers_RRESP),
        .S_AXI_rvalid(axi_mem_intercon_to_s11_couplers_RVALID),
        .S_AXI_wdata(axi_mem_intercon_to_s11_couplers_WDATA),
        .S_AXI_wlast(axi_mem_intercon_to_s11_couplers_WLAST),
        .S_AXI_wready(axi_mem_intercon_to_s11_couplers_WREADY),
        .S_AXI_wstrb(axi_mem_intercon_to_s11_couplers_WSTRB),
        .S_AXI_wvalid(axi_mem_intercon_to_s11_couplers_WVALID));
  s12_couplers_imp_1JPMFMY s12_couplers
       (.M_ACLK(axi_mem_intercon_ACLK_net),
        .M_ARESETN(axi_mem_intercon_ARESETN_net),
        .M_AXI_araddr(s12_couplers_to_xbar_ARADDR),
        .M_AXI_arburst(s12_couplers_to_xbar_ARBURST),
        .M_AXI_arcache(s12_couplers_to_xbar_ARCACHE),
        .M_AXI_arlen(s12_couplers_to_xbar_ARLEN),
        .M_AXI_arlock(s12_couplers_to_xbar_ARLOCK),
        .M_AXI_arprot(s12_couplers_to_xbar_ARPROT),
        .M_AXI_arqos(s12_couplers_to_xbar_ARQOS),
        .M_AXI_arready(s12_couplers_to_xbar_ARREADY),
        .M_AXI_arsize(s12_couplers_to_xbar_ARSIZE),
        .M_AXI_arvalid(s12_couplers_to_xbar_ARVALID),
        .M_AXI_rdata(s12_couplers_to_xbar_RDATA),
        .M_AXI_rlast(s12_couplers_to_xbar_RLAST),
        .M_AXI_rready(s12_couplers_to_xbar_RREADY),
        .M_AXI_rresp(s12_couplers_to_xbar_RRESP),
        .M_AXI_rvalid(s12_couplers_to_xbar_RVALID),
        .S_ACLK(S12_ACLK_1),
        .S_ARESETN(S12_ARESETN_1),
        .S_AXI_araddr(axi_mem_intercon_to_s12_couplers_ARADDR),
        .S_AXI_arburst(axi_mem_intercon_to_s12_couplers_ARBURST),
        .S_AXI_arcache(axi_mem_intercon_to_s12_couplers_ARCACHE),
        .S_AXI_arlen(axi_mem_intercon_to_s12_couplers_ARLEN),
        .S_AXI_arprot(axi_mem_intercon_to_s12_couplers_ARPROT),
        .S_AXI_arready(axi_mem_intercon_to_s12_couplers_ARREADY),
        .S_AXI_arsize(axi_mem_intercon_to_s12_couplers_ARSIZE),
        .S_AXI_arvalid(axi_mem_intercon_to_s12_couplers_ARVALID),
        .S_AXI_rdata(axi_mem_intercon_to_s12_couplers_RDATA),
        .S_AXI_rlast(axi_mem_intercon_to_s12_couplers_RLAST),
        .S_AXI_rready(axi_mem_intercon_to_s12_couplers_RREADY),
        .S_AXI_rresp(axi_mem_intercon_to_s12_couplers_RRESP),
        .S_AXI_rvalid(axi_mem_intercon_to_s12_couplers_RVALID));
  s13_couplers_imp_AZMDZF s13_couplers
       (.M_ACLK(axi_mem_intercon_ACLK_net),
        .M_ARESETN(axi_mem_intercon_ARESETN_net),
        .M_AXI_awaddr(s13_couplers_to_xbar_AWADDR),
        .M_AXI_awburst(s13_couplers_to_xbar_AWBURST),
        .M_AXI_awcache(s13_couplers_to_xbar_AWCACHE),
        .M_AXI_awlen(s13_couplers_to_xbar_AWLEN),
        .M_AXI_awlock(s13_couplers_to_xbar_AWLOCK),
        .M_AXI_awprot(s13_couplers_to_xbar_AWPROT),
        .M_AXI_awqos(s13_couplers_to_xbar_AWQOS),
        .M_AXI_awready(s13_couplers_to_xbar_AWREADY),
        .M_AXI_awsize(s13_couplers_to_xbar_AWSIZE),
        .M_AXI_awvalid(s13_couplers_to_xbar_AWVALID),
        .M_AXI_bready(s13_couplers_to_xbar_BREADY),
        .M_AXI_bresp(s13_couplers_to_xbar_BRESP),
        .M_AXI_bvalid(s13_couplers_to_xbar_BVALID),
        .M_AXI_wdata(s13_couplers_to_xbar_WDATA),
        .M_AXI_wlast(s13_couplers_to_xbar_WLAST),
        .M_AXI_wready(s13_couplers_to_xbar_WREADY),
        .M_AXI_wstrb(s13_couplers_to_xbar_WSTRB),
        .M_AXI_wvalid(s13_couplers_to_xbar_WVALID),
        .S_ACLK(S13_ACLK_1),
        .S_ARESETN(S13_ARESETN_1),
        .S_AXI_awaddr(axi_mem_intercon_to_s13_couplers_AWADDR),
        .S_AXI_awburst(axi_mem_intercon_to_s13_couplers_AWBURST),
        .S_AXI_awcache(axi_mem_intercon_to_s13_couplers_AWCACHE),
        .S_AXI_awlen(axi_mem_intercon_to_s13_couplers_AWLEN),
        .S_AXI_awprot(axi_mem_intercon_to_s13_couplers_AWPROT),
        .S_AXI_awready(axi_mem_intercon_to_s13_couplers_AWREADY),
        .S_AXI_awsize(axi_mem_intercon_to_s13_couplers_AWSIZE),
        .S_AXI_awvalid(axi_mem_intercon_to_s13_couplers_AWVALID),
        .S_AXI_bready(axi_mem_intercon_to_s13_couplers_BREADY),
        .S_AXI_bresp(axi_mem_intercon_to_s13_couplers_BRESP),
        .S_AXI_bvalid(axi_mem_intercon_to_s13_couplers_BVALID),
        .S_AXI_wdata(axi_mem_intercon_to_s13_couplers_WDATA),
        .S_AXI_wlast(axi_mem_intercon_to_s13_couplers_WLAST),
        .S_AXI_wready(axi_mem_intercon_to_s13_couplers_WREADY),
        .S_AXI_wstrb(axi_mem_intercon_to_s13_couplers_WSTRB),
        .S_AXI_wvalid(axi_mem_intercon_to_s13_couplers_WVALID));
  design_1_xbar_0 xbar
       (.aclk(axi_mem_intercon_ACLK_net),
        .aresetn(axi_mem_intercon_ARESETN_net),
        .m_axi_araddr(xbar_to_m00_couplers_ARADDR),
        .m_axi_arburst(xbar_to_m00_couplers_ARBURST),
        .m_axi_arcache(xbar_to_m00_couplers_ARCACHE),
        .m_axi_arid(xbar_to_m00_couplers_ARID),
        .m_axi_arlen(xbar_to_m00_couplers_ARLEN),
        .m_axi_arlock(xbar_to_m00_couplers_ARLOCK),
        .m_axi_arprot(xbar_to_m00_couplers_ARPROT),
        .m_axi_arqos(xbar_to_m00_couplers_ARQOS),
        .m_axi_arready(xbar_to_m00_couplers_ARREADY),
        .m_axi_arsize(xbar_to_m00_couplers_ARSIZE),
        .m_axi_arvalid(xbar_to_m00_couplers_ARVALID),
        .m_axi_awaddr(xbar_to_m00_couplers_AWADDR),
        .m_axi_awburst(xbar_to_m00_couplers_AWBURST),
        .m_axi_awcache(xbar_to_m00_couplers_AWCACHE),
        .m_axi_awid(xbar_to_m00_couplers_AWID),
        .m_axi_awlen(xbar_to_m00_couplers_AWLEN),
        .m_axi_awlock(xbar_to_m00_couplers_AWLOCK),
        .m_axi_awprot(xbar_to_m00_couplers_AWPROT),
        .m_axi_awqos(xbar_to_m00_couplers_AWQOS),
        .m_axi_awready(xbar_to_m00_couplers_AWREADY),
        .m_axi_awsize(xbar_to_m00_couplers_AWSIZE),
        .m_axi_awvalid(xbar_to_m00_couplers_AWVALID),
        .m_axi_bid(xbar_to_m00_couplers_BID),
        .m_axi_bready(xbar_to_m00_couplers_BREADY),
        .m_axi_bresp(xbar_to_m00_couplers_BRESP),
        .m_axi_bvalid(xbar_to_m00_couplers_BVALID),
        .m_axi_rdata(xbar_to_m00_couplers_RDATA),
        .m_axi_rid(xbar_to_m00_couplers_RID),
        .m_axi_rlast(xbar_to_m00_couplers_RLAST),
        .m_axi_rready(xbar_to_m00_couplers_RREADY),
        .m_axi_rresp(xbar_to_m00_couplers_RRESP),
        .m_axi_rvalid(xbar_to_m00_couplers_RVALID),
        .m_axi_wdata(xbar_to_m00_couplers_WDATA),
        .m_axi_wlast(xbar_to_m00_couplers_WLAST),
        .m_axi_wready(xbar_to_m00_couplers_WREADY),
        .m_axi_wstrb(xbar_to_m00_couplers_WSTRB),
        .m_axi_wvalid(xbar_to_m00_couplers_WVALID),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s12_couplers_to_xbar_ARADDR,s11_couplers_to_xbar_ARADDR,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s09_couplers_to_xbar_ARADDR,s08_couplers_to_xbar_ARADDR,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s06_couplers_to_xbar_ARADDR,s05_couplers_to_xbar_ARADDR,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s03_couplers_to_xbar_ARADDR,s02_couplers_to_xbar_ARADDR,s01_couplers_to_xbar_ARADDR,s00_couplers_to_xbar_ARADDR}),
        .s_axi_arburst({1'b0,1'b0,s12_couplers_to_xbar_ARBURST,s11_couplers_to_xbar_ARBURST,1'b0,1'b0,s09_couplers_to_xbar_ARBURST,s08_couplers_to_xbar_ARBURST,1'b0,1'b0,s06_couplers_to_xbar_ARBURST,s05_couplers_to_xbar_ARBURST,1'b0,1'b0,s03_couplers_to_xbar_ARBURST,s02_couplers_to_xbar_ARBURST,s01_couplers_to_xbar_ARBURST,s00_couplers_to_xbar_ARBURST}),
        .s_axi_arcache({1'b0,1'b0,1'b0,1'b0,s12_couplers_to_xbar_ARCACHE,s11_couplers_to_xbar_ARCACHE,1'b0,1'b0,1'b0,1'b0,s09_couplers_to_xbar_ARCACHE,s08_couplers_to_xbar_ARCACHE,1'b0,1'b0,1'b0,1'b0,s06_couplers_to_xbar_ARCACHE,s05_couplers_to_xbar_ARCACHE,1'b0,1'b0,1'b0,1'b0,s03_couplers_to_xbar_ARCACHE,s02_couplers_to_xbar_ARCACHE,s01_couplers_to_xbar_ARCACHE,s00_couplers_to_xbar_ARCACHE}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s12_couplers_to_xbar_ARLEN,s11_couplers_to_xbar_ARLEN,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s09_couplers_to_xbar_ARLEN,s08_couplers_to_xbar_ARLEN,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s06_couplers_to_xbar_ARLEN,s05_couplers_to_xbar_ARLEN,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s03_couplers_to_xbar_ARLEN,s02_couplers_to_xbar_ARLEN,s01_couplers_to_xbar_ARLEN,s00_couplers_to_xbar_ARLEN}),
        .s_axi_arlock({1'b0,s12_couplers_to_xbar_ARLOCK,s11_couplers_to_xbar_ARLOCK,1'b0,s09_couplers_to_xbar_ARLOCK,s08_couplers_to_xbar_ARLOCK,1'b0,s06_couplers_to_xbar_ARLOCK,s05_couplers_to_xbar_ARLOCK,1'b0,s03_couplers_to_xbar_ARLOCK,s02_couplers_to_xbar_ARLOCK,s01_couplers_to_xbar_ARLOCK,s00_couplers_to_xbar_ARLOCK}),
        .s_axi_arprot({1'b0,1'b0,1'b0,s12_couplers_to_xbar_ARPROT,s11_couplers_to_xbar_ARPROT,1'b0,1'b0,1'b0,s09_couplers_to_xbar_ARPROT,s08_couplers_to_xbar_ARPROT,1'b0,1'b0,1'b0,s06_couplers_to_xbar_ARPROT,s05_couplers_to_xbar_ARPROT,1'b0,1'b0,1'b0,s03_couplers_to_xbar_ARPROT,s02_couplers_to_xbar_ARPROT,s01_couplers_to_xbar_ARPROT,s00_couplers_to_xbar_ARPROT}),
        .s_axi_arqos({1'b0,1'b0,1'b0,1'b0,s12_couplers_to_xbar_ARQOS,s11_couplers_to_xbar_ARQOS,1'b0,1'b0,1'b0,1'b0,s09_couplers_to_xbar_ARQOS,s08_couplers_to_xbar_ARQOS,1'b0,1'b0,1'b0,1'b0,s06_couplers_to_xbar_ARQOS,s05_couplers_to_xbar_ARQOS,1'b0,1'b0,1'b0,1'b0,s03_couplers_to_xbar_ARQOS,s02_couplers_to_xbar_ARQOS,s01_couplers_to_xbar_ARQOS,s00_couplers_to_xbar_ARQOS}),
        .s_axi_arready({s12_couplers_to_xbar_ARREADY,s11_couplers_to_xbar_ARREADY,NLW_xbar_s_axi_arready_UNCONNECTED[10],s09_couplers_to_xbar_ARREADY,s08_couplers_to_xbar_ARREADY,NLW_xbar_s_axi_arready_UNCONNECTED[7],s06_couplers_to_xbar_ARREADY,s05_couplers_to_xbar_ARREADY,NLW_xbar_s_axi_arready_UNCONNECTED[4],s03_couplers_to_xbar_ARREADY,s02_couplers_to_xbar_ARREADY,s01_couplers_to_xbar_ARREADY,s00_couplers_to_xbar_ARREADY}),
        .s_axi_arsize({1'b0,1'b0,1'b0,s12_couplers_to_xbar_ARSIZE,s11_couplers_to_xbar_ARSIZE,1'b0,1'b0,1'b0,s09_couplers_to_xbar_ARSIZE,s08_couplers_to_xbar_ARSIZE,1'b0,1'b0,1'b0,s06_couplers_to_xbar_ARSIZE,s05_couplers_to_xbar_ARSIZE,1'b0,1'b0,1'b0,s03_couplers_to_xbar_ARSIZE,s02_couplers_to_xbar_ARSIZE,s01_couplers_to_xbar_ARSIZE,s00_couplers_to_xbar_ARSIZE}),
        .s_axi_arvalid({1'b0,s12_couplers_to_xbar_ARVALID,s11_couplers_to_xbar_ARVALID,1'b0,s09_couplers_to_xbar_ARVALID,s08_couplers_to_xbar_ARVALID,1'b0,s06_couplers_to_xbar_ARVALID,s05_couplers_to_xbar_ARVALID,1'b0,s03_couplers_to_xbar_ARVALID,s02_couplers_to_xbar_ARVALID,s01_couplers_to_xbar_ARVALID,s00_couplers_to_xbar_ARVALID}),
        .s_axi_awaddr({s13_couplers_to_xbar_AWADDR,1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s11_couplers_to_xbar_AWADDR,s10_couplers_to_xbar_AWADDR,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s08_couplers_to_xbar_AWADDR,s07_couplers_to_xbar_AWADDR,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s05_couplers_to_xbar_AWADDR,s04_couplers_to_xbar_AWADDR,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s02_couplers_to_xbar_AWADDR,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_couplers_to_xbar_AWADDR}),
        .s_axi_awburst({s13_couplers_to_xbar_AWBURST,1'b0,1'b0,s11_couplers_to_xbar_AWBURST,s10_couplers_to_xbar_AWBURST,1'b0,1'b0,s08_couplers_to_xbar_AWBURST,s07_couplers_to_xbar_AWBURST,1'b0,1'b0,s05_couplers_to_xbar_AWBURST,s04_couplers_to_xbar_AWBURST,1'b0,1'b0,s02_couplers_to_xbar_AWBURST,1'b0,1'b0,s00_couplers_to_xbar_AWBURST}),
        .s_axi_awcache({s13_couplers_to_xbar_AWCACHE,1'b0,1'b0,1'b0,1'b0,s11_couplers_to_xbar_AWCACHE,s10_couplers_to_xbar_AWCACHE,1'b0,1'b0,1'b0,1'b0,s08_couplers_to_xbar_AWCACHE,s07_couplers_to_xbar_AWCACHE,1'b0,1'b0,1'b0,1'b0,s05_couplers_to_xbar_AWCACHE,s04_couplers_to_xbar_AWCACHE,1'b0,1'b0,1'b0,1'b0,s02_couplers_to_xbar_AWCACHE,1'b0,1'b0,1'b0,1'b0,s00_couplers_to_xbar_AWCACHE}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({s13_couplers_to_xbar_AWLEN,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s11_couplers_to_xbar_AWLEN,s10_couplers_to_xbar_AWLEN,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s08_couplers_to_xbar_AWLEN,s07_couplers_to_xbar_AWLEN,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s05_couplers_to_xbar_AWLEN,s04_couplers_to_xbar_AWLEN,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s02_couplers_to_xbar_AWLEN,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_couplers_to_xbar_AWLEN}),
        .s_axi_awlock({s13_couplers_to_xbar_AWLOCK,1'b0,s11_couplers_to_xbar_AWLOCK,s10_couplers_to_xbar_AWLOCK,1'b0,s08_couplers_to_xbar_AWLOCK,s07_couplers_to_xbar_AWLOCK,1'b0,s05_couplers_to_xbar_AWLOCK,s04_couplers_to_xbar_AWLOCK,1'b0,s02_couplers_to_xbar_AWLOCK,1'b0,s00_couplers_to_xbar_AWLOCK}),
        .s_axi_awprot({s13_couplers_to_xbar_AWPROT,1'b0,1'b0,1'b0,s11_couplers_to_xbar_AWPROT,s10_couplers_to_xbar_AWPROT,1'b0,1'b0,1'b0,s08_couplers_to_xbar_AWPROT,s07_couplers_to_xbar_AWPROT,1'b0,1'b0,1'b0,s05_couplers_to_xbar_AWPROT,s04_couplers_to_xbar_AWPROT,1'b0,1'b0,1'b0,s02_couplers_to_xbar_AWPROT,1'b0,1'b0,1'b0,s00_couplers_to_xbar_AWPROT}),
        .s_axi_awqos({s13_couplers_to_xbar_AWQOS,1'b0,1'b0,1'b0,1'b0,s11_couplers_to_xbar_AWQOS,s10_couplers_to_xbar_AWQOS,1'b0,1'b0,1'b0,1'b0,s08_couplers_to_xbar_AWQOS,s07_couplers_to_xbar_AWQOS,1'b0,1'b0,1'b0,1'b0,s05_couplers_to_xbar_AWQOS,s04_couplers_to_xbar_AWQOS,1'b0,1'b0,1'b0,1'b0,s02_couplers_to_xbar_AWQOS,1'b0,1'b0,1'b0,1'b0,s00_couplers_to_xbar_AWQOS}),
        .s_axi_awready({s13_couplers_to_xbar_AWREADY,NLW_xbar_s_axi_awready_UNCONNECTED[12],s11_couplers_to_xbar_AWREADY,s10_couplers_to_xbar_AWREADY,NLW_xbar_s_axi_awready_UNCONNECTED[9],s08_couplers_to_xbar_AWREADY,s07_couplers_to_xbar_AWREADY,NLW_xbar_s_axi_awready_UNCONNECTED[6],s05_couplers_to_xbar_AWREADY,s04_couplers_to_xbar_AWREADY,NLW_xbar_s_axi_awready_UNCONNECTED[3],s02_couplers_to_xbar_AWREADY,NLW_xbar_s_axi_awready_UNCONNECTED[1],s00_couplers_to_xbar_AWREADY}),
        .s_axi_awsize({s13_couplers_to_xbar_AWSIZE,1'b0,1'b0,1'b0,s11_couplers_to_xbar_AWSIZE,s10_couplers_to_xbar_AWSIZE,1'b0,1'b0,1'b0,s08_couplers_to_xbar_AWSIZE,s07_couplers_to_xbar_AWSIZE,1'b0,1'b0,1'b0,s05_couplers_to_xbar_AWSIZE,s04_couplers_to_xbar_AWSIZE,1'b0,1'b0,1'b0,s02_couplers_to_xbar_AWSIZE,1'b0,1'b0,1'b0,s00_couplers_to_xbar_AWSIZE}),
        .s_axi_awvalid({s13_couplers_to_xbar_AWVALID,1'b0,s11_couplers_to_xbar_AWVALID,s10_couplers_to_xbar_AWVALID,1'b0,s08_couplers_to_xbar_AWVALID,s07_couplers_to_xbar_AWVALID,1'b0,s05_couplers_to_xbar_AWVALID,s04_couplers_to_xbar_AWVALID,1'b0,s02_couplers_to_xbar_AWVALID,1'b0,s00_couplers_to_xbar_AWVALID}),
        .s_axi_bready({s13_couplers_to_xbar_BREADY,1'b0,s11_couplers_to_xbar_BREADY,s10_couplers_to_xbar_BREADY,1'b0,s08_couplers_to_xbar_BREADY,s07_couplers_to_xbar_BREADY,1'b0,s05_couplers_to_xbar_BREADY,s04_couplers_to_xbar_BREADY,1'b0,s02_couplers_to_xbar_BREADY,1'b0,s00_couplers_to_xbar_BREADY}),
        .s_axi_bresp({s13_couplers_to_xbar_BRESP,NLW_xbar_s_axi_bresp_UNCONNECTED[25:24],s11_couplers_to_xbar_BRESP,s10_couplers_to_xbar_BRESP,NLW_xbar_s_axi_bresp_UNCONNECTED[19:18],s08_couplers_to_xbar_BRESP,s07_couplers_to_xbar_BRESP,NLW_xbar_s_axi_bresp_UNCONNECTED[13:12],s05_couplers_to_xbar_BRESP,s04_couplers_to_xbar_BRESP,NLW_xbar_s_axi_bresp_UNCONNECTED[7:6],s02_couplers_to_xbar_BRESP,NLW_xbar_s_axi_bresp_UNCONNECTED[3:2],s00_couplers_to_xbar_BRESP}),
        .s_axi_bvalid({s13_couplers_to_xbar_BVALID,NLW_xbar_s_axi_bvalid_UNCONNECTED[12],s11_couplers_to_xbar_BVALID,s10_couplers_to_xbar_BVALID,NLW_xbar_s_axi_bvalid_UNCONNECTED[9],s08_couplers_to_xbar_BVALID,s07_couplers_to_xbar_BVALID,NLW_xbar_s_axi_bvalid_UNCONNECTED[6],s05_couplers_to_xbar_BVALID,s04_couplers_to_xbar_BVALID,NLW_xbar_s_axi_bvalid_UNCONNECTED[3],s02_couplers_to_xbar_BVALID,NLW_xbar_s_axi_bvalid_UNCONNECTED[1],s00_couplers_to_xbar_BVALID}),
        .s_axi_rdata({s12_couplers_to_xbar_RDATA,s11_couplers_to_xbar_RDATA,NLW_xbar_s_axi_rdata_UNCONNECTED[5631:5120],s09_couplers_to_xbar_RDATA,s08_couplers_to_xbar_RDATA,NLW_xbar_s_axi_rdata_UNCONNECTED[4095:3584],s06_couplers_to_xbar_RDATA,s05_couplers_to_xbar_RDATA,NLW_xbar_s_axi_rdata_UNCONNECTED[2559:2048],s03_couplers_to_xbar_RDATA,s02_couplers_to_xbar_RDATA,s01_couplers_to_xbar_RDATA,s00_couplers_to_xbar_RDATA}),
        .s_axi_rlast({s12_couplers_to_xbar_RLAST,s11_couplers_to_xbar_RLAST,NLW_xbar_s_axi_rlast_UNCONNECTED[10],s09_couplers_to_xbar_RLAST,s08_couplers_to_xbar_RLAST,NLW_xbar_s_axi_rlast_UNCONNECTED[7],s06_couplers_to_xbar_RLAST,s05_couplers_to_xbar_RLAST,NLW_xbar_s_axi_rlast_UNCONNECTED[4],s03_couplers_to_xbar_RLAST,s02_couplers_to_xbar_RLAST,s01_couplers_to_xbar_RLAST,s00_couplers_to_xbar_RLAST}),
        .s_axi_rready({1'b0,s12_couplers_to_xbar_RREADY,s11_couplers_to_xbar_RREADY,1'b0,s09_couplers_to_xbar_RREADY,s08_couplers_to_xbar_RREADY,1'b0,s06_couplers_to_xbar_RREADY,s05_couplers_to_xbar_RREADY,1'b0,s03_couplers_to_xbar_RREADY,s02_couplers_to_xbar_RREADY,s01_couplers_to_xbar_RREADY,s00_couplers_to_xbar_RREADY}),
        .s_axi_rresp({s12_couplers_to_xbar_RRESP,s11_couplers_to_xbar_RRESP,NLW_xbar_s_axi_rresp_UNCONNECTED[21:20],s09_couplers_to_xbar_RRESP,s08_couplers_to_xbar_RRESP,NLW_xbar_s_axi_rresp_UNCONNECTED[15:14],s06_couplers_to_xbar_RRESP,s05_couplers_to_xbar_RRESP,NLW_xbar_s_axi_rresp_UNCONNECTED[9:8],s03_couplers_to_xbar_RRESP,s02_couplers_to_xbar_RRESP,s01_couplers_to_xbar_RRESP,s00_couplers_to_xbar_RRESP}),
        .s_axi_rvalid({s12_couplers_to_xbar_RVALID,s11_couplers_to_xbar_RVALID,NLW_xbar_s_axi_rvalid_UNCONNECTED[10],s09_couplers_to_xbar_RVALID,s08_couplers_to_xbar_RVALID,NLW_xbar_s_axi_rvalid_UNCONNECTED[7],s06_couplers_to_xbar_RVALID,s05_couplers_to_xbar_RVALID,NLW_xbar_s_axi_rvalid_UNCONNECTED[4],s03_couplers_to_xbar_RVALID,s02_couplers_to_xbar_RVALID,s01_couplers_to_xbar_RVALID,s00_couplers_to_xbar_RVALID}),
        .s_axi_wdata({s13_couplers_to_xbar_WDATA,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s11_couplers_to_xbar_WDATA,s10_couplers_to_xbar_WDATA,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s08_couplers_to_xbar_WDATA,s07_couplers_to_xbar_WDATA,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s05_couplers_to_xbar_WDATA,s04_couplers_to_xbar_WDATA,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s02_couplers_to_xbar_WDATA,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_couplers_to_xbar_WDATA}),
        .s_axi_wlast({s13_couplers_to_xbar_WLAST,1'b0,s11_couplers_to_xbar_WLAST,s10_couplers_to_xbar_WLAST,1'b0,s08_couplers_to_xbar_WLAST,s07_couplers_to_xbar_WLAST,1'b0,s05_couplers_to_xbar_WLAST,s04_couplers_to_xbar_WLAST,1'b0,s02_couplers_to_xbar_WLAST,1'b0,s00_couplers_to_xbar_WLAST}),
        .s_axi_wready({s13_couplers_to_xbar_WREADY,NLW_xbar_s_axi_wready_UNCONNECTED[12],s11_couplers_to_xbar_WREADY,s10_couplers_to_xbar_WREADY,NLW_xbar_s_axi_wready_UNCONNECTED[9],s08_couplers_to_xbar_WREADY,s07_couplers_to_xbar_WREADY,NLW_xbar_s_axi_wready_UNCONNECTED[6],s05_couplers_to_xbar_WREADY,s04_couplers_to_xbar_WREADY,NLW_xbar_s_axi_wready_UNCONNECTED[3],s02_couplers_to_xbar_WREADY,NLW_xbar_s_axi_wready_UNCONNECTED[1],s00_couplers_to_xbar_WREADY}),
        .s_axi_wstrb({s13_couplers_to_xbar_WSTRB,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s11_couplers_to_xbar_WSTRB,s10_couplers_to_xbar_WSTRB,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s08_couplers_to_xbar_WSTRB,s07_couplers_to_xbar_WSTRB,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s05_couplers_to_xbar_WSTRB,s04_couplers_to_xbar_WSTRB,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s02_couplers_to_xbar_WSTRB,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_couplers_to_xbar_WSTRB}),
        .s_axi_wvalid({s13_couplers_to_xbar_WVALID,1'b0,s11_couplers_to_xbar_WVALID,s10_couplers_to_xbar_WVALID,1'b0,s08_couplers_to_xbar_WVALID,s07_couplers_to_xbar_WVALID,1'b0,s05_couplers_to_xbar_WVALID,s04_couplers_to_xbar_WVALID,1'b0,s02_couplers_to_xbar_WVALID,1'b0,s00_couplers_to_xbar_WVALID}));
endmodule

module design_1_axis_interconnect_0_0
   (ACLK,
    ARESETN,
    M00_AXIS_ACLK,
    M00_AXIS_ARESETN,
    M00_AXIS_tdata,
    M00_AXIS_tdest,
    M00_AXIS_tid,
    M00_AXIS_tkeep,
    M00_AXIS_tlast,
    M00_AXIS_tready,
    M00_AXIS_tstrb,
    M00_AXIS_tuser,
    M00_AXIS_tvalid,
    M01_AXIS_ACLK,
    M01_AXIS_ARESETN,
    M01_AXIS_tdata,
    M01_AXIS_tdest,
    M01_AXIS_tid,
    M01_AXIS_tkeep,
    M01_AXIS_tlast,
    M01_AXIS_tready,
    M01_AXIS_tstrb,
    M01_AXIS_tuser,
    M01_AXIS_tvalid,
    M02_AXIS_ACLK,
    M02_AXIS_ARESETN,
    M02_AXIS_tdata,
    M02_AXIS_tdest,
    M02_AXIS_tid,
    M02_AXIS_tkeep,
    M02_AXIS_tlast,
    M02_AXIS_tready,
    M02_AXIS_tstrb,
    M02_AXIS_tuser,
    M02_AXIS_tvalid,
    S00_AXIS_ACLK,
    S00_AXIS_ARESETN,
    S00_AXIS_tdata,
    S00_AXIS_tdest,
    S00_AXIS_tid,
    S00_AXIS_tkeep,
    S00_AXIS_tlast,
    S00_AXIS_tready,
    S00_AXIS_tuser,
    S00_AXIS_tvalid);
  input ACLK;
  input [0:0]ARESETN;
  input M00_AXIS_ACLK;
  input [0:0]M00_AXIS_ARESETN;
  output [31:0]M00_AXIS_tdata;
  output [4:0]M00_AXIS_tdest;
  output [4:0]M00_AXIS_tid;
  output [3:0]M00_AXIS_tkeep;
  output [0:0]M00_AXIS_tlast;
  input M00_AXIS_tready;
  output [3:0]M00_AXIS_tstrb;
  output [3:0]M00_AXIS_tuser;
  output M00_AXIS_tvalid;
  input M01_AXIS_ACLK;
  input [0:0]M01_AXIS_ARESETN;
  output [31:0]M01_AXIS_tdata;
  output [4:0]M01_AXIS_tdest;
  output [4:0]M01_AXIS_tid;
  output [3:0]M01_AXIS_tkeep;
  output [0:0]M01_AXIS_tlast;
  input M01_AXIS_tready;
  output [3:0]M01_AXIS_tstrb;
  output [3:0]M01_AXIS_tuser;
  output M01_AXIS_tvalid;
  input M02_AXIS_ACLK;
  input [0:0]M02_AXIS_ARESETN;
  output [31:0]M02_AXIS_tdata;
  output [4:0]M02_AXIS_tdest;
  output [4:0]M02_AXIS_tid;
  output [3:0]M02_AXIS_tkeep;
  output [0:0]M02_AXIS_tlast;
  input M02_AXIS_tready;
  output [3:0]M02_AXIS_tstrb;
  output [3:0]M02_AXIS_tuser;
  output M02_AXIS_tvalid;
  input S00_AXIS_ACLK;
  input [0:0]S00_AXIS_ARESETN;
  input [31:0]S00_AXIS_tdata;
  input [4:0]S00_AXIS_tdest;
  input [4:0]S00_AXIS_tid;
  input [3:0]S00_AXIS_tkeep;
  input [0:0]S00_AXIS_tlast;
  output [0:0]S00_AXIS_tready;
  input [3:0]S00_AXIS_tuser;
  input [0:0]S00_AXIS_tvalid;

  wire axis_interconnect_0_ACLK_net;
  wire [0:0]axis_interconnect_0_ARESETN_net;
  wire [31:0]axis_interconnect_0_to_s00_couplers_TDATA;
  wire [4:0]axis_interconnect_0_to_s00_couplers_TDEST;
  wire [4:0]axis_interconnect_0_to_s00_couplers_TID;
  wire [3:0]axis_interconnect_0_to_s00_couplers_TKEEP;
  wire [0:0]axis_interconnect_0_to_s00_couplers_TLAST;
  wire [0:0]axis_interconnect_0_to_s00_couplers_TREADY;
  wire [3:0]axis_interconnect_0_to_s00_couplers_TUSER;
  wire [0:0]axis_interconnect_0_to_s00_couplers_TVALID;
  wire [31:0]m00_couplers_to_axis_interconnect_0_TDATA;
  wire [4:0]m00_couplers_to_axis_interconnect_0_TDEST;
  wire [4:0]m00_couplers_to_axis_interconnect_0_TID;
  wire [3:0]m00_couplers_to_axis_interconnect_0_TKEEP;
  wire [0:0]m00_couplers_to_axis_interconnect_0_TLAST;
  wire m00_couplers_to_axis_interconnect_0_TREADY;
  wire [3:0]m00_couplers_to_axis_interconnect_0_TSTRB;
  wire [3:0]m00_couplers_to_axis_interconnect_0_TUSER;
  wire m00_couplers_to_axis_interconnect_0_TVALID;
  wire [31:0]m01_couplers_to_axis_interconnect_0_TDATA;
  wire [4:0]m01_couplers_to_axis_interconnect_0_TDEST;
  wire [4:0]m01_couplers_to_axis_interconnect_0_TID;
  wire [3:0]m01_couplers_to_axis_interconnect_0_TKEEP;
  wire [0:0]m01_couplers_to_axis_interconnect_0_TLAST;
  wire m01_couplers_to_axis_interconnect_0_TREADY;
  wire [3:0]m01_couplers_to_axis_interconnect_0_TSTRB;
  wire [3:0]m01_couplers_to_axis_interconnect_0_TUSER;
  wire m01_couplers_to_axis_interconnect_0_TVALID;
  wire [31:0]m02_couplers_to_axis_interconnect_0_TDATA;
  wire [4:0]m02_couplers_to_axis_interconnect_0_TDEST;
  wire [4:0]m02_couplers_to_axis_interconnect_0_TID;
  wire [3:0]m02_couplers_to_axis_interconnect_0_TKEEP;
  wire [0:0]m02_couplers_to_axis_interconnect_0_TLAST;
  wire m02_couplers_to_axis_interconnect_0_TREADY;
  wire [3:0]m02_couplers_to_axis_interconnect_0_TSTRB;
  wire [3:0]m02_couplers_to_axis_interconnect_0_TUSER;
  wire m02_couplers_to_axis_interconnect_0_TVALID;
  wire [31:0]s00_couplers_to_xbar_TDATA;
  wire [4:0]s00_couplers_to_xbar_TDEST;
  wire [4:0]s00_couplers_to_xbar_TID;
  wire [3:0]s00_couplers_to_xbar_TKEEP;
  wire [0:0]s00_couplers_to_xbar_TLAST;
  wire [0:0]s00_couplers_to_xbar_TREADY;
  wire [3:0]s00_couplers_to_xbar_TUSER;
  wire [0:0]s00_couplers_to_xbar_TVALID;
  wire [31:0]xbar_to_m00_couplers_TDATA;
  wire [4:0]xbar_to_m00_couplers_TDEST;
  wire [4:0]xbar_to_m00_couplers_TID;
  wire [3:0]xbar_to_m00_couplers_TKEEP;
  wire [0:0]xbar_to_m00_couplers_TLAST;
  wire xbar_to_m00_couplers_TREADY;
  wire [3:0]xbar_to_m00_couplers_TUSER;
  wire [0:0]xbar_to_m00_couplers_TVALID;
  wire [63:32]xbar_to_m01_couplers_TDATA;
  wire [9:5]xbar_to_m01_couplers_TDEST;
  wire [9:5]xbar_to_m01_couplers_TID;
  wire [7:4]xbar_to_m01_couplers_TKEEP;
  wire [1:1]xbar_to_m01_couplers_TLAST;
  wire xbar_to_m01_couplers_TREADY;
  wire [7:4]xbar_to_m01_couplers_TUSER;
  wire [1:1]xbar_to_m01_couplers_TVALID;
  wire [95:64]xbar_to_m02_couplers_TDATA;
  wire [14:10]xbar_to_m02_couplers_TDEST;
  wire [14:10]xbar_to_m02_couplers_TID;
  wire [11:8]xbar_to_m02_couplers_TKEEP;
  wire [2:2]xbar_to_m02_couplers_TLAST;
  wire xbar_to_m02_couplers_TREADY;
  wire [11:8]xbar_to_m02_couplers_TUSER;
  wire [2:2]xbar_to_m02_couplers_TVALID;

  assign M00_AXIS_tdata[31:0] = m00_couplers_to_axis_interconnect_0_TDATA;
  assign M00_AXIS_tdest[4:0] = m00_couplers_to_axis_interconnect_0_TDEST;
  assign M00_AXIS_tid[4:0] = m00_couplers_to_axis_interconnect_0_TID;
  assign M00_AXIS_tkeep[3:0] = m00_couplers_to_axis_interconnect_0_TKEEP;
  assign M00_AXIS_tlast[0] = m00_couplers_to_axis_interconnect_0_TLAST;
  assign M00_AXIS_tstrb[3:0] = m00_couplers_to_axis_interconnect_0_TSTRB;
  assign M00_AXIS_tuser[3:0] = m00_couplers_to_axis_interconnect_0_TUSER;
  assign M00_AXIS_tvalid = m00_couplers_to_axis_interconnect_0_TVALID;
  assign M01_AXIS_tdata[31:0] = m01_couplers_to_axis_interconnect_0_TDATA;
  assign M01_AXIS_tdest[4:0] = m01_couplers_to_axis_interconnect_0_TDEST;
  assign M01_AXIS_tid[4:0] = m01_couplers_to_axis_interconnect_0_TID;
  assign M01_AXIS_tkeep[3:0] = m01_couplers_to_axis_interconnect_0_TKEEP;
  assign M01_AXIS_tlast[0] = m01_couplers_to_axis_interconnect_0_TLAST;
  assign M01_AXIS_tstrb[3:0] = m01_couplers_to_axis_interconnect_0_TSTRB;
  assign M01_AXIS_tuser[3:0] = m01_couplers_to_axis_interconnect_0_TUSER;
  assign M01_AXIS_tvalid = m01_couplers_to_axis_interconnect_0_TVALID;
  assign M02_AXIS_tdata[31:0] = m02_couplers_to_axis_interconnect_0_TDATA;
  assign M02_AXIS_tdest[4:0] = m02_couplers_to_axis_interconnect_0_TDEST;
  assign M02_AXIS_tid[4:0] = m02_couplers_to_axis_interconnect_0_TID;
  assign M02_AXIS_tkeep[3:0] = m02_couplers_to_axis_interconnect_0_TKEEP;
  assign M02_AXIS_tlast[0] = m02_couplers_to_axis_interconnect_0_TLAST;
  assign M02_AXIS_tstrb[3:0] = m02_couplers_to_axis_interconnect_0_TSTRB;
  assign M02_AXIS_tuser[3:0] = m02_couplers_to_axis_interconnect_0_TUSER;
  assign M02_AXIS_tvalid = m02_couplers_to_axis_interconnect_0_TVALID;
  assign S00_AXIS_tready[0] = axis_interconnect_0_to_s00_couplers_TREADY;
  assign axis_interconnect_0_ACLK_net = ACLK;
  assign axis_interconnect_0_ARESETN_net = ARESETN[0];
  assign axis_interconnect_0_to_s00_couplers_TDATA = S00_AXIS_tdata[31:0];
  assign axis_interconnect_0_to_s00_couplers_TDEST = S00_AXIS_tdest[4:0];
  assign axis_interconnect_0_to_s00_couplers_TID = S00_AXIS_tid[4:0];
  assign axis_interconnect_0_to_s00_couplers_TKEEP = S00_AXIS_tkeep[3:0];
  assign axis_interconnect_0_to_s00_couplers_TLAST = S00_AXIS_tlast[0];
  assign axis_interconnect_0_to_s00_couplers_TUSER = S00_AXIS_tuser[3:0];
  assign axis_interconnect_0_to_s00_couplers_TVALID = S00_AXIS_tvalid[0];
  assign m00_couplers_to_axis_interconnect_0_TREADY = M00_AXIS_tready;
  assign m01_couplers_to_axis_interconnect_0_TREADY = M01_AXIS_tready;
  assign m02_couplers_to_axis_interconnect_0_TREADY = M02_AXIS_tready;
  m00_couplers_imp_F63VTB m00_couplers
       (.M_AXIS_ACLK(axis_interconnect_0_ACLK_net),
        .M_AXIS_ARESETN(axis_interconnect_0_ARESETN_net),
        .M_AXIS_tdata(m00_couplers_to_axis_interconnect_0_TDATA),
        .M_AXIS_tdest(m00_couplers_to_axis_interconnect_0_TDEST),
        .M_AXIS_tid(m00_couplers_to_axis_interconnect_0_TID),
        .M_AXIS_tkeep(m00_couplers_to_axis_interconnect_0_TKEEP),
        .M_AXIS_tlast(m00_couplers_to_axis_interconnect_0_TLAST),
        .M_AXIS_tready(m00_couplers_to_axis_interconnect_0_TREADY),
        .M_AXIS_tstrb(m00_couplers_to_axis_interconnect_0_TSTRB),
        .M_AXIS_tuser(m00_couplers_to_axis_interconnect_0_TUSER),
        .M_AXIS_tvalid(m00_couplers_to_axis_interconnect_0_TVALID),
        .S_AXIS_ACLK(axis_interconnect_0_ACLK_net),
        .S_AXIS_ARESETN(axis_interconnect_0_ARESETN_net),
        .S_AXIS_tdata(xbar_to_m00_couplers_TDATA),
        .S_AXIS_tdest(xbar_to_m00_couplers_TDEST),
        .S_AXIS_tid(xbar_to_m00_couplers_TID),
        .S_AXIS_tkeep(xbar_to_m00_couplers_TKEEP),
        .S_AXIS_tlast(xbar_to_m00_couplers_TLAST),
        .S_AXIS_tready(xbar_to_m00_couplers_TREADY),
        .S_AXIS_tuser(xbar_to_m00_couplers_TUSER),
        .S_AXIS_tvalid(xbar_to_m00_couplers_TVALID));
  m01_couplers_imp_1OHHHDA m01_couplers
       (.M_AXIS_ACLK(axis_interconnect_0_ACLK_net),
        .M_AXIS_ARESETN(axis_interconnect_0_ARESETN_net),
        .M_AXIS_tdata(m01_couplers_to_axis_interconnect_0_TDATA),
        .M_AXIS_tdest(m01_couplers_to_axis_interconnect_0_TDEST),
        .M_AXIS_tid(m01_couplers_to_axis_interconnect_0_TID),
        .M_AXIS_tkeep(m01_couplers_to_axis_interconnect_0_TKEEP),
        .M_AXIS_tlast(m01_couplers_to_axis_interconnect_0_TLAST),
        .M_AXIS_tready(m01_couplers_to_axis_interconnect_0_TREADY),
        .M_AXIS_tstrb(m01_couplers_to_axis_interconnect_0_TSTRB),
        .M_AXIS_tuser(m01_couplers_to_axis_interconnect_0_TUSER),
        .M_AXIS_tvalid(m01_couplers_to_axis_interconnect_0_TVALID),
        .S_AXIS_ACLK(axis_interconnect_0_ACLK_net),
        .S_AXIS_ARESETN(axis_interconnect_0_ARESETN_net),
        .S_AXIS_tdata(xbar_to_m01_couplers_TDATA),
        .S_AXIS_tdest(xbar_to_m01_couplers_TDEST),
        .S_AXIS_tid(xbar_to_m01_couplers_TID),
        .S_AXIS_tkeep(xbar_to_m01_couplers_TKEEP),
        .S_AXIS_tlast(xbar_to_m01_couplers_TLAST),
        .S_AXIS_tready(xbar_to_m01_couplers_TREADY),
        .S_AXIS_tuser(xbar_to_m01_couplers_TUSER),
        .S_AXIS_tvalid(xbar_to_m01_couplers_TVALID));
  m02_couplers_imp_EA4A4S m02_couplers
       (.M_AXIS_ACLK(axis_interconnect_0_ACLK_net),
        .M_AXIS_ARESETN(axis_interconnect_0_ARESETN_net),
        .M_AXIS_tdata(m02_couplers_to_axis_interconnect_0_TDATA),
        .M_AXIS_tdest(m02_couplers_to_axis_interconnect_0_TDEST),
        .M_AXIS_tid(m02_couplers_to_axis_interconnect_0_TID),
        .M_AXIS_tkeep(m02_couplers_to_axis_interconnect_0_TKEEP),
        .M_AXIS_tlast(m02_couplers_to_axis_interconnect_0_TLAST),
        .M_AXIS_tready(m02_couplers_to_axis_interconnect_0_TREADY),
        .M_AXIS_tstrb(m02_couplers_to_axis_interconnect_0_TSTRB),
        .M_AXIS_tuser(m02_couplers_to_axis_interconnect_0_TUSER),
        .M_AXIS_tvalid(m02_couplers_to_axis_interconnect_0_TVALID),
        .S_AXIS_ACLK(axis_interconnect_0_ACLK_net),
        .S_AXIS_ARESETN(axis_interconnect_0_ARESETN_net),
        .S_AXIS_tdata(xbar_to_m02_couplers_TDATA),
        .S_AXIS_tdest(xbar_to_m02_couplers_TDEST),
        .S_AXIS_tid(xbar_to_m02_couplers_TID),
        .S_AXIS_tkeep(xbar_to_m02_couplers_TKEEP),
        .S_AXIS_tlast(xbar_to_m02_couplers_TLAST),
        .S_AXIS_tready(xbar_to_m02_couplers_TREADY),
        .S_AXIS_tuser(xbar_to_m02_couplers_TUSER),
        .S_AXIS_tvalid(xbar_to_m02_couplers_TVALID));
  s00_couplers_imp_1LLE45P s00_couplers
       (.M_AXIS_ACLK(axis_interconnect_0_ACLK_net),
        .M_AXIS_ARESETN(axis_interconnect_0_ARESETN_net),
        .M_AXIS_tdata(s00_couplers_to_xbar_TDATA),
        .M_AXIS_tdest(s00_couplers_to_xbar_TDEST),
        .M_AXIS_tid(s00_couplers_to_xbar_TID),
        .M_AXIS_tkeep(s00_couplers_to_xbar_TKEEP),
        .M_AXIS_tlast(s00_couplers_to_xbar_TLAST),
        .M_AXIS_tready(s00_couplers_to_xbar_TREADY),
        .M_AXIS_tuser(s00_couplers_to_xbar_TUSER),
        .M_AXIS_tvalid(s00_couplers_to_xbar_TVALID),
        .S_AXIS_ACLK(axis_interconnect_0_ACLK_net),
        .S_AXIS_ARESETN(axis_interconnect_0_ARESETN_net),
        .S_AXIS_tdata(axis_interconnect_0_to_s00_couplers_TDATA),
        .S_AXIS_tdest(axis_interconnect_0_to_s00_couplers_TDEST),
        .S_AXIS_tid(axis_interconnect_0_to_s00_couplers_TID),
        .S_AXIS_tkeep(axis_interconnect_0_to_s00_couplers_TKEEP),
        .S_AXIS_tlast(axis_interconnect_0_to_s00_couplers_TLAST),
        .S_AXIS_tready(axis_interconnect_0_to_s00_couplers_TREADY),
        .S_AXIS_tuser(axis_interconnect_0_to_s00_couplers_TUSER),
        .S_AXIS_tvalid(axis_interconnect_0_to_s00_couplers_TVALID));
  design_1_xbar_2 xbar
       (.aclk(axis_interconnect_0_ACLK_net),
        .aresetn(axis_interconnect_0_ARESETN_net),
        .m_axis_tdata({xbar_to_m02_couplers_TDATA,xbar_to_m01_couplers_TDATA,xbar_to_m00_couplers_TDATA}),
        .m_axis_tdest({xbar_to_m02_couplers_TDEST,xbar_to_m01_couplers_TDEST,xbar_to_m00_couplers_TDEST}),
        .m_axis_tid({xbar_to_m02_couplers_TID,xbar_to_m01_couplers_TID,xbar_to_m00_couplers_TID}),
        .m_axis_tkeep({xbar_to_m02_couplers_TKEEP,xbar_to_m01_couplers_TKEEP,xbar_to_m00_couplers_TKEEP}),
        .m_axis_tlast({xbar_to_m02_couplers_TLAST,xbar_to_m01_couplers_TLAST,xbar_to_m00_couplers_TLAST}),
        .m_axis_tready({xbar_to_m02_couplers_TREADY,xbar_to_m01_couplers_TREADY,xbar_to_m00_couplers_TREADY}),
        .m_axis_tuser({xbar_to_m02_couplers_TUSER,xbar_to_m01_couplers_TUSER,xbar_to_m00_couplers_TUSER}),
        .m_axis_tvalid({xbar_to_m02_couplers_TVALID,xbar_to_m01_couplers_TVALID,xbar_to_m00_couplers_TVALID}),
        .s_axis_tdata(s00_couplers_to_xbar_TDATA),
        .s_axis_tdest(s00_couplers_to_xbar_TDEST),
        .s_axis_tid(s00_couplers_to_xbar_TID),
        .s_axis_tkeep(s00_couplers_to_xbar_TKEEP),
        .s_axis_tlast(s00_couplers_to_xbar_TLAST),
        .s_axis_tready(s00_couplers_to_xbar_TREADY),
        .s_axis_tuser(s00_couplers_to_xbar_TUSER),
        .s_axis_tvalid(s00_couplers_to_xbar_TVALID));
endmodule

module design_1_axis_interconnect_0_1
   (ACLK,
    ARESETN,
    M00_AXIS_ACLK,
    M00_AXIS_ARESETN,
    M00_AXIS_tdata,
    M00_AXIS_tdest,
    M00_AXIS_tid,
    M00_AXIS_tkeep,
    M00_AXIS_tlast,
    M00_AXIS_tready,
    M00_AXIS_tstrb,
    M00_AXIS_tuser,
    M00_AXIS_tvalid,
    M01_AXIS_ACLK,
    M01_AXIS_ARESETN,
    M01_AXIS_tdata,
    M01_AXIS_tdest,
    M01_AXIS_tid,
    M01_AXIS_tkeep,
    M01_AXIS_tlast,
    M01_AXIS_tready,
    M01_AXIS_tstrb,
    M01_AXIS_tuser,
    M01_AXIS_tvalid,
    M02_AXIS_ACLK,
    M02_AXIS_ARESETN,
    M02_AXIS_tdata,
    M02_AXIS_tdest,
    M02_AXIS_tid,
    M02_AXIS_tkeep,
    M02_AXIS_tlast,
    M02_AXIS_tready,
    M02_AXIS_tstrb,
    M02_AXIS_tuser,
    M02_AXIS_tvalid,
    S00_AXIS_ACLK,
    S00_AXIS_ARESETN,
    S00_AXIS_tdata,
    S00_AXIS_tdest,
    S00_AXIS_tid,
    S00_AXIS_tkeep,
    S00_AXIS_tlast,
    S00_AXIS_tready,
    S00_AXIS_tuser,
    S00_AXIS_tvalid);
  input ACLK;
  input [0:0]ARESETN;
  input M00_AXIS_ACLK;
  input [0:0]M00_AXIS_ARESETN;
  output [31:0]M00_AXIS_tdata;
  output [4:0]M00_AXIS_tdest;
  output [4:0]M00_AXIS_tid;
  output [3:0]M00_AXIS_tkeep;
  output [0:0]M00_AXIS_tlast;
  input M00_AXIS_tready;
  output [3:0]M00_AXIS_tstrb;
  output [3:0]M00_AXIS_tuser;
  output M00_AXIS_tvalid;
  input M01_AXIS_ACLK;
  input [0:0]M01_AXIS_ARESETN;
  output [31:0]M01_AXIS_tdata;
  output [4:0]M01_AXIS_tdest;
  output [4:0]M01_AXIS_tid;
  output [3:0]M01_AXIS_tkeep;
  output [0:0]M01_AXIS_tlast;
  input M01_AXIS_tready;
  output [3:0]M01_AXIS_tstrb;
  output [3:0]M01_AXIS_tuser;
  output M01_AXIS_tvalid;
  input M02_AXIS_ACLK;
  input [0:0]M02_AXIS_ARESETN;
  output [31:0]M02_AXIS_tdata;
  output [4:0]M02_AXIS_tdest;
  output [4:0]M02_AXIS_tid;
  output [3:0]M02_AXIS_tkeep;
  output [0:0]M02_AXIS_tlast;
  input M02_AXIS_tready;
  output [3:0]M02_AXIS_tstrb;
  output [3:0]M02_AXIS_tuser;
  output M02_AXIS_tvalid;
  input S00_AXIS_ACLK;
  input [0:0]S00_AXIS_ARESETN;
  input [31:0]S00_AXIS_tdata;
  input [4:0]S00_AXIS_tdest;
  input [4:0]S00_AXIS_tid;
  input [3:0]S00_AXIS_tkeep;
  input [0:0]S00_AXIS_tlast;
  output [0:0]S00_AXIS_tready;
  input [3:0]S00_AXIS_tuser;
  input [0:0]S00_AXIS_tvalid;

  wire axis_interconnect_1_ACLK_net;
  wire [0:0]axis_interconnect_1_ARESETN_net;
  wire [31:0]axis_interconnect_1_to_s00_couplers_TDATA;
  wire [4:0]axis_interconnect_1_to_s00_couplers_TDEST;
  wire [4:0]axis_interconnect_1_to_s00_couplers_TID;
  wire [3:0]axis_interconnect_1_to_s00_couplers_TKEEP;
  wire [0:0]axis_interconnect_1_to_s00_couplers_TLAST;
  wire [0:0]axis_interconnect_1_to_s00_couplers_TREADY;
  wire [3:0]axis_interconnect_1_to_s00_couplers_TUSER;
  wire [0:0]axis_interconnect_1_to_s00_couplers_TVALID;
  wire [31:0]m00_couplers_to_axis_interconnect_1_TDATA;
  wire [4:0]m00_couplers_to_axis_interconnect_1_TDEST;
  wire [4:0]m00_couplers_to_axis_interconnect_1_TID;
  wire [3:0]m00_couplers_to_axis_interconnect_1_TKEEP;
  wire [0:0]m00_couplers_to_axis_interconnect_1_TLAST;
  wire m00_couplers_to_axis_interconnect_1_TREADY;
  wire [3:0]m00_couplers_to_axis_interconnect_1_TSTRB;
  wire [3:0]m00_couplers_to_axis_interconnect_1_TUSER;
  wire m00_couplers_to_axis_interconnect_1_TVALID;
  wire [31:0]m01_couplers_to_axis_interconnect_1_TDATA;
  wire [4:0]m01_couplers_to_axis_interconnect_1_TDEST;
  wire [4:0]m01_couplers_to_axis_interconnect_1_TID;
  wire [3:0]m01_couplers_to_axis_interconnect_1_TKEEP;
  wire [0:0]m01_couplers_to_axis_interconnect_1_TLAST;
  wire m01_couplers_to_axis_interconnect_1_TREADY;
  wire [3:0]m01_couplers_to_axis_interconnect_1_TSTRB;
  wire [3:0]m01_couplers_to_axis_interconnect_1_TUSER;
  wire m01_couplers_to_axis_interconnect_1_TVALID;
  wire [31:0]m02_couplers_to_axis_interconnect_1_TDATA;
  wire [4:0]m02_couplers_to_axis_interconnect_1_TDEST;
  wire [4:0]m02_couplers_to_axis_interconnect_1_TID;
  wire [3:0]m02_couplers_to_axis_interconnect_1_TKEEP;
  wire [0:0]m02_couplers_to_axis_interconnect_1_TLAST;
  wire m02_couplers_to_axis_interconnect_1_TREADY;
  wire [3:0]m02_couplers_to_axis_interconnect_1_TSTRB;
  wire [3:0]m02_couplers_to_axis_interconnect_1_TUSER;
  wire m02_couplers_to_axis_interconnect_1_TVALID;
  wire [31:0]s00_couplers_to_xbar_TDATA;
  wire [4:0]s00_couplers_to_xbar_TDEST;
  wire [4:0]s00_couplers_to_xbar_TID;
  wire [3:0]s00_couplers_to_xbar_TKEEP;
  wire [0:0]s00_couplers_to_xbar_TLAST;
  wire [0:0]s00_couplers_to_xbar_TREADY;
  wire [3:0]s00_couplers_to_xbar_TUSER;
  wire [0:0]s00_couplers_to_xbar_TVALID;
  wire [31:0]xbar_to_m00_couplers_TDATA;
  wire [4:0]xbar_to_m00_couplers_TDEST;
  wire [4:0]xbar_to_m00_couplers_TID;
  wire [3:0]xbar_to_m00_couplers_TKEEP;
  wire [0:0]xbar_to_m00_couplers_TLAST;
  wire xbar_to_m00_couplers_TREADY;
  wire [3:0]xbar_to_m00_couplers_TUSER;
  wire [0:0]xbar_to_m00_couplers_TVALID;
  wire [63:32]xbar_to_m01_couplers_TDATA;
  wire [9:5]xbar_to_m01_couplers_TDEST;
  wire [9:5]xbar_to_m01_couplers_TID;
  wire [7:4]xbar_to_m01_couplers_TKEEP;
  wire [1:1]xbar_to_m01_couplers_TLAST;
  wire xbar_to_m01_couplers_TREADY;
  wire [7:4]xbar_to_m01_couplers_TUSER;
  wire [1:1]xbar_to_m01_couplers_TVALID;
  wire [95:64]xbar_to_m02_couplers_TDATA;
  wire [14:10]xbar_to_m02_couplers_TDEST;
  wire [14:10]xbar_to_m02_couplers_TID;
  wire [11:8]xbar_to_m02_couplers_TKEEP;
  wire [2:2]xbar_to_m02_couplers_TLAST;
  wire xbar_to_m02_couplers_TREADY;
  wire [11:8]xbar_to_m02_couplers_TUSER;
  wire [2:2]xbar_to_m02_couplers_TVALID;

  assign M00_AXIS_tdata[31:0] = m00_couplers_to_axis_interconnect_1_TDATA;
  assign M00_AXIS_tdest[4:0] = m00_couplers_to_axis_interconnect_1_TDEST;
  assign M00_AXIS_tid[4:0] = m00_couplers_to_axis_interconnect_1_TID;
  assign M00_AXIS_tkeep[3:0] = m00_couplers_to_axis_interconnect_1_TKEEP;
  assign M00_AXIS_tlast[0] = m00_couplers_to_axis_interconnect_1_TLAST;
  assign M00_AXIS_tstrb[3:0] = m00_couplers_to_axis_interconnect_1_TSTRB;
  assign M00_AXIS_tuser[3:0] = m00_couplers_to_axis_interconnect_1_TUSER;
  assign M00_AXIS_tvalid = m00_couplers_to_axis_interconnect_1_TVALID;
  assign M01_AXIS_tdata[31:0] = m01_couplers_to_axis_interconnect_1_TDATA;
  assign M01_AXIS_tdest[4:0] = m01_couplers_to_axis_interconnect_1_TDEST;
  assign M01_AXIS_tid[4:0] = m01_couplers_to_axis_interconnect_1_TID;
  assign M01_AXIS_tkeep[3:0] = m01_couplers_to_axis_interconnect_1_TKEEP;
  assign M01_AXIS_tlast[0] = m01_couplers_to_axis_interconnect_1_TLAST;
  assign M01_AXIS_tstrb[3:0] = m01_couplers_to_axis_interconnect_1_TSTRB;
  assign M01_AXIS_tuser[3:0] = m01_couplers_to_axis_interconnect_1_TUSER;
  assign M01_AXIS_tvalid = m01_couplers_to_axis_interconnect_1_TVALID;
  assign M02_AXIS_tdata[31:0] = m02_couplers_to_axis_interconnect_1_TDATA;
  assign M02_AXIS_tdest[4:0] = m02_couplers_to_axis_interconnect_1_TDEST;
  assign M02_AXIS_tid[4:0] = m02_couplers_to_axis_interconnect_1_TID;
  assign M02_AXIS_tkeep[3:0] = m02_couplers_to_axis_interconnect_1_TKEEP;
  assign M02_AXIS_tlast[0] = m02_couplers_to_axis_interconnect_1_TLAST;
  assign M02_AXIS_tstrb[3:0] = m02_couplers_to_axis_interconnect_1_TSTRB;
  assign M02_AXIS_tuser[3:0] = m02_couplers_to_axis_interconnect_1_TUSER;
  assign M02_AXIS_tvalid = m02_couplers_to_axis_interconnect_1_TVALID;
  assign S00_AXIS_tready[0] = axis_interconnect_1_to_s00_couplers_TREADY;
  assign axis_interconnect_1_ACLK_net = ACLK;
  assign axis_interconnect_1_ARESETN_net = ARESETN[0];
  assign axis_interconnect_1_to_s00_couplers_TDATA = S00_AXIS_tdata[31:0];
  assign axis_interconnect_1_to_s00_couplers_TDEST = S00_AXIS_tdest[4:0];
  assign axis_interconnect_1_to_s00_couplers_TID = S00_AXIS_tid[4:0];
  assign axis_interconnect_1_to_s00_couplers_TKEEP = S00_AXIS_tkeep[3:0];
  assign axis_interconnect_1_to_s00_couplers_TLAST = S00_AXIS_tlast[0];
  assign axis_interconnect_1_to_s00_couplers_TUSER = S00_AXIS_tuser[3:0];
  assign axis_interconnect_1_to_s00_couplers_TVALID = S00_AXIS_tvalid[0];
  assign m00_couplers_to_axis_interconnect_1_TREADY = M00_AXIS_tready;
  assign m01_couplers_to_axis_interconnect_1_TREADY = M01_AXIS_tready;
  assign m02_couplers_to_axis_interconnect_1_TREADY = M02_AXIS_tready;
  m00_couplers_imp_9ASDFZ m00_couplers
       (.M_AXIS_ACLK(axis_interconnect_1_ACLK_net),
        .M_AXIS_ARESETN(axis_interconnect_1_ARESETN_net),
        .M_AXIS_tdata(m00_couplers_to_axis_interconnect_1_TDATA),
        .M_AXIS_tdest(m00_couplers_to_axis_interconnect_1_TDEST),
        .M_AXIS_tid(m00_couplers_to_axis_interconnect_1_TID),
        .M_AXIS_tkeep(m00_couplers_to_axis_interconnect_1_TKEEP),
        .M_AXIS_tlast(m00_couplers_to_axis_interconnect_1_TLAST),
        .M_AXIS_tready(m00_couplers_to_axis_interconnect_1_TREADY),
        .M_AXIS_tstrb(m00_couplers_to_axis_interconnect_1_TSTRB),
        .M_AXIS_tuser(m00_couplers_to_axis_interconnect_1_TUSER),
        .M_AXIS_tvalid(m00_couplers_to_axis_interconnect_1_TVALID),
        .S_AXIS_ACLK(axis_interconnect_1_ACLK_net),
        .S_AXIS_ARESETN(axis_interconnect_1_ARESETN_net),
        .S_AXIS_tdata(xbar_to_m00_couplers_TDATA),
        .S_AXIS_tdest(xbar_to_m00_couplers_TDEST),
        .S_AXIS_tid(xbar_to_m00_couplers_TID),
        .S_AXIS_tkeep(xbar_to_m00_couplers_TKEEP),
        .S_AXIS_tlast(xbar_to_m00_couplers_TLAST),
        .S_AXIS_tready(xbar_to_m00_couplers_TREADY),
        .S_AXIS_tuser(xbar_to_m00_couplers_TUSER),
        .S_AXIS_tvalid(xbar_to_m00_couplers_TVALID));
  m01_couplers_imp_1LE2BCE m01_couplers
       (.M_AXIS_ACLK(axis_interconnect_1_ACLK_net),
        .M_AXIS_ARESETN(axis_interconnect_1_ARESETN_net),
        .M_AXIS_tdata(m01_couplers_to_axis_interconnect_1_TDATA),
        .M_AXIS_tdest(m01_couplers_to_axis_interconnect_1_TDEST),
        .M_AXIS_tid(m01_couplers_to_axis_interconnect_1_TID),
        .M_AXIS_tkeep(m01_couplers_to_axis_interconnect_1_TKEEP),
        .M_AXIS_tlast(m01_couplers_to_axis_interconnect_1_TLAST),
        .M_AXIS_tready(m01_couplers_to_axis_interconnect_1_TREADY),
        .M_AXIS_tstrb(m01_couplers_to_axis_interconnect_1_TSTRB),
        .M_AXIS_tuser(m01_couplers_to_axis_interconnect_1_TUSER),
        .M_AXIS_tvalid(m01_couplers_to_axis_interconnect_1_TVALID),
        .S_AXIS_ACLK(axis_interconnect_1_ACLK_net),
        .S_AXIS_ARESETN(axis_interconnect_1_ARESETN_net),
        .S_AXIS_tdata(xbar_to_m01_couplers_TDATA),
        .S_AXIS_tdest(xbar_to_m01_couplers_TDEST),
        .S_AXIS_tid(xbar_to_m01_couplers_TID),
        .S_AXIS_tkeep(xbar_to_m01_couplers_TKEEP),
        .S_AXIS_tlast(xbar_to_m01_couplers_TLAST),
        .S_AXIS_tready(xbar_to_m01_couplers_TREADY),
        .S_AXIS_tuser(xbar_to_m01_couplers_TUSER),
        .S_AXIS_tvalid(xbar_to_m01_couplers_TVALID));
  m02_couplers_imp_A2PZM4 m02_couplers
       (.M_AXIS_ACLK(axis_interconnect_1_ACLK_net),
        .M_AXIS_ARESETN(axis_interconnect_1_ARESETN_net),
        .M_AXIS_tdata(m02_couplers_to_axis_interconnect_1_TDATA),
        .M_AXIS_tdest(m02_couplers_to_axis_interconnect_1_TDEST),
        .M_AXIS_tid(m02_couplers_to_axis_interconnect_1_TID),
        .M_AXIS_tkeep(m02_couplers_to_axis_interconnect_1_TKEEP),
        .M_AXIS_tlast(m02_couplers_to_axis_interconnect_1_TLAST),
        .M_AXIS_tready(m02_couplers_to_axis_interconnect_1_TREADY),
        .M_AXIS_tstrb(m02_couplers_to_axis_interconnect_1_TSTRB),
        .M_AXIS_tuser(m02_couplers_to_axis_interconnect_1_TUSER),
        .M_AXIS_tvalid(m02_couplers_to_axis_interconnect_1_TVALID),
        .S_AXIS_ACLK(axis_interconnect_1_ACLK_net),
        .S_AXIS_ARESETN(axis_interconnect_1_ARESETN_net),
        .S_AXIS_tdata(xbar_to_m02_couplers_TDATA),
        .S_AXIS_tdest(xbar_to_m02_couplers_TDEST),
        .S_AXIS_tid(xbar_to_m02_couplers_TID),
        .S_AXIS_tkeep(xbar_to_m02_couplers_TKEEP),
        .S_AXIS_tlast(xbar_to_m02_couplers_TLAST),
        .S_AXIS_tready(xbar_to_m02_couplers_TREADY),
        .S_AXIS_tuser(xbar_to_m02_couplers_TUSER),
        .S_AXIS_tvalid(xbar_to_m02_couplers_TVALID));
  s00_couplers_imp_1O4UG5P s00_couplers
       (.M_AXIS_ACLK(axis_interconnect_1_ACLK_net),
        .M_AXIS_ARESETN(axis_interconnect_1_ARESETN_net),
        .M_AXIS_tdata(s00_couplers_to_xbar_TDATA),
        .M_AXIS_tdest(s00_couplers_to_xbar_TDEST),
        .M_AXIS_tid(s00_couplers_to_xbar_TID),
        .M_AXIS_tkeep(s00_couplers_to_xbar_TKEEP),
        .M_AXIS_tlast(s00_couplers_to_xbar_TLAST),
        .M_AXIS_tready(s00_couplers_to_xbar_TREADY),
        .M_AXIS_tuser(s00_couplers_to_xbar_TUSER),
        .M_AXIS_tvalid(s00_couplers_to_xbar_TVALID),
        .S_AXIS_ACLK(axis_interconnect_1_ACLK_net),
        .S_AXIS_ARESETN(axis_interconnect_1_ARESETN_net),
        .S_AXIS_tdata(axis_interconnect_1_to_s00_couplers_TDATA),
        .S_AXIS_tdest(axis_interconnect_1_to_s00_couplers_TDEST),
        .S_AXIS_tid(axis_interconnect_1_to_s00_couplers_TID),
        .S_AXIS_tkeep(axis_interconnect_1_to_s00_couplers_TKEEP),
        .S_AXIS_tlast(axis_interconnect_1_to_s00_couplers_TLAST),
        .S_AXIS_tready(axis_interconnect_1_to_s00_couplers_TREADY),
        .S_AXIS_tuser(axis_interconnect_1_to_s00_couplers_TUSER),
        .S_AXIS_tvalid(axis_interconnect_1_to_s00_couplers_TVALID));
  design_1_xbar_3 xbar
       (.aclk(axis_interconnect_1_ACLK_net),
        .aresetn(axis_interconnect_1_ARESETN_net),
        .m_axis_tdata({xbar_to_m02_couplers_TDATA,xbar_to_m01_couplers_TDATA,xbar_to_m00_couplers_TDATA}),
        .m_axis_tdest({xbar_to_m02_couplers_TDEST,xbar_to_m01_couplers_TDEST,xbar_to_m00_couplers_TDEST}),
        .m_axis_tid({xbar_to_m02_couplers_TID,xbar_to_m01_couplers_TID,xbar_to_m00_couplers_TID}),
        .m_axis_tkeep({xbar_to_m02_couplers_TKEEP,xbar_to_m01_couplers_TKEEP,xbar_to_m00_couplers_TKEEP}),
        .m_axis_tlast({xbar_to_m02_couplers_TLAST,xbar_to_m01_couplers_TLAST,xbar_to_m00_couplers_TLAST}),
        .m_axis_tready({xbar_to_m02_couplers_TREADY,xbar_to_m01_couplers_TREADY,xbar_to_m00_couplers_TREADY}),
        .m_axis_tuser({xbar_to_m02_couplers_TUSER,xbar_to_m01_couplers_TUSER,xbar_to_m00_couplers_TUSER}),
        .m_axis_tvalid({xbar_to_m02_couplers_TVALID,xbar_to_m01_couplers_TVALID,xbar_to_m00_couplers_TVALID}),
        .s_axis_tdata(s00_couplers_to_xbar_TDATA),
        .s_axis_tdest(s00_couplers_to_xbar_TDEST),
        .s_axis_tid(s00_couplers_to_xbar_TID),
        .s_axis_tkeep(s00_couplers_to_xbar_TKEEP),
        .s_axis_tlast(s00_couplers_to_xbar_TLAST),
        .s_axis_tready(s00_couplers_to_xbar_TREADY),
        .s_axis_tuser(s00_couplers_to_xbar_TUSER),
        .s_axis_tvalid(s00_couplers_to_xbar_TVALID));
endmodule

module design_1_axis_interconnect_0_2
   (ACLK,
    ARESETN,
    M00_AXIS_ACLK,
    M00_AXIS_ARESETN,
    M00_AXIS_tdata,
    M00_AXIS_tdest,
    M00_AXIS_tid,
    M00_AXIS_tkeep,
    M00_AXIS_tlast,
    M00_AXIS_tready,
    M00_AXIS_tstrb,
    M00_AXIS_tuser,
    M00_AXIS_tvalid,
    M01_AXIS_ACLK,
    M01_AXIS_ARESETN,
    M01_AXIS_tdata,
    M01_AXIS_tdest,
    M01_AXIS_tid,
    M01_AXIS_tkeep,
    M01_AXIS_tlast,
    M01_AXIS_tready,
    M01_AXIS_tstrb,
    M01_AXIS_tuser,
    M01_AXIS_tvalid,
    M02_AXIS_ACLK,
    M02_AXIS_ARESETN,
    M02_AXIS_tdata,
    M02_AXIS_tdest,
    M02_AXIS_tid,
    M02_AXIS_tkeep,
    M02_AXIS_tlast,
    M02_AXIS_tready,
    M02_AXIS_tstrb,
    M02_AXIS_tuser,
    M02_AXIS_tvalid,
    S00_AXIS_ACLK,
    S00_AXIS_ARESETN,
    S00_AXIS_tdata,
    S00_AXIS_tdest,
    S00_AXIS_tid,
    S00_AXIS_tkeep,
    S00_AXIS_tlast,
    S00_AXIS_tready,
    S00_AXIS_tuser,
    S00_AXIS_tvalid);
  input ACLK;
  input [0:0]ARESETN;
  input M00_AXIS_ACLK;
  input [0:0]M00_AXIS_ARESETN;
  output [31:0]M00_AXIS_tdata;
  output [4:0]M00_AXIS_tdest;
  output [4:0]M00_AXIS_tid;
  output [3:0]M00_AXIS_tkeep;
  output [0:0]M00_AXIS_tlast;
  input M00_AXIS_tready;
  output [3:0]M00_AXIS_tstrb;
  output [3:0]M00_AXIS_tuser;
  output M00_AXIS_tvalid;
  input M01_AXIS_ACLK;
  input [0:0]M01_AXIS_ARESETN;
  output [31:0]M01_AXIS_tdata;
  output [4:0]M01_AXIS_tdest;
  output [4:0]M01_AXIS_tid;
  output [3:0]M01_AXIS_tkeep;
  output [0:0]M01_AXIS_tlast;
  input M01_AXIS_tready;
  output [3:0]M01_AXIS_tstrb;
  output [3:0]M01_AXIS_tuser;
  output M01_AXIS_tvalid;
  input M02_AXIS_ACLK;
  input [0:0]M02_AXIS_ARESETN;
  output [31:0]M02_AXIS_tdata;
  output [4:0]M02_AXIS_tdest;
  output [4:0]M02_AXIS_tid;
  output [3:0]M02_AXIS_tkeep;
  output [0:0]M02_AXIS_tlast;
  input M02_AXIS_tready;
  output [3:0]M02_AXIS_tstrb;
  output [3:0]M02_AXIS_tuser;
  output M02_AXIS_tvalid;
  input S00_AXIS_ACLK;
  input [0:0]S00_AXIS_ARESETN;
  input [31:0]S00_AXIS_tdata;
  input [4:0]S00_AXIS_tdest;
  input [4:0]S00_AXIS_tid;
  input [3:0]S00_AXIS_tkeep;
  input [0:0]S00_AXIS_tlast;
  output [0:0]S00_AXIS_tready;
  input [3:0]S00_AXIS_tuser;
  input [0:0]S00_AXIS_tvalid;

  wire axis_interconnect_2_ACLK_net;
  wire [0:0]axis_interconnect_2_ARESETN_net;
  wire [31:0]axis_interconnect_2_to_s00_couplers_TDATA;
  wire [4:0]axis_interconnect_2_to_s00_couplers_TDEST;
  wire [4:0]axis_interconnect_2_to_s00_couplers_TID;
  wire [3:0]axis_interconnect_2_to_s00_couplers_TKEEP;
  wire [0:0]axis_interconnect_2_to_s00_couplers_TLAST;
  wire [0:0]axis_interconnect_2_to_s00_couplers_TREADY;
  wire [3:0]axis_interconnect_2_to_s00_couplers_TUSER;
  wire [0:0]axis_interconnect_2_to_s00_couplers_TVALID;
  wire [31:0]m00_couplers_to_axis_interconnect_2_TDATA;
  wire [4:0]m00_couplers_to_axis_interconnect_2_TDEST;
  wire [4:0]m00_couplers_to_axis_interconnect_2_TID;
  wire [3:0]m00_couplers_to_axis_interconnect_2_TKEEP;
  wire [0:0]m00_couplers_to_axis_interconnect_2_TLAST;
  wire m00_couplers_to_axis_interconnect_2_TREADY;
  wire [3:0]m00_couplers_to_axis_interconnect_2_TSTRB;
  wire [3:0]m00_couplers_to_axis_interconnect_2_TUSER;
  wire m00_couplers_to_axis_interconnect_2_TVALID;
  wire [31:0]m01_couplers_to_axis_interconnect_2_TDATA;
  wire [4:0]m01_couplers_to_axis_interconnect_2_TDEST;
  wire [4:0]m01_couplers_to_axis_interconnect_2_TID;
  wire [3:0]m01_couplers_to_axis_interconnect_2_TKEEP;
  wire [0:0]m01_couplers_to_axis_interconnect_2_TLAST;
  wire m01_couplers_to_axis_interconnect_2_TREADY;
  wire [3:0]m01_couplers_to_axis_interconnect_2_TSTRB;
  wire [3:0]m01_couplers_to_axis_interconnect_2_TUSER;
  wire m01_couplers_to_axis_interconnect_2_TVALID;
  wire [31:0]m02_couplers_to_axis_interconnect_2_TDATA;
  wire [4:0]m02_couplers_to_axis_interconnect_2_TDEST;
  wire [4:0]m02_couplers_to_axis_interconnect_2_TID;
  wire [3:0]m02_couplers_to_axis_interconnect_2_TKEEP;
  wire [0:0]m02_couplers_to_axis_interconnect_2_TLAST;
  wire m02_couplers_to_axis_interconnect_2_TREADY;
  wire [3:0]m02_couplers_to_axis_interconnect_2_TSTRB;
  wire [3:0]m02_couplers_to_axis_interconnect_2_TUSER;
  wire m02_couplers_to_axis_interconnect_2_TVALID;
  wire [31:0]s00_couplers_to_xbar_TDATA;
  wire [4:0]s00_couplers_to_xbar_TDEST;
  wire [4:0]s00_couplers_to_xbar_TID;
  wire [3:0]s00_couplers_to_xbar_TKEEP;
  wire [0:0]s00_couplers_to_xbar_TLAST;
  wire [0:0]s00_couplers_to_xbar_TREADY;
  wire [3:0]s00_couplers_to_xbar_TUSER;
  wire [0:0]s00_couplers_to_xbar_TVALID;
  wire [31:0]xbar_to_m00_couplers_TDATA;
  wire [4:0]xbar_to_m00_couplers_TDEST;
  wire [4:0]xbar_to_m00_couplers_TID;
  wire [3:0]xbar_to_m00_couplers_TKEEP;
  wire [0:0]xbar_to_m00_couplers_TLAST;
  wire xbar_to_m00_couplers_TREADY;
  wire [3:0]xbar_to_m00_couplers_TUSER;
  wire [0:0]xbar_to_m00_couplers_TVALID;
  wire [63:32]xbar_to_m01_couplers_TDATA;
  wire [9:5]xbar_to_m01_couplers_TDEST;
  wire [9:5]xbar_to_m01_couplers_TID;
  wire [7:4]xbar_to_m01_couplers_TKEEP;
  wire [1:1]xbar_to_m01_couplers_TLAST;
  wire xbar_to_m01_couplers_TREADY;
  wire [7:4]xbar_to_m01_couplers_TUSER;
  wire [1:1]xbar_to_m01_couplers_TVALID;
  wire [95:64]xbar_to_m02_couplers_TDATA;
  wire [14:10]xbar_to_m02_couplers_TDEST;
  wire [14:10]xbar_to_m02_couplers_TID;
  wire [11:8]xbar_to_m02_couplers_TKEEP;
  wire [2:2]xbar_to_m02_couplers_TLAST;
  wire xbar_to_m02_couplers_TREADY;
  wire [11:8]xbar_to_m02_couplers_TUSER;
  wire [2:2]xbar_to_m02_couplers_TVALID;

  assign M00_AXIS_tdata[31:0] = m00_couplers_to_axis_interconnect_2_TDATA;
  assign M00_AXIS_tdest[4:0] = m00_couplers_to_axis_interconnect_2_TDEST;
  assign M00_AXIS_tid[4:0] = m00_couplers_to_axis_interconnect_2_TID;
  assign M00_AXIS_tkeep[3:0] = m00_couplers_to_axis_interconnect_2_TKEEP;
  assign M00_AXIS_tlast[0] = m00_couplers_to_axis_interconnect_2_TLAST;
  assign M00_AXIS_tstrb[3:0] = m00_couplers_to_axis_interconnect_2_TSTRB;
  assign M00_AXIS_tuser[3:0] = m00_couplers_to_axis_interconnect_2_TUSER;
  assign M00_AXIS_tvalid = m00_couplers_to_axis_interconnect_2_TVALID;
  assign M01_AXIS_tdata[31:0] = m01_couplers_to_axis_interconnect_2_TDATA;
  assign M01_AXIS_tdest[4:0] = m01_couplers_to_axis_interconnect_2_TDEST;
  assign M01_AXIS_tid[4:0] = m01_couplers_to_axis_interconnect_2_TID;
  assign M01_AXIS_tkeep[3:0] = m01_couplers_to_axis_interconnect_2_TKEEP;
  assign M01_AXIS_tlast[0] = m01_couplers_to_axis_interconnect_2_TLAST;
  assign M01_AXIS_tstrb[3:0] = m01_couplers_to_axis_interconnect_2_TSTRB;
  assign M01_AXIS_tuser[3:0] = m01_couplers_to_axis_interconnect_2_TUSER;
  assign M01_AXIS_tvalid = m01_couplers_to_axis_interconnect_2_TVALID;
  assign M02_AXIS_tdata[31:0] = m02_couplers_to_axis_interconnect_2_TDATA;
  assign M02_AXIS_tdest[4:0] = m02_couplers_to_axis_interconnect_2_TDEST;
  assign M02_AXIS_tid[4:0] = m02_couplers_to_axis_interconnect_2_TID;
  assign M02_AXIS_tkeep[3:0] = m02_couplers_to_axis_interconnect_2_TKEEP;
  assign M02_AXIS_tlast[0] = m02_couplers_to_axis_interconnect_2_TLAST;
  assign M02_AXIS_tstrb[3:0] = m02_couplers_to_axis_interconnect_2_TSTRB;
  assign M02_AXIS_tuser[3:0] = m02_couplers_to_axis_interconnect_2_TUSER;
  assign M02_AXIS_tvalid = m02_couplers_to_axis_interconnect_2_TVALID;
  assign S00_AXIS_tready[0] = axis_interconnect_2_to_s00_couplers_TREADY;
  assign axis_interconnect_2_ACLK_net = ACLK;
  assign axis_interconnect_2_ARESETN_net = ARESETN[0];
  assign axis_interconnect_2_to_s00_couplers_TDATA = S00_AXIS_tdata[31:0];
  assign axis_interconnect_2_to_s00_couplers_TDEST = S00_AXIS_tdest[4:0];
  assign axis_interconnect_2_to_s00_couplers_TID = S00_AXIS_tid[4:0];
  assign axis_interconnect_2_to_s00_couplers_TKEEP = S00_AXIS_tkeep[3:0];
  assign axis_interconnect_2_to_s00_couplers_TLAST = S00_AXIS_tlast[0];
  assign axis_interconnect_2_to_s00_couplers_TUSER = S00_AXIS_tuser[3:0];
  assign axis_interconnect_2_to_s00_couplers_TVALID = S00_AXIS_tvalid[0];
  assign m00_couplers_to_axis_interconnect_2_TREADY = M00_AXIS_tready;
  assign m01_couplers_to_axis_interconnect_2_TREADY = M01_AXIS_tready;
  assign m02_couplers_to_axis_interconnect_2_TREADY = M02_AXIS_tready;
  m00_couplers_imp_6XLIZ3 m00_couplers
       (.M_AXIS_ACLK(axis_interconnect_2_ACLK_net),
        .M_AXIS_ARESETN(axis_interconnect_2_ARESETN_net),
        .M_AXIS_tdata(m00_couplers_to_axis_interconnect_2_TDATA),
        .M_AXIS_tdest(m00_couplers_to_axis_interconnect_2_TDEST),
        .M_AXIS_tid(m00_couplers_to_axis_interconnect_2_TID),
        .M_AXIS_tkeep(m00_couplers_to_axis_interconnect_2_TKEEP),
        .M_AXIS_tlast(m00_couplers_to_axis_interconnect_2_TLAST),
        .M_AXIS_tready(m00_couplers_to_axis_interconnect_2_TREADY),
        .M_AXIS_tstrb(m00_couplers_to_axis_interconnect_2_TSTRB),
        .M_AXIS_tuser(m00_couplers_to_axis_interconnect_2_TUSER),
        .M_AXIS_tvalid(m00_couplers_to_axis_interconnect_2_TVALID),
        .S_AXIS_ACLK(axis_interconnect_2_ACLK_net),
        .S_AXIS_ARESETN(axis_interconnect_2_ARESETN_net),
        .S_AXIS_tdata(xbar_to_m00_couplers_TDATA),
        .S_AXIS_tdest(xbar_to_m00_couplers_TDEST),
        .S_AXIS_tid(xbar_to_m00_couplers_TID),
        .S_AXIS_tkeep(xbar_to_m00_couplers_TKEEP),
        .S_AXIS_tlast(xbar_to_m00_couplers_TLAST),
        .S_AXIS_tready(xbar_to_m00_couplers_TREADY),
        .S_AXIS_tuser(xbar_to_m00_couplers_TUSER),
        .S_AXIS_tvalid(xbar_to_m00_couplers_TVALID));
  m01_couplers_imp_1WQ0M9A m01_couplers
       (.M_AXIS_ACLK(axis_interconnect_2_ACLK_net),
        .M_AXIS_ARESETN(axis_interconnect_2_ARESETN_net),
        .M_AXIS_tdata(m01_couplers_to_axis_interconnect_2_TDATA),
        .M_AXIS_tdest(m01_couplers_to_axis_interconnect_2_TDEST),
        .M_AXIS_tid(m01_couplers_to_axis_interconnect_2_TID),
        .M_AXIS_tkeep(m01_couplers_to_axis_interconnect_2_TKEEP),
        .M_AXIS_tlast(m01_couplers_to_axis_interconnect_2_TLAST),
        .M_AXIS_tready(m01_couplers_to_axis_interconnect_2_TREADY),
        .M_AXIS_tstrb(m01_couplers_to_axis_interconnect_2_TSTRB),
        .M_AXIS_tuser(m01_couplers_to_axis_interconnect_2_TUSER),
        .M_AXIS_tvalid(m01_couplers_to_axis_interconnect_2_TVALID),
        .S_AXIS_ACLK(axis_interconnect_2_ACLK_net),
        .S_AXIS_ARESETN(axis_interconnect_2_ARESETN_net),
        .S_AXIS_tdata(xbar_to_m01_couplers_TDATA),
        .S_AXIS_tdest(xbar_to_m01_couplers_TDEST),
        .S_AXIS_tid(xbar_to_m01_couplers_TID),
        .S_AXIS_tkeep(xbar_to_m01_couplers_TKEEP),
        .S_AXIS_tlast(xbar_to_m01_couplers_TLAST),
        .S_AXIS_tready(xbar_to_m01_couplers_TREADY),
        .S_AXIS_tuser(xbar_to_m01_couplers_TUSER),
        .S_AXIS_tvalid(xbar_to_m01_couplers_TVALID));
  m02_couplers_imp_8395N0 m02_couplers
       (.M_AXIS_ACLK(axis_interconnect_2_ACLK_net),
        .M_AXIS_ARESETN(axis_interconnect_2_ARESETN_net),
        .M_AXIS_tdata(m02_couplers_to_axis_interconnect_2_TDATA),
        .M_AXIS_tdest(m02_couplers_to_axis_interconnect_2_TDEST),
        .M_AXIS_tid(m02_couplers_to_axis_interconnect_2_TID),
        .M_AXIS_tkeep(m02_couplers_to_axis_interconnect_2_TKEEP),
        .M_AXIS_tlast(m02_couplers_to_axis_interconnect_2_TLAST),
        .M_AXIS_tready(m02_couplers_to_axis_interconnect_2_TREADY),
        .M_AXIS_tstrb(m02_couplers_to_axis_interconnect_2_TSTRB),
        .M_AXIS_tuser(m02_couplers_to_axis_interconnect_2_TUSER),
        .M_AXIS_tvalid(m02_couplers_to_axis_interconnect_2_TVALID),
        .S_AXIS_ACLK(axis_interconnect_2_ACLK_net),
        .S_AXIS_ARESETN(axis_interconnect_2_ARESETN_net),
        .S_AXIS_tdata(xbar_to_m02_couplers_TDATA),
        .S_AXIS_tdest(xbar_to_m02_couplers_TDEST),
        .S_AXIS_tid(xbar_to_m02_couplers_TID),
        .S_AXIS_tkeep(xbar_to_m02_couplers_TKEEP),
        .S_AXIS_tlast(xbar_to_m02_couplers_TLAST),
        .S_AXIS_tready(xbar_to_m02_couplers_TREADY),
        .S_AXIS_tuser(xbar_to_m02_couplers_TUSER),
        .S_AXIS_tvalid(xbar_to_m02_couplers_TVALID));
  s00_couplers_imp_1QN17NX s00_couplers
       (.M_AXIS_ACLK(axis_interconnect_2_ACLK_net),
        .M_AXIS_ARESETN(axis_interconnect_2_ARESETN_net),
        .M_AXIS_tdata(s00_couplers_to_xbar_TDATA),
        .M_AXIS_tdest(s00_couplers_to_xbar_TDEST),
        .M_AXIS_tid(s00_couplers_to_xbar_TID),
        .M_AXIS_tkeep(s00_couplers_to_xbar_TKEEP),
        .M_AXIS_tlast(s00_couplers_to_xbar_TLAST),
        .M_AXIS_tready(s00_couplers_to_xbar_TREADY),
        .M_AXIS_tuser(s00_couplers_to_xbar_TUSER),
        .M_AXIS_tvalid(s00_couplers_to_xbar_TVALID),
        .S_AXIS_ACLK(axis_interconnect_2_ACLK_net),
        .S_AXIS_ARESETN(axis_interconnect_2_ARESETN_net),
        .S_AXIS_tdata(axis_interconnect_2_to_s00_couplers_TDATA),
        .S_AXIS_tdest(axis_interconnect_2_to_s00_couplers_TDEST),
        .S_AXIS_tid(axis_interconnect_2_to_s00_couplers_TID),
        .S_AXIS_tkeep(axis_interconnect_2_to_s00_couplers_TKEEP),
        .S_AXIS_tlast(axis_interconnect_2_to_s00_couplers_TLAST),
        .S_AXIS_tready(axis_interconnect_2_to_s00_couplers_TREADY),
        .S_AXIS_tuser(axis_interconnect_2_to_s00_couplers_TUSER),
        .S_AXIS_tvalid(axis_interconnect_2_to_s00_couplers_TVALID));
  design_1_xbar_4 xbar
       (.aclk(axis_interconnect_2_ACLK_net),
        .aresetn(axis_interconnect_2_ARESETN_net),
        .m_axis_tdata({xbar_to_m02_couplers_TDATA,xbar_to_m01_couplers_TDATA,xbar_to_m00_couplers_TDATA}),
        .m_axis_tdest({xbar_to_m02_couplers_TDEST,xbar_to_m01_couplers_TDEST,xbar_to_m00_couplers_TDEST}),
        .m_axis_tid({xbar_to_m02_couplers_TID,xbar_to_m01_couplers_TID,xbar_to_m00_couplers_TID}),
        .m_axis_tkeep({xbar_to_m02_couplers_TKEEP,xbar_to_m01_couplers_TKEEP,xbar_to_m00_couplers_TKEEP}),
        .m_axis_tlast({xbar_to_m02_couplers_TLAST,xbar_to_m01_couplers_TLAST,xbar_to_m00_couplers_TLAST}),
        .m_axis_tready({xbar_to_m02_couplers_TREADY,xbar_to_m01_couplers_TREADY,xbar_to_m00_couplers_TREADY}),
        .m_axis_tuser({xbar_to_m02_couplers_TUSER,xbar_to_m01_couplers_TUSER,xbar_to_m00_couplers_TUSER}),
        .m_axis_tvalid({xbar_to_m02_couplers_TVALID,xbar_to_m01_couplers_TVALID,xbar_to_m00_couplers_TVALID}),
        .s_axis_tdata(s00_couplers_to_xbar_TDATA),
        .s_axis_tdest(s00_couplers_to_xbar_TDEST),
        .s_axis_tid(s00_couplers_to_xbar_TID),
        .s_axis_tkeep(s00_couplers_to_xbar_TKEEP),
        .s_axis_tlast(s00_couplers_to_xbar_TLAST),
        .s_axis_tready(s00_couplers_to_xbar_TREADY),
        .s_axis_tuser(s00_couplers_to_xbar_TUSER),
        .s_axis_tvalid(s00_couplers_to_xbar_TVALID));
endmodule

module design_1_axis_interconnect_0_3
   (ACLK,
    ARESETN,
    M00_AXIS_ACLK,
    M00_AXIS_ARESETN,
    M00_AXIS_tdata,
    M00_AXIS_tdest,
    M00_AXIS_tid,
    M00_AXIS_tkeep,
    M00_AXIS_tlast,
    M00_AXIS_tready,
    M00_AXIS_tstrb,
    M00_AXIS_tuser,
    M00_AXIS_tvalid,
    M01_AXIS_ACLK,
    M01_AXIS_ARESETN,
    M01_AXIS_tdata,
    M01_AXIS_tdest,
    M01_AXIS_tid,
    M01_AXIS_tkeep,
    M01_AXIS_tlast,
    M01_AXIS_tready,
    M01_AXIS_tstrb,
    M01_AXIS_tuser,
    M01_AXIS_tvalid,
    M02_AXIS_ACLK,
    M02_AXIS_ARESETN,
    M02_AXIS_tdata,
    M02_AXIS_tdest,
    M02_AXIS_tid,
    M02_AXIS_tkeep,
    M02_AXIS_tlast,
    M02_AXIS_tready,
    M02_AXIS_tstrb,
    M02_AXIS_tuser,
    M02_AXIS_tvalid,
    S00_AXIS_ACLK,
    S00_AXIS_ARESETN,
    S00_AXIS_tdata,
    S00_AXIS_tdest,
    S00_AXIS_tid,
    S00_AXIS_tkeep,
    S00_AXIS_tlast,
    S00_AXIS_tready,
    S00_AXIS_tuser,
    S00_AXIS_tvalid);
  input ACLK;
  input [0:0]ARESETN;
  input M00_AXIS_ACLK;
  input [0:0]M00_AXIS_ARESETN;
  output [31:0]M00_AXIS_tdata;
  output [4:0]M00_AXIS_tdest;
  output [4:0]M00_AXIS_tid;
  output [3:0]M00_AXIS_tkeep;
  output [0:0]M00_AXIS_tlast;
  input M00_AXIS_tready;
  output [3:0]M00_AXIS_tstrb;
  output [3:0]M00_AXIS_tuser;
  output M00_AXIS_tvalid;
  input M01_AXIS_ACLK;
  input [0:0]M01_AXIS_ARESETN;
  output [31:0]M01_AXIS_tdata;
  output [4:0]M01_AXIS_tdest;
  output [4:0]M01_AXIS_tid;
  output [3:0]M01_AXIS_tkeep;
  output [0:0]M01_AXIS_tlast;
  input M01_AXIS_tready;
  output [3:0]M01_AXIS_tstrb;
  output [3:0]M01_AXIS_tuser;
  output M01_AXIS_tvalid;
  input M02_AXIS_ACLK;
  input [0:0]M02_AXIS_ARESETN;
  output [31:0]M02_AXIS_tdata;
  output [4:0]M02_AXIS_tdest;
  output [4:0]M02_AXIS_tid;
  output [3:0]M02_AXIS_tkeep;
  output [0:0]M02_AXIS_tlast;
  input M02_AXIS_tready;
  output [3:0]M02_AXIS_tstrb;
  output [3:0]M02_AXIS_tuser;
  output M02_AXIS_tvalid;
  input S00_AXIS_ACLK;
  input [0:0]S00_AXIS_ARESETN;
  input [31:0]S00_AXIS_tdata;
  input [4:0]S00_AXIS_tdest;
  input [4:0]S00_AXIS_tid;
  input [3:0]S00_AXIS_tkeep;
  input [0:0]S00_AXIS_tlast;
  output [0:0]S00_AXIS_tready;
  input [3:0]S00_AXIS_tuser;
  input [0:0]S00_AXIS_tvalid;

  wire axis_interconnect_3_ACLK_net;
  wire [0:0]axis_interconnect_3_ARESETN_net;
  wire [31:0]axis_interconnect_3_to_s00_couplers_TDATA;
  wire [4:0]axis_interconnect_3_to_s00_couplers_TDEST;
  wire [4:0]axis_interconnect_3_to_s00_couplers_TID;
  wire [3:0]axis_interconnect_3_to_s00_couplers_TKEEP;
  wire [0:0]axis_interconnect_3_to_s00_couplers_TLAST;
  wire [0:0]axis_interconnect_3_to_s00_couplers_TREADY;
  wire [3:0]axis_interconnect_3_to_s00_couplers_TUSER;
  wire [0:0]axis_interconnect_3_to_s00_couplers_TVALID;
  wire [31:0]m00_couplers_to_axis_interconnect_3_TDATA;
  wire [4:0]m00_couplers_to_axis_interconnect_3_TDEST;
  wire [4:0]m00_couplers_to_axis_interconnect_3_TID;
  wire [3:0]m00_couplers_to_axis_interconnect_3_TKEEP;
  wire [0:0]m00_couplers_to_axis_interconnect_3_TLAST;
  wire m00_couplers_to_axis_interconnect_3_TREADY;
  wire [3:0]m00_couplers_to_axis_interconnect_3_TSTRB;
  wire [3:0]m00_couplers_to_axis_interconnect_3_TUSER;
  wire m00_couplers_to_axis_interconnect_3_TVALID;
  wire [31:0]m01_couplers_to_axis_interconnect_3_TDATA;
  wire [4:0]m01_couplers_to_axis_interconnect_3_TDEST;
  wire [4:0]m01_couplers_to_axis_interconnect_3_TID;
  wire [3:0]m01_couplers_to_axis_interconnect_3_TKEEP;
  wire [0:0]m01_couplers_to_axis_interconnect_3_TLAST;
  wire m01_couplers_to_axis_interconnect_3_TREADY;
  wire [3:0]m01_couplers_to_axis_interconnect_3_TSTRB;
  wire [3:0]m01_couplers_to_axis_interconnect_3_TUSER;
  wire m01_couplers_to_axis_interconnect_3_TVALID;
  wire [31:0]m02_couplers_to_axis_interconnect_3_TDATA;
  wire [4:0]m02_couplers_to_axis_interconnect_3_TDEST;
  wire [4:0]m02_couplers_to_axis_interconnect_3_TID;
  wire [3:0]m02_couplers_to_axis_interconnect_3_TKEEP;
  wire [0:0]m02_couplers_to_axis_interconnect_3_TLAST;
  wire m02_couplers_to_axis_interconnect_3_TREADY;
  wire [3:0]m02_couplers_to_axis_interconnect_3_TSTRB;
  wire [3:0]m02_couplers_to_axis_interconnect_3_TUSER;
  wire m02_couplers_to_axis_interconnect_3_TVALID;
  wire [31:0]s00_couplers_to_xbar_TDATA;
  wire [4:0]s00_couplers_to_xbar_TDEST;
  wire [4:0]s00_couplers_to_xbar_TID;
  wire [3:0]s00_couplers_to_xbar_TKEEP;
  wire [0:0]s00_couplers_to_xbar_TLAST;
  wire [0:0]s00_couplers_to_xbar_TREADY;
  wire [3:0]s00_couplers_to_xbar_TUSER;
  wire [0:0]s00_couplers_to_xbar_TVALID;
  wire [31:0]xbar_to_m00_couplers_TDATA;
  wire [4:0]xbar_to_m00_couplers_TDEST;
  wire [4:0]xbar_to_m00_couplers_TID;
  wire [3:0]xbar_to_m00_couplers_TKEEP;
  wire [0:0]xbar_to_m00_couplers_TLAST;
  wire xbar_to_m00_couplers_TREADY;
  wire [3:0]xbar_to_m00_couplers_TUSER;
  wire [0:0]xbar_to_m00_couplers_TVALID;
  wire [63:32]xbar_to_m01_couplers_TDATA;
  wire [9:5]xbar_to_m01_couplers_TDEST;
  wire [9:5]xbar_to_m01_couplers_TID;
  wire [7:4]xbar_to_m01_couplers_TKEEP;
  wire [1:1]xbar_to_m01_couplers_TLAST;
  wire xbar_to_m01_couplers_TREADY;
  wire [7:4]xbar_to_m01_couplers_TUSER;
  wire [1:1]xbar_to_m01_couplers_TVALID;
  wire [95:64]xbar_to_m02_couplers_TDATA;
  wire [14:10]xbar_to_m02_couplers_TDEST;
  wire [14:10]xbar_to_m02_couplers_TID;
  wire [11:8]xbar_to_m02_couplers_TKEEP;
  wire [2:2]xbar_to_m02_couplers_TLAST;
  wire xbar_to_m02_couplers_TREADY;
  wire [11:8]xbar_to_m02_couplers_TUSER;
  wire [2:2]xbar_to_m02_couplers_TVALID;

  assign M00_AXIS_tdata[31:0] = m00_couplers_to_axis_interconnect_3_TDATA;
  assign M00_AXIS_tdest[4:0] = m00_couplers_to_axis_interconnect_3_TDEST;
  assign M00_AXIS_tid[4:0] = m00_couplers_to_axis_interconnect_3_TID;
  assign M00_AXIS_tkeep[3:0] = m00_couplers_to_axis_interconnect_3_TKEEP;
  assign M00_AXIS_tlast[0] = m00_couplers_to_axis_interconnect_3_TLAST;
  assign M00_AXIS_tstrb[3:0] = m00_couplers_to_axis_interconnect_3_TSTRB;
  assign M00_AXIS_tuser[3:0] = m00_couplers_to_axis_interconnect_3_TUSER;
  assign M00_AXIS_tvalid = m00_couplers_to_axis_interconnect_3_TVALID;
  assign M01_AXIS_tdata[31:0] = m01_couplers_to_axis_interconnect_3_TDATA;
  assign M01_AXIS_tdest[4:0] = m01_couplers_to_axis_interconnect_3_TDEST;
  assign M01_AXIS_tid[4:0] = m01_couplers_to_axis_interconnect_3_TID;
  assign M01_AXIS_tkeep[3:0] = m01_couplers_to_axis_interconnect_3_TKEEP;
  assign M01_AXIS_tlast[0] = m01_couplers_to_axis_interconnect_3_TLAST;
  assign M01_AXIS_tstrb[3:0] = m01_couplers_to_axis_interconnect_3_TSTRB;
  assign M01_AXIS_tuser[3:0] = m01_couplers_to_axis_interconnect_3_TUSER;
  assign M01_AXIS_tvalid = m01_couplers_to_axis_interconnect_3_TVALID;
  assign M02_AXIS_tdata[31:0] = m02_couplers_to_axis_interconnect_3_TDATA;
  assign M02_AXIS_tdest[4:0] = m02_couplers_to_axis_interconnect_3_TDEST;
  assign M02_AXIS_tid[4:0] = m02_couplers_to_axis_interconnect_3_TID;
  assign M02_AXIS_tkeep[3:0] = m02_couplers_to_axis_interconnect_3_TKEEP;
  assign M02_AXIS_tlast[0] = m02_couplers_to_axis_interconnect_3_TLAST;
  assign M02_AXIS_tstrb[3:0] = m02_couplers_to_axis_interconnect_3_TSTRB;
  assign M02_AXIS_tuser[3:0] = m02_couplers_to_axis_interconnect_3_TUSER;
  assign M02_AXIS_tvalid = m02_couplers_to_axis_interconnect_3_TVALID;
  assign S00_AXIS_tready[0] = axis_interconnect_3_to_s00_couplers_TREADY;
  assign axis_interconnect_3_ACLK_net = ACLK;
  assign axis_interconnect_3_ARESETN_net = ARESETN[0];
  assign axis_interconnect_3_to_s00_couplers_TDATA = S00_AXIS_tdata[31:0];
  assign axis_interconnect_3_to_s00_couplers_TDEST = S00_AXIS_tdest[4:0];
  assign axis_interconnect_3_to_s00_couplers_TID = S00_AXIS_tid[4:0];
  assign axis_interconnect_3_to_s00_couplers_TKEEP = S00_AXIS_tkeep[3:0];
  assign axis_interconnect_3_to_s00_couplers_TLAST = S00_AXIS_tlast[0];
  assign axis_interconnect_3_to_s00_couplers_TUSER = S00_AXIS_tuser[3:0];
  assign axis_interconnect_3_to_s00_couplers_TVALID = S00_AXIS_tvalid[0];
  assign m00_couplers_to_axis_interconnect_3_TREADY = M00_AXIS_tready;
  assign m01_couplers_to_axis_interconnect_3_TREADY = M01_AXIS_tready;
  assign m02_couplers_to_axis_interconnect_3_TREADY = M02_AXIS_tready;
  m00_couplers_imp_4E4G3Z m00_couplers
       (.M_AXIS_ACLK(axis_interconnect_3_ACLK_net),
        .M_AXIS_ARESETN(axis_interconnect_3_ARESETN_net),
        .M_AXIS_tdata(m00_couplers_to_axis_interconnect_3_TDATA),
        .M_AXIS_tdest(m00_couplers_to_axis_interconnect_3_TDEST),
        .M_AXIS_tid(m00_couplers_to_axis_interconnect_3_TID),
        .M_AXIS_tkeep(m00_couplers_to_axis_interconnect_3_TKEEP),
        .M_AXIS_tlast(m00_couplers_to_axis_interconnect_3_TLAST),
        .M_AXIS_tready(m00_couplers_to_axis_interconnect_3_TREADY),
        .M_AXIS_tstrb(m00_couplers_to_axis_interconnect_3_TSTRB),
        .M_AXIS_tuser(m00_couplers_to_axis_interconnect_3_TUSER),
        .M_AXIS_tvalid(m00_couplers_to_axis_interconnect_3_TVALID),
        .S_AXIS_ACLK(axis_interconnect_3_ACLK_net),
        .S_AXIS_ARESETN(axis_interconnect_3_ARESETN_net),
        .S_AXIS_tdata(xbar_to_m00_couplers_TDATA),
        .S_AXIS_tdest(xbar_to_m00_couplers_TDEST),
        .S_AXIS_tid(xbar_to_m00_couplers_TID),
        .S_AXIS_tkeep(xbar_to_m00_couplers_TKEEP),
        .S_AXIS_tlast(xbar_to_m00_couplers_TLAST),
        .S_AXIS_tready(xbar_to_m00_couplers_TREADY),
        .S_AXIS_tuser(xbar_to_m00_couplers_TUSER),
        .S_AXIS_tvalid(xbar_to_m00_couplers_TVALID));
  m01_couplers_imp_1QAPM5Q m01_couplers
       (.M_AXIS_ACLK(axis_interconnect_3_ACLK_net),
        .M_AXIS_ARESETN(axis_interconnect_3_ARESETN_net),
        .M_AXIS_tdata(m01_couplers_to_axis_interconnect_3_TDATA),
        .M_AXIS_tdest(m01_couplers_to_axis_interconnect_3_TDEST),
        .M_AXIS_tid(m01_couplers_to_axis_interconnect_3_TID),
        .M_AXIS_tkeep(m01_couplers_to_axis_interconnect_3_TKEEP),
        .M_AXIS_tlast(m01_couplers_to_axis_interconnect_3_TLAST),
        .M_AXIS_tready(m01_couplers_to_axis_interconnect_3_TREADY),
        .M_AXIS_tstrb(m01_couplers_to_axis_interconnect_3_TSTRB),
        .M_AXIS_tuser(m01_couplers_to_axis_interconnect_3_TUSER),
        .M_AXIS_tvalid(m01_couplers_to_axis_interconnect_3_TVALID),
        .S_AXIS_ACLK(axis_interconnect_3_ACLK_net),
        .S_AXIS_ARESETN(axis_interconnect_3_ARESETN_net),
        .S_AXIS_tdata(xbar_to_m01_couplers_TDATA),
        .S_AXIS_tdest(xbar_to_m01_couplers_TDEST),
        .S_AXIS_tid(xbar_to_m01_couplers_TID),
        .S_AXIS_tkeep(xbar_to_m01_couplers_TKEEP),
        .S_AXIS_tlast(xbar_to_m01_couplers_TLAST),
        .S_AXIS_tready(xbar_to_m01_couplers_TREADY),
        .S_AXIS_tuser(xbar_to_m01_couplers_TUSER),
        .S_AXIS_tvalid(xbar_to_m01_couplers_TVALID));
  m02_couplers_imp_2RX6OC m02_couplers
       (.M_AXIS_ACLK(axis_interconnect_3_ACLK_net),
        .M_AXIS_ARESETN(axis_interconnect_3_ARESETN_net),
        .M_AXIS_tdata(m02_couplers_to_axis_interconnect_3_TDATA),
        .M_AXIS_tdest(m02_couplers_to_axis_interconnect_3_TDEST),
        .M_AXIS_tid(m02_couplers_to_axis_interconnect_3_TID),
        .M_AXIS_tkeep(m02_couplers_to_axis_interconnect_3_TKEEP),
        .M_AXIS_tlast(m02_couplers_to_axis_interconnect_3_TLAST),
        .M_AXIS_tready(m02_couplers_to_axis_interconnect_3_TREADY),
        .M_AXIS_tstrb(m02_couplers_to_axis_interconnect_3_TSTRB),
        .M_AXIS_tuser(m02_couplers_to_axis_interconnect_3_TUSER),
        .M_AXIS_tvalid(m02_couplers_to_axis_interconnect_3_TVALID),
        .S_AXIS_ACLK(axis_interconnect_3_ACLK_net),
        .S_AXIS_ARESETN(axis_interconnect_3_ARESETN_net),
        .S_AXIS_tdata(xbar_to_m02_couplers_TDATA),
        .S_AXIS_tdest(xbar_to_m02_couplers_TDEST),
        .S_AXIS_tid(xbar_to_m02_couplers_TID),
        .S_AXIS_tkeep(xbar_to_m02_couplers_TKEEP),
        .S_AXIS_tlast(xbar_to_m02_couplers_TLAST),
        .S_AXIS_tready(xbar_to_m02_couplers_TREADY),
        .S_AXIS_tuser(xbar_to_m02_couplers_TUSER),
        .S_AXIS_tvalid(xbar_to_m02_couplers_TVALID));
  s00_couplers_imp_1WIDDQL s00_couplers
       (.M_AXIS_ACLK(axis_interconnect_3_ACLK_net),
        .M_AXIS_ARESETN(axis_interconnect_3_ARESETN_net),
        .M_AXIS_tdata(s00_couplers_to_xbar_TDATA),
        .M_AXIS_tdest(s00_couplers_to_xbar_TDEST),
        .M_AXIS_tid(s00_couplers_to_xbar_TID),
        .M_AXIS_tkeep(s00_couplers_to_xbar_TKEEP),
        .M_AXIS_tlast(s00_couplers_to_xbar_TLAST),
        .M_AXIS_tready(s00_couplers_to_xbar_TREADY),
        .M_AXIS_tuser(s00_couplers_to_xbar_TUSER),
        .M_AXIS_tvalid(s00_couplers_to_xbar_TVALID),
        .S_AXIS_ACLK(axis_interconnect_3_ACLK_net),
        .S_AXIS_ARESETN(axis_interconnect_3_ARESETN_net),
        .S_AXIS_tdata(axis_interconnect_3_to_s00_couplers_TDATA),
        .S_AXIS_tdest(axis_interconnect_3_to_s00_couplers_TDEST),
        .S_AXIS_tid(axis_interconnect_3_to_s00_couplers_TID),
        .S_AXIS_tkeep(axis_interconnect_3_to_s00_couplers_TKEEP),
        .S_AXIS_tlast(axis_interconnect_3_to_s00_couplers_TLAST),
        .S_AXIS_tready(axis_interconnect_3_to_s00_couplers_TREADY),
        .S_AXIS_tuser(axis_interconnect_3_to_s00_couplers_TUSER),
        .S_AXIS_tvalid(axis_interconnect_3_to_s00_couplers_TVALID));
  design_1_xbar_5 xbar
       (.aclk(axis_interconnect_3_ACLK_net),
        .aresetn(axis_interconnect_3_ARESETN_net),
        .m_axis_tdata({xbar_to_m02_couplers_TDATA,xbar_to_m01_couplers_TDATA,xbar_to_m00_couplers_TDATA}),
        .m_axis_tdest({xbar_to_m02_couplers_TDEST,xbar_to_m01_couplers_TDEST,xbar_to_m00_couplers_TDEST}),
        .m_axis_tid({xbar_to_m02_couplers_TID,xbar_to_m01_couplers_TID,xbar_to_m00_couplers_TID}),
        .m_axis_tkeep({xbar_to_m02_couplers_TKEEP,xbar_to_m01_couplers_TKEEP,xbar_to_m00_couplers_TKEEP}),
        .m_axis_tlast({xbar_to_m02_couplers_TLAST,xbar_to_m01_couplers_TLAST,xbar_to_m00_couplers_TLAST}),
        .m_axis_tready({xbar_to_m02_couplers_TREADY,xbar_to_m01_couplers_TREADY,xbar_to_m00_couplers_TREADY}),
        .m_axis_tuser({xbar_to_m02_couplers_TUSER,xbar_to_m01_couplers_TUSER,xbar_to_m00_couplers_TUSER}),
        .m_axis_tvalid({xbar_to_m02_couplers_TVALID,xbar_to_m01_couplers_TVALID,xbar_to_m00_couplers_TVALID}),
        .s_axis_tdata(s00_couplers_to_xbar_TDATA),
        .s_axis_tdest(s00_couplers_to_xbar_TDEST),
        .s_axis_tid(s00_couplers_to_xbar_TID),
        .s_axis_tkeep(s00_couplers_to_xbar_TKEEP),
        .s_axis_tlast(s00_couplers_to_xbar_TLAST),
        .s_axis_tready(s00_couplers_to_xbar_TREADY),
        .s_axis_tuser(s00_couplers_to_xbar_TUSER),
        .s_axis_tvalid(s00_couplers_to_xbar_TVALID));
endmodule

module design_1_microblaze_0_axi_periph_0
   (ACLK,
    ARESETN,
    M00_ACLK,
    M00_ARESETN,
    M00_AXI_araddr,
    M00_AXI_arready,
    M00_AXI_arvalid,
    M00_AXI_awaddr,
    M00_AXI_awready,
    M00_AXI_awvalid,
    M00_AXI_bready,
    M00_AXI_bresp,
    M00_AXI_bvalid,
    M00_AXI_rdata,
    M00_AXI_rready,
    M00_AXI_rresp,
    M00_AXI_rvalid,
    M00_AXI_wdata,
    M00_AXI_wready,
    M00_AXI_wstrb,
    M00_AXI_wvalid,
    M01_ACLK,
    M01_ARESETN,
    M01_AXI_araddr,
    M01_AXI_arready,
    M01_AXI_arvalid,
    M01_AXI_awaddr,
    M01_AXI_awready,
    M01_AXI_awvalid,
    M01_AXI_bready,
    M01_AXI_bresp,
    M01_AXI_bvalid,
    M01_AXI_rdata,
    M01_AXI_rready,
    M01_AXI_rresp,
    M01_AXI_rvalid,
    M01_AXI_wdata,
    M01_AXI_wready,
    M01_AXI_wstrb,
    M01_AXI_wvalid,
    M02_ACLK,
    M02_ARESETN,
    M02_AXI_araddr,
    M02_AXI_arready,
    M02_AXI_arvalid,
    M02_AXI_awaddr,
    M02_AXI_awready,
    M02_AXI_awvalid,
    M02_AXI_bready,
    M02_AXI_bresp,
    M02_AXI_bvalid,
    M02_AXI_rdata,
    M02_AXI_rready,
    M02_AXI_rresp,
    M02_AXI_rvalid,
    M02_AXI_wdata,
    M02_AXI_wready,
    M02_AXI_wstrb,
    M02_AXI_wvalid,
    M03_ACLK,
    M03_ARESETN,
    M03_AXI_araddr,
    M03_AXI_arready,
    M03_AXI_arvalid,
    M03_AXI_awaddr,
    M03_AXI_awready,
    M03_AXI_awvalid,
    M03_AXI_bready,
    M03_AXI_bresp,
    M03_AXI_bvalid,
    M03_AXI_rdata,
    M03_AXI_rready,
    M03_AXI_rresp,
    M03_AXI_rvalid,
    M03_AXI_wdata,
    M03_AXI_wready,
    M03_AXI_wvalid,
    M04_ACLK,
    M04_ARESETN,
    M04_AXI_araddr,
    M04_AXI_arready,
    M04_AXI_arvalid,
    M04_AXI_awaddr,
    M04_AXI_awready,
    M04_AXI_awvalid,
    M04_AXI_bready,
    M04_AXI_bresp,
    M04_AXI_bvalid,
    M04_AXI_rdata,
    M04_AXI_rready,
    M04_AXI_rresp,
    M04_AXI_rvalid,
    M04_AXI_wdata,
    M04_AXI_wready,
    M04_AXI_wvalid,
    M05_ACLK,
    M05_ARESETN,
    M05_AXI_araddr,
    M05_AXI_arready,
    M05_AXI_arvalid,
    M05_AXI_awaddr,
    M05_AXI_awready,
    M05_AXI_awvalid,
    M05_AXI_bready,
    M05_AXI_bresp,
    M05_AXI_bvalid,
    M05_AXI_rdata,
    M05_AXI_rready,
    M05_AXI_rresp,
    M05_AXI_rvalid,
    M05_AXI_wdata,
    M05_AXI_wready,
    M05_AXI_wvalid,
    M06_ACLK,
    M06_ARESETN,
    M06_AXI_araddr,
    M06_AXI_arready,
    M06_AXI_arvalid,
    M06_AXI_awaddr,
    M06_AXI_awready,
    M06_AXI_awvalid,
    M06_AXI_bready,
    M06_AXI_bresp,
    M06_AXI_bvalid,
    M06_AXI_rdata,
    M06_AXI_rready,
    M06_AXI_rresp,
    M06_AXI_rvalid,
    M06_AXI_wdata,
    M06_AXI_wready,
    M06_AXI_wvalid,
    S00_ACLK,
    S00_ARESETN,
    S00_AXI_araddr,
    S00_AXI_arprot,
    S00_AXI_arready,
    S00_AXI_arvalid,
    S00_AXI_awaddr,
    S00_AXI_awprot,
    S00_AXI_awready,
    S00_AXI_awvalid,
    S00_AXI_bready,
    S00_AXI_bresp,
    S00_AXI_bvalid,
    S00_AXI_rdata,
    S00_AXI_rready,
    S00_AXI_rresp,
    S00_AXI_rvalid,
    S00_AXI_wdata,
    S00_AXI_wready,
    S00_AXI_wstrb,
    S00_AXI_wvalid);
  input ACLK;
  input [0:0]ARESETN;
  input M00_ACLK;
  input [0:0]M00_ARESETN;
  output [31:0]M00_AXI_araddr;
  input [0:0]M00_AXI_arready;
  output [0:0]M00_AXI_arvalid;
  output [31:0]M00_AXI_awaddr;
  input [0:0]M00_AXI_awready;
  output [0:0]M00_AXI_awvalid;
  output [0:0]M00_AXI_bready;
  input [1:0]M00_AXI_bresp;
  input [0:0]M00_AXI_bvalid;
  input [31:0]M00_AXI_rdata;
  output [0:0]M00_AXI_rready;
  input [1:0]M00_AXI_rresp;
  input [0:0]M00_AXI_rvalid;
  output [31:0]M00_AXI_wdata;
  input [0:0]M00_AXI_wready;
  output [3:0]M00_AXI_wstrb;
  output [0:0]M00_AXI_wvalid;
  input M01_ACLK;
  input [0:0]M01_ARESETN;
  output [31:0]M01_AXI_araddr;
  input M01_AXI_arready;
  output M01_AXI_arvalid;
  output [31:0]M01_AXI_awaddr;
  input M01_AXI_awready;
  output M01_AXI_awvalid;
  output M01_AXI_bready;
  input [1:0]M01_AXI_bresp;
  input M01_AXI_bvalid;
  input [31:0]M01_AXI_rdata;
  output M01_AXI_rready;
  input [1:0]M01_AXI_rresp;
  input M01_AXI_rvalid;
  output [31:0]M01_AXI_wdata;
  input M01_AXI_wready;
  output [3:0]M01_AXI_wstrb;
  output M01_AXI_wvalid;
  input M02_ACLK;
  input [0:0]M02_ARESETN;
  output [31:0]M02_AXI_araddr;
  input M02_AXI_arready;
  output M02_AXI_arvalid;
  output [31:0]M02_AXI_awaddr;
  input M02_AXI_awready;
  output M02_AXI_awvalid;
  output M02_AXI_bready;
  input [1:0]M02_AXI_bresp;
  input M02_AXI_bvalid;
  input [31:0]M02_AXI_rdata;
  output M02_AXI_rready;
  input [1:0]M02_AXI_rresp;
  input M02_AXI_rvalid;
  output [31:0]M02_AXI_wdata;
  input M02_AXI_wready;
  output [3:0]M02_AXI_wstrb;
  output M02_AXI_wvalid;
  input M03_ACLK;
  input [0:0]M03_ARESETN;
  output [31:0]M03_AXI_araddr;
  input M03_AXI_arready;
  output M03_AXI_arvalid;
  output [31:0]M03_AXI_awaddr;
  input M03_AXI_awready;
  output M03_AXI_awvalid;
  output M03_AXI_bready;
  input [1:0]M03_AXI_bresp;
  input M03_AXI_bvalid;
  input [31:0]M03_AXI_rdata;
  output M03_AXI_rready;
  input [1:0]M03_AXI_rresp;
  input M03_AXI_rvalid;
  output [31:0]M03_AXI_wdata;
  input M03_AXI_wready;
  output M03_AXI_wvalid;
  input M04_ACLK;
  input [0:0]M04_ARESETN;
  output [31:0]M04_AXI_araddr;
  input M04_AXI_arready;
  output M04_AXI_arvalid;
  output [31:0]M04_AXI_awaddr;
  input M04_AXI_awready;
  output M04_AXI_awvalid;
  output M04_AXI_bready;
  input [1:0]M04_AXI_bresp;
  input M04_AXI_bvalid;
  input [31:0]M04_AXI_rdata;
  output M04_AXI_rready;
  input [1:0]M04_AXI_rresp;
  input M04_AXI_rvalid;
  output [31:0]M04_AXI_wdata;
  input M04_AXI_wready;
  output M04_AXI_wvalid;
  input M05_ACLK;
  input [0:0]M05_ARESETN;
  output [31:0]M05_AXI_araddr;
  input M05_AXI_arready;
  output M05_AXI_arvalid;
  output [31:0]M05_AXI_awaddr;
  input M05_AXI_awready;
  output M05_AXI_awvalid;
  output M05_AXI_bready;
  input [1:0]M05_AXI_bresp;
  input M05_AXI_bvalid;
  input [31:0]M05_AXI_rdata;
  output M05_AXI_rready;
  input [1:0]M05_AXI_rresp;
  input M05_AXI_rvalid;
  output [31:0]M05_AXI_wdata;
  input M05_AXI_wready;
  output M05_AXI_wvalid;
  input M06_ACLK;
  input [0:0]M06_ARESETN;
  output [31:0]M06_AXI_araddr;
  input M06_AXI_arready;
  output M06_AXI_arvalid;
  output [31:0]M06_AXI_awaddr;
  input M06_AXI_awready;
  output M06_AXI_awvalid;
  output M06_AXI_bready;
  input [1:0]M06_AXI_bresp;
  input M06_AXI_bvalid;
  input [31:0]M06_AXI_rdata;
  output M06_AXI_rready;
  input [1:0]M06_AXI_rresp;
  input M06_AXI_rvalid;
  output [31:0]M06_AXI_wdata;
  input M06_AXI_wready;
  output M06_AXI_wvalid;
  input S00_ACLK;
  input [0:0]S00_ARESETN;
  input [31:0]S00_AXI_araddr;
  input [2:0]S00_AXI_arprot;
  output S00_AXI_arready;
  input S00_AXI_arvalid;
  input [31:0]S00_AXI_awaddr;
  input [2:0]S00_AXI_awprot;
  output S00_AXI_awready;
  input S00_AXI_awvalid;
  input S00_AXI_bready;
  output [1:0]S00_AXI_bresp;
  output S00_AXI_bvalid;
  output [31:0]S00_AXI_rdata;
  input S00_AXI_rready;
  output [1:0]S00_AXI_rresp;
  output S00_AXI_rvalid;
  input [31:0]S00_AXI_wdata;
  output S00_AXI_wready;
  input [3:0]S00_AXI_wstrb;
  input S00_AXI_wvalid;

  wire M00_ACLK_1;
  wire [0:0]M00_ARESETN_1;
  wire M01_ACLK_1;
  wire [0:0]M01_ARESETN_1;
  wire M02_ACLK_1;
  wire [0:0]M02_ARESETN_1;
  wire M03_ACLK_1;
  wire [0:0]M03_ARESETN_1;
  wire M04_ACLK_1;
  wire [0:0]M04_ARESETN_1;
  wire M05_ACLK_1;
  wire [0:0]M05_ARESETN_1;
  wire M06_ACLK_1;
  wire [0:0]M06_ARESETN_1;
  wire S00_ACLK_1;
  wire [0:0]S00_ARESETN_1;
  wire [31:0]m00_couplers_to_microblaze_0_axi_periph_ARADDR;
  wire [0:0]m00_couplers_to_microblaze_0_axi_periph_ARREADY;
  wire [0:0]m00_couplers_to_microblaze_0_axi_periph_ARVALID;
  wire [31:0]m00_couplers_to_microblaze_0_axi_periph_AWADDR;
  wire [0:0]m00_couplers_to_microblaze_0_axi_periph_AWREADY;
  wire [0:0]m00_couplers_to_microblaze_0_axi_periph_AWVALID;
  wire [0:0]m00_couplers_to_microblaze_0_axi_periph_BREADY;
  wire [1:0]m00_couplers_to_microblaze_0_axi_periph_BRESP;
  wire [0:0]m00_couplers_to_microblaze_0_axi_periph_BVALID;
  wire [31:0]m00_couplers_to_microblaze_0_axi_periph_RDATA;
  wire [0:0]m00_couplers_to_microblaze_0_axi_periph_RREADY;
  wire [1:0]m00_couplers_to_microblaze_0_axi_periph_RRESP;
  wire [0:0]m00_couplers_to_microblaze_0_axi_periph_RVALID;
  wire [31:0]m00_couplers_to_microblaze_0_axi_periph_WDATA;
  wire [0:0]m00_couplers_to_microblaze_0_axi_periph_WREADY;
  wire [3:0]m00_couplers_to_microblaze_0_axi_periph_WSTRB;
  wire [0:0]m00_couplers_to_microblaze_0_axi_periph_WVALID;
  wire [31:0]m01_couplers_to_microblaze_0_axi_periph_ARADDR;
  wire m01_couplers_to_microblaze_0_axi_periph_ARREADY;
  wire m01_couplers_to_microblaze_0_axi_periph_ARVALID;
  wire [31:0]m01_couplers_to_microblaze_0_axi_periph_AWADDR;
  wire m01_couplers_to_microblaze_0_axi_periph_AWREADY;
  wire m01_couplers_to_microblaze_0_axi_periph_AWVALID;
  wire m01_couplers_to_microblaze_0_axi_periph_BREADY;
  wire [1:0]m01_couplers_to_microblaze_0_axi_periph_BRESP;
  wire m01_couplers_to_microblaze_0_axi_periph_BVALID;
  wire [31:0]m01_couplers_to_microblaze_0_axi_periph_RDATA;
  wire m01_couplers_to_microblaze_0_axi_periph_RREADY;
  wire [1:0]m01_couplers_to_microblaze_0_axi_periph_RRESP;
  wire m01_couplers_to_microblaze_0_axi_periph_RVALID;
  wire [31:0]m01_couplers_to_microblaze_0_axi_periph_WDATA;
  wire m01_couplers_to_microblaze_0_axi_periph_WREADY;
  wire [3:0]m01_couplers_to_microblaze_0_axi_periph_WSTRB;
  wire m01_couplers_to_microblaze_0_axi_periph_WVALID;
  wire [31:0]m02_couplers_to_microblaze_0_axi_periph_ARADDR;
  wire m02_couplers_to_microblaze_0_axi_periph_ARREADY;
  wire m02_couplers_to_microblaze_0_axi_periph_ARVALID;
  wire [31:0]m02_couplers_to_microblaze_0_axi_periph_AWADDR;
  wire m02_couplers_to_microblaze_0_axi_periph_AWREADY;
  wire m02_couplers_to_microblaze_0_axi_periph_AWVALID;
  wire m02_couplers_to_microblaze_0_axi_periph_BREADY;
  wire [1:0]m02_couplers_to_microblaze_0_axi_periph_BRESP;
  wire m02_couplers_to_microblaze_0_axi_periph_BVALID;
  wire [31:0]m02_couplers_to_microblaze_0_axi_periph_RDATA;
  wire m02_couplers_to_microblaze_0_axi_periph_RREADY;
  wire [1:0]m02_couplers_to_microblaze_0_axi_periph_RRESP;
  wire m02_couplers_to_microblaze_0_axi_periph_RVALID;
  wire [31:0]m02_couplers_to_microblaze_0_axi_periph_WDATA;
  wire m02_couplers_to_microblaze_0_axi_periph_WREADY;
  wire [3:0]m02_couplers_to_microblaze_0_axi_periph_WSTRB;
  wire m02_couplers_to_microblaze_0_axi_periph_WVALID;
  wire [31:0]m03_couplers_to_microblaze_0_axi_periph_ARADDR;
  wire m03_couplers_to_microblaze_0_axi_periph_ARREADY;
  wire m03_couplers_to_microblaze_0_axi_periph_ARVALID;
  wire [31:0]m03_couplers_to_microblaze_0_axi_periph_AWADDR;
  wire m03_couplers_to_microblaze_0_axi_periph_AWREADY;
  wire m03_couplers_to_microblaze_0_axi_periph_AWVALID;
  wire m03_couplers_to_microblaze_0_axi_periph_BREADY;
  wire [1:0]m03_couplers_to_microblaze_0_axi_periph_BRESP;
  wire m03_couplers_to_microblaze_0_axi_periph_BVALID;
  wire [31:0]m03_couplers_to_microblaze_0_axi_periph_RDATA;
  wire m03_couplers_to_microblaze_0_axi_periph_RREADY;
  wire [1:0]m03_couplers_to_microblaze_0_axi_periph_RRESP;
  wire m03_couplers_to_microblaze_0_axi_periph_RVALID;
  wire [31:0]m03_couplers_to_microblaze_0_axi_periph_WDATA;
  wire m03_couplers_to_microblaze_0_axi_periph_WREADY;
  wire m03_couplers_to_microblaze_0_axi_periph_WVALID;
  wire [31:0]m04_couplers_to_microblaze_0_axi_periph_ARADDR;
  wire m04_couplers_to_microblaze_0_axi_periph_ARREADY;
  wire m04_couplers_to_microblaze_0_axi_periph_ARVALID;
  wire [31:0]m04_couplers_to_microblaze_0_axi_periph_AWADDR;
  wire m04_couplers_to_microblaze_0_axi_periph_AWREADY;
  wire m04_couplers_to_microblaze_0_axi_periph_AWVALID;
  wire m04_couplers_to_microblaze_0_axi_periph_BREADY;
  wire [1:0]m04_couplers_to_microblaze_0_axi_periph_BRESP;
  wire m04_couplers_to_microblaze_0_axi_periph_BVALID;
  wire [31:0]m04_couplers_to_microblaze_0_axi_periph_RDATA;
  wire m04_couplers_to_microblaze_0_axi_periph_RREADY;
  wire [1:0]m04_couplers_to_microblaze_0_axi_periph_RRESP;
  wire m04_couplers_to_microblaze_0_axi_periph_RVALID;
  wire [31:0]m04_couplers_to_microblaze_0_axi_periph_WDATA;
  wire m04_couplers_to_microblaze_0_axi_periph_WREADY;
  wire m04_couplers_to_microblaze_0_axi_periph_WVALID;
  wire [31:0]m05_couplers_to_microblaze_0_axi_periph_ARADDR;
  wire m05_couplers_to_microblaze_0_axi_periph_ARREADY;
  wire m05_couplers_to_microblaze_0_axi_periph_ARVALID;
  wire [31:0]m05_couplers_to_microblaze_0_axi_periph_AWADDR;
  wire m05_couplers_to_microblaze_0_axi_periph_AWREADY;
  wire m05_couplers_to_microblaze_0_axi_periph_AWVALID;
  wire m05_couplers_to_microblaze_0_axi_periph_BREADY;
  wire [1:0]m05_couplers_to_microblaze_0_axi_periph_BRESP;
  wire m05_couplers_to_microblaze_0_axi_periph_BVALID;
  wire [31:0]m05_couplers_to_microblaze_0_axi_periph_RDATA;
  wire m05_couplers_to_microblaze_0_axi_periph_RREADY;
  wire [1:0]m05_couplers_to_microblaze_0_axi_periph_RRESP;
  wire m05_couplers_to_microblaze_0_axi_periph_RVALID;
  wire [31:0]m05_couplers_to_microblaze_0_axi_periph_WDATA;
  wire m05_couplers_to_microblaze_0_axi_periph_WREADY;
  wire m05_couplers_to_microblaze_0_axi_periph_WVALID;
  wire [31:0]m06_couplers_to_microblaze_0_axi_periph_ARADDR;
  wire m06_couplers_to_microblaze_0_axi_periph_ARREADY;
  wire m06_couplers_to_microblaze_0_axi_periph_ARVALID;
  wire [31:0]m06_couplers_to_microblaze_0_axi_periph_AWADDR;
  wire m06_couplers_to_microblaze_0_axi_periph_AWREADY;
  wire m06_couplers_to_microblaze_0_axi_periph_AWVALID;
  wire m06_couplers_to_microblaze_0_axi_periph_BREADY;
  wire [1:0]m06_couplers_to_microblaze_0_axi_periph_BRESP;
  wire m06_couplers_to_microblaze_0_axi_periph_BVALID;
  wire [31:0]m06_couplers_to_microblaze_0_axi_periph_RDATA;
  wire m06_couplers_to_microblaze_0_axi_periph_RREADY;
  wire [1:0]m06_couplers_to_microblaze_0_axi_periph_RRESP;
  wire m06_couplers_to_microblaze_0_axi_periph_RVALID;
  wire [31:0]m06_couplers_to_microblaze_0_axi_periph_WDATA;
  wire m06_couplers_to_microblaze_0_axi_periph_WREADY;
  wire m06_couplers_to_microblaze_0_axi_periph_WVALID;
  wire microblaze_0_axi_periph_ACLK_net;
  wire [0:0]microblaze_0_axi_periph_ARESETN_net;
  wire [31:0]microblaze_0_axi_periph_to_s00_couplers_ARADDR;
  wire [2:0]microblaze_0_axi_periph_to_s00_couplers_ARPROT;
  wire microblaze_0_axi_periph_to_s00_couplers_ARREADY;
  wire microblaze_0_axi_periph_to_s00_couplers_ARVALID;
  wire [31:0]microblaze_0_axi_periph_to_s00_couplers_AWADDR;
  wire [2:0]microblaze_0_axi_periph_to_s00_couplers_AWPROT;
  wire microblaze_0_axi_periph_to_s00_couplers_AWREADY;
  wire microblaze_0_axi_periph_to_s00_couplers_AWVALID;
  wire microblaze_0_axi_periph_to_s00_couplers_BREADY;
  wire [1:0]microblaze_0_axi_periph_to_s00_couplers_BRESP;
  wire microblaze_0_axi_periph_to_s00_couplers_BVALID;
  wire [31:0]microblaze_0_axi_periph_to_s00_couplers_RDATA;
  wire microblaze_0_axi_periph_to_s00_couplers_RREADY;
  wire [1:0]microblaze_0_axi_periph_to_s00_couplers_RRESP;
  wire microblaze_0_axi_periph_to_s00_couplers_RVALID;
  wire [31:0]microblaze_0_axi_periph_to_s00_couplers_WDATA;
  wire microblaze_0_axi_periph_to_s00_couplers_WREADY;
  wire [3:0]microblaze_0_axi_periph_to_s00_couplers_WSTRB;
  wire microblaze_0_axi_periph_to_s00_couplers_WVALID;
  wire [31:0]s00_couplers_to_xbar_ARADDR;
  wire [2:0]s00_couplers_to_xbar_ARPROT;
  wire [0:0]s00_couplers_to_xbar_ARREADY;
  wire s00_couplers_to_xbar_ARVALID;
  wire [31:0]s00_couplers_to_xbar_AWADDR;
  wire [2:0]s00_couplers_to_xbar_AWPROT;
  wire [0:0]s00_couplers_to_xbar_AWREADY;
  wire s00_couplers_to_xbar_AWVALID;
  wire s00_couplers_to_xbar_BREADY;
  wire [1:0]s00_couplers_to_xbar_BRESP;
  wire [0:0]s00_couplers_to_xbar_BVALID;
  wire [31:0]s00_couplers_to_xbar_RDATA;
  wire s00_couplers_to_xbar_RREADY;
  wire [1:0]s00_couplers_to_xbar_RRESP;
  wire [0:0]s00_couplers_to_xbar_RVALID;
  wire [31:0]s00_couplers_to_xbar_WDATA;
  wire [0:0]s00_couplers_to_xbar_WREADY;
  wire [3:0]s00_couplers_to_xbar_WSTRB;
  wire s00_couplers_to_xbar_WVALID;
  wire [31:0]xbar_to_m00_couplers_ARADDR;
  wire [0:0]xbar_to_m00_couplers_ARREADY;
  wire [0:0]xbar_to_m00_couplers_ARVALID;
  wire [31:0]xbar_to_m00_couplers_AWADDR;
  wire [0:0]xbar_to_m00_couplers_AWREADY;
  wire [0:0]xbar_to_m00_couplers_AWVALID;
  wire [0:0]xbar_to_m00_couplers_BREADY;
  wire [1:0]xbar_to_m00_couplers_BRESP;
  wire [0:0]xbar_to_m00_couplers_BVALID;
  wire [31:0]xbar_to_m00_couplers_RDATA;
  wire [0:0]xbar_to_m00_couplers_RREADY;
  wire [1:0]xbar_to_m00_couplers_RRESP;
  wire [0:0]xbar_to_m00_couplers_RVALID;
  wire [31:0]xbar_to_m00_couplers_WDATA;
  wire [0:0]xbar_to_m00_couplers_WREADY;
  wire [3:0]xbar_to_m00_couplers_WSTRB;
  wire [0:0]xbar_to_m00_couplers_WVALID;
  wire [63:32]xbar_to_m01_couplers_ARADDR;
  wire xbar_to_m01_couplers_ARREADY;
  wire [1:1]xbar_to_m01_couplers_ARVALID;
  wire [63:32]xbar_to_m01_couplers_AWADDR;
  wire xbar_to_m01_couplers_AWREADY;
  wire [1:1]xbar_to_m01_couplers_AWVALID;
  wire [1:1]xbar_to_m01_couplers_BREADY;
  wire [1:0]xbar_to_m01_couplers_BRESP;
  wire xbar_to_m01_couplers_BVALID;
  wire [31:0]xbar_to_m01_couplers_RDATA;
  wire [1:1]xbar_to_m01_couplers_RREADY;
  wire [1:0]xbar_to_m01_couplers_RRESP;
  wire xbar_to_m01_couplers_RVALID;
  wire [63:32]xbar_to_m01_couplers_WDATA;
  wire xbar_to_m01_couplers_WREADY;
  wire [7:4]xbar_to_m01_couplers_WSTRB;
  wire [1:1]xbar_to_m01_couplers_WVALID;
  wire [95:64]xbar_to_m02_couplers_ARADDR;
  wire xbar_to_m02_couplers_ARREADY;
  wire [2:2]xbar_to_m02_couplers_ARVALID;
  wire [95:64]xbar_to_m02_couplers_AWADDR;
  wire xbar_to_m02_couplers_AWREADY;
  wire [2:2]xbar_to_m02_couplers_AWVALID;
  wire [2:2]xbar_to_m02_couplers_BREADY;
  wire [1:0]xbar_to_m02_couplers_BRESP;
  wire xbar_to_m02_couplers_BVALID;
  wire [31:0]xbar_to_m02_couplers_RDATA;
  wire [2:2]xbar_to_m02_couplers_RREADY;
  wire [1:0]xbar_to_m02_couplers_RRESP;
  wire xbar_to_m02_couplers_RVALID;
  wire [95:64]xbar_to_m02_couplers_WDATA;
  wire xbar_to_m02_couplers_WREADY;
  wire [11:8]xbar_to_m02_couplers_WSTRB;
  wire [2:2]xbar_to_m02_couplers_WVALID;
  wire [127:96]xbar_to_m03_couplers_ARADDR;
  wire xbar_to_m03_couplers_ARREADY;
  wire [3:3]xbar_to_m03_couplers_ARVALID;
  wire [127:96]xbar_to_m03_couplers_AWADDR;
  wire xbar_to_m03_couplers_AWREADY;
  wire [3:3]xbar_to_m03_couplers_AWVALID;
  wire [3:3]xbar_to_m03_couplers_BREADY;
  wire [1:0]xbar_to_m03_couplers_BRESP;
  wire xbar_to_m03_couplers_BVALID;
  wire [31:0]xbar_to_m03_couplers_RDATA;
  wire [3:3]xbar_to_m03_couplers_RREADY;
  wire [1:0]xbar_to_m03_couplers_RRESP;
  wire xbar_to_m03_couplers_RVALID;
  wire [127:96]xbar_to_m03_couplers_WDATA;
  wire xbar_to_m03_couplers_WREADY;
  wire [3:3]xbar_to_m03_couplers_WVALID;
  wire [159:128]xbar_to_m04_couplers_ARADDR;
  wire xbar_to_m04_couplers_ARREADY;
  wire [4:4]xbar_to_m04_couplers_ARVALID;
  wire [159:128]xbar_to_m04_couplers_AWADDR;
  wire xbar_to_m04_couplers_AWREADY;
  wire [4:4]xbar_to_m04_couplers_AWVALID;
  wire [4:4]xbar_to_m04_couplers_BREADY;
  wire [1:0]xbar_to_m04_couplers_BRESP;
  wire xbar_to_m04_couplers_BVALID;
  wire [31:0]xbar_to_m04_couplers_RDATA;
  wire [4:4]xbar_to_m04_couplers_RREADY;
  wire [1:0]xbar_to_m04_couplers_RRESP;
  wire xbar_to_m04_couplers_RVALID;
  wire [159:128]xbar_to_m04_couplers_WDATA;
  wire xbar_to_m04_couplers_WREADY;
  wire [4:4]xbar_to_m04_couplers_WVALID;
  wire [191:160]xbar_to_m05_couplers_ARADDR;
  wire xbar_to_m05_couplers_ARREADY;
  wire [5:5]xbar_to_m05_couplers_ARVALID;
  wire [191:160]xbar_to_m05_couplers_AWADDR;
  wire xbar_to_m05_couplers_AWREADY;
  wire [5:5]xbar_to_m05_couplers_AWVALID;
  wire [5:5]xbar_to_m05_couplers_BREADY;
  wire [1:0]xbar_to_m05_couplers_BRESP;
  wire xbar_to_m05_couplers_BVALID;
  wire [31:0]xbar_to_m05_couplers_RDATA;
  wire [5:5]xbar_to_m05_couplers_RREADY;
  wire [1:0]xbar_to_m05_couplers_RRESP;
  wire xbar_to_m05_couplers_RVALID;
  wire [191:160]xbar_to_m05_couplers_WDATA;
  wire xbar_to_m05_couplers_WREADY;
  wire [5:5]xbar_to_m05_couplers_WVALID;
  wire [223:192]xbar_to_m06_couplers_ARADDR;
  wire xbar_to_m06_couplers_ARREADY;
  wire [6:6]xbar_to_m06_couplers_ARVALID;
  wire [223:192]xbar_to_m06_couplers_AWADDR;
  wire xbar_to_m06_couplers_AWREADY;
  wire [6:6]xbar_to_m06_couplers_AWVALID;
  wire [6:6]xbar_to_m06_couplers_BREADY;
  wire [1:0]xbar_to_m06_couplers_BRESP;
  wire xbar_to_m06_couplers_BVALID;
  wire [31:0]xbar_to_m06_couplers_RDATA;
  wire [6:6]xbar_to_m06_couplers_RREADY;
  wire [1:0]xbar_to_m06_couplers_RRESP;
  wire xbar_to_m06_couplers_RVALID;
  wire [223:192]xbar_to_m06_couplers_WDATA;
  wire xbar_to_m06_couplers_WREADY;
  wire [6:6]xbar_to_m06_couplers_WVALID;

  assign M00_ACLK_1 = M00_ACLK;
  assign M00_ARESETN_1 = M00_ARESETN[0];
  assign M00_AXI_araddr[31:0] = m00_couplers_to_microblaze_0_axi_periph_ARADDR;
  assign M00_AXI_arvalid[0] = m00_couplers_to_microblaze_0_axi_periph_ARVALID;
  assign M00_AXI_awaddr[31:0] = m00_couplers_to_microblaze_0_axi_periph_AWADDR;
  assign M00_AXI_awvalid[0] = m00_couplers_to_microblaze_0_axi_periph_AWVALID;
  assign M00_AXI_bready[0] = m00_couplers_to_microblaze_0_axi_periph_BREADY;
  assign M00_AXI_rready[0] = m00_couplers_to_microblaze_0_axi_periph_RREADY;
  assign M00_AXI_wdata[31:0] = m00_couplers_to_microblaze_0_axi_periph_WDATA;
  assign M00_AXI_wstrb[3:0] = m00_couplers_to_microblaze_0_axi_periph_WSTRB;
  assign M00_AXI_wvalid[0] = m00_couplers_to_microblaze_0_axi_periph_WVALID;
  assign M01_ACLK_1 = M01_ACLK;
  assign M01_ARESETN_1 = M01_ARESETN[0];
  assign M01_AXI_araddr[31:0] = m01_couplers_to_microblaze_0_axi_periph_ARADDR;
  assign M01_AXI_arvalid = m01_couplers_to_microblaze_0_axi_periph_ARVALID;
  assign M01_AXI_awaddr[31:0] = m01_couplers_to_microblaze_0_axi_periph_AWADDR;
  assign M01_AXI_awvalid = m01_couplers_to_microblaze_0_axi_periph_AWVALID;
  assign M01_AXI_bready = m01_couplers_to_microblaze_0_axi_periph_BREADY;
  assign M01_AXI_rready = m01_couplers_to_microblaze_0_axi_periph_RREADY;
  assign M01_AXI_wdata[31:0] = m01_couplers_to_microblaze_0_axi_periph_WDATA;
  assign M01_AXI_wstrb[3:0] = m01_couplers_to_microblaze_0_axi_periph_WSTRB;
  assign M01_AXI_wvalid = m01_couplers_to_microblaze_0_axi_periph_WVALID;
  assign M02_ACLK_1 = M02_ACLK;
  assign M02_ARESETN_1 = M02_ARESETN[0];
  assign M02_AXI_araddr[31:0] = m02_couplers_to_microblaze_0_axi_periph_ARADDR;
  assign M02_AXI_arvalid = m02_couplers_to_microblaze_0_axi_periph_ARVALID;
  assign M02_AXI_awaddr[31:0] = m02_couplers_to_microblaze_0_axi_periph_AWADDR;
  assign M02_AXI_awvalid = m02_couplers_to_microblaze_0_axi_periph_AWVALID;
  assign M02_AXI_bready = m02_couplers_to_microblaze_0_axi_periph_BREADY;
  assign M02_AXI_rready = m02_couplers_to_microblaze_0_axi_periph_RREADY;
  assign M02_AXI_wdata[31:0] = m02_couplers_to_microblaze_0_axi_periph_WDATA;
  assign M02_AXI_wstrb[3:0] = m02_couplers_to_microblaze_0_axi_periph_WSTRB;
  assign M02_AXI_wvalid = m02_couplers_to_microblaze_0_axi_periph_WVALID;
  assign M03_ACLK_1 = M03_ACLK;
  assign M03_ARESETN_1 = M03_ARESETN[0];
  assign M03_AXI_araddr[31:0] = m03_couplers_to_microblaze_0_axi_periph_ARADDR;
  assign M03_AXI_arvalid = m03_couplers_to_microblaze_0_axi_periph_ARVALID;
  assign M03_AXI_awaddr[31:0] = m03_couplers_to_microblaze_0_axi_periph_AWADDR;
  assign M03_AXI_awvalid = m03_couplers_to_microblaze_0_axi_periph_AWVALID;
  assign M03_AXI_bready = m03_couplers_to_microblaze_0_axi_periph_BREADY;
  assign M03_AXI_rready = m03_couplers_to_microblaze_0_axi_periph_RREADY;
  assign M03_AXI_wdata[31:0] = m03_couplers_to_microblaze_0_axi_periph_WDATA;
  assign M03_AXI_wvalid = m03_couplers_to_microblaze_0_axi_periph_WVALID;
  assign M04_ACLK_1 = M04_ACLK;
  assign M04_ARESETN_1 = M04_ARESETN[0];
  assign M04_AXI_araddr[31:0] = m04_couplers_to_microblaze_0_axi_periph_ARADDR;
  assign M04_AXI_arvalid = m04_couplers_to_microblaze_0_axi_periph_ARVALID;
  assign M04_AXI_awaddr[31:0] = m04_couplers_to_microblaze_0_axi_periph_AWADDR;
  assign M04_AXI_awvalid = m04_couplers_to_microblaze_0_axi_periph_AWVALID;
  assign M04_AXI_bready = m04_couplers_to_microblaze_0_axi_periph_BREADY;
  assign M04_AXI_rready = m04_couplers_to_microblaze_0_axi_periph_RREADY;
  assign M04_AXI_wdata[31:0] = m04_couplers_to_microblaze_0_axi_periph_WDATA;
  assign M04_AXI_wvalid = m04_couplers_to_microblaze_0_axi_periph_WVALID;
  assign M05_ACLK_1 = M05_ACLK;
  assign M05_ARESETN_1 = M05_ARESETN[0];
  assign M05_AXI_araddr[31:0] = m05_couplers_to_microblaze_0_axi_periph_ARADDR;
  assign M05_AXI_arvalid = m05_couplers_to_microblaze_0_axi_periph_ARVALID;
  assign M05_AXI_awaddr[31:0] = m05_couplers_to_microblaze_0_axi_periph_AWADDR;
  assign M05_AXI_awvalid = m05_couplers_to_microblaze_0_axi_periph_AWVALID;
  assign M05_AXI_bready = m05_couplers_to_microblaze_0_axi_periph_BREADY;
  assign M05_AXI_rready = m05_couplers_to_microblaze_0_axi_periph_RREADY;
  assign M05_AXI_wdata[31:0] = m05_couplers_to_microblaze_0_axi_periph_WDATA;
  assign M05_AXI_wvalid = m05_couplers_to_microblaze_0_axi_periph_WVALID;
  assign M06_ACLK_1 = M06_ACLK;
  assign M06_ARESETN_1 = M06_ARESETN[0];
  assign M06_AXI_araddr[31:0] = m06_couplers_to_microblaze_0_axi_periph_ARADDR;
  assign M06_AXI_arvalid = m06_couplers_to_microblaze_0_axi_periph_ARVALID;
  assign M06_AXI_awaddr[31:0] = m06_couplers_to_microblaze_0_axi_periph_AWADDR;
  assign M06_AXI_awvalid = m06_couplers_to_microblaze_0_axi_periph_AWVALID;
  assign M06_AXI_bready = m06_couplers_to_microblaze_0_axi_periph_BREADY;
  assign M06_AXI_rready = m06_couplers_to_microblaze_0_axi_periph_RREADY;
  assign M06_AXI_wdata[31:0] = m06_couplers_to_microblaze_0_axi_periph_WDATA;
  assign M06_AXI_wvalid = m06_couplers_to_microblaze_0_axi_periph_WVALID;
  assign S00_ACLK_1 = S00_ACLK;
  assign S00_ARESETN_1 = S00_ARESETN[0];
  assign S00_AXI_arready = microblaze_0_axi_periph_to_s00_couplers_ARREADY;
  assign S00_AXI_awready = microblaze_0_axi_periph_to_s00_couplers_AWREADY;
  assign S00_AXI_bresp[1:0] = microblaze_0_axi_periph_to_s00_couplers_BRESP;
  assign S00_AXI_bvalid = microblaze_0_axi_periph_to_s00_couplers_BVALID;
  assign S00_AXI_rdata[31:0] = microblaze_0_axi_periph_to_s00_couplers_RDATA;
  assign S00_AXI_rresp[1:0] = microblaze_0_axi_periph_to_s00_couplers_RRESP;
  assign S00_AXI_rvalid = microblaze_0_axi_periph_to_s00_couplers_RVALID;
  assign S00_AXI_wready = microblaze_0_axi_periph_to_s00_couplers_WREADY;
  assign m00_couplers_to_microblaze_0_axi_periph_ARREADY = M00_AXI_arready[0];
  assign m00_couplers_to_microblaze_0_axi_periph_AWREADY = M00_AXI_awready[0];
  assign m00_couplers_to_microblaze_0_axi_periph_BRESP = M00_AXI_bresp[1:0];
  assign m00_couplers_to_microblaze_0_axi_periph_BVALID = M00_AXI_bvalid[0];
  assign m00_couplers_to_microblaze_0_axi_periph_RDATA = M00_AXI_rdata[31:0];
  assign m00_couplers_to_microblaze_0_axi_periph_RRESP = M00_AXI_rresp[1:0];
  assign m00_couplers_to_microblaze_0_axi_periph_RVALID = M00_AXI_rvalid[0];
  assign m00_couplers_to_microblaze_0_axi_periph_WREADY = M00_AXI_wready[0];
  assign m01_couplers_to_microblaze_0_axi_periph_ARREADY = M01_AXI_arready;
  assign m01_couplers_to_microblaze_0_axi_periph_AWREADY = M01_AXI_awready;
  assign m01_couplers_to_microblaze_0_axi_periph_BRESP = M01_AXI_bresp[1:0];
  assign m01_couplers_to_microblaze_0_axi_periph_BVALID = M01_AXI_bvalid;
  assign m01_couplers_to_microblaze_0_axi_periph_RDATA = M01_AXI_rdata[31:0];
  assign m01_couplers_to_microblaze_0_axi_periph_RRESP = M01_AXI_rresp[1:0];
  assign m01_couplers_to_microblaze_0_axi_periph_RVALID = M01_AXI_rvalid;
  assign m01_couplers_to_microblaze_0_axi_periph_WREADY = M01_AXI_wready;
  assign m02_couplers_to_microblaze_0_axi_periph_ARREADY = M02_AXI_arready;
  assign m02_couplers_to_microblaze_0_axi_periph_AWREADY = M02_AXI_awready;
  assign m02_couplers_to_microblaze_0_axi_periph_BRESP = M02_AXI_bresp[1:0];
  assign m02_couplers_to_microblaze_0_axi_periph_BVALID = M02_AXI_bvalid;
  assign m02_couplers_to_microblaze_0_axi_periph_RDATA = M02_AXI_rdata[31:0];
  assign m02_couplers_to_microblaze_0_axi_periph_RRESP = M02_AXI_rresp[1:0];
  assign m02_couplers_to_microblaze_0_axi_periph_RVALID = M02_AXI_rvalid;
  assign m02_couplers_to_microblaze_0_axi_periph_WREADY = M02_AXI_wready;
  assign m03_couplers_to_microblaze_0_axi_periph_ARREADY = M03_AXI_arready;
  assign m03_couplers_to_microblaze_0_axi_periph_AWREADY = M03_AXI_awready;
  assign m03_couplers_to_microblaze_0_axi_periph_BRESP = M03_AXI_bresp[1:0];
  assign m03_couplers_to_microblaze_0_axi_periph_BVALID = M03_AXI_bvalid;
  assign m03_couplers_to_microblaze_0_axi_periph_RDATA = M03_AXI_rdata[31:0];
  assign m03_couplers_to_microblaze_0_axi_periph_RRESP = M03_AXI_rresp[1:0];
  assign m03_couplers_to_microblaze_0_axi_periph_RVALID = M03_AXI_rvalid;
  assign m03_couplers_to_microblaze_0_axi_periph_WREADY = M03_AXI_wready;
  assign m04_couplers_to_microblaze_0_axi_periph_ARREADY = M04_AXI_arready;
  assign m04_couplers_to_microblaze_0_axi_periph_AWREADY = M04_AXI_awready;
  assign m04_couplers_to_microblaze_0_axi_periph_BRESP = M04_AXI_bresp[1:0];
  assign m04_couplers_to_microblaze_0_axi_periph_BVALID = M04_AXI_bvalid;
  assign m04_couplers_to_microblaze_0_axi_periph_RDATA = M04_AXI_rdata[31:0];
  assign m04_couplers_to_microblaze_0_axi_periph_RRESP = M04_AXI_rresp[1:0];
  assign m04_couplers_to_microblaze_0_axi_periph_RVALID = M04_AXI_rvalid;
  assign m04_couplers_to_microblaze_0_axi_periph_WREADY = M04_AXI_wready;
  assign m05_couplers_to_microblaze_0_axi_periph_ARREADY = M05_AXI_arready;
  assign m05_couplers_to_microblaze_0_axi_periph_AWREADY = M05_AXI_awready;
  assign m05_couplers_to_microblaze_0_axi_periph_BRESP = M05_AXI_bresp[1:0];
  assign m05_couplers_to_microblaze_0_axi_periph_BVALID = M05_AXI_bvalid;
  assign m05_couplers_to_microblaze_0_axi_periph_RDATA = M05_AXI_rdata[31:0];
  assign m05_couplers_to_microblaze_0_axi_periph_RRESP = M05_AXI_rresp[1:0];
  assign m05_couplers_to_microblaze_0_axi_periph_RVALID = M05_AXI_rvalid;
  assign m05_couplers_to_microblaze_0_axi_periph_WREADY = M05_AXI_wready;
  assign m06_couplers_to_microblaze_0_axi_periph_ARREADY = M06_AXI_arready;
  assign m06_couplers_to_microblaze_0_axi_periph_AWREADY = M06_AXI_awready;
  assign m06_couplers_to_microblaze_0_axi_periph_BRESP = M06_AXI_bresp[1:0];
  assign m06_couplers_to_microblaze_0_axi_periph_BVALID = M06_AXI_bvalid;
  assign m06_couplers_to_microblaze_0_axi_periph_RDATA = M06_AXI_rdata[31:0];
  assign m06_couplers_to_microblaze_0_axi_periph_RRESP = M06_AXI_rresp[1:0];
  assign m06_couplers_to_microblaze_0_axi_periph_RVALID = M06_AXI_rvalid;
  assign m06_couplers_to_microblaze_0_axi_periph_WREADY = M06_AXI_wready;
  assign microblaze_0_axi_periph_ACLK_net = ACLK;
  assign microblaze_0_axi_periph_ARESETN_net = ARESETN[0];
  assign microblaze_0_axi_periph_to_s00_couplers_ARADDR = S00_AXI_araddr[31:0];
  assign microblaze_0_axi_periph_to_s00_couplers_ARPROT = S00_AXI_arprot[2:0];
  assign microblaze_0_axi_periph_to_s00_couplers_ARVALID = S00_AXI_arvalid;
  assign microblaze_0_axi_periph_to_s00_couplers_AWADDR = S00_AXI_awaddr[31:0];
  assign microblaze_0_axi_periph_to_s00_couplers_AWPROT = S00_AXI_awprot[2:0];
  assign microblaze_0_axi_periph_to_s00_couplers_AWVALID = S00_AXI_awvalid;
  assign microblaze_0_axi_periph_to_s00_couplers_BREADY = S00_AXI_bready;
  assign microblaze_0_axi_periph_to_s00_couplers_RREADY = S00_AXI_rready;
  assign microblaze_0_axi_periph_to_s00_couplers_WDATA = S00_AXI_wdata[31:0];
  assign microblaze_0_axi_periph_to_s00_couplers_WSTRB = S00_AXI_wstrb[3:0];
  assign microblaze_0_axi_periph_to_s00_couplers_WVALID = S00_AXI_wvalid;
  m00_couplers_imp_8RVYHO m00_couplers
       (.M_ACLK(M00_ACLK_1),
        .M_ARESETN(M00_ARESETN_1),
        .M_AXI_araddr(m00_couplers_to_microblaze_0_axi_periph_ARADDR),
        .M_AXI_arready(m00_couplers_to_microblaze_0_axi_periph_ARREADY),
        .M_AXI_arvalid(m00_couplers_to_microblaze_0_axi_periph_ARVALID),
        .M_AXI_awaddr(m00_couplers_to_microblaze_0_axi_periph_AWADDR),
        .M_AXI_awready(m00_couplers_to_microblaze_0_axi_periph_AWREADY),
        .M_AXI_awvalid(m00_couplers_to_microblaze_0_axi_periph_AWVALID),
        .M_AXI_bready(m00_couplers_to_microblaze_0_axi_periph_BREADY),
        .M_AXI_bresp(m00_couplers_to_microblaze_0_axi_periph_BRESP),
        .M_AXI_bvalid(m00_couplers_to_microblaze_0_axi_periph_BVALID),
        .M_AXI_rdata(m00_couplers_to_microblaze_0_axi_periph_RDATA),
        .M_AXI_rready(m00_couplers_to_microblaze_0_axi_periph_RREADY),
        .M_AXI_rresp(m00_couplers_to_microblaze_0_axi_periph_RRESP),
        .M_AXI_rvalid(m00_couplers_to_microblaze_0_axi_periph_RVALID),
        .M_AXI_wdata(m00_couplers_to_microblaze_0_axi_periph_WDATA),
        .M_AXI_wready(m00_couplers_to_microblaze_0_axi_periph_WREADY),
        .M_AXI_wstrb(m00_couplers_to_microblaze_0_axi_periph_WSTRB),
        .M_AXI_wvalid(m00_couplers_to_microblaze_0_axi_periph_WVALID),
        .S_ACLK(microblaze_0_axi_periph_ACLK_net),
        .S_ARESETN(microblaze_0_axi_periph_ARESETN_net),
        .S_AXI_araddr(xbar_to_m00_couplers_ARADDR),
        .S_AXI_arready(xbar_to_m00_couplers_ARREADY),
        .S_AXI_arvalid(xbar_to_m00_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m00_couplers_AWADDR),
        .S_AXI_awready(xbar_to_m00_couplers_AWREADY),
        .S_AXI_awvalid(xbar_to_m00_couplers_AWVALID),
        .S_AXI_bready(xbar_to_m00_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m00_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m00_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m00_couplers_RDATA),
        .S_AXI_rready(xbar_to_m00_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m00_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m00_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m00_couplers_WDATA),
        .S_AXI_wready(xbar_to_m00_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m00_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m00_couplers_WVALID));
  m01_couplers_imp_1UTB3Y5 m01_couplers
       (.M_ACLK(M01_ACLK_1),
        .M_ARESETN(M01_ARESETN_1),
        .M_AXI_araddr(m01_couplers_to_microblaze_0_axi_periph_ARADDR),
        .M_AXI_arready(m01_couplers_to_microblaze_0_axi_periph_ARREADY),
        .M_AXI_arvalid(m01_couplers_to_microblaze_0_axi_periph_ARVALID),
        .M_AXI_awaddr(m01_couplers_to_microblaze_0_axi_periph_AWADDR),
        .M_AXI_awready(m01_couplers_to_microblaze_0_axi_periph_AWREADY),
        .M_AXI_awvalid(m01_couplers_to_microblaze_0_axi_periph_AWVALID),
        .M_AXI_bready(m01_couplers_to_microblaze_0_axi_periph_BREADY),
        .M_AXI_bresp(m01_couplers_to_microblaze_0_axi_periph_BRESP),
        .M_AXI_bvalid(m01_couplers_to_microblaze_0_axi_periph_BVALID),
        .M_AXI_rdata(m01_couplers_to_microblaze_0_axi_periph_RDATA),
        .M_AXI_rready(m01_couplers_to_microblaze_0_axi_periph_RREADY),
        .M_AXI_rresp(m01_couplers_to_microblaze_0_axi_periph_RRESP),
        .M_AXI_rvalid(m01_couplers_to_microblaze_0_axi_periph_RVALID),
        .M_AXI_wdata(m01_couplers_to_microblaze_0_axi_periph_WDATA),
        .M_AXI_wready(m01_couplers_to_microblaze_0_axi_periph_WREADY),
        .M_AXI_wstrb(m01_couplers_to_microblaze_0_axi_periph_WSTRB),
        .M_AXI_wvalid(m01_couplers_to_microblaze_0_axi_periph_WVALID),
        .S_ACLK(microblaze_0_axi_periph_ACLK_net),
        .S_ARESETN(microblaze_0_axi_periph_ARESETN_net),
        .S_AXI_araddr(xbar_to_m01_couplers_ARADDR),
        .S_AXI_arready(xbar_to_m01_couplers_ARREADY),
        .S_AXI_arvalid(xbar_to_m01_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m01_couplers_AWADDR),
        .S_AXI_awready(xbar_to_m01_couplers_AWREADY),
        .S_AXI_awvalid(xbar_to_m01_couplers_AWVALID),
        .S_AXI_bready(xbar_to_m01_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m01_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m01_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m01_couplers_RDATA),
        .S_AXI_rready(xbar_to_m01_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m01_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m01_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m01_couplers_WDATA),
        .S_AXI_wready(xbar_to_m01_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m01_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m01_couplers_WVALID));
  m02_couplers_imp_7ANRHB m02_couplers
       (.M_ACLK(M02_ACLK_1),
        .M_ARESETN(M02_ARESETN_1),
        .M_AXI_araddr(m02_couplers_to_microblaze_0_axi_periph_ARADDR),
        .M_AXI_arready(m02_couplers_to_microblaze_0_axi_periph_ARREADY),
        .M_AXI_arvalid(m02_couplers_to_microblaze_0_axi_periph_ARVALID),
        .M_AXI_awaddr(m02_couplers_to_microblaze_0_axi_periph_AWADDR),
        .M_AXI_awready(m02_couplers_to_microblaze_0_axi_periph_AWREADY),
        .M_AXI_awvalid(m02_couplers_to_microblaze_0_axi_periph_AWVALID),
        .M_AXI_bready(m02_couplers_to_microblaze_0_axi_periph_BREADY),
        .M_AXI_bresp(m02_couplers_to_microblaze_0_axi_periph_BRESP),
        .M_AXI_bvalid(m02_couplers_to_microblaze_0_axi_periph_BVALID),
        .M_AXI_rdata(m02_couplers_to_microblaze_0_axi_periph_RDATA),
        .M_AXI_rready(m02_couplers_to_microblaze_0_axi_periph_RREADY),
        .M_AXI_rresp(m02_couplers_to_microblaze_0_axi_periph_RRESP),
        .M_AXI_rvalid(m02_couplers_to_microblaze_0_axi_periph_RVALID),
        .M_AXI_wdata(m02_couplers_to_microblaze_0_axi_periph_WDATA),
        .M_AXI_wready(m02_couplers_to_microblaze_0_axi_periph_WREADY),
        .M_AXI_wstrb(m02_couplers_to_microblaze_0_axi_periph_WSTRB),
        .M_AXI_wvalid(m02_couplers_to_microblaze_0_axi_periph_WVALID),
        .S_ACLK(microblaze_0_axi_periph_ACLK_net),
        .S_ARESETN(microblaze_0_axi_periph_ARESETN_net),
        .S_AXI_araddr(xbar_to_m02_couplers_ARADDR),
        .S_AXI_arready(xbar_to_m02_couplers_ARREADY),
        .S_AXI_arvalid(xbar_to_m02_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m02_couplers_AWADDR),
        .S_AXI_awready(xbar_to_m02_couplers_AWREADY),
        .S_AXI_awvalid(xbar_to_m02_couplers_AWVALID),
        .S_AXI_bready(xbar_to_m02_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m02_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m02_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m02_couplers_RDATA),
        .S_AXI_rready(xbar_to_m02_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m02_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m02_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m02_couplers_WDATA),
        .S_AXI_wready(xbar_to_m02_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m02_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m02_couplers_WVALID));
  m03_couplers_imp_1W07O72 m03_couplers
       (.M_ACLK(M03_ACLK_1),
        .M_ARESETN(M03_ARESETN_1),
        .M_AXI_araddr(m03_couplers_to_microblaze_0_axi_periph_ARADDR),
        .M_AXI_arready(m03_couplers_to_microblaze_0_axi_periph_ARREADY),
        .M_AXI_arvalid(m03_couplers_to_microblaze_0_axi_periph_ARVALID),
        .M_AXI_awaddr(m03_couplers_to_microblaze_0_axi_periph_AWADDR),
        .M_AXI_awready(m03_couplers_to_microblaze_0_axi_periph_AWREADY),
        .M_AXI_awvalid(m03_couplers_to_microblaze_0_axi_periph_AWVALID),
        .M_AXI_bready(m03_couplers_to_microblaze_0_axi_periph_BREADY),
        .M_AXI_bresp(m03_couplers_to_microblaze_0_axi_periph_BRESP),
        .M_AXI_bvalid(m03_couplers_to_microblaze_0_axi_periph_BVALID),
        .M_AXI_rdata(m03_couplers_to_microblaze_0_axi_periph_RDATA),
        .M_AXI_rready(m03_couplers_to_microblaze_0_axi_periph_RREADY),
        .M_AXI_rresp(m03_couplers_to_microblaze_0_axi_periph_RRESP),
        .M_AXI_rvalid(m03_couplers_to_microblaze_0_axi_periph_RVALID),
        .M_AXI_wdata(m03_couplers_to_microblaze_0_axi_periph_WDATA),
        .M_AXI_wready(m03_couplers_to_microblaze_0_axi_periph_WREADY),
        .M_AXI_wvalid(m03_couplers_to_microblaze_0_axi_periph_WVALID),
        .S_ACLK(microblaze_0_axi_periph_ACLK_net),
        .S_ARESETN(microblaze_0_axi_periph_ARESETN_net),
        .S_AXI_araddr(xbar_to_m03_couplers_ARADDR),
        .S_AXI_arready(xbar_to_m03_couplers_ARREADY),
        .S_AXI_arvalid(xbar_to_m03_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m03_couplers_AWADDR),
        .S_AXI_awready(xbar_to_m03_couplers_AWREADY),
        .S_AXI_awvalid(xbar_to_m03_couplers_AWVALID),
        .S_AXI_bready(xbar_to_m03_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m03_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m03_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m03_couplers_RDATA),
        .S_AXI_rready(xbar_to_m03_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m03_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m03_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m03_couplers_WDATA),
        .S_AXI_wready(xbar_to_m03_couplers_WREADY),
        .S_AXI_wvalid(xbar_to_m03_couplers_WVALID));
  m04_couplers_imp_5LX7BU m04_couplers
       (.M_ACLK(M04_ACLK_1),
        .M_ARESETN(M04_ARESETN_1),
        .M_AXI_araddr(m04_couplers_to_microblaze_0_axi_periph_ARADDR),
        .M_AXI_arready(m04_couplers_to_microblaze_0_axi_periph_ARREADY),
        .M_AXI_arvalid(m04_couplers_to_microblaze_0_axi_periph_ARVALID),
        .M_AXI_awaddr(m04_couplers_to_microblaze_0_axi_periph_AWADDR),
        .M_AXI_awready(m04_couplers_to_microblaze_0_axi_periph_AWREADY),
        .M_AXI_awvalid(m04_couplers_to_microblaze_0_axi_periph_AWVALID),
        .M_AXI_bready(m04_couplers_to_microblaze_0_axi_periph_BREADY),
        .M_AXI_bresp(m04_couplers_to_microblaze_0_axi_periph_BRESP),
        .M_AXI_bvalid(m04_couplers_to_microblaze_0_axi_periph_BVALID),
        .M_AXI_rdata(m04_couplers_to_microblaze_0_axi_periph_RDATA),
        .M_AXI_rready(m04_couplers_to_microblaze_0_axi_periph_RREADY),
        .M_AXI_rresp(m04_couplers_to_microblaze_0_axi_periph_RRESP),
        .M_AXI_rvalid(m04_couplers_to_microblaze_0_axi_periph_RVALID),
        .M_AXI_wdata(m04_couplers_to_microblaze_0_axi_periph_WDATA),
        .M_AXI_wready(m04_couplers_to_microblaze_0_axi_periph_WREADY),
        .M_AXI_wvalid(m04_couplers_to_microblaze_0_axi_periph_WVALID),
        .S_ACLK(microblaze_0_axi_periph_ACLK_net),
        .S_ARESETN(microblaze_0_axi_periph_ARESETN_net),
        .S_AXI_araddr(xbar_to_m04_couplers_ARADDR),
        .S_AXI_arready(xbar_to_m04_couplers_ARREADY),
        .S_AXI_arvalid(xbar_to_m04_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m04_couplers_AWADDR),
        .S_AXI_awready(xbar_to_m04_couplers_AWREADY),
        .S_AXI_awvalid(xbar_to_m04_couplers_AWVALID),
        .S_AXI_bready(xbar_to_m04_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m04_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m04_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m04_couplers_RDATA),
        .S_AXI_rready(xbar_to_m04_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m04_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m04_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m04_couplers_WDATA),
        .S_AXI_wready(xbar_to_m04_couplers_WREADY),
        .S_AXI_wvalid(xbar_to_m04_couplers_WVALID));
  m05_couplers_imp_1XR4ZAZ m05_couplers
       (.M_ACLK(M05_ACLK_1),
        .M_ARESETN(M05_ARESETN_1),
        .M_AXI_araddr(m05_couplers_to_microblaze_0_axi_periph_ARADDR),
        .M_AXI_arready(m05_couplers_to_microblaze_0_axi_periph_ARREADY),
        .M_AXI_arvalid(m05_couplers_to_microblaze_0_axi_periph_ARVALID),
        .M_AXI_awaddr(m05_couplers_to_microblaze_0_axi_periph_AWADDR),
        .M_AXI_awready(m05_couplers_to_microblaze_0_axi_periph_AWREADY),
        .M_AXI_awvalid(m05_couplers_to_microblaze_0_axi_periph_AWVALID),
        .M_AXI_bready(m05_couplers_to_microblaze_0_axi_periph_BREADY),
        .M_AXI_bresp(m05_couplers_to_microblaze_0_axi_periph_BRESP),
        .M_AXI_bvalid(m05_couplers_to_microblaze_0_axi_periph_BVALID),
        .M_AXI_rdata(m05_couplers_to_microblaze_0_axi_periph_RDATA),
        .M_AXI_rready(m05_couplers_to_microblaze_0_axi_periph_RREADY),
        .M_AXI_rresp(m05_couplers_to_microblaze_0_axi_periph_RRESP),
        .M_AXI_rvalid(m05_couplers_to_microblaze_0_axi_periph_RVALID),
        .M_AXI_wdata(m05_couplers_to_microblaze_0_axi_periph_WDATA),
        .M_AXI_wready(m05_couplers_to_microblaze_0_axi_periph_WREADY),
        .M_AXI_wvalid(m05_couplers_to_microblaze_0_axi_periph_WVALID),
        .S_ACLK(microblaze_0_axi_periph_ACLK_net),
        .S_ARESETN(microblaze_0_axi_periph_ARESETN_net),
        .S_AXI_araddr(xbar_to_m05_couplers_ARADDR),
        .S_AXI_arready(xbar_to_m05_couplers_ARREADY),
        .S_AXI_arvalid(xbar_to_m05_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m05_couplers_AWADDR),
        .S_AXI_awready(xbar_to_m05_couplers_AWREADY),
        .S_AXI_awvalid(xbar_to_m05_couplers_AWVALID),
        .S_AXI_bready(xbar_to_m05_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m05_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m05_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m05_couplers_RDATA),
        .S_AXI_rready(xbar_to_m05_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m05_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m05_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m05_couplers_WDATA),
        .S_AXI_wready(xbar_to_m05_couplers_WREADY),
        .S_AXI_wvalid(xbar_to_m05_couplers_WVALID));
  m06_couplers_imp_4YOIXL m06_couplers
       (.M_ACLK(M06_ACLK_1),
        .M_ARESETN(M06_ARESETN_1),
        .M_AXI_araddr(m06_couplers_to_microblaze_0_axi_periph_ARADDR),
        .M_AXI_arready(m06_couplers_to_microblaze_0_axi_periph_ARREADY),
        .M_AXI_arvalid(m06_couplers_to_microblaze_0_axi_periph_ARVALID),
        .M_AXI_awaddr(m06_couplers_to_microblaze_0_axi_periph_AWADDR),
        .M_AXI_awready(m06_couplers_to_microblaze_0_axi_periph_AWREADY),
        .M_AXI_awvalid(m06_couplers_to_microblaze_0_axi_periph_AWVALID),
        .M_AXI_bready(m06_couplers_to_microblaze_0_axi_periph_BREADY),
        .M_AXI_bresp(m06_couplers_to_microblaze_0_axi_periph_BRESP),
        .M_AXI_bvalid(m06_couplers_to_microblaze_0_axi_periph_BVALID),
        .M_AXI_rdata(m06_couplers_to_microblaze_0_axi_periph_RDATA),
        .M_AXI_rready(m06_couplers_to_microblaze_0_axi_periph_RREADY),
        .M_AXI_rresp(m06_couplers_to_microblaze_0_axi_periph_RRESP),
        .M_AXI_rvalid(m06_couplers_to_microblaze_0_axi_periph_RVALID),
        .M_AXI_wdata(m06_couplers_to_microblaze_0_axi_periph_WDATA),
        .M_AXI_wready(m06_couplers_to_microblaze_0_axi_periph_WREADY),
        .M_AXI_wvalid(m06_couplers_to_microblaze_0_axi_periph_WVALID),
        .S_ACLK(microblaze_0_axi_periph_ACLK_net),
        .S_ARESETN(microblaze_0_axi_periph_ARESETN_net),
        .S_AXI_araddr(xbar_to_m06_couplers_ARADDR),
        .S_AXI_arready(xbar_to_m06_couplers_ARREADY),
        .S_AXI_arvalid(xbar_to_m06_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m06_couplers_AWADDR),
        .S_AXI_awready(xbar_to_m06_couplers_AWREADY),
        .S_AXI_awvalid(xbar_to_m06_couplers_AWVALID),
        .S_AXI_bready(xbar_to_m06_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m06_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m06_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m06_couplers_RDATA),
        .S_AXI_rready(xbar_to_m06_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m06_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m06_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m06_couplers_WDATA),
        .S_AXI_wready(xbar_to_m06_couplers_WREADY),
        .S_AXI_wvalid(xbar_to_m06_couplers_WVALID));
  s00_couplers_imp_1RZP34U s00_couplers
       (.M_ACLK(microblaze_0_axi_periph_ACLK_net),
        .M_ARESETN(microblaze_0_axi_periph_ARESETN_net),
        .M_AXI_araddr(s00_couplers_to_xbar_ARADDR),
        .M_AXI_arprot(s00_couplers_to_xbar_ARPROT),
        .M_AXI_arready(s00_couplers_to_xbar_ARREADY),
        .M_AXI_arvalid(s00_couplers_to_xbar_ARVALID),
        .M_AXI_awaddr(s00_couplers_to_xbar_AWADDR),
        .M_AXI_awprot(s00_couplers_to_xbar_AWPROT),
        .M_AXI_awready(s00_couplers_to_xbar_AWREADY),
        .M_AXI_awvalid(s00_couplers_to_xbar_AWVALID),
        .M_AXI_bready(s00_couplers_to_xbar_BREADY),
        .M_AXI_bresp(s00_couplers_to_xbar_BRESP),
        .M_AXI_bvalid(s00_couplers_to_xbar_BVALID),
        .M_AXI_rdata(s00_couplers_to_xbar_RDATA),
        .M_AXI_rready(s00_couplers_to_xbar_RREADY),
        .M_AXI_rresp(s00_couplers_to_xbar_RRESP),
        .M_AXI_rvalid(s00_couplers_to_xbar_RVALID),
        .M_AXI_wdata(s00_couplers_to_xbar_WDATA),
        .M_AXI_wready(s00_couplers_to_xbar_WREADY),
        .M_AXI_wstrb(s00_couplers_to_xbar_WSTRB),
        .M_AXI_wvalid(s00_couplers_to_xbar_WVALID),
        .S_ACLK(S00_ACLK_1),
        .S_ARESETN(S00_ARESETN_1),
        .S_AXI_araddr(microblaze_0_axi_periph_to_s00_couplers_ARADDR),
        .S_AXI_arprot(microblaze_0_axi_periph_to_s00_couplers_ARPROT),
        .S_AXI_arready(microblaze_0_axi_periph_to_s00_couplers_ARREADY),
        .S_AXI_arvalid(microblaze_0_axi_periph_to_s00_couplers_ARVALID),
        .S_AXI_awaddr(microblaze_0_axi_periph_to_s00_couplers_AWADDR),
        .S_AXI_awprot(microblaze_0_axi_periph_to_s00_couplers_AWPROT),
        .S_AXI_awready(microblaze_0_axi_periph_to_s00_couplers_AWREADY),
        .S_AXI_awvalid(microblaze_0_axi_periph_to_s00_couplers_AWVALID),
        .S_AXI_bready(microblaze_0_axi_periph_to_s00_couplers_BREADY),
        .S_AXI_bresp(microblaze_0_axi_periph_to_s00_couplers_BRESP),
        .S_AXI_bvalid(microblaze_0_axi_periph_to_s00_couplers_BVALID),
        .S_AXI_rdata(microblaze_0_axi_periph_to_s00_couplers_RDATA),
        .S_AXI_rready(microblaze_0_axi_periph_to_s00_couplers_RREADY),
        .S_AXI_rresp(microblaze_0_axi_periph_to_s00_couplers_RRESP),
        .S_AXI_rvalid(microblaze_0_axi_periph_to_s00_couplers_RVALID),
        .S_AXI_wdata(microblaze_0_axi_periph_to_s00_couplers_WDATA),
        .S_AXI_wready(microblaze_0_axi_periph_to_s00_couplers_WREADY),
        .S_AXI_wstrb(microblaze_0_axi_periph_to_s00_couplers_WSTRB),
        .S_AXI_wvalid(microblaze_0_axi_periph_to_s00_couplers_WVALID));
  design_1_xbar_1 xbar
       (.aclk(microblaze_0_axi_periph_ACLK_net),
        .aresetn(microblaze_0_axi_periph_ARESETN_net),
        .m_axi_araddr({xbar_to_m06_couplers_ARADDR,xbar_to_m05_couplers_ARADDR,xbar_to_m04_couplers_ARADDR,xbar_to_m03_couplers_ARADDR,xbar_to_m02_couplers_ARADDR,xbar_to_m01_couplers_ARADDR,xbar_to_m00_couplers_ARADDR}),
        .m_axi_arready({xbar_to_m06_couplers_ARREADY,xbar_to_m05_couplers_ARREADY,xbar_to_m04_couplers_ARREADY,xbar_to_m03_couplers_ARREADY,xbar_to_m02_couplers_ARREADY,xbar_to_m01_couplers_ARREADY,xbar_to_m00_couplers_ARREADY}),
        .m_axi_arvalid({xbar_to_m06_couplers_ARVALID,xbar_to_m05_couplers_ARVALID,xbar_to_m04_couplers_ARVALID,xbar_to_m03_couplers_ARVALID,xbar_to_m02_couplers_ARVALID,xbar_to_m01_couplers_ARVALID,xbar_to_m00_couplers_ARVALID}),
        .m_axi_awaddr({xbar_to_m06_couplers_AWADDR,xbar_to_m05_couplers_AWADDR,xbar_to_m04_couplers_AWADDR,xbar_to_m03_couplers_AWADDR,xbar_to_m02_couplers_AWADDR,xbar_to_m01_couplers_AWADDR,xbar_to_m00_couplers_AWADDR}),
        .m_axi_awready({xbar_to_m06_couplers_AWREADY,xbar_to_m05_couplers_AWREADY,xbar_to_m04_couplers_AWREADY,xbar_to_m03_couplers_AWREADY,xbar_to_m02_couplers_AWREADY,xbar_to_m01_couplers_AWREADY,xbar_to_m00_couplers_AWREADY}),
        .m_axi_awvalid({xbar_to_m06_couplers_AWVALID,xbar_to_m05_couplers_AWVALID,xbar_to_m04_couplers_AWVALID,xbar_to_m03_couplers_AWVALID,xbar_to_m02_couplers_AWVALID,xbar_to_m01_couplers_AWVALID,xbar_to_m00_couplers_AWVALID}),
        .m_axi_bready({xbar_to_m06_couplers_BREADY,xbar_to_m05_couplers_BREADY,xbar_to_m04_couplers_BREADY,xbar_to_m03_couplers_BREADY,xbar_to_m02_couplers_BREADY,xbar_to_m01_couplers_BREADY,xbar_to_m00_couplers_BREADY}),
        .m_axi_bresp({xbar_to_m06_couplers_BRESP,xbar_to_m05_couplers_BRESP,xbar_to_m04_couplers_BRESP,xbar_to_m03_couplers_BRESP,xbar_to_m02_couplers_BRESP,xbar_to_m01_couplers_BRESP,xbar_to_m00_couplers_BRESP}),
        .m_axi_bvalid({xbar_to_m06_couplers_BVALID,xbar_to_m05_couplers_BVALID,xbar_to_m04_couplers_BVALID,xbar_to_m03_couplers_BVALID,xbar_to_m02_couplers_BVALID,xbar_to_m01_couplers_BVALID,xbar_to_m00_couplers_BVALID}),
        .m_axi_rdata({xbar_to_m06_couplers_RDATA,xbar_to_m05_couplers_RDATA,xbar_to_m04_couplers_RDATA,xbar_to_m03_couplers_RDATA,xbar_to_m02_couplers_RDATA,xbar_to_m01_couplers_RDATA,xbar_to_m00_couplers_RDATA}),
        .m_axi_rready({xbar_to_m06_couplers_RREADY,xbar_to_m05_couplers_RREADY,xbar_to_m04_couplers_RREADY,xbar_to_m03_couplers_RREADY,xbar_to_m02_couplers_RREADY,xbar_to_m01_couplers_RREADY,xbar_to_m00_couplers_RREADY}),
        .m_axi_rresp({xbar_to_m06_couplers_RRESP,xbar_to_m05_couplers_RRESP,xbar_to_m04_couplers_RRESP,xbar_to_m03_couplers_RRESP,xbar_to_m02_couplers_RRESP,xbar_to_m01_couplers_RRESP,xbar_to_m00_couplers_RRESP}),
        .m_axi_rvalid({xbar_to_m06_couplers_RVALID,xbar_to_m05_couplers_RVALID,xbar_to_m04_couplers_RVALID,xbar_to_m03_couplers_RVALID,xbar_to_m02_couplers_RVALID,xbar_to_m01_couplers_RVALID,xbar_to_m00_couplers_RVALID}),
        .m_axi_wdata({xbar_to_m06_couplers_WDATA,xbar_to_m05_couplers_WDATA,xbar_to_m04_couplers_WDATA,xbar_to_m03_couplers_WDATA,xbar_to_m02_couplers_WDATA,xbar_to_m01_couplers_WDATA,xbar_to_m00_couplers_WDATA}),
        .m_axi_wready({xbar_to_m06_couplers_WREADY,xbar_to_m05_couplers_WREADY,xbar_to_m04_couplers_WREADY,xbar_to_m03_couplers_WREADY,xbar_to_m02_couplers_WREADY,xbar_to_m01_couplers_WREADY,xbar_to_m00_couplers_WREADY}),
        .m_axi_wstrb({xbar_to_m02_couplers_WSTRB,xbar_to_m01_couplers_WSTRB,xbar_to_m00_couplers_WSTRB}),
        .m_axi_wvalid({xbar_to_m06_couplers_WVALID,xbar_to_m05_couplers_WVALID,xbar_to_m04_couplers_WVALID,xbar_to_m03_couplers_WVALID,xbar_to_m02_couplers_WVALID,xbar_to_m01_couplers_WVALID,xbar_to_m00_couplers_WVALID}),
        .s_axi_araddr(s00_couplers_to_xbar_ARADDR),
        .s_axi_arprot(s00_couplers_to_xbar_ARPROT),
        .s_axi_arready(s00_couplers_to_xbar_ARREADY),
        .s_axi_arvalid(s00_couplers_to_xbar_ARVALID),
        .s_axi_awaddr(s00_couplers_to_xbar_AWADDR),
        .s_axi_awprot(s00_couplers_to_xbar_AWPROT),
        .s_axi_awready(s00_couplers_to_xbar_AWREADY),
        .s_axi_awvalid(s00_couplers_to_xbar_AWVALID),
        .s_axi_bready(s00_couplers_to_xbar_BREADY),
        .s_axi_bresp(s00_couplers_to_xbar_BRESP),
        .s_axi_bvalid(s00_couplers_to_xbar_BVALID),
        .s_axi_rdata(s00_couplers_to_xbar_RDATA),
        .s_axi_rready(s00_couplers_to_xbar_RREADY),
        .s_axi_rresp(s00_couplers_to_xbar_RRESP),
        .s_axi_rvalid(s00_couplers_to_xbar_RVALID),
        .s_axi_wdata(s00_couplers_to_xbar_WDATA),
        .s_axi_wready(s00_couplers_to_xbar_WREADY),
        .s_axi_wstrb(s00_couplers_to_xbar_WSTRB),
        .s_axi_wvalid(s00_couplers_to_xbar_WVALID));
endmodule

module m00_couplers_imp_1R706YB
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arburst,
    M_AXI_arcache,
    M_AXI_arid,
    M_AXI_arlen,
    M_AXI_arlock,
    M_AXI_arprot,
    M_AXI_arqos,
    M_AXI_arready,
    M_AXI_arsize,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awburst,
    M_AXI_awcache,
    M_AXI_awid,
    M_AXI_awlen,
    M_AXI_awlock,
    M_AXI_awprot,
    M_AXI_awqos,
    M_AXI_awready,
    M_AXI_awsize,
    M_AXI_awvalid,
    M_AXI_bid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rid,
    M_AXI_rlast,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wlast,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arburst,
    S_AXI_arcache,
    S_AXI_arid,
    S_AXI_arlen,
    S_AXI_arlock,
    S_AXI_arprot,
    S_AXI_arqos,
    S_AXI_arready,
    S_AXI_arsize,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awburst,
    S_AXI_awcache,
    S_AXI_awid,
    S_AXI_awlen,
    S_AXI_awlock,
    S_AXI_awprot,
    S_AXI_awqos,
    S_AXI_awready,
    S_AXI_awsize,
    S_AXI_awvalid,
    S_AXI_bid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rid,
    S_AXI_rlast,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wlast,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [1:0]M_AXI_arburst;
  output [3:0]M_AXI_arcache;
  output [3:0]M_AXI_arid;
  output [7:0]M_AXI_arlen;
  output [0:0]M_AXI_arlock;
  output [2:0]M_AXI_arprot;
  output [3:0]M_AXI_arqos;
  input [0:0]M_AXI_arready;
  output [2:0]M_AXI_arsize;
  output [0:0]M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [1:0]M_AXI_awburst;
  output [3:0]M_AXI_awcache;
  output [3:0]M_AXI_awid;
  output [7:0]M_AXI_awlen;
  output [0:0]M_AXI_awlock;
  output [2:0]M_AXI_awprot;
  output [3:0]M_AXI_awqos;
  input [0:0]M_AXI_awready;
  output [2:0]M_AXI_awsize;
  output [0:0]M_AXI_awvalid;
  input [3:0]M_AXI_bid;
  output [0:0]M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input [0:0]M_AXI_bvalid;
  input [511:0]M_AXI_rdata;
  input [3:0]M_AXI_rid;
  input [0:0]M_AXI_rlast;
  output [0:0]M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input [0:0]M_AXI_rvalid;
  output [511:0]M_AXI_wdata;
  output [0:0]M_AXI_wlast;
  input [0:0]M_AXI_wready;
  output [63:0]M_AXI_wstrb;
  output [0:0]M_AXI_wvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [1:0]S_AXI_arburst;
  input [3:0]S_AXI_arcache;
  input [3:0]S_AXI_arid;
  input [7:0]S_AXI_arlen;
  input [0:0]S_AXI_arlock;
  input [2:0]S_AXI_arprot;
  input [3:0]S_AXI_arqos;
  output [0:0]S_AXI_arready;
  input [2:0]S_AXI_arsize;
  input [0:0]S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [1:0]S_AXI_awburst;
  input [3:0]S_AXI_awcache;
  input [3:0]S_AXI_awid;
  input [7:0]S_AXI_awlen;
  input [0:0]S_AXI_awlock;
  input [2:0]S_AXI_awprot;
  input [3:0]S_AXI_awqos;
  output [0:0]S_AXI_awready;
  input [2:0]S_AXI_awsize;
  input [0:0]S_AXI_awvalid;
  output [3:0]S_AXI_bid;
  input [0:0]S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output [0:0]S_AXI_bvalid;
  output [511:0]S_AXI_rdata;
  output [3:0]S_AXI_rid;
  output [0:0]S_AXI_rlast;
  input [0:0]S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output [0:0]S_AXI_rvalid;
  input [511:0]S_AXI_wdata;
  input [0:0]S_AXI_wlast;
  output [0:0]S_AXI_wready;
  input [63:0]S_AXI_wstrb;
  input [0:0]S_AXI_wvalid;

  wire [31:0]m00_couplers_to_m00_couplers_ARADDR;
  wire [1:0]m00_couplers_to_m00_couplers_ARBURST;
  wire [3:0]m00_couplers_to_m00_couplers_ARCACHE;
  wire [3:0]m00_couplers_to_m00_couplers_ARID;
  wire [7:0]m00_couplers_to_m00_couplers_ARLEN;
  wire [0:0]m00_couplers_to_m00_couplers_ARLOCK;
  wire [2:0]m00_couplers_to_m00_couplers_ARPROT;
  wire [3:0]m00_couplers_to_m00_couplers_ARQOS;
  wire [0:0]m00_couplers_to_m00_couplers_ARREADY;
  wire [2:0]m00_couplers_to_m00_couplers_ARSIZE;
  wire [0:0]m00_couplers_to_m00_couplers_ARVALID;
  wire [31:0]m00_couplers_to_m00_couplers_AWADDR;
  wire [1:0]m00_couplers_to_m00_couplers_AWBURST;
  wire [3:0]m00_couplers_to_m00_couplers_AWCACHE;
  wire [3:0]m00_couplers_to_m00_couplers_AWID;
  wire [7:0]m00_couplers_to_m00_couplers_AWLEN;
  wire [0:0]m00_couplers_to_m00_couplers_AWLOCK;
  wire [2:0]m00_couplers_to_m00_couplers_AWPROT;
  wire [3:0]m00_couplers_to_m00_couplers_AWQOS;
  wire [0:0]m00_couplers_to_m00_couplers_AWREADY;
  wire [2:0]m00_couplers_to_m00_couplers_AWSIZE;
  wire [0:0]m00_couplers_to_m00_couplers_AWVALID;
  wire [3:0]m00_couplers_to_m00_couplers_BID;
  wire [0:0]m00_couplers_to_m00_couplers_BREADY;
  wire [1:0]m00_couplers_to_m00_couplers_BRESP;
  wire [0:0]m00_couplers_to_m00_couplers_BVALID;
  wire [511:0]m00_couplers_to_m00_couplers_RDATA;
  wire [3:0]m00_couplers_to_m00_couplers_RID;
  wire [0:0]m00_couplers_to_m00_couplers_RLAST;
  wire [0:0]m00_couplers_to_m00_couplers_RREADY;
  wire [1:0]m00_couplers_to_m00_couplers_RRESP;
  wire [0:0]m00_couplers_to_m00_couplers_RVALID;
  wire [511:0]m00_couplers_to_m00_couplers_WDATA;
  wire [0:0]m00_couplers_to_m00_couplers_WLAST;
  wire [0:0]m00_couplers_to_m00_couplers_WREADY;
  wire [63:0]m00_couplers_to_m00_couplers_WSTRB;
  wire [0:0]m00_couplers_to_m00_couplers_WVALID;

  assign M_AXI_araddr[31:0] = m00_couplers_to_m00_couplers_ARADDR;
  assign M_AXI_arburst[1:0] = m00_couplers_to_m00_couplers_ARBURST;
  assign M_AXI_arcache[3:0] = m00_couplers_to_m00_couplers_ARCACHE;
  assign M_AXI_arid[3:0] = m00_couplers_to_m00_couplers_ARID;
  assign M_AXI_arlen[7:0] = m00_couplers_to_m00_couplers_ARLEN;
  assign M_AXI_arlock[0] = m00_couplers_to_m00_couplers_ARLOCK;
  assign M_AXI_arprot[2:0] = m00_couplers_to_m00_couplers_ARPROT;
  assign M_AXI_arqos[3:0] = m00_couplers_to_m00_couplers_ARQOS;
  assign M_AXI_arsize[2:0] = m00_couplers_to_m00_couplers_ARSIZE;
  assign M_AXI_arvalid[0] = m00_couplers_to_m00_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = m00_couplers_to_m00_couplers_AWADDR;
  assign M_AXI_awburst[1:0] = m00_couplers_to_m00_couplers_AWBURST;
  assign M_AXI_awcache[3:0] = m00_couplers_to_m00_couplers_AWCACHE;
  assign M_AXI_awid[3:0] = m00_couplers_to_m00_couplers_AWID;
  assign M_AXI_awlen[7:0] = m00_couplers_to_m00_couplers_AWLEN;
  assign M_AXI_awlock[0] = m00_couplers_to_m00_couplers_AWLOCK;
  assign M_AXI_awprot[2:0] = m00_couplers_to_m00_couplers_AWPROT;
  assign M_AXI_awqos[3:0] = m00_couplers_to_m00_couplers_AWQOS;
  assign M_AXI_awsize[2:0] = m00_couplers_to_m00_couplers_AWSIZE;
  assign M_AXI_awvalid[0] = m00_couplers_to_m00_couplers_AWVALID;
  assign M_AXI_bready[0] = m00_couplers_to_m00_couplers_BREADY;
  assign M_AXI_rready[0] = m00_couplers_to_m00_couplers_RREADY;
  assign M_AXI_wdata[511:0] = m00_couplers_to_m00_couplers_WDATA;
  assign M_AXI_wlast[0] = m00_couplers_to_m00_couplers_WLAST;
  assign M_AXI_wstrb[63:0] = m00_couplers_to_m00_couplers_WSTRB;
  assign M_AXI_wvalid[0] = m00_couplers_to_m00_couplers_WVALID;
  assign S_AXI_arready[0] = m00_couplers_to_m00_couplers_ARREADY;
  assign S_AXI_awready[0] = m00_couplers_to_m00_couplers_AWREADY;
  assign S_AXI_bid[3:0] = m00_couplers_to_m00_couplers_BID;
  assign S_AXI_bresp[1:0] = m00_couplers_to_m00_couplers_BRESP;
  assign S_AXI_bvalid[0] = m00_couplers_to_m00_couplers_BVALID;
  assign S_AXI_rdata[511:0] = m00_couplers_to_m00_couplers_RDATA;
  assign S_AXI_rid[3:0] = m00_couplers_to_m00_couplers_RID;
  assign S_AXI_rlast[0] = m00_couplers_to_m00_couplers_RLAST;
  assign S_AXI_rresp[1:0] = m00_couplers_to_m00_couplers_RRESP;
  assign S_AXI_rvalid[0] = m00_couplers_to_m00_couplers_RVALID;
  assign S_AXI_wready[0] = m00_couplers_to_m00_couplers_WREADY;
  assign m00_couplers_to_m00_couplers_ARADDR = S_AXI_araddr[31:0];
  assign m00_couplers_to_m00_couplers_ARBURST = S_AXI_arburst[1:0];
  assign m00_couplers_to_m00_couplers_ARCACHE = S_AXI_arcache[3:0];
  assign m00_couplers_to_m00_couplers_ARID = S_AXI_arid[3:0];
  assign m00_couplers_to_m00_couplers_ARLEN = S_AXI_arlen[7:0];
  assign m00_couplers_to_m00_couplers_ARLOCK = S_AXI_arlock[0];
  assign m00_couplers_to_m00_couplers_ARPROT = S_AXI_arprot[2:0];
  assign m00_couplers_to_m00_couplers_ARQOS = S_AXI_arqos[3:0];
  assign m00_couplers_to_m00_couplers_ARREADY = M_AXI_arready[0];
  assign m00_couplers_to_m00_couplers_ARSIZE = S_AXI_arsize[2:0];
  assign m00_couplers_to_m00_couplers_ARVALID = S_AXI_arvalid[0];
  assign m00_couplers_to_m00_couplers_AWADDR = S_AXI_awaddr[31:0];
  assign m00_couplers_to_m00_couplers_AWBURST = S_AXI_awburst[1:0];
  assign m00_couplers_to_m00_couplers_AWCACHE = S_AXI_awcache[3:0];
  assign m00_couplers_to_m00_couplers_AWID = S_AXI_awid[3:0];
  assign m00_couplers_to_m00_couplers_AWLEN = S_AXI_awlen[7:0];
  assign m00_couplers_to_m00_couplers_AWLOCK = S_AXI_awlock[0];
  assign m00_couplers_to_m00_couplers_AWPROT = S_AXI_awprot[2:0];
  assign m00_couplers_to_m00_couplers_AWQOS = S_AXI_awqos[3:0];
  assign m00_couplers_to_m00_couplers_AWREADY = M_AXI_awready[0];
  assign m00_couplers_to_m00_couplers_AWSIZE = S_AXI_awsize[2:0];
  assign m00_couplers_to_m00_couplers_AWVALID = S_AXI_awvalid[0];
  assign m00_couplers_to_m00_couplers_BID = M_AXI_bid[3:0];
  assign m00_couplers_to_m00_couplers_BREADY = S_AXI_bready[0];
  assign m00_couplers_to_m00_couplers_BRESP = M_AXI_bresp[1:0];
  assign m00_couplers_to_m00_couplers_BVALID = M_AXI_bvalid[0];
  assign m00_couplers_to_m00_couplers_RDATA = M_AXI_rdata[511:0];
  assign m00_couplers_to_m00_couplers_RID = M_AXI_rid[3:0];
  assign m00_couplers_to_m00_couplers_RLAST = M_AXI_rlast[0];
  assign m00_couplers_to_m00_couplers_RREADY = S_AXI_rready[0];
  assign m00_couplers_to_m00_couplers_RRESP = M_AXI_rresp[1:0];
  assign m00_couplers_to_m00_couplers_RVALID = M_AXI_rvalid[0];
  assign m00_couplers_to_m00_couplers_WDATA = S_AXI_wdata[511:0];
  assign m00_couplers_to_m00_couplers_WLAST = S_AXI_wlast[0];
  assign m00_couplers_to_m00_couplers_WREADY = M_AXI_wready[0];
  assign m00_couplers_to_m00_couplers_WSTRB = S_AXI_wstrb[63:0];
  assign m00_couplers_to_m00_couplers_WVALID = S_AXI_wvalid[0];
endmodule

module m00_couplers_imp_4E4G3Z
   (M_AXIS_ACLK,
    M_AXIS_ARESETN,
    M_AXIS_tdata,
    M_AXIS_tdest,
    M_AXIS_tid,
    M_AXIS_tkeep,
    M_AXIS_tlast,
    M_AXIS_tready,
    M_AXIS_tstrb,
    M_AXIS_tuser,
    M_AXIS_tvalid,
    S_AXIS_ACLK,
    S_AXIS_ARESETN,
    S_AXIS_tdata,
    S_AXIS_tdest,
    S_AXIS_tid,
    S_AXIS_tkeep,
    S_AXIS_tlast,
    S_AXIS_tready,
    S_AXIS_tuser,
    S_AXIS_tvalid);
  input M_AXIS_ACLK;
  input [0:0]M_AXIS_ARESETN;
  output [31:0]M_AXIS_tdata;
  output [4:0]M_AXIS_tdest;
  output [4:0]M_AXIS_tid;
  output [3:0]M_AXIS_tkeep;
  output [0:0]M_AXIS_tlast;
  input M_AXIS_tready;
  output [3:0]M_AXIS_tstrb;
  output [3:0]M_AXIS_tuser;
  output M_AXIS_tvalid;
  input S_AXIS_ACLK;
  input [0:0]S_AXIS_ARESETN;
  input [31:0]S_AXIS_tdata;
  input [4:0]S_AXIS_tdest;
  input [4:0]S_AXIS_tid;
  input [3:0]S_AXIS_tkeep;
  input S_AXIS_tlast;
  output S_AXIS_tready;
  input [3:0]S_AXIS_tuser;
  input S_AXIS_tvalid;

  wire [31:0]AXIS_DATA_COUNT_to_S_AXIS_DATA_COUNT;
  wire [31:0]AXIS_RD_DATA_COUNT_to_S_AXIS_RD_DATA_COUNT;
  wire [31:0]AXIS_WR_DATA_COUNT_to_S_AXIS_WR_DATA_COUNT;
  wire S_AXIS_ACLK;
  wire [0:0]S_AXIS_ARESETN;
  wire [31:0]auto_ss_slidr_to_m00_couplers_TDATA;
  wire [4:0]auto_ss_slidr_to_m00_couplers_TDEST;
  wire [4:0]auto_ss_slidr_to_m00_couplers_TID;
  wire [3:0]auto_ss_slidr_to_m00_couplers_TKEEP;
  wire auto_ss_slidr_to_m00_couplers_TLAST;
  wire auto_ss_slidr_to_m00_couplers_TREADY;
  wire [3:0]auto_ss_slidr_to_m00_couplers_TSTRB;
  wire [3:0]auto_ss_slidr_to_m00_couplers_TUSER;
  wire auto_ss_slidr_to_m00_couplers_TVALID;
  wire [31:0]m00_couplers_to_m00_data_fifo_TDATA;
  wire [4:0]m00_couplers_to_m00_data_fifo_TDEST;
  wire [4:0]m00_couplers_to_m00_data_fifo_TID;
  wire [3:0]m00_couplers_to_m00_data_fifo_TKEEP;
  wire m00_couplers_to_m00_data_fifo_TLAST;
  wire m00_couplers_to_m00_data_fifo_TREADY;
  wire [3:0]m00_couplers_to_m00_data_fifo_TUSER;
  wire m00_couplers_to_m00_data_fifo_TVALID;
  wire [31:0]m00_data_fifo_to_auto_ss_slidr_TDATA;
  wire [4:0]m00_data_fifo_to_auto_ss_slidr_TDEST;
  wire [4:0]m00_data_fifo_to_auto_ss_slidr_TID;
  wire [3:0]m00_data_fifo_to_auto_ss_slidr_TKEEP;
  wire m00_data_fifo_to_auto_ss_slidr_TLAST;
  wire m00_data_fifo_to_auto_ss_slidr_TREADY;
  wire [3:0]m00_data_fifo_to_auto_ss_slidr_TUSER;
  wire m00_data_fifo_to_auto_ss_slidr_TVALID;

  assign M_AXIS_tdata[31:0] = auto_ss_slidr_to_m00_couplers_TDATA;
  assign M_AXIS_tdest[4:0] = auto_ss_slidr_to_m00_couplers_TDEST;
  assign M_AXIS_tid[4:0] = auto_ss_slidr_to_m00_couplers_TID;
  assign M_AXIS_tkeep[3:0] = auto_ss_slidr_to_m00_couplers_TKEEP;
  assign M_AXIS_tlast[0] = auto_ss_slidr_to_m00_couplers_TLAST;
  assign M_AXIS_tstrb[3:0] = auto_ss_slidr_to_m00_couplers_TSTRB;
  assign M_AXIS_tuser[3:0] = auto_ss_slidr_to_m00_couplers_TUSER;
  assign M_AXIS_tvalid = auto_ss_slidr_to_m00_couplers_TVALID;
  assign S_AXIS_tready = m00_couplers_to_m00_data_fifo_TREADY;
  assign auto_ss_slidr_to_m00_couplers_TREADY = M_AXIS_tready;
  assign m00_couplers_to_m00_data_fifo_TDATA = S_AXIS_tdata[31:0];
  assign m00_couplers_to_m00_data_fifo_TDEST = S_AXIS_tdest[4:0];
  assign m00_couplers_to_m00_data_fifo_TID = S_AXIS_tid[4:0];
  assign m00_couplers_to_m00_data_fifo_TKEEP = S_AXIS_tkeep[3:0];
  assign m00_couplers_to_m00_data_fifo_TLAST = S_AXIS_tlast;
  assign m00_couplers_to_m00_data_fifo_TUSER = S_AXIS_tuser[3:0];
  assign m00_couplers_to_m00_data_fifo_TVALID = S_AXIS_tvalid;
  design_1_auto_ss_slidr_9 auto_ss_slidr
       (.aclk(S_AXIS_ACLK),
        .aresetn(S_AXIS_ARESETN),
        .m_axis_tdata(auto_ss_slidr_to_m00_couplers_TDATA),
        .m_axis_tdest(auto_ss_slidr_to_m00_couplers_TDEST),
        .m_axis_tid(auto_ss_slidr_to_m00_couplers_TID),
        .m_axis_tkeep(auto_ss_slidr_to_m00_couplers_TKEEP),
        .m_axis_tlast(auto_ss_slidr_to_m00_couplers_TLAST),
        .m_axis_tready(auto_ss_slidr_to_m00_couplers_TREADY),
        .m_axis_tstrb(auto_ss_slidr_to_m00_couplers_TSTRB),
        .m_axis_tuser(auto_ss_slidr_to_m00_couplers_TUSER),
        .m_axis_tvalid(auto_ss_slidr_to_m00_couplers_TVALID),
        .s_axis_tdata(m00_data_fifo_to_auto_ss_slidr_TDATA),
        .s_axis_tdest(m00_data_fifo_to_auto_ss_slidr_TDEST),
        .s_axis_tid(m00_data_fifo_to_auto_ss_slidr_TID),
        .s_axis_tkeep(m00_data_fifo_to_auto_ss_slidr_TKEEP),
        .s_axis_tlast(m00_data_fifo_to_auto_ss_slidr_TLAST),
        .s_axis_tready(m00_data_fifo_to_auto_ss_slidr_TREADY),
        .s_axis_tuser(m00_data_fifo_to_auto_ss_slidr_TUSER),
        .s_axis_tvalid(m00_data_fifo_to_auto_ss_slidr_TVALID));
  design_1_m00_data_fifo_3 m00_data_fifo
       (.axis_data_count(AXIS_DATA_COUNT_to_S_AXIS_DATA_COUNT),
        .axis_rd_data_count(AXIS_RD_DATA_COUNT_to_S_AXIS_RD_DATA_COUNT),
        .axis_wr_data_count(AXIS_WR_DATA_COUNT_to_S_AXIS_WR_DATA_COUNT),
        .m_axis_tdata(m00_data_fifo_to_auto_ss_slidr_TDATA),
        .m_axis_tdest(m00_data_fifo_to_auto_ss_slidr_TDEST),
        .m_axis_tid(m00_data_fifo_to_auto_ss_slidr_TID),
        .m_axis_tkeep(m00_data_fifo_to_auto_ss_slidr_TKEEP),
        .m_axis_tlast(m00_data_fifo_to_auto_ss_slidr_TLAST),
        .m_axis_tready(m00_data_fifo_to_auto_ss_slidr_TREADY),
        .m_axis_tuser(m00_data_fifo_to_auto_ss_slidr_TUSER),
        .m_axis_tvalid(m00_data_fifo_to_auto_ss_slidr_TVALID),
        .s_axis_aclk(S_AXIS_ACLK),
        .s_axis_aresetn(S_AXIS_ARESETN),
        .s_axis_tdata(m00_couplers_to_m00_data_fifo_TDATA),
        .s_axis_tdest(m00_couplers_to_m00_data_fifo_TDEST),
        .s_axis_tid(m00_couplers_to_m00_data_fifo_TID),
        .s_axis_tkeep(m00_couplers_to_m00_data_fifo_TKEEP),
        .s_axis_tlast(m00_couplers_to_m00_data_fifo_TLAST),
        .s_axis_tready(m00_couplers_to_m00_data_fifo_TREADY),
        .s_axis_tuser(m00_couplers_to_m00_data_fifo_TUSER),
        .s_axis_tvalid(m00_couplers_to_m00_data_fifo_TVALID));
endmodule

module m00_couplers_imp_6XLIZ3
   (M_AXIS_ACLK,
    M_AXIS_ARESETN,
    M_AXIS_tdata,
    M_AXIS_tdest,
    M_AXIS_tid,
    M_AXIS_tkeep,
    M_AXIS_tlast,
    M_AXIS_tready,
    M_AXIS_tstrb,
    M_AXIS_tuser,
    M_AXIS_tvalid,
    S_AXIS_ACLK,
    S_AXIS_ARESETN,
    S_AXIS_tdata,
    S_AXIS_tdest,
    S_AXIS_tid,
    S_AXIS_tkeep,
    S_AXIS_tlast,
    S_AXIS_tready,
    S_AXIS_tuser,
    S_AXIS_tvalid);
  input M_AXIS_ACLK;
  input [0:0]M_AXIS_ARESETN;
  output [31:0]M_AXIS_tdata;
  output [4:0]M_AXIS_tdest;
  output [4:0]M_AXIS_tid;
  output [3:0]M_AXIS_tkeep;
  output [0:0]M_AXIS_tlast;
  input M_AXIS_tready;
  output [3:0]M_AXIS_tstrb;
  output [3:0]M_AXIS_tuser;
  output M_AXIS_tvalid;
  input S_AXIS_ACLK;
  input [0:0]S_AXIS_ARESETN;
  input [31:0]S_AXIS_tdata;
  input [4:0]S_AXIS_tdest;
  input [4:0]S_AXIS_tid;
  input [3:0]S_AXIS_tkeep;
  input S_AXIS_tlast;
  output S_AXIS_tready;
  input [3:0]S_AXIS_tuser;
  input S_AXIS_tvalid;

  wire [31:0]AXIS_DATA_COUNT_to_S_AXIS_DATA_COUNT;
  wire [31:0]AXIS_RD_DATA_COUNT_to_S_AXIS_RD_DATA_COUNT;
  wire [31:0]AXIS_WR_DATA_COUNT_to_S_AXIS_WR_DATA_COUNT;
  wire S_AXIS_ACLK;
  wire [0:0]S_AXIS_ARESETN;
  wire [31:0]auto_ss_slidr_to_m00_couplers_TDATA;
  wire [4:0]auto_ss_slidr_to_m00_couplers_TDEST;
  wire [4:0]auto_ss_slidr_to_m00_couplers_TID;
  wire [3:0]auto_ss_slidr_to_m00_couplers_TKEEP;
  wire auto_ss_slidr_to_m00_couplers_TLAST;
  wire auto_ss_slidr_to_m00_couplers_TREADY;
  wire [3:0]auto_ss_slidr_to_m00_couplers_TSTRB;
  wire [3:0]auto_ss_slidr_to_m00_couplers_TUSER;
  wire auto_ss_slidr_to_m00_couplers_TVALID;
  wire [31:0]m00_couplers_to_m00_data_fifo_TDATA;
  wire [4:0]m00_couplers_to_m00_data_fifo_TDEST;
  wire [4:0]m00_couplers_to_m00_data_fifo_TID;
  wire [3:0]m00_couplers_to_m00_data_fifo_TKEEP;
  wire m00_couplers_to_m00_data_fifo_TLAST;
  wire m00_couplers_to_m00_data_fifo_TREADY;
  wire [3:0]m00_couplers_to_m00_data_fifo_TUSER;
  wire m00_couplers_to_m00_data_fifo_TVALID;
  wire [31:0]m00_data_fifo_to_auto_ss_slidr_TDATA;
  wire [4:0]m00_data_fifo_to_auto_ss_slidr_TDEST;
  wire [4:0]m00_data_fifo_to_auto_ss_slidr_TID;
  wire [3:0]m00_data_fifo_to_auto_ss_slidr_TKEEP;
  wire m00_data_fifo_to_auto_ss_slidr_TLAST;
  wire m00_data_fifo_to_auto_ss_slidr_TREADY;
  wire [3:0]m00_data_fifo_to_auto_ss_slidr_TUSER;
  wire m00_data_fifo_to_auto_ss_slidr_TVALID;

  assign M_AXIS_tdata[31:0] = auto_ss_slidr_to_m00_couplers_TDATA;
  assign M_AXIS_tdest[4:0] = auto_ss_slidr_to_m00_couplers_TDEST;
  assign M_AXIS_tid[4:0] = auto_ss_slidr_to_m00_couplers_TID;
  assign M_AXIS_tkeep[3:0] = auto_ss_slidr_to_m00_couplers_TKEEP;
  assign M_AXIS_tlast[0] = auto_ss_slidr_to_m00_couplers_TLAST;
  assign M_AXIS_tstrb[3:0] = auto_ss_slidr_to_m00_couplers_TSTRB;
  assign M_AXIS_tuser[3:0] = auto_ss_slidr_to_m00_couplers_TUSER;
  assign M_AXIS_tvalid = auto_ss_slidr_to_m00_couplers_TVALID;
  assign S_AXIS_tready = m00_couplers_to_m00_data_fifo_TREADY;
  assign auto_ss_slidr_to_m00_couplers_TREADY = M_AXIS_tready;
  assign m00_couplers_to_m00_data_fifo_TDATA = S_AXIS_tdata[31:0];
  assign m00_couplers_to_m00_data_fifo_TDEST = S_AXIS_tdest[4:0];
  assign m00_couplers_to_m00_data_fifo_TID = S_AXIS_tid[4:0];
  assign m00_couplers_to_m00_data_fifo_TKEEP = S_AXIS_tkeep[3:0];
  assign m00_couplers_to_m00_data_fifo_TLAST = S_AXIS_tlast;
  assign m00_couplers_to_m00_data_fifo_TUSER = S_AXIS_tuser[3:0];
  assign m00_couplers_to_m00_data_fifo_TVALID = S_AXIS_tvalid;
  design_1_auto_ss_slidr_6 auto_ss_slidr
       (.aclk(S_AXIS_ACLK),
        .aresetn(S_AXIS_ARESETN),
        .m_axis_tdata(auto_ss_slidr_to_m00_couplers_TDATA),
        .m_axis_tdest(auto_ss_slidr_to_m00_couplers_TDEST),
        .m_axis_tid(auto_ss_slidr_to_m00_couplers_TID),
        .m_axis_tkeep(auto_ss_slidr_to_m00_couplers_TKEEP),
        .m_axis_tlast(auto_ss_slidr_to_m00_couplers_TLAST),
        .m_axis_tready(auto_ss_slidr_to_m00_couplers_TREADY),
        .m_axis_tstrb(auto_ss_slidr_to_m00_couplers_TSTRB),
        .m_axis_tuser(auto_ss_slidr_to_m00_couplers_TUSER),
        .m_axis_tvalid(auto_ss_slidr_to_m00_couplers_TVALID),
        .s_axis_tdata(m00_data_fifo_to_auto_ss_slidr_TDATA),
        .s_axis_tdest(m00_data_fifo_to_auto_ss_slidr_TDEST),
        .s_axis_tid(m00_data_fifo_to_auto_ss_slidr_TID),
        .s_axis_tkeep(m00_data_fifo_to_auto_ss_slidr_TKEEP),
        .s_axis_tlast(m00_data_fifo_to_auto_ss_slidr_TLAST),
        .s_axis_tready(m00_data_fifo_to_auto_ss_slidr_TREADY),
        .s_axis_tuser(m00_data_fifo_to_auto_ss_slidr_TUSER),
        .s_axis_tvalid(m00_data_fifo_to_auto_ss_slidr_TVALID));
  design_1_m00_data_fifo_2 m00_data_fifo
       (.axis_data_count(AXIS_DATA_COUNT_to_S_AXIS_DATA_COUNT),
        .axis_rd_data_count(AXIS_RD_DATA_COUNT_to_S_AXIS_RD_DATA_COUNT),
        .axis_wr_data_count(AXIS_WR_DATA_COUNT_to_S_AXIS_WR_DATA_COUNT),
        .m_axis_tdata(m00_data_fifo_to_auto_ss_slidr_TDATA),
        .m_axis_tdest(m00_data_fifo_to_auto_ss_slidr_TDEST),
        .m_axis_tid(m00_data_fifo_to_auto_ss_slidr_TID),
        .m_axis_tkeep(m00_data_fifo_to_auto_ss_slidr_TKEEP),
        .m_axis_tlast(m00_data_fifo_to_auto_ss_slidr_TLAST),
        .m_axis_tready(m00_data_fifo_to_auto_ss_slidr_TREADY),
        .m_axis_tuser(m00_data_fifo_to_auto_ss_slidr_TUSER),
        .m_axis_tvalid(m00_data_fifo_to_auto_ss_slidr_TVALID),
        .s_axis_aclk(S_AXIS_ACLK),
        .s_axis_aresetn(S_AXIS_ARESETN),
        .s_axis_tdata(m00_couplers_to_m00_data_fifo_TDATA),
        .s_axis_tdest(m00_couplers_to_m00_data_fifo_TDEST),
        .s_axis_tid(m00_couplers_to_m00_data_fifo_TID),
        .s_axis_tkeep(m00_couplers_to_m00_data_fifo_TKEEP),
        .s_axis_tlast(m00_couplers_to_m00_data_fifo_TLAST),
        .s_axis_tready(m00_couplers_to_m00_data_fifo_TREADY),
        .s_axis_tuser(m00_couplers_to_m00_data_fifo_TUSER),
        .s_axis_tvalid(m00_couplers_to_m00_data_fifo_TVALID));
endmodule

module m00_couplers_imp_8RVYHO
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [31:0]M_AXI_araddr;
  input [0:0]M_AXI_arready;
  output [0:0]M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  input [0:0]M_AXI_awready;
  output [0:0]M_AXI_awvalid;
  output [0:0]M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input [0:0]M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output [0:0]M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input [0:0]M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input [0:0]M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output [0:0]M_AXI_wvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [31:0]S_AXI_araddr;
  output [0:0]S_AXI_arready;
  input [0:0]S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  output [0:0]S_AXI_awready;
  input [0:0]S_AXI_awvalid;
  input [0:0]S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output [0:0]S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  input [0:0]S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output [0:0]S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  output [0:0]S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input [0:0]S_AXI_wvalid;

  wire [31:0]m00_couplers_to_m00_couplers_ARADDR;
  wire [0:0]m00_couplers_to_m00_couplers_ARREADY;
  wire [0:0]m00_couplers_to_m00_couplers_ARVALID;
  wire [31:0]m00_couplers_to_m00_couplers_AWADDR;
  wire [0:0]m00_couplers_to_m00_couplers_AWREADY;
  wire [0:0]m00_couplers_to_m00_couplers_AWVALID;
  wire [0:0]m00_couplers_to_m00_couplers_BREADY;
  wire [1:0]m00_couplers_to_m00_couplers_BRESP;
  wire [0:0]m00_couplers_to_m00_couplers_BVALID;
  wire [31:0]m00_couplers_to_m00_couplers_RDATA;
  wire [0:0]m00_couplers_to_m00_couplers_RREADY;
  wire [1:0]m00_couplers_to_m00_couplers_RRESP;
  wire [0:0]m00_couplers_to_m00_couplers_RVALID;
  wire [31:0]m00_couplers_to_m00_couplers_WDATA;
  wire [0:0]m00_couplers_to_m00_couplers_WREADY;
  wire [3:0]m00_couplers_to_m00_couplers_WSTRB;
  wire [0:0]m00_couplers_to_m00_couplers_WVALID;

  assign M_AXI_araddr[31:0] = m00_couplers_to_m00_couplers_ARADDR;
  assign M_AXI_arvalid[0] = m00_couplers_to_m00_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = m00_couplers_to_m00_couplers_AWADDR;
  assign M_AXI_awvalid[0] = m00_couplers_to_m00_couplers_AWVALID;
  assign M_AXI_bready[0] = m00_couplers_to_m00_couplers_BREADY;
  assign M_AXI_rready[0] = m00_couplers_to_m00_couplers_RREADY;
  assign M_AXI_wdata[31:0] = m00_couplers_to_m00_couplers_WDATA;
  assign M_AXI_wstrb[3:0] = m00_couplers_to_m00_couplers_WSTRB;
  assign M_AXI_wvalid[0] = m00_couplers_to_m00_couplers_WVALID;
  assign S_AXI_arready[0] = m00_couplers_to_m00_couplers_ARREADY;
  assign S_AXI_awready[0] = m00_couplers_to_m00_couplers_AWREADY;
  assign S_AXI_bresp[1:0] = m00_couplers_to_m00_couplers_BRESP;
  assign S_AXI_bvalid[0] = m00_couplers_to_m00_couplers_BVALID;
  assign S_AXI_rdata[31:0] = m00_couplers_to_m00_couplers_RDATA;
  assign S_AXI_rresp[1:0] = m00_couplers_to_m00_couplers_RRESP;
  assign S_AXI_rvalid[0] = m00_couplers_to_m00_couplers_RVALID;
  assign S_AXI_wready[0] = m00_couplers_to_m00_couplers_WREADY;
  assign m00_couplers_to_m00_couplers_ARADDR = S_AXI_araddr[31:0];
  assign m00_couplers_to_m00_couplers_ARREADY = M_AXI_arready[0];
  assign m00_couplers_to_m00_couplers_ARVALID = S_AXI_arvalid[0];
  assign m00_couplers_to_m00_couplers_AWADDR = S_AXI_awaddr[31:0];
  assign m00_couplers_to_m00_couplers_AWREADY = M_AXI_awready[0];
  assign m00_couplers_to_m00_couplers_AWVALID = S_AXI_awvalid[0];
  assign m00_couplers_to_m00_couplers_BREADY = S_AXI_bready[0];
  assign m00_couplers_to_m00_couplers_BRESP = M_AXI_bresp[1:0];
  assign m00_couplers_to_m00_couplers_BVALID = M_AXI_bvalid[0];
  assign m00_couplers_to_m00_couplers_RDATA = M_AXI_rdata[31:0];
  assign m00_couplers_to_m00_couplers_RREADY = S_AXI_rready[0];
  assign m00_couplers_to_m00_couplers_RRESP = M_AXI_rresp[1:0];
  assign m00_couplers_to_m00_couplers_RVALID = M_AXI_rvalid[0];
  assign m00_couplers_to_m00_couplers_WDATA = S_AXI_wdata[31:0];
  assign m00_couplers_to_m00_couplers_WREADY = M_AXI_wready[0];
  assign m00_couplers_to_m00_couplers_WSTRB = S_AXI_wstrb[3:0];
  assign m00_couplers_to_m00_couplers_WVALID = S_AXI_wvalid[0];
endmodule

module m00_couplers_imp_9ASDFZ
   (M_AXIS_ACLK,
    M_AXIS_ARESETN,
    M_AXIS_tdata,
    M_AXIS_tdest,
    M_AXIS_tid,
    M_AXIS_tkeep,
    M_AXIS_tlast,
    M_AXIS_tready,
    M_AXIS_tstrb,
    M_AXIS_tuser,
    M_AXIS_tvalid,
    S_AXIS_ACLK,
    S_AXIS_ARESETN,
    S_AXIS_tdata,
    S_AXIS_tdest,
    S_AXIS_tid,
    S_AXIS_tkeep,
    S_AXIS_tlast,
    S_AXIS_tready,
    S_AXIS_tuser,
    S_AXIS_tvalid);
  input M_AXIS_ACLK;
  input [0:0]M_AXIS_ARESETN;
  output [31:0]M_AXIS_tdata;
  output [4:0]M_AXIS_tdest;
  output [4:0]M_AXIS_tid;
  output [3:0]M_AXIS_tkeep;
  output [0:0]M_AXIS_tlast;
  input M_AXIS_tready;
  output [3:0]M_AXIS_tstrb;
  output [3:0]M_AXIS_tuser;
  output M_AXIS_tvalid;
  input S_AXIS_ACLK;
  input [0:0]S_AXIS_ARESETN;
  input [31:0]S_AXIS_tdata;
  input [4:0]S_AXIS_tdest;
  input [4:0]S_AXIS_tid;
  input [3:0]S_AXIS_tkeep;
  input S_AXIS_tlast;
  output S_AXIS_tready;
  input [3:0]S_AXIS_tuser;
  input S_AXIS_tvalid;

  wire [31:0]AXIS_DATA_COUNT_to_S_AXIS_DATA_COUNT;
  wire [31:0]AXIS_RD_DATA_COUNT_to_S_AXIS_RD_DATA_COUNT;
  wire [31:0]AXIS_WR_DATA_COUNT_to_S_AXIS_WR_DATA_COUNT;
  wire S_AXIS_ACLK;
  wire [0:0]S_AXIS_ARESETN;
  wire [31:0]auto_ss_slidr_to_m00_couplers_TDATA;
  wire [4:0]auto_ss_slidr_to_m00_couplers_TDEST;
  wire [4:0]auto_ss_slidr_to_m00_couplers_TID;
  wire [3:0]auto_ss_slidr_to_m00_couplers_TKEEP;
  wire auto_ss_slidr_to_m00_couplers_TLAST;
  wire auto_ss_slidr_to_m00_couplers_TREADY;
  wire [3:0]auto_ss_slidr_to_m00_couplers_TSTRB;
  wire [3:0]auto_ss_slidr_to_m00_couplers_TUSER;
  wire auto_ss_slidr_to_m00_couplers_TVALID;
  wire [31:0]m00_couplers_to_m00_data_fifo_TDATA;
  wire [4:0]m00_couplers_to_m00_data_fifo_TDEST;
  wire [4:0]m00_couplers_to_m00_data_fifo_TID;
  wire [3:0]m00_couplers_to_m00_data_fifo_TKEEP;
  wire m00_couplers_to_m00_data_fifo_TLAST;
  wire m00_couplers_to_m00_data_fifo_TREADY;
  wire [3:0]m00_couplers_to_m00_data_fifo_TUSER;
  wire m00_couplers_to_m00_data_fifo_TVALID;
  wire [31:0]m00_data_fifo_to_auto_ss_slidr_TDATA;
  wire [4:0]m00_data_fifo_to_auto_ss_slidr_TDEST;
  wire [4:0]m00_data_fifo_to_auto_ss_slidr_TID;
  wire [3:0]m00_data_fifo_to_auto_ss_slidr_TKEEP;
  wire m00_data_fifo_to_auto_ss_slidr_TLAST;
  wire m00_data_fifo_to_auto_ss_slidr_TREADY;
  wire [3:0]m00_data_fifo_to_auto_ss_slidr_TUSER;
  wire m00_data_fifo_to_auto_ss_slidr_TVALID;

  assign M_AXIS_tdata[31:0] = auto_ss_slidr_to_m00_couplers_TDATA;
  assign M_AXIS_tdest[4:0] = auto_ss_slidr_to_m00_couplers_TDEST;
  assign M_AXIS_tid[4:0] = auto_ss_slidr_to_m00_couplers_TID;
  assign M_AXIS_tkeep[3:0] = auto_ss_slidr_to_m00_couplers_TKEEP;
  assign M_AXIS_tlast[0] = auto_ss_slidr_to_m00_couplers_TLAST;
  assign M_AXIS_tstrb[3:0] = auto_ss_slidr_to_m00_couplers_TSTRB;
  assign M_AXIS_tuser[3:0] = auto_ss_slidr_to_m00_couplers_TUSER;
  assign M_AXIS_tvalid = auto_ss_slidr_to_m00_couplers_TVALID;
  assign S_AXIS_tready = m00_couplers_to_m00_data_fifo_TREADY;
  assign auto_ss_slidr_to_m00_couplers_TREADY = M_AXIS_tready;
  assign m00_couplers_to_m00_data_fifo_TDATA = S_AXIS_tdata[31:0];
  assign m00_couplers_to_m00_data_fifo_TDEST = S_AXIS_tdest[4:0];
  assign m00_couplers_to_m00_data_fifo_TID = S_AXIS_tid[4:0];
  assign m00_couplers_to_m00_data_fifo_TKEEP = S_AXIS_tkeep[3:0];
  assign m00_couplers_to_m00_data_fifo_TLAST = S_AXIS_tlast;
  assign m00_couplers_to_m00_data_fifo_TUSER = S_AXIS_tuser[3:0];
  assign m00_couplers_to_m00_data_fifo_TVALID = S_AXIS_tvalid;
  design_1_auto_ss_slidr_3 auto_ss_slidr
       (.aclk(S_AXIS_ACLK),
        .aresetn(S_AXIS_ARESETN),
        .m_axis_tdata(auto_ss_slidr_to_m00_couplers_TDATA),
        .m_axis_tdest(auto_ss_slidr_to_m00_couplers_TDEST),
        .m_axis_tid(auto_ss_slidr_to_m00_couplers_TID),
        .m_axis_tkeep(auto_ss_slidr_to_m00_couplers_TKEEP),
        .m_axis_tlast(auto_ss_slidr_to_m00_couplers_TLAST),
        .m_axis_tready(auto_ss_slidr_to_m00_couplers_TREADY),
        .m_axis_tstrb(auto_ss_slidr_to_m00_couplers_TSTRB),
        .m_axis_tuser(auto_ss_slidr_to_m00_couplers_TUSER),
        .m_axis_tvalid(auto_ss_slidr_to_m00_couplers_TVALID),
        .s_axis_tdata(m00_data_fifo_to_auto_ss_slidr_TDATA),
        .s_axis_tdest(m00_data_fifo_to_auto_ss_slidr_TDEST),
        .s_axis_tid(m00_data_fifo_to_auto_ss_slidr_TID),
        .s_axis_tkeep(m00_data_fifo_to_auto_ss_slidr_TKEEP),
        .s_axis_tlast(m00_data_fifo_to_auto_ss_slidr_TLAST),
        .s_axis_tready(m00_data_fifo_to_auto_ss_slidr_TREADY),
        .s_axis_tuser(m00_data_fifo_to_auto_ss_slidr_TUSER),
        .s_axis_tvalid(m00_data_fifo_to_auto_ss_slidr_TVALID));
  design_1_m00_data_fifo_1 m00_data_fifo
       (.axis_data_count(AXIS_DATA_COUNT_to_S_AXIS_DATA_COUNT),
        .axis_rd_data_count(AXIS_RD_DATA_COUNT_to_S_AXIS_RD_DATA_COUNT),
        .axis_wr_data_count(AXIS_WR_DATA_COUNT_to_S_AXIS_WR_DATA_COUNT),
        .m_axis_tdata(m00_data_fifo_to_auto_ss_slidr_TDATA),
        .m_axis_tdest(m00_data_fifo_to_auto_ss_slidr_TDEST),
        .m_axis_tid(m00_data_fifo_to_auto_ss_slidr_TID),
        .m_axis_tkeep(m00_data_fifo_to_auto_ss_slidr_TKEEP),
        .m_axis_tlast(m00_data_fifo_to_auto_ss_slidr_TLAST),
        .m_axis_tready(m00_data_fifo_to_auto_ss_slidr_TREADY),
        .m_axis_tuser(m00_data_fifo_to_auto_ss_slidr_TUSER),
        .m_axis_tvalid(m00_data_fifo_to_auto_ss_slidr_TVALID),
        .s_axis_aclk(S_AXIS_ACLK),
        .s_axis_aresetn(S_AXIS_ARESETN),
        .s_axis_tdata(m00_couplers_to_m00_data_fifo_TDATA),
        .s_axis_tdest(m00_couplers_to_m00_data_fifo_TDEST),
        .s_axis_tid(m00_couplers_to_m00_data_fifo_TID),
        .s_axis_tkeep(m00_couplers_to_m00_data_fifo_TKEEP),
        .s_axis_tlast(m00_couplers_to_m00_data_fifo_TLAST),
        .s_axis_tready(m00_couplers_to_m00_data_fifo_TREADY),
        .s_axis_tuser(m00_couplers_to_m00_data_fifo_TUSER),
        .s_axis_tvalid(m00_couplers_to_m00_data_fifo_TVALID));
endmodule

module m00_couplers_imp_F63VTB
   (M_AXIS_ACLK,
    M_AXIS_ARESETN,
    M_AXIS_tdata,
    M_AXIS_tdest,
    M_AXIS_tid,
    M_AXIS_tkeep,
    M_AXIS_tlast,
    M_AXIS_tready,
    M_AXIS_tstrb,
    M_AXIS_tuser,
    M_AXIS_tvalid,
    S_AXIS_ACLK,
    S_AXIS_ARESETN,
    S_AXIS_tdata,
    S_AXIS_tdest,
    S_AXIS_tid,
    S_AXIS_tkeep,
    S_AXIS_tlast,
    S_AXIS_tready,
    S_AXIS_tuser,
    S_AXIS_tvalid);
  input M_AXIS_ACLK;
  input [0:0]M_AXIS_ARESETN;
  output [31:0]M_AXIS_tdata;
  output [4:0]M_AXIS_tdest;
  output [4:0]M_AXIS_tid;
  output [3:0]M_AXIS_tkeep;
  output [0:0]M_AXIS_tlast;
  input M_AXIS_tready;
  output [3:0]M_AXIS_tstrb;
  output [3:0]M_AXIS_tuser;
  output M_AXIS_tvalid;
  input S_AXIS_ACLK;
  input [0:0]S_AXIS_ARESETN;
  input [31:0]S_AXIS_tdata;
  input [4:0]S_AXIS_tdest;
  input [4:0]S_AXIS_tid;
  input [3:0]S_AXIS_tkeep;
  input S_AXIS_tlast;
  output S_AXIS_tready;
  input [3:0]S_AXIS_tuser;
  input S_AXIS_tvalid;

  wire [31:0]AXIS_DATA_COUNT_to_S_AXIS_DATA_COUNT;
  wire [31:0]AXIS_RD_DATA_COUNT_to_S_AXIS_RD_DATA_COUNT;
  wire [31:0]AXIS_WR_DATA_COUNT_to_S_AXIS_WR_DATA_COUNT;
  wire S_AXIS_ACLK;
  wire [0:0]S_AXIS_ARESETN;
  wire [31:0]auto_ss_slidr_to_m00_couplers_TDATA;
  wire [4:0]auto_ss_slidr_to_m00_couplers_TDEST;
  wire [4:0]auto_ss_slidr_to_m00_couplers_TID;
  wire [3:0]auto_ss_slidr_to_m00_couplers_TKEEP;
  wire auto_ss_slidr_to_m00_couplers_TLAST;
  wire auto_ss_slidr_to_m00_couplers_TREADY;
  wire [3:0]auto_ss_slidr_to_m00_couplers_TSTRB;
  wire [3:0]auto_ss_slidr_to_m00_couplers_TUSER;
  wire auto_ss_slidr_to_m00_couplers_TVALID;
  wire [31:0]m00_couplers_to_m00_data_fifo_TDATA;
  wire [4:0]m00_couplers_to_m00_data_fifo_TDEST;
  wire [4:0]m00_couplers_to_m00_data_fifo_TID;
  wire [3:0]m00_couplers_to_m00_data_fifo_TKEEP;
  wire m00_couplers_to_m00_data_fifo_TLAST;
  wire m00_couplers_to_m00_data_fifo_TREADY;
  wire [3:0]m00_couplers_to_m00_data_fifo_TUSER;
  wire m00_couplers_to_m00_data_fifo_TVALID;
  wire [31:0]m00_data_fifo_to_auto_ss_slidr_TDATA;
  wire [4:0]m00_data_fifo_to_auto_ss_slidr_TDEST;
  wire [4:0]m00_data_fifo_to_auto_ss_slidr_TID;
  wire [3:0]m00_data_fifo_to_auto_ss_slidr_TKEEP;
  wire m00_data_fifo_to_auto_ss_slidr_TLAST;
  wire m00_data_fifo_to_auto_ss_slidr_TREADY;
  wire [3:0]m00_data_fifo_to_auto_ss_slidr_TUSER;
  wire m00_data_fifo_to_auto_ss_slidr_TVALID;

  assign M_AXIS_tdata[31:0] = auto_ss_slidr_to_m00_couplers_TDATA;
  assign M_AXIS_tdest[4:0] = auto_ss_slidr_to_m00_couplers_TDEST;
  assign M_AXIS_tid[4:0] = auto_ss_slidr_to_m00_couplers_TID;
  assign M_AXIS_tkeep[3:0] = auto_ss_slidr_to_m00_couplers_TKEEP;
  assign M_AXIS_tlast[0] = auto_ss_slidr_to_m00_couplers_TLAST;
  assign M_AXIS_tstrb[3:0] = auto_ss_slidr_to_m00_couplers_TSTRB;
  assign M_AXIS_tuser[3:0] = auto_ss_slidr_to_m00_couplers_TUSER;
  assign M_AXIS_tvalid = auto_ss_slidr_to_m00_couplers_TVALID;
  assign S_AXIS_tready = m00_couplers_to_m00_data_fifo_TREADY;
  assign auto_ss_slidr_to_m00_couplers_TREADY = M_AXIS_tready;
  assign m00_couplers_to_m00_data_fifo_TDATA = S_AXIS_tdata[31:0];
  assign m00_couplers_to_m00_data_fifo_TDEST = S_AXIS_tdest[4:0];
  assign m00_couplers_to_m00_data_fifo_TID = S_AXIS_tid[4:0];
  assign m00_couplers_to_m00_data_fifo_TKEEP = S_AXIS_tkeep[3:0];
  assign m00_couplers_to_m00_data_fifo_TLAST = S_AXIS_tlast;
  assign m00_couplers_to_m00_data_fifo_TUSER = S_AXIS_tuser[3:0];
  assign m00_couplers_to_m00_data_fifo_TVALID = S_AXIS_tvalid;
  design_1_auto_ss_slidr_0 auto_ss_slidr
       (.aclk(S_AXIS_ACLK),
        .aresetn(S_AXIS_ARESETN),
        .m_axis_tdata(auto_ss_slidr_to_m00_couplers_TDATA),
        .m_axis_tdest(auto_ss_slidr_to_m00_couplers_TDEST),
        .m_axis_tid(auto_ss_slidr_to_m00_couplers_TID),
        .m_axis_tkeep(auto_ss_slidr_to_m00_couplers_TKEEP),
        .m_axis_tlast(auto_ss_slidr_to_m00_couplers_TLAST),
        .m_axis_tready(auto_ss_slidr_to_m00_couplers_TREADY),
        .m_axis_tstrb(auto_ss_slidr_to_m00_couplers_TSTRB),
        .m_axis_tuser(auto_ss_slidr_to_m00_couplers_TUSER),
        .m_axis_tvalid(auto_ss_slidr_to_m00_couplers_TVALID),
        .s_axis_tdata(m00_data_fifo_to_auto_ss_slidr_TDATA),
        .s_axis_tdest(m00_data_fifo_to_auto_ss_slidr_TDEST),
        .s_axis_tid(m00_data_fifo_to_auto_ss_slidr_TID),
        .s_axis_tkeep(m00_data_fifo_to_auto_ss_slidr_TKEEP),
        .s_axis_tlast(m00_data_fifo_to_auto_ss_slidr_TLAST),
        .s_axis_tready(m00_data_fifo_to_auto_ss_slidr_TREADY),
        .s_axis_tuser(m00_data_fifo_to_auto_ss_slidr_TUSER),
        .s_axis_tvalid(m00_data_fifo_to_auto_ss_slidr_TVALID));
  design_1_m00_data_fifo_0 m00_data_fifo
       (.axis_data_count(AXIS_DATA_COUNT_to_S_AXIS_DATA_COUNT),
        .axis_rd_data_count(AXIS_RD_DATA_COUNT_to_S_AXIS_RD_DATA_COUNT),
        .axis_wr_data_count(AXIS_WR_DATA_COUNT_to_S_AXIS_WR_DATA_COUNT),
        .m_axis_tdata(m00_data_fifo_to_auto_ss_slidr_TDATA),
        .m_axis_tdest(m00_data_fifo_to_auto_ss_slidr_TDEST),
        .m_axis_tid(m00_data_fifo_to_auto_ss_slidr_TID),
        .m_axis_tkeep(m00_data_fifo_to_auto_ss_slidr_TKEEP),
        .m_axis_tlast(m00_data_fifo_to_auto_ss_slidr_TLAST),
        .m_axis_tready(m00_data_fifo_to_auto_ss_slidr_TREADY),
        .m_axis_tuser(m00_data_fifo_to_auto_ss_slidr_TUSER),
        .m_axis_tvalid(m00_data_fifo_to_auto_ss_slidr_TVALID),
        .s_axis_aclk(S_AXIS_ACLK),
        .s_axis_aresetn(S_AXIS_ARESETN),
        .s_axis_tdata(m00_couplers_to_m00_data_fifo_TDATA),
        .s_axis_tdest(m00_couplers_to_m00_data_fifo_TDEST),
        .s_axis_tid(m00_couplers_to_m00_data_fifo_TID),
        .s_axis_tkeep(m00_couplers_to_m00_data_fifo_TKEEP),
        .s_axis_tlast(m00_couplers_to_m00_data_fifo_TLAST),
        .s_axis_tready(m00_couplers_to_m00_data_fifo_TREADY),
        .s_axis_tuser(m00_couplers_to_m00_data_fifo_TUSER),
        .s_axis_tvalid(m00_couplers_to_m00_data_fifo_TVALID));
endmodule

module m01_couplers_imp_1LE2BCE
   (M_AXIS_ACLK,
    M_AXIS_ARESETN,
    M_AXIS_tdata,
    M_AXIS_tdest,
    M_AXIS_tid,
    M_AXIS_tkeep,
    M_AXIS_tlast,
    M_AXIS_tready,
    M_AXIS_tstrb,
    M_AXIS_tuser,
    M_AXIS_tvalid,
    S_AXIS_ACLK,
    S_AXIS_ARESETN,
    S_AXIS_tdata,
    S_AXIS_tdest,
    S_AXIS_tid,
    S_AXIS_tkeep,
    S_AXIS_tlast,
    S_AXIS_tready,
    S_AXIS_tuser,
    S_AXIS_tvalid);
  input M_AXIS_ACLK;
  input [0:0]M_AXIS_ARESETN;
  output [31:0]M_AXIS_tdata;
  output [4:0]M_AXIS_tdest;
  output [4:0]M_AXIS_tid;
  output [3:0]M_AXIS_tkeep;
  output [0:0]M_AXIS_tlast;
  input M_AXIS_tready;
  output [3:0]M_AXIS_tstrb;
  output [3:0]M_AXIS_tuser;
  output M_AXIS_tvalid;
  input S_AXIS_ACLK;
  input [0:0]S_AXIS_ARESETN;
  input [31:0]S_AXIS_tdata;
  input [4:0]S_AXIS_tdest;
  input [4:0]S_AXIS_tid;
  input [3:0]S_AXIS_tkeep;
  input S_AXIS_tlast;
  output S_AXIS_tready;
  input [3:0]S_AXIS_tuser;
  input S_AXIS_tvalid;

  wire [31:0]AXIS_DATA_COUNT_to_S_AXIS_DATA_COUNT;
  wire [31:0]AXIS_RD_DATA_COUNT_to_S_AXIS_RD_DATA_COUNT;
  wire [31:0]AXIS_WR_DATA_COUNT_to_S_AXIS_WR_DATA_COUNT;
  wire S_AXIS_ACLK;
  wire [0:0]S_AXIS_ARESETN;
  wire [31:0]auto_ss_slidr_to_m01_couplers_TDATA;
  wire [4:0]auto_ss_slidr_to_m01_couplers_TDEST;
  wire [4:0]auto_ss_slidr_to_m01_couplers_TID;
  wire [3:0]auto_ss_slidr_to_m01_couplers_TKEEP;
  wire auto_ss_slidr_to_m01_couplers_TLAST;
  wire auto_ss_slidr_to_m01_couplers_TREADY;
  wire [3:0]auto_ss_slidr_to_m01_couplers_TSTRB;
  wire [3:0]auto_ss_slidr_to_m01_couplers_TUSER;
  wire auto_ss_slidr_to_m01_couplers_TVALID;
  wire [31:0]m01_couplers_to_m01_data_fifo_TDATA;
  wire [4:0]m01_couplers_to_m01_data_fifo_TDEST;
  wire [4:0]m01_couplers_to_m01_data_fifo_TID;
  wire [3:0]m01_couplers_to_m01_data_fifo_TKEEP;
  wire m01_couplers_to_m01_data_fifo_TLAST;
  wire m01_couplers_to_m01_data_fifo_TREADY;
  wire [3:0]m01_couplers_to_m01_data_fifo_TUSER;
  wire m01_couplers_to_m01_data_fifo_TVALID;
  wire [31:0]m01_data_fifo_to_auto_ss_slidr_TDATA;
  wire [4:0]m01_data_fifo_to_auto_ss_slidr_TDEST;
  wire [4:0]m01_data_fifo_to_auto_ss_slidr_TID;
  wire [3:0]m01_data_fifo_to_auto_ss_slidr_TKEEP;
  wire m01_data_fifo_to_auto_ss_slidr_TLAST;
  wire m01_data_fifo_to_auto_ss_slidr_TREADY;
  wire [3:0]m01_data_fifo_to_auto_ss_slidr_TUSER;
  wire m01_data_fifo_to_auto_ss_slidr_TVALID;

  assign M_AXIS_tdata[31:0] = auto_ss_slidr_to_m01_couplers_TDATA;
  assign M_AXIS_tdest[4:0] = auto_ss_slidr_to_m01_couplers_TDEST;
  assign M_AXIS_tid[4:0] = auto_ss_slidr_to_m01_couplers_TID;
  assign M_AXIS_tkeep[3:0] = auto_ss_slidr_to_m01_couplers_TKEEP;
  assign M_AXIS_tlast[0] = auto_ss_slidr_to_m01_couplers_TLAST;
  assign M_AXIS_tstrb[3:0] = auto_ss_slidr_to_m01_couplers_TSTRB;
  assign M_AXIS_tuser[3:0] = auto_ss_slidr_to_m01_couplers_TUSER;
  assign M_AXIS_tvalid = auto_ss_slidr_to_m01_couplers_TVALID;
  assign S_AXIS_tready = m01_couplers_to_m01_data_fifo_TREADY;
  assign auto_ss_slidr_to_m01_couplers_TREADY = M_AXIS_tready;
  assign m01_couplers_to_m01_data_fifo_TDATA = S_AXIS_tdata[31:0];
  assign m01_couplers_to_m01_data_fifo_TDEST = S_AXIS_tdest[4:0];
  assign m01_couplers_to_m01_data_fifo_TID = S_AXIS_tid[4:0];
  assign m01_couplers_to_m01_data_fifo_TKEEP = S_AXIS_tkeep[3:0];
  assign m01_couplers_to_m01_data_fifo_TLAST = S_AXIS_tlast;
  assign m01_couplers_to_m01_data_fifo_TUSER = S_AXIS_tuser[3:0];
  assign m01_couplers_to_m01_data_fifo_TVALID = S_AXIS_tvalid;
  design_1_auto_ss_slidr_4 auto_ss_slidr
       (.aclk(S_AXIS_ACLK),
        .aresetn(S_AXIS_ARESETN),
        .m_axis_tdata(auto_ss_slidr_to_m01_couplers_TDATA),
        .m_axis_tdest(auto_ss_slidr_to_m01_couplers_TDEST),
        .m_axis_tid(auto_ss_slidr_to_m01_couplers_TID),
        .m_axis_tkeep(auto_ss_slidr_to_m01_couplers_TKEEP),
        .m_axis_tlast(auto_ss_slidr_to_m01_couplers_TLAST),
        .m_axis_tready(auto_ss_slidr_to_m01_couplers_TREADY),
        .m_axis_tstrb(auto_ss_slidr_to_m01_couplers_TSTRB),
        .m_axis_tuser(auto_ss_slidr_to_m01_couplers_TUSER),
        .m_axis_tvalid(auto_ss_slidr_to_m01_couplers_TVALID),
        .s_axis_tdata(m01_data_fifo_to_auto_ss_slidr_TDATA),
        .s_axis_tdest(m01_data_fifo_to_auto_ss_slidr_TDEST),
        .s_axis_tid(m01_data_fifo_to_auto_ss_slidr_TID),
        .s_axis_tkeep(m01_data_fifo_to_auto_ss_slidr_TKEEP),
        .s_axis_tlast(m01_data_fifo_to_auto_ss_slidr_TLAST),
        .s_axis_tready(m01_data_fifo_to_auto_ss_slidr_TREADY),
        .s_axis_tuser(m01_data_fifo_to_auto_ss_slidr_TUSER),
        .s_axis_tvalid(m01_data_fifo_to_auto_ss_slidr_TVALID));
  design_1_m01_data_fifo_1 m01_data_fifo
       (.axis_data_count(AXIS_DATA_COUNT_to_S_AXIS_DATA_COUNT),
        .axis_rd_data_count(AXIS_RD_DATA_COUNT_to_S_AXIS_RD_DATA_COUNT),
        .axis_wr_data_count(AXIS_WR_DATA_COUNT_to_S_AXIS_WR_DATA_COUNT),
        .m_axis_tdata(m01_data_fifo_to_auto_ss_slidr_TDATA),
        .m_axis_tdest(m01_data_fifo_to_auto_ss_slidr_TDEST),
        .m_axis_tid(m01_data_fifo_to_auto_ss_slidr_TID),
        .m_axis_tkeep(m01_data_fifo_to_auto_ss_slidr_TKEEP),
        .m_axis_tlast(m01_data_fifo_to_auto_ss_slidr_TLAST),
        .m_axis_tready(m01_data_fifo_to_auto_ss_slidr_TREADY),
        .m_axis_tuser(m01_data_fifo_to_auto_ss_slidr_TUSER),
        .m_axis_tvalid(m01_data_fifo_to_auto_ss_slidr_TVALID),
        .s_axis_aclk(S_AXIS_ACLK),
        .s_axis_aresetn(S_AXIS_ARESETN),
        .s_axis_tdata(m01_couplers_to_m01_data_fifo_TDATA),
        .s_axis_tdest(m01_couplers_to_m01_data_fifo_TDEST),
        .s_axis_tid(m01_couplers_to_m01_data_fifo_TID),
        .s_axis_tkeep(m01_couplers_to_m01_data_fifo_TKEEP),
        .s_axis_tlast(m01_couplers_to_m01_data_fifo_TLAST),
        .s_axis_tready(m01_couplers_to_m01_data_fifo_TREADY),
        .s_axis_tuser(m01_couplers_to_m01_data_fifo_TUSER),
        .s_axis_tvalid(m01_couplers_to_m01_data_fifo_TVALID));
endmodule

module m01_couplers_imp_1OHHHDA
   (M_AXIS_ACLK,
    M_AXIS_ARESETN,
    M_AXIS_tdata,
    M_AXIS_tdest,
    M_AXIS_tid,
    M_AXIS_tkeep,
    M_AXIS_tlast,
    M_AXIS_tready,
    M_AXIS_tstrb,
    M_AXIS_tuser,
    M_AXIS_tvalid,
    S_AXIS_ACLK,
    S_AXIS_ARESETN,
    S_AXIS_tdata,
    S_AXIS_tdest,
    S_AXIS_tid,
    S_AXIS_tkeep,
    S_AXIS_tlast,
    S_AXIS_tready,
    S_AXIS_tuser,
    S_AXIS_tvalid);
  input M_AXIS_ACLK;
  input [0:0]M_AXIS_ARESETN;
  output [31:0]M_AXIS_tdata;
  output [4:0]M_AXIS_tdest;
  output [4:0]M_AXIS_tid;
  output [3:0]M_AXIS_tkeep;
  output [0:0]M_AXIS_tlast;
  input M_AXIS_tready;
  output [3:0]M_AXIS_tstrb;
  output [3:0]M_AXIS_tuser;
  output M_AXIS_tvalid;
  input S_AXIS_ACLK;
  input [0:0]S_AXIS_ARESETN;
  input [31:0]S_AXIS_tdata;
  input [4:0]S_AXIS_tdest;
  input [4:0]S_AXIS_tid;
  input [3:0]S_AXIS_tkeep;
  input S_AXIS_tlast;
  output S_AXIS_tready;
  input [3:0]S_AXIS_tuser;
  input S_AXIS_tvalid;

  wire [31:0]AXIS_DATA_COUNT_to_S_AXIS_DATA_COUNT;
  wire [31:0]AXIS_RD_DATA_COUNT_to_S_AXIS_RD_DATA_COUNT;
  wire [31:0]AXIS_WR_DATA_COUNT_to_S_AXIS_WR_DATA_COUNT;
  wire S_AXIS_ACLK;
  wire [0:0]S_AXIS_ARESETN;
  wire [31:0]auto_ss_slidr_to_m01_couplers_TDATA;
  wire [4:0]auto_ss_slidr_to_m01_couplers_TDEST;
  wire [4:0]auto_ss_slidr_to_m01_couplers_TID;
  wire [3:0]auto_ss_slidr_to_m01_couplers_TKEEP;
  wire auto_ss_slidr_to_m01_couplers_TLAST;
  wire auto_ss_slidr_to_m01_couplers_TREADY;
  wire [3:0]auto_ss_slidr_to_m01_couplers_TSTRB;
  wire [3:0]auto_ss_slidr_to_m01_couplers_TUSER;
  wire auto_ss_slidr_to_m01_couplers_TVALID;
  wire [31:0]m01_couplers_to_m01_data_fifo_TDATA;
  wire [4:0]m01_couplers_to_m01_data_fifo_TDEST;
  wire [4:0]m01_couplers_to_m01_data_fifo_TID;
  wire [3:0]m01_couplers_to_m01_data_fifo_TKEEP;
  wire m01_couplers_to_m01_data_fifo_TLAST;
  wire m01_couplers_to_m01_data_fifo_TREADY;
  wire [3:0]m01_couplers_to_m01_data_fifo_TUSER;
  wire m01_couplers_to_m01_data_fifo_TVALID;
  wire [31:0]m01_data_fifo_to_auto_ss_slidr_TDATA;
  wire [4:0]m01_data_fifo_to_auto_ss_slidr_TDEST;
  wire [4:0]m01_data_fifo_to_auto_ss_slidr_TID;
  wire [3:0]m01_data_fifo_to_auto_ss_slidr_TKEEP;
  wire m01_data_fifo_to_auto_ss_slidr_TLAST;
  wire m01_data_fifo_to_auto_ss_slidr_TREADY;
  wire [3:0]m01_data_fifo_to_auto_ss_slidr_TUSER;
  wire m01_data_fifo_to_auto_ss_slidr_TVALID;

  assign M_AXIS_tdata[31:0] = auto_ss_slidr_to_m01_couplers_TDATA;
  assign M_AXIS_tdest[4:0] = auto_ss_slidr_to_m01_couplers_TDEST;
  assign M_AXIS_tid[4:0] = auto_ss_slidr_to_m01_couplers_TID;
  assign M_AXIS_tkeep[3:0] = auto_ss_slidr_to_m01_couplers_TKEEP;
  assign M_AXIS_tlast[0] = auto_ss_slidr_to_m01_couplers_TLAST;
  assign M_AXIS_tstrb[3:0] = auto_ss_slidr_to_m01_couplers_TSTRB;
  assign M_AXIS_tuser[3:0] = auto_ss_slidr_to_m01_couplers_TUSER;
  assign M_AXIS_tvalid = auto_ss_slidr_to_m01_couplers_TVALID;
  assign S_AXIS_tready = m01_couplers_to_m01_data_fifo_TREADY;
  assign auto_ss_slidr_to_m01_couplers_TREADY = M_AXIS_tready;
  assign m01_couplers_to_m01_data_fifo_TDATA = S_AXIS_tdata[31:0];
  assign m01_couplers_to_m01_data_fifo_TDEST = S_AXIS_tdest[4:0];
  assign m01_couplers_to_m01_data_fifo_TID = S_AXIS_tid[4:0];
  assign m01_couplers_to_m01_data_fifo_TKEEP = S_AXIS_tkeep[3:0];
  assign m01_couplers_to_m01_data_fifo_TLAST = S_AXIS_tlast;
  assign m01_couplers_to_m01_data_fifo_TUSER = S_AXIS_tuser[3:0];
  assign m01_couplers_to_m01_data_fifo_TVALID = S_AXIS_tvalid;
  design_1_auto_ss_slidr_1 auto_ss_slidr
       (.aclk(S_AXIS_ACLK),
        .aresetn(S_AXIS_ARESETN),
        .m_axis_tdata(auto_ss_slidr_to_m01_couplers_TDATA),
        .m_axis_tdest(auto_ss_slidr_to_m01_couplers_TDEST),
        .m_axis_tid(auto_ss_slidr_to_m01_couplers_TID),
        .m_axis_tkeep(auto_ss_slidr_to_m01_couplers_TKEEP),
        .m_axis_tlast(auto_ss_slidr_to_m01_couplers_TLAST),
        .m_axis_tready(auto_ss_slidr_to_m01_couplers_TREADY),
        .m_axis_tstrb(auto_ss_slidr_to_m01_couplers_TSTRB),
        .m_axis_tuser(auto_ss_slidr_to_m01_couplers_TUSER),
        .m_axis_tvalid(auto_ss_slidr_to_m01_couplers_TVALID),
        .s_axis_tdata(m01_data_fifo_to_auto_ss_slidr_TDATA),
        .s_axis_tdest(m01_data_fifo_to_auto_ss_slidr_TDEST),
        .s_axis_tid(m01_data_fifo_to_auto_ss_slidr_TID),
        .s_axis_tkeep(m01_data_fifo_to_auto_ss_slidr_TKEEP),
        .s_axis_tlast(m01_data_fifo_to_auto_ss_slidr_TLAST),
        .s_axis_tready(m01_data_fifo_to_auto_ss_slidr_TREADY),
        .s_axis_tuser(m01_data_fifo_to_auto_ss_slidr_TUSER),
        .s_axis_tvalid(m01_data_fifo_to_auto_ss_slidr_TVALID));
  design_1_m01_data_fifo_0 m01_data_fifo
       (.axis_data_count(AXIS_DATA_COUNT_to_S_AXIS_DATA_COUNT),
        .axis_rd_data_count(AXIS_RD_DATA_COUNT_to_S_AXIS_RD_DATA_COUNT),
        .axis_wr_data_count(AXIS_WR_DATA_COUNT_to_S_AXIS_WR_DATA_COUNT),
        .m_axis_tdata(m01_data_fifo_to_auto_ss_slidr_TDATA),
        .m_axis_tdest(m01_data_fifo_to_auto_ss_slidr_TDEST),
        .m_axis_tid(m01_data_fifo_to_auto_ss_slidr_TID),
        .m_axis_tkeep(m01_data_fifo_to_auto_ss_slidr_TKEEP),
        .m_axis_tlast(m01_data_fifo_to_auto_ss_slidr_TLAST),
        .m_axis_tready(m01_data_fifo_to_auto_ss_slidr_TREADY),
        .m_axis_tuser(m01_data_fifo_to_auto_ss_slidr_TUSER),
        .m_axis_tvalid(m01_data_fifo_to_auto_ss_slidr_TVALID),
        .s_axis_aclk(S_AXIS_ACLK),
        .s_axis_aresetn(S_AXIS_ARESETN),
        .s_axis_tdata(m01_couplers_to_m01_data_fifo_TDATA),
        .s_axis_tdest(m01_couplers_to_m01_data_fifo_TDEST),
        .s_axis_tid(m01_couplers_to_m01_data_fifo_TID),
        .s_axis_tkeep(m01_couplers_to_m01_data_fifo_TKEEP),
        .s_axis_tlast(m01_couplers_to_m01_data_fifo_TLAST),
        .s_axis_tready(m01_couplers_to_m01_data_fifo_TREADY),
        .s_axis_tuser(m01_couplers_to_m01_data_fifo_TUSER),
        .s_axis_tvalid(m01_couplers_to_m01_data_fifo_TVALID));
endmodule

module m01_couplers_imp_1QAPM5Q
   (M_AXIS_ACLK,
    M_AXIS_ARESETN,
    M_AXIS_tdata,
    M_AXIS_tdest,
    M_AXIS_tid,
    M_AXIS_tkeep,
    M_AXIS_tlast,
    M_AXIS_tready,
    M_AXIS_tstrb,
    M_AXIS_tuser,
    M_AXIS_tvalid,
    S_AXIS_ACLK,
    S_AXIS_ARESETN,
    S_AXIS_tdata,
    S_AXIS_tdest,
    S_AXIS_tid,
    S_AXIS_tkeep,
    S_AXIS_tlast,
    S_AXIS_tready,
    S_AXIS_tuser,
    S_AXIS_tvalid);
  input M_AXIS_ACLK;
  input [0:0]M_AXIS_ARESETN;
  output [31:0]M_AXIS_tdata;
  output [4:0]M_AXIS_tdest;
  output [4:0]M_AXIS_tid;
  output [3:0]M_AXIS_tkeep;
  output [0:0]M_AXIS_tlast;
  input M_AXIS_tready;
  output [3:0]M_AXIS_tstrb;
  output [3:0]M_AXIS_tuser;
  output M_AXIS_tvalid;
  input S_AXIS_ACLK;
  input [0:0]S_AXIS_ARESETN;
  input [31:0]S_AXIS_tdata;
  input [4:0]S_AXIS_tdest;
  input [4:0]S_AXIS_tid;
  input [3:0]S_AXIS_tkeep;
  input S_AXIS_tlast;
  output S_AXIS_tready;
  input [3:0]S_AXIS_tuser;
  input S_AXIS_tvalid;

  wire [31:0]AXIS_DATA_COUNT_to_S_AXIS_DATA_COUNT;
  wire [31:0]AXIS_RD_DATA_COUNT_to_S_AXIS_RD_DATA_COUNT;
  wire [31:0]AXIS_WR_DATA_COUNT_to_S_AXIS_WR_DATA_COUNT;
  wire S_AXIS_ACLK;
  wire [0:0]S_AXIS_ARESETN;
  wire [31:0]auto_ss_slidr_to_m01_couplers_TDATA;
  wire [4:0]auto_ss_slidr_to_m01_couplers_TDEST;
  wire [4:0]auto_ss_slidr_to_m01_couplers_TID;
  wire [3:0]auto_ss_slidr_to_m01_couplers_TKEEP;
  wire auto_ss_slidr_to_m01_couplers_TLAST;
  wire auto_ss_slidr_to_m01_couplers_TREADY;
  wire [3:0]auto_ss_slidr_to_m01_couplers_TSTRB;
  wire [3:0]auto_ss_slidr_to_m01_couplers_TUSER;
  wire auto_ss_slidr_to_m01_couplers_TVALID;
  wire [31:0]m01_couplers_to_m01_data_fifo_TDATA;
  wire [4:0]m01_couplers_to_m01_data_fifo_TDEST;
  wire [4:0]m01_couplers_to_m01_data_fifo_TID;
  wire [3:0]m01_couplers_to_m01_data_fifo_TKEEP;
  wire m01_couplers_to_m01_data_fifo_TLAST;
  wire m01_couplers_to_m01_data_fifo_TREADY;
  wire [3:0]m01_couplers_to_m01_data_fifo_TUSER;
  wire m01_couplers_to_m01_data_fifo_TVALID;
  wire [31:0]m01_data_fifo_to_auto_ss_slidr_TDATA;
  wire [4:0]m01_data_fifo_to_auto_ss_slidr_TDEST;
  wire [4:0]m01_data_fifo_to_auto_ss_slidr_TID;
  wire [3:0]m01_data_fifo_to_auto_ss_slidr_TKEEP;
  wire m01_data_fifo_to_auto_ss_slidr_TLAST;
  wire m01_data_fifo_to_auto_ss_slidr_TREADY;
  wire [3:0]m01_data_fifo_to_auto_ss_slidr_TUSER;
  wire m01_data_fifo_to_auto_ss_slidr_TVALID;

  assign M_AXIS_tdata[31:0] = auto_ss_slidr_to_m01_couplers_TDATA;
  assign M_AXIS_tdest[4:0] = auto_ss_slidr_to_m01_couplers_TDEST;
  assign M_AXIS_tid[4:0] = auto_ss_slidr_to_m01_couplers_TID;
  assign M_AXIS_tkeep[3:0] = auto_ss_slidr_to_m01_couplers_TKEEP;
  assign M_AXIS_tlast[0] = auto_ss_slidr_to_m01_couplers_TLAST;
  assign M_AXIS_tstrb[3:0] = auto_ss_slidr_to_m01_couplers_TSTRB;
  assign M_AXIS_tuser[3:0] = auto_ss_slidr_to_m01_couplers_TUSER;
  assign M_AXIS_tvalid = auto_ss_slidr_to_m01_couplers_TVALID;
  assign S_AXIS_tready = m01_couplers_to_m01_data_fifo_TREADY;
  assign auto_ss_slidr_to_m01_couplers_TREADY = M_AXIS_tready;
  assign m01_couplers_to_m01_data_fifo_TDATA = S_AXIS_tdata[31:0];
  assign m01_couplers_to_m01_data_fifo_TDEST = S_AXIS_tdest[4:0];
  assign m01_couplers_to_m01_data_fifo_TID = S_AXIS_tid[4:0];
  assign m01_couplers_to_m01_data_fifo_TKEEP = S_AXIS_tkeep[3:0];
  assign m01_couplers_to_m01_data_fifo_TLAST = S_AXIS_tlast;
  assign m01_couplers_to_m01_data_fifo_TUSER = S_AXIS_tuser[3:0];
  assign m01_couplers_to_m01_data_fifo_TVALID = S_AXIS_tvalid;
  design_1_auto_ss_slidr_10 auto_ss_slidr
       (.aclk(S_AXIS_ACLK),
        .aresetn(S_AXIS_ARESETN),
        .m_axis_tdata(auto_ss_slidr_to_m01_couplers_TDATA),
        .m_axis_tdest(auto_ss_slidr_to_m01_couplers_TDEST),
        .m_axis_tid(auto_ss_slidr_to_m01_couplers_TID),
        .m_axis_tkeep(auto_ss_slidr_to_m01_couplers_TKEEP),
        .m_axis_tlast(auto_ss_slidr_to_m01_couplers_TLAST),
        .m_axis_tready(auto_ss_slidr_to_m01_couplers_TREADY),
        .m_axis_tstrb(auto_ss_slidr_to_m01_couplers_TSTRB),
        .m_axis_tuser(auto_ss_slidr_to_m01_couplers_TUSER),
        .m_axis_tvalid(auto_ss_slidr_to_m01_couplers_TVALID),
        .s_axis_tdata(m01_data_fifo_to_auto_ss_slidr_TDATA),
        .s_axis_tdest(m01_data_fifo_to_auto_ss_slidr_TDEST),
        .s_axis_tid(m01_data_fifo_to_auto_ss_slidr_TID),
        .s_axis_tkeep(m01_data_fifo_to_auto_ss_slidr_TKEEP),
        .s_axis_tlast(m01_data_fifo_to_auto_ss_slidr_TLAST),
        .s_axis_tready(m01_data_fifo_to_auto_ss_slidr_TREADY),
        .s_axis_tuser(m01_data_fifo_to_auto_ss_slidr_TUSER),
        .s_axis_tvalid(m01_data_fifo_to_auto_ss_slidr_TVALID));
  design_1_m01_data_fifo_3 m01_data_fifo
       (.axis_data_count(AXIS_DATA_COUNT_to_S_AXIS_DATA_COUNT),
        .axis_rd_data_count(AXIS_RD_DATA_COUNT_to_S_AXIS_RD_DATA_COUNT),
        .axis_wr_data_count(AXIS_WR_DATA_COUNT_to_S_AXIS_WR_DATA_COUNT),
        .m_axis_tdata(m01_data_fifo_to_auto_ss_slidr_TDATA),
        .m_axis_tdest(m01_data_fifo_to_auto_ss_slidr_TDEST),
        .m_axis_tid(m01_data_fifo_to_auto_ss_slidr_TID),
        .m_axis_tkeep(m01_data_fifo_to_auto_ss_slidr_TKEEP),
        .m_axis_tlast(m01_data_fifo_to_auto_ss_slidr_TLAST),
        .m_axis_tready(m01_data_fifo_to_auto_ss_slidr_TREADY),
        .m_axis_tuser(m01_data_fifo_to_auto_ss_slidr_TUSER),
        .m_axis_tvalid(m01_data_fifo_to_auto_ss_slidr_TVALID),
        .s_axis_aclk(S_AXIS_ACLK),
        .s_axis_aresetn(S_AXIS_ARESETN),
        .s_axis_tdata(m01_couplers_to_m01_data_fifo_TDATA),
        .s_axis_tdest(m01_couplers_to_m01_data_fifo_TDEST),
        .s_axis_tid(m01_couplers_to_m01_data_fifo_TID),
        .s_axis_tkeep(m01_couplers_to_m01_data_fifo_TKEEP),
        .s_axis_tlast(m01_couplers_to_m01_data_fifo_TLAST),
        .s_axis_tready(m01_couplers_to_m01_data_fifo_TREADY),
        .s_axis_tuser(m01_couplers_to_m01_data_fifo_TUSER),
        .s_axis_tvalid(m01_couplers_to_m01_data_fifo_TVALID));
endmodule

module m01_couplers_imp_1UTB3Y5
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [31:0]M_AXI_araddr;
  input M_AXI_arready;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  input M_AXI_awready;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [31:0]S_AXI_araddr;
  output S_AXI_arready;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  output S_AXI_awready;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire [31:0]m01_couplers_to_m01_couplers_ARADDR;
  wire m01_couplers_to_m01_couplers_ARREADY;
  wire m01_couplers_to_m01_couplers_ARVALID;
  wire [31:0]m01_couplers_to_m01_couplers_AWADDR;
  wire m01_couplers_to_m01_couplers_AWREADY;
  wire m01_couplers_to_m01_couplers_AWVALID;
  wire m01_couplers_to_m01_couplers_BREADY;
  wire [1:0]m01_couplers_to_m01_couplers_BRESP;
  wire m01_couplers_to_m01_couplers_BVALID;
  wire [31:0]m01_couplers_to_m01_couplers_RDATA;
  wire m01_couplers_to_m01_couplers_RREADY;
  wire [1:0]m01_couplers_to_m01_couplers_RRESP;
  wire m01_couplers_to_m01_couplers_RVALID;
  wire [31:0]m01_couplers_to_m01_couplers_WDATA;
  wire m01_couplers_to_m01_couplers_WREADY;
  wire [3:0]m01_couplers_to_m01_couplers_WSTRB;
  wire m01_couplers_to_m01_couplers_WVALID;

  assign M_AXI_araddr[31:0] = m01_couplers_to_m01_couplers_ARADDR;
  assign M_AXI_arvalid = m01_couplers_to_m01_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = m01_couplers_to_m01_couplers_AWADDR;
  assign M_AXI_awvalid = m01_couplers_to_m01_couplers_AWVALID;
  assign M_AXI_bready = m01_couplers_to_m01_couplers_BREADY;
  assign M_AXI_rready = m01_couplers_to_m01_couplers_RREADY;
  assign M_AXI_wdata[31:0] = m01_couplers_to_m01_couplers_WDATA;
  assign M_AXI_wstrb[3:0] = m01_couplers_to_m01_couplers_WSTRB;
  assign M_AXI_wvalid = m01_couplers_to_m01_couplers_WVALID;
  assign S_AXI_arready = m01_couplers_to_m01_couplers_ARREADY;
  assign S_AXI_awready = m01_couplers_to_m01_couplers_AWREADY;
  assign S_AXI_bresp[1:0] = m01_couplers_to_m01_couplers_BRESP;
  assign S_AXI_bvalid = m01_couplers_to_m01_couplers_BVALID;
  assign S_AXI_rdata[31:0] = m01_couplers_to_m01_couplers_RDATA;
  assign S_AXI_rresp[1:0] = m01_couplers_to_m01_couplers_RRESP;
  assign S_AXI_rvalid = m01_couplers_to_m01_couplers_RVALID;
  assign S_AXI_wready = m01_couplers_to_m01_couplers_WREADY;
  assign m01_couplers_to_m01_couplers_ARADDR = S_AXI_araddr[31:0];
  assign m01_couplers_to_m01_couplers_ARREADY = M_AXI_arready;
  assign m01_couplers_to_m01_couplers_ARVALID = S_AXI_arvalid;
  assign m01_couplers_to_m01_couplers_AWADDR = S_AXI_awaddr[31:0];
  assign m01_couplers_to_m01_couplers_AWREADY = M_AXI_awready;
  assign m01_couplers_to_m01_couplers_AWVALID = S_AXI_awvalid;
  assign m01_couplers_to_m01_couplers_BREADY = S_AXI_bready;
  assign m01_couplers_to_m01_couplers_BRESP = M_AXI_bresp[1:0];
  assign m01_couplers_to_m01_couplers_BVALID = M_AXI_bvalid;
  assign m01_couplers_to_m01_couplers_RDATA = M_AXI_rdata[31:0];
  assign m01_couplers_to_m01_couplers_RREADY = S_AXI_rready;
  assign m01_couplers_to_m01_couplers_RRESP = M_AXI_rresp[1:0];
  assign m01_couplers_to_m01_couplers_RVALID = M_AXI_rvalid;
  assign m01_couplers_to_m01_couplers_WDATA = S_AXI_wdata[31:0];
  assign m01_couplers_to_m01_couplers_WREADY = M_AXI_wready;
  assign m01_couplers_to_m01_couplers_WSTRB = S_AXI_wstrb[3:0];
  assign m01_couplers_to_m01_couplers_WVALID = S_AXI_wvalid;
endmodule

module m01_couplers_imp_1WQ0M9A
   (M_AXIS_ACLK,
    M_AXIS_ARESETN,
    M_AXIS_tdata,
    M_AXIS_tdest,
    M_AXIS_tid,
    M_AXIS_tkeep,
    M_AXIS_tlast,
    M_AXIS_tready,
    M_AXIS_tstrb,
    M_AXIS_tuser,
    M_AXIS_tvalid,
    S_AXIS_ACLK,
    S_AXIS_ARESETN,
    S_AXIS_tdata,
    S_AXIS_tdest,
    S_AXIS_tid,
    S_AXIS_tkeep,
    S_AXIS_tlast,
    S_AXIS_tready,
    S_AXIS_tuser,
    S_AXIS_tvalid);
  input M_AXIS_ACLK;
  input [0:0]M_AXIS_ARESETN;
  output [31:0]M_AXIS_tdata;
  output [4:0]M_AXIS_tdest;
  output [4:0]M_AXIS_tid;
  output [3:0]M_AXIS_tkeep;
  output [0:0]M_AXIS_tlast;
  input M_AXIS_tready;
  output [3:0]M_AXIS_tstrb;
  output [3:0]M_AXIS_tuser;
  output M_AXIS_tvalid;
  input S_AXIS_ACLK;
  input [0:0]S_AXIS_ARESETN;
  input [31:0]S_AXIS_tdata;
  input [4:0]S_AXIS_tdest;
  input [4:0]S_AXIS_tid;
  input [3:0]S_AXIS_tkeep;
  input S_AXIS_tlast;
  output S_AXIS_tready;
  input [3:0]S_AXIS_tuser;
  input S_AXIS_tvalid;

  wire [31:0]AXIS_DATA_COUNT_to_S_AXIS_DATA_COUNT;
  wire [31:0]AXIS_RD_DATA_COUNT_to_S_AXIS_RD_DATA_COUNT;
  wire [31:0]AXIS_WR_DATA_COUNT_to_S_AXIS_WR_DATA_COUNT;
  wire S_AXIS_ACLK;
  wire [0:0]S_AXIS_ARESETN;
  wire [31:0]auto_ss_slidr_to_m01_couplers_TDATA;
  wire [4:0]auto_ss_slidr_to_m01_couplers_TDEST;
  wire [4:0]auto_ss_slidr_to_m01_couplers_TID;
  wire [3:0]auto_ss_slidr_to_m01_couplers_TKEEP;
  wire auto_ss_slidr_to_m01_couplers_TLAST;
  wire auto_ss_slidr_to_m01_couplers_TREADY;
  wire [3:0]auto_ss_slidr_to_m01_couplers_TSTRB;
  wire [3:0]auto_ss_slidr_to_m01_couplers_TUSER;
  wire auto_ss_slidr_to_m01_couplers_TVALID;
  wire [31:0]m01_couplers_to_m01_data_fifo_TDATA;
  wire [4:0]m01_couplers_to_m01_data_fifo_TDEST;
  wire [4:0]m01_couplers_to_m01_data_fifo_TID;
  wire [3:0]m01_couplers_to_m01_data_fifo_TKEEP;
  wire m01_couplers_to_m01_data_fifo_TLAST;
  wire m01_couplers_to_m01_data_fifo_TREADY;
  wire [3:0]m01_couplers_to_m01_data_fifo_TUSER;
  wire m01_couplers_to_m01_data_fifo_TVALID;
  wire [31:0]m01_data_fifo_to_auto_ss_slidr_TDATA;
  wire [4:0]m01_data_fifo_to_auto_ss_slidr_TDEST;
  wire [4:0]m01_data_fifo_to_auto_ss_slidr_TID;
  wire [3:0]m01_data_fifo_to_auto_ss_slidr_TKEEP;
  wire m01_data_fifo_to_auto_ss_slidr_TLAST;
  wire m01_data_fifo_to_auto_ss_slidr_TREADY;
  wire [3:0]m01_data_fifo_to_auto_ss_slidr_TUSER;
  wire m01_data_fifo_to_auto_ss_slidr_TVALID;

  assign M_AXIS_tdata[31:0] = auto_ss_slidr_to_m01_couplers_TDATA;
  assign M_AXIS_tdest[4:0] = auto_ss_slidr_to_m01_couplers_TDEST;
  assign M_AXIS_tid[4:0] = auto_ss_slidr_to_m01_couplers_TID;
  assign M_AXIS_tkeep[3:0] = auto_ss_slidr_to_m01_couplers_TKEEP;
  assign M_AXIS_tlast[0] = auto_ss_slidr_to_m01_couplers_TLAST;
  assign M_AXIS_tstrb[3:0] = auto_ss_slidr_to_m01_couplers_TSTRB;
  assign M_AXIS_tuser[3:0] = auto_ss_slidr_to_m01_couplers_TUSER;
  assign M_AXIS_tvalid = auto_ss_slidr_to_m01_couplers_TVALID;
  assign S_AXIS_tready = m01_couplers_to_m01_data_fifo_TREADY;
  assign auto_ss_slidr_to_m01_couplers_TREADY = M_AXIS_tready;
  assign m01_couplers_to_m01_data_fifo_TDATA = S_AXIS_tdata[31:0];
  assign m01_couplers_to_m01_data_fifo_TDEST = S_AXIS_tdest[4:0];
  assign m01_couplers_to_m01_data_fifo_TID = S_AXIS_tid[4:0];
  assign m01_couplers_to_m01_data_fifo_TKEEP = S_AXIS_tkeep[3:0];
  assign m01_couplers_to_m01_data_fifo_TLAST = S_AXIS_tlast;
  assign m01_couplers_to_m01_data_fifo_TUSER = S_AXIS_tuser[3:0];
  assign m01_couplers_to_m01_data_fifo_TVALID = S_AXIS_tvalid;
  design_1_auto_ss_slidr_7 auto_ss_slidr
       (.aclk(S_AXIS_ACLK),
        .aresetn(S_AXIS_ARESETN),
        .m_axis_tdata(auto_ss_slidr_to_m01_couplers_TDATA),
        .m_axis_tdest(auto_ss_slidr_to_m01_couplers_TDEST),
        .m_axis_tid(auto_ss_slidr_to_m01_couplers_TID),
        .m_axis_tkeep(auto_ss_slidr_to_m01_couplers_TKEEP),
        .m_axis_tlast(auto_ss_slidr_to_m01_couplers_TLAST),
        .m_axis_tready(auto_ss_slidr_to_m01_couplers_TREADY),
        .m_axis_tstrb(auto_ss_slidr_to_m01_couplers_TSTRB),
        .m_axis_tuser(auto_ss_slidr_to_m01_couplers_TUSER),
        .m_axis_tvalid(auto_ss_slidr_to_m01_couplers_TVALID),
        .s_axis_tdata(m01_data_fifo_to_auto_ss_slidr_TDATA),
        .s_axis_tdest(m01_data_fifo_to_auto_ss_slidr_TDEST),
        .s_axis_tid(m01_data_fifo_to_auto_ss_slidr_TID),
        .s_axis_tkeep(m01_data_fifo_to_auto_ss_slidr_TKEEP),
        .s_axis_tlast(m01_data_fifo_to_auto_ss_slidr_TLAST),
        .s_axis_tready(m01_data_fifo_to_auto_ss_slidr_TREADY),
        .s_axis_tuser(m01_data_fifo_to_auto_ss_slidr_TUSER),
        .s_axis_tvalid(m01_data_fifo_to_auto_ss_slidr_TVALID));
  design_1_m01_data_fifo_2 m01_data_fifo
       (.axis_data_count(AXIS_DATA_COUNT_to_S_AXIS_DATA_COUNT),
        .axis_rd_data_count(AXIS_RD_DATA_COUNT_to_S_AXIS_RD_DATA_COUNT),
        .axis_wr_data_count(AXIS_WR_DATA_COUNT_to_S_AXIS_WR_DATA_COUNT),
        .m_axis_tdata(m01_data_fifo_to_auto_ss_slidr_TDATA),
        .m_axis_tdest(m01_data_fifo_to_auto_ss_slidr_TDEST),
        .m_axis_tid(m01_data_fifo_to_auto_ss_slidr_TID),
        .m_axis_tkeep(m01_data_fifo_to_auto_ss_slidr_TKEEP),
        .m_axis_tlast(m01_data_fifo_to_auto_ss_slidr_TLAST),
        .m_axis_tready(m01_data_fifo_to_auto_ss_slidr_TREADY),
        .m_axis_tuser(m01_data_fifo_to_auto_ss_slidr_TUSER),
        .m_axis_tvalid(m01_data_fifo_to_auto_ss_slidr_TVALID),
        .s_axis_aclk(S_AXIS_ACLK),
        .s_axis_aresetn(S_AXIS_ARESETN),
        .s_axis_tdata(m01_couplers_to_m01_data_fifo_TDATA),
        .s_axis_tdest(m01_couplers_to_m01_data_fifo_TDEST),
        .s_axis_tid(m01_couplers_to_m01_data_fifo_TID),
        .s_axis_tkeep(m01_couplers_to_m01_data_fifo_TKEEP),
        .s_axis_tlast(m01_couplers_to_m01_data_fifo_TLAST),
        .s_axis_tready(m01_couplers_to_m01_data_fifo_TREADY),
        .s_axis_tuser(m01_couplers_to_m01_data_fifo_TUSER),
        .s_axis_tvalid(m01_couplers_to_m01_data_fifo_TVALID));
endmodule

module m02_couplers_imp_2RX6OC
   (M_AXIS_ACLK,
    M_AXIS_ARESETN,
    M_AXIS_tdata,
    M_AXIS_tdest,
    M_AXIS_tid,
    M_AXIS_tkeep,
    M_AXIS_tlast,
    M_AXIS_tready,
    M_AXIS_tstrb,
    M_AXIS_tuser,
    M_AXIS_tvalid,
    S_AXIS_ACLK,
    S_AXIS_ARESETN,
    S_AXIS_tdata,
    S_AXIS_tdest,
    S_AXIS_tid,
    S_AXIS_tkeep,
    S_AXIS_tlast,
    S_AXIS_tready,
    S_AXIS_tuser,
    S_AXIS_tvalid);
  input M_AXIS_ACLK;
  input [0:0]M_AXIS_ARESETN;
  output [31:0]M_AXIS_tdata;
  output [4:0]M_AXIS_tdest;
  output [4:0]M_AXIS_tid;
  output [3:0]M_AXIS_tkeep;
  output [0:0]M_AXIS_tlast;
  input M_AXIS_tready;
  output [3:0]M_AXIS_tstrb;
  output [3:0]M_AXIS_tuser;
  output M_AXIS_tvalid;
  input S_AXIS_ACLK;
  input [0:0]S_AXIS_ARESETN;
  input [31:0]S_AXIS_tdata;
  input [4:0]S_AXIS_tdest;
  input [4:0]S_AXIS_tid;
  input [3:0]S_AXIS_tkeep;
  input S_AXIS_tlast;
  output S_AXIS_tready;
  input [3:0]S_AXIS_tuser;
  input S_AXIS_tvalid;

  wire [31:0]AXIS_DATA_COUNT_to_S_AXIS_DATA_COUNT;
  wire [31:0]AXIS_RD_DATA_COUNT_to_S_AXIS_RD_DATA_COUNT;
  wire [31:0]AXIS_WR_DATA_COUNT_to_S_AXIS_WR_DATA_COUNT;
  wire S_AXIS_ACLK;
  wire [0:0]S_AXIS_ARESETN;
  wire [31:0]auto_ss_slidr_to_m02_couplers_TDATA;
  wire [4:0]auto_ss_slidr_to_m02_couplers_TDEST;
  wire [4:0]auto_ss_slidr_to_m02_couplers_TID;
  wire [3:0]auto_ss_slidr_to_m02_couplers_TKEEP;
  wire auto_ss_slidr_to_m02_couplers_TLAST;
  wire auto_ss_slidr_to_m02_couplers_TREADY;
  wire [3:0]auto_ss_slidr_to_m02_couplers_TSTRB;
  wire [3:0]auto_ss_slidr_to_m02_couplers_TUSER;
  wire auto_ss_slidr_to_m02_couplers_TVALID;
  wire [31:0]m02_couplers_to_m02_data_fifo_TDATA;
  wire [4:0]m02_couplers_to_m02_data_fifo_TDEST;
  wire [4:0]m02_couplers_to_m02_data_fifo_TID;
  wire [3:0]m02_couplers_to_m02_data_fifo_TKEEP;
  wire m02_couplers_to_m02_data_fifo_TLAST;
  wire m02_couplers_to_m02_data_fifo_TREADY;
  wire [3:0]m02_couplers_to_m02_data_fifo_TUSER;
  wire m02_couplers_to_m02_data_fifo_TVALID;
  wire [31:0]m02_data_fifo_to_auto_ss_slidr_TDATA;
  wire [4:0]m02_data_fifo_to_auto_ss_slidr_TDEST;
  wire [4:0]m02_data_fifo_to_auto_ss_slidr_TID;
  wire [3:0]m02_data_fifo_to_auto_ss_slidr_TKEEP;
  wire m02_data_fifo_to_auto_ss_slidr_TLAST;
  wire m02_data_fifo_to_auto_ss_slidr_TREADY;
  wire [3:0]m02_data_fifo_to_auto_ss_slidr_TUSER;
  wire m02_data_fifo_to_auto_ss_slidr_TVALID;

  assign M_AXIS_tdata[31:0] = auto_ss_slidr_to_m02_couplers_TDATA;
  assign M_AXIS_tdest[4:0] = auto_ss_slidr_to_m02_couplers_TDEST;
  assign M_AXIS_tid[4:0] = auto_ss_slidr_to_m02_couplers_TID;
  assign M_AXIS_tkeep[3:0] = auto_ss_slidr_to_m02_couplers_TKEEP;
  assign M_AXIS_tlast[0] = auto_ss_slidr_to_m02_couplers_TLAST;
  assign M_AXIS_tstrb[3:0] = auto_ss_slidr_to_m02_couplers_TSTRB;
  assign M_AXIS_tuser[3:0] = auto_ss_slidr_to_m02_couplers_TUSER;
  assign M_AXIS_tvalid = auto_ss_slidr_to_m02_couplers_TVALID;
  assign S_AXIS_tready = m02_couplers_to_m02_data_fifo_TREADY;
  assign auto_ss_slidr_to_m02_couplers_TREADY = M_AXIS_tready;
  assign m02_couplers_to_m02_data_fifo_TDATA = S_AXIS_tdata[31:0];
  assign m02_couplers_to_m02_data_fifo_TDEST = S_AXIS_tdest[4:0];
  assign m02_couplers_to_m02_data_fifo_TID = S_AXIS_tid[4:0];
  assign m02_couplers_to_m02_data_fifo_TKEEP = S_AXIS_tkeep[3:0];
  assign m02_couplers_to_m02_data_fifo_TLAST = S_AXIS_tlast;
  assign m02_couplers_to_m02_data_fifo_TUSER = S_AXIS_tuser[3:0];
  assign m02_couplers_to_m02_data_fifo_TVALID = S_AXIS_tvalid;
  design_1_auto_ss_slidr_11 auto_ss_slidr
       (.aclk(S_AXIS_ACLK),
        .aresetn(S_AXIS_ARESETN),
        .m_axis_tdata(auto_ss_slidr_to_m02_couplers_TDATA),
        .m_axis_tdest(auto_ss_slidr_to_m02_couplers_TDEST),
        .m_axis_tid(auto_ss_slidr_to_m02_couplers_TID),
        .m_axis_tkeep(auto_ss_slidr_to_m02_couplers_TKEEP),
        .m_axis_tlast(auto_ss_slidr_to_m02_couplers_TLAST),
        .m_axis_tready(auto_ss_slidr_to_m02_couplers_TREADY),
        .m_axis_tstrb(auto_ss_slidr_to_m02_couplers_TSTRB),
        .m_axis_tuser(auto_ss_slidr_to_m02_couplers_TUSER),
        .m_axis_tvalid(auto_ss_slidr_to_m02_couplers_TVALID),
        .s_axis_tdata(m02_data_fifo_to_auto_ss_slidr_TDATA),
        .s_axis_tdest(m02_data_fifo_to_auto_ss_slidr_TDEST),
        .s_axis_tid(m02_data_fifo_to_auto_ss_slidr_TID),
        .s_axis_tkeep(m02_data_fifo_to_auto_ss_slidr_TKEEP),
        .s_axis_tlast(m02_data_fifo_to_auto_ss_slidr_TLAST),
        .s_axis_tready(m02_data_fifo_to_auto_ss_slidr_TREADY),
        .s_axis_tuser(m02_data_fifo_to_auto_ss_slidr_TUSER),
        .s_axis_tvalid(m02_data_fifo_to_auto_ss_slidr_TVALID));
  design_1_m02_data_fifo_3 m02_data_fifo
       (.axis_data_count(AXIS_DATA_COUNT_to_S_AXIS_DATA_COUNT),
        .axis_rd_data_count(AXIS_RD_DATA_COUNT_to_S_AXIS_RD_DATA_COUNT),
        .axis_wr_data_count(AXIS_WR_DATA_COUNT_to_S_AXIS_WR_DATA_COUNT),
        .m_axis_tdata(m02_data_fifo_to_auto_ss_slidr_TDATA),
        .m_axis_tdest(m02_data_fifo_to_auto_ss_slidr_TDEST),
        .m_axis_tid(m02_data_fifo_to_auto_ss_slidr_TID),
        .m_axis_tkeep(m02_data_fifo_to_auto_ss_slidr_TKEEP),
        .m_axis_tlast(m02_data_fifo_to_auto_ss_slidr_TLAST),
        .m_axis_tready(m02_data_fifo_to_auto_ss_slidr_TREADY),
        .m_axis_tuser(m02_data_fifo_to_auto_ss_slidr_TUSER),
        .m_axis_tvalid(m02_data_fifo_to_auto_ss_slidr_TVALID),
        .s_axis_aclk(S_AXIS_ACLK),
        .s_axis_aresetn(S_AXIS_ARESETN),
        .s_axis_tdata(m02_couplers_to_m02_data_fifo_TDATA),
        .s_axis_tdest(m02_couplers_to_m02_data_fifo_TDEST),
        .s_axis_tid(m02_couplers_to_m02_data_fifo_TID),
        .s_axis_tkeep(m02_couplers_to_m02_data_fifo_TKEEP),
        .s_axis_tlast(m02_couplers_to_m02_data_fifo_TLAST),
        .s_axis_tready(m02_couplers_to_m02_data_fifo_TREADY),
        .s_axis_tuser(m02_couplers_to_m02_data_fifo_TUSER),
        .s_axis_tvalid(m02_couplers_to_m02_data_fifo_TVALID));
endmodule

module m02_couplers_imp_7ANRHB
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [31:0]M_AXI_araddr;
  input M_AXI_arready;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  input M_AXI_awready;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [31:0]S_AXI_araddr;
  output S_AXI_arready;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  output S_AXI_awready;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire [31:0]m02_couplers_to_m02_couplers_ARADDR;
  wire m02_couplers_to_m02_couplers_ARREADY;
  wire m02_couplers_to_m02_couplers_ARVALID;
  wire [31:0]m02_couplers_to_m02_couplers_AWADDR;
  wire m02_couplers_to_m02_couplers_AWREADY;
  wire m02_couplers_to_m02_couplers_AWVALID;
  wire m02_couplers_to_m02_couplers_BREADY;
  wire [1:0]m02_couplers_to_m02_couplers_BRESP;
  wire m02_couplers_to_m02_couplers_BVALID;
  wire [31:0]m02_couplers_to_m02_couplers_RDATA;
  wire m02_couplers_to_m02_couplers_RREADY;
  wire [1:0]m02_couplers_to_m02_couplers_RRESP;
  wire m02_couplers_to_m02_couplers_RVALID;
  wire [31:0]m02_couplers_to_m02_couplers_WDATA;
  wire m02_couplers_to_m02_couplers_WREADY;
  wire [3:0]m02_couplers_to_m02_couplers_WSTRB;
  wire m02_couplers_to_m02_couplers_WVALID;

  assign M_AXI_araddr[31:0] = m02_couplers_to_m02_couplers_ARADDR;
  assign M_AXI_arvalid = m02_couplers_to_m02_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = m02_couplers_to_m02_couplers_AWADDR;
  assign M_AXI_awvalid = m02_couplers_to_m02_couplers_AWVALID;
  assign M_AXI_bready = m02_couplers_to_m02_couplers_BREADY;
  assign M_AXI_rready = m02_couplers_to_m02_couplers_RREADY;
  assign M_AXI_wdata[31:0] = m02_couplers_to_m02_couplers_WDATA;
  assign M_AXI_wstrb[3:0] = m02_couplers_to_m02_couplers_WSTRB;
  assign M_AXI_wvalid = m02_couplers_to_m02_couplers_WVALID;
  assign S_AXI_arready = m02_couplers_to_m02_couplers_ARREADY;
  assign S_AXI_awready = m02_couplers_to_m02_couplers_AWREADY;
  assign S_AXI_bresp[1:0] = m02_couplers_to_m02_couplers_BRESP;
  assign S_AXI_bvalid = m02_couplers_to_m02_couplers_BVALID;
  assign S_AXI_rdata[31:0] = m02_couplers_to_m02_couplers_RDATA;
  assign S_AXI_rresp[1:0] = m02_couplers_to_m02_couplers_RRESP;
  assign S_AXI_rvalid = m02_couplers_to_m02_couplers_RVALID;
  assign S_AXI_wready = m02_couplers_to_m02_couplers_WREADY;
  assign m02_couplers_to_m02_couplers_ARADDR = S_AXI_araddr[31:0];
  assign m02_couplers_to_m02_couplers_ARREADY = M_AXI_arready;
  assign m02_couplers_to_m02_couplers_ARVALID = S_AXI_arvalid;
  assign m02_couplers_to_m02_couplers_AWADDR = S_AXI_awaddr[31:0];
  assign m02_couplers_to_m02_couplers_AWREADY = M_AXI_awready;
  assign m02_couplers_to_m02_couplers_AWVALID = S_AXI_awvalid;
  assign m02_couplers_to_m02_couplers_BREADY = S_AXI_bready;
  assign m02_couplers_to_m02_couplers_BRESP = M_AXI_bresp[1:0];
  assign m02_couplers_to_m02_couplers_BVALID = M_AXI_bvalid;
  assign m02_couplers_to_m02_couplers_RDATA = M_AXI_rdata[31:0];
  assign m02_couplers_to_m02_couplers_RREADY = S_AXI_rready;
  assign m02_couplers_to_m02_couplers_RRESP = M_AXI_rresp[1:0];
  assign m02_couplers_to_m02_couplers_RVALID = M_AXI_rvalid;
  assign m02_couplers_to_m02_couplers_WDATA = S_AXI_wdata[31:0];
  assign m02_couplers_to_m02_couplers_WREADY = M_AXI_wready;
  assign m02_couplers_to_m02_couplers_WSTRB = S_AXI_wstrb[3:0];
  assign m02_couplers_to_m02_couplers_WVALID = S_AXI_wvalid;
endmodule

module m02_couplers_imp_8395N0
   (M_AXIS_ACLK,
    M_AXIS_ARESETN,
    M_AXIS_tdata,
    M_AXIS_tdest,
    M_AXIS_tid,
    M_AXIS_tkeep,
    M_AXIS_tlast,
    M_AXIS_tready,
    M_AXIS_tstrb,
    M_AXIS_tuser,
    M_AXIS_tvalid,
    S_AXIS_ACLK,
    S_AXIS_ARESETN,
    S_AXIS_tdata,
    S_AXIS_tdest,
    S_AXIS_tid,
    S_AXIS_tkeep,
    S_AXIS_tlast,
    S_AXIS_tready,
    S_AXIS_tuser,
    S_AXIS_tvalid);
  input M_AXIS_ACLK;
  input [0:0]M_AXIS_ARESETN;
  output [31:0]M_AXIS_tdata;
  output [4:0]M_AXIS_tdest;
  output [4:0]M_AXIS_tid;
  output [3:0]M_AXIS_tkeep;
  output [0:0]M_AXIS_tlast;
  input M_AXIS_tready;
  output [3:0]M_AXIS_tstrb;
  output [3:0]M_AXIS_tuser;
  output M_AXIS_tvalid;
  input S_AXIS_ACLK;
  input [0:0]S_AXIS_ARESETN;
  input [31:0]S_AXIS_tdata;
  input [4:0]S_AXIS_tdest;
  input [4:0]S_AXIS_tid;
  input [3:0]S_AXIS_tkeep;
  input S_AXIS_tlast;
  output S_AXIS_tready;
  input [3:0]S_AXIS_tuser;
  input S_AXIS_tvalid;

  wire [31:0]AXIS_DATA_COUNT_to_S_AXIS_DATA_COUNT;
  wire [31:0]AXIS_RD_DATA_COUNT_to_S_AXIS_RD_DATA_COUNT;
  wire [31:0]AXIS_WR_DATA_COUNT_to_S_AXIS_WR_DATA_COUNT;
  wire S_AXIS_ACLK;
  wire [0:0]S_AXIS_ARESETN;
  wire [31:0]auto_ss_slidr_to_m02_couplers_TDATA;
  wire [4:0]auto_ss_slidr_to_m02_couplers_TDEST;
  wire [4:0]auto_ss_slidr_to_m02_couplers_TID;
  wire [3:0]auto_ss_slidr_to_m02_couplers_TKEEP;
  wire auto_ss_slidr_to_m02_couplers_TLAST;
  wire auto_ss_slidr_to_m02_couplers_TREADY;
  wire [3:0]auto_ss_slidr_to_m02_couplers_TSTRB;
  wire [3:0]auto_ss_slidr_to_m02_couplers_TUSER;
  wire auto_ss_slidr_to_m02_couplers_TVALID;
  wire [31:0]m02_couplers_to_m02_data_fifo_TDATA;
  wire [4:0]m02_couplers_to_m02_data_fifo_TDEST;
  wire [4:0]m02_couplers_to_m02_data_fifo_TID;
  wire [3:0]m02_couplers_to_m02_data_fifo_TKEEP;
  wire m02_couplers_to_m02_data_fifo_TLAST;
  wire m02_couplers_to_m02_data_fifo_TREADY;
  wire [3:0]m02_couplers_to_m02_data_fifo_TUSER;
  wire m02_couplers_to_m02_data_fifo_TVALID;
  wire [31:0]m02_data_fifo_to_auto_ss_slidr_TDATA;
  wire [4:0]m02_data_fifo_to_auto_ss_slidr_TDEST;
  wire [4:0]m02_data_fifo_to_auto_ss_slidr_TID;
  wire [3:0]m02_data_fifo_to_auto_ss_slidr_TKEEP;
  wire m02_data_fifo_to_auto_ss_slidr_TLAST;
  wire m02_data_fifo_to_auto_ss_slidr_TREADY;
  wire [3:0]m02_data_fifo_to_auto_ss_slidr_TUSER;
  wire m02_data_fifo_to_auto_ss_slidr_TVALID;

  assign M_AXIS_tdata[31:0] = auto_ss_slidr_to_m02_couplers_TDATA;
  assign M_AXIS_tdest[4:0] = auto_ss_slidr_to_m02_couplers_TDEST;
  assign M_AXIS_tid[4:0] = auto_ss_slidr_to_m02_couplers_TID;
  assign M_AXIS_tkeep[3:0] = auto_ss_slidr_to_m02_couplers_TKEEP;
  assign M_AXIS_tlast[0] = auto_ss_slidr_to_m02_couplers_TLAST;
  assign M_AXIS_tstrb[3:0] = auto_ss_slidr_to_m02_couplers_TSTRB;
  assign M_AXIS_tuser[3:0] = auto_ss_slidr_to_m02_couplers_TUSER;
  assign M_AXIS_tvalid = auto_ss_slidr_to_m02_couplers_TVALID;
  assign S_AXIS_tready = m02_couplers_to_m02_data_fifo_TREADY;
  assign auto_ss_slidr_to_m02_couplers_TREADY = M_AXIS_tready;
  assign m02_couplers_to_m02_data_fifo_TDATA = S_AXIS_tdata[31:0];
  assign m02_couplers_to_m02_data_fifo_TDEST = S_AXIS_tdest[4:0];
  assign m02_couplers_to_m02_data_fifo_TID = S_AXIS_tid[4:0];
  assign m02_couplers_to_m02_data_fifo_TKEEP = S_AXIS_tkeep[3:0];
  assign m02_couplers_to_m02_data_fifo_TLAST = S_AXIS_tlast;
  assign m02_couplers_to_m02_data_fifo_TUSER = S_AXIS_tuser[3:0];
  assign m02_couplers_to_m02_data_fifo_TVALID = S_AXIS_tvalid;
  design_1_auto_ss_slidr_8 auto_ss_slidr
       (.aclk(S_AXIS_ACLK),
        .aresetn(S_AXIS_ARESETN),
        .m_axis_tdata(auto_ss_slidr_to_m02_couplers_TDATA),
        .m_axis_tdest(auto_ss_slidr_to_m02_couplers_TDEST),
        .m_axis_tid(auto_ss_slidr_to_m02_couplers_TID),
        .m_axis_tkeep(auto_ss_slidr_to_m02_couplers_TKEEP),
        .m_axis_tlast(auto_ss_slidr_to_m02_couplers_TLAST),
        .m_axis_tready(auto_ss_slidr_to_m02_couplers_TREADY),
        .m_axis_tstrb(auto_ss_slidr_to_m02_couplers_TSTRB),
        .m_axis_tuser(auto_ss_slidr_to_m02_couplers_TUSER),
        .m_axis_tvalid(auto_ss_slidr_to_m02_couplers_TVALID),
        .s_axis_tdata(m02_data_fifo_to_auto_ss_slidr_TDATA),
        .s_axis_tdest(m02_data_fifo_to_auto_ss_slidr_TDEST),
        .s_axis_tid(m02_data_fifo_to_auto_ss_slidr_TID),
        .s_axis_tkeep(m02_data_fifo_to_auto_ss_slidr_TKEEP),
        .s_axis_tlast(m02_data_fifo_to_auto_ss_slidr_TLAST),
        .s_axis_tready(m02_data_fifo_to_auto_ss_slidr_TREADY),
        .s_axis_tuser(m02_data_fifo_to_auto_ss_slidr_TUSER),
        .s_axis_tvalid(m02_data_fifo_to_auto_ss_slidr_TVALID));
  design_1_m02_data_fifo_2 m02_data_fifo
       (.axis_data_count(AXIS_DATA_COUNT_to_S_AXIS_DATA_COUNT),
        .axis_rd_data_count(AXIS_RD_DATA_COUNT_to_S_AXIS_RD_DATA_COUNT),
        .axis_wr_data_count(AXIS_WR_DATA_COUNT_to_S_AXIS_WR_DATA_COUNT),
        .m_axis_tdata(m02_data_fifo_to_auto_ss_slidr_TDATA),
        .m_axis_tdest(m02_data_fifo_to_auto_ss_slidr_TDEST),
        .m_axis_tid(m02_data_fifo_to_auto_ss_slidr_TID),
        .m_axis_tkeep(m02_data_fifo_to_auto_ss_slidr_TKEEP),
        .m_axis_tlast(m02_data_fifo_to_auto_ss_slidr_TLAST),
        .m_axis_tready(m02_data_fifo_to_auto_ss_slidr_TREADY),
        .m_axis_tuser(m02_data_fifo_to_auto_ss_slidr_TUSER),
        .m_axis_tvalid(m02_data_fifo_to_auto_ss_slidr_TVALID),
        .s_axis_aclk(S_AXIS_ACLK),
        .s_axis_aresetn(S_AXIS_ARESETN),
        .s_axis_tdata(m02_couplers_to_m02_data_fifo_TDATA),
        .s_axis_tdest(m02_couplers_to_m02_data_fifo_TDEST),
        .s_axis_tid(m02_couplers_to_m02_data_fifo_TID),
        .s_axis_tkeep(m02_couplers_to_m02_data_fifo_TKEEP),
        .s_axis_tlast(m02_couplers_to_m02_data_fifo_TLAST),
        .s_axis_tready(m02_couplers_to_m02_data_fifo_TREADY),
        .s_axis_tuser(m02_couplers_to_m02_data_fifo_TUSER),
        .s_axis_tvalid(m02_couplers_to_m02_data_fifo_TVALID));
endmodule

module m02_couplers_imp_A2PZM4
   (M_AXIS_ACLK,
    M_AXIS_ARESETN,
    M_AXIS_tdata,
    M_AXIS_tdest,
    M_AXIS_tid,
    M_AXIS_tkeep,
    M_AXIS_tlast,
    M_AXIS_tready,
    M_AXIS_tstrb,
    M_AXIS_tuser,
    M_AXIS_tvalid,
    S_AXIS_ACLK,
    S_AXIS_ARESETN,
    S_AXIS_tdata,
    S_AXIS_tdest,
    S_AXIS_tid,
    S_AXIS_tkeep,
    S_AXIS_tlast,
    S_AXIS_tready,
    S_AXIS_tuser,
    S_AXIS_tvalid);
  input M_AXIS_ACLK;
  input [0:0]M_AXIS_ARESETN;
  output [31:0]M_AXIS_tdata;
  output [4:0]M_AXIS_tdest;
  output [4:0]M_AXIS_tid;
  output [3:0]M_AXIS_tkeep;
  output [0:0]M_AXIS_tlast;
  input M_AXIS_tready;
  output [3:0]M_AXIS_tstrb;
  output [3:0]M_AXIS_tuser;
  output M_AXIS_tvalid;
  input S_AXIS_ACLK;
  input [0:0]S_AXIS_ARESETN;
  input [31:0]S_AXIS_tdata;
  input [4:0]S_AXIS_tdest;
  input [4:0]S_AXIS_tid;
  input [3:0]S_AXIS_tkeep;
  input S_AXIS_tlast;
  output S_AXIS_tready;
  input [3:0]S_AXIS_tuser;
  input S_AXIS_tvalid;

  wire [31:0]AXIS_DATA_COUNT_to_S_AXIS_DATA_COUNT;
  wire [31:0]AXIS_RD_DATA_COUNT_to_S_AXIS_RD_DATA_COUNT;
  wire [31:0]AXIS_WR_DATA_COUNT_to_S_AXIS_WR_DATA_COUNT;
  wire S_AXIS_ACLK;
  wire [0:0]S_AXIS_ARESETN;
  wire [31:0]auto_ss_slidr_to_m02_couplers_TDATA;
  wire [4:0]auto_ss_slidr_to_m02_couplers_TDEST;
  wire [4:0]auto_ss_slidr_to_m02_couplers_TID;
  wire [3:0]auto_ss_slidr_to_m02_couplers_TKEEP;
  wire auto_ss_slidr_to_m02_couplers_TLAST;
  wire auto_ss_slidr_to_m02_couplers_TREADY;
  wire [3:0]auto_ss_slidr_to_m02_couplers_TSTRB;
  wire [3:0]auto_ss_slidr_to_m02_couplers_TUSER;
  wire auto_ss_slidr_to_m02_couplers_TVALID;
  wire [31:0]m02_couplers_to_m02_data_fifo_TDATA;
  wire [4:0]m02_couplers_to_m02_data_fifo_TDEST;
  wire [4:0]m02_couplers_to_m02_data_fifo_TID;
  wire [3:0]m02_couplers_to_m02_data_fifo_TKEEP;
  wire m02_couplers_to_m02_data_fifo_TLAST;
  wire m02_couplers_to_m02_data_fifo_TREADY;
  wire [3:0]m02_couplers_to_m02_data_fifo_TUSER;
  wire m02_couplers_to_m02_data_fifo_TVALID;
  wire [31:0]m02_data_fifo_to_auto_ss_slidr_TDATA;
  wire [4:0]m02_data_fifo_to_auto_ss_slidr_TDEST;
  wire [4:0]m02_data_fifo_to_auto_ss_slidr_TID;
  wire [3:0]m02_data_fifo_to_auto_ss_slidr_TKEEP;
  wire m02_data_fifo_to_auto_ss_slidr_TLAST;
  wire m02_data_fifo_to_auto_ss_slidr_TREADY;
  wire [3:0]m02_data_fifo_to_auto_ss_slidr_TUSER;
  wire m02_data_fifo_to_auto_ss_slidr_TVALID;

  assign M_AXIS_tdata[31:0] = auto_ss_slidr_to_m02_couplers_TDATA;
  assign M_AXIS_tdest[4:0] = auto_ss_slidr_to_m02_couplers_TDEST;
  assign M_AXIS_tid[4:0] = auto_ss_slidr_to_m02_couplers_TID;
  assign M_AXIS_tkeep[3:0] = auto_ss_slidr_to_m02_couplers_TKEEP;
  assign M_AXIS_tlast[0] = auto_ss_slidr_to_m02_couplers_TLAST;
  assign M_AXIS_tstrb[3:0] = auto_ss_slidr_to_m02_couplers_TSTRB;
  assign M_AXIS_tuser[3:0] = auto_ss_slidr_to_m02_couplers_TUSER;
  assign M_AXIS_tvalid = auto_ss_slidr_to_m02_couplers_TVALID;
  assign S_AXIS_tready = m02_couplers_to_m02_data_fifo_TREADY;
  assign auto_ss_slidr_to_m02_couplers_TREADY = M_AXIS_tready;
  assign m02_couplers_to_m02_data_fifo_TDATA = S_AXIS_tdata[31:0];
  assign m02_couplers_to_m02_data_fifo_TDEST = S_AXIS_tdest[4:0];
  assign m02_couplers_to_m02_data_fifo_TID = S_AXIS_tid[4:0];
  assign m02_couplers_to_m02_data_fifo_TKEEP = S_AXIS_tkeep[3:0];
  assign m02_couplers_to_m02_data_fifo_TLAST = S_AXIS_tlast;
  assign m02_couplers_to_m02_data_fifo_TUSER = S_AXIS_tuser[3:0];
  assign m02_couplers_to_m02_data_fifo_TVALID = S_AXIS_tvalid;
  design_1_auto_ss_slidr_5 auto_ss_slidr
       (.aclk(S_AXIS_ACLK),
        .aresetn(S_AXIS_ARESETN),
        .m_axis_tdata(auto_ss_slidr_to_m02_couplers_TDATA),
        .m_axis_tdest(auto_ss_slidr_to_m02_couplers_TDEST),
        .m_axis_tid(auto_ss_slidr_to_m02_couplers_TID),
        .m_axis_tkeep(auto_ss_slidr_to_m02_couplers_TKEEP),
        .m_axis_tlast(auto_ss_slidr_to_m02_couplers_TLAST),
        .m_axis_tready(auto_ss_slidr_to_m02_couplers_TREADY),
        .m_axis_tstrb(auto_ss_slidr_to_m02_couplers_TSTRB),
        .m_axis_tuser(auto_ss_slidr_to_m02_couplers_TUSER),
        .m_axis_tvalid(auto_ss_slidr_to_m02_couplers_TVALID),
        .s_axis_tdata(m02_data_fifo_to_auto_ss_slidr_TDATA),
        .s_axis_tdest(m02_data_fifo_to_auto_ss_slidr_TDEST),
        .s_axis_tid(m02_data_fifo_to_auto_ss_slidr_TID),
        .s_axis_tkeep(m02_data_fifo_to_auto_ss_slidr_TKEEP),
        .s_axis_tlast(m02_data_fifo_to_auto_ss_slidr_TLAST),
        .s_axis_tready(m02_data_fifo_to_auto_ss_slidr_TREADY),
        .s_axis_tuser(m02_data_fifo_to_auto_ss_slidr_TUSER),
        .s_axis_tvalid(m02_data_fifo_to_auto_ss_slidr_TVALID));
  design_1_m02_data_fifo_1 m02_data_fifo
       (.axis_data_count(AXIS_DATA_COUNT_to_S_AXIS_DATA_COUNT),
        .axis_rd_data_count(AXIS_RD_DATA_COUNT_to_S_AXIS_RD_DATA_COUNT),
        .axis_wr_data_count(AXIS_WR_DATA_COUNT_to_S_AXIS_WR_DATA_COUNT),
        .m_axis_tdata(m02_data_fifo_to_auto_ss_slidr_TDATA),
        .m_axis_tdest(m02_data_fifo_to_auto_ss_slidr_TDEST),
        .m_axis_tid(m02_data_fifo_to_auto_ss_slidr_TID),
        .m_axis_tkeep(m02_data_fifo_to_auto_ss_slidr_TKEEP),
        .m_axis_tlast(m02_data_fifo_to_auto_ss_slidr_TLAST),
        .m_axis_tready(m02_data_fifo_to_auto_ss_slidr_TREADY),
        .m_axis_tuser(m02_data_fifo_to_auto_ss_slidr_TUSER),
        .m_axis_tvalid(m02_data_fifo_to_auto_ss_slidr_TVALID),
        .s_axis_aclk(S_AXIS_ACLK),
        .s_axis_aresetn(S_AXIS_ARESETN),
        .s_axis_tdata(m02_couplers_to_m02_data_fifo_TDATA),
        .s_axis_tdest(m02_couplers_to_m02_data_fifo_TDEST),
        .s_axis_tid(m02_couplers_to_m02_data_fifo_TID),
        .s_axis_tkeep(m02_couplers_to_m02_data_fifo_TKEEP),
        .s_axis_tlast(m02_couplers_to_m02_data_fifo_TLAST),
        .s_axis_tready(m02_couplers_to_m02_data_fifo_TREADY),
        .s_axis_tuser(m02_couplers_to_m02_data_fifo_TUSER),
        .s_axis_tvalid(m02_couplers_to_m02_data_fifo_TVALID));
endmodule

module m02_couplers_imp_EA4A4S
   (M_AXIS_ACLK,
    M_AXIS_ARESETN,
    M_AXIS_tdata,
    M_AXIS_tdest,
    M_AXIS_tid,
    M_AXIS_tkeep,
    M_AXIS_tlast,
    M_AXIS_tready,
    M_AXIS_tstrb,
    M_AXIS_tuser,
    M_AXIS_tvalid,
    S_AXIS_ACLK,
    S_AXIS_ARESETN,
    S_AXIS_tdata,
    S_AXIS_tdest,
    S_AXIS_tid,
    S_AXIS_tkeep,
    S_AXIS_tlast,
    S_AXIS_tready,
    S_AXIS_tuser,
    S_AXIS_tvalid);
  input M_AXIS_ACLK;
  input [0:0]M_AXIS_ARESETN;
  output [31:0]M_AXIS_tdata;
  output [4:0]M_AXIS_tdest;
  output [4:0]M_AXIS_tid;
  output [3:0]M_AXIS_tkeep;
  output [0:0]M_AXIS_tlast;
  input M_AXIS_tready;
  output [3:0]M_AXIS_tstrb;
  output [3:0]M_AXIS_tuser;
  output M_AXIS_tvalid;
  input S_AXIS_ACLK;
  input [0:0]S_AXIS_ARESETN;
  input [31:0]S_AXIS_tdata;
  input [4:0]S_AXIS_tdest;
  input [4:0]S_AXIS_tid;
  input [3:0]S_AXIS_tkeep;
  input S_AXIS_tlast;
  output S_AXIS_tready;
  input [3:0]S_AXIS_tuser;
  input S_AXIS_tvalid;

  wire [31:0]AXIS_DATA_COUNT_to_S_AXIS_DATA_COUNT;
  wire [31:0]AXIS_RD_DATA_COUNT_to_S_AXIS_RD_DATA_COUNT;
  wire [31:0]AXIS_WR_DATA_COUNT_to_S_AXIS_WR_DATA_COUNT;
  wire S_AXIS_ACLK;
  wire [0:0]S_AXIS_ARESETN;
  wire [31:0]auto_ss_slidr_to_m02_couplers_TDATA;
  wire [4:0]auto_ss_slidr_to_m02_couplers_TDEST;
  wire [4:0]auto_ss_slidr_to_m02_couplers_TID;
  wire [3:0]auto_ss_slidr_to_m02_couplers_TKEEP;
  wire auto_ss_slidr_to_m02_couplers_TLAST;
  wire auto_ss_slidr_to_m02_couplers_TREADY;
  wire [3:0]auto_ss_slidr_to_m02_couplers_TSTRB;
  wire [3:0]auto_ss_slidr_to_m02_couplers_TUSER;
  wire auto_ss_slidr_to_m02_couplers_TVALID;
  wire [31:0]m02_couplers_to_m02_data_fifo_TDATA;
  wire [4:0]m02_couplers_to_m02_data_fifo_TDEST;
  wire [4:0]m02_couplers_to_m02_data_fifo_TID;
  wire [3:0]m02_couplers_to_m02_data_fifo_TKEEP;
  wire m02_couplers_to_m02_data_fifo_TLAST;
  wire m02_couplers_to_m02_data_fifo_TREADY;
  wire [3:0]m02_couplers_to_m02_data_fifo_TUSER;
  wire m02_couplers_to_m02_data_fifo_TVALID;
  wire [31:0]m02_data_fifo_to_auto_ss_slidr_TDATA;
  wire [4:0]m02_data_fifo_to_auto_ss_slidr_TDEST;
  wire [4:0]m02_data_fifo_to_auto_ss_slidr_TID;
  wire [3:0]m02_data_fifo_to_auto_ss_slidr_TKEEP;
  wire m02_data_fifo_to_auto_ss_slidr_TLAST;
  wire m02_data_fifo_to_auto_ss_slidr_TREADY;
  wire [3:0]m02_data_fifo_to_auto_ss_slidr_TUSER;
  wire m02_data_fifo_to_auto_ss_slidr_TVALID;

  assign M_AXIS_tdata[31:0] = auto_ss_slidr_to_m02_couplers_TDATA;
  assign M_AXIS_tdest[4:0] = auto_ss_slidr_to_m02_couplers_TDEST;
  assign M_AXIS_tid[4:0] = auto_ss_slidr_to_m02_couplers_TID;
  assign M_AXIS_tkeep[3:0] = auto_ss_slidr_to_m02_couplers_TKEEP;
  assign M_AXIS_tlast[0] = auto_ss_slidr_to_m02_couplers_TLAST;
  assign M_AXIS_tstrb[3:0] = auto_ss_slidr_to_m02_couplers_TSTRB;
  assign M_AXIS_tuser[3:0] = auto_ss_slidr_to_m02_couplers_TUSER;
  assign M_AXIS_tvalid = auto_ss_slidr_to_m02_couplers_TVALID;
  assign S_AXIS_tready = m02_couplers_to_m02_data_fifo_TREADY;
  assign auto_ss_slidr_to_m02_couplers_TREADY = M_AXIS_tready;
  assign m02_couplers_to_m02_data_fifo_TDATA = S_AXIS_tdata[31:0];
  assign m02_couplers_to_m02_data_fifo_TDEST = S_AXIS_tdest[4:0];
  assign m02_couplers_to_m02_data_fifo_TID = S_AXIS_tid[4:0];
  assign m02_couplers_to_m02_data_fifo_TKEEP = S_AXIS_tkeep[3:0];
  assign m02_couplers_to_m02_data_fifo_TLAST = S_AXIS_tlast;
  assign m02_couplers_to_m02_data_fifo_TUSER = S_AXIS_tuser[3:0];
  assign m02_couplers_to_m02_data_fifo_TVALID = S_AXIS_tvalid;
  design_1_auto_ss_slidr_2 auto_ss_slidr
       (.aclk(S_AXIS_ACLK),
        .aresetn(S_AXIS_ARESETN),
        .m_axis_tdata(auto_ss_slidr_to_m02_couplers_TDATA),
        .m_axis_tdest(auto_ss_slidr_to_m02_couplers_TDEST),
        .m_axis_tid(auto_ss_slidr_to_m02_couplers_TID),
        .m_axis_tkeep(auto_ss_slidr_to_m02_couplers_TKEEP),
        .m_axis_tlast(auto_ss_slidr_to_m02_couplers_TLAST),
        .m_axis_tready(auto_ss_slidr_to_m02_couplers_TREADY),
        .m_axis_tstrb(auto_ss_slidr_to_m02_couplers_TSTRB),
        .m_axis_tuser(auto_ss_slidr_to_m02_couplers_TUSER),
        .m_axis_tvalid(auto_ss_slidr_to_m02_couplers_TVALID),
        .s_axis_tdata(m02_data_fifo_to_auto_ss_slidr_TDATA),
        .s_axis_tdest(m02_data_fifo_to_auto_ss_slidr_TDEST),
        .s_axis_tid(m02_data_fifo_to_auto_ss_slidr_TID),
        .s_axis_tkeep(m02_data_fifo_to_auto_ss_slidr_TKEEP),
        .s_axis_tlast(m02_data_fifo_to_auto_ss_slidr_TLAST),
        .s_axis_tready(m02_data_fifo_to_auto_ss_slidr_TREADY),
        .s_axis_tuser(m02_data_fifo_to_auto_ss_slidr_TUSER),
        .s_axis_tvalid(m02_data_fifo_to_auto_ss_slidr_TVALID));
  design_1_m02_data_fifo_0 m02_data_fifo
       (.axis_data_count(AXIS_DATA_COUNT_to_S_AXIS_DATA_COUNT),
        .axis_rd_data_count(AXIS_RD_DATA_COUNT_to_S_AXIS_RD_DATA_COUNT),
        .axis_wr_data_count(AXIS_WR_DATA_COUNT_to_S_AXIS_WR_DATA_COUNT),
        .m_axis_tdata(m02_data_fifo_to_auto_ss_slidr_TDATA),
        .m_axis_tdest(m02_data_fifo_to_auto_ss_slidr_TDEST),
        .m_axis_tid(m02_data_fifo_to_auto_ss_slidr_TID),
        .m_axis_tkeep(m02_data_fifo_to_auto_ss_slidr_TKEEP),
        .m_axis_tlast(m02_data_fifo_to_auto_ss_slidr_TLAST),
        .m_axis_tready(m02_data_fifo_to_auto_ss_slidr_TREADY),
        .m_axis_tuser(m02_data_fifo_to_auto_ss_slidr_TUSER),
        .m_axis_tvalid(m02_data_fifo_to_auto_ss_slidr_TVALID),
        .s_axis_aclk(S_AXIS_ACLK),
        .s_axis_aresetn(S_AXIS_ARESETN),
        .s_axis_tdata(m02_couplers_to_m02_data_fifo_TDATA),
        .s_axis_tdest(m02_couplers_to_m02_data_fifo_TDEST),
        .s_axis_tid(m02_couplers_to_m02_data_fifo_TID),
        .s_axis_tkeep(m02_couplers_to_m02_data_fifo_TKEEP),
        .s_axis_tlast(m02_couplers_to_m02_data_fifo_TLAST),
        .s_axis_tready(m02_couplers_to_m02_data_fifo_TREADY),
        .s_axis_tuser(m02_couplers_to_m02_data_fifo_TUSER),
        .s_axis_tvalid(m02_couplers_to_m02_data_fifo_TVALID));
endmodule

module m03_couplers_imp_1W07O72
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [31:0]M_AXI_araddr;
  input M_AXI_arready;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  input M_AXI_awready;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input M_AXI_wready;
  output M_AXI_wvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [31:0]S_AXI_araddr;
  output S_AXI_arready;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  output S_AXI_awready;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  output S_AXI_wready;
  input S_AXI_wvalid;

  wire [31:0]m03_couplers_to_m03_couplers_ARADDR;
  wire m03_couplers_to_m03_couplers_ARREADY;
  wire m03_couplers_to_m03_couplers_ARVALID;
  wire [31:0]m03_couplers_to_m03_couplers_AWADDR;
  wire m03_couplers_to_m03_couplers_AWREADY;
  wire m03_couplers_to_m03_couplers_AWVALID;
  wire m03_couplers_to_m03_couplers_BREADY;
  wire [1:0]m03_couplers_to_m03_couplers_BRESP;
  wire m03_couplers_to_m03_couplers_BVALID;
  wire [31:0]m03_couplers_to_m03_couplers_RDATA;
  wire m03_couplers_to_m03_couplers_RREADY;
  wire [1:0]m03_couplers_to_m03_couplers_RRESP;
  wire m03_couplers_to_m03_couplers_RVALID;
  wire [31:0]m03_couplers_to_m03_couplers_WDATA;
  wire m03_couplers_to_m03_couplers_WREADY;
  wire m03_couplers_to_m03_couplers_WVALID;

  assign M_AXI_araddr[31:0] = m03_couplers_to_m03_couplers_ARADDR;
  assign M_AXI_arvalid = m03_couplers_to_m03_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = m03_couplers_to_m03_couplers_AWADDR;
  assign M_AXI_awvalid = m03_couplers_to_m03_couplers_AWVALID;
  assign M_AXI_bready = m03_couplers_to_m03_couplers_BREADY;
  assign M_AXI_rready = m03_couplers_to_m03_couplers_RREADY;
  assign M_AXI_wdata[31:0] = m03_couplers_to_m03_couplers_WDATA;
  assign M_AXI_wvalid = m03_couplers_to_m03_couplers_WVALID;
  assign S_AXI_arready = m03_couplers_to_m03_couplers_ARREADY;
  assign S_AXI_awready = m03_couplers_to_m03_couplers_AWREADY;
  assign S_AXI_bresp[1:0] = m03_couplers_to_m03_couplers_BRESP;
  assign S_AXI_bvalid = m03_couplers_to_m03_couplers_BVALID;
  assign S_AXI_rdata[31:0] = m03_couplers_to_m03_couplers_RDATA;
  assign S_AXI_rresp[1:0] = m03_couplers_to_m03_couplers_RRESP;
  assign S_AXI_rvalid = m03_couplers_to_m03_couplers_RVALID;
  assign S_AXI_wready = m03_couplers_to_m03_couplers_WREADY;
  assign m03_couplers_to_m03_couplers_ARADDR = S_AXI_araddr[31:0];
  assign m03_couplers_to_m03_couplers_ARREADY = M_AXI_arready;
  assign m03_couplers_to_m03_couplers_ARVALID = S_AXI_arvalid;
  assign m03_couplers_to_m03_couplers_AWADDR = S_AXI_awaddr[31:0];
  assign m03_couplers_to_m03_couplers_AWREADY = M_AXI_awready;
  assign m03_couplers_to_m03_couplers_AWVALID = S_AXI_awvalid;
  assign m03_couplers_to_m03_couplers_BREADY = S_AXI_bready;
  assign m03_couplers_to_m03_couplers_BRESP = M_AXI_bresp[1:0];
  assign m03_couplers_to_m03_couplers_BVALID = M_AXI_bvalid;
  assign m03_couplers_to_m03_couplers_RDATA = M_AXI_rdata[31:0];
  assign m03_couplers_to_m03_couplers_RREADY = S_AXI_rready;
  assign m03_couplers_to_m03_couplers_RRESP = M_AXI_rresp[1:0];
  assign m03_couplers_to_m03_couplers_RVALID = M_AXI_rvalid;
  assign m03_couplers_to_m03_couplers_WDATA = S_AXI_wdata[31:0];
  assign m03_couplers_to_m03_couplers_WREADY = M_AXI_wready;
  assign m03_couplers_to_m03_couplers_WVALID = S_AXI_wvalid;
endmodule

module m04_couplers_imp_5LX7BU
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [31:0]M_AXI_araddr;
  input M_AXI_arready;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  input M_AXI_awready;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input M_AXI_wready;
  output M_AXI_wvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [31:0]S_AXI_araddr;
  output S_AXI_arready;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  output S_AXI_awready;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  output S_AXI_wready;
  input S_AXI_wvalid;

  wire [31:0]m04_couplers_to_m04_couplers_ARADDR;
  wire m04_couplers_to_m04_couplers_ARREADY;
  wire m04_couplers_to_m04_couplers_ARVALID;
  wire [31:0]m04_couplers_to_m04_couplers_AWADDR;
  wire m04_couplers_to_m04_couplers_AWREADY;
  wire m04_couplers_to_m04_couplers_AWVALID;
  wire m04_couplers_to_m04_couplers_BREADY;
  wire [1:0]m04_couplers_to_m04_couplers_BRESP;
  wire m04_couplers_to_m04_couplers_BVALID;
  wire [31:0]m04_couplers_to_m04_couplers_RDATA;
  wire m04_couplers_to_m04_couplers_RREADY;
  wire [1:0]m04_couplers_to_m04_couplers_RRESP;
  wire m04_couplers_to_m04_couplers_RVALID;
  wire [31:0]m04_couplers_to_m04_couplers_WDATA;
  wire m04_couplers_to_m04_couplers_WREADY;
  wire m04_couplers_to_m04_couplers_WVALID;

  assign M_AXI_araddr[31:0] = m04_couplers_to_m04_couplers_ARADDR;
  assign M_AXI_arvalid = m04_couplers_to_m04_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = m04_couplers_to_m04_couplers_AWADDR;
  assign M_AXI_awvalid = m04_couplers_to_m04_couplers_AWVALID;
  assign M_AXI_bready = m04_couplers_to_m04_couplers_BREADY;
  assign M_AXI_rready = m04_couplers_to_m04_couplers_RREADY;
  assign M_AXI_wdata[31:0] = m04_couplers_to_m04_couplers_WDATA;
  assign M_AXI_wvalid = m04_couplers_to_m04_couplers_WVALID;
  assign S_AXI_arready = m04_couplers_to_m04_couplers_ARREADY;
  assign S_AXI_awready = m04_couplers_to_m04_couplers_AWREADY;
  assign S_AXI_bresp[1:0] = m04_couplers_to_m04_couplers_BRESP;
  assign S_AXI_bvalid = m04_couplers_to_m04_couplers_BVALID;
  assign S_AXI_rdata[31:0] = m04_couplers_to_m04_couplers_RDATA;
  assign S_AXI_rresp[1:0] = m04_couplers_to_m04_couplers_RRESP;
  assign S_AXI_rvalid = m04_couplers_to_m04_couplers_RVALID;
  assign S_AXI_wready = m04_couplers_to_m04_couplers_WREADY;
  assign m04_couplers_to_m04_couplers_ARADDR = S_AXI_araddr[31:0];
  assign m04_couplers_to_m04_couplers_ARREADY = M_AXI_arready;
  assign m04_couplers_to_m04_couplers_ARVALID = S_AXI_arvalid;
  assign m04_couplers_to_m04_couplers_AWADDR = S_AXI_awaddr[31:0];
  assign m04_couplers_to_m04_couplers_AWREADY = M_AXI_awready;
  assign m04_couplers_to_m04_couplers_AWVALID = S_AXI_awvalid;
  assign m04_couplers_to_m04_couplers_BREADY = S_AXI_bready;
  assign m04_couplers_to_m04_couplers_BRESP = M_AXI_bresp[1:0];
  assign m04_couplers_to_m04_couplers_BVALID = M_AXI_bvalid;
  assign m04_couplers_to_m04_couplers_RDATA = M_AXI_rdata[31:0];
  assign m04_couplers_to_m04_couplers_RREADY = S_AXI_rready;
  assign m04_couplers_to_m04_couplers_RRESP = M_AXI_rresp[1:0];
  assign m04_couplers_to_m04_couplers_RVALID = M_AXI_rvalid;
  assign m04_couplers_to_m04_couplers_WDATA = S_AXI_wdata[31:0];
  assign m04_couplers_to_m04_couplers_WREADY = M_AXI_wready;
  assign m04_couplers_to_m04_couplers_WVALID = S_AXI_wvalid;
endmodule

module m05_couplers_imp_1XR4ZAZ
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [31:0]M_AXI_araddr;
  input M_AXI_arready;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  input M_AXI_awready;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input M_AXI_wready;
  output M_AXI_wvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [31:0]S_AXI_araddr;
  output S_AXI_arready;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  output S_AXI_awready;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  output S_AXI_wready;
  input S_AXI_wvalid;

  wire [31:0]m05_couplers_to_m05_couplers_ARADDR;
  wire m05_couplers_to_m05_couplers_ARREADY;
  wire m05_couplers_to_m05_couplers_ARVALID;
  wire [31:0]m05_couplers_to_m05_couplers_AWADDR;
  wire m05_couplers_to_m05_couplers_AWREADY;
  wire m05_couplers_to_m05_couplers_AWVALID;
  wire m05_couplers_to_m05_couplers_BREADY;
  wire [1:0]m05_couplers_to_m05_couplers_BRESP;
  wire m05_couplers_to_m05_couplers_BVALID;
  wire [31:0]m05_couplers_to_m05_couplers_RDATA;
  wire m05_couplers_to_m05_couplers_RREADY;
  wire [1:0]m05_couplers_to_m05_couplers_RRESP;
  wire m05_couplers_to_m05_couplers_RVALID;
  wire [31:0]m05_couplers_to_m05_couplers_WDATA;
  wire m05_couplers_to_m05_couplers_WREADY;
  wire m05_couplers_to_m05_couplers_WVALID;

  assign M_AXI_araddr[31:0] = m05_couplers_to_m05_couplers_ARADDR;
  assign M_AXI_arvalid = m05_couplers_to_m05_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = m05_couplers_to_m05_couplers_AWADDR;
  assign M_AXI_awvalid = m05_couplers_to_m05_couplers_AWVALID;
  assign M_AXI_bready = m05_couplers_to_m05_couplers_BREADY;
  assign M_AXI_rready = m05_couplers_to_m05_couplers_RREADY;
  assign M_AXI_wdata[31:0] = m05_couplers_to_m05_couplers_WDATA;
  assign M_AXI_wvalid = m05_couplers_to_m05_couplers_WVALID;
  assign S_AXI_arready = m05_couplers_to_m05_couplers_ARREADY;
  assign S_AXI_awready = m05_couplers_to_m05_couplers_AWREADY;
  assign S_AXI_bresp[1:0] = m05_couplers_to_m05_couplers_BRESP;
  assign S_AXI_bvalid = m05_couplers_to_m05_couplers_BVALID;
  assign S_AXI_rdata[31:0] = m05_couplers_to_m05_couplers_RDATA;
  assign S_AXI_rresp[1:0] = m05_couplers_to_m05_couplers_RRESP;
  assign S_AXI_rvalid = m05_couplers_to_m05_couplers_RVALID;
  assign S_AXI_wready = m05_couplers_to_m05_couplers_WREADY;
  assign m05_couplers_to_m05_couplers_ARADDR = S_AXI_araddr[31:0];
  assign m05_couplers_to_m05_couplers_ARREADY = M_AXI_arready;
  assign m05_couplers_to_m05_couplers_ARVALID = S_AXI_arvalid;
  assign m05_couplers_to_m05_couplers_AWADDR = S_AXI_awaddr[31:0];
  assign m05_couplers_to_m05_couplers_AWREADY = M_AXI_awready;
  assign m05_couplers_to_m05_couplers_AWVALID = S_AXI_awvalid;
  assign m05_couplers_to_m05_couplers_BREADY = S_AXI_bready;
  assign m05_couplers_to_m05_couplers_BRESP = M_AXI_bresp[1:0];
  assign m05_couplers_to_m05_couplers_BVALID = M_AXI_bvalid;
  assign m05_couplers_to_m05_couplers_RDATA = M_AXI_rdata[31:0];
  assign m05_couplers_to_m05_couplers_RREADY = S_AXI_rready;
  assign m05_couplers_to_m05_couplers_RRESP = M_AXI_rresp[1:0];
  assign m05_couplers_to_m05_couplers_RVALID = M_AXI_rvalid;
  assign m05_couplers_to_m05_couplers_WDATA = S_AXI_wdata[31:0];
  assign m05_couplers_to_m05_couplers_WREADY = M_AXI_wready;
  assign m05_couplers_to_m05_couplers_WVALID = S_AXI_wvalid;
endmodule

module m06_couplers_imp_4YOIXL
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [31:0]M_AXI_araddr;
  input M_AXI_arready;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  input M_AXI_awready;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input M_AXI_wready;
  output M_AXI_wvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [31:0]S_AXI_araddr;
  output S_AXI_arready;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  output S_AXI_awready;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  output S_AXI_wready;
  input S_AXI_wvalid;

  wire [31:0]m06_couplers_to_m06_couplers_ARADDR;
  wire m06_couplers_to_m06_couplers_ARREADY;
  wire m06_couplers_to_m06_couplers_ARVALID;
  wire [31:0]m06_couplers_to_m06_couplers_AWADDR;
  wire m06_couplers_to_m06_couplers_AWREADY;
  wire m06_couplers_to_m06_couplers_AWVALID;
  wire m06_couplers_to_m06_couplers_BREADY;
  wire [1:0]m06_couplers_to_m06_couplers_BRESP;
  wire m06_couplers_to_m06_couplers_BVALID;
  wire [31:0]m06_couplers_to_m06_couplers_RDATA;
  wire m06_couplers_to_m06_couplers_RREADY;
  wire [1:0]m06_couplers_to_m06_couplers_RRESP;
  wire m06_couplers_to_m06_couplers_RVALID;
  wire [31:0]m06_couplers_to_m06_couplers_WDATA;
  wire m06_couplers_to_m06_couplers_WREADY;
  wire m06_couplers_to_m06_couplers_WVALID;

  assign M_AXI_araddr[31:0] = m06_couplers_to_m06_couplers_ARADDR;
  assign M_AXI_arvalid = m06_couplers_to_m06_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = m06_couplers_to_m06_couplers_AWADDR;
  assign M_AXI_awvalid = m06_couplers_to_m06_couplers_AWVALID;
  assign M_AXI_bready = m06_couplers_to_m06_couplers_BREADY;
  assign M_AXI_rready = m06_couplers_to_m06_couplers_RREADY;
  assign M_AXI_wdata[31:0] = m06_couplers_to_m06_couplers_WDATA;
  assign M_AXI_wvalid = m06_couplers_to_m06_couplers_WVALID;
  assign S_AXI_arready = m06_couplers_to_m06_couplers_ARREADY;
  assign S_AXI_awready = m06_couplers_to_m06_couplers_AWREADY;
  assign S_AXI_bresp[1:0] = m06_couplers_to_m06_couplers_BRESP;
  assign S_AXI_bvalid = m06_couplers_to_m06_couplers_BVALID;
  assign S_AXI_rdata[31:0] = m06_couplers_to_m06_couplers_RDATA;
  assign S_AXI_rresp[1:0] = m06_couplers_to_m06_couplers_RRESP;
  assign S_AXI_rvalid = m06_couplers_to_m06_couplers_RVALID;
  assign S_AXI_wready = m06_couplers_to_m06_couplers_WREADY;
  assign m06_couplers_to_m06_couplers_ARADDR = S_AXI_araddr[31:0];
  assign m06_couplers_to_m06_couplers_ARREADY = M_AXI_arready;
  assign m06_couplers_to_m06_couplers_ARVALID = S_AXI_arvalid;
  assign m06_couplers_to_m06_couplers_AWADDR = S_AXI_awaddr[31:0];
  assign m06_couplers_to_m06_couplers_AWREADY = M_AXI_awready;
  assign m06_couplers_to_m06_couplers_AWVALID = S_AXI_awvalid;
  assign m06_couplers_to_m06_couplers_BREADY = S_AXI_bready;
  assign m06_couplers_to_m06_couplers_BRESP = M_AXI_bresp[1:0];
  assign m06_couplers_to_m06_couplers_BVALID = M_AXI_bvalid;
  assign m06_couplers_to_m06_couplers_RDATA = M_AXI_rdata[31:0];
  assign m06_couplers_to_m06_couplers_RREADY = S_AXI_rready;
  assign m06_couplers_to_m06_couplers_RRESP = M_AXI_rresp[1:0];
  assign m06_couplers_to_m06_couplers_RVALID = M_AXI_rvalid;
  assign m06_couplers_to_m06_couplers_WDATA = S_AXI_wdata[31:0];
  assign m06_couplers_to_m06_couplers_WREADY = M_AXI_wready;
  assign m06_couplers_to_m06_couplers_WVALID = S_AXI_wvalid;
endmodule

module microblaze_0_local_memory_imp_1K0VQXK
   (DLMB_abus,
    DLMB_addrstrobe,
    DLMB_be,
    DLMB_ce,
    DLMB_readdbus,
    DLMB_readstrobe,
    DLMB_ready,
    DLMB_ue,
    DLMB_wait,
    DLMB_writedbus,
    DLMB_writestrobe,
    ILMB_abus,
    ILMB_addrstrobe,
    ILMB_ce,
    ILMB_readdbus,
    ILMB_readstrobe,
    ILMB_ready,
    ILMB_ue,
    ILMB_wait,
    LMB_Clk,
    SYS_Rst);
  input [0:31]DLMB_abus;
  input DLMB_addrstrobe;
  input [0:3]DLMB_be;
  output DLMB_ce;
  output [0:31]DLMB_readdbus;
  input DLMB_readstrobe;
  output DLMB_ready;
  output DLMB_ue;
  output DLMB_wait;
  input [0:31]DLMB_writedbus;
  input DLMB_writestrobe;
  input [0:31]ILMB_abus;
  input ILMB_addrstrobe;
  output ILMB_ce;
  output [0:31]ILMB_readdbus;
  input ILMB_readstrobe;
  output ILMB_ready;
  output ILMB_ue;
  output ILMB_wait;
  input LMB_Clk;
  input [0:0]SYS_Rst;

  wire [0:0]SYS_Rst_1;
  wire microblaze_0_Clk;
  wire [0:31]microblaze_0_dlmb_ABUS;
  wire microblaze_0_dlmb_ADDRSTROBE;
  wire [0:3]microblaze_0_dlmb_BE;
  wire microblaze_0_dlmb_CE;
  wire [0:31]microblaze_0_dlmb_READDBUS;
  wire microblaze_0_dlmb_READSTROBE;
  wire microblaze_0_dlmb_READY;
  wire microblaze_0_dlmb_UE;
  wire microblaze_0_dlmb_WAIT;
  wire [0:31]microblaze_0_dlmb_WRITEDBUS;
  wire microblaze_0_dlmb_WRITESTROBE;
  wire [0:31]microblaze_0_dlmb_bus_ABUS;
  wire microblaze_0_dlmb_bus_ADDRSTROBE;
  wire [0:3]microblaze_0_dlmb_bus_BE;
  wire microblaze_0_dlmb_bus_CE;
  wire [0:31]microblaze_0_dlmb_bus_READDBUS;
  wire microblaze_0_dlmb_bus_READSTROBE;
  wire microblaze_0_dlmb_bus_READY;
  wire microblaze_0_dlmb_bus_UE;
  wire microblaze_0_dlmb_bus_WAIT;
  wire [0:31]microblaze_0_dlmb_bus_WRITEDBUS;
  wire microblaze_0_dlmb_bus_WRITESTROBE;
  wire [0:31]microblaze_0_dlmb_cntlr_ADDR;
  wire microblaze_0_dlmb_cntlr_CLK;
  wire [0:31]microblaze_0_dlmb_cntlr_DIN;
  wire [31:0]microblaze_0_dlmb_cntlr_DOUT;
  wire microblaze_0_dlmb_cntlr_EN;
  wire microblaze_0_dlmb_cntlr_RST;
  wire [0:3]microblaze_0_dlmb_cntlr_WE;
  wire [0:31]microblaze_0_ilmb_ABUS;
  wire microblaze_0_ilmb_ADDRSTROBE;
  wire microblaze_0_ilmb_CE;
  wire [0:31]microblaze_0_ilmb_READDBUS;
  wire microblaze_0_ilmb_READSTROBE;
  wire microblaze_0_ilmb_READY;
  wire microblaze_0_ilmb_UE;
  wire microblaze_0_ilmb_WAIT;
  wire [0:31]microblaze_0_ilmb_bus_ABUS;
  wire microblaze_0_ilmb_bus_ADDRSTROBE;
  wire [0:3]microblaze_0_ilmb_bus_BE;
  wire microblaze_0_ilmb_bus_CE;
  wire [0:31]microblaze_0_ilmb_bus_READDBUS;
  wire microblaze_0_ilmb_bus_READSTROBE;
  wire microblaze_0_ilmb_bus_READY;
  wire microblaze_0_ilmb_bus_UE;
  wire microblaze_0_ilmb_bus_WAIT;
  wire [0:31]microblaze_0_ilmb_bus_WRITEDBUS;
  wire microblaze_0_ilmb_bus_WRITESTROBE;
  wire [0:31]microblaze_0_ilmb_cntlr_ADDR;
  wire microblaze_0_ilmb_cntlr_CLK;
  wire [0:31]microblaze_0_ilmb_cntlr_DIN;
  wire [31:0]microblaze_0_ilmb_cntlr_DOUT;
  wire microblaze_0_ilmb_cntlr_EN;
  wire microblaze_0_ilmb_cntlr_RST;
  wire [0:3]microblaze_0_ilmb_cntlr_WE;

  assign DLMB_ce = microblaze_0_dlmb_CE;
  assign DLMB_readdbus[0:31] = microblaze_0_dlmb_READDBUS;
  assign DLMB_ready = microblaze_0_dlmb_READY;
  assign DLMB_ue = microblaze_0_dlmb_UE;
  assign DLMB_wait = microblaze_0_dlmb_WAIT;
  assign ILMB_ce = microblaze_0_ilmb_CE;
  assign ILMB_readdbus[0:31] = microblaze_0_ilmb_READDBUS;
  assign ILMB_ready = microblaze_0_ilmb_READY;
  assign ILMB_ue = microblaze_0_ilmb_UE;
  assign ILMB_wait = microblaze_0_ilmb_WAIT;
  assign SYS_Rst_1 = SYS_Rst[0];
  assign microblaze_0_Clk = LMB_Clk;
  assign microblaze_0_dlmb_ABUS = DLMB_abus[0:31];
  assign microblaze_0_dlmb_ADDRSTROBE = DLMB_addrstrobe;
  assign microblaze_0_dlmb_BE = DLMB_be[0:3];
  assign microblaze_0_dlmb_READSTROBE = DLMB_readstrobe;
  assign microblaze_0_dlmb_WRITEDBUS = DLMB_writedbus[0:31];
  assign microblaze_0_dlmb_WRITESTROBE = DLMB_writestrobe;
  assign microblaze_0_ilmb_ABUS = ILMB_abus[0:31];
  assign microblaze_0_ilmb_ADDRSTROBE = ILMB_addrstrobe;
  assign microblaze_0_ilmb_READSTROBE = ILMB_readstrobe;
  (* BMM_INFO_ADDRESS_SPACE = "byte  0x0 32 > design_1 microblaze_0_local_memory/lmb_bram" *) 
  (* KEEP_HIERARCHY = "yes" *) 
  design_1_dlmb_bram_if_cntlr_0 dlmb_bram_if_cntlr
       (.BRAM_Addr_A(microblaze_0_dlmb_cntlr_ADDR),
        .BRAM_Clk_A(microblaze_0_dlmb_cntlr_CLK),
        .BRAM_Din_A({microblaze_0_dlmb_cntlr_DOUT[31],microblaze_0_dlmb_cntlr_DOUT[30],microblaze_0_dlmb_cntlr_DOUT[29],microblaze_0_dlmb_cntlr_DOUT[28],microblaze_0_dlmb_cntlr_DOUT[27],microblaze_0_dlmb_cntlr_DOUT[26],microblaze_0_dlmb_cntlr_DOUT[25],microblaze_0_dlmb_cntlr_DOUT[24],microblaze_0_dlmb_cntlr_DOUT[23],microblaze_0_dlmb_cntlr_DOUT[22],microblaze_0_dlmb_cntlr_DOUT[21],microblaze_0_dlmb_cntlr_DOUT[20],microblaze_0_dlmb_cntlr_DOUT[19],microblaze_0_dlmb_cntlr_DOUT[18],microblaze_0_dlmb_cntlr_DOUT[17],microblaze_0_dlmb_cntlr_DOUT[16],microblaze_0_dlmb_cntlr_DOUT[15],microblaze_0_dlmb_cntlr_DOUT[14],microblaze_0_dlmb_cntlr_DOUT[13],microblaze_0_dlmb_cntlr_DOUT[12],microblaze_0_dlmb_cntlr_DOUT[11],microblaze_0_dlmb_cntlr_DOUT[10],microblaze_0_dlmb_cntlr_DOUT[9],microblaze_0_dlmb_cntlr_DOUT[8],microblaze_0_dlmb_cntlr_DOUT[7],microblaze_0_dlmb_cntlr_DOUT[6],microblaze_0_dlmb_cntlr_DOUT[5],microblaze_0_dlmb_cntlr_DOUT[4],microblaze_0_dlmb_cntlr_DOUT[3],microblaze_0_dlmb_cntlr_DOUT[2],microblaze_0_dlmb_cntlr_DOUT[1],microblaze_0_dlmb_cntlr_DOUT[0]}),
        .BRAM_Dout_A(microblaze_0_dlmb_cntlr_DIN),
        .BRAM_EN_A(microblaze_0_dlmb_cntlr_EN),
        .BRAM_Rst_A(microblaze_0_dlmb_cntlr_RST),
        .BRAM_WEN_A(microblaze_0_dlmb_cntlr_WE),
        .LMB_ABus(microblaze_0_dlmb_bus_ABUS),
        .LMB_AddrStrobe(microblaze_0_dlmb_bus_ADDRSTROBE),
        .LMB_BE(microblaze_0_dlmb_bus_BE),
        .LMB_Clk(microblaze_0_Clk),
        .LMB_ReadStrobe(microblaze_0_dlmb_bus_READSTROBE),
        .LMB_Rst(SYS_Rst_1),
        .LMB_WriteDBus(microblaze_0_dlmb_bus_WRITEDBUS),
        .LMB_WriteStrobe(microblaze_0_dlmb_bus_WRITESTROBE),
        .Sl_CE(microblaze_0_dlmb_bus_CE),
        .Sl_DBus(microblaze_0_dlmb_bus_READDBUS),
        .Sl_Ready(microblaze_0_dlmb_bus_READY),
        .Sl_UE(microblaze_0_dlmb_bus_UE),
        .Sl_Wait(microblaze_0_dlmb_bus_WAIT));
  design_1_dlmb_v10_0 dlmb_v10
       (.LMB_ABus(microblaze_0_dlmb_bus_ABUS),
        .LMB_AddrStrobe(microblaze_0_dlmb_bus_ADDRSTROBE),
        .LMB_BE(microblaze_0_dlmb_bus_BE),
        .LMB_CE(microblaze_0_dlmb_CE),
        .LMB_Clk(microblaze_0_Clk),
        .LMB_ReadDBus(microblaze_0_dlmb_READDBUS),
        .LMB_ReadStrobe(microblaze_0_dlmb_bus_READSTROBE),
        .LMB_Ready(microblaze_0_dlmb_READY),
        .LMB_UE(microblaze_0_dlmb_UE),
        .LMB_Wait(microblaze_0_dlmb_WAIT),
        .LMB_WriteDBus(microblaze_0_dlmb_bus_WRITEDBUS),
        .LMB_WriteStrobe(microblaze_0_dlmb_bus_WRITESTROBE),
        .M_ABus(microblaze_0_dlmb_ABUS),
        .M_AddrStrobe(microblaze_0_dlmb_ADDRSTROBE),
        .M_BE(microblaze_0_dlmb_BE),
        .M_DBus(microblaze_0_dlmb_WRITEDBUS),
        .M_ReadStrobe(microblaze_0_dlmb_READSTROBE),
        .M_WriteStrobe(microblaze_0_dlmb_WRITESTROBE),
        .SYS_Rst(SYS_Rst_1),
        .Sl_CE(microblaze_0_dlmb_bus_CE),
        .Sl_DBus(microblaze_0_dlmb_bus_READDBUS),
        .Sl_Ready(microblaze_0_dlmb_bus_READY),
        .Sl_UE(microblaze_0_dlmb_bus_UE),
        .Sl_Wait(microblaze_0_dlmb_bus_WAIT));
  design_1_ilmb_bram_if_cntlr_0 ilmb_bram_if_cntlr
       (.BRAM_Addr_A(microblaze_0_ilmb_cntlr_ADDR),
        .BRAM_Clk_A(microblaze_0_ilmb_cntlr_CLK),
        .BRAM_Din_A({microblaze_0_ilmb_cntlr_DOUT[31],microblaze_0_ilmb_cntlr_DOUT[30],microblaze_0_ilmb_cntlr_DOUT[29],microblaze_0_ilmb_cntlr_DOUT[28],microblaze_0_ilmb_cntlr_DOUT[27],microblaze_0_ilmb_cntlr_DOUT[26],microblaze_0_ilmb_cntlr_DOUT[25],microblaze_0_ilmb_cntlr_DOUT[24],microblaze_0_ilmb_cntlr_DOUT[23],microblaze_0_ilmb_cntlr_DOUT[22],microblaze_0_ilmb_cntlr_DOUT[21],microblaze_0_ilmb_cntlr_DOUT[20],microblaze_0_ilmb_cntlr_DOUT[19],microblaze_0_ilmb_cntlr_DOUT[18],microblaze_0_ilmb_cntlr_DOUT[17],microblaze_0_ilmb_cntlr_DOUT[16],microblaze_0_ilmb_cntlr_DOUT[15],microblaze_0_ilmb_cntlr_DOUT[14],microblaze_0_ilmb_cntlr_DOUT[13],microblaze_0_ilmb_cntlr_DOUT[12],microblaze_0_ilmb_cntlr_DOUT[11],microblaze_0_ilmb_cntlr_DOUT[10],microblaze_0_ilmb_cntlr_DOUT[9],microblaze_0_ilmb_cntlr_DOUT[8],microblaze_0_ilmb_cntlr_DOUT[7],microblaze_0_ilmb_cntlr_DOUT[6],microblaze_0_ilmb_cntlr_DOUT[5],microblaze_0_ilmb_cntlr_DOUT[4],microblaze_0_ilmb_cntlr_DOUT[3],microblaze_0_ilmb_cntlr_DOUT[2],microblaze_0_ilmb_cntlr_DOUT[1],microblaze_0_ilmb_cntlr_DOUT[0]}),
        .BRAM_Dout_A(microblaze_0_ilmb_cntlr_DIN),
        .BRAM_EN_A(microblaze_0_ilmb_cntlr_EN),
        .BRAM_Rst_A(microblaze_0_ilmb_cntlr_RST),
        .BRAM_WEN_A(microblaze_0_ilmb_cntlr_WE),
        .LMB_ABus(microblaze_0_ilmb_bus_ABUS),
        .LMB_AddrStrobe(microblaze_0_ilmb_bus_ADDRSTROBE),
        .LMB_BE(microblaze_0_ilmb_bus_BE),
        .LMB_Clk(microblaze_0_Clk),
        .LMB_ReadStrobe(microblaze_0_ilmb_bus_READSTROBE),
        .LMB_Rst(SYS_Rst_1),
        .LMB_WriteDBus(microblaze_0_ilmb_bus_WRITEDBUS),
        .LMB_WriteStrobe(microblaze_0_ilmb_bus_WRITESTROBE),
        .Sl_CE(microblaze_0_ilmb_bus_CE),
        .Sl_DBus(microblaze_0_ilmb_bus_READDBUS),
        .Sl_Ready(microblaze_0_ilmb_bus_READY),
        .Sl_UE(microblaze_0_ilmb_bus_UE),
        .Sl_Wait(microblaze_0_ilmb_bus_WAIT));
  design_1_ilmb_v10_0 ilmb_v10
       (.LMB_ABus(microblaze_0_ilmb_bus_ABUS),
        .LMB_AddrStrobe(microblaze_0_ilmb_bus_ADDRSTROBE),
        .LMB_BE(microblaze_0_ilmb_bus_BE),
        .LMB_CE(microblaze_0_ilmb_CE),
        .LMB_Clk(microblaze_0_Clk),
        .LMB_ReadDBus(microblaze_0_ilmb_READDBUS),
        .LMB_ReadStrobe(microblaze_0_ilmb_bus_READSTROBE),
        .LMB_Ready(microblaze_0_ilmb_READY),
        .LMB_UE(microblaze_0_ilmb_UE),
        .LMB_Wait(microblaze_0_ilmb_WAIT),
        .LMB_WriteDBus(microblaze_0_ilmb_bus_WRITEDBUS),
        .LMB_WriteStrobe(microblaze_0_ilmb_bus_WRITESTROBE),
        .M_ABus(microblaze_0_ilmb_ABUS),
        .M_AddrStrobe(microblaze_0_ilmb_ADDRSTROBE),
        .M_BE({1'b0,1'b0,1'b0,1'b0}),
        .M_DBus({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .M_ReadStrobe(microblaze_0_ilmb_READSTROBE),
        .M_WriteStrobe(1'b0),
        .SYS_Rst(SYS_Rst_1),
        .Sl_CE(microblaze_0_ilmb_bus_CE),
        .Sl_DBus(microblaze_0_ilmb_bus_READDBUS),
        .Sl_Ready(microblaze_0_ilmb_bus_READY),
        .Sl_UE(microblaze_0_ilmb_bus_UE),
        .Sl_Wait(microblaze_0_ilmb_bus_WAIT));
  design_1_lmb_bram_0 lmb_bram
       (.addra({microblaze_0_dlmb_cntlr_ADDR[0],microblaze_0_dlmb_cntlr_ADDR[1],microblaze_0_dlmb_cntlr_ADDR[2],microblaze_0_dlmb_cntlr_ADDR[3],microblaze_0_dlmb_cntlr_ADDR[4],microblaze_0_dlmb_cntlr_ADDR[5],microblaze_0_dlmb_cntlr_ADDR[6],microblaze_0_dlmb_cntlr_ADDR[7],microblaze_0_dlmb_cntlr_ADDR[8],microblaze_0_dlmb_cntlr_ADDR[9],microblaze_0_dlmb_cntlr_ADDR[10],microblaze_0_dlmb_cntlr_ADDR[11],microblaze_0_dlmb_cntlr_ADDR[12],microblaze_0_dlmb_cntlr_ADDR[13],microblaze_0_dlmb_cntlr_ADDR[14],microblaze_0_dlmb_cntlr_ADDR[15],microblaze_0_dlmb_cntlr_ADDR[16],microblaze_0_dlmb_cntlr_ADDR[17],microblaze_0_dlmb_cntlr_ADDR[18],microblaze_0_dlmb_cntlr_ADDR[19],microblaze_0_dlmb_cntlr_ADDR[20],microblaze_0_dlmb_cntlr_ADDR[21],microblaze_0_dlmb_cntlr_ADDR[22],microblaze_0_dlmb_cntlr_ADDR[23],microblaze_0_dlmb_cntlr_ADDR[24],microblaze_0_dlmb_cntlr_ADDR[25],microblaze_0_dlmb_cntlr_ADDR[26],microblaze_0_dlmb_cntlr_ADDR[27],microblaze_0_dlmb_cntlr_ADDR[28],microblaze_0_dlmb_cntlr_ADDR[29],microblaze_0_dlmb_cntlr_ADDR[30],microblaze_0_dlmb_cntlr_ADDR[31]}),
        .addrb({microblaze_0_ilmb_cntlr_ADDR[0],microblaze_0_ilmb_cntlr_ADDR[1],microblaze_0_ilmb_cntlr_ADDR[2],microblaze_0_ilmb_cntlr_ADDR[3],microblaze_0_ilmb_cntlr_ADDR[4],microblaze_0_ilmb_cntlr_ADDR[5],microblaze_0_ilmb_cntlr_ADDR[6],microblaze_0_ilmb_cntlr_ADDR[7],microblaze_0_ilmb_cntlr_ADDR[8],microblaze_0_ilmb_cntlr_ADDR[9],microblaze_0_ilmb_cntlr_ADDR[10],microblaze_0_ilmb_cntlr_ADDR[11],microblaze_0_ilmb_cntlr_ADDR[12],microblaze_0_ilmb_cntlr_ADDR[13],microblaze_0_ilmb_cntlr_ADDR[14],microblaze_0_ilmb_cntlr_ADDR[15],microblaze_0_ilmb_cntlr_ADDR[16],microblaze_0_ilmb_cntlr_ADDR[17],microblaze_0_ilmb_cntlr_ADDR[18],microblaze_0_ilmb_cntlr_ADDR[19],microblaze_0_ilmb_cntlr_ADDR[20],microblaze_0_ilmb_cntlr_ADDR[21],microblaze_0_ilmb_cntlr_ADDR[22],microblaze_0_ilmb_cntlr_ADDR[23],microblaze_0_ilmb_cntlr_ADDR[24],microblaze_0_ilmb_cntlr_ADDR[25],microblaze_0_ilmb_cntlr_ADDR[26],microblaze_0_ilmb_cntlr_ADDR[27],microblaze_0_ilmb_cntlr_ADDR[28],microblaze_0_ilmb_cntlr_ADDR[29],microblaze_0_ilmb_cntlr_ADDR[30],microblaze_0_ilmb_cntlr_ADDR[31]}),
        .clka(microblaze_0_dlmb_cntlr_CLK),
        .clkb(microblaze_0_ilmb_cntlr_CLK),
        .dina({microblaze_0_dlmb_cntlr_DIN[0],microblaze_0_dlmb_cntlr_DIN[1],microblaze_0_dlmb_cntlr_DIN[2],microblaze_0_dlmb_cntlr_DIN[3],microblaze_0_dlmb_cntlr_DIN[4],microblaze_0_dlmb_cntlr_DIN[5],microblaze_0_dlmb_cntlr_DIN[6],microblaze_0_dlmb_cntlr_DIN[7],microblaze_0_dlmb_cntlr_DIN[8],microblaze_0_dlmb_cntlr_DIN[9],microblaze_0_dlmb_cntlr_DIN[10],microblaze_0_dlmb_cntlr_DIN[11],microblaze_0_dlmb_cntlr_DIN[12],microblaze_0_dlmb_cntlr_DIN[13],microblaze_0_dlmb_cntlr_DIN[14],microblaze_0_dlmb_cntlr_DIN[15],microblaze_0_dlmb_cntlr_DIN[16],microblaze_0_dlmb_cntlr_DIN[17],microblaze_0_dlmb_cntlr_DIN[18],microblaze_0_dlmb_cntlr_DIN[19],microblaze_0_dlmb_cntlr_DIN[20],microblaze_0_dlmb_cntlr_DIN[21],microblaze_0_dlmb_cntlr_DIN[22],microblaze_0_dlmb_cntlr_DIN[23],microblaze_0_dlmb_cntlr_DIN[24],microblaze_0_dlmb_cntlr_DIN[25],microblaze_0_dlmb_cntlr_DIN[26],microblaze_0_dlmb_cntlr_DIN[27],microblaze_0_dlmb_cntlr_DIN[28],microblaze_0_dlmb_cntlr_DIN[29],microblaze_0_dlmb_cntlr_DIN[30],microblaze_0_dlmb_cntlr_DIN[31]}),
        .dinb({microblaze_0_ilmb_cntlr_DIN[0],microblaze_0_ilmb_cntlr_DIN[1],microblaze_0_ilmb_cntlr_DIN[2],microblaze_0_ilmb_cntlr_DIN[3],microblaze_0_ilmb_cntlr_DIN[4],microblaze_0_ilmb_cntlr_DIN[5],microblaze_0_ilmb_cntlr_DIN[6],microblaze_0_ilmb_cntlr_DIN[7],microblaze_0_ilmb_cntlr_DIN[8],microblaze_0_ilmb_cntlr_DIN[9],microblaze_0_ilmb_cntlr_DIN[10],microblaze_0_ilmb_cntlr_DIN[11],microblaze_0_ilmb_cntlr_DIN[12],microblaze_0_ilmb_cntlr_DIN[13],microblaze_0_ilmb_cntlr_DIN[14],microblaze_0_ilmb_cntlr_DIN[15],microblaze_0_ilmb_cntlr_DIN[16],microblaze_0_ilmb_cntlr_DIN[17],microblaze_0_ilmb_cntlr_DIN[18],microblaze_0_ilmb_cntlr_DIN[19],microblaze_0_ilmb_cntlr_DIN[20],microblaze_0_ilmb_cntlr_DIN[21],microblaze_0_ilmb_cntlr_DIN[22],microblaze_0_ilmb_cntlr_DIN[23],microblaze_0_ilmb_cntlr_DIN[24],microblaze_0_ilmb_cntlr_DIN[25],microblaze_0_ilmb_cntlr_DIN[26],microblaze_0_ilmb_cntlr_DIN[27],microblaze_0_ilmb_cntlr_DIN[28],microblaze_0_ilmb_cntlr_DIN[29],microblaze_0_ilmb_cntlr_DIN[30],microblaze_0_ilmb_cntlr_DIN[31]}),
        .douta(microblaze_0_dlmb_cntlr_DOUT),
        .doutb(microblaze_0_ilmb_cntlr_DOUT),
        .ena(microblaze_0_dlmb_cntlr_EN),
        .enb(microblaze_0_ilmb_cntlr_EN),
        .rsta(microblaze_0_dlmb_cntlr_RST),
        .rstb(microblaze_0_ilmb_cntlr_RST),
        .wea({microblaze_0_dlmb_cntlr_WE[0],microblaze_0_dlmb_cntlr_WE[1],microblaze_0_dlmb_cntlr_WE[2],microblaze_0_dlmb_cntlr_WE[3]}),
        .web({microblaze_0_ilmb_cntlr_WE[0],microblaze_0_ilmb_cntlr_WE[1],microblaze_0_ilmb_cntlr_WE[2],microblaze_0_ilmb_cntlr_WE[3]}));
endmodule

module s00_couplers_imp_1LLE45P
   (M_AXIS_ACLK,
    M_AXIS_ARESETN,
    M_AXIS_tdata,
    M_AXIS_tdest,
    M_AXIS_tid,
    M_AXIS_tkeep,
    M_AXIS_tlast,
    M_AXIS_tready,
    M_AXIS_tuser,
    M_AXIS_tvalid,
    S_AXIS_ACLK,
    S_AXIS_ARESETN,
    S_AXIS_tdata,
    S_AXIS_tdest,
    S_AXIS_tid,
    S_AXIS_tkeep,
    S_AXIS_tlast,
    S_AXIS_tready,
    S_AXIS_tuser,
    S_AXIS_tvalid);
  input M_AXIS_ACLK;
  input [0:0]M_AXIS_ARESETN;
  output [31:0]M_AXIS_tdata;
  output [4:0]M_AXIS_tdest;
  output [4:0]M_AXIS_tid;
  output [3:0]M_AXIS_tkeep;
  output [0:0]M_AXIS_tlast;
  input [0:0]M_AXIS_tready;
  output [3:0]M_AXIS_tuser;
  output [0:0]M_AXIS_tvalid;
  input S_AXIS_ACLK;
  input [0:0]S_AXIS_ARESETN;
  input [31:0]S_AXIS_tdata;
  input [4:0]S_AXIS_tdest;
  input [4:0]S_AXIS_tid;
  input [3:0]S_AXIS_tkeep;
  input [0:0]S_AXIS_tlast;
  output [0:0]S_AXIS_tready;
  input [3:0]S_AXIS_tuser;
  input [0:0]S_AXIS_tvalid;

  wire [31:0]s00_couplers_to_s00_couplers_TDATA;
  wire [4:0]s00_couplers_to_s00_couplers_TDEST;
  wire [4:0]s00_couplers_to_s00_couplers_TID;
  wire [3:0]s00_couplers_to_s00_couplers_TKEEP;
  wire [0:0]s00_couplers_to_s00_couplers_TLAST;
  wire [0:0]s00_couplers_to_s00_couplers_TREADY;
  wire [3:0]s00_couplers_to_s00_couplers_TUSER;
  wire [0:0]s00_couplers_to_s00_couplers_TVALID;

  assign M_AXIS_tdata[31:0] = s00_couplers_to_s00_couplers_TDATA;
  assign M_AXIS_tdest[4:0] = s00_couplers_to_s00_couplers_TDEST;
  assign M_AXIS_tid[4:0] = s00_couplers_to_s00_couplers_TID;
  assign M_AXIS_tkeep[3:0] = s00_couplers_to_s00_couplers_TKEEP;
  assign M_AXIS_tlast[0] = s00_couplers_to_s00_couplers_TLAST;
  assign M_AXIS_tuser[3:0] = s00_couplers_to_s00_couplers_TUSER;
  assign M_AXIS_tvalid[0] = s00_couplers_to_s00_couplers_TVALID;
  assign S_AXIS_tready[0] = s00_couplers_to_s00_couplers_TREADY;
  assign s00_couplers_to_s00_couplers_TDATA = S_AXIS_tdata[31:0];
  assign s00_couplers_to_s00_couplers_TDEST = S_AXIS_tdest[4:0];
  assign s00_couplers_to_s00_couplers_TID = S_AXIS_tid[4:0];
  assign s00_couplers_to_s00_couplers_TKEEP = S_AXIS_tkeep[3:0];
  assign s00_couplers_to_s00_couplers_TLAST = S_AXIS_tlast[0];
  assign s00_couplers_to_s00_couplers_TREADY = M_AXIS_tready[0];
  assign s00_couplers_to_s00_couplers_TUSER = S_AXIS_tuser[3:0];
  assign s00_couplers_to_s00_couplers_TVALID = S_AXIS_tvalid[0];
endmodule

module s00_couplers_imp_1O4UG5P
   (M_AXIS_ACLK,
    M_AXIS_ARESETN,
    M_AXIS_tdata,
    M_AXIS_tdest,
    M_AXIS_tid,
    M_AXIS_tkeep,
    M_AXIS_tlast,
    M_AXIS_tready,
    M_AXIS_tuser,
    M_AXIS_tvalid,
    S_AXIS_ACLK,
    S_AXIS_ARESETN,
    S_AXIS_tdata,
    S_AXIS_tdest,
    S_AXIS_tid,
    S_AXIS_tkeep,
    S_AXIS_tlast,
    S_AXIS_tready,
    S_AXIS_tuser,
    S_AXIS_tvalid);
  input M_AXIS_ACLK;
  input [0:0]M_AXIS_ARESETN;
  output [31:0]M_AXIS_tdata;
  output [4:0]M_AXIS_tdest;
  output [4:0]M_AXIS_tid;
  output [3:0]M_AXIS_tkeep;
  output [0:0]M_AXIS_tlast;
  input [0:0]M_AXIS_tready;
  output [3:0]M_AXIS_tuser;
  output [0:0]M_AXIS_tvalid;
  input S_AXIS_ACLK;
  input [0:0]S_AXIS_ARESETN;
  input [31:0]S_AXIS_tdata;
  input [4:0]S_AXIS_tdest;
  input [4:0]S_AXIS_tid;
  input [3:0]S_AXIS_tkeep;
  input [0:0]S_AXIS_tlast;
  output [0:0]S_AXIS_tready;
  input [3:0]S_AXIS_tuser;
  input [0:0]S_AXIS_tvalid;

  wire [31:0]s00_couplers_to_s00_couplers_TDATA;
  wire [4:0]s00_couplers_to_s00_couplers_TDEST;
  wire [4:0]s00_couplers_to_s00_couplers_TID;
  wire [3:0]s00_couplers_to_s00_couplers_TKEEP;
  wire [0:0]s00_couplers_to_s00_couplers_TLAST;
  wire [0:0]s00_couplers_to_s00_couplers_TREADY;
  wire [3:0]s00_couplers_to_s00_couplers_TUSER;
  wire [0:0]s00_couplers_to_s00_couplers_TVALID;

  assign M_AXIS_tdata[31:0] = s00_couplers_to_s00_couplers_TDATA;
  assign M_AXIS_tdest[4:0] = s00_couplers_to_s00_couplers_TDEST;
  assign M_AXIS_tid[4:0] = s00_couplers_to_s00_couplers_TID;
  assign M_AXIS_tkeep[3:0] = s00_couplers_to_s00_couplers_TKEEP;
  assign M_AXIS_tlast[0] = s00_couplers_to_s00_couplers_TLAST;
  assign M_AXIS_tuser[3:0] = s00_couplers_to_s00_couplers_TUSER;
  assign M_AXIS_tvalid[0] = s00_couplers_to_s00_couplers_TVALID;
  assign S_AXIS_tready[0] = s00_couplers_to_s00_couplers_TREADY;
  assign s00_couplers_to_s00_couplers_TDATA = S_AXIS_tdata[31:0];
  assign s00_couplers_to_s00_couplers_TDEST = S_AXIS_tdest[4:0];
  assign s00_couplers_to_s00_couplers_TID = S_AXIS_tid[4:0];
  assign s00_couplers_to_s00_couplers_TKEEP = S_AXIS_tkeep[3:0];
  assign s00_couplers_to_s00_couplers_TLAST = S_AXIS_tlast[0];
  assign s00_couplers_to_s00_couplers_TREADY = M_AXIS_tready[0];
  assign s00_couplers_to_s00_couplers_TUSER = S_AXIS_tuser[3:0];
  assign s00_couplers_to_s00_couplers_TVALID = S_AXIS_tvalid[0];
endmodule

module s00_couplers_imp_1QN17NX
   (M_AXIS_ACLK,
    M_AXIS_ARESETN,
    M_AXIS_tdata,
    M_AXIS_tdest,
    M_AXIS_tid,
    M_AXIS_tkeep,
    M_AXIS_tlast,
    M_AXIS_tready,
    M_AXIS_tuser,
    M_AXIS_tvalid,
    S_AXIS_ACLK,
    S_AXIS_ARESETN,
    S_AXIS_tdata,
    S_AXIS_tdest,
    S_AXIS_tid,
    S_AXIS_tkeep,
    S_AXIS_tlast,
    S_AXIS_tready,
    S_AXIS_tuser,
    S_AXIS_tvalid);
  input M_AXIS_ACLK;
  input [0:0]M_AXIS_ARESETN;
  output [31:0]M_AXIS_tdata;
  output [4:0]M_AXIS_tdest;
  output [4:0]M_AXIS_tid;
  output [3:0]M_AXIS_tkeep;
  output [0:0]M_AXIS_tlast;
  input [0:0]M_AXIS_tready;
  output [3:0]M_AXIS_tuser;
  output [0:0]M_AXIS_tvalid;
  input S_AXIS_ACLK;
  input [0:0]S_AXIS_ARESETN;
  input [31:0]S_AXIS_tdata;
  input [4:0]S_AXIS_tdest;
  input [4:0]S_AXIS_tid;
  input [3:0]S_AXIS_tkeep;
  input [0:0]S_AXIS_tlast;
  output [0:0]S_AXIS_tready;
  input [3:0]S_AXIS_tuser;
  input [0:0]S_AXIS_tvalid;

  wire [31:0]s00_couplers_to_s00_couplers_TDATA;
  wire [4:0]s00_couplers_to_s00_couplers_TDEST;
  wire [4:0]s00_couplers_to_s00_couplers_TID;
  wire [3:0]s00_couplers_to_s00_couplers_TKEEP;
  wire [0:0]s00_couplers_to_s00_couplers_TLAST;
  wire [0:0]s00_couplers_to_s00_couplers_TREADY;
  wire [3:0]s00_couplers_to_s00_couplers_TUSER;
  wire [0:0]s00_couplers_to_s00_couplers_TVALID;

  assign M_AXIS_tdata[31:0] = s00_couplers_to_s00_couplers_TDATA;
  assign M_AXIS_tdest[4:0] = s00_couplers_to_s00_couplers_TDEST;
  assign M_AXIS_tid[4:0] = s00_couplers_to_s00_couplers_TID;
  assign M_AXIS_tkeep[3:0] = s00_couplers_to_s00_couplers_TKEEP;
  assign M_AXIS_tlast[0] = s00_couplers_to_s00_couplers_TLAST;
  assign M_AXIS_tuser[3:0] = s00_couplers_to_s00_couplers_TUSER;
  assign M_AXIS_tvalid[0] = s00_couplers_to_s00_couplers_TVALID;
  assign S_AXIS_tready[0] = s00_couplers_to_s00_couplers_TREADY;
  assign s00_couplers_to_s00_couplers_TDATA = S_AXIS_tdata[31:0];
  assign s00_couplers_to_s00_couplers_TDEST = S_AXIS_tdest[4:0];
  assign s00_couplers_to_s00_couplers_TID = S_AXIS_tid[4:0];
  assign s00_couplers_to_s00_couplers_TKEEP = S_AXIS_tkeep[3:0];
  assign s00_couplers_to_s00_couplers_TLAST = S_AXIS_tlast[0];
  assign s00_couplers_to_s00_couplers_TREADY = M_AXIS_tready[0];
  assign s00_couplers_to_s00_couplers_TUSER = S_AXIS_tuser[3:0];
  assign s00_couplers_to_s00_couplers_TVALID = S_AXIS_tvalid[0];
endmodule

module s00_couplers_imp_1RZP34U
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arprot,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awprot,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [2:0]M_AXI_arprot;
  input M_AXI_arready;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [2:0]M_AXI_awprot;
  input M_AXI_awready;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [2:0]S_AXI_arprot;
  output S_AXI_arready;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [2:0]S_AXI_awprot;
  output S_AXI_awready;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire M_ACLK_1;
  wire [0:0]M_ARESETN_1;
  wire [31:0]s00_couplers_to_s00_data_fifo_ARADDR;
  wire [2:0]s00_couplers_to_s00_data_fifo_ARPROT;
  wire s00_couplers_to_s00_data_fifo_ARREADY;
  wire s00_couplers_to_s00_data_fifo_ARVALID;
  wire [31:0]s00_couplers_to_s00_data_fifo_AWADDR;
  wire [2:0]s00_couplers_to_s00_data_fifo_AWPROT;
  wire s00_couplers_to_s00_data_fifo_AWREADY;
  wire s00_couplers_to_s00_data_fifo_AWVALID;
  wire s00_couplers_to_s00_data_fifo_BREADY;
  wire [1:0]s00_couplers_to_s00_data_fifo_BRESP;
  wire s00_couplers_to_s00_data_fifo_BVALID;
  wire [31:0]s00_couplers_to_s00_data_fifo_RDATA;
  wire s00_couplers_to_s00_data_fifo_RREADY;
  wire [1:0]s00_couplers_to_s00_data_fifo_RRESP;
  wire s00_couplers_to_s00_data_fifo_RVALID;
  wire [31:0]s00_couplers_to_s00_data_fifo_WDATA;
  wire s00_couplers_to_s00_data_fifo_WREADY;
  wire [3:0]s00_couplers_to_s00_data_fifo_WSTRB;
  wire s00_couplers_to_s00_data_fifo_WVALID;
  wire [31:0]s00_data_fifo_to_s00_couplers_ARADDR;
  wire [2:0]s00_data_fifo_to_s00_couplers_ARPROT;
  wire s00_data_fifo_to_s00_couplers_ARREADY;
  wire s00_data_fifo_to_s00_couplers_ARVALID;
  wire [31:0]s00_data_fifo_to_s00_couplers_AWADDR;
  wire [2:0]s00_data_fifo_to_s00_couplers_AWPROT;
  wire s00_data_fifo_to_s00_couplers_AWREADY;
  wire s00_data_fifo_to_s00_couplers_AWVALID;
  wire s00_data_fifo_to_s00_couplers_BREADY;
  wire [1:0]s00_data_fifo_to_s00_couplers_BRESP;
  wire s00_data_fifo_to_s00_couplers_BVALID;
  wire [31:0]s00_data_fifo_to_s00_couplers_RDATA;
  wire s00_data_fifo_to_s00_couplers_RREADY;
  wire [1:0]s00_data_fifo_to_s00_couplers_RRESP;
  wire s00_data_fifo_to_s00_couplers_RVALID;
  wire [31:0]s00_data_fifo_to_s00_couplers_WDATA;
  wire s00_data_fifo_to_s00_couplers_WREADY;
  wire [3:0]s00_data_fifo_to_s00_couplers_WSTRB;
  wire s00_data_fifo_to_s00_couplers_WVALID;

  assign M_ACLK_1 = M_ACLK;
  assign M_ARESETN_1 = M_ARESETN[0];
  assign M_AXI_araddr[31:0] = s00_data_fifo_to_s00_couplers_ARADDR;
  assign M_AXI_arprot[2:0] = s00_data_fifo_to_s00_couplers_ARPROT;
  assign M_AXI_arvalid = s00_data_fifo_to_s00_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = s00_data_fifo_to_s00_couplers_AWADDR;
  assign M_AXI_awprot[2:0] = s00_data_fifo_to_s00_couplers_AWPROT;
  assign M_AXI_awvalid = s00_data_fifo_to_s00_couplers_AWVALID;
  assign M_AXI_bready = s00_data_fifo_to_s00_couplers_BREADY;
  assign M_AXI_rready = s00_data_fifo_to_s00_couplers_RREADY;
  assign M_AXI_wdata[31:0] = s00_data_fifo_to_s00_couplers_WDATA;
  assign M_AXI_wstrb[3:0] = s00_data_fifo_to_s00_couplers_WSTRB;
  assign M_AXI_wvalid = s00_data_fifo_to_s00_couplers_WVALID;
  assign S_AXI_arready = s00_couplers_to_s00_data_fifo_ARREADY;
  assign S_AXI_awready = s00_couplers_to_s00_data_fifo_AWREADY;
  assign S_AXI_bresp[1:0] = s00_couplers_to_s00_data_fifo_BRESP;
  assign S_AXI_bvalid = s00_couplers_to_s00_data_fifo_BVALID;
  assign S_AXI_rdata[31:0] = s00_couplers_to_s00_data_fifo_RDATA;
  assign S_AXI_rresp[1:0] = s00_couplers_to_s00_data_fifo_RRESP;
  assign S_AXI_rvalid = s00_couplers_to_s00_data_fifo_RVALID;
  assign S_AXI_wready = s00_couplers_to_s00_data_fifo_WREADY;
  assign s00_couplers_to_s00_data_fifo_ARADDR = S_AXI_araddr[31:0];
  assign s00_couplers_to_s00_data_fifo_ARPROT = S_AXI_arprot[2:0];
  assign s00_couplers_to_s00_data_fifo_ARVALID = S_AXI_arvalid;
  assign s00_couplers_to_s00_data_fifo_AWADDR = S_AXI_awaddr[31:0];
  assign s00_couplers_to_s00_data_fifo_AWPROT = S_AXI_awprot[2:0];
  assign s00_couplers_to_s00_data_fifo_AWVALID = S_AXI_awvalid;
  assign s00_couplers_to_s00_data_fifo_BREADY = S_AXI_bready;
  assign s00_couplers_to_s00_data_fifo_RREADY = S_AXI_rready;
  assign s00_couplers_to_s00_data_fifo_WDATA = S_AXI_wdata[31:0];
  assign s00_couplers_to_s00_data_fifo_WSTRB = S_AXI_wstrb[3:0];
  assign s00_couplers_to_s00_data_fifo_WVALID = S_AXI_wvalid;
  assign s00_data_fifo_to_s00_couplers_ARREADY = M_AXI_arready;
  assign s00_data_fifo_to_s00_couplers_AWREADY = M_AXI_awready;
  assign s00_data_fifo_to_s00_couplers_BRESP = M_AXI_bresp[1:0];
  assign s00_data_fifo_to_s00_couplers_BVALID = M_AXI_bvalid;
  assign s00_data_fifo_to_s00_couplers_RDATA = M_AXI_rdata[31:0];
  assign s00_data_fifo_to_s00_couplers_RRESP = M_AXI_rresp[1:0];
  assign s00_data_fifo_to_s00_couplers_RVALID = M_AXI_rvalid;
  assign s00_data_fifo_to_s00_couplers_WREADY = M_AXI_wready;
  design_1_s00_data_fifo_1 s00_data_fifo
       (.aclk(M_ACLK_1),
        .aresetn(M_ARESETN_1),
        .m_axi_araddr(s00_data_fifo_to_s00_couplers_ARADDR),
        .m_axi_arprot(s00_data_fifo_to_s00_couplers_ARPROT),
        .m_axi_arready(s00_data_fifo_to_s00_couplers_ARREADY),
        .m_axi_arvalid(s00_data_fifo_to_s00_couplers_ARVALID),
        .m_axi_awaddr(s00_data_fifo_to_s00_couplers_AWADDR),
        .m_axi_awprot(s00_data_fifo_to_s00_couplers_AWPROT),
        .m_axi_awready(s00_data_fifo_to_s00_couplers_AWREADY),
        .m_axi_awvalid(s00_data_fifo_to_s00_couplers_AWVALID),
        .m_axi_bready(s00_data_fifo_to_s00_couplers_BREADY),
        .m_axi_bresp(s00_data_fifo_to_s00_couplers_BRESP),
        .m_axi_bvalid(s00_data_fifo_to_s00_couplers_BVALID),
        .m_axi_rdata(s00_data_fifo_to_s00_couplers_RDATA),
        .m_axi_rready(s00_data_fifo_to_s00_couplers_RREADY),
        .m_axi_rresp(s00_data_fifo_to_s00_couplers_RRESP),
        .m_axi_rvalid(s00_data_fifo_to_s00_couplers_RVALID),
        .m_axi_wdata(s00_data_fifo_to_s00_couplers_WDATA),
        .m_axi_wready(s00_data_fifo_to_s00_couplers_WREADY),
        .m_axi_wstrb(s00_data_fifo_to_s00_couplers_WSTRB),
        .m_axi_wvalid(s00_data_fifo_to_s00_couplers_WVALID),
        .s_axi_araddr(s00_couplers_to_s00_data_fifo_ARADDR),
        .s_axi_arprot(s00_couplers_to_s00_data_fifo_ARPROT),
        .s_axi_arready(s00_couplers_to_s00_data_fifo_ARREADY),
        .s_axi_arvalid(s00_couplers_to_s00_data_fifo_ARVALID),
        .s_axi_awaddr(s00_couplers_to_s00_data_fifo_AWADDR),
        .s_axi_awprot(s00_couplers_to_s00_data_fifo_AWPROT),
        .s_axi_awready(s00_couplers_to_s00_data_fifo_AWREADY),
        .s_axi_awvalid(s00_couplers_to_s00_data_fifo_AWVALID),
        .s_axi_bready(s00_couplers_to_s00_data_fifo_BREADY),
        .s_axi_bresp(s00_couplers_to_s00_data_fifo_BRESP),
        .s_axi_bvalid(s00_couplers_to_s00_data_fifo_BVALID),
        .s_axi_rdata(s00_couplers_to_s00_data_fifo_RDATA),
        .s_axi_rready(s00_couplers_to_s00_data_fifo_RREADY),
        .s_axi_rresp(s00_couplers_to_s00_data_fifo_RRESP),
        .s_axi_rvalid(s00_couplers_to_s00_data_fifo_RVALID),
        .s_axi_wdata(s00_couplers_to_s00_data_fifo_WDATA),
        .s_axi_wready(s00_couplers_to_s00_data_fifo_WREADY),
        .s_axi_wstrb(s00_couplers_to_s00_data_fifo_WSTRB),
        .s_axi_wvalid(s00_couplers_to_s00_data_fifo_WVALID));
endmodule

module s00_couplers_imp_1WIDDQL
   (M_AXIS_ACLK,
    M_AXIS_ARESETN,
    M_AXIS_tdata,
    M_AXIS_tdest,
    M_AXIS_tid,
    M_AXIS_tkeep,
    M_AXIS_tlast,
    M_AXIS_tready,
    M_AXIS_tuser,
    M_AXIS_tvalid,
    S_AXIS_ACLK,
    S_AXIS_ARESETN,
    S_AXIS_tdata,
    S_AXIS_tdest,
    S_AXIS_tid,
    S_AXIS_tkeep,
    S_AXIS_tlast,
    S_AXIS_tready,
    S_AXIS_tuser,
    S_AXIS_tvalid);
  input M_AXIS_ACLK;
  input [0:0]M_AXIS_ARESETN;
  output [31:0]M_AXIS_tdata;
  output [4:0]M_AXIS_tdest;
  output [4:0]M_AXIS_tid;
  output [3:0]M_AXIS_tkeep;
  output [0:0]M_AXIS_tlast;
  input [0:0]M_AXIS_tready;
  output [3:0]M_AXIS_tuser;
  output [0:0]M_AXIS_tvalid;
  input S_AXIS_ACLK;
  input [0:0]S_AXIS_ARESETN;
  input [31:0]S_AXIS_tdata;
  input [4:0]S_AXIS_tdest;
  input [4:0]S_AXIS_tid;
  input [3:0]S_AXIS_tkeep;
  input [0:0]S_AXIS_tlast;
  output [0:0]S_AXIS_tready;
  input [3:0]S_AXIS_tuser;
  input [0:0]S_AXIS_tvalid;

  wire [31:0]s00_couplers_to_s00_couplers_TDATA;
  wire [4:0]s00_couplers_to_s00_couplers_TDEST;
  wire [4:0]s00_couplers_to_s00_couplers_TID;
  wire [3:0]s00_couplers_to_s00_couplers_TKEEP;
  wire [0:0]s00_couplers_to_s00_couplers_TLAST;
  wire [0:0]s00_couplers_to_s00_couplers_TREADY;
  wire [3:0]s00_couplers_to_s00_couplers_TUSER;
  wire [0:0]s00_couplers_to_s00_couplers_TVALID;

  assign M_AXIS_tdata[31:0] = s00_couplers_to_s00_couplers_TDATA;
  assign M_AXIS_tdest[4:0] = s00_couplers_to_s00_couplers_TDEST;
  assign M_AXIS_tid[4:0] = s00_couplers_to_s00_couplers_TID;
  assign M_AXIS_tkeep[3:0] = s00_couplers_to_s00_couplers_TKEEP;
  assign M_AXIS_tlast[0] = s00_couplers_to_s00_couplers_TLAST;
  assign M_AXIS_tuser[3:0] = s00_couplers_to_s00_couplers_TUSER;
  assign M_AXIS_tvalid[0] = s00_couplers_to_s00_couplers_TVALID;
  assign S_AXIS_tready[0] = s00_couplers_to_s00_couplers_TREADY;
  assign s00_couplers_to_s00_couplers_TDATA = S_AXIS_tdata[31:0];
  assign s00_couplers_to_s00_couplers_TDEST = S_AXIS_tdest[4:0];
  assign s00_couplers_to_s00_couplers_TID = S_AXIS_tid[4:0];
  assign s00_couplers_to_s00_couplers_TKEEP = S_AXIS_tkeep[3:0];
  assign s00_couplers_to_s00_couplers_TLAST = S_AXIS_tlast[0];
  assign s00_couplers_to_s00_couplers_TREADY = M_AXIS_tready[0];
  assign s00_couplers_to_s00_couplers_TUSER = S_AXIS_tuser[3:0];
  assign s00_couplers_to_s00_couplers_TVALID = S_AXIS_tvalid[0];
endmodule

module s00_couplers_imp_7HNO1D
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arburst,
    M_AXI_arcache,
    M_AXI_arlen,
    M_AXI_arlock,
    M_AXI_arprot,
    M_AXI_arqos,
    M_AXI_arready,
    M_AXI_arsize,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awburst,
    M_AXI_awcache,
    M_AXI_awlen,
    M_AXI_awlock,
    M_AXI_awprot,
    M_AXI_awqos,
    M_AXI_awready,
    M_AXI_awsize,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rlast,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wlast,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arburst,
    S_AXI_arcache,
    S_AXI_arlen,
    S_AXI_arlock,
    S_AXI_arprot,
    S_AXI_arqos,
    S_AXI_arready,
    S_AXI_arsize,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awburst,
    S_AXI_awcache,
    S_AXI_awlen,
    S_AXI_awlock,
    S_AXI_awprot,
    S_AXI_awqos,
    S_AXI_awready,
    S_AXI_awsize,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rlast,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wlast,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [1:0]M_AXI_arburst;
  output [3:0]M_AXI_arcache;
  output [7:0]M_AXI_arlen;
  output [0:0]M_AXI_arlock;
  output [2:0]M_AXI_arprot;
  output [3:0]M_AXI_arqos;
  input M_AXI_arready;
  output [2:0]M_AXI_arsize;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [1:0]M_AXI_awburst;
  output [3:0]M_AXI_awcache;
  output [7:0]M_AXI_awlen;
  output [0:0]M_AXI_awlock;
  output [2:0]M_AXI_awprot;
  output [3:0]M_AXI_awqos;
  input M_AXI_awready;
  output [2:0]M_AXI_awsize;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [511:0]M_AXI_rdata;
  input M_AXI_rlast;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [511:0]M_AXI_wdata;
  output M_AXI_wlast;
  input M_AXI_wready;
  output [63:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [1:0]S_AXI_arburst;
  input [3:0]S_AXI_arcache;
  input [7:0]S_AXI_arlen;
  input [0:0]S_AXI_arlock;
  input [2:0]S_AXI_arprot;
  input [3:0]S_AXI_arqos;
  output S_AXI_arready;
  input [2:0]S_AXI_arsize;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [1:0]S_AXI_awburst;
  input [3:0]S_AXI_awcache;
  input [7:0]S_AXI_awlen;
  input [0:0]S_AXI_awlock;
  input [2:0]S_AXI_awprot;
  input [3:0]S_AXI_awqos;
  output S_AXI_awready;
  input [2:0]S_AXI_awsize;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  output S_AXI_rlast;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  input S_AXI_wlast;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire M_ACLK_1;
  wire [0:0]M_ARESETN_1;
  wire S_ACLK_1;
  wire [0:0]S_ARESETN_1;
  wire [31:0]auto_us_df_to_s00_couplers_ARADDR;
  wire [1:0]auto_us_df_to_s00_couplers_ARBURST;
  wire [3:0]auto_us_df_to_s00_couplers_ARCACHE;
  wire [7:0]auto_us_df_to_s00_couplers_ARLEN;
  wire [0:0]auto_us_df_to_s00_couplers_ARLOCK;
  wire [2:0]auto_us_df_to_s00_couplers_ARPROT;
  wire [3:0]auto_us_df_to_s00_couplers_ARQOS;
  wire auto_us_df_to_s00_couplers_ARREADY;
  wire [2:0]auto_us_df_to_s00_couplers_ARSIZE;
  wire auto_us_df_to_s00_couplers_ARVALID;
  wire [31:0]auto_us_df_to_s00_couplers_AWADDR;
  wire [1:0]auto_us_df_to_s00_couplers_AWBURST;
  wire [3:0]auto_us_df_to_s00_couplers_AWCACHE;
  wire [7:0]auto_us_df_to_s00_couplers_AWLEN;
  wire [0:0]auto_us_df_to_s00_couplers_AWLOCK;
  wire [2:0]auto_us_df_to_s00_couplers_AWPROT;
  wire [3:0]auto_us_df_to_s00_couplers_AWQOS;
  wire auto_us_df_to_s00_couplers_AWREADY;
  wire [2:0]auto_us_df_to_s00_couplers_AWSIZE;
  wire auto_us_df_to_s00_couplers_AWVALID;
  wire auto_us_df_to_s00_couplers_BREADY;
  wire [1:0]auto_us_df_to_s00_couplers_BRESP;
  wire auto_us_df_to_s00_couplers_BVALID;
  wire [511:0]auto_us_df_to_s00_couplers_RDATA;
  wire auto_us_df_to_s00_couplers_RLAST;
  wire auto_us_df_to_s00_couplers_RREADY;
  wire [1:0]auto_us_df_to_s00_couplers_RRESP;
  wire auto_us_df_to_s00_couplers_RVALID;
  wire [511:0]auto_us_df_to_s00_couplers_WDATA;
  wire auto_us_df_to_s00_couplers_WLAST;
  wire auto_us_df_to_s00_couplers_WREADY;
  wire [63:0]auto_us_df_to_s00_couplers_WSTRB;
  wire auto_us_df_to_s00_couplers_WVALID;
  wire [31:0]s00_couplers_to_auto_us_df_ARADDR;
  wire [1:0]s00_couplers_to_auto_us_df_ARBURST;
  wire [3:0]s00_couplers_to_auto_us_df_ARCACHE;
  wire [7:0]s00_couplers_to_auto_us_df_ARLEN;
  wire [0:0]s00_couplers_to_auto_us_df_ARLOCK;
  wire [2:0]s00_couplers_to_auto_us_df_ARPROT;
  wire [3:0]s00_couplers_to_auto_us_df_ARQOS;
  wire s00_couplers_to_auto_us_df_ARREADY;
  wire [2:0]s00_couplers_to_auto_us_df_ARSIZE;
  wire s00_couplers_to_auto_us_df_ARVALID;
  wire [31:0]s00_couplers_to_auto_us_df_AWADDR;
  wire [1:0]s00_couplers_to_auto_us_df_AWBURST;
  wire [3:0]s00_couplers_to_auto_us_df_AWCACHE;
  wire [7:0]s00_couplers_to_auto_us_df_AWLEN;
  wire [0:0]s00_couplers_to_auto_us_df_AWLOCK;
  wire [2:0]s00_couplers_to_auto_us_df_AWPROT;
  wire [3:0]s00_couplers_to_auto_us_df_AWQOS;
  wire s00_couplers_to_auto_us_df_AWREADY;
  wire [2:0]s00_couplers_to_auto_us_df_AWSIZE;
  wire s00_couplers_to_auto_us_df_AWVALID;
  wire s00_couplers_to_auto_us_df_BREADY;
  wire [1:0]s00_couplers_to_auto_us_df_BRESP;
  wire s00_couplers_to_auto_us_df_BVALID;
  wire [31:0]s00_couplers_to_auto_us_df_RDATA;
  wire s00_couplers_to_auto_us_df_RLAST;
  wire s00_couplers_to_auto_us_df_RREADY;
  wire [1:0]s00_couplers_to_auto_us_df_RRESP;
  wire s00_couplers_to_auto_us_df_RVALID;
  wire [31:0]s00_couplers_to_auto_us_df_WDATA;
  wire s00_couplers_to_auto_us_df_WLAST;
  wire s00_couplers_to_auto_us_df_WREADY;
  wire [3:0]s00_couplers_to_auto_us_df_WSTRB;
  wire s00_couplers_to_auto_us_df_WVALID;

  assign M_ACLK_1 = M_ACLK;
  assign M_ARESETN_1 = M_ARESETN[0];
  assign M_AXI_araddr[31:0] = auto_us_df_to_s00_couplers_ARADDR;
  assign M_AXI_arburst[1:0] = auto_us_df_to_s00_couplers_ARBURST;
  assign M_AXI_arcache[3:0] = auto_us_df_to_s00_couplers_ARCACHE;
  assign M_AXI_arlen[7:0] = auto_us_df_to_s00_couplers_ARLEN;
  assign M_AXI_arlock[0] = auto_us_df_to_s00_couplers_ARLOCK;
  assign M_AXI_arprot[2:0] = auto_us_df_to_s00_couplers_ARPROT;
  assign M_AXI_arqos[3:0] = auto_us_df_to_s00_couplers_ARQOS;
  assign M_AXI_arsize[2:0] = auto_us_df_to_s00_couplers_ARSIZE;
  assign M_AXI_arvalid = auto_us_df_to_s00_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = auto_us_df_to_s00_couplers_AWADDR;
  assign M_AXI_awburst[1:0] = auto_us_df_to_s00_couplers_AWBURST;
  assign M_AXI_awcache[3:0] = auto_us_df_to_s00_couplers_AWCACHE;
  assign M_AXI_awlen[7:0] = auto_us_df_to_s00_couplers_AWLEN;
  assign M_AXI_awlock[0] = auto_us_df_to_s00_couplers_AWLOCK;
  assign M_AXI_awprot[2:0] = auto_us_df_to_s00_couplers_AWPROT;
  assign M_AXI_awqos[3:0] = auto_us_df_to_s00_couplers_AWQOS;
  assign M_AXI_awsize[2:0] = auto_us_df_to_s00_couplers_AWSIZE;
  assign M_AXI_awvalid = auto_us_df_to_s00_couplers_AWVALID;
  assign M_AXI_bready = auto_us_df_to_s00_couplers_BREADY;
  assign M_AXI_rready = auto_us_df_to_s00_couplers_RREADY;
  assign M_AXI_wdata[511:0] = auto_us_df_to_s00_couplers_WDATA;
  assign M_AXI_wlast = auto_us_df_to_s00_couplers_WLAST;
  assign M_AXI_wstrb[63:0] = auto_us_df_to_s00_couplers_WSTRB;
  assign M_AXI_wvalid = auto_us_df_to_s00_couplers_WVALID;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN[0];
  assign S_AXI_arready = s00_couplers_to_auto_us_df_ARREADY;
  assign S_AXI_awready = s00_couplers_to_auto_us_df_AWREADY;
  assign S_AXI_bresp[1:0] = s00_couplers_to_auto_us_df_BRESP;
  assign S_AXI_bvalid = s00_couplers_to_auto_us_df_BVALID;
  assign S_AXI_rdata[31:0] = s00_couplers_to_auto_us_df_RDATA;
  assign S_AXI_rlast = s00_couplers_to_auto_us_df_RLAST;
  assign S_AXI_rresp[1:0] = s00_couplers_to_auto_us_df_RRESP;
  assign S_AXI_rvalid = s00_couplers_to_auto_us_df_RVALID;
  assign S_AXI_wready = s00_couplers_to_auto_us_df_WREADY;
  assign auto_us_df_to_s00_couplers_ARREADY = M_AXI_arready;
  assign auto_us_df_to_s00_couplers_AWREADY = M_AXI_awready;
  assign auto_us_df_to_s00_couplers_BRESP = M_AXI_bresp[1:0];
  assign auto_us_df_to_s00_couplers_BVALID = M_AXI_bvalid;
  assign auto_us_df_to_s00_couplers_RDATA = M_AXI_rdata[511:0];
  assign auto_us_df_to_s00_couplers_RLAST = M_AXI_rlast;
  assign auto_us_df_to_s00_couplers_RRESP = M_AXI_rresp[1:0];
  assign auto_us_df_to_s00_couplers_RVALID = M_AXI_rvalid;
  assign auto_us_df_to_s00_couplers_WREADY = M_AXI_wready;
  assign s00_couplers_to_auto_us_df_ARADDR = S_AXI_araddr[31:0];
  assign s00_couplers_to_auto_us_df_ARBURST = S_AXI_arburst[1:0];
  assign s00_couplers_to_auto_us_df_ARCACHE = S_AXI_arcache[3:0];
  assign s00_couplers_to_auto_us_df_ARLEN = S_AXI_arlen[7:0];
  assign s00_couplers_to_auto_us_df_ARLOCK = S_AXI_arlock[0];
  assign s00_couplers_to_auto_us_df_ARPROT = S_AXI_arprot[2:0];
  assign s00_couplers_to_auto_us_df_ARQOS = S_AXI_arqos[3:0];
  assign s00_couplers_to_auto_us_df_ARSIZE = S_AXI_arsize[2:0];
  assign s00_couplers_to_auto_us_df_ARVALID = S_AXI_arvalid;
  assign s00_couplers_to_auto_us_df_AWADDR = S_AXI_awaddr[31:0];
  assign s00_couplers_to_auto_us_df_AWBURST = S_AXI_awburst[1:0];
  assign s00_couplers_to_auto_us_df_AWCACHE = S_AXI_awcache[3:0];
  assign s00_couplers_to_auto_us_df_AWLEN = S_AXI_awlen[7:0];
  assign s00_couplers_to_auto_us_df_AWLOCK = S_AXI_awlock[0];
  assign s00_couplers_to_auto_us_df_AWPROT = S_AXI_awprot[2:0];
  assign s00_couplers_to_auto_us_df_AWQOS = S_AXI_awqos[3:0];
  assign s00_couplers_to_auto_us_df_AWSIZE = S_AXI_awsize[2:0];
  assign s00_couplers_to_auto_us_df_AWVALID = S_AXI_awvalid;
  assign s00_couplers_to_auto_us_df_BREADY = S_AXI_bready;
  assign s00_couplers_to_auto_us_df_RREADY = S_AXI_rready;
  assign s00_couplers_to_auto_us_df_WDATA = S_AXI_wdata[31:0];
  assign s00_couplers_to_auto_us_df_WLAST = S_AXI_wlast;
  assign s00_couplers_to_auto_us_df_WSTRB = S_AXI_wstrb[3:0];
  assign s00_couplers_to_auto_us_df_WVALID = S_AXI_wvalid;
  design_1_auto_us_df_0 auto_us_df
       (.m_axi_araddr(auto_us_df_to_s00_couplers_ARADDR),
        .m_axi_arburst(auto_us_df_to_s00_couplers_ARBURST),
        .m_axi_arcache(auto_us_df_to_s00_couplers_ARCACHE),
        .m_axi_arlen(auto_us_df_to_s00_couplers_ARLEN),
        .m_axi_arlock(auto_us_df_to_s00_couplers_ARLOCK),
        .m_axi_arprot(auto_us_df_to_s00_couplers_ARPROT),
        .m_axi_arqos(auto_us_df_to_s00_couplers_ARQOS),
        .m_axi_arready(auto_us_df_to_s00_couplers_ARREADY),
        .m_axi_arsize(auto_us_df_to_s00_couplers_ARSIZE),
        .m_axi_arvalid(auto_us_df_to_s00_couplers_ARVALID),
        .m_axi_awaddr(auto_us_df_to_s00_couplers_AWADDR),
        .m_axi_awburst(auto_us_df_to_s00_couplers_AWBURST),
        .m_axi_awcache(auto_us_df_to_s00_couplers_AWCACHE),
        .m_axi_awlen(auto_us_df_to_s00_couplers_AWLEN),
        .m_axi_awlock(auto_us_df_to_s00_couplers_AWLOCK),
        .m_axi_awprot(auto_us_df_to_s00_couplers_AWPROT),
        .m_axi_awqos(auto_us_df_to_s00_couplers_AWQOS),
        .m_axi_awready(auto_us_df_to_s00_couplers_AWREADY),
        .m_axi_awsize(auto_us_df_to_s00_couplers_AWSIZE),
        .m_axi_awvalid(auto_us_df_to_s00_couplers_AWVALID),
        .m_axi_bready(auto_us_df_to_s00_couplers_BREADY),
        .m_axi_bresp(auto_us_df_to_s00_couplers_BRESP),
        .m_axi_bvalid(auto_us_df_to_s00_couplers_BVALID),
        .m_axi_rdata(auto_us_df_to_s00_couplers_RDATA),
        .m_axi_rlast(auto_us_df_to_s00_couplers_RLAST),
        .m_axi_rready(auto_us_df_to_s00_couplers_RREADY),
        .m_axi_rresp(auto_us_df_to_s00_couplers_RRESP),
        .m_axi_rvalid(auto_us_df_to_s00_couplers_RVALID),
        .m_axi_wdata(auto_us_df_to_s00_couplers_WDATA),
        .m_axi_wlast(auto_us_df_to_s00_couplers_WLAST),
        .m_axi_wready(auto_us_df_to_s00_couplers_WREADY),
        .m_axi_wstrb(auto_us_df_to_s00_couplers_WSTRB),
        .m_axi_wvalid(auto_us_df_to_s00_couplers_WVALID),
        .s_axi_aclk(S_ACLK_1),
        .s_axi_araddr(s00_couplers_to_auto_us_df_ARADDR),
        .s_axi_arburst(s00_couplers_to_auto_us_df_ARBURST),
        .s_axi_arcache(s00_couplers_to_auto_us_df_ARCACHE),
        .s_axi_aresetn(S_ARESETN_1),
        .s_axi_arlen(s00_couplers_to_auto_us_df_ARLEN),
        .s_axi_arlock(s00_couplers_to_auto_us_df_ARLOCK),
        .s_axi_arprot(s00_couplers_to_auto_us_df_ARPROT),
        .s_axi_arqos(s00_couplers_to_auto_us_df_ARQOS),
        .s_axi_arready(s00_couplers_to_auto_us_df_ARREADY),
        .s_axi_arregion({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arsize(s00_couplers_to_auto_us_df_ARSIZE),
        .s_axi_arvalid(s00_couplers_to_auto_us_df_ARVALID),
        .s_axi_awaddr(s00_couplers_to_auto_us_df_AWADDR),
        .s_axi_awburst(s00_couplers_to_auto_us_df_AWBURST),
        .s_axi_awcache(s00_couplers_to_auto_us_df_AWCACHE),
        .s_axi_awlen(s00_couplers_to_auto_us_df_AWLEN),
        .s_axi_awlock(s00_couplers_to_auto_us_df_AWLOCK),
        .s_axi_awprot(s00_couplers_to_auto_us_df_AWPROT),
        .s_axi_awqos(s00_couplers_to_auto_us_df_AWQOS),
        .s_axi_awready(s00_couplers_to_auto_us_df_AWREADY),
        .s_axi_awregion({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awsize(s00_couplers_to_auto_us_df_AWSIZE),
        .s_axi_awvalid(s00_couplers_to_auto_us_df_AWVALID),
        .s_axi_bready(s00_couplers_to_auto_us_df_BREADY),
        .s_axi_bresp(s00_couplers_to_auto_us_df_BRESP),
        .s_axi_bvalid(s00_couplers_to_auto_us_df_BVALID),
        .s_axi_rdata(s00_couplers_to_auto_us_df_RDATA),
        .s_axi_rlast(s00_couplers_to_auto_us_df_RLAST),
        .s_axi_rready(s00_couplers_to_auto_us_df_RREADY),
        .s_axi_rresp(s00_couplers_to_auto_us_df_RRESP),
        .s_axi_rvalid(s00_couplers_to_auto_us_df_RVALID),
        .s_axi_wdata(s00_couplers_to_auto_us_df_WDATA),
        .s_axi_wlast(s00_couplers_to_auto_us_df_WLAST),
        .s_axi_wready(s00_couplers_to_auto_us_df_WREADY),
        .s_axi_wstrb(s00_couplers_to_auto_us_df_WSTRB),
        .s_axi_wvalid(s00_couplers_to_auto_us_df_WVALID));
endmodule

module s01_couplers_imp_1W60HW0
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arburst,
    M_AXI_arcache,
    M_AXI_arlen,
    M_AXI_arlock,
    M_AXI_arprot,
    M_AXI_arqos,
    M_AXI_arready,
    M_AXI_arsize,
    M_AXI_arvalid,
    M_AXI_rdata,
    M_AXI_rlast,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arburst,
    S_AXI_arcache,
    S_AXI_arlen,
    S_AXI_arlock,
    S_AXI_arprot,
    S_AXI_arqos,
    S_AXI_arready,
    S_AXI_arsize,
    S_AXI_arvalid,
    S_AXI_rdata,
    S_AXI_rlast,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [1:0]M_AXI_arburst;
  output [3:0]M_AXI_arcache;
  output [7:0]M_AXI_arlen;
  output [0:0]M_AXI_arlock;
  output [2:0]M_AXI_arprot;
  output [3:0]M_AXI_arqos;
  input M_AXI_arready;
  output [2:0]M_AXI_arsize;
  output M_AXI_arvalid;
  input [511:0]M_AXI_rdata;
  input M_AXI_rlast;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [1:0]S_AXI_arburst;
  input [3:0]S_AXI_arcache;
  input [7:0]S_AXI_arlen;
  input [0:0]S_AXI_arlock;
  input [2:0]S_AXI_arprot;
  input [3:0]S_AXI_arqos;
  output S_AXI_arready;
  input [2:0]S_AXI_arsize;
  input S_AXI_arvalid;
  output [31:0]S_AXI_rdata;
  output S_AXI_rlast;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;

  wire M_ACLK_1;
  wire [0:0]M_ARESETN_1;
  wire S_ACLK_1;
  wire [0:0]S_ARESETN_1;
  wire [31:0]auto_us_df_to_s01_couplers_ARADDR;
  wire [1:0]auto_us_df_to_s01_couplers_ARBURST;
  wire [3:0]auto_us_df_to_s01_couplers_ARCACHE;
  wire [7:0]auto_us_df_to_s01_couplers_ARLEN;
  wire [0:0]auto_us_df_to_s01_couplers_ARLOCK;
  wire [2:0]auto_us_df_to_s01_couplers_ARPROT;
  wire [3:0]auto_us_df_to_s01_couplers_ARQOS;
  wire auto_us_df_to_s01_couplers_ARREADY;
  wire [2:0]auto_us_df_to_s01_couplers_ARSIZE;
  wire auto_us_df_to_s01_couplers_ARVALID;
  wire [511:0]auto_us_df_to_s01_couplers_RDATA;
  wire auto_us_df_to_s01_couplers_RLAST;
  wire auto_us_df_to_s01_couplers_RREADY;
  wire [1:0]auto_us_df_to_s01_couplers_RRESP;
  wire auto_us_df_to_s01_couplers_RVALID;
  wire [31:0]s01_couplers_to_auto_us_df_ARADDR;
  wire [1:0]s01_couplers_to_auto_us_df_ARBURST;
  wire [3:0]s01_couplers_to_auto_us_df_ARCACHE;
  wire [7:0]s01_couplers_to_auto_us_df_ARLEN;
  wire [0:0]s01_couplers_to_auto_us_df_ARLOCK;
  wire [2:0]s01_couplers_to_auto_us_df_ARPROT;
  wire [3:0]s01_couplers_to_auto_us_df_ARQOS;
  wire s01_couplers_to_auto_us_df_ARREADY;
  wire [2:0]s01_couplers_to_auto_us_df_ARSIZE;
  wire s01_couplers_to_auto_us_df_ARVALID;
  wire [31:0]s01_couplers_to_auto_us_df_RDATA;
  wire s01_couplers_to_auto_us_df_RLAST;
  wire s01_couplers_to_auto_us_df_RREADY;
  wire [1:0]s01_couplers_to_auto_us_df_RRESP;
  wire s01_couplers_to_auto_us_df_RVALID;

  assign M_ACLK_1 = M_ACLK;
  assign M_ARESETN_1 = M_ARESETN[0];
  assign M_AXI_araddr[31:0] = auto_us_df_to_s01_couplers_ARADDR;
  assign M_AXI_arburst[1:0] = auto_us_df_to_s01_couplers_ARBURST;
  assign M_AXI_arcache[3:0] = auto_us_df_to_s01_couplers_ARCACHE;
  assign M_AXI_arlen[7:0] = auto_us_df_to_s01_couplers_ARLEN;
  assign M_AXI_arlock[0] = auto_us_df_to_s01_couplers_ARLOCK;
  assign M_AXI_arprot[2:0] = auto_us_df_to_s01_couplers_ARPROT;
  assign M_AXI_arqos[3:0] = auto_us_df_to_s01_couplers_ARQOS;
  assign M_AXI_arsize[2:0] = auto_us_df_to_s01_couplers_ARSIZE;
  assign M_AXI_arvalid = auto_us_df_to_s01_couplers_ARVALID;
  assign M_AXI_rready = auto_us_df_to_s01_couplers_RREADY;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN[0];
  assign S_AXI_arready = s01_couplers_to_auto_us_df_ARREADY;
  assign S_AXI_rdata[31:0] = s01_couplers_to_auto_us_df_RDATA;
  assign S_AXI_rlast = s01_couplers_to_auto_us_df_RLAST;
  assign S_AXI_rresp[1:0] = s01_couplers_to_auto_us_df_RRESP;
  assign S_AXI_rvalid = s01_couplers_to_auto_us_df_RVALID;
  assign auto_us_df_to_s01_couplers_ARREADY = M_AXI_arready;
  assign auto_us_df_to_s01_couplers_RDATA = M_AXI_rdata[511:0];
  assign auto_us_df_to_s01_couplers_RLAST = M_AXI_rlast;
  assign auto_us_df_to_s01_couplers_RRESP = M_AXI_rresp[1:0];
  assign auto_us_df_to_s01_couplers_RVALID = M_AXI_rvalid;
  assign s01_couplers_to_auto_us_df_ARADDR = S_AXI_araddr[31:0];
  assign s01_couplers_to_auto_us_df_ARBURST = S_AXI_arburst[1:0];
  assign s01_couplers_to_auto_us_df_ARCACHE = S_AXI_arcache[3:0];
  assign s01_couplers_to_auto_us_df_ARLEN = S_AXI_arlen[7:0];
  assign s01_couplers_to_auto_us_df_ARLOCK = S_AXI_arlock[0];
  assign s01_couplers_to_auto_us_df_ARPROT = S_AXI_arprot[2:0];
  assign s01_couplers_to_auto_us_df_ARQOS = S_AXI_arqos[3:0];
  assign s01_couplers_to_auto_us_df_ARSIZE = S_AXI_arsize[2:0];
  assign s01_couplers_to_auto_us_df_ARVALID = S_AXI_arvalid;
  assign s01_couplers_to_auto_us_df_RREADY = S_AXI_rready;
  design_1_auto_us_df_1 auto_us_df
       (.m_axi_araddr(auto_us_df_to_s01_couplers_ARADDR),
        .m_axi_arburst(auto_us_df_to_s01_couplers_ARBURST),
        .m_axi_arcache(auto_us_df_to_s01_couplers_ARCACHE),
        .m_axi_arlen(auto_us_df_to_s01_couplers_ARLEN),
        .m_axi_arlock(auto_us_df_to_s01_couplers_ARLOCK),
        .m_axi_arprot(auto_us_df_to_s01_couplers_ARPROT),
        .m_axi_arqos(auto_us_df_to_s01_couplers_ARQOS),
        .m_axi_arready(auto_us_df_to_s01_couplers_ARREADY),
        .m_axi_arsize(auto_us_df_to_s01_couplers_ARSIZE),
        .m_axi_arvalid(auto_us_df_to_s01_couplers_ARVALID),
        .m_axi_rdata(auto_us_df_to_s01_couplers_RDATA),
        .m_axi_rlast(auto_us_df_to_s01_couplers_RLAST),
        .m_axi_rready(auto_us_df_to_s01_couplers_RREADY),
        .m_axi_rresp(auto_us_df_to_s01_couplers_RRESP),
        .m_axi_rvalid(auto_us_df_to_s01_couplers_RVALID),
        .s_axi_aclk(S_ACLK_1),
        .s_axi_araddr(s01_couplers_to_auto_us_df_ARADDR),
        .s_axi_arburst(s01_couplers_to_auto_us_df_ARBURST),
        .s_axi_arcache(s01_couplers_to_auto_us_df_ARCACHE),
        .s_axi_aresetn(S_ARESETN_1),
        .s_axi_arlen(s01_couplers_to_auto_us_df_ARLEN),
        .s_axi_arlock(s01_couplers_to_auto_us_df_ARLOCK),
        .s_axi_arprot(s01_couplers_to_auto_us_df_ARPROT),
        .s_axi_arqos(s01_couplers_to_auto_us_df_ARQOS),
        .s_axi_arready(s01_couplers_to_auto_us_df_ARREADY),
        .s_axi_arregion({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arsize(s01_couplers_to_auto_us_df_ARSIZE),
        .s_axi_arvalid(s01_couplers_to_auto_us_df_ARVALID),
        .s_axi_rdata(s01_couplers_to_auto_us_df_RDATA),
        .s_axi_rlast(s01_couplers_to_auto_us_df_RLAST),
        .s_axi_rready(s01_couplers_to_auto_us_df_RREADY),
        .s_axi_rresp(s01_couplers_to_auto_us_df_RRESP),
        .s_axi_rvalid(s01_couplers_to_auto_us_df_RVALID));
endmodule

module s02_couplers_imp_8NCF02
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arburst,
    M_AXI_arcache,
    M_AXI_arlen,
    M_AXI_arlock,
    M_AXI_arprot,
    M_AXI_arqos,
    M_AXI_arready,
    M_AXI_arsize,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awburst,
    M_AXI_awcache,
    M_AXI_awlen,
    M_AXI_awlock,
    M_AXI_awprot,
    M_AXI_awqos,
    M_AXI_awready,
    M_AXI_awsize,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rlast,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wlast,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arburst,
    S_AXI_arcache,
    S_AXI_arlen,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arsize,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awburst,
    S_AXI_awcache,
    S_AXI_awlen,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awsize,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rlast,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wlast,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [1:0]M_AXI_arburst;
  output [3:0]M_AXI_arcache;
  output [7:0]M_AXI_arlen;
  output [0:0]M_AXI_arlock;
  output [2:0]M_AXI_arprot;
  output [3:0]M_AXI_arqos;
  input M_AXI_arready;
  output [2:0]M_AXI_arsize;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [1:0]M_AXI_awburst;
  output [3:0]M_AXI_awcache;
  output [7:0]M_AXI_awlen;
  output [0:0]M_AXI_awlock;
  output [2:0]M_AXI_awprot;
  output [3:0]M_AXI_awqos;
  input M_AXI_awready;
  output [2:0]M_AXI_awsize;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [511:0]M_AXI_rdata;
  input M_AXI_rlast;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [511:0]M_AXI_wdata;
  output M_AXI_wlast;
  input M_AXI_wready;
  output [63:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [1:0]S_AXI_arburst;
  input [3:0]S_AXI_arcache;
  input [7:0]S_AXI_arlen;
  input [2:0]S_AXI_arprot;
  output S_AXI_arready;
  input [2:0]S_AXI_arsize;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [1:0]S_AXI_awburst;
  input [3:0]S_AXI_awcache;
  input [7:0]S_AXI_awlen;
  input [2:0]S_AXI_awprot;
  output S_AXI_awready;
  input [2:0]S_AXI_awsize;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  output S_AXI_rlast;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  input S_AXI_wlast;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire M_ACLK_1;
  wire [0:0]M_ARESETN_1;
  wire S_ACLK_1;
  wire [0:0]S_ARESETN_1;
  wire [31:0]auto_us_df_to_s02_couplers_ARADDR;
  wire [1:0]auto_us_df_to_s02_couplers_ARBURST;
  wire [3:0]auto_us_df_to_s02_couplers_ARCACHE;
  wire [7:0]auto_us_df_to_s02_couplers_ARLEN;
  wire [0:0]auto_us_df_to_s02_couplers_ARLOCK;
  wire [2:0]auto_us_df_to_s02_couplers_ARPROT;
  wire [3:0]auto_us_df_to_s02_couplers_ARQOS;
  wire auto_us_df_to_s02_couplers_ARREADY;
  wire [2:0]auto_us_df_to_s02_couplers_ARSIZE;
  wire auto_us_df_to_s02_couplers_ARVALID;
  wire [31:0]auto_us_df_to_s02_couplers_AWADDR;
  wire [1:0]auto_us_df_to_s02_couplers_AWBURST;
  wire [3:0]auto_us_df_to_s02_couplers_AWCACHE;
  wire [7:0]auto_us_df_to_s02_couplers_AWLEN;
  wire [0:0]auto_us_df_to_s02_couplers_AWLOCK;
  wire [2:0]auto_us_df_to_s02_couplers_AWPROT;
  wire [3:0]auto_us_df_to_s02_couplers_AWQOS;
  wire auto_us_df_to_s02_couplers_AWREADY;
  wire [2:0]auto_us_df_to_s02_couplers_AWSIZE;
  wire auto_us_df_to_s02_couplers_AWVALID;
  wire auto_us_df_to_s02_couplers_BREADY;
  wire [1:0]auto_us_df_to_s02_couplers_BRESP;
  wire auto_us_df_to_s02_couplers_BVALID;
  wire [511:0]auto_us_df_to_s02_couplers_RDATA;
  wire auto_us_df_to_s02_couplers_RLAST;
  wire auto_us_df_to_s02_couplers_RREADY;
  wire [1:0]auto_us_df_to_s02_couplers_RRESP;
  wire auto_us_df_to_s02_couplers_RVALID;
  wire [511:0]auto_us_df_to_s02_couplers_WDATA;
  wire auto_us_df_to_s02_couplers_WLAST;
  wire auto_us_df_to_s02_couplers_WREADY;
  wire [63:0]auto_us_df_to_s02_couplers_WSTRB;
  wire auto_us_df_to_s02_couplers_WVALID;
  wire [31:0]s02_couplers_to_auto_us_df_ARADDR;
  wire [1:0]s02_couplers_to_auto_us_df_ARBURST;
  wire [3:0]s02_couplers_to_auto_us_df_ARCACHE;
  wire [7:0]s02_couplers_to_auto_us_df_ARLEN;
  wire [2:0]s02_couplers_to_auto_us_df_ARPROT;
  wire s02_couplers_to_auto_us_df_ARREADY;
  wire [2:0]s02_couplers_to_auto_us_df_ARSIZE;
  wire s02_couplers_to_auto_us_df_ARVALID;
  wire [31:0]s02_couplers_to_auto_us_df_AWADDR;
  wire [1:0]s02_couplers_to_auto_us_df_AWBURST;
  wire [3:0]s02_couplers_to_auto_us_df_AWCACHE;
  wire [7:0]s02_couplers_to_auto_us_df_AWLEN;
  wire [2:0]s02_couplers_to_auto_us_df_AWPROT;
  wire s02_couplers_to_auto_us_df_AWREADY;
  wire [2:0]s02_couplers_to_auto_us_df_AWSIZE;
  wire s02_couplers_to_auto_us_df_AWVALID;
  wire s02_couplers_to_auto_us_df_BREADY;
  wire [1:0]s02_couplers_to_auto_us_df_BRESP;
  wire s02_couplers_to_auto_us_df_BVALID;
  wire [31:0]s02_couplers_to_auto_us_df_RDATA;
  wire s02_couplers_to_auto_us_df_RLAST;
  wire s02_couplers_to_auto_us_df_RREADY;
  wire [1:0]s02_couplers_to_auto_us_df_RRESP;
  wire s02_couplers_to_auto_us_df_RVALID;
  wire [31:0]s02_couplers_to_auto_us_df_WDATA;
  wire s02_couplers_to_auto_us_df_WLAST;
  wire s02_couplers_to_auto_us_df_WREADY;
  wire [3:0]s02_couplers_to_auto_us_df_WSTRB;
  wire s02_couplers_to_auto_us_df_WVALID;

  assign M_ACLK_1 = M_ACLK;
  assign M_ARESETN_1 = M_ARESETN[0];
  assign M_AXI_araddr[31:0] = auto_us_df_to_s02_couplers_ARADDR;
  assign M_AXI_arburst[1:0] = auto_us_df_to_s02_couplers_ARBURST;
  assign M_AXI_arcache[3:0] = auto_us_df_to_s02_couplers_ARCACHE;
  assign M_AXI_arlen[7:0] = auto_us_df_to_s02_couplers_ARLEN;
  assign M_AXI_arlock[0] = auto_us_df_to_s02_couplers_ARLOCK;
  assign M_AXI_arprot[2:0] = auto_us_df_to_s02_couplers_ARPROT;
  assign M_AXI_arqos[3:0] = auto_us_df_to_s02_couplers_ARQOS;
  assign M_AXI_arsize[2:0] = auto_us_df_to_s02_couplers_ARSIZE;
  assign M_AXI_arvalid = auto_us_df_to_s02_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = auto_us_df_to_s02_couplers_AWADDR;
  assign M_AXI_awburst[1:0] = auto_us_df_to_s02_couplers_AWBURST;
  assign M_AXI_awcache[3:0] = auto_us_df_to_s02_couplers_AWCACHE;
  assign M_AXI_awlen[7:0] = auto_us_df_to_s02_couplers_AWLEN;
  assign M_AXI_awlock[0] = auto_us_df_to_s02_couplers_AWLOCK;
  assign M_AXI_awprot[2:0] = auto_us_df_to_s02_couplers_AWPROT;
  assign M_AXI_awqos[3:0] = auto_us_df_to_s02_couplers_AWQOS;
  assign M_AXI_awsize[2:0] = auto_us_df_to_s02_couplers_AWSIZE;
  assign M_AXI_awvalid = auto_us_df_to_s02_couplers_AWVALID;
  assign M_AXI_bready = auto_us_df_to_s02_couplers_BREADY;
  assign M_AXI_rready = auto_us_df_to_s02_couplers_RREADY;
  assign M_AXI_wdata[511:0] = auto_us_df_to_s02_couplers_WDATA;
  assign M_AXI_wlast = auto_us_df_to_s02_couplers_WLAST;
  assign M_AXI_wstrb[63:0] = auto_us_df_to_s02_couplers_WSTRB;
  assign M_AXI_wvalid = auto_us_df_to_s02_couplers_WVALID;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN[0];
  assign S_AXI_arready = s02_couplers_to_auto_us_df_ARREADY;
  assign S_AXI_awready = s02_couplers_to_auto_us_df_AWREADY;
  assign S_AXI_bresp[1:0] = s02_couplers_to_auto_us_df_BRESP;
  assign S_AXI_bvalid = s02_couplers_to_auto_us_df_BVALID;
  assign S_AXI_rdata[31:0] = s02_couplers_to_auto_us_df_RDATA;
  assign S_AXI_rlast = s02_couplers_to_auto_us_df_RLAST;
  assign S_AXI_rresp[1:0] = s02_couplers_to_auto_us_df_RRESP;
  assign S_AXI_rvalid = s02_couplers_to_auto_us_df_RVALID;
  assign S_AXI_wready = s02_couplers_to_auto_us_df_WREADY;
  assign auto_us_df_to_s02_couplers_ARREADY = M_AXI_arready;
  assign auto_us_df_to_s02_couplers_AWREADY = M_AXI_awready;
  assign auto_us_df_to_s02_couplers_BRESP = M_AXI_bresp[1:0];
  assign auto_us_df_to_s02_couplers_BVALID = M_AXI_bvalid;
  assign auto_us_df_to_s02_couplers_RDATA = M_AXI_rdata[511:0];
  assign auto_us_df_to_s02_couplers_RLAST = M_AXI_rlast;
  assign auto_us_df_to_s02_couplers_RRESP = M_AXI_rresp[1:0];
  assign auto_us_df_to_s02_couplers_RVALID = M_AXI_rvalid;
  assign auto_us_df_to_s02_couplers_WREADY = M_AXI_wready;
  assign s02_couplers_to_auto_us_df_ARADDR = S_AXI_araddr[31:0];
  assign s02_couplers_to_auto_us_df_ARBURST = S_AXI_arburst[1:0];
  assign s02_couplers_to_auto_us_df_ARCACHE = S_AXI_arcache[3:0];
  assign s02_couplers_to_auto_us_df_ARLEN = S_AXI_arlen[7:0];
  assign s02_couplers_to_auto_us_df_ARPROT = S_AXI_arprot[2:0];
  assign s02_couplers_to_auto_us_df_ARSIZE = S_AXI_arsize[2:0];
  assign s02_couplers_to_auto_us_df_ARVALID = S_AXI_arvalid;
  assign s02_couplers_to_auto_us_df_AWADDR = S_AXI_awaddr[31:0];
  assign s02_couplers_to_auto_us_df_AWBURST = S_AXI_awburst[1:0];
  assign s02_couplers_to_auto_us_df_AWCACHE = S_AXI_awcache[3:0];
  assign s02_couplers_to_auto_us_df_AWLEN = S_AXI_awlen[7:0];
  assign s02_couplers_to_auto_us_df_AWPROT = S_AXI_awprot[2:0];
  assign s02_couplers_to_auto_us_df_AWSIZE = S_AXI_awsize[2:0];
  assign s02_couplers_to_auto_us_df_AWVALID = S_AXI_awvalid;
  assign s02_couplers_to_auto_us_df_BREADY = S_AXI_bready;
  assign s02_couplers_to_auto_us_df_RREADY = S_AXI_rready;
  assign s02_couplers_to_auto_us_df_WDATA = S_AXI_wdata[31:0];
  assign s02_couplers_to_auto_us_df_WLAST = S_AXI_wlast;
  assign s02_couplers_to_auto_us_df_WSTRB = S_AXI_wstrb[3:0];
  assign s02_couplers_to_auto_us_df_WVALID = S_AXI_wvalid;
  design_1_auto_us_df_2 auto_us_df
       (.m_axi_araddr(auto_us_df_to_s02_couplers_ARADDR),
        .m_axi_arburst(auto_us_df_to_s02_couplers_ARBURST),
        .m_axi_arcache(auto_us_df_to_s02_couplers_ARCACHE),
        .m_axi_arlen(auto_us_df_to_s02_couplers_ARLEN),
        .m_axi_arlock(auto_us_df_to_s02_couplers_ARLOCK),
        .m_axi_arprot(auto_us_df_to_s02_couplers_ARPROT),
        .m_axi_arqos(auto_us_df_to_s02_couplers_ARQOS),
        .m_axi_arready(auto_us_df_to_s02_couplers_ARREADY),
        .m_axi_arsize(auto_us_df_to_s02_couplers_ARSIZE),
        .m_axi_arvalid(auto_us_df_to_s02_couplers_ARVALID),
        .m_axi_awaddr(auto_us_df_to_s02_couplers_AWADDR),
        .m_axi_awburst(auto_us_df_to_s02_couplers_AWBURST),
        .m_axi_awcache(auto_us_df_to_s02_couplers_AWCACHE),
        .m_axi_awlen(auto_us_df_to_s02_couplers_AWLEN),
        .m_axi_awlock(auto_us_df_to_s02_couplers_AWLOCK),
        .m_axi_awprot(auto_us_df_to_s02_couplers_AWPROT),
        .m_axi_awqos(auto_us_df_to_s02_couplers_AWQOS),
        .m_axi_awready(auto_us_df_to_s02_couplers_AWREADY),
        .m_axi_awsize(auto_us_df_to_s02_couplers_AWSIZE),
        .m_axi_awvalid(auto_us_df_to_s02_couplers_AWVALID),
        .m_axi_bready(auto_us_df_to_s02_couplers_BREADY),
        .m_axi_bresp(auto_us_df_to_s02_couplers_BRESP),
        .m_axi_bvalid(auto_us_df_to_s02_couplers_BVALID),
        .m_axi_rdata(auto_us_df_to_s02_couplers_RDATA),
        .m_axi_rlast(auto_us_df_to_s02_couplers_RLAST),
        .m_axi_rready(auto_us_df_to_s02_couplers_RREADY),
        .m_axi_rresp(auto_us_df_to_s02_couplers_RRESP),
        .m_axi_rvalid(auto_us_df_to_s02_couplers_RVALID),
        .m_axi_wdata(auto_us_df_to_s02_couplers_WDATA),
        .m_axi_wlast(auto_us_df_to_s02_couplers_WLAST),
        .m_axi_wready(auto_us_df_to_s02_couplers_WREADY),
        .m_axi_wstrb(auto_us_df_to_s02_couplers_WSTRB),
        .m_axi_wvalid(auto_us_df_to_s02_couplers_WVALID),
        .s_axi_aclk(S_ACLK_1),
        .s_axi_araddr(s02_couplers_to_auto_us_df_ARADDR),
        .s_axi_arburst(s02_couplers_to_auto_us_df_ARBURST),
        .s_axi_arcache(s02_couplers_to_auto_us_df_ARCACHE),
        .s_axi_aresetn(S_ARESETN_1),
        .s_axi_arlen(s02_couplers_to_auto_us_df_ARLEN),
        .s_axi_arlock(1'b0),
        .s_axi_arprot(s02_couplers_to_auto_us_df_ARPROT),
        .s_axi_arqos({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(s02_couplers_to_auto_us_df_ARREADY),
        .s_axi_arregion({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arsize(s02_couplers_to_auto_us_df_ARSIZE),
        .s_axi_arvalid(s02_couplers_to_auto_us_df_ARVALID),
        .s_axi_awaddr(s02_couplers_to_auto_us_df_AWADDR),
        .s_axi_awburst(s02_couplers_to_auto_us_df_AWBURST),
        .s_axi_awcache(s02_couplers_to_auto_us_df_AWCACHE),
        .s_axi_awlen(s02_couplers_to_auto_us_df_AWLEN),
        .s_axi_awlock(1'b0),
        .s_axi_awprot(s02_couplers_to_auto_us_df_AWPROT),
        .s_axi_awqos({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(s02_couplers_to_auto_us_df_AWREADY),
        .s_axi_awregion({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awsize(s02_couplers_to_auto_us_df_AWSIZE),
        .s_axi_awvalid(s02_couplers_to_auto_us_df_AWVALID),
        .s_axi_bready(s02_couplers_to_auto_us_df_BREADY),
        .s_axi_bresp(s02_couplers_to_auto_us_df_BRESP),
        .s_axi_bvalid(s02_couplers_to_auto_us_df_BVALID),
        .s_axi_rdata(s02_couplers_to_auto_us_df_RDATA),
        .s_axi_rlast(s02_couplers_to_auto_us_df_RLAST),
        .s_axi_rready(s02_couplers_to_auto_us_df_RREADY),
        .s_axi_rresp(s02_couplers_to_auto_us_df_RRESP),
        .s_axi_rvalid(s02_couplers_to_auto_us_df_RVALID),
        .s_axi_wdata(s02_couplers_to_auto_us_df_WDATA),
        .s_axi_wlast(s02_couplers_to_auto_us_df_WLAST),
        .s_axi_wready(s02_couplers_to_auto_us_df_WREADY),
        .s_axi_wstrb(s02_couplers_to_auto_us_df_WSTRB),
        .s_axi_wvalid(s02_couplers_to_auto_us_df_WVALID));
endmodule

module s03_couplers_imp_1UQ1PUB
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arburst,
    M_AXI_arcache,
    M_AXI_arlen,
    M_AXI_arlock,
    M_AXI_arprot,
    M_AXI_arqos,
    M_AXI_arready,
    M_AXI_arsize,
    M_AXI_arvalid,
    M_AXI_rdata,
    M_AXI_rlast,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arburst,
    S_AXI_arcache,
    S_AXI_arlen,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arsize,
    S_AXI_arvalid,
    S_AXI_rdata,
    S_AXI_rlast,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [1:0]M_AXI_arburst;
  output [3:0]M_AXI_arcache;
  output [7:0]M_AXI_arlen;
  output [0:0]M_AXI_arlock;
  output [2:0]M_AXI_arprot;
  output [3:0]M_AXI_arqos;
  input M_AXI_arready;
  output [2:0]M_AXI_arsize;
  output M_AXI_arvalid;
  input [511:0]M_AXI_rdata;
  input M_AXI_rlast;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [1:0]S_AXI_arburst;
  input [3:0]S_AXI_arcache;
  input [7:0]S_AXI_arlen;
  input [2:0]S_AXI_arprot;
  output S_AXI_arready;
  input [2:0]S_AXI_arsize;
  input S_AXI_arvalid;
  output [31:0]S_AXI_rdata;
  output S_AXI_rlast;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;

  wire M_ACLK_1;
  wire [0:0]M_ARESETN_1;
  wire S_ACLK_1;
  wire [0:0]S_ARESETN_1;
  wire [31:0]auto_us_df_to_s03_couplers_ARADDR;
  wire [1:0]auto_us_df_to_s03_couplers_ARBURST;
  wire [3:0]auto_us_df_to_s03_couplers_ARCACHE;
  wire [7:0]auto_us_df_to_s03_couplers_ARLEN;
  wire [0:0]auto_us_df_to_s03_couplers_ARLOCK;
  wire [2:0]auto_us_df_to_s03_couplers_ARPROT;
  wire [3:0]auto_us_df_to_s03_couplers_ARQOS;
  wire auto_us_df_to_s03_couplers_ARREADY;
  wire [2:0]auto_us_df_to_s03_couplers_ARSIZE;
  wire auto_us_df_to_s03_couplers_ARVALID;
  wire [511:0]auto_us_df_to_s03_couplers_RDATA;
  wire auto_us_df_to_s03_couplers_RLAST;
  wire auto_us_df_to_s03_couplers_RREADY;
  wire [1:0]auto_us_df_to_s03_couplers_RRESP;
  wire auto_us_df_to_s03_couplers_RVALID;
  wire [31:0]s03_couplers_to_auto_us_df_ARADDR;
  wire [1:0]s03_couplers_to_auto_us_df_ARBURST;
  wire [3:0]s03_couplers_to_auto_us_df_ARCACHE;
  wire [7:0]s03_couplers_to_auto_us_df_ARLEN;
  wire [2:0]s03_couplers_to_auto_us_df_ARPROT;
  wire s03_couplers_to_auto_us_df_ARREADY;
  wire [2:0]s03_couplers_to_auto_us_df_ARSIZE;
  wire s03_couplers_to_auto_us_df_ARVALID;
  wire [31:0]s03_couplers_to_auto_us_df_RDATA;
  wire s03_couplers_to_auto_us_df_RLAST;
  wire s03_couplers_to_auto_us_df_RREADY;
  wire [1:0]s03_couplers_to_auto_us_df_RRESP;
  wire s03_couplers_to_auto_us_df_RVALID;

  assign M_ACLK_1 = M_ACLK;
  assign M_ARESETN_1 = M_ARESETN[0];
  assign M_AXI_araddr[31:0] = auto_us_df_to_s03_couplers_ARADDR;
  assign M_AXI_arburst[1:0] = auto_us_df_to_s03_couplers_ARBURST;
  assign M_AXI_arcache[3:0] = auto_us_df_to_s03_couplers_ARCACHE;
  assign M_AXI_arlen[7:0] = auto_us_df_to_s03_couplers_ARLEN;
  assign M_AXI_arlock[0] = auto_us_df_to_s03_couplers_ARLOCK;
  assign M_AXI_arprot[2:0] = auto_us_df_to_s03_couplers_ARPROT;
  assign M_AXI_arqos[3:0] = auto_us_df_to_s03_couplers_ARQOS;
  assign M_AXI_arsize[2:0] = auto_us_df_to_s03_couplers_ARSIZE;
  assign M_AXI_arvalid = auto_us_df_to_s03_couplers_ARVALID;
  assign M_AXI_rready = auto_us_df_to_s03_couplers_RREADY;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN[0];
  assign S_AXI_arready = s03_couplers_to_auto_us_df_ARREADY;
  assign S_AXI_rdata[31:0] = s03_couplers_to_auto_us_df_RDATA;
  assign S_AXI_rlast = s03_couplers_to_auto_us_df_RLAST;
  assign S_AXI_rresp[1:0] = s03_couplers_to_auto_us_df_RRESP;
  assign S_AXI_rvalid = s03_couplers_to_auto_us_df_RVALID;
  assign auto_us_df_to_s03_couplers_ARREADY = M_AXI_arready;
  assign auto_us_df_to_s03_couplers_RDATA = M_AXI_rdata[511:0];
  assign auto_us_df_to_s03_couplers_RLAST = M_AXI_rlast;
  assign auto_us_df_to_s03_couplers_RRESP = M_AXI_rresp[1:0];
  assign auto_us_df_to_s03_couplers_RVALID = M_AXI_rvalid;
  assign s03_couplers_to_auto_us_df_ARADDR = S_AXI_araddr[31:0];
  assign s03_couplers_to_auto_us_df_ARBURST = S_AXI_arburst[1:0];
  assign s03_couplers_to_auto_us_df_ARCACHE = S_AXI_arcache[3:0];
  assign s03_couplers_to_auto_us_df_ARLEN = S_AXI_arlen[7:0];
  assign s03_couplers_to_auto_us_df_ARPROT = S_AXI_arprot[2:0];
  assign s03_couplers_to_auto_us_df_ARSIZE = S_AXI_arsize[2:0];
  assign s03_couplers_to_auto_us_df_ARVALID = S_AXI_arvalid;
  assign s03_couplers_to_auto_us_df_RREADY = S_AXI_rready;
  design_1_auto_us_df_3 auto_us_df
       (.m_axi_araddr(auto_us_df_to_s03_couplers_ARADDR),
        .m_axi_arburst(auto_us_df_to_s03_couplers_ARBURST),
        .m_axi_arcache(auto_us_df_to_s03_couplers_ARCACHE),
        .m_axi_arlen(auto_us_df_to_s03_couplers_ARLEN),
        .m_axi_arlock(auto_us_df_to_s03_couplers_ARLOCK),
        .m_axi_arprot(auto_us_df_to_s03_couplers_ARPROT),
        .m_axi_arqos(auto_us_df_to_s03_couplers_ARQOS),
        .m_axi_arready(auto_us_df_to_s03_couplers_ARREADY),
        .m_axi_arsize(auto_us_df_to_s03_couplers_ARSIZE),
        .m_axi_arvalid(auto_us_df_to_s03_couplers_ARVALID),
        .m_axi_rdata(auto_us_df_to_s03_couplers_RDATA),
        .m_axi_rlast(auto_us_df_to_s03_couplers_RLAST),
        .m_axi_rready(auto_us_df_to_s03_couplers_RREADY),
        .m_axi_rresp(auto_us_df_to_s03_couplers_RRESP),
        .m_axi_rvalid(auto_us_df_to_s03_couplers_RVALID),
        .s_axi_aclk(S_ACLK_1),
        .s_axi_araddr(s03_couplers_to_auto_us_df_ARADDR),
        .s_axi_arburst(s03_couplers_to_auto_us_df_ARBURST),
        .s_axi_arcache(s03_couplers_to_auto_us_df_ARCACHE),
        .s_axi_aresetn(S_ARESETN_1),
        .s_axi_arlen(s03_couplers_to_auto_us_df_ARLEN),
        .s_axi_arlock(1'b0),
        .s_axi_arprot(s03_couplers_to_auto_us_df_ARPROT),
        .s_axi_arqos({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(s03_couplers_to_auto_us_df_ARREADY),
        .s_axi_arregion({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arsize(s03_couplers_to_auto_us_df_ARSIZE),
        .s_axi_arvalid(s03_couplers_to_auto_us_df_ARVALID),
        .s_axi_rdata(s03_couplers_to_auto_us_df_RDATA),
        .s_axi_rlast(s03_couplers_to_auto_us_df_RLAST),
        .s_axi_rready(s03_couplers_to_auto_us_df_RREADY),
        .s_axi_rresp(s03_couplers_to_auto_us_df_RRESP),
        .s_axi_rvalid(s03_couplers_to_auto_us_df_RVALID));
endmodule

module s04_couplers_imp_4T8GAF
   (M_ACLK,
    M_ARESETN,
    M_AXI_awaddr,
    M_AXI_awburst,
    M_AXI_awcache,
    M_AXI_awlen,
    M_AXI_awlock,
    M_AXI_awprot,
    M_AXI_awqos,
    M_AXI_awready,
    M_AXI_awsize,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_wdata,
    M_AXI_wlast,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_awaddr,
    S_AXI_awburst,
    S_AXI_awcache,
    S_AXI_awlen,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awsize,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_wdata,
    S_AXI_wlast,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [31:0]M_AXI_awaddr;
  output [1:0]M_AXI_awburst;
  output [3:0]M_AXI_awcache;
  output [7:0]M_AXI_awlen;
  output [0:0]M_AXI_awlock;
  output [2:0]M_AXI_awprot;
  output [3:0]M_AXI_awqos;
  input M_AXI_awready;
  output [2:0]M_AXI_awsize;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  output [511:0]M_AXI_wdata;
  output M_AXI_wlast;
  input M_AXI_wready;
  output [63:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [31:0]S_AXI_awaddr;
  input [1:0]S_AXI_awburst;
  input [3:0]S_AXI_awcache;
  input [7:0]S_AXI_awlen;
  input [2:0]S_AXI_awprot;
  output S_AXI_awready;
  input [2:0]S_AXI_awsize;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  input [31:0]S_AXI_wdata;
  input S_AXI_wlast;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire M_ACLK_1;
  wire [0:0]M_ARESETN_1;
  wire S_ACLK_1;
  wire [0:0]S_ARESETN_1;
  wire [31:0]auto_us_df_to_s04_couplers_AWADDR;
  wire [1:0]auto_us_df_to_s04_couplers_AWBURST;
  wire [3:0]auto_us_df_to_s04_couplers_AWCACHE;
  wire [7:0]auto_us_df_to_s04_couplers_AWLEN;
  wire [0:0]auto_us_df_to_s04_couplers_AWLOCK;
  wire [2:0]auto_us_df_to_s04_couplers_AWPROT;
  wire [3:0]auto_us_df_to_s04_couplers_AWQOS;
  wire auto_us_df_to_s04_couplers_AWREADY;
  wire [2:0]auto_us_df_to_s04_couplers_AWSIZE;
  wire auto_us_df_to_s04_couplers_AWVALID;
  wire auto_us_df_to_s04_couplers_BREADY;
  wire [1:0]auto_us_df_to_s04_couplers_BRESP;
  wire auto_us_df_to_s04_couplers_BVALID;
  wire [511:0]auto_us_df_to_s04_couplers_WDATA;
  wire auto_us_df_to_s04_couplers_WLAST;
  wire auto_us_df_to_s04_couplers_WREADY;
  wire [63:0]auto_us_df_to_s04_couplers_WSTRB;
  wire auto_us_df_to_s04_couplers_WVALID;
  wire [31:0]s04_couplers_to_auto_us_df_AWADDR;
  wire [1:0]s04_couplers_to_auto_us_df_AWBURST;
  wire [3:0]s04_couplers_to_auto_us_df_AWCACHE;
  wire [7:0]s04_couplers_to_auto_us_df_AWLEN;
  wire [2:0]s04_couplers_to_auto_us_df_AWPROT;
  wire s04_couplers_to_auto_us_df_AWREADY;
  wire [2:0]s04_couplers_to_auto_us_df_AWSIZE;
  wire s04_couplers_to_auto_us_df_AWVALID;
  wire s04_couplers_to_auto_us_df_BREADY;
  wire [1:0]s04_couplers_to_auto_us_df_BRESP;
  wire s04_couplers_to_auto_us_df_BVALID;
  wire [31:0]s04_couplers_to_auto_us_df_WDATA;
  wire s04_couplers_to_auto_us_df_WLAST;
  wire s04_couplers_to_auto_us_df_WREADY;
  wire [3:0]s04_couplers_to_auto_us_df_WSTRB;
  wire s04_couplers_to_auto_us_df_WVALID;

  assign M_ACLK_1 = M_ACLK;
  assign M_ARESETN_1 = M_ARESETN[0];
  assign M_AXI_awaddr[31:0] = auto_us_df_to_s04_couplers_AWADDR;
  assign M_AXI_awburst[1:0] = auto_us_df_to_s04_couplers_AWBURST;
  assign M_AXI_awcache[3:0] = auto_us_df_to_s04_couplers_AWCACHE;
  assign M_AXI_awlen[7:0] = auto_us_df_to_s04_couplers_AWLEN;
  assign M_AXI_awlock[0] = auto_us_df_to_s04_couplers_AWLOCK;
  assign M_AXI_awprot[2:0] = auto_us_df_to_s04_couplers_AWPROT;
  assign M_AXI_awqos[3:0] = auto_us_df_to_s04_couplers_AWQOS;
  assign M_AXI_awsize[2:0] = auto_us_df_to_s04_couplers_AWSIZE;
  assign M_AXI_awvalid = auto_us_df_to_s04_couplers_AWVALID;
  assign M_AXI_bready = auto_us_df_to_s04_couplers_BREADY;
  assign M_AXI_wdata[511:0] = auto_us_df_to_s04_couplers_WDATA;
  assign M_AXI_wlast = auto_us_df_to_s04_couplers_WLAST;
  assign M_AXI_wstrb[63:0] = auto_us_df_to_s04_couplers_WSTRB;
  assign M_AXI_wvalid = auto_us_df_to_s04_couplers_WVALID;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN[0];
  assign S_AXI_awready = s04_couplers_to_auto_us_df_AWREADY;
  assign S_AXI_bresp[1:0] = s04_couplers_to_auto_us_df_BRESP;
  assign S_AXI_bvalid = s04_couplers_to_auto_us_df_BVALID;
  assign S_AXI_wready = s04_couplers_to_auto_us_df_WREADY;
  assign auto_us_df_to_s04_couplers_AWREADY = M_AXI_awready;
  assign auto_us_df_to_s04_couplers_BRESP = M_AXI_bresp[1:0];
  assign auto_us_df_to_s04_couplers_BVALID = M_AXI_bvalid;
  assign auto_us_df_to_s04_couplers_WREADY = M_AXI_wready;
  assign s04_couplers_to_auto_us_df_AWADDR = S_AXI_awaddr[31:0];
  assign s04_couplers_to_auto_us_df_AWBURST = S_AXI_awburst[1:0];
  assign s04_couplers_to_auto_us_df_AWCACHE = S_AXI_awcache[3:0];
  assign s04_couplers_to_auto_us_df_AWLEN = S_AXI_awlen[7:0];
  assign s04_couplers_to_auto_us_df_AWPROT = S_AXI_awprot[2:0];
  assign s04_couplers_to_auto_us_df_AWSIZE = S_AXI_awsize[2:0];
  assign s04_couplers_to_auto_us_df_AWVALID = S_AXI_awvalid;
  assign s04_couplers_to_auto_us_df_BREADY = S_AXI_bready;
  assign s04_couplers_to_auto_us_df_WDATA = S_AXI_wdata[31:0];
  assign s04_couplers_to_auto_us_df_WLAST = S_AXI_wlast;
  assign s04_couplers_to_auto_us_df_WSTRB = S_AXI_wstrb[3:0];
  assign s04_couplers_to_auto_us_df_WVALID = S_AXI_wvalid;
  design_1_auto_us_df_4 auto_us_df
       (.m_axi_awaddr(auto_us_df_to_s04_couplers_AWADDR),
        .m_axi_awburst(auto_us_df_to_s04_couplers_AWBURST),
        .m_axi_awcache(auto_us_df_to_s04_couplers_AWCACHE),
        .m_axi_awlen(auto_us_df_to_s04_couplers_AWLEN),
        .m_axi_awlock(auto_us_df_to_s04_couplers_AWLOCK),
        .m_axi_awprot(auto_us_df_to_s04_couplers_AWPROT),
        .m_axi_awqos(auto_us_df_to_s04_couplers_AWQOS),
        .m_axi_awready(auto_us_df_to_s04_couplers_AWREADY),
        .m_axi_awsize(auto_us_df_to_s04_couplers_AWSIZE),
        .m_axi_awvalid(auto_us_df_to_s04_couplers_AWVALID),
        .m_axi_bready(auto_us_df_to_s04_couplers_BREADY),
        .m_axi_bresp(auto_us_df_to_s04_couplers_BRESP),
        .m_axi_bvalid(auto_us_df_to_s04_couplers_BVALID),
        .m_axi_wdata(auto_us_df_to_s04_couplers_WDATA),
        .m_axi_wlast(auto_us_df_to_s04_couplers_WLAST),
        .m_axi_wready(auto_us_df_to_s04_couplers_WREADY),
        .m_axi_wstrb(auto_us_df_to_s04_couplers_WSTRB),
        .m_axi_wvalid(auto_us_df_to_s04_couplers_WVALID),
        .s_axi_aclk(S_ACLK_1),
        .s_axi_aresetn(S_ARESETN_1),
        .s_axi_awaddr(s04_couplers_to_auto_us_df_AWADDR),
        .s_axi_awburst(s04_couplers_to_auto_us_df_AWBURST),
        .s_axi_awcache(s04_couplers_to_auto_us_df_AWCACHE),
        .s_axi_awlen(s04_couplers_to_auto_us_df_AWLEN),
        .s_axi_awlock(1'b0),
        .s_axi_awprot(s04_couplers_to_auto_us_df_AWPROT),
        .s_axi_awqos({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(s04_couplers_to_auto_us_df_AWREADY),
        .s_axi_awregion({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awsize(s04_couplers_to_auto_us_df_AWSIZE),
        .s_axi_awvalid(s04_couplers_to_auto_us_df_AWVALID),
        .s_axi_bready(s04_couplers_to_auto_us_df_BREADY),
        .s_axi_bresp(s04_couplers_to_auto_us_df_BRESP),
        .s_axi_bvalid(s04_couplers_to_auto_us_df_BVALID),
        .s_axi_wdata(s04_couplers_to_auto_us_df_WDATA),
        .s_axi_wlast(s04_couplers_to_auto_us_df_WLAST),
        .s_axi_wready(s04_couplers_to_auto_us_df_WREADY),
        .s_axi_wstrb(s04_couplers_to_auto_us_df_WSTRB),
        .s_axi_wvalid(s04_couplers_to_auto_us_df_WVALID));
endmodule

module s05_couplers_imp_1YHCGIE
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arburst,
    M_AXI_arcache,
    M_AXI_arlen,
    M_AXI_arlock,
    M_AXI_arprot,
    M_AXI_arqos,
    M_AXI_arready,
    M_AXI_arsize,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awburst,
    M_AXI_awcache,
    M_AXI_awlen,
    M_AXI_awlock,
    M_AXI_awprot,
    M_AXI_awqos,
    M_AXI_awready,
    M_AXI_awsize,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rlast,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wlast,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arburst,
    S_AXI_arcache,
    S_AXI_arlen,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arsize,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awburst,
    S_AXI_awcache,
    S_AXI_awlen,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awsize,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rlast,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wlast,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [1:0]M_AXI_arburst;
  output [3:0]M_AXI_arcache;
  output [7:0]M_AXI_arlen;
  output [0:0]M_AXI_arlock;
  output [2:0]M_AXI_arprot;
  output [3:0]M_AXI_arqos;
  input M_AXI_arready;
  output [2:0]M_AXI_arsize;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [1:0]M_AXI_awburst;
  output [3:0]M_AXI_awcache;
  output [7:0]M_AXI_awlen;
  output [0:0]M_AXI_awlock;
  output [2:0]M_AXI_awprot;
  output [3:0]M_AXI_awqos;
  input M_AXI_awready;
  output [2:0]M_AXI_awsize;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [511:0]M_AXI_rdata;
  input M_AXI_rlast;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [511:0]M_AXI_wdata;
  output M_AXI_wlast;
  input M_AXI_wready;
  output [63:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [1:0]S_AXI_arburst;
  input [3:0]S_AXI_arcache;
  input [7:0]S_AXI_arlen;
  input [2:0]S_AXI_arprot;
  output S_AXI_arready;
  input [2:0]S_AXI_arsize;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [1:0]S_AXI_awburst;
  input [3:0]S_AXI_awcache;
  input [7:0]S_AXI_awlen;
  input [2:0]S_AXI_awprot;
  output S_AXI_awready;
  input [2:0]S_AXI_awsize;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  output S_AXI_rlast;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  input S_AXI_wlast;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire M_ACLK_1;
  wire [0:0]M_ARESETN_1;
  wire S_ACLK_1;
  wire [0:0]S_ARESETN_1;
  wire [31:0]auto_us_df_to_s05_couplers_ARADDR;
  wire [1:0]auto_us_df_to_s05_couplers_ARBURST;
  wire [3:0]auto_us_df_to_s05_couplers_ARCACHE;
  wire [7:0]auto_us_df_to_s05_couplers_ARLEN;
  wire [0:0]auto_us_df_to_s05_couplers_ARLOCK;
  wire [2:0]auto_us_df_to_s05_couplers_ARPROT;
  wire [3:0]auto_us_df_to_s05_couplers_ARQOS;
  wire auto_us_df_to_s05_couplers_ARREADY;
  wire [2:0]auto_us_df_to_s05_couplers_ARSIZE;
  wire auto_us_df_to_s05_couplers_ARVALID;
  wire [31:0]auto_us_df_to_s05_couplers_AWADDR;
  wire [1:0]auto_us_df_to_s05_couplers_AWBURST;
  wire [3:0]auto_us_df_to_s05_couplers_AWCACHE;
  wire [7:0]auto_us_df_to_s05_couplers_AWLEN;
  wire [0:0]auto_us_df_to_s05_couplers_AWLOCK;
  wire [2:0]auto_us_df_to_s05_couplers_AWPROT;
  wire [3:0]auto_us_df_to_s05_couplers_AWQOS;
  wire auto_us_df_to_s05_couplers_AWREADY;
  wire [2:0]auto_us_df_to_s05_couplers_AWSIZE;
  wire auto_us_df_to_s05_couplers_AWVALID;
  wire auto_us_df_to_s05_couplers_BREADY;
  wire [1:0]auto_us_df_to_s05_couplers_BRESP;
  wire auto_us_df_to_s05_couplers_BVALID;
  wire [511:0]auto_us_df_to_s05_couplers_RDATA;
  wire auto_us_df_to_s05_couplers_RLAST;
  wire auto_us_df_to_s05_couplers_RREADY;
  wire [1:0]auto_us_df_to_s05_couplers_RRESP;
  wire auto_us_df_to_s05_couplers_RVALID;
  wire [511:0]auto_us_df_to_s05_couplers_WDATA;
  wire auto_us_df_to_s05_couplers_WLAST;
  wire auto_us_df_to_s05_couplers_WREADY;
  wire [63:0]auto_us_df_to_s05_couplers_WSTRB;
  wire auto_us_df_to_s05_couplers_WVALID;
  wire [31:0]s05_couplers_to_auto_us_df_ARADDR;
  wire [1:0]s05_couplers_to_auto_us_df_ARBURST;
  wire [3:0]s05_couplers_to_auto_us_df_ARCACHE;
  wire [7:0]s05_couplers_to_auto_us_df_ARLEN;
  wire [2:0]s05_couplers_to_auto_us_df_ARPROT;
  wire s05_couplers_to_auto_us_df_ARREADY;
  wire [2:0]s05_couplers_to_auto_us_df_ARSIZE;
  wire s05_couplers_to_auto_us_df_ARVALID;
  wire [31:0]s05_couplers_to_auto_us_df_AWADDR;
  wire [1:0]s05_couplers_to_auto_us_df_AWBURST;
  wire [3:0]s05_couplers_to_auto_us_df_AWCACHE;
  wire [7:0]s05_couplers_to_auto_us_df_AWLEN;
  wire [2:0]s05_couplers_to_auto_us_df_AWPROT;
  wire s05_couplers_to_auto_us_df_AWREADY;
  wire [2:0]s05_couplers_to_auto_us_df_AWSIZE;
  wire s05_couplers_to_auto_us_df_AWVALID;
  wire s05_couplers_to_auto_us_df_BREADY;
  wire [1:0]s05_couplers_to_auto_us_df_BRESP;
  wire s05_couplers_to_auto_us_df_BVALID;
  wire [31:0]s05_couplers_to_auto_us_df_RDATA;
  wire s05_couplers_to_auto_us_df_RLAST;
  wire s05_couplers_to_auto_us_df_RREADY;
  wire [1:0]s05_couplers_to_auto_us_df_RRESP;
  wire s05_couplers_to_auto_us_df_RVALID;
  wire [31:0]s05_couplers_to_auto_us_df_WDATA;
  wire s05_couplers_to_auto_us_df_WLAST;
  wire s05_couplers_to_auto_us_df_WREADY;
  wire [3:0]s05_couplers_to_auto_us_df_WSTRB;
  wire s05_couplers_to_auto_us_df_WVALID;

  assign M_ACLK_1 = M_ACLK;
  assign M_ARESETN_1 = M_ARESETN[0];
  assign M_AXI_araddr[31:0] = auto_us_df_to_s05_couplers_ARADDR;
  assign M_AXI_arburst[1:0] = auto_us_df_to_s05_couplers_ARBURST;
  assign M_AXI_arcache[3:0] = auto_us_df_to_s05_couplers_ARCACHE;
  assign M_AXI_arlen[7:0] = auto_us_df_to_s05_couplers_ARLEN;
  assign M_AXI_arlock[0] = auto_us_df_to_s05_couplers_ARLOCK;
  assign M_AXI_arprot[2:0] = auto_us_df_to_s05_couplers_ARPROT;
  assign M_AXI_arqos[3:0] = auto_us_df_to_s05_couplers_ARQOS;
  assign M_AXI_arsize[2:0] = auto_us_df_to_s05_couplers_ARSIZE;
  assign M_AXI_arvalid = auto_us_df_to_s05_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = auto_us_df_to_s05_couplers_AWADDR;
  assign M_AXI_awburst[1:0] = auto_us_df_to_s05_couplers_AWBURST;
  assign M_AXI_awcache[3:0] = auto_us_df_to_s05_couplers_AWCACHE;
  assign M_AXI_awlen[7:0] = auto_us_df_to_s05_couplers_AWLEN;
  assign M_AXI_awlock[0] = auto_us_df_to_s05_couplers_AWLOCK;
  assign M_AXI_awprot[2:0] = auto_us_df_to_s05_couplers_AWPROT;
  assign M_AXI_awqos[3:0] = auto_us_df_to_s05_couplers_AWQOS;
  assign M_AXI_awsize[2:0] = auto_us_df_to_s05_couplers_AWSIZE;
  assign M_AXI_awvalid = auto_us_df_to_s05_couplers_AWVALID;
  assign M_AXI_bready = auto_us_df_to_s05_couplers_BREADY;
  assign M_AXI_rready = auto_us_df_to_s05_couplers_RREADY;
  assign M_AXI_wdata[511:0] = auto_us_df_to_s05_couplers_WDATA;
  assign M_AXI_wlast = auto_us_df_to_s05_couplers_WLAST;
  assign M_AXI_wstrb[63:0] = auto_us_df_to_s05_couplers_WSTRB;
  assign M_AXI_wvalid = auto_us_df_to_s05_couplers_WVALID;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN[0];
  assign S_AXI_arready = s05_couplers_to_auto_us_df_ARREADY;
  assign S_AXI_awready = s05_couplers_to_auto_us_df_AWREADY;
  assign S_AXI_bresp[1:0] = s05_couplers_to_auto_us_df_BRESP;
  assign S_AXI_bvalid = s05_couplers_to_auto_us_df_BVALID;
  assign S_AXI_rdata[31:0] = s05_couplers_to_auto_us_df_RDATA;
  assign S_AXI_rlast = s05_couplers_to_auto_us_df_RLAST;
  assign S_AXI_rresp[1:0] = s05_couplers_to_auto_us_df_RRESP;
  assign S_AXI_rvalid = s05_couplers_to_auto_us_df_RVALID;
  assign S_AXI_wready = s05_couplers_to_auto_us_df_WREADY;
  assign auto_us_df_to_s05_couplers_ARREADY = M_AXI_arready;
  assign auto_us_df_to_s05_couplers_AWREADY = M_AXI_awready;
  assign auto_us_df_to_s05_couplers_BRESP = M_AXI_bresp[1:0];
  assign auto_us_df_to_s05_couplers_BVALID = M_AXI_bvalid;
  assign auto_us_df_to_s05_couplers_RDATA = M_AXI_rdata[511:0];
  assign auto_us_df_to_s05_couplers_RLAST = M_AXI_rlast;
  assign auto_us_df_to_s05_couplers_RRESP = M_AXI_rresp[1:0];
  assign auto_us_df_to_s05_couplers_RVALID = M_AXI_rvalid;
  assign auto_us_df_to_s05_couplers_WREADY = M_AXI_wready;
  assign s05_couplers_to_auto_us_df_ARADDR = S_AXI_araddr[31:0];
  assign s05_couplers_to_auto_us_df_ARBURST = S_AXI_arburst[1:0];
  assign s05_couplers_to_auto_us_df_ARCACHE = S_AXI_arcache[3:0];
  assign s05_couplers_to_auto_us_df_ARLEN = S_AXI_arlen[7:0];
  assign s05_couplers_to_auto_us_df_ARPROT = S_AXI_arprot[2:0];
  assign s05_couplers_to_auto_us_df_ARSIZE = S_AXI_arsize[2:0];
  assign s05_couplers_to_auto_us_df_ARVALID = S_AXI_arvalid;
  assign s05_couplers_to_auto_us_df_AWADDR = S_AXI_awaddr[31:0];
  assign s05_couplers_to_auto_us_df_AWBURST = S_AXI_awburst[1:0];
  assign s05_couplers_to_auto_us_df_AWCACHE = S_AXI_awcache[3:0];
  assign s05_couplers_to_auto_us_df_AWLEN = S_AXI_awlen[7:0];
  assign s05_couplers_to_auto_us_df_AWPROT = S_AXI_awprot[2:0];
  assign s05_couplers_to_auto_us_df_AWSIZE = S_AXI_awsize[2:0];
  assign s05_couplers_to_auto_us_df_AWVALID = S_AXI_awvalid;
  assign s05_couplers_to_auto_us_df_BREADY = S_AXI_bready;
  assign s05_couplers_to_auto_us_df_RREADY = S_AXI_rready;
  assign s05_couplers_to_auto_us_df_WDATA = S_AXI_wdata[31:0];
  assign s05_couplers_to_auto_us_df_WLAST = S_AXI_wlast;
  assign s05_couplers_to_auto_us_df_WSTRB = S_AXI_wstrb[3:0];
  assign s05_couplers_to_auto_us_df_WVALID = S_AXI_wvalid;
  design_1_auto_us_df_5 auto_us_df
       (.m_axi_araddr(auto_us_df_to_s05_couplers_ARADDR),
        .m_axi_arburst(auto_us_df_to_s05_couplers_ARBURST),
        .m_axi_arcache(auto_us_df_to_s05_couplers_ARCACHE),
        .m_axi_arlen(auto_us_df_to_s05_couplers_ARLEN),
        .m_axi_arlock(auto_us_df_to_s05_couplers_ARLOCK),
        .m_axi_arprot(auto_us_df_to_s05_couplers_ARPROT),
        .m_axi_arqos(auto_us_df_to_s05_couplers_ARQOS),
        .m_axi_arready(auto_us_df_to_s05_couplers_ARREADY),
        .m_axi_arsize(auto_us_df_to_s05_couplers_ARSIZE),
        .m_axi_arvalid(auto_us_df_to_s05_couplers_ARVALID),
        .m_axi_awaddr(auto_us_df_to_s05_couplers_AWADDR),
        .m_axi_awburst(auto_us_df_to_s05_couplers_AWBURST),
        .m_axi_awcache(auto_us_df_to_s05_couplers_AWCACHE),
        .m_axi_awlen(auto_us_df_to_s05_couplers_AWLEN),
        .m_axi_awlock(auto_us_df_to_s05_couplers_AWLOCK),
        .m_axi_awprot(auto_us_df_to_s05_couplers_AWPROT),
        .m_axi_awqos(auto_us_df_to_s05_couplers_AWQOS),
        .m_axi_awready(auto_us_df_to_s05_couplers_AWREADY),
        .m_axi_awsize(auto_us_df_to_s05_couplers_AWSIZE),
        .m_axi_awvalid(auto_us_df_to_s05_couplers_AWVALID),
        .m_axi_bready(auto_us_df_to_s05_couplers_BREADY),
        .m_axi_bresp(auto_us_df_to_s05_couplers_BRESP),
        .m_axi_bvalid(auto_us_df_to_s05_couplers_BVALID),
        .m_axi_rdata(auto_us_df_to_s05_couplers_RDATA),
        .m_axi_rlast(auto_us_df_to_s05_couplers_RLAST),
        .m_axi_rready(auto_us_df_to_s05_couplers_RREADY),
        .m_axi_rresp(auto_us_df_to_s05_couplers_RRESP),
        .m_axi_rvalid(auto_us_df_to_s05_couplers_RVALID),
        .m_axi_wdata(auto_us_df_to_s05_couplers_WDATA),
        .m_axi_wlast(auto_us_df_to_s05_couplers_WLAST),
        .m_axi_wready(auto_us_df_to_s05_couplers_WREADY),
        .m_axi_wstrb(auto_us_df_to_s05_couplers_WSTRB),
        .m_axi_wvalid(auto_us_df_to_s05_couplers_WVALID),
        .s_axi_aclk(S_ACLK_1),
        .s_axi_araddr(s05_couplers_to_auto_us_df_ARADDR),
        .s_axi_arburst(s05_couplers_to_auto_us_df_ARBURST),
        .s_axi_arcache(s05_couplers_to_auto_us_df_ARCACHE),
        .s_axi_aresetn(S_ARESETN_1),
        .s_axi_arlen(s05_couplers_to_auto_us_df_ARLEN),
        .s_axi_arlock(1'b0),
        .s_axi_arprot(s05_couplers_to_auto_us_df_ARPROT),
        .s_axi_arqos({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(s05_couplers_to_auto_us_df_ARREADY),
        .s_axi_arregion({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arsize(s05_couplers_to_auto_us_df_ARSIZE),
        .s_axi_arvalid(s05_couplers_to_auto_us_df_ARVALID),
        .s_axi_awaddr(s05_couplers_to_auto_us_df_AWADDR),
        .s_axi_awburst(s05_couplers_to_auto_us_df_AWBURST),
        .s_axi_awcache(s05_couplers_to_auto_us_df_AWCACHE),
        .s_axi_awlen(s05_couplers_to_auto_us_df_AWLEN),
        .s_axi_awlock(1'b0),
        .s_axi_awprot(s05_couplers_to_auto_us_df_AWPROT),
        .s_axi_awqos({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(s05_couplers_to_auto_us_df_AWREADY),
        .s_axi_awregion({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awsize(s05_couplers_to_auto_us_df_AWSIZE),
        .s_axi_awvalid(s05_couplers_to_auto_us_df_AWVALID),
        .s_axi_bready(s05_couplers_to_auto_us_df_BREADY),
        .s_axi_bresp(s05_couplers_to_auto_us_df_BRESP),
        .s_axi_bvalid(s05_couplers_to_auto_us_df_BVALID),
        .s_axi_rdata(s05_couplers_to_auto_us_df_RDATA),
        .s_axi_rlast(s05_couplers_to_auto_us_df_RLAST),
        .s_axi_rready(s05_couplers_to_auto_us_df_RREADY),
        .s_axi_rresp(s05_couplers_to_auto_us_df_RRESP),
        .s_axi_rvalid(s05_couplers_to_auto_us_df_RVALID),
        .s_axi_wdata(s05_couplers_to_auto_us_df_WDATA),
        .s_axi_wlast(s05_couplers_to_auto_us_df_WLAST),
        .s_axi_wready(s05_couplers_to_auto_us_df_WREADY),
        .s_axi_wstrb(s05_couplers_to_auto_us_df_WSTRB),
        .s_axi_wvalid(s05_couplers_to_auto_us_df_WVALID));
endmodule

module s06_couplers_imp_5OWWZ8
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arburst,
    M_AXI_arcache,
    M_AXI_arlen,
    M_AXI_arlock,
    M_AXI_arprot,
    M_AXI_arqos,
    M_AXI_arready,
    M_AXI_arsize,
    M_AXI_arvalid,
    M_AXI_rdata,
    M_AXI_rlast,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arburst,
    S_AXI_arcache,
    S_AXI_arlen,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arsize,
    S_AXI_arvalid,
    S_AXI_rdata,
    S_AXI_rlast,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [1:0]M_AXI_arburst;
  output [3:0]M_AXI_arcache;
  output [7:0]M_AXI_arlen;
  output [0:0]M_AXI_arlock;
  output [2:0]M_AXI_arprot;
  output [3:0]M_AXI_arqos;
  input M_AXI_arready;
  output [2:0]M_AXI_arsize;
  output M_AXI_arvalid;
  input [511:0]M_AXI_rdata;
  input M_AXI_rlast;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [1:0]S_AXI_arburst;
  input [3:0]S_AXI_arcache;
  input [7:0]S_AXI_arlen;
  input [2:0]S_AXI_arprot;
  output S_AXI_arready;
  input [2:0]S_AXI_arsize;
  input S_AXI_arvalid;
  output [31:0]S_AXI_rdata;
  output S_AXI_rlast;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;

  wire M_ACLK_1;
  wire [0:0]M_ARESETN_1;
  wire S_ACLK_1;
  wire [0:0]S_ARESETN_1;
  wire [31:0]auto_us_df_to_s06_couplers_ARADDR;
  wire [1:0]auto_us_df_to_s06_couplers_ARBURST;
  wire [3:0]auto_us_df_to_s06_couplers_ARCACHE;
  wire [7:0]auto_us_df_to_s06_couplers_ARLEN;
  wire [0:0]auto_us_df_to_s06_couplers_ARLOCK;
  wire [2:0]auto_us_df_to_s06_couplers_ARPROT;
  wire [3:0]auto_us_df_to_s06_couplers_ARQOS;
  wire auto_us_df_to_s06_couplers_ARREADY;
  wire [2:0]auto_us_df_to_s06_couplers_ARSIZE;
  wire auto_us_df_to_s06_couplers_ARVALID;
  wire [511:0]auto_us_df_to_s06_couplers_RDATA;
  wire auto_us_df_to_s06_couplers_RLAST;
  wire auto_us_df_to_s06_couplers_RREADY;
  wire [1:0]auto_us_df_to_s06_couplers_RRESP;
  wire auto_us_df_to_s06_couplers_RVALID;
  wire [31:0]s06_couplers_to_auto_us_df_ARADDR;
  wire [1:0]s06_couplers_to_auto_us_df_ARBURST;
  wire [3:0]s06_couplers_to_auto_us_df_ARCACHE;
  wire [7:0]s06_couplers_to_auto_us_df_ARLEN;
  wire [2:0]s06_couplers_to_auto_us_df_ARPROT;
  wire s06_couplers_to_auto_us_df_ARREADY;
  wire [2:0]s06_couplers_to_auto_us_df_ARSIZE;
  wire s06_couplers_to_auto_us_df_ARVALID;
  wire [31:0]s06_couplers_to_auto_us_df_RDATA;
  wire s06_couplers_to_auto_us_df_RLAST;
  wire s06_couplers_to_auto_us_df_RREADY;
  wire [1:0]s06_couplers_to_auto_us_df_RRESP;
  wire s06_couplers_to_auto_us_df_RVALID;

  assign M_ACLK_1 = M_ACLK;
  assign M_ARESETN_1 = M_ARESETN[0];
  assign M_AXI_araddr[31:0] = auto_us_df_to_s06_couplers_ARADDR;
  assign M_AXI_arburst[1:0] = auto_us_df_to_s06_couplers_ARBURST;
  assign M_AXI_arcache[3:0] = auto_us_df_to_s06_couplers_ARCACHE;
  assign M_AXI_arlen[7:0] = auto_us_df_to_s06_couplers_ARLEN;
  assign M_AXI_arlock[0] = auto_us_df_to_s06_couplers_ARLOCK;
  assign M_AXI_arprot[2:0] = auto_us_df_to_s06_couplers_ARPROT;
  assign M_AXI_arqos[3:0] = auto_us_df_to_s06_couplers_ARQOS;
  assign M_AXI_arsize[2:0] = auto_us_df_to_s06_couplers_ARSIZE;
  assign M_AXI_arvalid = auto_us_df_to_s06_couplers_ARVALID;
  assign M_AXI_rready = auto_us_df_to_s06_couplers_RREADY;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN[0];
  assign S_AXI_arready = s06_couplers_to_auto_us_df_ARREADY;
  assign S_AXI_rdata[31:0] = s06_couplers_to_auto_us_df_RDATA;
  assign S_AXI_rlast = s06_couplers_to_auto_us_df_RLAST;
  assign S_AXI_rresp[1:0] = s06_couplers_to_auto_us_df_RRESP;
  assign S_AXI_rvalid = s06_couplers_to_auto_us_df_RVALID;
  assign auto_us_df_to_s06_couplers_ARREADY = M_AXI_arready;
  assign auto_us_df_to_s06_couplers_RDATA = M_AXI_rdata[511:0];
  assign auto_us_df_to_s06_couplers_RLAST = M_AXI_rlast;
  assign auto_us_df_to_s06_couplers_RRESP = M_AXI_rresp[1:0];
  assign auto_us_df_to_s06_couplers_RVALID = M_AXI_rvalid;
  assign s06_couplers_to_auto_us_df_ARADDR = S_AXI_araddr[31:0];
  assign s06_couplers_to_auto_us_df_ARBURST = S_AXI_arburst[1:0];
  assign s06_couplers_to_auto_us_df_ARCACHE = S_AXI_arcache[3:0];
  assign s06_couplers_to_auto_us_df_ARLEN = S_AXI_arlen[7:0];
  assign s06_couplers_to_auto_us_df_ARPROT = S_AXI_arprot[2:0];
  assign s06_couplers_to_auto_us_df_ARSIZE = S_AXI_arsize[2:0];
  assign s06_couplers_to_auto_us_df_ARVALID = S_AXI_arvalid;
  assign s06_couplers_to_auto_us_df_RREADY = S_AXI_rready;
  design_1_auto_us_df_6 auto_us_df
       (.m_axi_araddr(auto_us_df_to_s06_couplers_ARADDR),
        .m_axi_arburst(auto_us_df_to_s06_couplers_ARBURST),
        .m_axi_arcache(auto_us_df_to_s06_couplers_ARCACHE),
        .m_axi_arlen(auto_us_df_to_s06_couplers_ARLEN),
        .m_axi_arlock(auto_us_df_to_s06_couplers_ARLOCK),
        .m_axi_arprot(auto_us_df_to_s06_couplers_ARPROT),
        .m_axi_arqos(auto_us_df_to_s06_couplers_ARQOS),
        .m_axi_arready(auto_us_df_to_s06_couplers_ARREADY),
        .m_axi_arsize(auto_us_df_to_s06_couplers_ARSIZE),
        .m_axi_arvalid(auto_us_df_to_s06_couplers_ARVALID),
        .m_axi_rdata(auto_us_df_to_s06_couplers_RDATA),
        .m_axi_rlast(auto_us_df_to_s06_couplers_RLAST),
        .m_axi_rready(auto_us_df_to_s06_couplers_RREADY),
        .m_axi_rresp(auto_us_df_to_s06_couplers_RRESP),
        .m_axi_rvalid(auto_us_df_to_s06_couplers_RVALID),
        .s_axi_aclk(S_ACLK_1),
        .s_axi_araddr(s06_couplers_to_auto_us_df_ARADDR),
        .s_axi_arburst(s06_couplers_to_auto_us_df_ARBURST),
        .s_axi_arcache(s06_couplers_to_auto_us_df_ARCACHE),
        .s_axi_aresetn(S_ARESETN_1),
        .s_axi_arlen(s06_couplers_to_auto_us_df_ARLEN),
        .s_axi_arlock(1'b0),
        .s_axi_arprot(s06_couplers_to_auto_us_df_ARPROT),
        .s_axi_arqos({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(s06_couplers_to_auto_us_df_ARREADY),
        .s_axi_arregion({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arsize(s06_couplers_to_auto_us_df_ARSIZE),
        .s_axi_arvalid(s06_couplers_to_auto_us_df_ARVALID),
        .s_axi_rdata(s06_couplers_to_auto_us_df_RDATA),
        .s_axi_rlast(s06_couplers_to_auto_us_df_RLAST),
        .s_axi_rready(s06_couplers_to_auto_us_df_RREADY),
        .s_axi_rresp(s06_couplers_to_auto_us_df_RRESP),
        .s_axi_rvalid(s06_couplers_to_auto_us_df_RVALID));
endmodule

module s07_couplers_imp_1XVBQ51
   (M_ACLK,
    M_ARESETN,
    M_AXI_awaddr,
    M_AXI_awburst,
    M_AXI_awcache,
    M_AXI_awlen,
    M_AXI_awlock,
    M_AXI_awprot,
    M_AXI_awqos,
    M_AXI_awready,
    M_AXI_awsize,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_wdata,
    M_AXI_wlast,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_awaddr,
    S_AXI_awburst,
    S_AXI_awcache,
    S_AXI_awlen,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awsize,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_wdata,
    S_AXI_wlast,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [31:0]M_AXI_awaddr;
  output [1:0]M_AXI_awburst;
  output [3:0]M_AXI_awcache;
  output [7:0]M_AXI_awlen;
  output [0:0]M_AXI_awlock;
  output [2:0]M_AXI_awprot;
  output [3:0]M_AXI_awqos;
  input M_AXI_awready;
  output [2:0]M_AXI_awsize;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  output [511:0]M_AXI_wdata;
  output M_AXI_wlast;
  input M_AXI_wready;
  output [63:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [31:0]S_AXI_awaddr;
  input [1:0]S_AXI_awburst;
  input [3:0]S_AXI_awcache;
  input [7:0]S_AXI_awlen;
  input [2:0]S_AXI_awprot;
  output S_AXI_awready;
  input [2:0]S_AXI_awsize;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  input [31:0]S_AXI_wdata;
  input S_AXI_wlast;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire M_ACLK_1;
  wire [0:0]M_ARESETN_1;
  wire S_ACLK_1;
  wire [0:0]S_ARESETN_1;
  wire [31:0]auto_us_df_to_s07_couplers_AWADDR;
  wire [1:0]auto_us_df_to_s07_couplers_AWBURST;
  wire [3:0]auto_us_df_to_s07_couplers_AWCACHE;
  wire [7:0]auto_us_df_to_s07_couplers_AWLEN;
  wire [0:0]auto_us_df_to_s07_couplers_AWLOCK;
  wire [2:0]auto_us_df_to_s07_couplers_AWPROT;
  wire [3:0]auto_us_df_to_s07_couplers_AWQOS;
  wire auto_us_df_to_s07_couplers_AWREADY;
  wire [2:0]auto_us_df_to_s07_couplers_AWSIZE;
  wire auto_us_df_to_s07_couplers_AWVALID;
  wire auto_us_df_to_s07_couplers_BREADY;
  wire [1:0]auto_us_df_to_s07_couplers_BRESP;
  wire auto_us_df_to_s07_couplers_BVALID;
  wire [511:0]auto_us_df_to_s07_couplers_WDATA;
  wire auto_us_df_to_s07_couplers_WLAST;
  wire auto_us_df_to_s07_couplers_WREADY;
  wire [63:0]auto_us_df_to_s07_couplers_WSTRB;
  wire auto_us_df_to_s07_couplers_WVALID;
  wire [31:0]s07_couplers_to_auto_us_df_AWADDR;
  wire [1:0]s07_couplers_to_auto_us_df_AWBURST;
  wire [3:0]s07_couplers_to_auto_us_df_AWCACHE;
  wire [7:0]s07_couplers_to_auto_us_df_AWLEN;
  wire [2:0]s07_couplers_to_auto_us_df_AWPROT;
  wire s07_couplers_to_auto_us_df_AWREADY;
  wire [2:0]s07_couplers_to_auto_us_df_AWSIZE;
  wire s07_couplers_to_auto_us_df_AWVALID;
  wire s07_couplers_to_auto_us_df_BREADY;
  wire [1:0]s07_couplers_to_auto_us_df_BRESP;
  wire s07_couplers_to_auto_us_df_BVALID;
  wire [31:0]s07_couplers_to_auto_us_df_WDATA;
  wire s07_couplers_to_auto_us_df_WLAST;
  wire s07_couplers_to_auto_us_df_WREADY;
  wire [3:0]s07_couplers_to_auto_us_df_WSTRB;
  wire s07_couplers_to_auto_us_df_WVALID;

  assign M_ACLK_1 = M_ACLK;
  assign M_ARESETN_1 = M_ARESETN[0];
  assign M_AXI_awaddr[31:0] = auto_us_df_to_s07_couplers_AWADDR;
  assign M_AXI_awburst[1:0] = auto_us_df_to_s07_couplers_AWBURST;
  assign M_AXI_awcache[3:0] = auto_us_df_to_s07_couplers_AWCACHE;
  assign M_AXI_awlen[7:0] = auto_us_df_to_s07_couplers_AWLEN;
  assign M_AXI_awlock[0] = auto_us_df_to_s07_couplers_AWLOCK;
  assign M_AXI_awprot[2:0] = auto_us_df_to_s07_couplers_AWPROT;
  assign M_AXI_awqos[3:0] = auto_us_df_to_s07_couplers_AWQOS;
  assign M_AXI_awsize[2:0] = auto_us_df_to_s07_couplers_AWSIZE;
  assign M_AXI_awvalid = auto_us_df_to_s07_couplers_AWVALID;
  assign M_AXI_bready = auto_us_df_to_s07_couplers_BREADY;
  assign M_AXI_wdata[511:0] = auto_us_df_to_s07_couplers_WDATA;
  assign M_AXI_wlast = auto_us_df_to_s07_couplers_WLAST;
  assign M_AXI_wstrb[63:0] = auto_us_df_to_s07_couplers_WSTRB;
  assign M_AXI_wvalid = auto_us_df_to_s07_couplers_WVALID;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN[0];
  assign S_AXI_awready = s07_couplers_to_auto_us_df_AWREADY;
  assign S_AXI_bresp[1:0] = s07_couplers_to_auto_us_df_BRESP;
  assign S_AXI_bvalid = s07_couplers_to_auto_us_df_BVALID;
  assign S_AXI_wready = s07_couplers_to_auto_us_df_WREADY;
  assign auto_us_df_to_s07_couplers_AWREADY = M_AXI_awready;
  assign auto_us_df_to_s07_couplers_BRESP = M_AXI_bresp[1:0];
  assign auto_us_df_to_s07_couplers_BVALID = M_AXI_bvalid;
  assign auto_us_df_to_s07_couplers_WREADY = M_AXI_wready;
  assign s07_couplers_to_auto_us_df_AWADDR = S_AXI_awaddr[31:0];
  assign s07_couplers_to_auto_us_df_AWBURST = S_AXI_awburst[1:0];
  assign s07_couplers_to_auto_us_df_AWCACHE = S_AXI_awcache[3:0];
  assign s07_couplers_to_auto_us_df_AWLEN = S_AXI_awlen[7:0];
  assign s07_couplers_to_auto_us_df_AWPROT = S_AXI_awprot[2:0];
  assign s07_couplers_to_auto_us_df_AWSIZE = S_AXI_awsize[2:0];
  assign s07_couplers_to_auto_us_df_AWVALID = S_AXI_awvalid;
  assign s07_couplers_to_auto_us_df_BREADY = S_AXI_bready;
  assign s07_couplers_to_auto_us_df_WDATA = S_AXI_wdata[31:0];
  assign s07_couplers_to_auto_us_df_WLAST = S_AXI_wlast;
  assign s07_couplers_to_auto_us_df_WSTRB = S_AXI_wstrb[3:0];
  assign s07_couplers_to_auto_us_df_WVALID = S_AXI_wvalid;
  design_1_auto_us_df_7 auto_us_df
       (.m_axi_awaddr(auto_us_df_to_s07_couplers_AWADDR),
        .m_axi_awburst(auto_us_df_to_s07_couplers_AWBURST),
        .m_axi_awcache(auto_us_df_to_s07_couplers_AWCACHE),
        .m_axi_awlen(auto_us_df_to_s07_couplers_AWLEN),
        .m_axi_awlock(auto_us_df_to_s07_couplers_AWLOCK),
        .m_axi_awprot(auto_us_df_to_s07_couplers_AWPROT),
        .m_axi_awqos(auto_us_df_to_s07_couplers_AWQOS),
        .m_axi_awready(auto_us_df_to_s07_couplers_AWREADY),
        .m_axi_awsize(auto_us_df_to_s07_couplers_AWSIZE),
        .m_axi_awvalid(auto_us_df_to_s07_couplers_AWVALID),
        .m_axi_bready(auto_us_df_to_s07_couplers_BREADY),
        .m_axi_bresp(auto_us_df_to_s07_couplers_BRESP),
        .m_axi_bvalid(auto_us_df_to_s07_couplers_BVALID),
        .m_axi_wdata(auto_us_df_to_s07_couplers_WDATA),
        .m_axi_wlast(auto_us_df_to_s07_couplers_WLAST),
        .m_axi_wready(auto_us_df_to_s07_couplers_WREADY),
        .m_axi_wstrb(auto_us_df_to_s07_couplers_WSTRB),
        .m_axi_wvalid(auto_us_df_to_s07_couplers_WVALID),
        .s_axi_aclk(S_ACLK_1),
        .s_axi_aresetn(S_ARESETN_1),
        .s_axi_awaddr(s07_couplers_to_auto_us_df_AWADDR),
        .s_axi_awburst(s07_couplers_to_auto_us_df_AWBURST),
        .s_axi_awcache(s07_couplers_to_auto_us_df_AWCACHE),
        .s_axi_awlen(s07_couplers_to_auto_us_df_AWLEN),
        .s_axi_awlock(1'b0),
        .s_axi_awprot(s07_couplers_to_auto_us_df_AWPROT),
        .s_axi_awqos({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(s07_couplers_to_auto_us_df_AWREADY),
        .s_axi_awregion({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awsize(s07_couplers_to_auto_us_df_AWSIZE),
        .s_axi_awvalid(s07_couplers_to_auto_us_df_AWVALID),
        .s_axi_bready(s07_couplers_to_auto_us_df_BREADY),
        .s_axi_bresp(s07_couplers_to_auto_us_df_BRESP),
        .s_axi_bvalid(s07_couplers_to_auto_us_df_BVALID),
        .s_axi_wdata(s07_couplers_to_auto_us_df_WDATA),
        .s_axi_wlast(s07_couplers_to_auto_us_df_WLAST),
        .s_axi_wready(s07_couplers_to_auto_us_df_WREADY),
        .s_axi_wstrb(s07_couplers_to_auto_us_df_WSTRB),
        .s_axi_wvalid(s07_couplers_to_auto_us_df_WVALID));
endmodule

module s08_couplers_imp_3SO22L
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arburst,
    M_AXI_arcache,
    M_AXI_arlen,
    M_AXI_arlock,
    M_AXI_arprot,
    M_AXI_arqos,
    M_AXI_arready,
    M_AXI_arsize,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awburst,
    M_AXI_awcache,
    M_AXI_awlen,
    M_AXI_awlock,
    M_AXI_awprot,
    M_AXI_awqos,
    M_AXI_awready,
    M_AXI_awsize,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rlast,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wlast,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arburst,
    S_AXI_arcache,
    S_AXI_arlen,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arsize,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awburst,
    S_AXI_awcache,
    S_AXI_awlen,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awsize,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rlast,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wlast,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [1:0]M_AXI_arburst;
  output [3:0]M_AXI_arcache;
  output [7:0]M_AXI_arlen;
  output [0:0]M_AXI_arlock;
  output [2:0]M_AXI_arprot;
  output [3:0]M_AXI_arqos;
  input M_AXI_arready;
  output [2:0]M_AXI_arsize;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [1:0]M_AXI_awburst;
  output [3:0]M_AXI_awcache;
  output [7:0]M_AXI_awlen;
  output [0:0]M_AXI_awlock;
  output [2:0]M_AXI_awprot;
  output [3:0]M_AXI_awqos;
  input M_AXI_awready;
  output [2:0]M_AXI_awsize;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [511:0]M_AXI_rdata;
  input M_AXI_rlast;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [511:0]M_AXI_wdata;
  output M_AXI_wlast;
  input M_AXI_wready;
  output [63:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [1:0]S_AXI_arburst;
  input [3:0]S_AXI_arcache;
  input [7:0]S_AXI_arlen;
  input [2:0]S_AXI_arprot;
  output S_AXI_arready;
  input [2:0]S_AXI_arsize;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [1:0]S_AXI_awburst;
  input [3:0]S_AXI_awcache;
  input [7:0]S_AXI_awlen;
  input [2:0]S_AXI_awprot;
  output S_AXI_awready;
  input [2:0]S_AXI_awsize;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  output S_AXI_rlast;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  input S_AXI_wlast;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire M_ACLK_1;
  wire [0:0]M_ARESETN_1;
  wire S_ACLK_1;
  wire [0:0]S_ARESETN_1;
  wire [31:0]auto_us_df_to_s08_couplers_ARADDR;
  wire [1:0]auto_us_df_to_s08_couplers_ARBURST;
  wire [3:0]auto_us_df_to_s08_couplers_ARCACHE;
  wire [7:0]auto_us_df_to_s08_couplers_ARLEN;
  wire [0:0]auto_us_df_to_s08_couplers_ARLOCK;
  wire [2:0]auto_us_df_to_s08_couplers_ARPROT;
  wire [3:0]auto_us_df_to_s08_couplers_ARQOS;
  wire auto_us_df_to_s08_couplers_ARREADY;
  wire [2:0]auto_us_df_to_s08_couplers_ARSIZE;
  wire auto_us_df_to_s08_couplers_ARVALID;
  wire [31:0]auto_us_df_to_s08_couplers_AWADDR;
  wire [1:0]auto_us_df_to_s08_couplers_AWBURST;
  wire [3:0]auto_us_df_to_s08_couplers_AWCACHE;
  wire [7:0]auto_us_df_to_s08_couplers_AWLEN;
  wire [0:0]auto_us_df_to_s08_couplers_AWLOCK;
  wire [2:0]auto_us_df_to_s08_couplers_AWPROT;
  wire [3:0]auto_us_df_to_s08_couplers_AWQOS;
  wire auto_us_df_to_s08_couplers_AWREADY;
  wire [2:0]auto_us_df_to_s08_couplers_AWSIZE;
  wire auto_us_df_to_s08_couplers_AWVALID;
  wire auto_us_df_to_s08_couplers_BREADY;
  wire [1:0]auto_us_df_to_s08_couplers_BRESP;
  wire auto_us_df_to_s08_couplers_BVALID;
  wire [511:0]auto_us_df_to_s08_couplers_RDATA;
  wire auto_us_df_to_s08_couplers_RLAST;
  wire auto_us_df_to_s08_couplers_RREADY;
  wire [1:0]auto_us_df_to_s08_couplers_RRESP;
  wire auto_us_df_to_s08_couplers_RVALID;
  wire [511:0]auto_us_df_to_s08_couplers_WDATA;
  wire auto_us_df_to_s08_couplers_WLAST;
  wire auto_us_df_to_s08_couplers_WREADY;
  wire [63:0]auto_us_df_to_s08_couplers_WSTRB;
  wire auto_us_df_to_s08_couplers_WVALID;
  wire [31:0]s08_couplers_to_auto_us_df_ARADDR;
  wire [1:0]s08_couplers_to_auto_us_df_ARBURST;
  wire [3:0]s08_couplers_to_auto_us_df_ARCACHE;
  wire [7:0]s08_couplers_to_auto_us_df_ARLEN;
  wire [2:0]s08_couplers_to_auto_us_df_ARPROT;
  wire s08_couplers_to_auto_us_df_ARREADY;
  wire [2:0]s08_couplers_to_auto_us_df_ARSIZE;
  wire s08_couplers_to_auto_us_df_ARVALID;
  wire [31:0]s08_couplers_to_auto_us_df_AWADDR;
  wire [1:0]s08_couplers_to_auto_us_df_AWBURST;
  wire [3:0]s08_couplers_to_auto_us_df_AWCACHE;
  wire [7:0]s08_couplers_to_auto_us_df_AWLEN;
  wire [2:0]s08_couplers_to_auto_us_df_AWPROT;
  wire s08_couplers_to_auto_us_df_AWREADY;
  wire [2:0]s08_couplers_to_auto_us_df_AWSIZE;
  wire s08_couplers_to_auto_us_df_AWVALID;
  wire s08_couplers_to_auto_us_df_BREADY;
  wire [1:0]s08_couplers_to_auto_us_df_BRESP;
  wire s08_couplers_to_auto_us_df_BVALID;
  wire [31:0]s08_couplers_to_auto_us_df_RDATA;
  wire s08_couplers_to_auto_us_df_RLAST;
  wire s08_couplers_to_auto_us_df_RREADY;
  wire [1:0]s08_couplers_to_auto_us_df_RRESP;
  wire s08_couplers_to_auto_us_df_RVALID;
  wire [31:0]s08_couplers_to_auto_us_df_WDATA;
  wire s08_couplers_to_auto_us_df_WLAST;
  wire s08_couplers_to_auto_us_df_WREADY;
  wire [3:0]s08_couplers_to_auto_us_df_WSTRB;
  wire s08_couplers_to_auto_us_df_WVALID;

  assign M_ACLK_1 = M_ACLK;
  assign M_ARESETN_1 = M_ARESETN[0];
  assign M_AXI_araddr[31:0] = auto_us_df_to_s08_couplers_ARADDR;
  assign M_AXI_arburst[1:0] = auto_us_df_to_s08_couplers_ARBURST;
  assign M_AXI_arcache[3:0] = auto_us_df_to_s08_couplers_ARCACHE;
  assign M_AXI_arlen[7:0] = auto_us_df_to_s08_couplers_ARLEN;
  assign M_AXI_arlock[0] = auto_us_df_to_s08_couplers_ARLOCK;
  assign M_AXI_arprot[2:0] = auto_us_df_to_s08_couplers_ARPROT;
  assign M_AXI_arqos[3:0] = auto_us_df_to_s08_couplers_ARQOS;
  assign M_AXI_arsize[2:0] = auto_us_df_to_s08_couplers_ARSIZE;
  assign M_AXI_arvalid = auto_us_df_to_s08_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = auto_us_df_to_s08_couplers_AWADDR;
  assign M_AXI_awburst[1:0] = auto_us_df_to_s08_couplers_AWBURST;
  assign M_AXI_awcache[3:0] = auto_us_df_to_s08_couplers_AWCACHE;
  assign M_AXI_awlen[7:0] = auto_us_df_to_s08_couplers_AWLEN;
  assign M_AXI_awlock[0] = auto_us_df_to_s08_couplers_AWLOCK;
  assign M_AXI_awprot[2:0] = auto_us_df_to_s08_couplers_AWPROT;
  assign M_AXI_awqos[3:0] = auto_us_df_to_s08_couplers_AWQOS;
  assign M_AXI_awsize[2:0] = auto_us_df_to_s08_couplers_AWSIZE;
  assign M_AXI_awvalid = auto_us_df_to_s08_couplers_AWVALID;
  assign M_AXI_bready = auto_us_df_to_s08_couplers_BREADY;
  assign M_AXI_rready = auto_us_df_to_s08_couplers_RREADY;
  assign M_AXI_wdata[511:0] = auto_us_df_to_s08_couplers_WDATA;
  assign M_AXI_wlast = auto_us_df_to_s08_couplers_WLAST;
  assign M_AXI_wstrb[63:0] = auto_us_df_to_s08_couplers_WSTRB;
  assign M_AXI_wvalid = auto_us_df_to_s08_couplers_WVALID;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN[0];
  assign S_AXI_arready = s08_couplers_to_auto_us_df_ARREADY;
  assign S_AXI_awready = s08_couplers_to_auto_us_df_AWREADY;
  assign S_AXI_bresp[1:0] = s08_couplers_to_auto_us_df_BRESP;
  assign S_AXI_bvalid = s08_couplers_to_auto_us_df_BVALID;
  assign S_AXI_rdata[31:0] = s08_couplers_to_auto_us_df_RDATA;
  assign S_AXI_rlast = s08_couplers_to_auto_us_df_RLAST;
  assign S_AXI_rresp[1:0] = s08_couplers_to_auto_us_df_RRESP;
  assign S_AXI_rvalid = s08_couplers_to_auto_us_df_RVALID;
  assign S_AXI_wready = s08_couplers_to_auto_us_df_WREADY;
  assign auto_us_df_to_s08_couplers_ARREADY = M_AXI_arready;
  assign auto_us_df_to_s08_couplers_AWREADY = M_AXI_awready;
  assign auto_us_df_to_s08_couplers_BRESP = M_AXI_bresp[1:0];
  assign auto_us_df_to_s08_couplers_BVALID = M_AXI_bvalid;
  assign auto_us_df_to_s08_couplers_RDATA = M_AXI_rdata[511:0];
  assign auto_us_df_to_s08_couplers_RLAST = M_AXI_rlast;
  assign auto_us_df_to_s08_couplers_RRESP = M_AXI_rresp[1:0];
  assign auto_us_df_to_s08_couplers_RVALID = M_AXI_rvalid;
  assign auto_us_df_to_s08_couplers_WREADY = M_AXI_wready;
  assign s08_couplers_to_auto_us_df_ARADDR = S_AXI_araddr[31:0];
  assign s08_couplers_to_auto_us_df_ARBURST = S_AXI_arburst[1:0];
  assign s08_couplers_to_auto_us_df_ARCACHE = S_AXI_arcache[3:0];
  assign s08_couplers_to_auto_us_df_ARLEN = S_AXI_arlen[7:0];
  assign s08_couplers_to_auto_us_df_ARPROT = S_AXI_arprot[2:0];
  assign s08_couplers_to_auto_us_df_ARSIZE = S_AXI_arsize[2:0];
  assign s08_couplers_to_auto_us_df_ARVALID = S_AXI_arvalid;
  assign s08_couplers_to_auto_us_df_AWADDR = S_AXI_awaddr[31:0];
  assign s08_couplers_to_auto_us_df_AWBURST = S_AXI_awburst[1:0];
  assign s08_couplers_to_auto_us_df_AWCACHE = S_AXI_awcache[3:0];
  assign s08_couplers_to_auto_us_df_AWLEN = S_AXI_awlen[7:0];
  assign s08_couplers_to_auto_us_df_AWPROT = S_AXI_awprot[2:0];
  assign s08_couplers_to_auto_us_df_AWSIZE = S_AXI_awsize[2:0];
  assign s08_couplers_to_auto_us_df_AWVALID = S_AXI_awvalid;
  assign s08_couplers_to_auto_us_df_BREADY = S_AXI_bready;
  assign s08_couplers_to_auto_us_df_RREADY = S_AXI_rready;
  assign s08_couplers_to_auto_us_df_WDATA = S_AXI_wdata[31:0];
  assign s08_couplers_to_auto_us_df_WLAST = S_AXI_wlast;
  assign s08_couplers_to_auto_us_df_WSTRB = S_AXI_wstrb[3:0];
  assign s08_couplers_to_auto_us_df_WVALID = S_AXI_wvalid;
  design_1_auto_us_df_8 auto_us_df
       (.m_axi_araddr(auto_us_df_to_s08_couplers_ARADDR),
        .m_axi_arburst(auto_us_df_to_s08_couplers_ARBURST),
        .m_axi_arcache(auto_us_df_to_s08_couplers_ARCACHE),
        .m_axi_arlen(auto_us_df_to_s08_couplers_ARLEN),
        .m_axi_arlock(auto_us_df_to_s08_couplers_ARLOCK),
        .m_axi_arprot(auto_us_df_to_s08_couplers_ARPROT),
        .m_axi_arqos(auto_us_df_to_s08_couplers_ARQOS),
        .m_axi_arready(auto_us_df_to_s08_couplers_ARREADY),
        .m_axi_arsize(auto_us_df_to_s08_couplers_ARSIZE),
        .m_axi_arvalid(auto_us_df_to_s08_couplers_ARVALID),
        .m_axi_awaddr(auto_us_df_to_s08_couplers_AWADDR),
        .m_axi_awburst(auto_us_df_to_s08_couplers_AWBURST),
        .m_axi_awcache(auto_us_df_to_s08_couplers_AWCACHE),
        .m_axi_awlen(auto_us_df_to_s08_couplers_AWLEN),
        .m_axi_awlock(auto_us_df_to_s08_couplers_AWLOCK),
        .m_axi_awprot(auto_us_df_to_s08_couplers_AWPROT),
        .m_axi_awqos(auto_us_df_to_s08_couplers_AWQOS),
        .m_axi_awready(auto_us_df_to_s08_couplers_AWREADY),
        .m_axi_awsize(auto_us_df_to_s08_couplers_AWSIZE),
        .m_axi_awvalid(auto_us_df_to_s08_couplers_AWVALID),
        .m_axi_bready(auto_us_df_to_s08_couplers_BREADY),
        .m_axi_bresp(auto_us_df_to_s08_couplers_BRESP),
        .m_axi_bvalid(auto_us_df_to_s08_couplers_BVALID),
        .m_axi_rdata(auto_us_df_to_s08_couplers_RDATA),
        .m_axi_rlast(auto_us_df_to_s08_couplers_RLAST),
        .m_axi_rready(auto_us_df_to_s08_couplers_RREADY),
        .m_axi_rresp(auto_us_df_to_s08_couplers_RRESP),
        .m_axi_rvalid(auto_us_df_to_s08_couplers_RVALID),
        .m_axi_wdata(auto_us_df_to_s08_couplers_WDATA),
        .m_axi_wlast(auto_us_df_to_s08_couplers_WLAST),
        .m_axi_wready(auto_us_df_to_s08_couplers_WREADY),
        .m_axi_wstrb(auto_us_df_to_s08_couplers_WSTRB),
        .m_axi_wvalid(auto_us_df_to_s08_couplers_WVALID),
        .s_axi_aclk(S_ACLK_1),
        .s_axi_araddr(s08_couplers_to_auto_us_df_ARADDR),
        .s_axi_arburst(s08_couplers_to_auto_us_df_ARBURST),
        .s_axi_arcache(s08_couplers_to_auto_us_df_ARCACHE),
        .s_axi_aresetn(S_ARESETN_1),
        .s_axi_arlen(s08_couplers_to_auto_us_df_ARLEN),
        .s_axi_arlock(1'b0),
        .s_axi_arprot(s08_couplers_to_auto_us_df_ARPROT),
        .s_axi_arqos({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(s08_couplers_to_auto_us_df_ARREADY),
        .s_axi_arregion({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arsize(s08_couplers_to_auto_us_df_ARSIZE),
        .s_axi_arvalid(s08_couplers_to_auto_us_df_ARVALID),
        .s_axi_awaddr(s08_couplers_to_auto_us_df_AWADDR),
        .s_axi_awburst(s08_couplers_to_auto_us_df_AWBURST),
        .s_axi_awcache(s08_couplers_to_auto_us_df_AWCACHE),
        .s_axi_awlen(s08_couplers_to_auto_us_df_AWLEN),
        .s_axi_awlock(1'b0),
        .s_axi_awprot(s08_couplers_to_auto_us_df_AWPROT),
        .s_axi_awqos({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(s08_couplers_to_auto_us_df_AWREADY),
        .s_axi_awregion({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awsize(s08_couplers_to_auto_us_df_AWSIZE),
        .s_axi_awvalid(s08_couplers_to_auto_us_df_AWVALID),
        .s_axi_bready(s08_couplers_to_auto_us_df_BREADY),
        .s_axi_bresp(s08_couplers_to_auto_us_df_BRESP),
        .s_axi_bvalid(s08_couplers_to_auto_us_df_BVALID),
        .s_axi_rdata(s08_couplers_to_auto_us_df_RDATA),
        .s_axi_rlast(s08_couplers_to_auto_us_df_RLAST),
        .s_axi_rready(s08_couplers_to_auto_us_df_RREADY),
        .s_axi_rresp(s08_couplers_to_auto_us_df_RRESP),
        .s_axi_rvalid(s08_couplers_to_auto_us_df_RVALID),
        .s_axi_wdata(s08_couplers_to_auto_us_df_WDATA),
        .s_axi_wlast(s08_couplers_to_auto_us_df_WLAST),
        .s_axi_wready(s08_couplers_to_auto_us_df_WREADY),
        .s_axi_wstrb(s08_couplers_to_auto_us_df_WSTRB),
        .s_axi_wvalid(s08_couplers_to_auto_us_df_WVALID));
endmodule

module s09_couplers_imp_1QZADNG
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arburst,
    M_AXI_arcache,
    M_AXI_arlen,
    M_AXI_arlock,
    M_AXI_arprot,
    M_AXI_arqos,
    M_AXI_arready,
    M_AXI_arsize,
    M_AXI_arvalid,
    M_AXI_rdata,
    M_AXI_rlast,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arburst,
    S_AXI_arcache,
    S_AXI_arlen,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arsize,
    S_AXI_arvalid,
    S_AXI_rdata,
    S_AXI_rlast,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [1:0]M_AXI_arburst;
  output [3:0]M_AXI_arcache;
  output [7:0]M_AXI_arlen;
  output [0:0]M_AXI_arlock;
  output [2:0]M_AXI_arprot;
  output [3:0]M_AXI_arqos;
  input M_AXI_arready;
  output [2:0]M_AXI_arsize;
  output M_AXI_arvalid;
  input [511:0]M_AXI_rdata;
  input M_AXI_rlast;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [1:0]S_AXI_arburst;
  input [3:0]S_AXI_arcache;
  input [7:0]S_AXI_arlen;
  input [2:0]S_AXI_arprot;
  output S_AXI_arready;
  input [2:0]S_AXI_arsize;
  input S_AXI_arvalid;
  output [31:0]S_AXI_rdata;
  output S_AXI_rlast;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;

  wire M_ACLK_1;
  wire [0:0]M_ARESETN_1;
  wire S_ACLK_1;
  wire [0:0]S_ARESETN_1;
  wire [31:0]auto_us_df_to_s09_couplers_ARADDR;
  wire [1:0]auto_us_df_to_s09_couplers_ARBURST;
  wire [3:0]auto_us_df_to_s09_couplers_ARCACHE;
  wire [7:0]auto_us_df_to_s09_couplers_ARLEN;
  wire [0:0]auto_us_df_to_s09_couplers_ARLOCK;
  wire [2:0]auto_us_df_to_s09_couplers_ARPROT;
  wire [3:0]auto_us_df_to_s09_couplers_ARQOS;
  wire auto_us_df_to_s09_couplers_ARREADY;
  wire [2:0]auto_us_df_to_s09_couplers_ARSIZE;
  wire auto_us_df_to_s09_couplers_ARVALID;
  wire [511:0]auto_us_df_to_s09_couplers_RDATA;
  wire auto_us_df_to_s09_couplers_RLAST;
  wire auto_us_df_to_s09_couplers_RREADY;
  wire [1:0]auto_us_df_to_s09_couplers_RRESP;
  wire auto_us_df_to_s09_couplers_RVALID;
  wire [31:0]s09_couplers_to_auto_us_df_ARADDR;
  wire [1:0]s09_couplers_to_auto_us_df_ARBURST;
  wire [3:0]s09_couplers_to_auto_us_df_ARCACHE;
  wire [7:0]s09_couplers_to_auto_us_df_ARLEN;
  wire [2:0]s09_couplers_to_auto_us_df_ARPROT;
  wire s09_couplers_to_auto_us_df_ARREADY;
  wire [2:0]s09_couplers_to_auto_us_df_ARSIZE;
  wire s09_couplers_to_auto_us_df_ARVALID;
  wire [31:0]s09_couplers_to_auto_us_df_RDATA;
  wire s09_couplers_to_auto_us_df_RLAST;
  wire s09_couplers_to_auto_us_df_RREADY;
  wire [1:0]s09_couplers_to_auto_us_df_RRESP;
  wire s09_couplers_to_auto_us_df_RVALID;

  assign M_ACLK_1 = M_ACLK;
  assign M_ARESETN_1 = M_ARESETN[0];
  assign M_AXI_araddr[31:0] = auto_us_df_to_s09_couplers_ARADDR;
  assign M_AXI_arburst[1:0] = auto_us_df_to_s09_couplers_ARBURST;
  assign M_AXI_arcache[3:0] = auto_us_df_to_s09_couplers_ARCACHE;
  assign M_AXI_arlen[7:0] = auto_us_df_to_s09_couplers_ARLEN;
  assign M_AXI_arlock[0] = auto_us_df_to_s09_couplers_ARLOCK;
  assign M_AXI_arprot[2:0] = auto_us_df_to_s09_couplers_ARPROT;
  assign M_AXI_arqos[3:0] = auto_us_df_to_s09_couplers_ARQOS;
  assign M_AXI_arsize[2:0] = auto_us_df_to_s09_couplers_ARSIZE;
  assign M_AXI_arvalid = auto_us_df_to_s09_couplers_ARVALID;
  assign M_AXI_rready = auto_us_df_to_s09_couplers_RREADY;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN[0];
  assign S_AXI_arready = s09_couplers_to_auto_us_df_ARREADY;
  assign S_AXI_rdata[31:0] = s09_couplers_to_auto_us_df_RDATA;
  assign S_AXI_rlast = s09_couplers_to_auto_us_df_RLAST;
  assign S_AXI_rresp[1:0] = s09_couplers_to_auto_us_df_RRESP;
  assign S_AXI_rvalid = s09_couplers_to_auto_us_df_RVALID;
  assign auto_us_df_to_s09_couplers_ARREADY = M_AXI_arready;
  assign auto_us_df_to_s09_couplers_RDATA = M_AXI_rdata[511:0];
  assign auto_us_df_to_s09_couplers_RLAST = M_AXI_rlast;
  assign auto_us_df_to_s09_couplers_RRESP = M_AXI_rresp[1:0];
  assign auto_us_df_to_s09_couplers_RVALID = M_AXI_rvalid;
  assign s09_couplers_to_auto_us_df_ARADDR = S_AXI_araddr[31:0];
  assign s09_couplers_to_auto_us_df_ARBURST = S_AXI_arburst[1:0];
  assign s09_couplers_to_auto_us_df_ARCACHE = S_AXI_arcache[3:0];
  assign s09_couplers_to_auto_us_df_ARLEN = S_AXI_arlen[7:0];
  assign s09_couplers_to_auto_us_df_ARPROT = S_AXI_arprot[2:0];
  assign s09_couplers_to_auto_us_df_ARSIZE = S_AXI_arsize[2:0];
  assign s09_couplers_to_auto_us_df_ARVALID = S_AXI_arvalid;
  assign s09_couplers_to_auto_us_df_RREADY = S_AXI_rready;
  design_1_auto_us_df_9 auto_us_df
       (.m_axi_araddr(auto_us_df_to_s09_couplers_ARADDR),
        .m_axi_arburst(auto_us_df_to_s09_couplers_ARBURST),
        .m_axi_arcache(auto_us_df_to_s09_couplers_ARCACHE),
        .m_axi_arlen(auto_us_df_to_s09_couplers_ARLEN),
        .m_axi_arlock(auto_us_df_to_s09_couplers_ARLOCK),
        .m_axi_arprot(auto_us_df_to_s09_couplers_ARPROT),
        .m_axi_arqos(auto_us_df_to_s09_couplers_ARQOS),
        .m_axi_arready(auto_us_df_to_s09_couplers_ARREADY),
        .m_axi_arsize(auto_us_df_to_s09_couplers_ARSIZE),
        .m_axi_arvalid(auto_us_df_to_s09_couplers_ARVALID),
        .m_axi_rdata(auto_us_df_to_s09_couplers_RDATA),
        .m_axi_rlast(auto_us_df_to_s09_couplers_RLAST),
        .m_axi_rready(auto_us_df_to_s09_couplers_RREADY),
        .m_axi_rresp(auto_us_df_to_s09_couplers_RRESP),
        .m_axi_rvalid(auto_us_df_to_s09_couplers_RVALID),
        .s_axi_aclk(S_ACLK_1),
        .s_axi_araddr(s09_couplers_to_auto_us_df_ARADDR),
        .s_axi_arburst(s09_couplers_to_auto_us_df_ARBURST),
        .s_axi_arcache(s09_couplers_to_auto_us_df_ARCACHE),
        .s_axi_aresetn(S_ARESETN_1),
        .s_axi_arlen(s09_couplers_to_auto_us_df_ARLEN),
        .s_axi_arlock(1'b0),
        .s_axi_arprot(s09_couplers_to_auto_us_df_ARPROT),
        .s_axi_arqos({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(s09_couplers_to_auto_us_df_ARREADY),
        .s_axi_arregion({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arsize(s09_couplers_to_auto_us_df_ARSIZE),
        .s_axi_arvalid(s09_couplers_to_auto_us_df_ARVALID),
        .s_axi_rdata(s09_couplers_to_auto_us_df_RDATA),
        .s_axi_rlast(s09_couplers_to_auto_us_df_RLAST),
        .s_axi_rready(s09_couplers_to_auto_us_df_RREADY),
        .s_axi_rresp(s09_couplers_to_auto_us_df_RRESP),
        .s_axi_rvalid(s09_couplers_to_auto_us_df_RVALID));
endmodule

module s10_couplers_imp_1KWU8FD
   (M_ACLK,
    M_ARESETN,
    M_AXI_awaddr,
    M_AXI_awburst,
    M_AXI_awcache,
    M_AXI_awlen,
    M_AXI_awlock,
    M_AXI_awprot,
    M_AXI_awqos,
    M_AXI_awready,
    M_AXI_awsize,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_wdata,
    M_AXI_wlast,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_awaddr,
    S_AXI_awburst,
    S_AXI_awcache,
    S_AXI_awlen,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awsize,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_wdata,
    S_AXI_wlast,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [31:0]M_AXI_awaddr;
  output [1:0]M_AXI_awburst;
  output [3:0]M_AXI_awcache;
  output [7:0]M_AXI_awlen;
  output [0:0]M_AXI_awlock;
  output [2:0]M_AXI_awprot;
  output [3:0]M_AXI_awqos;
  input M_AXI_awready;
  output [2:0]M_AXI_awsize;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  output [511:0]M_AXI_wdata;
  output M_AXI_wlast;
  input M_AXI_wready;
  output [63:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [31:0]S_AXI_awaddr;
  input [1:0]S_AXI_awburst;
  input [3:0]S_AXI_awcache;
  input [7:0]S_AXI_awlen;
  input [2:0]S_AXI_awprot;
  output S_AXI_awready;
  input [2:0]S_AXI_awsize;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  input [31:0]S_AXI_wdata;
  input S_AXI_wlast;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire M_ACLK_1;
  wire [0:0]M_ARESETN_1;
  wire S_ACLK_1;
  wire [0:0]S_ARESETN_1;
  wire [31:0]auto_us_df_to_s10_couplers_AWADDR;
  wire [1:0]auto_us_df_to_s10_couplers_AWBURST;
  wire [3:0]auto_us_df_to_s10_couplers_AWCACHE;
  wire [7:0]auto_us_df_to_s10_couplers_AWLEN;
  wire [0:0]auto_us_df_to_s10_couplers_AWLOCK;
  wire [2:0]auto_us_df_to_s10_couplers_AWPROT;
  wire [3:0]auto_us_df_to_s10_couplers_AWQOS;
  wire auto_us_df_to_s10_couplers_AWREADY;
  wire [2:0]auto_us_df_to_s10_couplers_AWSIZE;
  wire auto_us_df_to_s10_couplers_AWVALID;
  wire auto_us_df_to_s10_couplers_BREADY;
  wire [1:0]auto_us_df_to_s10_couplers_BRESP;
  wire auto_us_df_to_s10_couplers_BVALID;
  wire [511:0]auto_us_df_to_s10_couplers_WDATA;
  wire auto_us_df_to_s10_couplers_WLAST;
  wire auto_us_df_to_s10_couplers_WREADY;
  wire [63:0]auto_us_df_to_s10_couplers_WSTRB;
  wire auto_us_df_to_s10_couplers_WVALID;
  wire [31:0]s10_couplers_to_auto_us_df_AWADDR;
  wire [1:0]s10_couplers_to_auto_us_df_AWBURST;
  wire [3:0]s10_couplers_to_auto_us_df_AWCACHE;
  wire [7:0]s10_couplers_to_auto_us_df_AWLEN;
  wire [2:0]s10_couplers_to_auto_us_df_AWPROT;
  wire s10_couplers_to_auto_us_df_AWREADY;
  wire [2:0]s10_couplers_to_auto_us_df_AWSIZE;
  wire s10_couplers_to_auto_us_df_AWVALID;
  wire s10_couplers_to_auto_us_df_BREADY;
  wire [1:0]s10_couplers_to_auto_us_df_BRESP;
  wire s10_couplers_to_auto_us_df_BVALID;
  wire [31:0]s10_couplers_to_auto_us_df_WDATA;
  wire s10_couplers_to_auto_us_df_WLAST;
  wire s10_couplers_to_auto_us_df_WREADY;
  wire [3:0]s10_couplers_to_auto_us_df_WSTRB;
  wire s10_couplers_to_auto_us_df_WVALID;

  assign M_ACLK_1 = M_ACLK;
  assign M_ARESETN_1 = M_ARESETN[0];
  assign M_AXI_awaddr[31:0] = auto_us_df_to_s10_couplers_AWADDR;
  assign M_AXI_awburst[1:0] = auto_us_df_to_s10_couplers_AWBURST;
  assign M_AXI_awcache[3:0] = auto_us_df_to_s10_couplers_AWCACHE;
  assign M_AXI_awlen[7:0] = auto_us_df_to_s10_couplers_AWLEN;
  assign M_AXI_awlock[0] = auto_us_df_to_s10_couplers_AWLOCK;
  assign M_AXI_awprot[2:0] = auto_us_df_to_s10_couplers_AWPROT;
  assign M_AXI_awqos[3:0] = auto_us_df_to_s10_couplers_AWQOS;
  assign M_AXI_awsize[2:0] = auto_us_df_to_s10_couplers_AWSIZE;
  assign M_AXI_awvalid = auto_us_df_to_s10_couplers_AWVALID;
  assign M_AXI_bready = auto_us_df_to_s10_couplers_BREADY;
  assign M_AXI_wdata[511:0] = auto_us_df_to_s10_couplers_WDATA;
  assign M_AXI_wlast = auto_us_df_to_s10_couplers_WLAST;
  assign M_AXI_wstrb[63:0] = auto_us_df_to_s10_couplers_WSTRB;
  assign M_AXI_wvalid = auto_us_df_to_s10_couplers_WVALID;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN[0];
  assign S_AXI_awready = s10_couplers_to_auto_us_df_AWREADY;
  assign S_AXI_bresp[1:0] = s10_couplers_to_auto_us_df_BRESP;
  assign S_AXI_bvalid = s10_couplers_to_auto_us_df_BVALID;
  assign S_AXI_wready = s10_couplers_to_auto_us_df_WREADY;
  assign auto_us_df_to_s10_couplers_AWREADY = M_AXI_awready;
  assign auto_us_df_to_s10_couplers_BRESP = M_AXI_bresp[1:0];
  assign auto_us_df_to_s10_couplers_BVALID = M_AXI_bvalid;
  assign auto_us_df_to_s10_couplers_WREADY = M_AXI_wready;
  assign s10_couplers_to_auto_us_df_AWADDR = S_AXI_awaddr[31:0];
  assign s10_couplers_to_auto_us_df_AWBURST = S_AXI_awburst[1:0];
  assign s10_couplers_to_auto_us_df_AWCACHE = S_AXI_awcache[3:0];
  assign s10_couplers_to_auto_us_df_AWLEN = S_AXI_awlen[7:0];
  assign s10_couplers_to_auto_us_df_AWPROT = S_AXI_awprot[2:0];
  assign s10_couplers_to_auto_us_df_AWSIZE = S_AXI_awsize[2:0];
  assign s10_couplers_to_auto_us_df_AWVALID = S_AXI_awvalid;
  assign s10_couplers_to_auto_us_df_BREADY = S_AXI_bready;
  assign s10_couplers_to_auto_us_df_WDATA = S_AXI_wdata[31:0];
  assign s10_couplers_to_auto_us_df_WLAST = S_AXI_wlast;
  assign s10_couplers_to_auto_us_df_WSTRB = S_AXI_wstrb[3:0];
  assign s10_couplers_to_auto_us_df_WVALID = S_AXI_wvalid;
  design_1_auto_us_df_10 auto_us_df
       (.m_axi_awaddr(auto_us_df_to_s10_couplers_AWADDR),
        .m_axi_awburst(auto_us_df_to_s10_couplers_AWBURST),
        .m_axi_awcache(auto_us_df_to_s10_couplers_AWCACHE),
        .m_axi_awlen(auto_us_df_to_s10_couplers_AWLEN),
        .m_axi_awlock(auto_us_df_to_s10_couplers_AWLOCK),
        .m_axi_awprot(auto_us_df_to_s10_couplers_AWPROT),
        .m_axi_awqos(auto_us_df_to_s10_couplers_AWQOS),
        .m_axi_awready(auto_us_df_to_s10_couplers_AWREADY),
        .m_axi_awsize(auto_us_df_to_s10_couplers_AWSIZE),
        .m_axi_awvalid(auto_us_df_to_s10_couplers_AWVALID),
        .m_axi_bready(auto_us_df_to_s10_couplers_BREADY),
        .m_axi_bresp(auto_us_df_to_s10_couplers_BRESP),
        .m_axi_bvalid(auto_us_df_to_s10_couplers_BVALID),
        .m_axi_wdata(auto_us_df_to_s10_couplers_WDATA),
        .m_axi_wlast(auto_us_df_to_s10_couplers_WLAST),
        .m_axi_wready(auto_us_df_to_s10_couplers_WREADY),
        .m_axi_wstrb(auto_us_df_to_s10_couplers_WSTRB),
        .m_axi_wvalid(auto_us_df_to_s10_couplers_WVALID),
        .s_axi_aclk(S_ACLK_1),
        .s_axi_aresetn(S_ARESETN_1),
        .s_axi_awaddr(s10_couplers_to_auto_us_df_AWADDR),
        .s_axi_awburst(s10_couplers_to_auto_us_df_AWBURST),
        .s_axi_awcache(s10_couplers_to_auto_us_df_AWCACHE),
        .s_axi_awlen(s10_couplers_to_auto_us_df_AWLEN),
        .s_axi_awlock(1'b0),
        .s_axi_awprot(s10_couplers_to_auto_us_df_AWPROT),
        .s_axi_awqos({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(s10_couplers_to_auto_us_df_AWREADY),
        .s_axi_awregion({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awsize(s10_couplers_to_auto_us_df_AWSIZE),
        .s_axi_awvalid(s10_couplers_to_auto_us_df_AWVALID),
        .s_axi_bready(s10_couplers_to_auto_us_df_BREADY),
        .s_axi_bresp(s10_couplers_to_auto_us_df_BRESP),
        .s_axi_bvalid(s10_couplers_to_auto_us_df_BVALID),
        .s_axi_wdata(s10_couplers_to_auto_us_df_WDATA),
        .s_axi_wlast(s10_couplers_to_auto_us_df_WLAST),
        .s_axi_wready(s10_couplers_to_auto_us_df_WREADY),
        .s_axi_wstrb(s10_couplers_to_auto_us_df_WSTRB),
        .s_axi_wvalid(s10_couplers_to_auto_us_df_WVALID));
endmodule

module s11_couplers_imp_9IPFIG
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arburst,
    M_AXI_arcache,
    M_AXI_arlen,
    M_AXI_arlock,
    M_AXI_arprot,
    M_AXI_arqos,
    M_AXI_arready,
    M_AXI_arsize,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awburst,
    M_AXI_awcache,
    M_AXI_awlen,
    M_AXI_awlock,
    M_AXI_awprot,
    M_AXI_awqos,
    M_AXI_awready,
    M_AXI_awsize,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rlast,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wlast,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arburst,
    S_AXI_arcache,
    S_AXI_arlen,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arsize,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awburst,
    S_AXI_awcache,
    S_AXI_awlen,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awsize,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rlast,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wlast,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [1:0]M_AXI_arburst;
  output [3:0]M_AXI_arcache;
  output [7:0]M_AXI_arlen;
  output [0:0]M_AXI_arlock;
  output [2:0]M_AXI_arprot;
  output [3:0]M_AXI_arqos;
  input M_AXI_arready;
  output [2:0]M_AXI_arsize;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [1:0]M_AXI_awburst;
  output [3:0]M_AXI_awcache;
  output [7:0]M_AXI_awlen;
  output [0:0]M_AXI_awlock;
  output [2:0]M_AXI_awprot;
  output [3:0]M_AXI_awqos;
  input M_AXI_awready;
  output [2:0]M_AXI_awsize;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [511:0]M_AXI_rdata;
  input M_AXI_rlast;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [511:0]M_AXI_wdata;
  output M_AXI_wlast;
  input M_AXI_wready;
  output [63:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [1:0]S_AXI_arburst;
  input [3:0]S_AXI_arcache;
  input [7:0]S_AXI_arlen;
  input [2:0]S_AXI_arprot;
  output S_AXI_arready;
  input [2:0]S_AXI_arsize;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [1:0]S_AXI_awburst;
  input [3:0]S_AXI_awcache;
  input [7:0]S_AXI_awlen;
  input [2:0]S_AXI_awprot;
  output S_AXI_awready;
  input [2:0]S_AXI_awsize;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  output S_AXI_rlast;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  input S_AXI_wlast;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire M_ACLK_1;
  wire [0:0]M_ARESETN_1;
  wire S_ACLK_1;
  wire [0:0]S_ARESETN_1;
  wire [31:0]auto_us_df_to_s11_couplers_ARADDR;
  wire [1:0]auto_us_df_to_s11_couplers_ARBURST;
  wire [3:0]auto_us_df_to_s11_couplers_ARCACHE;
  wire [7:0]auto_us_df_to_s11_couplers_ARLEN;
  wire [0:0]auto_us_df_to_s11_couplers_ARLOCK;
  wire [2:0]auto_us_df_to_s11_couplers_ARPROT;
  wire [3:0]auto_us_df_to_s11_couplers_ARQOS;
  wire auto_us_df_to_s11_couplers_ARREADY;
  wire [2:0]auto_us_df_to_s11_couplers_ARSIZE;
  wire auto_us_df_to_s11_couplers_ARVALID;
  wire [31:0]auto_us_df_to_s11_couplers_AWADDR;
  wire [1:0]auto_us_df_to_s11_couplers_AWBURST;
  wire [3:0]auto_us_df_to_s11_couplers_AWCACHE;
  wire [7:0]auto_us_df_to_s11_couplers_AWLEN;
  wire [0:0]auto_us_df_to_s11_couplers_AWLOCK;
  wire [2:0]auto_us_df_to_s11_couplers_AWPROT;
  wire [3:0]auto_us_df_to_s11_couplers_AWQOS;
  wire auto_us_df_to_s11_couplers_AWREADY;
  wire [2:0]auto_us_df_to_s11_couplers_AWSIZE;
  wire auto_us_df_to_s11_couplers_AWVALID;
  wire auto_us_df_to_s11_couplers_BREADY;
  wire [1:0]auto_us_df_to_s11_couplers_BRESP;
  wire auto_us_df_to_s11_couplers_BVALID;
  wire [511:0]auto_us_df_to_s11_couplers_RDATA;
  wire auto_us_df_to_s11_couplers_RLAST;
  wire auto_us_df_to_s11_couplers_RREADY;
  wire [1:0]auto_us_df_to_s11_couplers_RRESP;
  wire auto_us_df_to_s11_couplers_RVALID;
  wire [511:0]auto_us_df_to_s11_couplers_WDATA;
  wire auto_us_df_to_s11_couplers_WLAST;
  wire auto_us_df_to_s11_couplers_WREADY;
  wire [63:0]auto_us_df_to_s11_couplers_WSTRB;
  wire auto_us_df_to_s11_couplers_WVALID;
  wire [31:0]s11_couplers_to_auto_us_df_ARADDR;
  wire [1:0]s11_couplers_to_auto_us_df_ARBURST;
  wire [3:0]s11_couplers_to_auto_us_df_ARCACHE;
  wire [7:0]s11_couplers_to_auto_us_df_ARLEN;
  wire [2:0]s11_couplers_to_auto_us_df_ARPROT;
  wire s11_couplers_to_auto_us_df_ARREADY;
  wire [2:0]s11_couplers_to_auto_us_df_ARSIZE;
  wire s11_couplers_to_auto_us_df_ARVALID;
  wire [31:0]s11_couplers_to_auto_us_df_AWADDR;
  wire [1:0]s11_couplers_to_auto_us_df_AWBURST;
  wire [3:0]s11_couplers_to_auto_us_df_AWCACHE;
  wire [7:0]s11_couplers_to_auto_us_df_AWLEN;
  wire [2:0]s11_couplers_to_auto_us_df_AWPROT;
  wire s11_couplers_to_auto_us_df_AWREADY;
  wire [2:0]s11_couplers_to_auto_us_df_AWSIZE;
  wire s11_couplers_to_auto_us_df_AWVALID;
  wire s11_couplers_to_auto_us_df_BREADY;
  wire [1:0]s11_couplers_to_auto_us_df_BRESP;
  wire s11_couplers_to_auto_us_df_BVALID;
  wire [31:0]s11_couplers_to_auto_us_df_RDATA;
  wire s11_couplers_to_auto_us_df_RLAST;
  wire s11_couplers_to_auto_us_df_RREADY;
  wire [1:0]s11_couplers_to_auto_us_df_RRESP;
  wire s11_couplers_to_auto_us_df_RVALID;
  wire [31:0]s11_couplers_to_auto_us_df_WDATA;
  wire s11_couplers_to_auto_us_df_WLAST;
  wire s11_couplers_to_auto_us_df_WREADY;
  wire [3:0]s11_couplers_to_auto_us_df_WSTRB;
  wire s11_couplers_to_auto_us_df_WVALID;

  assign M_ACLK_1 = M_ACLK;
  assign M_ARESETN_1 = M_ARESETN[0];
  assign M_AXI_araddr[31:0] = auto_us_df_to_s11_couplers_ARADDR;
  assign M_AXI_arburst[1:0] = auto_us_df_to_s11_couplers_ARBURST;
  assign M_AXI_arcache[3:0] = auto_us_df_to_s11_couplers_ARCACHE;
  assign M_AXI_arlen[7:0] = auto_us_df_to_s11_couplers_ARLEN;
  assign M_AXI_arlock[0] = auto_us_df_to_s11_couplers_ARLOCK;
  assign M_AXI_arprot[2:0] = auto_us_df_to_s11_couplers_ARPROT;
  assign M_AXI_arqos[3:0] = auto_us_df_to_s11_couplers_ARQOS;
  assign M_AXI_arsize[2:0] = auto_us_df_to_s11_couplers_ARSIZE;
  assign M_AXI_arvalid = auto_us_df_to_s11_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = auto_us_df_to_s11_couplers_AWADDR;
  assign M_AXI_awburst[1:0] = auto_us_df_to_s11_couplers_AWBURST;
  assign M_AXI_awcache[3:0] = auto_us_df_to_s11_couplers_AWCACHE;
  assign M_AXI_awlen[7:0] = auto_us_df_to_s11_couplers_AWLEN;
  assign M_AXI_awlock[0] = auto_us_df_to_s11_couplers_AWLOCK;
  assign M_AXI_awprot[2:0] = auto_us_df_to_s11_couplers_AWPROT;
  assign M_AXI_awqos[3:0] = auto_us_df_to_s11_couplers_AWQOS;
  assign M_AXI_awsize[2:0] = auto_us_df_to_s11_couplers_AWSIZE;
  assign M_AXI_awvalid = auto_us_df_to_s11_couplers_AWVALID;
  assign M_AXI_bready = auto_us_df_to_s11_couplers_BREADY;
  assign M_AXI_rready = auto_us_df_to_s11_couplers_RREADY;
  assign M_AXI_wdata[511:0] = auto_us_df_to_s11_couplers_WDATA;
  assign M_AXI_wlast = auto_us_df_to_s11_couplers_WLAST;
  assign M_AXI_wstrb[63:0] = auto_us_df_to_s11_couplers_WSTRB;
  assign M_AXI_wvalid = auto_us_df_to_s11_couplers_WVALID;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN[0];
  assign S_AXI_arready = s11_couplers_to_auto_us_df_ARREADY;
  assign S_AXI_awready = s11_couplers_to_auto_us_df_AWREADY;
  assign S_AXI_bresp[1:0] = s11_couplers_to_auto_us_df_BRESP;
  assign S_AXI_bvalid = s11_couplers_to_auto_us_df_BVALID;
  assign S_AXI_rdata[31:0] = s11_couplers_to_auto_us_df_RDATA;
  assign S_AXI_rlast = s11_couplers_to_auto_us_df_RLAST;
  assign S_AXI_rresp[1:0] = s11_couplers_to_auto_us_df_RRESP;
  assign S_AXI_rvalid = s11_couplers_to_auto_us_df_RVALID;
  assign S_AXI_wready = s11_couplers_to_auto_us_df_WREADY;
  assign auto_us_df_to_s11_couplers_ARREADY = M_AXI_arready;
  assign auto_us_df_to_s11_couplers_AWREADY = M_AXI_awready;
  assign auto_us_df_to_s11_couplers_BRESP = M_AXI_bresp[1:0];
  assign auto_us_df_to_s11_couplers_BVALID = M_AXI_bvalid;
  assign auto_us_df_to_s11_couplers_RDATA = M_AXI_rdata[511:0];
  assign auto_us_df_to_s11_couplers_RLAST = M_AXI_rlast;
  assign auto_us_df_to_s11_couplers_RRESP = M_AXI_rresp[1:0];
  assign auto_us_df_to_s11_couplers_RVALID = M_AXI_rvalid;
  assign auto_us_df_to_s11_couplers_WREADY = M_AXI_wready;
  assign s11_couplers_to_auto_us_df_ARADDR = S_AXI_araddr[31:0];
  assign s11_couplers_to_auto_us_df_ARBURST = S_AXI_arburst[1:0];
  assign s11_couplers_to_auto_us_df_ARCACHE = S_AXI_arcache[3:0];
  assign s11_couplers_to_auto_us_df_ARLEN = S_AXI_arlen[7:0];
  assign s11_couplers_to_auto_us_df_ARPROT = S_AXI_arprot[2:0];
  assign s11_couplers_to_auto_us_df_ARSIZE = S_AXI_arsize[2:0];
  assign s11_couplers_to_auto_us_df_ARVALID = S_AXI_arvalid;
  assign s11_couplers_to_auto_us_df_AWADDR = S_AXI_awaddr[31:0];
  assign s11_couplers_to_auto_us_df_AWBURST = S_AXI_awburst[1:0];
  assign s11_couplers_to_auto_us_df_AWCACHE = S_AXI_awcache[3:0];
  assign s11_couplers_to_auto_us_df_AWLEN = S_AXI_awlen[7:0];
  assign s11_couplers_to_auto_us_df_AWPROT = S_AXI_awprot[2:0];
  assign s11_couplers_to_auto_us_df_AWSIZE = S_AXI_awsize[2:0];
  assign s11_couplers_to_auto_us_df_AWVALID = S_AXI_awvalid;
  assign s11_couplers_to_auto_us_df_BREADY = S_AXI_bready;
  assign s11_couplers_to_auto_us_df_RREADY = S_AXI_rready;
  assign s11_couplers_to_auto_us_df_WDATA = S_AXI_wdata[31:0];
  assign s11_couplers_to_auto_us_df_WLAST = S_AXI_wlast;
  assign s11_couplers_to_auto_us_df_WSTRB = S_AXI_wstrb[3:0];
  assign s11_couplers_to_auto_us_df_WVALID = S_AXI_wvalid;
  design_1_auto_us_df_11 auto_us_df
       (.m_axi_araddr(auto_us_df_to_s11_couplers_ARADDR),
        .m_axi_arburst(auto_us_df_to_s11_couplers_ARBURST),
        .m_axi_arcache(auto_us_df_to_s11_couplers_ARCACHE),
        .m_axi_arlen(auto_us_df_to_s11_couplers_ARLEN),
        .m_axi_arlock(auto_us_df_to_s11_couplers_ARLOCK),
        .m_axi_arprot(auto_us_df_to_s11_couplers_ARPROT),
        .m_axi_arqos(auto_us_df_to_s11_couplers_ARQOS),
        .m_axi_arready(auto_us_df_to_s11_couplers_ARREADY),
        .m_axi_arsize(auto_us_df_to_s11_couplers_ARSIZE),
        .m_axi_arvalid(auto_us_df_to_s11_couplers_ARVALID),
        .m_axi_awaddr(auto_us_df_to_s11_couplers_AWADDR),
        .m_axi_awburst(auto_us_df_to_s11_couplers_AWBURST),
        .m_axi_awcache(auto_us_df_to_s11_couplers_AWCACHE),
        .m_axi_awlen(auto_us_df_to_s11_couplers_AWLEN),
        .m_axi_awlock(auto_us_df_to_s11_couplers_AWLOCK),
        .m_axi_awprot(auto_us_df_to_s11_couplers_AWPROT),
        .m_axi_awqos(auto_us_df_to_s11_couplers_AWQOS),
        .m_axi_awready(auto_us_df_to_s11_couplers_AWREADY),
        .m_axi_awsize(auto_us_df_to_s11_couplers_AWSIZE),
        .m_axi_awvalid(auto_us_df_to_s11_couplers_AWVALID),
        .m_axi_bready(auto_us_df_to_s11_couplers_BREADY),
        .m_axi_bresp(auto_us_df_to_s11_couplers_BRESP),
        .m_axi_bvalid(auto_us_df_to_s11_couplers_BVALID),
        .m_axi_rdata(auto_us_df_to_s11_couplers_RDATA),
        .m_axi_rlast(auto_us_df_to_s11_couplers_RLAST),
        .m_axi_rready(auto_us_df_to_s11_couplers_RREADY),
        .m_axi_rresp(auto_us_df_to_s11_couplers_RRESP),
        .m_axi_rvalid(auto_us_df_to_s11_couplers_RVALID),
        .m_axi_wdata(auto_us_df_to_s11_couplers_WDATA),
        .m_axi_wlast(auto_us_df_to_s11_couplers_WLAST),
        .m_axi_wready(auto_us_df_to_s11_couplers_WREADY),
        .m_axi_wstrb(auto_us_df_to_s11_couplers_WSTRB),
        .m_axi_wvalid(auto_us_df_to_s11_couplers_WVALID),
        .s_axi_aclk(S_ACLK_1),
        .s_axi_araddr(s11_couplers_to_auto_us_df_ARADDR),
        .s_axi_arburst(s11_couplers_to_auto_us_df_ARBURST),
        .s_axi_arcache(s11_couplers_to_auto_us_df_ARCACHE),
        .s_axi_aresetn(S_ARESETN_1),
        .s_axi_arlen(s11_couplers_to_auto_us_df_ARLEN),
        .s_axi_arlock(1'b0),
        .s_axi_arprot(s11_couplers_to_auto_us_df_ARPROT),
        .s_axi_arqos({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(s11_couplers_to_auto_us_df_ARREADY),
        .s_axi_arregion({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arsize(s11_couplers_to_auto_us_df_ARSIZE),
        .s_axi_arvalid(s11_couplers_to_auto_us_df_ARVALID),
        .s_axi_awaddr(s11_couplers_to_auto_us_df_AWADDR),
        .s_axi_awburst(s11_couplers_to_auto_us_df_AWBURST),
        .s_axi_awcache(s11_couplers_to_auto_us_df_AWCACHE),
        .s_axi_awlen(s11_couplers_to_auto_us_df_AWLEN),
        .s_axi_awlock(1'b0),
        .s_axi_awprot(s11_couplers_to_auto_us_df_AWPROT),
        .s_axi_awqos({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(s11_couplers_to_auto_us_df_AWREADY),
        .s_axi_awregion({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awsize(s11_couplers_to_auto_us_df_AWSIZE),
        .s_axi_awvalid(s11_couplers_to_auto_us_df_AWVALID),
        .s_axi_bready(s11_couplers_to_auto_us_df_BREADY),
        .s_axi_bresp(s11_couplers_to_auto_us_df_BRESP),
        .s_axi_bvalid(s11_couplers_to_auto_us_df_BVALID),
        .s_axi_rdata(s11_couplers_to_auto_us_df_RDATA),
        .s_axi_rlast(s11_couplers_to_auto_us_df_RLAST),
        .s_axi_rready(s11_couplers_to_auto_us_df_RREADY),
        .s_axi_rresp(s11_couplers_to_auto_us_df_RRESP),
        .s_axi_rvalid(s11_couplers_to_auto_us_df_RVALID),
        .s_axi_wdata(s11_couplers_to_auto_us_df_WDATA),
        .s_axi_wlast(s11_couplers_to_auto_us_df_WLAST),
        .s_axi_wready(s11_couplers_to_auto_us_df_WREADY),
        .s_axi_wstrb(s11_couplers_to_auto_us_df_WSTRB),
        .s_axi_wvalid(s11_couplers_to_auto_us_df_WVALID));
endmodule

module s12_couplers_imp_1JPMFMY
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arburst,
    M_AXI_arcache,
    M_AXI_arlen,
    M_AXI_arlock,
    M_AXI_arprot,
    M_AXI_arqos,
    M_AXI_arready,
    M_AXI_arsize,
    M_AXI_arvalid,
    M_AXI_rdata,
    M_AXI_rlast,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arburst,
    S_AXI_arcache,
    S_AXI_arlen,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arsize,
    S_AXI_arvalid,
    S_AXI_rdata,
    S_AXI_rlast,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [1:0]M_AXI_arburst;
  output [3:0]M_AXI_arcache;
  output [7:0]M_AXI_arlen;
  output [0:0]M_AXI_arlock;
  output [2:0]M_AXI_arprot;
  output [3:0]M_AXI_arqos;
  input M_AXI_arready;
  output [2:0]M_AXI_arsize;
  output M_AXI_arvalid;
  input [511:0]M_AXI_rdata;
  input M_AXI_rlast;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [1:0]S_AXI_arburst;
  input [3:0]S_AXI_arcache;
  input [7:0]S_AXI_arlen;
  input [2:0]S_AXI_arprot;
  output S_AXI_arready;
  input [2:0]S_AXI_arsize;
  input S_AXI_arvalid;
  output [31:0]S_AXI_rdata;
  output S_AXI_rlast;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;

  wire M_ACLK_1;
  wire [0:0]M_ARESETN_1;
  wire S_ACLK_1;
  wire [0:0]S_ARESETN_1;
  wire [31:0]auto_us_df_to_s12_couplers_ARADDR;
  wire [1:0]auto_us_df_to_s12_couplers_ARBURST;
  wire [3:0]auto_us_df_to_s12_couplers_ARCACHE;
  wire [7:0]auto_us_df_to_s12_couplers_ARLEN;
  wire [0:0]auto_us_df_to_s12_couplers_ARLOCK;
  wire [2:0]auto_us_df_to_s12_couplers_ARPROT;
  wire [3:0]auto_us_df_to_s12_couplers_ARQOS;
  wire auto_us_df_to_s12_couplers_ARREADY;
  wire [2:0]auto_us_df_to_s12_couplers_ARSIZE;
  wire auto_us_df_to_s12_couplers_ARVALID;
  wire [511:0]auto_us_df_to_s12_couplers_RDATA;
  wire auto_us_df_to_s12_couplers_RLAST;
  wire auto_us_df_to_s12_couplers_RREADY;
  wire [1:0]auto_us_df_to_s12_couplers_RRESP;
  wire auto_us_df_to_s12_couplers_RVALID;
  wire [31:0]s12_couplers_to_auto_us_df_ARADDR;
  wire [1:0]s12_couplers_to_auto_us_df_ARBURST;
  wire [3:0]s12_couplers_to_auto_us_df_ARCACHE;
  wire [7:0]s12_couplers_to_auto_us_df_ARLEN;
  wire [2:0]s12_couplers_to_auto_us_df_ARPROT;
  wire s12_couplers_to_auto_us_df_ARREADY;
  wire [2:0]s12_couplers_to_auto_us_df_ARSIZE;
  wire s12_couplers_to_auto_us_df_ARVALID;
  wire [31:0]s12_couplers_to_auto_us_df_RDATA;
  wire s12_couplers_to_auto_us_df_RLAST;
  wire s12_couplers_to_auto_us_df_RREADY;
  wire [1:0]s12_couplers_to_auto_us_df_RRESP;
  wire s12_couplers_to_auto_us_df_RVALID;

  assign M_ACLK_1 = M_ACLK;
  assign M_ARESETN_1 = M_ARESETN[0];
  assign M_AXI_araddr[31:0] = auto_us_df_to_s12_couplers_ARADDR;
  assign M_AXI_arburst[1:0] = auto_us_df_to_s12_couplers_ARBURST;
  assign M_AXI_arcache[3:0] = auto_us_df_to_s12_couplers_ARCACHE;
  assign M_AXI_arlen[7:0] = auto_us_df_to_s12_couplers_ARLEN;
  assign M_AXI_arlock[0] = auto_us_df_to_s12_couplers_ARLOCK;
  assign M_AXI_arprot[2:0] = auto_us_df_to_s12_couplers_ARPROT;
  assign M_AXI_arqos[3:0] = auto_us_df_to_s12_couplers_ARQOS;
  assign M_AXI_arsize[2:0] = auto_us_df_to_s12_couplers_ARSIZE;
  assign M_AXI_arvalid = auto_us_df_to_s12_couplers_ARVALID;
  assign M_AXI_rready = auto_us_df_to_s12_couplers_RREADY;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN[0];
  assign S_AXI_arready = s12_couplers_to_auto_us_df_ARREADY;
  assign S_AXI_rdata[31:0] = s12_couplers_to_auto_us_df_RDATA;
  assign S_AXI_rlast = s12_couplers_to_auto_us_df_RLAST;
  assign S_AXI_rresp[1:0] = s12_couplers_to_auto_us_df_RRESP;
  assign S_AXI_rvalid = s12_couplers_to_auto_us_df_RVALID;
  assign auto_us_df_to_s12_couplers_ARREADY = M_AXI_arready;
  assign auto_us_df_to_s12_couplers_RDATA = M_AXI_rdata[511:0];
  assign auto_us_df_to_s12_couplers_RLAST = M_AXI_rlast;
  assign auto_us_df_to_s12_couplers_RRESP = M_AXI_rresp[1:0];
  assign auto_us_df_to_s12_couplers_RVALID = M_AXI_rvalid;
  assign s12_couplers_to_auto_us_df_ARADDR = S_AXI_araddr[31:0];
  assign s12_couplers_to_auto_us_df_ARBURST = S_AXI_arburst[1:0];
  assign s12_couplers_to_auto_us_df_ARCACHE = S_AXI_arcache[3:0];
  assign s12_couplers_to_auto_us_df_ARLEN = S_AXI_arlen[7:0];
  assign s12_couplers_to_auto_us_df_ARPROT = S_AXI_arprot[2:0];
  assign s12_couplers_to_auto_us_df_ARSIZE = S_AXI_arsize[2:0];
  assign s12_couplers_to_auto_us_df_ARVALID = S_AXI_arvalid;
  assign s12_couplers_to_auto_us_df_RREADY = S_AXI_rready;
  design_1_auto_us_df_12 auto_us_df
       (.m_axi_araddr(auto_us_df_to_s12_couplers_ARADDR),
        .m_axi_arburst(auto_us_df_to_s12_couplers_ARBURST),
        .m_axi_arcache(auto_us_df_to_s12_couplers_ARCACHE),
        .m_axi_arlen(auto_us_df_to_s12_couplers_ARLEN),
        .m_axi_arlock(auto_us_df_to_s12_couplers_ARLOCK),
        .m_axi_arprot(auto_us_df_to_s12_couplers_ARPROT),
        .m_axi_arqos(auto_us_df_to_s12_couplers_ARQOS),
        .m_axi_arready(auto_us_df_to_s12_couplers_ARREADY),
        .m_axi_arsize(auto_us_df_to_s12_couplers_ARSIZE),
        .m_axi_arvalid(auto_us_df_to_s12_couplers_ARVALID),
        .m_axi_rdata(auto_us_df_to_s12_couplers_RDATA),
        .m_axi_rlast(auto_us_df_to_s12_couplers_RLAST),
        .m_axi_rready(auto_us_df_to_s12_couplers_RREADY),
        .m_axi_rresp(auto_us_df_to_s12_couplers_RRESP),
        .m_axi_rvalid(auto_us_df_to_s12_couplers_RVALID),
        .s_axi_aclk(S_ACLK_1),
        .s_axi_araddr(s12_couplers_to_auto_us_df_ARADDR),
        .s_axi_arburst(s12_couplers_to_auto_us_df_ARBURST),
        .s_axi_arcache(s12_couplers_to_auto_us_df_ARCACHE),
        .s_axi_aresetn(S_ARESETN_1),
        .s_axi_arlen(s12_couplers_to_auto_us_df_ARLEN),
        .s_axi_arlock(1'b0),
        .s_axi_arprot(s12_couplers_to_auto_us_df_ARPROT),
        .s_axi_arqos({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(s12_couplers_to_auto_us_df_ARREADY),
        .s_axi_arregion({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arsize(s12_couplers_to_auto_us_df_ARSIZE),
        .s_axi_arvalid(s12_couplers_to_auto_us_df_ARVALID),
        .s_axi_rdata(s12_couplers_to_auto_us_df_RDATA),
        .s_axi_rlast(s12_couplers_to_auto_us_df_RLAST),
        .s_axi_rready(s12_couplers_to_auto_us_df_RREADY),
        .s_axi_rresp(s12_couplers_to_auto_us_df_RRESP),
        .s_axi_rvalid(s12_couplers_to_auto_us_df_RVALID));
endmodule

module s13_couplers_imp_AZMDZF
   (M_ACLK,
    M_ARESETN,
    M_AXI_awaddr,
    M_AXI_awburst,
    M_AXI_awcache,
    M_AXI_awlen,
    M_AXI_awlock,
    M_AXI_awprot,
    M_AXI_awqos,
    M_AXI_awready,
    M_AXI_awsize,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_wdata,
    M_AXI_wlast,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_awaddr,
    S_AXI_awburst,
    S_AXI_awcache,
    S_AXI_awlen,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awsize,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_wdata,
    S_AXI_wlast,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [31:0]M_AXI_awaddr;
  output [1:0]M_AXI_awburst;
  output [3:0]M_AXI_awcache;
  output [7:0]M_AXI_awlen;
  output [0:0]M_AXI_awlock;
  output [2:0]M_AXI_awprot;
  output [3:0]M_AXI_awqos;
  input M_AXI_awready;
  output [2:0]M_AXI_awsize;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  output [511:0]M_AXI_wdata;
  output M_AXI_wlast;
  input M_AXI_wready;
  output [63:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [31:0]S_AXI_awaddr;
  input [1:0]S_AXI_awburst;
  input [3:0]S_AXI_awcache;
  input [7:0]S_AXI_awlen;
  input [2:0]S_AXI_awprot;
  output S_AXI_awready;
  input [2:0]S_AXI_awsize;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  input [31:0]S_AXI_wdata;
  input S_AXI_wlast;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire M_ACLK_1;
  wire [0:0]M_ARESETN_1;
  wire S_ACLK_1;
  wire [0:0]S_ARESETN_1;
  wire [31:0]auto_us_df_to_s13_couplers_AWADDR;
  wire [1:0]auto_us_df_to_s13_couplers_AWBURST;
  wire [3:0]auto_us_df_to_s13_couplers_AWCACHE;
  wire [7:0]auto_us_df_to_s13_couplers_AWLEN;
  wire [0:0]auto_us_df_to_s13_couplers_AWLOCK;
  wire [2:0]auto_us_df_to_s13_couplers_AWPROT;
  wire [3:0]auto_us_df_to_s13_couplers_AWQOS;
  wire auto_us_df_to_s13_couplers_AWREADY;
  wire [2:0]auto_us_df_to_s13_couplers_AWSIZE;
  wire auto_us_df_to_s13_couplers_AWVALID;
  wire auto_us_df_to_s13_couplers_BREADY;
  wire [1:0]auto_us_df_to_s13_couplers_BRESP;
  wire auto_us_df_to_s13_couplers_BVALID;
  wire [511:0]auto_us_df_to_s13_couplers_WDATA;
  wire auto_us_df_to_s13_couplers_WLAST;
  wire auto_us_df_to_s13_couplers_WREADY;
  wire [63:0]auto_us_df_to_s13_couplers_WSTRB;
  wire auto_us_df_to_s13_couplers_WVALID;
  wire [31:0]s13_couplers_to_auto_us_df_AWADDR;
  wire [1:0]s13_couplers_to_auto_us_df_AWBURST;
  wire [3:0]s13_couplers_to_auto_us_df_AWCACHE;
  wire [7:0]s13_couplers_to_auto_us_df_AWLEN;
  wire [2:0]s13_couplers_to_auto_us_df_AWPROT;
  wire s13_couplers_to_auto_us_df_AWREADY;
  wire [2:0]s13_couplers_to_auto_us_df_AWSIZE;
  wire s13_couplers_to_auto_us_df_AWVALID;
  wire s13_couplers_to_auto_us_df_BREADY;
  wire [1:0]s13_couplers_to_auto_us_df_BRESP;
  wire s13_couplers_to_auto_us_df_BVALID;
  wire [31:0]s13_couplers_to_auto_us_df_WDATA;
  wire s13_couplers_to_auto_us_df_WLAST;
  wire s13_couplers_to_auto_us_df_WREADY;
  wire [3:0]s13_couplers_to_auto_us_df_WSTRB;
  wire s13_couplers_to_auto_us_df_WVALID;

  assign M_ACLK_1 = M_ACLK;
  assign M_ARESETN_1 = M_ARESETN[0];
  assign M_AXI_awaddr[31:0] = auto_us_df_to_s13_couplers_AWADDR;
  assign M_AXI_awburst[1:0] = auto_us_df_to_s13_couplers_AWBURST;
  assign M_AXI_awcache[3:0] = auto_us_df_to_s13_couplers_AWCACHE;
  assign M_AXI_awlen[7:0] = auto_us_df_to_s13_couplers_AWLEN;
  assign M_AXI_awlock[0] = auto_us_df_to_s13_couplers_AWLOCK;
  assign M_AXI_awprot[2:0] = auto_us_df_to_s13_couplers_AWPROT;
  assign M_AXI_awqos[3:0] = auto_us_df_to_s13_couplers_AWQOS;
  assign M_AXI_awsize[2:0] = auto_us_df_to_s13_couplers_AWSIZE;
  assign M_AXI_awvalid = auto_us_df_to_s13_couplers_AWVALID;
  assign M_AXI_bready = auto_us_df_to_s13_couplers_BREADY;
  assign M_AXI_wdata[511:0] = auto_us_df_to_s13_couplers_WDATA;
  assign M_AXI_wlast = auto_us_df_to_s13_couplers_WLAST;
  assign M_AXI_wstrb[63:0] = auto_us_df_to_s13_couplers_WSTRB;
  assign M_AXI_wvalid = auto_us_df_to_s13_couplers_WVALID;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN[0];
  assign S_AXI_awready = s13_couplers_to_auto_us_df_AWREADY;
  assign S_AXI_bresp[1:0] = s13_couplers_to_auto_us_df_BRESP;
  assign S_AXI_bvalid = s13_couplers_to_auto_us_df_BVALID;
  assign S_AXI_wready = s13_couplers_to_auto_us_df_WREADY;
  assign auto_us_df_to_s13_couplers_AWREADY = M_AXI_awready;
  assign auto_us_df_to_s13_couplers_BRESP = M_AXI_bresp[1:0];
  assign auto_us_df_to_s13_couplers_BVALID = M_AXI_bvalid;
  assign auto_us_df_to_s13_couplers_WREADY = M_AXI_wready;
  assign s13_couplers_to_auto_us_df_AWADDR = S_AXI_awaddr[31:0];
  assign s13_couplers_to_auto_us_df_AWBURST = S_AXI_awburst[1:0];
  assign s13_couplers_to_auto_us_df_AWCACHE = S_AXI_awcache[3:0];
  assign s13_couplers_to_auto_us_df_AWLEN = S_AXI_awlen[7:0];
  assign s13_couplers_to_auto_us_df_AWPROT = S_AXI_awprot[2:0];
  assign s13_couplers_to_auto_us_df_AWSIZE = S_AXI_awsize[2:0];
  assign s13_couplers_to_auto_us_df_AWVALID = S_AXI_awvalid;
  assign s13_couplers_to_auto_us_df_BREADY = S_AXI_bready;
  assign s13_couplers_to_auto_us_df_WDATA = S_AXI_wdata[31:0];
  assign s13_couplers_to_auto_us_df_WLAST = S_AXI_wlast;
  assign s13_couplers_to_auto_us_df_WSTRB = S_AXI_wstrb[3:0];
  assign s13_couplers_to_auto_us_df_WVALID = S_AXI_wvalid;
  design_1_auto_us_df_13 auto_us_df
       (.m_axi_awaddr(auto_us_df_to_s13_couplers_AWADDR),
        .m_axi_awburst(auto_us_df_to_s13_couplers_AWBURST),
        .m_axi_awcache(auto_us_df_to_s13_couplers_AWCACHE),
        .m_axi_awlen(auto_us_df_to_s13_couplers_AWLEN),
        .m_axi_awlock(auto_us_df_to_s13_couplers_AWLOCK),
        .m_axi_awprot(auto_us_df_to_s13_couplers_AWPROT),
        .m_axi_awqos(auto_us_df_to_s13_couplers_AWQOS),
        .m_axi_awready(auto_us_df_to_s13_couplers_AWREADY),
        .m_axi_awsize(auto_us_df_to_s13_couplers_AWSIZE),
        .m_axi_awvalid(auto_us_df_to_s13_couplers_AWVALID),
        .m_axi_bready(auto_us_df_to_s13_couplers_BREADY),
        .m_axi_bresp(auto_us_df_to_s13_couplers_BRESP),
        .m_axi_bvalid(auto_us_df_to_s13_couplers_BVALID),
        .m_axi_wdata(auto_us_df_to_s13_couplers_WDATA),
        .m_axi_wlast(auto_us_df_to_s13_couplers_WLAST),
        .m_axi_wready(auto_us_df_to_s13_couplers_WREADY),
        .m_axi_wstrb(auto_us_df_to_s13_couplers_WSTRB),
        .m_axi_wvalid(auto_us_df_to_s13_couplers_WVALID),
        .s_axi_aclk(S_ACLK_1),
        .s_axi_aresetn(S_ARESETN_1),
        .s_axi_awaddr(s13_couplers_to_auto_us_df_AWADDR),
        .s_axi_awburst(s13_couplers_to_auto_us_df_AWBURST),
        .s_axi_awcache(s13_couplers_to_auto_us_df_AWCACHE),
        .s_axi_awlen(s13_couplers_to_auto_us_df_AWLEN),
        .s_axi_awlock(1'b0),
        .s_axi_awprot(s13_couplers_to_auto_us_df_AWPROT),
        .s_axi_awqos({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(s13_couplers_to_auto_us_df_AWREADY),
        .s_axi_awregion({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awsize(s13_couplers_to_auto_us_df_AWSIZE),
        .s_axi_awvalid(s13_couplers_to_auto_us_df_AWVALID),
        .s_axi_bready(s13_couplers_to_auto_us_df_BREADY),
        .s_axi_bresp(s13_couplers_to_auto_us_df_BRESP),
        .s_axi_bvalid(s13_couplers_to_auto_us_df_BVALID),
        .s_axi_wdata(s13_couplers_to_auto_us_df_WDATA),
        .s_axi_wlast(s13_couplers_to_auto_us_df_WLAST),
        .s_axi_wready(s13_couplers_to_auto_us_df_WREADY),
        .s_axi_wstrb(s13_couplers_to_auto_us_df_WSTRB),
        .s_axi_wvalid(s13_couplers_to_auto_us_df_WVALID));
endmodule
