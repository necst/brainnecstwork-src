// (c) Copyright 1995-2016 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.


// IP VLNV: xilinx.com:hls:pcc_stream:1.0
// IP Revision: 1606231801

(* X_CORE_INFO = "pcc_stream,Vivado 2015.4" *)
(* CHECK_LICENSE_TYPE = "design_1_pcc_stream_0_2,pcc_stream,{}" *)
(* CORE_GENERATION_INFO = "design_1_pcc_stream_0_2,pcc_stream,{x_ipProduct=Vivado 2015.4,x_ipVendor=xilinx.com,x_ipLibrary=hls,x_ipName=pcc_stream,x_ipVersion=1.0,x_ipCoreRevision=1606231801,x_ipLanguage=VERILOG,x_ipSimLanguage=MIXED}" *)
(* DowngradeIPIdentifiedWarnings = "yes" *)
module design_1_pcc_stream_0_2 (
  ap_clk,
  ap_rst_n,
  DMA_NumberOfCorrelation_TVALID,
  DMA_NumberOfCorrelation_TREADY,
  DMA_NumberOfCorrelation_TDATA,
  DMA_NumberOfCorrelation_TKEEP,
  DMA_NumberOfCorrelation_TSTRB,
  DMA_NumberOfCorrelation_TUSER,
  DMA_NumberOfCorrelation_TLAST,
  DMA_NumberOfCorrelation_TID,
  DMA_NumberOfCorrelation_TDEST,
  DMA_dep1_TVALID,
  DMA_dep1_TREADY,
  DMA_dep1_TDATA,
  DMA_dep1_TKEEP,
  DMA_dep1_TSTRB,
  DMA_dep1_TUSER,
  DMA_dep1_TLAST,
  DMA_dep1_TID,
  DMA_dep1_TDEST,
  DMA_dep2_TVALID,
  DMA_dep2_TREADY,
  DMA_dep2_TDATA,
  DMA_dep2_TKEEP,
  DMA_dep2_TSTRB,
  DMA_dep2_TUSER,
  DMA_dep2_TLAST,
  DMA_dep2_TID,
  DMA_dep2_TDEST,
  DMA_pcc_TVALID,
  DMA_pcc_TREADY,
  DMA_pcc_TDATA,
  DMA_pcc_TKEEP,
  DMA_pcc_TSTRB,
  DMA_pcc_TUSER,
  DMA_pcc_TLAST,
  DMA_pcc_TID,
  DMA_pcc_TDEST
);

(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 ap_clk CLK" *)
input wire ap_clk;
(* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 ap_rst_n RST" *)
input wire ap_rst_n;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 DMA_NumberOfCorrelation TVALID" *)
input wire DMA_NumberOfCorrelation_TVALID;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 DMA_NumberOfCorrelation TREADY" *)
output wire DMA_NumberOfCorrelation_TREADY;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 DMA_NumberOfCorrelation TDATA" *)
input wire [31 : 0] DMA_NumberOfCorrelation_TDATA;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 DMA_NumberOfCorrelation TKEEP" *)
input wire [3 : 0] DMA_NumberOfCorrelation_TKEEP;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 DMA_NumberOfCorrelation TSTRB" *)
input wire [3 : 0] DMA_NumberOfCorrelation_TSTRB;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 DMA_NumberOfCorrelation TUSER" *)
input wire [3 : 0] DMA_NumberOfCorrelation_TUSER;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 DMA_NumberOfCorrelation TLAST" *)
input wire [0 : 0] DMA_NumberOfCorrelation_TLAST;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 DMA_NumberOfCorrelation TID" *)
input wire [4 : 0] DMA_NumberOfCorrelation_TID;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 DMA_NumberOfCorrelation TDEST" *)
input wire [4 : 0] DMA_NumberOfCorrelation_TDEST;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 DMA_dep1 TVALID" *)
input wire DMA_dep1_TVALID;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 DMA_dep1 TREADY" *)
output wire DMA_dep1_TREADY;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 DMA_dep1 TDATA" *)
input wire [31 : 0] DMA_dep1_TDATA;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 DMA_dep1 TKEEP" *)
input wire [3 : 0] DMA_dep1_TKEEP;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 DMA_dep1 TSTRB" *)
input wire [3 : 0] DMA_dep1_TSTRB;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 DMA_dep1 TUSER" *)
input wire [3 : 0] DMA_dep1_TUSER;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 DMA_dep1 TLAST" *)
input wire [0 : 0] DMA_dep1_TLAST;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 DMA_dep1 TID" *)
input wire [4 : 0] DMA_dep1_TID;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 DMA_dep1 TDEST" *)
input wire [4 : 0] DMA_dep1_TDEST;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 DMA_dep2 TVALID" *)
input wire DMA_dep2_TVALID;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 DMA_dep2 TREADY" *)
output wire DMA_dep2_TREADY;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 DMA_dep2 TDATA" *)
input wire [31 : 0] DMA_dep2_TDATA;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 DMA_dep2 TKEEP" *)
input wire [3 : 0] DMA_dep2_TKEEP;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 DMA_dep2 TSTRB" *)
input wire [3 : 0] DMA_dep2_TSTRB;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 DMA_dep2 TUSER" *)
input wire [3 : 0] DMA_dep2_TUSER;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 DMA_dep2 TLAST" *)
input wire [0 : 0] DMA_dep2_TLAST;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 DMA_dep2 TID" *)
input wire [4 : 0] DMA_dep2_TID;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 DMA_dep2 TDEST" *)
input wire [4 : 0] DMA_dep2_TDEST;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 DMA_pcc TVALID" *)
output wire DMA_pcc_TVALID;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 DMA_pcc TREADY" *)
input wire DMA_pcc_TREADY;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 DMA_pcc TDATA" *)
output wire [31 : 0] DMA_pcc_TDATA;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 DMA_pcc TKEEP" *)
output wire [3 : 0] DMA_pcc_TKEEP;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 DMA_pcc TSTRB" *)
output wire [3 : 0] DMA_pcc_TSTRB;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 DMA_pcc TUSER" *)
output wire [3 : 0] DMA_pcc_TUSER;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 DMA_pcc TLAST" *)
output wire [0 : 0] DMA_pcc_TLAST;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 DMA_pcc TID" *)
output wire [4 : 0] DMA_pcc_TID;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 DMA_pcc TDEST" *)
output wire [4 : 0] DMA_pcc_TDEST;

  pcc_stream inst (
    .ap_clk(ap_clk),
    .ap_rst_n(ap_rst_n),
    .DMA_NumberOfCorrelation_TVALID(DMA_NumberOfCorrelation_TVALID),
    .DMA_NumberOfCorrelation_TREADY(DMA_NumberOfCorrelation_TREADY),
    .DMA_NumberOfCorrelation_TDATA(DMA_NumberOfCorrelation_TDATA),
    .DMA_NumberOfCorrelation_TKEEP(DMA_NumberOfCorrelation_TKEEP),
    .DMA_NumberOfCorrelation_TSTRB(DMA_NumberOfCorrelation_TSTRB),
    .DMA_NumberOfCorrelation_TUSER(DMA_NumberOfCorrelation_TUSER),
    .DMA_NumberOfCorrelation_TLAST(DMA_NumberOfCorrelation_TLAST),
    .DMA_NumberOfCorrelation_TID(DMA_NumberOfCorrelation_TID),
    .DMA_NumberOfCorrelation_TDEST(DMA_NumberOfCorrelation_TDEST),
    .DMA_dep1_TVALID(DMA_dep1_TVALID),
    .DMA_dep1_TREADY(DMA_dep1_TREADY),
    .DMA_dep1_TDATA(DMA_dep1_TDATA),
    .DMA_dep1_TKEEP(DMA_dep1_TKEEP),
    .DMA_dep1_TSTRB(DMA_dep1_TSTRB),
    .DMA_dep1_TUSER(DMA_dep1_TUSER),
    .DMA_dep1_TLAST(DMA_dep1_TLAST),
    .DMA_dep1_TID(DMA_dep1_TID),
    .DMA_dep1_TDEST(DMA_dep1_TDEST),
    .DMA_dep2_TVALID(DMA_dep2_TVALID),
    .DMA_dep2_TREADY(DMA_dep2_TREADY),
    .DMA_dep2_TDATA(DMA_dep2_TDATA),
    .DMA_dep2_TKEEP(DMA_dep2_TKEEP),
    .DMA_dep2_TSTRB(DMA_dep2_TSTRB),
    .DMA_dep2_TUSER(DMA_dep2_TUSER),
    .DMA_dep2_TLAST(DMA_dep2_TLAST),
    .DMA_dep2_TID(DMA_dep2_TID),
    .DMA_dep2_TDEST(DMA_dep2_TDEST),
    .DMA_pcc_TVALID(DMA_pcc_TVALID),
    .DMA_pcc_TREADY(DMA_pcc_TREADY),
    .DMA_pcc_TDATA(DMA_pcc_TDATA),
    .DMA_pcc_TKEEP(DMA_pcc_TKEEP),
    .DMA_pcc_TSTRB(DMA_pcc_TSTRB),
    .DMA_pcc_TUSER(DMA_pcc_TUSER),
    .DMA_pcc_TLAST(DMA_pcc_TLAST),
    .DMA_pcc_TID(DMA_pcc_TID),
    .DMA_pcc_TDEST(DMA_pcc_TDEST)
  );
endmodule
