
################################################################
# This is a generated script based on design: design_1
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2015.4
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   puts "ERROR: This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_bd_tcl to create an updated script."

   return 1
}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source design_1_script.tcl

# If you do not already have a project created,
# you can create a project using the following command:
#    create_project project_1 myproj -part xc7vx485tffg1761-2
#    set_property BOARD_PART xilinx.com:vc707:part0:1.2 [current_project]

# CHECKING IF PROJECT EXISTS
if { [get_projects -quiet] eq "" } {
   puts "ERROR: Please open or create a project!"
   return 1
}



# CHANGE DESIGN NAME HERE
set design_name design_1

# If you do not already have an existing IP Integrator design open,
# you can create a design using the following command:
#    create_bd_design $design_name

# Creating design if needed
set errMsg ""
set nRet 0

set cur_design [current_bd_design -quiet]
set list_cells [get_bd_cells -quiet]

if { ${design_name} eq "" } {
   # USE CASES:
   #    1) Design_name not set

   set errMsg "ERROR: Please set the variable <design_name> to a non-empty value."
   set nRet 1

} elseif { ${cur_design} ne "" && ${list_cells} eq "" } {
   # USE CASES:
   #    2): Current design opened AND is empty AND names same.
   #    3): Current design opened AND is empty AND names diff; design_name NOT in project.
   #    4): Current design opened AND is empty AND names diff; design_name exists in project.

   if { $cur_design ne $design_name } {
      puts "INFO: Changing value of <design_name> from <$design_name> to <$cur_design> since current design is empty."
      set design_name [get_property NAME $cur_design]
   }
   puts "INFO: Constructing design in IPI design <$cur_design>..."

} elseif { ${cur_design} ne "" && $list_cells ne "" && $cur_design eq $design_name } {
   # USE CASES:
   #    5) Current design opened AND has components AND same names.

   set errMsg "ERROR: Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 1
} elseif { [get_files -quiet ${design_name}.bd] ne "" } {
   # USE CASES: 
   #    6) Current opened design, has components, but diff names, design_name exists in project.
   #    7) No opened design, design_name exists in project.

   set errMsg "ERROR: Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 2

} else {
   # USE CASES:
   #    8) No opened design, design_name not in project.
   #    9) Current opened design, has components, but diff names, design_name not in project.

   puts "INFO: Currently there is no design <$design_name> in project, so creating one..."

   create_bd_design $design_name

   puts "INFO: Making design <$design_name> as current_bd_design."
   current_bd_design $design_name

}

puts "INFO: Currently the variable <design_name> is equal to \"$design_name\"."

if { $nRet != 0 } {
   puts $errMsg
   return $nRet
}

##################################################################
# DESIGN PROCs
##################################################################


# Hierarchical cell: microblaze_0_local_memory
proc create_hier_cell_microblaze_0_local_memory { parentCell nameHier } {

  if { $parentCell eq "" || $nameHier eq "" } {
     puts "ERROR: create_hier_cell_microblaze_0_local_memory() - Empty argument(s)!"
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     puts "ERROR: Unable to find parent cell <$parentCell>!"
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     puts "ERROR: Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode MirroredMaster -vlnv xilinx.com:interface:lmb_rtl:1.0 DLMB
  create_bd_intf_pin -mode MirroredMaster -vlnv xilinx.com:interface:lmb_rtl:1.0 ILMB

  # Create pins
  create_bd_pin -dir I -type clk LMB_Clk
  create_bd_pin -dir I -from 0 -to 0 -type rst SYS_Rst

  # Create instance: dlmb_bram_if_cntlr, and set properties
  set dlmb_bram_if_cntlr [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_bram_if_cntlr:4.0 dlmb_bram_if_cntlr ]
  set_property -dict [ list \
CONFIG.C_ECC {0} \
 ] $dlmb_bram_if_cntlr

  # Create instance: dlmb_v10, and set properties
  set dlmb_v10 [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_v10:3.0 dlmb_v10 ]

  # Create instance: ilmb_bram_if_cntlr, and set properties
  set ilmb_bram_if_cntlr [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_bram_if_cntlr:4.0 ilmb_bram_if_cntlr ]
  set_property -dict [ list \
CONFIG.C_ECC {0} \
 ] $ilmb_bram_if_cntlr

  # Create instance: ilmb_v10, and set properties
  set ilmb_v10 [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_v10:3.0 ilmb_v10 ]

  # Create instance: lmb_bram, and set properties
  set lmb_bram [ create_bd_cell -type ip -vlnv xilinx.com:ip:blk_mem_gen:8.3 lmb_bram ]
  set_property -dict [ list \
CONFIG.Memory_Type {True_Dual_Port_RAM} \
CONFIG.use_bram_block {BRAM_Controller} \
 ] $lmb_bram

  # Create interface connections
  connect_bd_intf_net -intf_net microblaze_0_dlmb [get_bd_intf_pins DLMB] [get_bd_intf_pins dlmb_v10/LMB_M]
  connect_bd_intf_net -intf_net microblaze_0_dlmb_bus [get_bd_intf_pins dlmb_bram_if_cntlr/SLMB] [get_bd_intf_pins dlmb_v10/LMB_Sl_0]
  connect_bd_intf_net -intf_net microblaze_0_dlmb_cntlr [get_bd_intf_pins dlmb_bram_if_cntlr/BRAM_PORT] [get_bd_intf_pins lmb_bram/BRAM_PORTA]
  connect_bd_intf_net -intf_net microblaze_0_ilmb [get_bd_intf_pins ILMB] [get_bd_intf_pins ilmb_v10/LMB_M]
  connect_bd_intf_net -intf_net microblaze_0_ilmb_bus [get_bd_intf_pins ilmb_bram_if_cntlr/SLMB] [get_bd_intf_pins ilmb_v10/LMB_Sl_0]
  connect_bd_intf_net -intf_net microblaze_0_ilmb_cntlr [get_bd_intf_pins ilmb_bram_if_cntlr/BRAM_PORT] [get_bd_intf_pins lmb_bram/BRAM_PORTB]

  # Create port connections
  connect_bd_net -net SYS_Rst_1 [get_bd_pins SYS_Rst] [get_bd_pins dlmb_bram_if_cntlr/LMB_Rst] [get_bd_pins dlmb_v10/SYS_Rst] [get_bd_pins ilmb_bram_if_cntlr/LMB_Rst] [get_bd_pins ilmb_v10/SYS_Rst]
  connect_bd_net -net microblaze_0_Clk [get_bd_pins LMB_Clk] [get_bd_pins dlmb_bram_if_cntlr/LMB_Clk] [get_bd_pins dlmb_v10/LMB_Clk] [get_bd_pins ilmb_bram_if_cntlr/LMB_Clk] [get_bd_pins ilmb_v10/LMB_Clk]

  # Restore current instance
  current_bd_instance $oldCurInst
}


# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     puts "ERROR: Unable to find parent cell <$parentCell>!"
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     puts "ERROR: Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports
  set ddr3_sdram [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:ddrx_rtl:1.0 ddr3_sdram ]
  set sys_diff_clock [ create_bd_intf_port -mode Slave -vlnv xilinx.com:interface:diff_clock_rtl:1.0 sys_diff_clock ]

  # Create ports
  set reset [ create_bd_port -dir I -type rst reset ]
  set_property -dict [ list \
CONFIG.POLARITY {ACTIVE_HIGH} \
 ] $reset

  # Create instance: axi_dma_0, and set properties
  set axi_dma_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_dma:7.1 axi_dma_0 ]
  set_property -dict [ list \
CONFIG.c_enable_multi_channel {1} \
CONFIG.c_num_mm2s_channels {3} \
CONFIG.c_sg_include_stscntrl_strm {0} \
CONFIG.c_sg_length_width {23} \
 ] $axi_dma_0

  # Create instance: axi_dma_1, and set properties
  set axi_dma_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_dma:7.1 axi_dma_1 ]
  set_property -dict [ list \
CONFIG.c_enable_multi_channel {1} \
CONFIG.c_num_mm2s_channels {3} \
CONFIG.c_sg_include_stscntrl_strm {0} \
CONFIG.c_sg_length_width {23} \
 ] $axi_dma_1

  # Create instance: axi_dma_2, and set properties
  set axi_dma_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_dma:7.1 axi_dma_2 ]
  set_property -dict [ list \
CONFIG.c_enable_multi_channel {1} \
CONFIG.c_num_mm2s_channels {3} \
CONFIG.c_sg_include_stscntrl_strm {0} \
CONFIG.c_sg_length_width {23} \
 ] $axi_dma_2

  # Create instance: axi_dma_3, and set properties
  set axi_dma_3 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_dma:7.1 axi_dma_3 ]
  set_property -dict [ list \
CONFIG.c_enable_multi_channel {1} \
CONFIG.c_num_mm2s_channels {3} \
CONFIG.c_sg_include_stscntrl_strm {0} \
CONFIG.c_sg_length_width {23} \
 ] $axi_dma_3

  # Create instance: axi_intc_0, and set properties
  set axi_intc_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_intc:4.1 axi_intc_0 ]
  set_property -dict [ list \
CONFIG.C_HAS_FAST {1} \
 ] $axi_intc_0

  # Create instance: axi_mem_intercon, and set properties
  set axi_mem_intercon [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 axi_mem_intercon ]
  set_property -dict [ list \
CONFIG.ENABLE_ADVANCED_OPTIONS {1} \
CONFIG.NUM_MI {1} \
CONFIG.NUM_SI {14} \
CONFIG.S00_HAS_DATA_FIFO {2} \
CONFIG.S01_HAS_DATA_FIFO {2} \
CONFIG.S02_HAS_DATA_FIFO {2} \
CONFIG.S03_HAS_DATA_FIFO {2} \
CONFIG.S04_HAS_DATA_FIFO {2} \
CONFIG.S05_HAS_DATA_FIFO {2} \
CONFIG.S06_HAS_DATA_FIFO {2} \
CONFIG.S07_HAS_DATA_FIFO {2} \
CONFIG.S08_HAS_DATA_FIFO {2} \
CONFIG.S09_HAS_DATA_FIFO {2} \
CONFIG.S10_HAS_DATA_FIFO {2} \
CONFIG.S11_HAS_DATA_FIFO {2} \
CONFIG.S12_HAS_DATA_FIFO {2} \
CONFIG.S13_HAS_DATA_FIFO {2} \
CONFIG.STRATEGY {2} \
CONFIG.XBAR_DATA_WIDTH {512} \
 ] $axi_mem_intercon

  # Create instance: axi_timer_0, and set properties
  set axi_timer_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_timer:2.0 axi_timer_0 ]
  set_property -dict [ list \
CONFIG.mode_64bit {1} \
 ] $axi_timer_0

  # Create instance: axis_data_fifo_0, and set properties
  set axis_data_fifo_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:1.1 axis_data_fifo_0 ]
  set_property -dict [ list \
CONFIG.FIFO_DEPTH {4096} \
 ] $axis_data_fifo_0

  # Create instance: axis_data_fifo_1, and set properties
  set axis_data_fifo_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:1.1 axis_data_fifo_1 ]
  set_property -dict [ list \
CONFIG.FIFO_DEPTH {4096} \
 ] $axis_data_fifo_1

  # Create instance: axis_data_fifo_2, and set properties
  set axis_data_fifo_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:1.1 axis_data_fifo_2 ]
  set_property -dict [ list \
CONFIG.FIFO_DEPTH {4096} \
 ] $axis_data_fifo_2

  # Create instance: axis_data_fifo_3, and set properties
  set axis_data_fifo_3 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:1.1 axis_data_fifo_3 ]
  set_property -dict [ list \
CONFIG.FIFO_DEPTH {4096} \
 ] $axis_data_fifo_3

  # Create instance: axis_interconnect_0, and set properties
  set axis_interconnect_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_interconnect:2.1 axis_interconnect_0 ]
  set_property -dict [ list \
CONFIG.M00_FIFO_DEPTH {4096} \
CONFIG.M01_FIFO_DEPTH {4096} \
CONFIG.M02_FIFO_DEPTH {4096} \
CONFIG.NUM_MI {3} \
 ] $axis_interconnect_0

  # Create instance: axis_interconnect_1, and set properties
  set axis_interconnect_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_interconnect:2.1 axis_interconnect_1 ]
  set_property -dict [ list \
CONFIG.M00_FIFO_DEPTH {4096} \
CONFIG.M01_FIFO_DEPTH {4096} \
CONFIG.M02_FIFO_DEPTH {4096} \
CONFIG.NUM_MI {3} \
 ] $axis_interconnect_1

  # Create instance: axis_interconnect_2, and set properties
  set axis_interconnect_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_interconnect:2.1 axis_interconnect_2 ]
  set_property -dict [ list \
CONFIG.M00_FIFO_DEPTH {4096} \
CONFIG.M01_FIFO_DEPTH {4096} \
CONFIG.M02_FIFO_DEPTH {4096} \
CONFIG.NUM_MI {3} \
 ] $axis_interconnect_2

  # Create instance: axis_interconnect_3, and set properties
  set axis_interconnect_3 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_interconnect:2.1 axis_interconnect_3 ]
  set_property -dict [ list \
CONFIG.M00_FIFO_DEPTH {4096} \
CONFIG.M01_FIFO_DEPTH {4096} \
CONFIG.M02_FIFO_DEPTH {4096} \
CONFIG.NUM_MI {3} \
 ] $axis_interconnect_3

  # Create instance: mdm_1, and set properties
  set mdm_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:mdm:3.2 mdm_1 ]
  set_property -dict [ list \
CONFIG.C_USE_UART {1} \
 ] $mdm_1

  # Create instance: microblaze_0, and set properties
  set microblaze_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:microblaze:9.5 microblaze_0 ]
  set_property -dict [ list \
CONFIG.C_CACHE_BYTE_SIZE {8192} \
CONFIG.C_DCACHE_BYTE_SIZE {8192} \
CONFIG.C_DCACHE_LINE_LEN {8} \
CONFIG.C_DCACHE_USE_WRITEBACK {1} \
CONFIG.C_DCACHE_VICTIMS {8} \
CONFIG.C_DEBUG_ENABLED {1} \
CONFIG.C_D_AXI {1} \
CONFIG.C_D_LMB {1} \
CONFIG.C_ICACHE_LINE_LEN {8} \
CONFIG.C_ICACHE_STREAMS {1} \
CONFIG.C_ICACHE_VICTIMS {8} \
CONFIG.C_I_LMB {1} \
CONFIG.C_MMU_ZONES {2} \
CONFIG.C_USE_BARREL {1} \
CONFIG.C_USE_BRANCH_TARGET_CACHE {1} \
CONFIG.C_USE_DCACHE {1} \
CONFIG.C_USE_DIV {1} \
CONFIG.C_USE_FPU {2} \
CONFIG.C_USE_HW_MUL {2} \
CONFIG.C_USE_ICACHE {1} \
CONFIG.C_USE_MSR_INSTR {1} \
CONFIG.C_USE_PCMP_INSTR {1} \
CONFIG.G_TEMPLATE_LIST {2} \
 ] $microblaze_0

  # Create instance: microblaze_0_axi_periph, and set properties
  set microblaze_0_axi_periph [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 microblaze_0_axi_periph ]
  set_property -dict [ list \
CONFIG.NUM_MI {7} \
CONFIG.S00_HAS_DATA_FIFO {2} \
CONFIG.STRATEGY {2} \
 ] $microblaze_0_axi_periph

  # Create instance: microblaze_0_local_memory
  create_hier_cell_microblaze_0_local_memory [current_bd_instance .] microblaze_0_local_memory

  # Create instance: mig_7series_0, and set properties
  set mig_7series_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:mig_7series:2.4 mig_7series_0 ]
  set_property -dict [ list \
CONFIG.BOARD_MIG_PARAM {ddr3_sdram} \
CONFIG.RESET_BOARD_INTERFACE {reset} \
 ] $mig_7series_0

  # Create instance: pcc_stream_0, and set properties
  set pcc_stream_0 [ create_bd_cell -type ip -vlnv xilinx.com:hls:pcc_stream:1.0 pcc_stream_0 ]

  # Create instance: pcc_stream_1, and set properties
  set pcc_stream_1 [ create_bd_cell -type ip -vlnv xilinx.com:hls:pcc_stream:1.0 pcc_stream_1 ]

  # Create instance: pcc_stream_2, and set properties
  set pcc_stream_2 [ create_bd_cell -type ip -vlnv xilinx.com:hls:pcc_stream:1.0 pcc_stream_2 ]

  # Create instance: pcc_stream_3, and set properties
  set pcc_stream_3 [ create_bd_cell -type ip -vlnv xilinx.com:hls:pcc_stream:1.0 pcc_stream_3 ]

  # Create instance: rst_mig_7series_0_100M, and set properties
  set rst_mig_7series_0_100M [ create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset:5.0 rst_mig_7series_0_100M ]

  # Create instance: xlconcat_0, and set properties
  set xlconcat_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 xlconcat_0 ]
  set_property -dict [ list \
CONFIG.NUM_PORTS {8} \
 ] $xlconcat_0

  # Create interface connections
  connect_bd_intf_net -intf_net axi_dma_0_M_AXIS_MM2S [get_bd_intf_pins axi_dma_0/M_AXIS_MM2S] [get_bd_intf_pins axis_interconnect_0/S00_AXIS]
  connect_bd_intf_net -intf_net axi_dma_0_M_AXI_MM2S [get_bd_intf_pins axi_dma_0/M_AXI_MM2S] [get_bd_intf_pins axi_mem_intercon/S03_AXI]
  connect_bd_intf_net -intf_net axi_dma_0_M_AXI_S2MM [get_bd_intf_pins axi_dma_0/M_AXI_S2MM] [get_bd_intf_pins axi_mem_intercon/S04_AXI]
  connect_bd_intf_net -intf_net axi_dma_0_M_AXI_SG [get_bd_intf_pins axi_dma_0/M_AXI_SG] [get_bd_intf_pins axi_mem_intercon/S02_AXI]
  connect_bd_intf_net -intf_net axi_dma_1_M_AXIS_MM2S [get_bd_intf_pins axi_dma_1/M_AXIS_MM2S] [get_bd_intf_pins axis_interconnect_1/S00_AXIS]
  connect_bd_intf_net -intf_net axi_dma_1_M_AXI_MM2S [get_bd_intf_pins axi_dma_1/M_AXI_MM2S] [get_bd_intf_pins axi_mem_intercon/S06_AXI]
  connect_bd_intf_net -intf_net axi_dma_1_M_AXI_S2MM [get_bd_intf_pins axi_dma_1/M_AXI_S2MM] [get_bd_intf_pins axi_mem_intercon/S07_AXI]
  connect_bd_intf_net -intf_net axi_dma_1_M_AXI_SG [get_bd_intf_pins axi_dma_1/M_AXI_SG] [get_bd_intf_pins axi_mem_intercon/S05_AXI]
  connect_bd_intf_net -intf_net axi_dma_2_M_AXIS_MM2S [get_bd_intf_pins axi_dma_2/M_AXIS_MM2S] [get_bd_intf_pins axis_interconnect_2/S00_AXIS]
  connect_bd_intf_net -intf_net axi_dma_2_M_AXI_MM2S [get_bd_intf_pins axi_dma_2/M_AXI_MM2S] [get_bd_intf_pins axi_mem_intercon/S09_AXI]
  connect_bd_intf_net -intf_net axi_dma_2_M_AXI_S2MM [get_bd_intf_pins axi_dma_2/M_AXI_S2MM] [get_bd_intf_pins axi_mem_intercon/S10_AXI]
  connect_bd_intf_net -intf_net axi_dma_2_M_AXI_SG [get_bd_intf_pins axi_dma_2/M_AXI_SG] [get_bd_intf_pins axi_mem_intercon/S08_AXI]
  connect_bd_intf_net -intf_net axi_dma_3_M_AXIS_MM2S [get_bd_intf_pins axi_dma_3/M_AXIS_MM2S] [get_bd_intf_pins axis_interconnect_3/S00_AXIS]
  connect_bd_intf_net -intf_net axi_dma_3_M_AXI_MM2S [get_bd_intf_pins axi_dma_3/M_AXI_MM2S] [get_bd_intf_pins axi_mem_intercon/S12_AXI]
  connect_bd_intf_net -intf_net axi_dma_3_M_AXI_S2MM [get_bd_intf_pins axi_dma_3/M_AXI_S2MM] [get_bd_intf_pins axi_mem_intercon/S13_AXI]
  connect_bd_intf_net -intf_net axi_dma_3_M_AXI_SG [get_bd_intf_pins axi_dma_3/M_AXI_SG] [get_bd_intf_pins axi_mem_intercon/S11_AXI]
  connect_bd_intf_net -intf_net axi_intc_0_interrupt [get_bd_intf_pins axi_intc_0/interrupt] [get_bd_intf_pins microblaze_0/INTERRUPT]
  connect_bd_intf_net -intf_net axi_mem_intercon_M00_AXI [get_bd_intf_pins axi_mem_intercon/M00_AXI] [get_bd_intf_pins mig_7series_0/S_AXI]
  connect_bd_intf_net -intf_net axis_data_fifo_0_M_AXIS [get_bd_intf_pins axi_dma_0/S_AXIS_S2MM] [get_bd_intf_pins axis_data_fifo_0/M_AXIS]
  connect_bd_intf_net -intf_net axis_data_fifo_1_M_AXIS [get_bd_intf_pins axi_dma_1/S_AXIS_S2MM] [get_bd_intf_pins axis_data_fifo_1/M_AXIS]
  connect_bd_intf_net -intf_net axis_data_fifo_2_M_AXIS [get_bd_intf_pins axi_dma_2/S_AXIS_S2MM] [get_bd_intf_pins axis_data_fifo_2/M_AXIS]
  connect_bd_intf_net -intf_net axis_data_fifo_3_M_AXIS [get_bd_intf_pins axi_dma_3/S_AXIS_S2MM] [get_bd_intf_pins axis_data_fifo_3/M_AXIS]
  connect_bd_intf_net -intf_net axis_interconnect_0_M00_AXIS [get_bd_intf_pins axis_interconnect_0/M00_AXIS] [get_bd_intf_pins pcc_stream_0/DMA_NumberOfCorrelation]
  connect_bd_intf_net -intf_net axis_interconnect_0_M01_AXIS [get_bd_intf_pins axis_interconnect_0/M01_AXIS] [get_bd_intf_pins pcc_stream_0/DMA_dep1]
  connect_bd_intf_net -intf_net axis_interconnect_0_M02_AXIS [get_bd_intf_pins axis_interconnect_0/M02_AXIS] [get_bd_intf_pins pcc_stream_0/DMA_dep2]
  connect_bd_intf_net -intf_net axis_interconnect_1_M00_AXIS [get_bd_intf_pins axis_interconnect_1/M00_AXIS] [get_bd_intf_pins pcc_stream_1/DMA_NumberOfCorrelation]
  connect_bd_intf_net -intf_net axis_interconnect_1_M01_AXIS [get_bd_intf_pins axis_interconnect_1/M01_AXIS] [get_bd_intf_pins pcc_stream_1/DMA_dep1]
  connect_bd_intf_net -intf_net axis_interconnect_1_M02_AXIS [get_bd_intf_pins axis_interconnect_1/M02_AXIS] [get_bd_intf_pins pcc_stream_1/DMA_dep2]
  connect_bd_intf_net -intf_net axis_interconnect_2_M00_AXIS [get_bd_intf_pins axis_interconnect_2/M00_AXIS] [get_bd_intf_pins pcc_stream_2/DMA_NumberOfCorrelation]
  connect_bd_intf_net -intf_net axis_interconnect_2_M01_AXIS [get_bd_intf_pins axis_interconnect_2/M01_AXIS] [get_bd_intf_pins pcc_stream_2/DMA_dep1]
  connect_bd_intf_net -intf_net axis_interconnect_2_M02_AXIS [get_bd_intf_pins axis_interconnect_2/M02_AXIS] [get_bd_intf_pins pcc_stream_2/DMA_dep2]
  connect_bd_intf_net -intf_net axis_interconnect_3_M00_AXIS [get_bd_intf_pins axis_interconnect_3/M00_AXIS] [get_bd_intf_pins pcc_stream_3/DMA_NumberOfCorrelation]
  connect_bd_intf_net -intf_net axis_interconnect_3_M01_AXIS [get_bd_intf_pins axis_interconnect_3/M01_AXIS] [get_bd_intf_pins pcc_stream_3/DMA_dep1]
  connect_bd_intf_net -intf_net axis_interconnect_3_M02_AXIS [get_bd_intf_pins axis_interconnect_3/M02_AXIS] [get_bd_intf_pins pcc_stream_3/DMA_dep2]
  connect_bd_intf_net -intf_net microblaze_0_M_AXI_DC [get_bd_intf_pins axi_mem_intercon/S00_AXI] [get_bd_intf_pins microblaze_0/M_AXI_DC]
  connect_bd_intf_net -intf_net microblaze_0_M_AXI_DP [get_bd_intf_pins microblaze_0/M_AXI_DP] [get_bd_intf_pins microblaze_0_axi_periph/S00_AXI]
  connect_bd_intf_net -intf_net microblaze_0_M_AXI_IC [get_bd_intf_pins axi_mem_intercon/S01_AXI] [get_bd_intf_pins microblaze_0/M_AXI_IC]
  connect_bd_intf_net -intf_net microblaze_0_axi_periph_M00_AXI [get_bd_intf_pins mdm_1/S_AXI] [get_bd_intf_pins microblaze_0_axi_periph/M00_AXI]
  connect_bd_intf_net -intf_net microblaze_0_axi_periph_M01_AXI [get_bd_intf_pins axi_timer_0/S_AXI] [get_bd_intf_pins microblaze_0_axi_periph/M01_AXI]
  connect_bd_intf_net -intf_net microblaze_0_axi_periph_M02_AXI [get_bd_intf_pins axi_intc_0/s_axi] [get_bd_intf_pins microblaze_0_axi_periph/M02_AXI]
  connect_bd_intf_net -intf_net microblaze_0_axi_periph_M03_AXI [get_bd_intf_pins axi_dma_0/S_AXI_LITE] [get_bd_intf_pins microblaze_0_axi_periph/M03_AXI]
  connect_bd_intf_net -intf_net microblaze_0_axi_periph_M04_AXI [get_bd_intf_pins axi_dma_1/S_AXI_LITE] [get_bd_intf_pins microblaze_0_axi_periph/M04_AXI]
  connect_bd_intf_net -intf_net microblaze_0_axi_periph_M05_AXI [get_bd_intf_pins axi_dma_2/S_AXI_LITE] [get_bd_intf_pins microblaze_0_axi_periph/M05_AXI]
  connect_bd_intf_net -intf_net microblaze_0_axi_periph_M06_AXI [get_bd_intf_pins axi_dma_3/S_AXI_LITE] [get_bd_intf_pins microblaze_0_axi_periph/M06_AXI]
  connect_bd_intf_net -intf_net microblaze_0_debug [get_bd_intf_pins mdm_1/MBDEBUG_0] [get_bd_intf_pins microblaze_0/DEBUG]
  connect_bd_intf_net -intf_net microblaze_0_dlmb_1 [get_bd_intf_pins microblaze_0/DLMB] [get_bd_intf_pins microblaze_0_local_memory/DLMB]
  connect_bd_intf_net -intf_net microblaze_0_ilmb_1 [get_bd_intf_pins microblaze_0/ILMB] [get_bd_intf_pins microblaze_0_local_memory/ILMB]
  connect_bd_intf_net -intf_net mig_7series_0_DDR3 [get_bd_intf_ports ddr3_sdram] [get_bd_intf_pins mig_7series_0/DDR3]
  connect_bd_intf_net -intf_net pcc_stream_0_DMA_pcc [get_bd_intf_pins axis_data_fifo_0/S_AXIS] [get_bd_intf_pins pcc_stream_0/DMA_pcc]
  connect_bd_intf_net -intf_net pcc_stream_1_DMA_pcc [get_bd_intf_pins axis_data_fifo_1/S_AXIS] [get_bd_intf_pins pcc_stream_1/DMA_pcc]
  connect_bd_intf_net -intf_net pcc_stream_2_DMA_pcc [get_bd_intf_pins axis_data_fifo_2/S_AXIS] [get_bd_intf_pins pcc_stream_2/DMA_pcc]
  connect_bd_intf_net -intf_net pcc_stream_3_DMA_pcc [get_bd_intf_pins axis_data_fifo_3/S_AXIS] [get_bd_intf_pins pcc_stream_3/DMA_pcc]
  connect_bd_intf_net -intf_net sys_diff_clock_1 [get_bd_intf_ports sys_diff_clock] [get_bd_intf_pins mig_7series_0/SYS_CLK]

  # Create port connections
  connect_bd_net -net axi_dma_0_mm2s_introut [get_bd_pins axi_dma_0/mm2s_introut] [get_bd_pins xlconcat_0/In0]
  connect_bd_net -net axi_dma_0_s2mm_introut [get_bd_pins axi_dma_0/s2mm_introut] [get_bd_pins xlconcat_0/In1]
  connect_bd_net -net axi_dma_1_mm2s_introut [get_bd_pins axi_dma_1/mm2s_introut] [get_bd_pins xlconcat_0/In2]
  connect_bd_net -net axi_dma_1_s2mm_introut [get_bd_pins axi_dma_1/s2mm_introut] [get_bd_pins xlconcat_0/In3]
  connect_bd_net -net axi_dma_2_mm2s_introut [get_bd_pins axi_dma_2/mm2s_introut] [get_bd_pins xlconcat_0/In4]
  connect_bd_net -net axi_dma_2_s2mm_introut [get_bd_pins axi_dma_2/s2mm_introut] [get_bd_pins xlconcat_0/In5]
  connect_bd_net -net axi_dma_3_mm2s_introut [get_bd_pins axi_dma_3/mm2s_introut] [get_bd_pins xlconcat_0/In6]
  connect_bd_net -net axi_dma_3_s2mm_introut [get_bd_pins axi_dma_3/s2mm_introut] [get_bd_pins xlconcat_0/In7]
  connect_bd_net -net mdm_1_debug_sys_rst [get_bd_pins mdm_1/Debug_SYS_Rst] [get_bd_pins rst_mig_7series_0_100M/mb_debug_sys_rst]
  connect_bd_net -net microblaze_0_Clk [get_bd_pins axi_dma_0/m_axi_mm2s_aclk] [get_bd_pins axi_dma_0/m_axi_s2mm_aclk] [get_bd_pins axi_dma_0/m_axi_sg_aclk] [get_bd_pins axi_dma_0/s_axi_lite_aclk] [get_bd_pins axi_dma_1/m_axi_mm2s_aclk] [get_bd_pins axi_dma_1/m_axi_s2mm_aclk] [get_bd_pins axi_dma_1/m_axi_sg_aclk] [get_bd_pins axi_dma_1/s_axi_lite_aclk] [get_bd_pins axi_dma_2/m_axi_mm2s_aclk] [get_bd_pins axi_dma_2/m_axi_s2mm_aclk] [get_bd_pins axi_dma_2/m_axi_sg_aclk] [get_bd_pins axi_dma_2/s_axi_lite_aclk] [get_bd_pins axi_dma_3/m_axi_mm2s_aclk] [get_bd_pins axi_dma_3/m_axi_s2mm_aclk] [get_bd_pins axi_dma_3/m_axi_sg_aclk] [get_bd_pins axi_dma_3/s_axi_lite_aclk] [get_bd_pins axi_intc_0/processor_clk] [get_bd_pins axi_intc_0/s_axi_aclk] [get_bd_pins axi_mem_intercon/ACLK] [get_bd_pins axi_mem_intercon/M00_ACLK] [get_bd_pins axi_mem_intercon/S00_ACLK] [get_bd_pins axi_mem_intercon/S01_ACLK] [get_bd_pins axi_mem_intercon/S02_ACLK] [get_bd_pins axi_mem_intercon/S03_ACLK] [get_bd_pins axi_mem_intercon/S04_ACLK] [get_bd_pins axi_mem_intercon/S05_ACLK] [get_bd_pins axi_mem_intercon/S06_ACLK] [get_bd_pins axi_mem_intercon/S07_ACLK] [get_bd_pins axi_mem_intercon/S08_ACLK] [get_bd_pins axi_mem_intercon/S09_ACLK] [get_bd_pins axi_mem_intercon/S10_ACLK] [get_bd_pins axi_mem_intercon/S11_ACLK] [get_bd_pins axi_mem_intercon/S12_ACLK] [get_bd_pins axi_mem_intercon/S13_ACLK] [get_bd_pins axi_timer_0/s_axi_aclk] [get_bd_pins axis_data_fifo_0/s_axis_aclk] [get_bd_pins axis_data_fifo_1/s_axis_aclk] [get_bd_pins axis_data_fifo_2/s_axis_aclk] [get_bd_pins axis_data_fifo_3/s_axis_aclk] [get_bd_pins axis_interconnect_0/ACLK] [get_bd_pins axis_interconnect_0/M00_AXIS_ACLK] [get_bd_pins axis_interconnect_0/M01_AXIS_ACLK] [get_bd_pins axis_interconnect_0/M02_AXIS_ACLK] [get_bd_pins axis_interconnect_0/S00_AXIS_ACLK] [get_bd_pins axis_interconnect_1/ACLK] [get_bd_pins axis_interconnect_1/M00_AXIS_ACLK] [get_bd_pins axis_interconnect_1/M01_AXIS_ACLK] [get_bd_pins axis_interconnect_1/M02_AXIS_ACLK] [get_bd_pins axis_interconnect_1/S00_AXIS_ACLK] [get_bd_pins axis_interconnect_2/ACLK] [get_bd_pins axis_interconnect_2/M00_AXIS_ACLK] [get_bd_pins axis_interconnect_2/M01_AXIS_ACLK] [get_bd_pins axis_interconnect_2/M02_AXIS_ACLK] [get_bd_pins axis_interconnect_2/S00_AXIS_ACLK] [get_bd_pins axis_interconnect_3/ACLK] [get_bd_pins axis_interconnect_3/M00_AXIS_ACLK] [get_bd_pins axis_interconnect_3/M01_AXIS_ACLK] [get_bd_pins axis_interconnect_3/M02_AXIS_ACLK] [get_bd_pins axis_interconnect_3/S00_AXIS_ACLK] [get_bd_pins mdm_1/S_AXI_ACLK] [get_bd_pins microblaze_0/Clk] [get_bd_pins microblaze_0_axi_periph/ACLK] [get_bd_pins microblaze_0_axi_periph/M00_ACLK] [get_bd_pins microblaze_0_axi_periph/M01_ACLK] [get_bd_pins microblaze_0_axi_periph/M02_ACLK] [get_bd_pins microblaze_0_axi_periph/M03_ACLK] [get_bd_pins microblaze_0_axi_periph/M04_ACLK] [get_bd_pins microblaze_0_axi_periph/M05_ACLK] [get_bd_pins microblaze_0_axi_periph/M06_ACLK] [get_bd_pins microblaze_0_axi_periph/S00_ACLK] [get_bd_pins microblaze_0_local_memory/LMB_Clk] [get_bd_pins mig_7series_0/ui_clk] [get_bd_pins pcc_stream_0/ap_clk] [get_bd_pins pcc_stream_1/ap_clk] [get_bd_pins pcc_stream_2/ap_clk] [get_bd_pins pcc_stream_3/ap_clk] [get_bd_pins rst_mig_7series_0_100M/slowest_sync_clk]
  connect_bd_net -net mig_7series_0_mmcm_locked [get_bd_pins mig_7series_0/mmcm_locked] [get_bd_pins rst_mig_7series_0_100M/dcm_locked]
  connect_bd_net -net mig_7series_0_ui_clk_sync_rst [get_bd_pins mig_7series_0/ui_clk_sync_rst] [get_bd_pins rst_mig_7series_0_100M/ext_reset_in]
  connect_bd_net -net reset_1 [get_bd_ports reset] [get_bd_pins mig_7series_0/sys_rst]
  connect_bd_net -net rst_mig_7series_0_100M_bus_struct_reset [get_bd_pins microblaze_0_local_memory/SYS_Rst] [get_bd_pins rst_mig_7series_0_100M/bus_struct_reset]
  connect_bd_net -net rst_mig_7series_0_100M_interconnect_aresetn [get_bd_pins axi_mem_intercon/ARESETN] [get_bd_pins microblaze_0_axi_periph/ARESETN] [get_bd_pins rst_mig_7series_0_100M/interconnect_aresetn]
  connect_bd_net -net rst_mig_7series_0_100M_mb_reset [get_bd_pins axi_intc_0/processor_rst] [get_bd_pins microblaze_0/Reset] [get_bd_pins rst_mig_7series_0_100M/mb_reset]
  connect_bd_net -net rst_mig_7series_0_100M_peripheral_aresetn [get_bd_pins axi_dma_0/axi_resetn] [get_bd_pins axi_dma_1/axi_resetn] [get_bd_pins axi_dma_2/axi_resetn] [get_bd_pins axi_dma_3/axi_resetn] [get_bd_pins axi_intc_0/s_axi_aresetn] [get_bd_pins axi_mem_intercon/M00_ARESETN] [get_bd_pins axi_mem_intercon/S00_ARESETN] [get_bd_pins axi_mem_intercon/S01_ARESETN] [get_bd_pins axi_mem_intercon/S02_ARESETN] [get_bd_pins axi_mem_intercon/S03_ARESETN] [get_bd_pins axi_mem_intercon/S04_ARESETN] [get_bd_pins axi_mem_intercon/S05_ARESETN] [get_bd_pins axi_mem_intercon/S06_ARESETN] [get_bd_pins axi_mem_intercon/S07_ARESETN] [get_bd_pins axi_mem_intercon/S08_ARESETN] [get_bd_pins axi_mem_intercon/S09_ARESETN] [get_bd_pins axi_mem_intercon/S10_ARESETN] [get_bd_pins axi_mem_intercon/S11_ARESETN] [get_bd_pins axi_mem_intercon/S12_ARESETN] [get_bd_pins axi_mem_intercon/S13_ARESETN] [get_bd_pins axi_timer_0/s_axi_aresetn] [get_bd_pins axis_data_fifo_0/s_axis_aresetn] [get_bd_pins axis_data_fifo_1/s_axis_aresetn] [get_bd_pins axis_data_fifo_2/s_axis_aresetn] [get_bd_pins axis_data_fifo_3/s_axis_aresetn] [get_bd_pins axis_interconnect_0/ARESETN] [get_bd_pins axis_interconnect_0/M00_AXIS_ARESETN] [get_bd_pins axis_interconnect_0/M01_AXIS_ARESETN] [get_bd_pins axis_interconnect_0/M02_AXIS_ARESETN] [get_bd_pins axis_interconnect_0/S00_AXIS_ARESETN] [get_bd_pins axis_interconnect_1/ARESETN] [get_bd_pins axis_interconnect_1/M00_AXIS_ARESETN] [get_bd_pins axis_interconnect_1/M01_AXIS_ARESETN] [get_bd_pins axis_interconnect_1/M02_AXIS_ARESETN] [get_bd_pins axis_interconnect_1/S00_AXIS_ARESETN] [get_bd_pins axis_interconnect_2/ARESETN] [get_bd_pins axis_interconnect_2/M00_AXIS_ARESETN] [get_bd_pins axis_interconnect_2/M01_AXIS_ARESETN] [get_bd_pins axis_interconnect_2/M02_AXIS_ARESETN] [get_bd_pins axis_interconnect_2/S00_AXIS_ARESETN] [get_bd_pins axis_interconnect_3/ARESETN] [get_bd_pins axis_interconnect_3/M00_AXIS_ARESETN] [get_bd_pins axis_interconnect_3/M01_AXIS_ARESETN] [get_bd_pins axis_interconnect_3/M02_AXIS_ARESETN] [get_bd_pins axis_interconnect_3/S00_AXIS_ARESETN] [get_bd_pins mdm_1/S_AXI_ARESETN] [get_bd_pins microblaze_0_axi_periph/M00_ARESETN] [get_bd_pins microblaze_0_axi_periph/M01_ARESETN] [get_bd_pins microblaze_0_axi_periph/M02_ARESETN] [get_bd_pins microblaze_0_axi_periph/M03_ARESETN] [get_bd_pins microblaze_0_axi_periph/M04_ARESETN] [get_bd_pins microblaze_0_axi_periph/M05_ARESETN] [get_bd_pins microblaze_0_axi_periph/M06_ARESETN] [get_bd_pins microblaze_0_axi_periph/S00_ARESETN] [get_bd_pins mig_7series_0/aresetn] [get_bd_pins pcc_stream_0/ap_rst_n] [get_bd_pins pcc_stream_1/ap_rst_n] [get_bd_pins pcc_stream_2/ap_rst_n] [get_bd_pins pcc_stream_3/ap_rst_n] [get_bd_pins rst_mig_7series_0_100M/peripheral_aresetn]
  connect_bd_net -net xlconcat_0_dout [get_bd_pins axi_intc_0/intr] [get_bd_pins xlconcat_0/dout]

  # Create address segments
  create_bd_addr_seg -range 0x40000000 -offset 0x80000000 [get_bd_addr_spaces axi_dma_0/Data_SG] [get_bd_addr_segs mig_7series_0/memmap/memaddr] SEG_mig_7series_0_memaddr
  create_bd_addr_seg -range 0x40000000 -offset 0x80000000 [get_bd_addr_spaces axi_dma_0/Data_MM2S] [get_bd_addr_segs mig_7series_0/memmap/memaddr] SEG_mig_7series_0_memaddr
  create_bd_addr_seg -range 0x40000000 -offset 0x80000000 [get_bd_addr_spaces axi_dma_0/Data_S2MM] [get_bd_addr_segs mig_7series_0/memmap/memaddr] SEG_mig_7series_0_memaddr
  create_bd_addr_seg -range 0x40000000 -offset 0x80000000 [get_bd_addr_spaces axi_dma_1/Data_SG] [get_bd_addr_segs mig_7series_0/memmap/memaddr] SEG_mig_7series_0_memaddr
  create_bd_addr_seg -range 0x40000000 -offset 0x80000000 [get_bd_addr_spaces axi_dma_1/Data_MM2S] [get_bd_addr_segs mig_7series_0/memmap/memaddr] SEG_mig_7series_0_memaddr
  create_bd_addr_seg -range 0x40000000 -offset 0x80000000 [get_bd_addr_spaces axi_dma_1/Data_S2MM] [get_bd_addr_segs mig_7series_0/memmap/memaddr] SEG_mig_7series_0_memaddr
  create_bd_addr_seg -range 0x40000000 -offset 0x80000000 [get_bd_addr_spaces axi_dma_2/Data_SG] [get_bd_addr_segs mig_7series_0/memmap/memaddr] SEG_mig_7series_0_memaddr
  create_bd_addr_seg -range 0x40000000 -offset 0x80000000 [get_bd_addr_spaces axi_dma_2/Data_MM2S] [get_bd_addr_segs mig_7series_0/memmap/memaddr] SEG_mig_7series_0_memaddr
  create_bd_addr_seg -range 0x40000000 -offset 0x80000000 [get_bd_addr_spaces axi_dma_2/Data_S2MM] [get_bd_addr_segs mig_7series_0/memmap/memaddr] SEG_mig_7series_0_memaddr
  create_bd_addr_seg -range 0x40000000 -offset 0x80000000 [get_bd_addr_spaces axi_dma_3/Data_SG] [get_bd_addr_segs mig_7series_0/memmap/memaddr] SEG_mig_7series_0_memaddr
  create_bd_addr_seg -range 0x40000000 -offset 0x80000000 [get_bd_addr_spaces axi_dma_3/Data_MM2S] [get_bd_addr_segs mig_7series_0/memmap/memaddr] SEG_mig_7series_0_memaddr
  create_bd_addr_seg -range 0x40000000 -offset 0x80000000 [get_bd_addr_spaces axi_dma_3/Data_S2MM] [get_bd_addr_segs mig_7series_0/memmap/memaddr] SEG_mig_7series_0_memaddr
  create_bd_addr_seg -range 0x10000 -offset 0x41E00000 [get_bd_addr_spaces microblaze_0/Data] [get_bd_addr_segs axi_dma_0/S_AXI_LITE/Reg] SEG_axi_dma_0_Reg
  create_bd_addr_seg -range 0x10000 -offset 0x41E10000 [get_bd_addr_spaces microblaze_0/Data] [get_bd_addr_segs axi_dma_1/S_AXI_LITE/Reg] SEG_axi_dma_1_Reg
  create_bd_addr_seg -range 0x10000 -offset 0x41E20000 [get_bd_addr_spaces microblaze_0/Data] [get_bd_addr_segs axi_dma_2/S_AXI_LITE/Reg] SEG_axi_dma_2_Reg
  create_bd_addr_seg -range 0x10000 -offset 0x41E30000 [get_bd_addr_spaces microblaze_0/Data] [get_bd_addr_segs axi_dma_3/S_AXI_LITE/Reg] SEG_axi_dma_3_Reg
  create_bd_addr_seg -range 0x10000 -offset 0x41200000 [get_bd_addr_spaces microblaze_0/Data] [get_bd_addr_segs axi_intc_0/s_axi/Reg] SEG_axi_intc_0_Reg
  create_bd_addr_seg -range 0x10000 -offset 0x41C00000 [get_bd_addr_spaces microblaze_0/Data] [get_bd_addr_segs axi_timer_0/S_AXI/Reg] SEG_axi_timer_0_Reg
  create_bd_addr_seg -range 0x2000 -offset 0x0 [get_bd_addr_spaces microblaze_0/Data] [get_bd_addr_segs microblaze_0_local_memory/dlmb_bram_if_cntlr/SLMB/Mem] SEG_dlmb_bram_if_cntlr_Mem
  create_bd_addr_seg -range 0x2000 -offset 0x0 [get_bd_addr_spaces microblaze_0/Instruction] [get_bd_addr_segs microblaze_0_local_memory/ilmb_bram_if_cntlr/SLMB/Mem] SEG_ilmb_bram_if_cntlr_Mem
  create_bd_addr_seg -range 0x1000 -offset 0x41400000 [get_bd_addr_spaces microblaze_0/Data] [get_bd_addr_segs mdm_1/S_AXI/Reg] SEG_mdm_1_Reg
  create_bd_addr_seg -range 0x40000000 -offset 0x80000000 [get_bd_addr_spaces microblaze_0/Data] [get_bd_addr_segs mig_7series_0/memmap/memaddr] SEG_mig_7series_0_memaddr
  create_bd_addr_seg -range 0x40000000 -offset 0x80000000 [get_bd_addr_spaces microblaze_0/Instruction] [get_bd_addr_segs mig_7series_0/memmap/memaddr] SEG_mig_7series_0_memaddr

  # Perform GUI Layout
  regenerate_bd_layout -layout_string {
   guistr: "# # String gsaved with Nlview 6.5.5  2015-06-26 bk=1.3371 VDI=38 GEI=35 GUI=JA:1.8
#  -string -flagsOSRD
preplace port ddr3_sdram -pg 1 -y -420 -defaultsOSRD
preplace port reset -pg 1 -y -260 -defaultsOSRD
preplace port sys_diff_clock -pg 1 -y -390 -defaultsOSRD
preplace inst axis_data_fifo_1 -pg 1 -lvl 5 -y 150 -defaultsOSRD
preplace inst axis_data_fifo_2 -pg 1 -lvl 5 -y 470 -defaultsOSRD
preplace inst axi_dma_0 -pg 1 -lvl 1 -y -410 -defaultsOSRD
preplace inst rst_mig_7series_0_100M -pg 1 -lvl 5 -y -600 -defaultsOSRD
preplace inst axis_data_fifo_3 -pg 1 -lvl 5 -y 770 -defaultsOSRD
preplace inst axis_interconnect_0 -pg 1 -lvl 2 -y -190 -defaultsOSRD
preplace inst axi_dma_1 -pg 1 -lvl 1 -y -110 -defaultsOSRD
preplace inst axi_intc_0 -pg 1 -lvl 3 -y -600 -defaultsOSRD
preplace inst mig_7series_0 -pg 1 -lvl 5 -y -760 -defaultsOSRD
preplace inst axis_interconnect_1 -pg 1 -lvl 2 -y 150 -defaultsOSRD
preplace inst axi_dma_2 -pg 1 -lvl 1 -y 280 -defaultsOSRD
preplace inst axis_interconnect_2 -pg 1 -lvl 2 -y 550 -defaultsOSRD
preplace inst axi_dma_3 -pg 1 -lvl 1 -y 560 -defaultsOSRD
preplace inst microblaze_0_axi_periph -pg 1 -lvl 6 -y 520 -defaultsOSRD
preplace inst xlconcat_0 -pg 1 -lvl 4 -y -300 -defaultsOSRD
preplace inst axis_interconnect_3 -pg 1 -lvl 2 -y 880 -defaultsOSRD
preplace inst axi_timer_0 -pg 1 -lvl 2 -y -810 -defaultsOSRD
preplace inst pcc_stream_0 -pg 1 -lvl 4 -y -90 -defaultsOSRD
preplace inst mdm_1 -pg 1 -lvl 4 -y -770 -defaultsOSRD
preplace inst pcc_stream_1 -pg 1 -lvl 4 -y 160 -defaultsOSRD
preplace inst pcc_stream_2 -pg 1 -lvl 4 -y 480 -defaultsOSRD
preplace inst microblaze_0 -pg 1 -lvl 4 -y -610 -defaultsOSRD
preplace inst pcc_stream_3 -pg 1 -lvl 4 -y 780 -defaultsOSRD
preplace inst axi_mem_intercon -pg 1 -lvl 6 -y -210 -defaultsOSRD
preplace inst microblaze_0_local_memory -pg 1 -lvl 6 -y -800 -defaultsOSRD
preplace inst axis_data_fifo_0 -pg 1 -lvl 5 -y -60 -defaultsOSRD
preplace netloc microblaze_0_axi_periph_M02_AXI 1 2 5 NJ -970 NJ -970 NJ -970 NJ -970 3120
preplace netloc mig_7series_0_DDR3 1 5 2 NJ -720 NJ
preplace netloc pcc_stream_1_DMA_pcc 1 4 1 1920
preplace netloc axi_dma_2_mm2s_introut 1 1 3 670 -350 NJ -340 NJ
preplace netloc rst_mig_7series_0_100M_interconnect_aresetn 1 5 1 2370
preplace netloc axis_data_fifo_3_M_AXIS 1 0 6 NJ 710 NJ 710 NJ 690 NJ 690 NJ 690 2310
preplace netloc axi_dma_0_mm2s_introut 1 1 3 550 -380 NJ -380 NJ
preplace netloc sys_diff_clock_1 1 0 5 NJ -910 NJ -910 NJ -910 NJ -910 NJ
preplace netloc axi_dma_3_M_AXI_MM2S 1 1 5 NJ 310 NJ 310 NJ 310 NJ 310 NJ
preplace netloc axi_dma_2_M_AXI_S2MM 1 1 5 610 -470 NJ -470 NJ -470 NJ -470 NJ
preplace netloc microblaze_0_axi_periph_M03_AXI 1 0 7 NJ -970 NJ -970 NJ -960 NJ -960 NJ -960 NJ -960 3110
preplace netloc microblaze_0_axi_periph_M01_AXI 1 1 6 660 -980 NJ -980 NJ -980 NJ -980 NJ -980 3130
preplace netloc axis_interconnect_0_M01_AXIS 1 2 2 N -190 1280
preplace netloc pcc_stream_0_DMA_pcc 1 4 1 1840
preplace netloc microblaze_0_dlmb_1 1 4 2 NJ -880 NJ
preplace netloc microblaze_0_M_AXI_IC 1 4 2 NJ -890 NJ
preplace netloc microblaze_0_axi_periph_M00_AXI 1 3 4 NJ -990 NJ -990 NJ -990 3140
preplace netloc xlconcat_0_dout 1 2 3 NJ -460 N -460 1830
preplace netloc axis_interconnect_1_M02_AXIS 1 2 2 1020 160 N
preplace netloc microblaze_0_M_AXI_DP 1 4 2 NJ -860 NJ
preplace netloc axi_dma_1_M_AXI_MM2S 1 1 5 640 -20 NJ -20 NJ -510 NJ -510 NJ
preplace netloc axi_dma_0_M_AXI_SG 1 1 5 NJ -1060 NJ -1060 NJ -1060 NJ -1060 NJ
preplace netloc mig_7series_0_mmcm_locked 1 4 2 1950 -900 NJ
preplace netloc axi_dma_3_M_AXI_S2MM 1 1 5 NJ 350 NJ 350 NJ 350 NJ 350 NJ
preplace netloc axi_dma_3_s2mm_introut 1 1 3 NJ 320 NJ 320 NJ
preplace netloc mig_7series_0_ui_clk_sync_rst 1 4 2 1940 -910 NJ
preplace netloc axi_dma_1_s2mm_introut 1 1 3 600 -400 NJ -400 NJ
preplace netloc rst_mig_7series_0_100M_peripheral_aresetn 1 0 6 -40 -280 660 -700 1000 -700 1330 -440 1930 -460 NJ
preplace netloc microblaze_0_ilmb_1 1 4 2 NJ -850 NJ
preplace netloc axis_data_fifo_1_M_AXIS 1 0 6 NJ -530 NJ -530 NJ -490 NJ -490 NJ -490 2330
preplace netloc axi_dma_3_M_AXI_SG 1 1 5 NJ 380 NJ 380 NJ 380 NJ 380 NJ
preplace netloc axi_dma_0_M_AXIS_MM2S 1 1 1 680
preplace netloc mdm_1_debug_sys_rst 1 4 1 NJ
preplace netloc axi_dma_3_M_AXIS_MM2S 1 1 1 530
preplace netloc axis_interconnect_3_M02_AXIS 1 2 2 1040 780 N
preplace netloc axi_dma_1_M_AXI_SG 1 1 5 540 -1020 NJ -1020 NJ -1020 NJ -1020 NJ
preplace netloc axis_interconnect_2_M02_AXIS 1 2 2 1040 480 N
preplace netloc axi_dma_2_M_AXIS_MM2S 1 1 1 530
preplace netloc axi_dma_0_M_AXI_MM2S 1 1 5 NJ -1030 NJ -1030 NJ -1030 NJ -1030 NJ
preplace netloc microblaze_0_Clk 1 0 6 -50 -290 650 -710 NJ -710 NJ -840 NJ -920 2400
preplace netloc axi_dma_3_mm2s_introut 1 1 3 NJ 360 N 360 1360
preplace netloc axi_dma_0_M_AXI_S2MM 1 1 5 NJ -930 NJ -930 NJ -930 NJ -930 NJ
preplace netloc rst_mig_7series_0_100M_bus_struct_reset 1 5 1 2370
preplace netloc axis_interconnect_3_M01_AXIS 1 2 2 1030 760 N
preplace netloc axis_data_fifo_2_M_AXIS 1 0 6 NJ 400 NJ 390 NJ 390 NJ 390 NJ 390 2310
preplace netloc axis_data_fifo_0_M_AXIS 1 0 6 NJ -540 NJ -540 NJ -450 NJ -450 NJ -450 2310
preplace netloc axis_interconnect_2_M00_AXIS 1 2 2 1010 440 N
preplace netloc axi_dma_2_M_AXI_SG 1 1 5 560 -500 NJ -500 NJ -500 NJ -500 NJ
preplace netloc axi_dma_1_mm2s_introut 1 1 3 570 -410 NJ -410 NJ
preplace netloc microblaze_0_debug 1 3 2 NJ -700 NJ
preplace netloc axis_interconnect_0_M02_AXIS 1 2 2 N -170 1270
preplace netloc axis_interconnect_3_M00_AXIS 1 2 2 1020 740 N
preplace netloc axis_interconnect_0_M00_AXIS 1 2 2 N -210 1290
preplace netloc rst_mig_7series_0_100M_mb_reset 1 2 4 NJ -730 NJ -1050 NJ -1050 NJ
preplace netloc microblaze_0_axi_periph_M06_AXI 1 0 7 NJ 720 NJ 720 NJ 870 NJ 870 NJ 870 NJ 870 3100
preplace netloc axi_dma_2_M_AXI_MM2S 1 1 5 590 -480 NJ -480 NJ -480 NJ -480 NJ
preplace netloc pcc_stream_2_DMA_pcc 1 4 1 1920
preplace netloc microblaze_0_axi_periph_M04_AXI 1 0 7 NJ -950 NJ -950 NJ -950 NJ -950 NJ -950 NJ -950 3100
preplace netloc axis_interconnect_1_M01_AXIS 1 2 2 1020 140 N
preplace netloc axi_dma_1_M_AXIS_MM2S 1 1 1 620
preplace netloc pcc_stream_3_DMA_pcc 1 4 1 1950
preplace netloc axis_interconnect_2_M01_AXIS 1 2 2 1030 460 N
preplace netloc axi_mem_intercon_M00_AXI 1 4 3 1930 -1000 NJ -1000 3150
preplace netloc microblaze_0_M_AXI_DC 1 4 2 NJ -870 NJ
preplace netloc axi_dma_1_M_AXI_S2MM 1 1 5 630 -30 NJ -180 NJ -180 NJ -180 NJ
preplace netloc axis_interconnect_1_M00_AXIS 1 2 2 1020 120 N
preplace netloc axi_dma_0_s2mm_introut 1 1 3 580 -370 NJ -350 NJ
preplace netloc reset_1 1 0 5 NJ -920 NJ -920 NJ -920 NJ -920 NJ
preplace netloc microblaze_0_axi_periph_M05_AXI 1 0 7 NJ 10 NJ -10 NJ -10 NJ 0 NJ 20 NJ 760 3110
preplace netloc axi_intc_0_interrupt 1 3 1 1300
preplace netloc axi_dma_2_s2mm_introut 1 1 3 NJ 340 NJ 340 NJ
levelinfo -pg 1 -90 340 850 1160 1610 2140 2950 3180 -top -1070 -bot 1040
",
}

  # Restore current instance
  current_bd_instance $oldCurInst

  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""


