############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 2015 Xilinx Inc. All rights reserved.
############################################################
open_project pcc_HLS
set_top pcc_stream
add_files axis.hpp
add_files pcc_stream.cpp
add_files pcc_stream.hpp
add_files -tb main.cpp
open_solution "solution1"
set_part {xc7vx485tffg1761-2}
create_clock -period 10 -name default
#source "./pcc_HLS/solution1/directives.tcl"
csim_design
csynth_design
cosim_design
export_design -evaluate verilog -format ip_catalog
