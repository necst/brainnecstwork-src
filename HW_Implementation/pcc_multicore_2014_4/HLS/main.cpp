/*
 * Copyright 2016 Enrico Reggiani, Eleonora D'Arnese, Marco Gucciardi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/  
#include <stdio.h>
#include "axis.hpp"
#include "pcc_stream.hpp"
void initValues(float *mat, int rows, int cols)
{
	int i, j;

	for (i = 0; i < rows; i++)
		for (j = 0; j < cols; j++)
			mat[i * cols + j] = i + j;
}

int main()
{

	int P = 1000;
	int K = 50;
	int KDEV = K + 1;

	int i, j, k, r, a, b, c1, c2, s;
	float tmp = 0;
	float devtot = 0;

	//Memory's Initialization
	float *pixel = (float *)malloc(sizeof(float) * P * K);
	float *average = (float *)malloc(sizeof(float) * P);
	float *dep = (float *)malloc(P * KDEV * sizeof(float));

	initValues(pixel, P, K);

	printf("Starting Computation... \n");

	for (i = 0; i < P; i++) {
		average[i] = 0;
		// printf("Computing avg for ROW: %d\n", i);
		for (j = 0; j < K; j++) {
			average[i] = average[i] + *(pixel + K * i + j);
		}
		average[i] = average[i] / K;
	}
	printf("Media\n");
	for (k = 0; k < P; k++) {
		for (r = 0; r < K; r++) {
			dep[k * KDEV + r] = *(pixel + k * K + r) - average[k];
		}
	}

	printf("dep\n");
	for (a = 0; a < P; a++) {
		tmp = 0;
		for (b = 0; b < K; b++) {
			tmp = tmp + dep[a * KDEV + b] * dep[a * KDEV + b];
		}
		tmp = tmp / (K - 1.0);
		dep[a * KDEV + K] = sqrt(tmp);
	}
	//          printf("devs\n");

	StreamData dep1, dep2, totCorr, pccStream;

	AXIS_Data temp;
	temp.data.ui = 1;
	totCorr << temp;

	for (int r = 0; r < 1000; ++r) {
		for (int f = 0; f < KDEV; ++f) {
			temp.data.f = dep[r * KDEV + f];
			dep1 << temp;
			dep2 << temp;
		}
	}

	pcc_stream(totCorr, dep1, dep2, pccStream);

	for (int r1 = 0; r1 < 1000; ++r1) {
		for (int r2 = 0; r2 < 1000; ++r2) {
			pccStream >> temp;
			float t = temp.data.f;
			float diff = abs(t - 0.9800);

			bool last = temp.last;

			if (diff > 0.0001) {
				printf("Error! Element [%d] [%d] is %f\n", r1,
				       r2, t);
			}

		}
	}
	printf("End\n");

	return 0;
}
