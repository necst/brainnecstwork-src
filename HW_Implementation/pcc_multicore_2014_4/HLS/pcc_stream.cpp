/*
 * Copyright 2016 Enrico Reggiani, Eleonora D'Arnese, Marco Gucciardi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
#include "pcc_stream.hpp"
#define BLOCKROWS		1000
#define NUMBEROFFRAME	50
#define DEVS			(50 + 1)

//--------------------------------------------------------------------------------------------------------
void memsetAXIS_Data(AXIS_Data & d)
{
	d.data.f = 0;
	d.last = 0;
	d.keep = (1 << (BIT_WIDTH / 8)) - 1;	// Keep all the bytes
	d.strb = (1 << (BIT_WIDTH / 8)) - 1;	// All the bytes are data

	// Set to default value if not using
	d.dest = 0;
	d.id = 0;
	d.user = 0;
}

//--------------------------------------------------------------------------------------------------------

void getNumberOfCorrelation(StreamData & DMA_NumberOfCorrelation,
			    int &numberOfCorrelation)
{
#pragma HLS PIPELINE II=1
	AXIS_Data temp;
	DMA_NumberOfCorrelation >> temp;
	numberOfCorrelation = temp.data.ui;
}

void depToBuffer(StreamData & DMA_dep, float buffr[BLOCKROWS][DEVS])
{
	AXIS_Data temp;
 LOOP_ROW:for (int i = 0; i < BLOCKROWS; ++i)
 LOOP_FRAME:	for (int j = 0; j < DEVS; ++j) {
#pragma HLS PIPELINE II=1
#pragma HLS LOOP_FLATTEN
			DMA_dep >> temp;
			buffr[i][j] = temp.data.f;
		}
}

void pushOutput(StreamData & DMA_pcc, float pcc, bool last)
{
#pragma HLS PIPELINE II=1
	AXIS_Data pccStream;

	memsetAXIS_Data(pccStream);

	pccStream.data.f = pcc;
	pccStream.last = last;
	pccStream.dest = 0;
	pccStream.id = 0;

	DMA_pcc << pccStream;
}

void calculatePcc(float buffr1[BLOCKROWS][DEVS], float buffr2[BLOCKROWS][DEVS],
		  float &pcc, StreamData & DMA_pcc)
{
	int s, k, h;
	int j = 0;

	float acc = 0;
	float temp_array0[50];
	float temp_array1[25];
	float temp_array2[12];
	float temp_array3[6];
	float temp_array4[3];
	float temp_array5[2];

 LOOP_G:for (k = 0; k < BLOCKROWS; ++k) {
 LOOP_I:	for (h = 0; h < BLOCKROWS; ++h) {
#pragma HLS PIPELINE II=1
#pragma HLS LOOP_FLATTEN
 TEMP_ARRAY_LOOP0:	for (s = 0; s < 50; s++) {
				temp_array0[s] = buffr1[k][s] * buffr2[h][s];
			}

 TEMP_ARRAY_LOOP1:	for (j = 0; j < 25; j++) {
				temp_array1[j] =
				    temp_array0[j * 2] + temp_array0[j * 2 + 1];
			}

 TEMP_ARRAY_LOOP2:	for (j = 0; j < 12; j++) {
				temp_array2[j] =
				    temp_array1[j * 2] + temp_array1[j * 2 + 1];
			}

 TEMP_ARRAY_LOOP3:	for (j = 0; j < 6; j++) {
				temp_array3[j] =
				    temp_array2[j * 2] + temp_array2[j * 2 + 1];
			}

 TEMP_ARRAY_LOOP4:	for (j = 0; j < 3; j++) {
				temp_array4[j] =
				    temp_array3[j * 2] + temp_array3[j * 2 + 1];
			}

			temp_array5[0] = temp_array4[0] + temp_array4[1];
			temp_array5[1] = temp_array4[2] + temp_array1[24];

			acc = temp_array5[0] + temp_array5[1];

			pcc =
			    acc / (buffr1[k][50] * buffr2[h][50] *
				   NUMBEROFFRAME);

			bool last = (k == (BLOCKROWS - 1)
				     && h == (BLOCKROWS - 1));
			pushOutput(DMA_pcc, pcc, last);
		}
	}
}

void insideLoopCalculation(float buffr1[BLOCKROWS][DEVS], StreamData & DMA_dep2,
			   StreamData & DMA_pcc)
{
#pragma HLS DATAFLOW

	float pcc, acc;
	float buffr2[BLOCKROWS][DEVS];

#pragma HLS RESOURCE variable=buffr2 core=RAM_2P_BRAM
#pragma HLS ARRAY_PARTITION variable=buffr2 complete dim=2

	depToBuffer(DMA_dep2, buffr2);
	calculatePcc(buffr1, buffr2, pcc, DMA_pcc);
}

//--------------------------------------------------------------------------------------------------------
void pcc_stream(StreamData & DMA_NumberOfCorrelation, StreamData & DMA_dep1,
		StreamData & DMA_dep2, StreamData & DMA_pcc)
{

#pragma HLS INTERFACE ap_ctrl_none port=return

#pragma HLS INTERFACE axis port=DMA_pcc
#pragma HLS INTERFACE axis port=DMA_dep2
#pragma HLS INTERFACE axis port=DMA_dep1
#pragma HLS INTERFACE axis port=DMA_NumberOfCorrelation
#pragma HLS DATAFLOW
	float buffr1[BLOCKROWS][DEVS];

#pragma HLS RESOURCE variable=buffr1 core=RAM_2P_BRAM
#pragma HLS ARRAY_PARTITION variable=buffr1 complete dim=2
	int numberOfCorrelation, i, j;

	getNumberOfCorrelation(DMA_NumberOfCorrelation, numberOfCorrelation);
	depToBuffer(DMA_dep1, buffr1);

	for (i = 0; i < numberOfCorrelation; ++i)
		insideLoopCalculation(buffr1, DMA_dep2, DMA_pcc);
}

//-----------------------------------------------------------------------------------------------------------------
