/*
 * Copyright 2016 Enrico Reggiani, Eleonora D'Arnese, Marco Gucciardi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
#ifndef AXIS_HPP
#define AXIS_HPP

#include "ap_int.h"

template < class DataType, int D, int U, int TI, int TD > struct my_ap_axis {
	DataType data;
	 ap_uint < (D + 7) / 8 > keep;
	 ap_uint < (D + 7) / 8 > strb;
	 ap_uint < U > user;
	 ap_uint < 1 > last;
	 ap_uint < TI > id;
	 ap_uint < TD > dest;
};

#endif
