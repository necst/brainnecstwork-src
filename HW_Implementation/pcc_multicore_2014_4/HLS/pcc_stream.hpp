/*
 * Copyright 2016 Enrico Reggiani, Eleonora D'Arnese, Marco Gucciardi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "axis.hpp"
#include <hls_stream.h>

#define BIT_WIDTH 32

typedef union {
	float f;
	unsigned int ui;
	char c[BIT_WIDTH / 8];
} DataType;

typedef my_ap_axis < DataType, BIT_WIDTH, 4, 5, 5 > AXIS_Data;
typedef hls::stream < AXIS_Data > StreamData;

void memsetAXIS_Data(AXIS_Data & d);

void pcc_stream(StreamData & DMA_NumberOfCorrelation, StreamData & DMA_dep1,
		StreamData & DMA_dep2, StreamData & DMA_pcc);
